.class public abstract Lcom/google/android/gms/internal/ii;
.super Lcom/google/android/gms/internal/jy;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/fm;

.field private final b:Lcom/google/android/gms/internal/ih;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/fm;Lcom/google/android/gms/internal/ih;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/jy;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ii;->a:Lcom/google/android/gms/internal/fm;

    iput-object p2, p0, Lcom/google/android/gms/internal/ii;->b:Lcom/google/android/gms/internal/ih;

    return-void
.end method

.method private static a(Lcom/google/android/gms/internal/ip;Lcom/google/android/gms/internal/fm;)Lcom/google/android/gms/internal/fo;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-interface {p0, p1}, Lcom/google/android/gms/internal/ip;->a(Lcom/google/android/gms/internal/fm;)Lcom/google/android/gms/internal/fo;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/google/android/gms/internal/jt;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ii;->d()Lcom/google/android/gms/internal/ip;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/fo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/internal/ii;->c()V

    iget-object v1, p0, Lcom/google/android/gms/internal/ii;->b:Lcom/google/android/gms/internal/ih;

    invoke-interface {v1, v0}, Lcom/google/android/gms/internal/ih;->a(Lcom/google/android/gms/internal/fo;)V

    return-void

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/internal/ii;->a:Lcom/google/android/gms/internal/fm;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/ii;->a(Lcom/google/android/gms/internal/ip;Lcom/google/android/gms/internal/fm;)Lcom/google/android/gms/internal/fo;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/fo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/internal/fo;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ii;->c()V

    throw v0
.end method

.method public final b()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ii;->c()V

    return-void
.end method

.method public abstract c()V
.end method

.method public abstract d()Lcom/google/android/gms/internal/ip;
.end method
