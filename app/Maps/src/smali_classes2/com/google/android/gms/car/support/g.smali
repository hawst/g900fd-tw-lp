.class final Lcom/google/android/gms/car/support/g;
.super Lcom/google/android/gms/car/support/ai;


# static fields
.field static a:Z

.field static final b:Z

.field static final u:Landroid/view/animation/Interpolator;

.field static final v:Landroid/view/animation/Interpolator;


# instance fields
.field c:Z

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/car/support/ab;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/car/support/ab;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/car/support/bg;",
            ">;"
        }
    .end annotation
.end field

.field h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/car/support/bg;",
            ">;"
        }
    .end annotation
.end field

.field i:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field j:I

.field k:Lcom/google/android/gms/car/support/ae;

.field l:Lcom/google/android/gms/car/support/bk;

.field m:Lcom/google/android/gms/car/support/ab;

.field n:Z

.field o:Z

.field p:Z

.field q:Z

.field r:Landroid/os/Bundle;

.field s:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation
.end field

.field t:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v0, 0x0

    const/high16 v4, 0x40200000    # 2.5f

    const/high16 v3, 0x3fc00000    # 1.5f

    sput-boolean v0, Lcom/google/android/gms/car/support/g;->a:Z

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/google/android/gms/car/support/g;->b:Z

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/gms/car/support/g;->u:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/gms/car/support/g;->v:Landroid/view/animation/Interpolator;

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v4}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v3}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    return-void
.end method

.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/car/support/ai;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/support/g;->j:I

    iput-object v1, p0, Lcom/google/android/gms/car/support/g;->r:Landroid/os/Bundle;

    iput-object v1, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    new-instance v0, Lcom/google/android/gms/car/support/bl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/bl;-><init>(Lcom/google/android/gms/car/support/g;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->t:Ljava/lang/Runnable;

    return-void
.end method

.method private static a(FFFF)Landroid/view/animation/Animation;
    .locals 12

    const-wide/16 v10, 0xdc

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    new-instance v9, Landroid/view/animation/AnimationSet;

    const/4 v0, 0x0

    invoke-direct {v9, v0}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    move v1, p0

    move v2, p1

    move v3, p0

    move v4, p1

    move v7, v5

    move v8, v6

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFIFIF)V

    sget-object v1, Lcom/google/android/gms/car/support/g;->u:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/ScaleAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v10, v11}, Landroid/view/animation/ScaleAnimation;->setDuration(J)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    sget-object v1, Lcom/google/android/gms/car/support/g;->v:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v10, v11}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v9, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    return-object v9
.end method

.method private a(Lcom/google/android/gms/car/support/ab;IZI)Landroid/view/animation/Animation;
    .locals 8

    const-wide/16 v6, 0xdc

    const v5, 0x3f79999a    # 0.975f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    iget v0, p1, Lcom/google/android/gms/car/support/ab;->F:I

    invoke-static {}, Lcom/google/android/gms/car/support/ab;->b()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget v0, p1, Lcom/google/android/gms/car/support/ab;->F:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/ae;->h()Landroid/content/Context;

    move-result-object v0

    iget v2, p1, Lcom/google/android/gms/car/support/ab;->F:I

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    if-nez v0, :cond_0

    :cond_2
    if-nez p2, :cond_3

    move-object v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    sparse-switch p2, :sswitch_data_0

    :goto_1
    if-gez v0, :cond_7

    move-object v0, v1

    goto :goto_0

    :sswitch_0
    if-eqz p3, :cond_4

    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    const/4 v0, 0x2

    goto :goto_1

    :sswitch_1
    if-eqz p3, :cond_5

    const/4 v0, 0x3

    goto :goto_1

    :cond_5
    const/4 v0, 0x4

    goto :goto_1

    :sswitch_2
    if-eqz p3, :cond_6

    const/4 v0, 0x5

    goto :goto_1

    :cond_6
    const/4 v0, 0x6

    goto :goto_1

    :cond_7
    packed-switch v0, :pswitch_data_0

    if-nez p4, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v0, v0, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v0, v0, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget p4, v0, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    :cond_8
    if-nez p4, :cond_9

    move-object v0, v1

    goto :goto_0

    :pswitch_0
    const/high16 v0, 0x3f900000    # 1.125f

    invoke-static {v0, v3, v4, v3}, Lcom/google/android/gms/car/support/g;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-static {v3, v5, v3, v4}, Lcom/google/android/gms/car/support/g;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-static {v5, v3, v4, v3}, Lcom/google/android/gms/car/support/g;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const v0, 0x3f89999a    # 1.075f

    invoke-static {v3, v0, v3, v4}, Lcom/google/android/gms/car/support/g;->a(FFFF)Landroid/view/animation/Animation;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    sget-object v1, Lcom/google/android/gms/car/support/g;->v:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    sget-object v1, Lcom/google/android/gms/car/support/g;->v:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    goto :goto_0

    :cond_9
    move-object v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/gms/car/support/ab;
    .locals 5

    const/4 v0, -0x1

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-ne v1, v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment no longer exists for key "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-nez v0, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fragment no longer exists for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    goto :goto_0
.end method

.method private a(ILcom/google/android/gms/car/support/bg;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :goto_1
    if-ge v0, p1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->i:Ljava/util/ArrayList;

    if-nez v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/car/support/g;->i:Ljava/util/ArrayList;

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->i:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Lcom/google/android/gms/car/support/ab;)V
    .locals 2

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    if-nez v0, :cond_2

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    :goto_1
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->saveHierarchyState(Landroid/util/SparseArray;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->s:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    goto :goto_1
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .locals 7

    invoke-virtual {p1}, Ljava/lang/RuntimeException;->getMessage()Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/car/support/bo;

    const-string v1, "FragmentManager"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bo;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/PrintWriter;

    invoke-direct {v1, v0}, Ljava/io/PrintWriter;-><init>(Ljava/io/Writer;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    const-string v2, "  "

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "Local FragmentActivity "

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " State:"

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mCreated="

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ae;->s:Z

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, "mResumed="

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ae;->t:Z

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mStopped="

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ae;->u:Z

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mReallyStopped="

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ae;->v:Z

    invoke-virtual {v1, v5}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v0, v0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "View Hierarchy:"

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    throw p1

    :cond_0
    :try_start_1
    const-string v0, "  "

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v1, v3}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    const/4 v0, 0x0

    sparse-switch p0, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    const/16 v0, 0x2002

    goto :goto_0

    :sswitch_1
    const/16 v0, 0x1001

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1003

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1001 -> :sswitch_0
        0x1003 -> :sswitch_2
        0x2002 -> :sswitch_1
    .end sparse-switch
.end method

.method private k()V
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    move v6, v3

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/ab;

    if-eqz v1, :cond_2

    iget-boolean v0, v1, Lcom/google/android/gms/car/support/ab;->J:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->c:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/g;->q:Z

    :cond_2
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_3
    iput-boolean v3, v1, Lcom/google/android/gms/car/support/ab;->J:Z

    iget v2, p0, Lcom/google/android/gms/car/support/g;->j:I

    move-object v0, p0

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    goto :goto_1
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/car/support/ab;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_1

    iget v2, v0, Lcom/google/android/gms/car/support/ab;->w:I

    if-ne v2, p1, :cond_1

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_3

    iget v2, v0, Lcom/google/android/gms/car/support/ab;->w:I

    if-eq v2, p1, :cond_0

    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final a(IIIZ)V
    .locals 7

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p4, :cond_2

    iget v0, p0, Lcom/google/android/gms/car/support/g;->j:I

    if-ne v0, p1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput p1, p0, Lcom/google/android/gms/car/support/g;->j:I

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    move v6, v5

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/ab;

    if-eqz v1, :cond_3

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/car/support/g;->k()V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/support/g;->j:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    invoke-static {}, Lcom/google/android/gms/car/support/ae;->w()V

    iput-boolean v5, p0, Lcom/google/android/gms/car/support/g;->n:Z

    goto :goto_0
.end method

.method final a(IZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v0, p2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/ab;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v2, v0, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/g;->a(Landroid/content/res/Configuration;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method final a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Parcelable;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/car/support/ab;",
            ">;)V"
        }
    .end annotation

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/google/android/gms/car/support/h;

    iget-object v0, p1, Lcom/google/android/gms/car/support/h;->a:[Lcom/google/android/gms/car/support/FragmentState;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    iget-object v3, p1, Lcom/google/android/gms/car/support/h;->a:[Lcom/google/android/gms/car/support/FragmentState;

    iget v4, v0, Lcom/google/android/gms/car/support/ab;->f:I

    aget-object v3, v3, v4

    iput-object v0, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iput-object v8, v0, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    iput v2, v0, Lcom/google/android/gms/car/support/ab;->r:I

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/ab;->p:Z

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/ab;->l:Z

    iput-object v8, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    if-eqz v4, :cond_2

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v5, v5, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v3, v3, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    const-string v4, "android:view_state"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/gms/car/support/h;->a:[Lcom/google/android/gms/car/support/FragmentState;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_4
    move v0, v2

    :goto_2
    iget-object v1, p1, Lcom/google/android/gms/car/support/h;->a:[Lcom/google/android/gms/car/support/FragmentState;

    array-length v1, v1

    if-ge v0, v1, :cond_a

    iget-object v1, p1, Lcom/google/android/gms/car/support/h;->a:[Lcom/google/android/gms/car/support/FragmentState;

    aget-object v3, v1, v0

    if-eqz v3, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v4, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    if-eqz v5, :cond_5

    iget-object v1, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    :goto_3
    iget-object v4, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v8, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    if-eqz v5, :cond_6

    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    iget-object v6, v1, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_6
    invoke-virtual {v1}, Lcom/google/android/gms/car/support/ae;->h()Landroid/content/Context;

    move-result-object v5

    iget-object v6, v3, Lcom/google/android/gms/car/support/FragmentState;->a:Ljava/lang/String;

    iget-object v7, v3, Lcom/google/android/gms/car/support/FragmentState;->i:Landroid/os/Bundle;

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/car/support/ab;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/car/support/ab;

    move-result-object v5

    iput-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    if-eqz v5, :cond_7

    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    iget-object v6, v1, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget-object v6, v3, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    iput-object v6, v5, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    :cond_7
    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget v6, v3, Lcom/google/android/gms/car/support/FragmentState;->b:I

    invoke-virtual {v5, v6, v4}, Lcom/google/android/gms/car/support/ab;->a(ILcom/google/android/gms/car/support/ab;)V

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget-boolean v5, v3, Lcom/google/android/gms/car/support/FragmentState;->c:Z

    iput-boolean v5, v4, Lcom/google/android/gms/car/support/ab;->o:Z

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iput-boolean v9, v4, Lcom/google/android/gms/car/support/ab;->q:Z

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget v5, v3, Lcom/google/android/gms/car/support/FragmentState;->d:I

    iput v5, v4, Lcom/google/android/gms/car/support/ab;->w:I

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget v5, v3, Lcom/google/android/gms/car/support/FragmentState;->e:I

    iput v5, v4, Lcom/google/android/gms/car/support/ab;->x:I

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget-object v5, v3, Lcom/google/android/gms/car/support/FragmentState;->f:Ljava/lang/String;

    iput-object v5, v4, Lcom/google/android/gms/car/support/ab;->y:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget-boolean v5, v3, Lcom/google/android/gms/car/support/FragmentState;->g:Z

    iput-boolean v5, v4, Lcom/google/android/gms/car/support/ab;->B:Z

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget-boolean v5, v3, Lcom/google/android/gms/car/support/FragmentState;->h:Z

    iput-boolean v5, v4, Lcom/google/android/gms/car/support/ab;->A:Z

    iget-object v4, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    iget-object v1, v1, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-object v1, v4, Lcom/google/android/gms/car/support/ab;->s:Lcom/google/android/gms/car/support/g;

    iget-object v1, v3, Lcom/google/android/gms/car/support/FragmentState;->k:Lcom/google/android/gms/car/support/ab;

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_9

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_a
    if-eqz p2, :cond_d

    move v3, v2

    :goto_5
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_d

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    iget v1, v0, Lcom/google/android/gms/car/support/ab;->j:I

    if-ltz v1, :cond_b

    iget v1, v0, Lcom/google/android/gms/car/support/ab;->j:I

    iget-object v4, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_c

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    iget v4, v0, Lcom/google/android/gms/car/support/ab;->j:I

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/ab;

    iput-object v1, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    :cond_b
    :goto_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Re-attaching retained fragment "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " target no longer exists: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v4, v0, Lcom/google/android/gms/car/support/ab;->j:I

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput-object v8, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    goto :goto_6

    :cond_d
    iget-object v0, p1, Lcom/google/android/gms/car/support/h;->b:[I

    if-eqz v0, :cond_10

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/gms/car/support/h;->b:[I

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    move v1, v2

    :goto_7
    iget-object v0, p1, Lcom/google/android/gms/car/support/h;->b:[I

    array-length v0, v0

    if-ge v1, v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/google/android/gms/car/support/h;->b:[I

    aget v3, v3, v1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-nez v0, :cond_e

    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No instantiated fragment for index #"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/google/android/gms/car/support/h;->b:[I

    aget v5, v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    :cond_e
    iput-boolean v9, v0, Lcom/google/android/gms/car/support/ab;->l:Z

    iget-object v3, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already added!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_f
    iget-object v3, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_10
    iput-object v8, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    :cond_11
    iget-object v0, p1, Lcom/google/android/gms/car/support/h;->c:[Lcom/google/android/gms/car/support/c;

    if-eqz v0, :cond_13

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcom/google/android/gms/car/support/h;->c:[Lcom/google/android/gms/car/support/c;

    array-length v1, v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    :goto_8
    iget-object v0, p1, Lcom/google/android/gms/car/support/h;->c:[Lcom/google/android/gms/car/support/c;

    array-length v0, v0

    if-ge v2, v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/car/support/h;->c:[Lcom/google/android/gms/car/support/c;

    aget-object v0, v0, v2

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/support/c;->a(Lcom/google/android/gms/car/support/g;)Lcom/google/android/gms/car/support/bg;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v0, Lcom/google/android/gms/car/support/bg;->i:I

    if-ltz v1, :cond_12

    iget v1, v0, Lcom/google/android/gms/car/support/bg;->i:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/car/support/g;->a(ILcom/google/android/gms/car/support/bg;)V

    :cond_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    :cond_13
    iput-object v8, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/support/ab;II)V
    .locals 6

    const/4 v1, 0x1

    const/4 v5, 0x0

    iget v0, p1, Lcom/google/android/gms/car/support/ab;->r:I

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iget-boolean v2, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    if-eqz v2, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_1
    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    iput-boolean v1, p1, Lcom/google/android/gms/car/support/ab;->m:Z

    if-eqz v0, :cond_5

    move v2, v5

    :goto_2
    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    :cond_2
    return-void

    :cond_3
    move v0, v5

    goto :goto_0

    :cond_4
    move v0, v5

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2
.end method

.method final a(Lcom/google/android/gms/car/support/ab;IIIZ)V
    .locals 10

    const/4 v9, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    if-eqz v0, :cond_1

    :cond_0
    if-le p2, v5, :cond_1

    move p2, v5

    :cond_1
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->m:Z

    if-eqz v0, :cond_2

    iget v0, p1, Lcom/google/android/gms/car/support/ab;->a:I

    if-le p2, v0, :cond_2

    iget p2, p1, Lcom/google/android/gms/car/support/ab;->a:I

    :cond_2
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->J:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lcom/google/android/gms/car/support/ab;->a:I

    if-ge v0, v9, :cond_3

    if-le p2, v6, :cond_3

    move p2, v6

    :cond_3
    iget v0, p1, Lcom/google/android/gms/car/support/ab;->a:I

    if-ge v0, p2, :cond_27

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->o:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->p:Z

    if-nez v0, :cond_4

    :goto_0
    return-void

    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    if-eqz v0, :cond_5

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    iget v2, p1, Lcom/google/android/gms/car/support/ab;->c:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    :cond_5
    iget v0, p1, Lcom/google/android/gms/car/support/ab;->a:I

    packed-switch v0, :pswitch_data_0

    :cond_6
    :goto_1
    iput p2, p1, Lcom/google/android/gms/car/support/ab;->a:I

    goto :goto_0

    :pswitch_0
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    const-string v1, "android:view_state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSparseParcelableArray(Ljava/lang/String;)Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    const-string v1, "android:target_state"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/support/g;->a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/gms/car/support/ab;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    const-string v1, "android:target_req_state"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p1, Lcom/google/android/gms/car/support/ab;->k:I

    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    const-string v1, "android:user_visible_hint"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->K:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->K:Z

    if-nez v0, :cond_8

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->J:Z

    if-le p2, v6, :cond_8

    move p2, v6

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->v:Lcom/google/android/gms/car/support/ab;

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    iget-object v0, v0, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    :goto_2
    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->s:Lcom/google/android/gms/car/support/g;

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onAttach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v0, v0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    goto :goto_2

    :cond_a
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->v:Lcom/google/android/gms/car/support/ab;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    invoke-static {}, Lcom/google/android/gms/car/support/ae;->x()V

    :cond_b
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v1, :cond_c

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/g;->noteStateNotSaved()V

    :cond_c
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v1, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v1, :cond_d

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    if-eqz v0, :cond_f

    const-string v1, "android:support:fragments"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-nez v1, :cond_e

    new-instance v1, Lcom/google/android/gms/car/support/g;

    invoke-direct {v1}, Lcom/google/android/gms/car/support/g;-><init>()V

    iput-object v1, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    iget-object v2, p1, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    new-instance v4, Lcom/google/android/gms/car/support/ac;

    invoke-direct {v4, p1}, Lcom/google/android/gms/car/support/ac;-><init>(Lcom/google/android/gms/car/support/ab;)V

    invoke-virtual {v1, v2, v4, p1}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ae;Lcom/google/android/gms/car/support/bk;Lcom/google/android/gms/car/support/ab;)V

    :cond_e
    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v1, v0, v7}, Lcom/google/android/gms/car/support/g;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->c()V

    :cond_f
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->C:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->o:Z

    if-eqz v0, :cond_11

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    iget-object v0, v0, Lcom/google/android/gms/car/l;->m:Landroid/view/LayoutInflater;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v7, v1}, Lcom/google/android/gms/car/support/ab;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v0, :cond_12

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/car/support/k;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->z:Z

    if-eqz v0, :cond_10

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_10
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/car/support/ab;->c()V

    :cond_11
    :goto_3
    :pswitch_1
    if-le p2, v5, :cond_20

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->o:Z

    if-nez v0, :cond_19

    iget v0, p1, Lcom/google/android/gms/car/support/ab;->x:I

    if-eqz v0, :cond_3b

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->l:Lcom/google/android/gms/car/support/bk;

    iget v1, p1, Lcom/google/android/gms/car/support/ab;->x:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/support/bk;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_15

    iget-boolean v1, p1, Lcom/google/android/gms/car/support/ab;->q:Z

    if-nez v1, :cond_15

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No view found for id 0x"

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Lcom/google/android/gms/car/support/ab;->x:I

    invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    if-nez v4, :cond_13

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    goto :goto_3

    :cond_13
    iget-object v4, p1, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    iget-object v8, v4, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    if-nez v8, :cond_14

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarProjectionActivity not initialized with attach()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_14
    iget-object v4, v4, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v8, p1, Lcom/google/android/gms/car/support/ab;->x:I

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ") for fragment "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    :cond_15
    :goto_4
    iput-object v0, p1, Lcom/google/android/gms/car/support/ab;->G:Landroid/view/ViewGroup;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    iget-object v1, v1, Lcom/google/android/gms/car/l;->m:Landroid/view/LayoutInflater;

    iget-object v2, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    invoke-virtual {p1, v1, v0, v2}, Lcom/google/android/gms/car/support/ab;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v1, :cond_1b

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iput-object v1, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-static {v1}, Lcom/google/android/gms/car/support/k;->a(Landroid/view/View;)Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v0, :cond_17

    invoke-direct {p0, p1, p3, v5, p4}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IZI)Landroid/view/animation/Animation;

    move-result-object v1

    if-eqz v1, :cond_16

    iget-object v2, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_16
    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_17
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->z:Z

    if-eqz v0, :cond_18

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_18
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/car/support/ab;->c()V

    :cond_19
    :goto_5
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_1a

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->noteStateNotSaved()V

    :cond_1a
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_1c

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    goto :goto_5

    :cond_1c
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_1d

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->d()V

    :cond_1d
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v0, :cond_1f

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_1e

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    :cond_1e
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_1f

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1f
    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    :cond_20
    :pswitch_2
    if-le p2, v6, :cond_23

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_21

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->noteStateNotSaved()V

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    :cond_21
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_22

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_22
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_23

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->e()V

    :cond_23
    :pswitch_3
    if-le p2, v9, :cond_6

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->n:Z

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_24

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->noteStateNotSaved()V

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    :cond_24
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_25

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_25
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_26

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->f()V

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    :cond_26
    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    goto/16 :goto_1

    :cond_27
    iget v0, p1, Lcom/google/android/gms/car/support/ab;->a:I

    if-le v0, p2, :cond_6

    iget v0, p1, Lcom/google/android/gms/car/support/ab;->a:I

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_1

    :cond_28
    :goto_6
    :pswitch_4
    if-gtz p2, :cond_6

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->p:Z

    if-eqz v0, :cond_29

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    if-eqz v0, :cond_29

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    :cond_29
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    if-eqz v0, :cond_35

    iput p2, p1, Lcom/google/android/gms/car/support/ab;->c:I

    move p2, v5

    goto/16 :goto_1

    :pswitch_5
    const/4 v0, 0x5

    if-ge p2, v0, :cond_2c

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_2a

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->g()V

    :cond_2a
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_2b

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2b
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->n:Z

    :cond_2c
    :pswitch_6
    if-ge p2, v9, :cond_2e

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_2d

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->h()V

    :cond_2d
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_2e

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2e
    :pswitch_7
    if-ge p2, v6, :cond_2f

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_2f

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->i()V

    :cond_2f
    :pswitch_8
    const/4 v0, 0x2

    if-ge p2, v0, :cond_28

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v0, :cond_30

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-boolean v0, v0, Lcom/google/android/gms/car/l;->o:Z

    if-nez v0, :cond_30

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    if-nez v0, :cond_30

    invoke-direct {p0, p1}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;)V

    :cond_30
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_31

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, v5, v3}, Lcom/google/android/gms/car/support/g;->a(IZ)V

    :cond_31
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_32

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_32
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v0, :cond_34

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->G:Landroid/view/ViewGroup;

    if-eqz v0, :cond_34

    iget v0, p0, Lcom/google/android/gms/car/support/g;->j:I

    if-lez v0, :cond_3a

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->p:Z

    if-nez v0, :cond_3a

    invoke-direct {p0, p1, p3, v3, p4}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    :goto_7
    if-eqz v0, :cond_33

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iput-object v1, p1, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    iput p2, p1, Lcom/google/android/gms/car/support/ab;->c:I

    new-instance v1, Lcom/google/android/gms/car/support/bm;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/support/bm;-><init>(Lcom/google/android/gms/car/support/g;Lcom/google/android/gms/car/support/ab;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_33
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->G:Landroid/view/ViewGroup;

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_34
    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->G:Landroid/view/ViewGroup;

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    goto/16 :goto_6

    :cond_35
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v0, :cond_36

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->j()V

    :cond_36
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_37

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_37
    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->E:Z

    if-nez v0, :cond_38

    new-instance v0, Lcom/google/android/gms/car/support/bd;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDetach()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/bd;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_38
    if-nez p5, :cond_6

    iget v0, p1, Lcom/google/android/gms/car/support/ab;->f:I

    if-ltz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    iget v1, p1, Lcom/google/android/gms/car/support/ab;->f:I

    invoke-virtual {v0, v1, v7}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_39

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    :cond_39
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    iget v1, p1, Lcom/google/android/gms/car/support/ab;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->g:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/car/support/ae;->y()V

    const/4 v0, -0x1

    iput v0, p1, Lcom/google/android/gms/car/support/ab;->f:I

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->g:Ljava/lang/String;

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->m:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->n:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->o:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->p:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->q:Z

    iput v3, p1, Lcom/google/android/gms/car/support/ab;->r:I

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->s:Lcom/google/android/gms/car/support/g;

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    iput v3, p1, Lcom/google/android/gms/car/support/ab;->w:I

    iput v3, p1, Lcom/google/android/gms/car/support/ab;->x:I

    iput-object v7, p1, Lcom/google/android/gms/car/support/ab;->y:Ljava/lang/String;

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->z:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->C:Z

    goto/16 :goto_1

    :cond_3a
    move-object v0, v7

    goto/16 :goto_7

    :cond_3b
    move-object v0, v7

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/car/support/ab;Z)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    :cond_0
    iget v0, p1, Lcom/google/android/gms/car/support/ab;->f:I

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/car/support/ab;->a(ILcom/google/android/gms/car/support/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_0
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/car/support/ab;->a(ILcom/google/android/gms/car/support/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    iget v1, p1, Lcom/google/android/gms/car/support/ab;->f:I

    invoke-virtual {v0, v1, p1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    iput-boolean v3, p1, Lcom/google/android/gms/car/support/ab;->m:Z

    if-eqz p2, :cond_6

    iget v2, p0, Lcom/google/android/gms/car/support/g;->j:I

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    :cond_6
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/support/ae;Lcom/google/android/gms/car/support/bk;Lcom/google/android/gms/car/support/ab;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already attached"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iput-object p2, p0, Lcom/google/android/gms/car/support/g;->l:Lcom/google/android/gms/car/support/bk;

    iput-object p3, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 7

    const/4 v1, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_d

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Active Fragments in "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, ":"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    if-eqz v0, :cond_c

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mFragmentId=#"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->w:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " mContainerId=#"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->x:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " mTag="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->y:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->a:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, " mIndex="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->f:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, " mWho="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->g:Ljava/lang/String;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, " mBackStackNesting="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->r:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mAdded="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->l:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mRemoving="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->m:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mResumed="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->n:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mFromLayout="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->o:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mInLayout="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->p:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mHidden="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->z:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mDetached="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->A:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mMenuVisible="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->D:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mHasMenu="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->println(Z)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mRetainInstance="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->B:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mRetaining="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(Z)V

    const-string v5, " mUserVisibleHint="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/ab;->K:Z

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Z)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->s:Lcom/google/android/gms/car/support/g;

    if-eqz v5, :cond_0

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mFragmentManager="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->s:Lcom/google/android/gms/car/support/g;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_0
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    if-eqz v5, :cond_1

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mActivity="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->t:Lcom/google/android/gms/car/support/ae;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_1
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->v:Lcom/google/android/gms/car/support/ab;

    if-eqz v5, :cond_2

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mParentFragment="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->v:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_2
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->h:Landroid/os/Bundle;

    if-eqz v5, :cond_3

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mArguments="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->h:Landroid/os/Bundle;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_3
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    if-eqz v5, :cond_4

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mSavedFragmentState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_4
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    if-eqz v5, :cond_5

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mSavedViewState="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_5
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    if-eqz v5, :cond_6

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mTarget="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    const-string v5, " mTargetRequestCode="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->k:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    :cond_6
    iget v5, v0, Lcom/google/android/gms/car/support/ab;->F:I

    if-eqz v5, :cond_7

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mNextAnim="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->F:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    :cond_7
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->G:Landroid/view/ViewGroup;

    if-eqz v5, :cond_8

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mContainer="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->G:Landroid/view/ViewGroup;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_8
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v5, :cond_9

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mView="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_9
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->I:Landroid/view/View;

    if-eqz v5, :cond_a

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mInnerView="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_a
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    if-eqz v5, :cond_b

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mAnimatingAway="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->b:Landroid/view/View;

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "mStateAfterAnimating="

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/gms/car/support/ab;->c:I

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(I)V

    :cond_b
    iget-object v5, v0, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    if-eqz v5, :cond_c

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Child "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/car/support/ab;->u:Lcom/google/android/gms/car/support/g;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "  "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5, p2, p3, p4}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    :cond_c
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_e

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Added Fragments:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/ab;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_f

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/bg;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v2}, Ljava/io/PrintWriter;->print(I)V

    const-string v5, ": "

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/bg;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v0, v3, p3}, Lcom/google/android/gms/car/support/bg;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_f
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_10

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Back Stack Indices:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :goto_3
    if-ge v1, v2, :cond_10

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/bg;

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v3, "  #"

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/io/PrintWriter;->print(I)V

    const-string v3, ": "

    invoke-virtual {p3, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_11

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAvailBackStackIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_11
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "FragmentManager misc state:"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->l:Lcom/google/android/gms/car/support/bk;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_12

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mParent="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    :cond_12
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mCurState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/g;->j:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    const-string v0, " mStateSaved="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->o:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    const-string v0, " mDestroyed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->p:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->n:Z

    if-eqz v0, :cond_13

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mNeedMenuInvalidate="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->n:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_14

    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "  mAvailIndices: "

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_14
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Recursive entry to executePendingTransactions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v1, v1, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be called from main thread of process"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    monitor-enter p0

    :try_start_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/g;->q:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lcom/google/android/gms/car/support/g;->q:Z

    invoke-direct {p0}, Lcom/google/android/gms/car/support/g;->k()V

    :cond_2
    return v2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method final a(Ljava/lang/String;II)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    if-nez p1, :cond_3

    if-gez p2, :cond_3

    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/bg;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/support/bg;->a(Z)V

    :cond_2
    move v3, v2

    goto :goto_0

    :cond_3
    const/4 v0, -0x1

    if-nez p1, :cond_4

    if-ltz p2, :cond_b

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_1
    if-ltz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/bg;

    if-eqz p1, :cond_5

    iget-object v4, v0, Lcom/google/android/gms/car/support/bg;->h:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    :cond_5
    if-ltz p2, :cond_6

    iget v0, v0, Lcom/google/android/gms/car/support/bg;->i:I

    if-eq p2, v0, :cond_7

    :cond_6
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_7
    if-ltz v1, :cond_0

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_a

    add-int/lit8 v1, v1, -0x1

    :goto_2
    if-ltz v1, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/bg;

    if-eqz p1, :cond_8

    iget-object v4, v0, Lcom/google/android/gms/car/support/bg;->h:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    :cond_8
    if-ltz p2, :cond_a

    iget v0, v0, Lcom/google/android/gms/car/support/bg;->i:I

    if-ne p2, v0, :cond_a

    :cond_9
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    :cond_a
    move v0, v1

    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_3
    if-le v1, v0, :cond_c

    iget-object v4, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, -0x1

    goto :goto_3

    :cond_c
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v6, v0, -0x1

    move v4, v3

    :goto_4
    if-gt v4, v6, :cond_2

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/bg;

    if-ne v4, v6, :cond_d

    move v1, v2

    :goto_5
    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/bg;->a(Z)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_d
    move v1, v3

    goto :goto_5
.end method

.method final b()Landroid/os/Parcelable;
    .locals 14

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/g;->a()Z

    sget-boolean v0, Lcom/google/android/gms/car/support/g;->b:Z

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/gms/car/support/g;->o:Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gtz v0, :cond_2

    :cond_1
    :goto_0
    return-object v2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v7, v6, [Lcom/google/android/gms/car/support/FragmentState;

    move v5, v4

    move v1, v4

    :goto_1
    if-ge v5, v6, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_15

    iget v1, v0, Lcom/google/android/gms/car/support/ab;->f:I

    if-gez v1, :cond_3

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Failure saving state: active "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has cleared index: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, v0, Lcom/google/android/gms/car/support/ab;->f:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    :cond_3
    new-instance v8, Lcom/google/android/gms/car/support/FragmentState;

    invoke-direct {v8, v0}, Lcom/google/android/gms/car/support/FragmentState;-><init>(Lcom/google/android/gms/car/support/ab;)V

    aput-object v8, v7, v5

    iget v1, v0, Lcom/google/android/gms/car/support/ab;->a:I

    if-lez v1, :cond_d

    iget-object v1, v8, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    if-nez v1, :cond_d

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->r:Landroid/os/Bundle;

    if-nez v1, :cond_4

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/car/support/g;->r:Landroid/os/Bundle;

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->r:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/ab;->a(Landroid/os/Bundle;)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->r:Landroid/os/Bundle;

    invoke-virtual {v1}, Landroid/os/Bundle;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/google/android/gms/car/support/g;->r:Landroid/os/Bundle;

    iput-object v2, p0, Lcom/google/android/gms/car/support/g;->r:Landroid/os/Bundle;

    :goto_2
    iget-object v9, v0, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v9, :cond_5

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;)V

    :cond_5
    iget-object v9, v0, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    if-eqz v9, :cond_7

    if-nez v1, :cond_6

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :cond_6
    const-string v9, "android:view_state"

    iget-object v10, v0, Lcom/google/android/gms/car/support/ab;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    :cond_7
    iget-boolean v9, v0, Lcom/google/android/gms/car/support/ab;->K:Z

    if-nez v9, :cond_9

    if-nez v1, :cond_8

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    :cond_8
    const-string v9, "android:user_visible_hint"

    iget-boolean v10, v0, Lcom/google/android/gms/car/support/ab;->K:Z

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_9
    iput-object v1, v8, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    iget-object v1, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    if-eqz v1, :cond_e

    iget-object v1, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    iget v1, v1, Lcom/google/android/gms/car/support/ab;->f:I

    if-gez v1, :cond_a

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Failure saving state: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " has target not in fragment manager: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    :cond_a
    iget-object v1, v8, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    if-nez v1, :cond_b

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, v8, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    :cond_b
    iget-object v1, v8, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    const-string v9, "android:target_state"

    iget-object v10, v0, Lcom/google/android/gms/car/support/ab;->i:Lcom/google/android/gms/car/support/ab;

    iget v11, v10, Lcom/google/android/gms/car/support/ab;->f:I

    if-gez v11, :cond_c

    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "Fragment "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is not currently in the FragmentManager"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v11}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    :cond_c
    iget v10, v10, Lcom/google/android/gms/car/support/ab;->f:I

    invoke-virtual {v1, v9, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget v1, v0, Lcom/google/android/gms/car/support/ab;->k:I

    if-eqz v1, :cond_e

    iget-object v1, v8, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    const-string v8, "android:target_req_state"

    iget v0, v0, Lcom/google/android/gms/car/support/ab;->k:I

    invoke-virtual {v1, v8, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v0, v3

    :goto_3
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v1, v0

    goto/16 :goto_1

    :cond_d
    iget-object v0, v0, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iput-object v0, v8, Lcom/google/android/gms/car/support/FragmentState;->j:Landroid/os/Bundle;

    :cond_e
    move v0, v3

    goto :goto_3

    :cond_f
    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_11

    new-array v1, v5, [I

    move v3, v4

    :goto_4
    if-ge v3, v5, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    iget v0, v0, Lcom/google/android/gms/car/support/ab;->f:I

    aput v0, v1, v3

    aget v0, v1, v3

    if-gez v0, :cond_10

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "Failure saving state: active "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " has cleared index: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v8, v1, v3

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/RuntimeException;)V

    :cond_10
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    :cond_11
    move-object v1, v2

    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_13

    new-array v2, v5, [Lcom/google/android/gms/car/support/c;

    move v3, v4

    :goto_5
    if-ge v3, v5, :cond_13

    new-instance v4, Lcom/google/android/gms/car/support/c;

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/bg;

    invoke-direct {v4, v0}, Lcom/google/android/gms/car/support/c;-><init>(Lcom/google/android/gms/car/support/bg;)V

    aput-object v4, v2, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    :cond_13
    new-instance v0, Lcom/google/android/gms/car/support/h;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/h;-><init>()V

    iput-object v7, v0, Lcom/google/android/gms/car/support/h;->a:[Lcom/google/android/gms/car/support/FragmentState;

    iput-object v1, v0, Lcom/google/android/gms/car/support/h;->b:[I

    iput-object v2, v0, Lcom/google/android/gms/car/support/h;->c:[Lcom/google/android/gms/car/support/c;

    move-object v2, v0

    goto/16 :goto_0

    :cond_14
    move-object v1, v2

    goto/16 :goto_2

    :cond_15
    move v0, v1

    goto/16 :goto_3
.end method

.method public final b(Lcom/google/android/gms/car/support/ab;II)V
    .locals 2

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->z:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->z:Z

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    invoke-static {}, Lcom/google/android/gms/car/support/ab;->a()V

    :cond_2
    return-void
.end method

.method public final c()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final c(Lcom/google/android/gms/car/support/ab;II)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->z:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p1, Lcom/google/android/gms/car/support/ab;->z:Z

    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IZI)Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    invoke-static {}, Lcom/google/android/gms/car/support/ab;->a()V

    :cond_2
    return-void
.end method

.method public final d()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final d(Lcom/google/android/gms/car/support/ab;II)V
    .locals 6

    const/4 v2, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    if-nez v0, :cond_1

    iput-boolean v2, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    :cond_1
    return-void
.end method

.method public final e()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final e(Lcom/google/android/gms/car/support/ab;II)V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    if-eqz v0, :cond_2

    iput-boolean v5, p1, Lcom/google/android/gms/car/support/ab;->A:Z

    iget-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment already added: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x1

    iput-boolean v0, p1, Lcom/google/android/gms/car/support/ab;->l:Z

    iget v2, p0, Lcom/google/android/gms/car/support/g;->j:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    :cond_2
    return-void
.end method

.method public final f()V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v0, 0x5

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final g()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final h()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final i()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final j()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/g;->p:Z

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/g;->a()Z

    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    iput-object v2, p0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iput-object v2, p0, Lcom/google/android/gms/car/support/g;->l:Lcom/google/android/gms/car/support/bk;

    iput-object v2, p0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    return-void
.end method

.method public final noteStateNotSaved()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/g;->o:Z

    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "FragmentManager{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
