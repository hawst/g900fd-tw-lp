.class public Lcom/google/android/gms/internal/hw;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation runtime Lcom/google/android/gms/internal/hp;
.end annotation


# instance fields
.field final a:Landroid/os/Handler;

.field final b:J

.field public final c:Lcom/google/android/gms/internal/gz;

.field public d:Z

.field public e:Z

.field private f:J

.field private g:Lcom/google/android/gms/internal/kz;

.field private final h:I

.field private final i:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/kz;Lcom/google/android/gms/internal/gz;II)V
    .locals 10

    const-wide/16 v6, 0xc8

    const-wide/16 v8, 0x32

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/internal/hw;-><init>(Lcom/google/android/gms/internal/kz;Lcom/google/android/gms/internal/gz;IIJJ)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/internal/kz;Lcom/google/android/gms/internal/gz;IIJJ)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p5, p0, Lcom/google/android/gms/internal/hw;->b:J

    iput-wide p7, p0, Lcom/google/android/gms/internal/hw;->f:J

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/hw;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/google/android/gms/internal/hw;->c:Lcom/google/android/gms/internal/gz;

    iput-object p1, p0, Lcom/google/android/gms/internal/hw;->g:Lcom/google/android/gms/internal/kz;

    iput-boolean v2, p0, Lcom/google/android/gms/internal/hw;->d:Z

    iput-boolean v2, p0, Lcom/google/android/gms/internal/hw;->e:Z

    iput p4, p0, Lcom/google/android/gms/internal/hw;->h:I

    iput p3, p0, Lcom/google/android/gms/internal/hw;->i:I

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/hw;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/hw;->i:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/hw;)I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/hw;->h:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/hw;)J
    .locals 4

    iget-wide v0, p0, Lcom/google/android/gms/internal/hw;->f:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/internal/hw;->f:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/gms/internal/hw;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/internal/hw;->f:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/gms/internal/hw;)Lcom/google/android/gms/internal/kz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/hw;->g:Lcom/google/android/gms/internal/kz;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/internal/hw;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/internal/hw;->b:J

    return-wide v0
.end method

.method static synthetic g(Lcom/google/android/gms/internal/hw;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/hw;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/hw;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/hw;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/hw;->c:Lcom/google/android/gms/internal/gz;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/hw;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/hw;->g:Lcom/google/android/gms/internal/kz;

    iget-object v1, p0, Lcom/google/android/gms/internal/hw;->c:Lcom/google/android/gms/internal/gz;

    invoke-interface {v0, v1}, Lcom/google/android/gms/internal/kz;->a(Lcom/google/android/gms/internal/gz;)V

    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/google/android/gms/internal/hx;

    iget-object v1, p0, Lcom/google/android/gms/internal/hw;->c:Lcom/google/android/gms/internal/gz;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/hx;-><init>(Lcom/google/android/gms/internal/hw;Landroid/webkit/WebView;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/hx;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
