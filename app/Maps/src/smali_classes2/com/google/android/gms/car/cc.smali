.class public interface abstract Lcom/google/android/gms/car/cc;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/car/CarCall;",
            ">;"
        }
    .end annotation
.end method

.method public abstract a(I)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;C)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;Z)V
.end method

.method public abstract a(Lcom/google/android/gms/car/CarCall;ZLjava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a(Lcom/google/android/gms/car/cf;)Z
.end method

.method public abstract b(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract b()Z
.end method

.method public abstract b(Lcom/google/android/gms/car/cf;)Z
.end method

.method public abstract c()I
.end method

.method public abstract c(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract d()I
.end method

.method public abstract d(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract e(Lcom/google/android/gms/car/CarCall;)V
.end method

.method public abstract f(Lcom/google/android/gms/car/CarCall;)V
.end method
