.class Lcom/google/android/gms/internal/aa;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/internal/gx;

.field final synthetic c:Lcom/google/android/gms/internal/ki;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/google/android/gms/internal/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/internal/y;Landroid/content/Context;Lcom/google/android/gms/internal/gx;Lcom/google/android/gms/internal/ki;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/aa;->e:Lcom/google/android/gms/internal/y;

    iput-object p2, p0, Lcom/google/android/gms/internal/aa;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/internal/aa;->b:Lcom/google/android/gms/internal/gx;

    iput-object p4, p0, Lcom/google/android/gms/internal/aa;->c:Lcom/google/android/gms/internal/ki;

    iput-object p5, p0, Lcom/google/android/gms/internal/aa;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->e:Lcom/google/android/gms/internal/y;

    iget-object v1, p0, Lcom/google/android/gms/internal/aa;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/internal/aa;->b:Lcom/google/android/gms/internal/gx;

    iget-object v3, p0, Lcom/google/android/gms/internal/aa;->c:Lcom/google/android/gms/internal/ki;

    new-instance v4, Lcom/google/android/gms/internal/ac;

    invoke-direct {v4, v1, v2}, Lcom/google/android/gms/internal/ac;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/gx;)V

    new-instance v1, Lcom/google/android/gms/internal/ab;

    invoke-direct {v1, v0, v3, v4}, Lcom/google/android/gms/internal/ab;-><init>(Lcom/google/android/gms/internal/y;Lcom/google/android/gms/internal/ki;Lcom/google/android/gms/internal/w;)V

    invoke-interface {v4, v1}, Lcom/google/android/gms/internal/w;->a(Lcom/google/android/gms/internal/x;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/aa;->d:Ljava/lang/String;

    invoke-interface {v4, v0}, Lcom/google/android/gms/internal/w;->a(Ljava/lang/String;)V

    return-void
.end method
