.class public interface abstract Lcom/google/android/gms/car/ay;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Lcom/google/android/gms/car/CarInfo;
.end method

.method public abstract a(Ljava/lang/String;)Lcom/google/android/gms/car/ee;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/car/CarFacet;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/bb;)V
.end method

.method public abstract a(Lcom/google/android/gms/car/ci;)V
.end method

.method public abstract a(Landroid/content/Intent;)Z
.end method

.method public abstract a(Ljava/lang/String;Z)Z
.end method

.method public abstract b()Lcom/google/android/gms/car/CarUiInfo;
.end method

.method public abstract b(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract b(Landroid/content/Intent;)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end method

.method public abstract b(Lcom/google/android/gms/car/bb;)V
.end method

.method public abstract b(Lcom/google/android/gms/car/ci;)V
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Ljava/lang/String;Z)V
.end method

.method public abstract c()Z
.end method

.method public abstract d()I
.end method

.method public abstract e()Lcom/google/android/gms/car/du;
.end method

.method public abstract f()Lcom/google/android/gms/car/be;
.end method

.method public abstract g()Lcom/google/android/gms/car/dd;
.end method

.method public abstract h()Lcom/google/android/gms/car/cc;
.end method

.method public abstract i()Lcom/google/android/gms/car/cr;
.end method

.method public abstract j()Lcom/google/android/gms/car/cl;
.end method

.method public abstract k()Lcom/google/android/gms/car/dj;
.end method

.method public abstract l()Lcom/google/android/gms/car/cx;
.end method

.method public abstract m()Lcom/google/android/gms/car/bw;
.end method

.method public abstract n()V
.end method

.method public abstract o()V
.end method
