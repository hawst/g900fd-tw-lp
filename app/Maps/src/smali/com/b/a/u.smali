.class public final Lcom/b/a/u;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/b/a/l;

.field final d:Lcom/b/a/w;

.field final e:Ljava/lang/Object;

.field volatile f:Ljava/net/URL;

.field public volatile g:Lcom/b/a/d;

.field private volatile h:Ljava/net/URI;


# direct methods
.method public constructor <init>(Lcom/b/a/v;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iget-object v0, p1, Lcom/b/a/v;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/u;->a:Ljava/lang/String;

    .line 44
    iget-object v0, p1, Lcom/b/a/v;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/u;->b:Ljava/lang/String;

    .line 45
    iget-object v0, p1, Lcom/b/a/v;->d:Lcom/b/a/m;

    new-instance v1, Lcom/b/a/l;

    invoke-direct {v1, v0}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    iput-object v1, p0, Lcom/b/a/u;->c:Lcom/b/a/l;

    .line 46
    iget-object v0, p1, Lcom/b/a/v;->e:Lcom/b/a/w;

    iput-object v0, p0, Lcom/b/a/u;->d:Lcom/b/a/w;

    .line 47
    iget-object v0, p1, Lcom/b/a/v;->f:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/b/a/v;->f:Ljava/lang/Object;

    :goto_0
    iput-object v0, p0, Lcom/b/a/u;->e:Ljava/lang/Object;

    .line 48
    iget-object v0, p1, Lcom/b/a/v;->b:Ljava/net/URL;

    iput-object v0, p0, Lcom/b/a/u;->f:Ljava/net/URL;

    .line 49
    return-void

    :cond_0
    move-object v0, p0

    .line 47
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/b/a/u;->c:Lcom/b/a/l;

    invoke-virtual {v0, p1}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/net/URL;
    .locals 5

    .prologue
    .line 53
    :try_start_0
    iget-object v0, p0, Lcom/b/a/u;->f:Ljava/net/URL;

    .line 54
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lcom/b/a/u;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/b/a/u;->f:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 56
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Malformed URL: "

    iget-object v0, p0, Lcom/b/a/u;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b()Ljava/net/URI;
    .locals 2

    .prologue
    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/b/a/u;->h:Ljava/net/URI;

    .line 63
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    invoke-virtual {p0}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/d;->a(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/u;->h:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final c()Lcom/b/a/d;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/b/a/u;->g:Lcom/b/a/d;

    .line 107
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/b/a/u;->c:Lcom/b/a/l;

    invoke-static {v0}, Lcom/b/a/d;->a(Lcom/b/a/l;)Lcom/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/u;->g:Lcom/b/a/d;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 115
    iget-object v0, p0, Lcom/b/a/u;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/b/a/u;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/b/a/u;->e:Ljava/lang/Object;

    if-eq v0, p0, :cond_0

    iget-object v0, p0, Lcom/b/a/u;->e:Ljava/lang/Object;

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1c

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Request{method="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", url="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", tag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
