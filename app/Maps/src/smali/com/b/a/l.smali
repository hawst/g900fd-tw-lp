.class public final Lcom/b/a/l;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/b/a/m;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iget-object v0, p1, Lcom/b/a/m;->a:Ljava/util/List;

    iget-object v1, p1, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 60
    iget-object v1, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v0, v1

    add-int/lit8 v0, v0, -0x2

    :goto_0
    if-ltz v0, :cond_1

    aget-object v2, v1, v0

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, -0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)Ljava/util/Date;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/b/a/a/a/m;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 126
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_4

    .line 128
    shl-int/lit8 v1, v0, 0x1

    if-ltz v1, :cond_0

    iget-object v4, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_2

    :cond_0
    move-object v1, v2

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    shl-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    if-ltz v1, :cond_1

    iget-object v5, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v5, v5

    if-lt v1, v5, :cond_3

    :cond_1
    move-object v1, v2

    :goto_2
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_2
    iget-object v4, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v1, v4, v1

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v1, v5, v1

    goto :goto_2

    .line 130
    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
