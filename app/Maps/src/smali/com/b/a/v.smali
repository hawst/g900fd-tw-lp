.class public Lcom/b/a/v;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/net/URL;

.field c:Ljava/lang/String;

.field public d:Lcom/b/a/m;

.field e:Lcom/b/a/w;

.field f:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    const-string v0, "GET"

    iput-object v0, p0, Lcom/b/a/v;->c:Ljava/lang/String;

    .line 134
    new-instance v0, Lcom/b/a/m;

    invoke-direct {v0}, Lcom/b/a/m;-><init>()V

    iput-object v0, p0, Lcom/b/a/v;->d:Lcom/b/a/m;

    .line 135
    return-void
.end method

.method public constructor <init>(Lcom/b/a/u;)V
    .locals 3

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    iget-object v0, p1, Lcom/b/a/u;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/v;->a:Ljava/lang/String;

    .line 139
    iget-object v0, p1, Lcom/b/a/u;->f:Ljava/net/URL;

    iput-object v0, p0, Lcom/b/a/v;->b:Ljava/net/URL;

    .line 140
    iget-object v0, p1, Lcom/b/a/u;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/v;->c:Ljava/lang/String;

    .line 141
    iget-object v0, p1, Lcom/b/a/u;->d:Lcom/b/a/w;

    iput-object v0, p0, Lcom/b/a/v;->e:Lcom/b/a/w;

    .line 142
    iget-object v0, p1, Lcom/b/a/u;->e:Ljava/lang/Object;

    iput-object v0, p0, Lcom/b/a/v;->f:Ljava/lang/Object;

    .line 143
    iget-object v0, p1, Lcom/b/a/u;->c:Lcom/b/a/l;

    new-instance v1, Lcom/b/a/m;

    invoke-direct {v1}, Lcom/b/a/m;-><init>()V

    iget-object v2, v1, Lcom/b/a/m;->a:Ljava/util/List;

    iget-object v0, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v1, p0, Lcom/b/a/v;->d:Lcom/b/a/m;

    .line 144
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/b/a/v;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/b/a/v;->d:Lcom/b/a/m;

    invoke-virtual {v0, p1}, Lcom/b/a/m;->a(Ljava/lang/String;)Lcom/b/a/m;

    .line 179
    return-object p0
.end method

.method public final a(Ljava/lang/String;Lcom/b/a/w;)Lcom/b/a/v;
    .locals 4

    .prologue
    .line 213
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 214
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "method == null || method.length() == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :cond_1
    if-eqz p2, :cond_2

    invoke-static {p1}, Lcom/b/a/a/a/q;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "method "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must not have a request body."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_2
    iput-object p1, p0, Lcom/b/a/v;->c:Ljava/lang/String;

    .line 220
    iput-object p2, p0, Lcom/b/a/v;->e:Lcom/b/a/w;

    .line 221
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/b/a/v;->d:Lcom/b/a/m;

    invoke-virtual {v0, p1}, Lcom/b/a/m;->a(Ljava/lang/String;)Lcom/b/a/m;

    invoke-virtual {v0, p1, p2}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    .line 165
    return-object p0
.end method
