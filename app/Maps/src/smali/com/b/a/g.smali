.class public final Lcom/b/a/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final f:Lcom/b/a/g;


# instance fields
.field final a:I

.field final b:J

.field final c:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/b/a/f;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/concurrent/ExecutorService;

.field final e:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 61
    const-string v0, "http.keepAlive"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 62
    const-string v0, "http.keepAliveDuration"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    const-string v1, "http.maxConnections"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 66
    :goto_0
    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 67
    new-instance v2, Lcom/b/a/g;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, v1}, Lcom/b/a/g;-><init>(IJ)V

    sput-object v2, Lcom/b/a/g;->f:Lcom/b/a/g;

    .line 73
    :goto_1
    return-void

    .line 64
    :cond_0
    const-wide/32 v0, 0x493e0

    goto :goto_0

    .line 68
    :cond_1
    if-eqz v3, :cond_2

    .line 69
    new-instance v2, Lcom/b/a/g;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v2, v3, v0, v1}, Lcom/b/a/g;-><init>(IJ)V

    sput-object v2, Lcom/b/a/g;->f:Lcom/b/a/g;

    goto :goto_1

    .line 71
    :cond_2
    new-instance v2, Lcom/b/a/g;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v0, v1}, Lcom/b/a/g;-><init>(IJ)V

    sput-object v2, Lcom/b/a/g;->f:Lcom/b/a/g;

    goto :goto_1
.end method

.method private constructor <init>(IJ)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x3e8

    const/4 v3, 0x1

    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    .line 82
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const-string v0, "OkHttp ConnectionPool"

    invoke-static {v0, v3}, Lcom/b/a/a/i;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/b/a/g;->d:Ljava/util/concurrent/ExecutorService;

    .line 85
    new-instance v0, Lcom/b/a/h;

    invoke-direct {v0, p0}, Lcom/b/a/h;-><init>(Lcom/b/a/g;)V

    iput-object v0, p0, Lcom/b/a/g;->e:Ljava/lang/Runnable;

    .line 119
    iput p1, p0, Lcom/b/a/g;->a:I

    .line 120
    mul-long v0, p2, v10

    mul-long/2addr v0, v10

    iput-wide v0, p0, Lcom/b/a/g;->b:J

    .line 121
    return-void
.end method

.method public static a()Lcom/b/a/g;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/b/a/g;->f:Lcom/b/a/g;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/b/a/a;)Lcom/b/a/f;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 178
    monitor-enter p0

    const/4 v4, 0x0

    .line 179
    :try_start_0
    iget-object v0, p0, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v5

    .line 180
    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 181
    invoke-interface {v5}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/f;

    .line 182
    iget-object v1, v0, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v1, v1, Lcom/b/a/aa;->a:Lcom/b/a/a;

    invoke-virtual {v1, p1}, Lcom/b/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/b/a/f;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    iget-object v1, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-nez v1, :cond_3

    iget-wide v6, v0, Lcom/b/a/f;->h:J

    :goto_1
    sub-long v6, v8, v6

    iget-wide v8, p0, Lcom/b/a/g;->b:J

    cmp-long v1, v6, v8

    if-gez v1, :cond_0

    .line 185
    invoke-interface {v5}, Ljava/util/ListIterator;->remove()V

    .line 188
    iget-object v1, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_4

    move v1, v2

    :goto_2
    if-nez v1, :cond_1

    .line 190
    :try_start_1
    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    move-result-object v1

    iget-object v6, v0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v1, v6}, Lcom/b/a/a/d;->a(Ljava/net/Socket;)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    move-object v1, v0

    .line 199
    :goto_3
    if-eqz v1, :cond_2

    :try_start_2
    iget-object v0, v1, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 206
    :cond_2
    iget-object v0, p0, Lcom/b/a/g;->d:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcom/b/a/g;->e:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 207
    monitor-exit p0

    return-object v1

    .line 182
    :cond_3
    :try_start_3
    iget-object v1, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    invoke-virtual {v1}, Lcom/b/a/a/c/ac;->b()J

    move-result-wide v6

    goto :goto_1

    :cond_4
    move v1, v3

    .line 188
    goto :goto_2

    .line 191
    :catch_0
    move-exception v1

    .line 192
    iget-object v0, v0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/Socket;)V

    .line 194
    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x17

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unable to tagSocket(): "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/d;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_5
    move v0, v3

    .line 199
    goto :goto_4

    :cond_6
    move-object v1, v4

    goto :goto_3
.end method
