.class public final Lcom/b/a/x;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/b/a/u;

.field public final b:Lcom/b/a/t;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Lcom/b/a/k;

.field public final f:Lcom/b/a/l;

.field public final g:Lcom/b/a/z;

.field public h:Lcom/b/a/x;

.field public i:Lcom/b/a/x;

.field final j:Lcom/b/a/x;

.field public volatile k:Lcom/b/a/d;


# direct methods
.method constructor <init>(Lcom/b/a/y;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iget-object v0, p1, Lcom/b/a/y;->a:Lcom/b/a/u;

    iput-object v0, p0, Lcom/b/a/x;->a:Lcom/b/a/u;

    .line 51
    iget-object v0, p1, Lcom/b/a/y;->b:Lcom/b/a/t;

    iput-object v0, p0, Lcom/b/a/x;->b:Lcom/b/a/t;

    .line 52
    iget v0, p1, Lcom/b/a/y;->c:I

    iput v0, p0, Lcom/b/a/x;->c:I

    .line 53
    iget-object v0, p1, Lcom/b/a/y;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/x;->d:Ljava/lang/String;

    .line 54
    iget-object v0, p1, Lcom/b/a/y;->e:Lcom/b/a/k;

    iput-object v0, p0, Lcom/b/a/x;->e:Lcom/b/a/k;

    .line 55
    iget-object v0, p1, Lcom/b/a/y;->f:Lcom/b/a/m;

    new-instance v1, Lcom/b/a/l;

    invoke-direct {v1, v0}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    iput-object v1, p0, Lcom/b/a/x;->f:Lcom/b/a/l;

    .line 56
    iget-object v0, p1, Lcom/b/a/y;->g:Lcom/b/a/z;

    iput-object v0, p0, Lcom/b/a/x;->g:Lcom/b/a/z;

    .line 57
    iget-object v0, p1, Lcom/b/a/y;->h:Lcom/b/a/x;

    iput-object v0, p0, Lcom/b/a/x;->h:Lcom/b/a/x;

    .line 58
    iget-object v0, p1, Lcom/b/a/y;->i:Lcom/b/a/x;

    iput-object v0, p0, Lcom/b/a/x;->i:Lcom/b/a/x;

    .line 59
    iget-object v0, p1, Lcom/b/a/y;->j:Lcom/b/a/x;

    iput-object v0, p0, Lcom/b/a/x;->j:Lcom/b/a/x;

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/b/a/x;->f:Lcom/b/a/l;

    invoke-virtual {v0, p1}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget v0, p0, Lcom/b/a/x;->c:I

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    .line 189
    const-string v0, "WWW-Authenticate"

    .line 195
    :goto_0
    iget-object v1, p0, Lcom/b/a/x;->f:Lcom/b/a/l;

    invoke-static {v1, v0}, Lcom/b/a/a/a/s;->b(Lcom/b/a/l;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    :goto_1
    return-object v0

    .line 190
    :cond_0
    iget v0, p0, Lcom/b/a/x;->c:I

    const/16 v1, 0x197

    if-ne v0, v1, :cond_1

    .line 191
    const-string v0, "Proxy-Authenticate"

    goto :goto_0

    .line 193
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1
.end method

.method public final b()Lcom/b/a/d;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/b/a/x;->k:Lcom/b/a/d;

    .line 204
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/b/a/x;->f:Lcom/b/a/l;

    invoke-static {v0}, Lcom/b/a/d;->a(Lcom/b/a/l;)Lcom/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/x;->k:Lcom/b/a/d;

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 208
    iget-object v0, p0, Lcom/b/a/x;->b:Lcom/b/a/t;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/b/a/x;->c:I

    iget-object v2, p0, Lcom/b/a/x;->d:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/b/a/x;->a:Lcom/b/a/u;

    iget-object v3, v3, Lcom/b/a/u;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x35

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Response{protocol="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", code="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", message="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
