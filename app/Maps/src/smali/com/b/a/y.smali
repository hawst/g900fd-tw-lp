.class public Lcom/b/a/y;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/b/a/u;

.field public b:Lcom/b/a/t;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Lcom/b/a/k;

.field public f:Lcom/b/a/m;

.field public g:Lcom/b/a/z;

.field public h:Lcom/b/a/x;

.field public i:Lcom/b/a/x;

.field public j:Lcom/b/a/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const/4 v0, -0x1

    iput v0, p0, Lcom/b/a/y;->c:I

    .line 232
    new-instance v0, Lcom/b/a/m;

    invoke-direct {v0}, Lcom/b/a/m;-><init>()V

    iput-object v0, p0, Lcom/b/a/y;->f:Lcom/b/a/m;

    .line 233
    return-void
.end method

.method public constructor <init>(Lcom/b/a/x;)V
    .locals 3

    .prologue
    .line 235
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 222
    const/4 v0, -0x1

    iput v0, p0, Lcom/b/a/y;->c:I

    .line 236
    iget-object v0, p1, Lcom/b/a/x;->a:Lcom/b/a/u;

    iput-object v0, p0, Lcom/b/a/y;->a:Lcom/b/a/u;

    .line 237
    iget-object v0, p1, Lcom/b/a/x;->b:Lcom/b/a/t;

    iput-object v0, p0, Lcom/b/a/y;->b:Lcom/b/a/t;

    .line 238
    iget v0, p1, Lcom/b/a/x;->c:I

    iput v0, p0, Lcom/b/a/y;->c:I

    .line 239
    iget-object v0, p1, Lcom/b/a/x;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/y;->d:Ljava/lang/String;

    .line 240
    iget-object v0, p1, Lcom/b/a/x;->e:Lcom/b/a/k;

    iput-object v0, p0, Lcom/b/a/y;->e:Lcom/b/a/k;

    .line 241
    iget-object v0, p1, Lcom/b/a/x;->f:Lcom/b/a/l;

    new-instance v1, Lcom/b/a/m;

    invoke-direct {v1}, Lcom/b/a/m;-><init>()V

    iget-object v2, v1, Lcom/b/a/m;->a:Ljava/util/List;

    iget-object v0, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v1, p0, Lcom/b/a/y;->f:Lcom/b/a/m;

    .line 242
    iget-object v0, p1, Lcom/b/a/x;->g:Lcom/b/a/z;

    iput-object v0, p0, Lcom/b/a/y;->g:Lcom/b/a/z;

    .line 243
    iget-object v0, p1, Lcom/b/a/x;->h:Lcom/b/a/x;

    iput-object v0, p0, Lcom/b/a/y;->h:Lcom/b/a/x;

    .line 244
    iget-object v0, p1, Lcom/b/a/x;->i:Lcom/b/a/x;

    iput-object v0, p0, Lcom/b/a/y;->i:Lcom/b/a/x;

    .line 245
    iget-object v0, p1, Lcom/b/a/x;->j:Lcom/b/a/x;

    iput-object v0, p0, Lcom/b/a/y;->j:Lcom/b/a/x;

    .line 246
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/b/a/x;)V
    .locals 3

    .prologue
    .line 320
    iget-object v0, p1, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v0, :cond_0

    .line 321
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".body != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_0
    iget-object v0, p1, Lcom/b/a/x;->h:Lcom/b/a/x;

    if-eqz v0, :cond_1

    .line 323
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".networkResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 324
    :cond_1
    iget-object v0, p1, Lcom/b/a/x;->i:Lcom/b/a/x;

    if-eqz v0, :cond_2

    .line 325
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".cacheResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_2
    iget-object v0, p1, Lcom/b/a/x;->j:Lcom/b/a/x;

    if-eqz v0, :cond_3

    .line 327
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ".priorResponse != null"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()Lcom/b/a/x;
    .locals 4

    .prologue
    .line 344
    iget-object v0, p0, Lcom/b/a/y;->a:Lcom/b/a/u;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "request == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_0
    iget-object v0, p0, Lcom/b/a/y;->b:Lcom/b/a/t;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "protocol == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 346
    :cond_1
    iget v0, p0, Lcom/b/a/y;->c:I

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lcom/b/a/y;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x15

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "code < 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 347
    :cond_2
    new-instance v0, Lcom/b/a/x;

    invoke-direct {v0, p0}, Lcom/b/a/x;-><init>(Lcom/b/a/y;)V

    return-object v0
.end method

.method public final a(Lcom/b/a/l;)Lcom/b/a/y;
    .locals 3

    .prologue
    .line 298
    new-instance v0, Lcom/b/a/m;

    invoke-direct {v0}, Lcom/b/a/m;-><init>()V

    iget-object v1, v0, Lcom/b/a/m;->a:Ljava/util/List;

    iget-object v2, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v0, p0, Lcom/b/a/y;->f:Lcom/b/a/m;

    .line 299
    return-object p0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/y;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/b/a/y;->f:Lcom/b/a/m;

    invoke-virtual {v0, p1}, Lcom/b/a/m;->a(Ljava/lang/String;)Lcom/b/a/m;

    invoke-virtual {v0, p1, p2}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    .line 279
    return-object p0
.end method
