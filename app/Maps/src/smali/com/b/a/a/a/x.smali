.class public final Lcom/b/a/a/a/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/b/a/a/a/aa;


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lc/k;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lc/k;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lcom/b/a/a/a/o;

.field private final d:Lcom/b/a/a/c/ac;

.field private e:Lcom/b/a/a/c/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 52
    const/4 v0, 0x5

    new-array v0, v0, [Lc/k;

    const-string v1, "connection"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "host"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "keep-alive"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "proxy-connection"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "transfer-encoding"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/b/a/a/i;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/a/x;->a:Ljava/util/List;

    .line 60
    const/16 v0, 0x8

    new-array v0, v0, [Lc/k;

    const-string v1, "connection"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v2

    const-string v1, "host"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v3

    const-string v1, "keep-alive"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v4

    const-string v1, "proxy-connection"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v5

    const-string v1, "te"

    invoke-static {v1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "transfer-encoding"

    invoke-static {v2}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "encoding"

    invoke-static {v2}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "upgrade"

    invoke-static {v2}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/b/a/a/i;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/a/x;->b:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lcom/b/a/a/a/o;Lcom/b/a/a/c/ac;)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/b/a/a/a/x;->c:Lcom/b/a/a/a/o;

    .line 76
    iput-object p2, p0, Lcom/b/a/a/a/x;->d:Lcom/b/a/a/c/ac;

    .line 77
    return-void
.end method

.method private static a(Lcom/b/a/t;Lc/k;)Z
    .locals 1

    .prologue
    .line 232
    sget-object v0, Lcom/b/a/t;->c:Lcom/b/a/t;

    if-ne p0, v0, :cond_0

    .line 233
    sget-object v0, Lcom/b/a/a/a/x;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    .line 235
    :goto_0
    return v0

    .line 234
    :cond_0
    sget-object v0, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne p0, v0, :cond_1

    .line 235
    sget-object v0, Lcom/b/a/a/a/x;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 237
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/b/a/a/a/b;)Lc/aa;
    .locals 2

    .prologue
    .line 216
    new-instance v0, Lcom/b/a/a/a/y;

    iget-object v1, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    invoke-direct {v0, v1, p1}, Lcom/b/a/a/a/y;-><init>(Lcom/b/a/a/c/ao;Lcom/b/a/a/a/b;)V

    return-object v0
.end method

.method public final a(Lcom/b/a/u;)Lc/z;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0, p1}, Lcom/b/a/a/a/x;->b(Lcom/b/a/u;)V

    .line 82
    iget-object v0, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Lcom/b/a/a/c/ao;->d()Lc/z;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Lcom/b/a/a/c/ao;->d()Lc/z;

    move-result-object v0

    invoke-interface {v0}, Lc/z;->close()V

    .line 104
    return-void
.end method

.method public final a(Lcom/b/a/a/a/o;)V
    .locals 3

    .prologue
    .line 223
    iget-object v0, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    sget-object v1, Lcom/b/a/a/c/a;->l:Lcom/b/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v0, v0, Lcom/b/a/a/c/ao;->c:I

    iget-object v2, v2, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v2, v0, v1}, Lcom/b/a/a/c/d;->a(ILcom/b/a/a/c/a;)V

    .line 224
    :cond_0
    return-void
.end method

.method public final a(Lcom/b/a/a/a/v;)V
    .locals 1

    .prologue
    .line 99
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b()Lcom/b/a/y;
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 107
    iget-object v0, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Lcom/b/a/a/c/ao;->c()Ljava/util/List;

    move-result-object v6

    iget-object v0, p0, Lcom/b/a/a/a/x;->d:Lcom/b/a/a/c/ac;

    iget-object v7, v0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    const/4 v2, 0x0

    const-string v1, "HTTP/1.1"

    new-instance v8, Lcom/b/a/m;

    invoke-direct {v8}, Lcom/b/a/m;-><init>()V

    sget-object v0, Lcom/b/a/a/a/s;->d:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/b/a/t;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v0}, Lcom/b/a/m;->a(Ljava/lang/String;)Lcom/b/a/m;

    invoke-virtual {v8, v0, v4}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    move v5, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_5

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/e;

    iget-object v9, v0, Lcom/b/a/a/c/e;->h:Lc/k;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/e;

    iget-object v0, v0, Lcom/b/a/a/c/e;->i:Lc/k;

    invoke-virtual {v0}, Lc/k;->a()Ljava/lang/String;

    move-result-object v10

    move-object v0, v1

    move v1, v3

    :goto_1
    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    invoke-virtual {v10, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    const/4 v11, -0x1

    if-ne v4, v11, :cond_0

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v4

    :cond_0
    invoke-virtual {v10, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    sget-object v11, Lcom/b/a/a/c/e;->a:Lc/k;

    invoke-virtual {v9, v11}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v12, v2

    move-object v2, v1

    move v1, v12

    goto :goto_1

    :cond_1
    sget-object v11, Lcom/b/a/a/c/e;->g:Lc/k;

    invoke-virtual {v9, v11}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move-object v0, v1

    move-object v1, v2

    goto :goto_2

    :cond_2
    invoke-static {v7, v9}, Lcom/b/a/a/a/x;->a(Lcom/b/a/t;Lc/k;)Z

    move-result v11

    if-nez v11, :cond_3

    invoke-virtual {v9}, Lc/k;->a()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11, v1}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    :cond_3
    move-object v1, v2

    goto :goto_2

    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v1, v0

    goto :goto_0

    :cond_5
    if-nez v2, :cond_6

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Expected \':status\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-nez v1, :cond_7

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Expected \':version\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/a/z;->a(Ljava/lang/String;)Lcom/b/a/a/a/z;

    move-result-object v0

    new-instance v1, Lcom/b/a/y;

    invoke-direct {v1}, Lcom/b/a/y;-><init>()V

    iput-object v7, v1, Lcom/b/a/y;->b:Lcom/b/a/t;

    iget v2, v0, Lcom/b/a/a/a/z;->b:I

    iput v2, v1, Lcom/b/a/y;->c:I

    iget-object v0, v0, Lcom/b/a/a/a/z;->c:Ljava/lang/String;

    iput-object v0, v1, Lcom/b/a/y;->d:Ljava/lang/String;

    new-instance v0, Lcom/b/a/l;

    invoke-direct {v0, v8}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    invoke-virtual {v1, v0}, Lcom/b/a/y;->a(Lcom/b/a/l;)Lcom/b/a/y;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/b/a/u;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 86
    iget-object v0, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    if-eqz v0, :cond_0

    .line 96
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/x;->c:Lcom/b/a/a/a/o;

    iget-wide v2, v0, Lcom/b/a/a/a/o;->h:J

    const-wide/16 v6, -0x1

    cmp-long v2, v2, v6

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/b/a/a/a/o;->h:J

    .line 89
    iget-object v0, p0, Lcom/b/a/a/a/x;->c:Lcom/b/a/a/a/o;

    invoke-virtual {v0}, Lcom/b/a/a/a/o;->a()Z

    move-result v6

    .line 90
    iget-object v0, p0, Lcom/b/a/a/a/x;->c:Lcom/b/a/a/a/o;

    iget-object v0, v0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v0, v0, Lcom/b/a/f;->g:Lcom/b/a/t;

    sget-object v2, Lcom/b/a/t;->a:Lcom/b/a/t;

    if-ne v0, v2, :cond_5

    const-string v0, "HTTP/1.0"

    .line 92
    :goto_1
    iget-object v7, p0, Lcom/b/a/a/a/x;->d:Lcom/b/a/a/c/ac;

    iget-object v2, p0, Lcom/b/a/a/a/x;->d:Lcom/b/a/a/c/ac;

    iget-object v8, v2, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    iget-object v9, p1, Lcom/b/a/u;->c:Lcom/b/a/l;

    new-instance v10, Ljava/util/ArrayList;

    iget-object v2, v9, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v2, v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, 0xa

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Lcom/b/a/a/c/e;

    sget-object v3, Lcom/b/a/a/c/e;->b:Lc/k;

    iget-object v5, p1, Lcom/b/a/u;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v5}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/b/a/a/c/e;

    sget-object v3, Lcom/b/a/a/c/e;->c:Lc/k;

    invoke-virtual {p1}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v5

    invoke-static {v5}, Lcom/b/a/a/a/u;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v2

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/b/a/t;->c:Lcom/b/a/t;

    if-ne v3, v8, :cond_6

    new-instance v3, Lcom/b/a/a/c/e;

    sget-object v5, Lcom/b/a/a/c/e;->g:Lc/k;

    invoke-direct {v3, v5, v0}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/b/a/a/c/e;

    sget-object v3, Lcom/b/a/a/c/e;->f:Lc/k;

    invoke-direct {v0, v3, v2}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    new-instance v0, Lcom/b/a/a/c/e;

    sget-object v2, Lcom/b/a/a/c/e;->d:Lc/k;

    invoke-virtual {p1}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v11, Ljava/util/LinkedHashSet;

    invoke-direct {v11}, Ljava/util/LinkedHashSet;-><init>()V

    move v5, v4

    :goto_3
    iget-object v0, v9, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    if-ge v5, v0, :cond_c

    shl-int/lit8 v0, v5, 0x1

    if-ltz v0, :cond_2

    iget-object v2, v9, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_8

    :cond_2
    move-object v0, v1

    :goto_4
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v12

    shl-int/lit8 v0, v5, 0x1

    add-int/lit8 v0, v0, 0x1

    if-ltz v0, :cond_3

    iget-object v2, v9, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_9

    :cond_3
    move-object v2, v1

    :goto_5
    invoke-static {v8, v12}, Lcom/b/a/a/a/x;->a(Lcom/b/a/t;Lc/k;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/e;->b:Lc/k;

    invoke-virtual {v12, v0}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/e;->c:Lc/k;

    invoke-virtual {v12, v0}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/e;->d:Lc/k;

    invoke-virtual {v12, v0}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/e;->e:Lc/k;

    invoke-virtual {v12, v0}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/e;->f:Lc/k;

    invoke-virtual {v12, v0}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/e;->g:Lc/k;

    invoke-virtual {v12, v0}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v11, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lcom/b/a/a/c/e;

    invoke-direct {v0, v12, v2}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_6
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    .line 90
    :cond_5
    const-string v0, "HTTP/1.1"

    goto/16 :goto_1

    .line 92
    :cond_6
    sget-object v0, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v0, v8, :cond_7

    new-instance v0, Lcom/b/a/a/c/e;

    sget-object v3, Lcom/b/a/a/c/e;->e:Lc/k;

    invoke-direct {v0, v3, v2}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_7
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_8
    iget-object v2, v9, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v0, v2, v0

    goto :goto_4

    :cond_9
    iget-object v2, v9, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v0, v2, v0

    move-object v2, v0

    goto :goto_5

    :cond_a
    move v3, v4

    :goto_7
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/e;

    iget-object v0, v0, Lcom/b/a/a/c/e;->h:Lc/k;

    invoke-virtual {v0, v12}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/e;

    iget-object v0, v0, Lcom/b/a/a/c/e;->i:Lc/k;

    invoke-virtual {v0}, Lc/k;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/b/a/a/c/e;

    invoke-direct {v2, v12, v0}, Lcom/b/a/a/c/e;-><init>(Lc/k;Ljava/lang/String;)V

    invoke-interface {v10, v3, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_c
    const/4 v0, 0x1

    invoke-virtual {v7, v4, v10, v6, v0}, Lcom/b/a/a/c/ac;->a(ILjava/util/List;ZZ)Lcom/b/a/a/c/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    .line 95
    iget-object v0, p0, Lcom/b/a/a/a/x;->e:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    iget-object v1, p0, Lcom/b/a/a/a/x;->c:Lcom/b/a/a/a/o;

    iget-object v1, v1, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget v1, v1, Lcom/b/a/p;->q:I

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lc/ab;->a(JLjava/util/concurrent/TimeUnit;)Lc/ab;

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 220
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x1

    return v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 213
    return-void
.end method
