.class public final Lcom/b/a/a/b/c;
.super Lcom/b/a/a/b/a;
.source "PG"


# instance fields
.field private final a:Lcom/b/a/a/b/b;


# direct methods
.method private constructor <init>(Lcom/b/a/a/b/b;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/b/a/a/b/a;-><init>(Ljava/net/HttpURLConnection;)V

    .line 34
    iput-object p1, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    .line 35
    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Lcom/b/a/p;)V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/b/a/a/b/b;

    invoke-direct {v0, p1, p2}, Lcom/b/a/a/b/b;-><init>(Ljava/net/URL;Lcom/b/a/p;)V

    invoke-direct {p0, v0}, Lcom/b/a/a/b/c;-><init>(Lcom/b/a/a/b/b;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected final a()Lcom/b/a/k;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Connection has not yet been established"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v1, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget-object v0, v0, Lcom/b/a/x;->e:Lcom/b/a/k;

    :goto_1
    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->d:Lcom/b/a/k;

    goto :goto_1
.end method

.method public final bridge synthetic addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lcom/b/a/a/b/a;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final bridge synthetic connect()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->connect()V

    return-void
.end method

.method public final bridge synthetic disconnect()V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->disconnect()V

    return-void
.end method

.method public final bridge synthetic getAllowUserInteraction()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getAllowUserInteraction()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getCipherSuite()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getConnectTimeout()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getConnectTimeout()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getContent()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getContent([Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->getContent([Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getContentEncoding()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getContentLength()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getContentLength()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getContentType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getDate()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getDate()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getDefaultUseCaches()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getDefaultUseCaches()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getDoInput()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getDoInput()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getDoOutput()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getDoOutput()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getErrorStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getExpiration()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getExpiration()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getHeaderField(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getHeaderFieldDate(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/b/a/a/b/a;->getHeaderFieldDate(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getHeaderFieldInt(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lcom/b/a/a/b/a;->getHeaderFieldInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getHeaderFields()Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iget-object v0, v0, Lcom/b/a/p;->j:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public final bridge synthetic getIfModifiedSince()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getIfModifiedSince()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getInputStream()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getInstanceFollowRedirects()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getInstanceFollowRedirects()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic getLastModified()J
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getLastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public final bridge synthetic getLocalCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getLocalCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getLocalPrincipal()Ljava/security/Principal;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getLocalPrincipal()Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getOutputStream()Ljava/io/OutputStream;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getPeerPrincipal()Ljava/security/Principal;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getPeerPrincipal()Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getPermission()Ljava/security/Permission;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getPermission()Ljava/security/Permission;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getReadTimeout()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getReadTimeout()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getRequestMethod()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getRequestMethod()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getRequestProperties()Ljava/util/Map;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getRequestProperties()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getResponseCode()I
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public final bridge synthetic getResponseMessage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iget-object v0, v0, Lcom/b/a/p;->i:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public final bridge synthetic getServerCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getServerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getURL()Ljava/net/URL;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getURL()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic getUseCaches()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->getUseCaches()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic setAllowUserInteraction(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setAllowUserInteraction(Z)V

    return-void
.end method

.method public final bridge synthetic setChunkedStreamingMode(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setChunkedStreamingMode(I)V

    return-void
.end method

.method public final bridge synthetic setConnectTimeout(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setConnectTimeout(I)V

    return-void
.end method

.method public final bridge synthetic setDefaultUseCaches(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setDefaultUseCaches(Z)V

    return-void
.end method

.method public final bridge synthetic setDoInput(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setDoInput(Z)V

    return-void
.end method

.method public final bridge synthetic setDoOutput(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setDoOutput(Z)V

    return-void
.end method

.method public final bridge synthetic setFixedLengthStreamingMode(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setFixedLengthStreamingMode(I)V

    return-void
.end method

.method public final setFixedLengthStreamingMode(J)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    invoke-virtual {v0, p1, p2}, Lcom/b/a/a/b/b;->setFixedLengthStreamingMode(J)V

    .line 72
    return-void
.end method

.method public final setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iput-object p1, v0, Lcom/b/a/p;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 52
    return-void
.end method

.method public final bridge synthetic setIfModifiedSince(J)V
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lcom/b/a/a/b/a;->setIfModifiedSince(J)V

    return-void
.end method

.method public final bridge synthetic setInstanceFollowRedirects(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setInstanceFollowRedirects(Z)V

    return-void
.end method

.method public final bridge synthetic setReadTimeout(I)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setReadTimeout(I)V

    return-void
.end method

.method public final bridge synthetic setRequestMethod(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setRequestMethod(Ljava/lang/String;)V

    return-void
.end method

.method public final bridge synthetic setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1, p2}, Lcom/b/a/a/b/a;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/b/a/a/b/c;->a:Lcom/b/a/a/b/b;

    iget-object v0, v0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iput-object p1, v0, Lcom/b/a/p;->i:Ljavax/net/ssl/SSLSocketFactory;

    .line 60
    return-void
.end method

.method public final bridge synthetic setUseCaches(Z)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/b/a/a/b/a;->setUseCaches(Z)V

    return-void
.end method

.method public final bridge synthetic toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic usingProxy()Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/b/a/a/b/a;->usingProxy()Z

    move-result v0

    return v0
.end method
