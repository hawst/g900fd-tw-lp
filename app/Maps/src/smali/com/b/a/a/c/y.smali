.class public final Lcom/b/a/a/c/y;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field b:I

.field c:I

.field public final d:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/16 v0, 0xa

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/b/a/a/c/y;->d:[I

    return-void
.end method


# virtual methods
.method final a(I)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 124
    .line 125
    shl-int v0, v2, p1

    iget v3, p0, Lcom/b/a/a/c/y;->c:I

    and-int/2addr v0, v3

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    const/4 v0, 0x2

    .line 126
    :goto_1
    shl-int v3, v2, p1

    iget v4, p0, Lcom/b/a/a/c/y;->b:I

    and-int/2addr v3, v4

    if-eqz v3, :cond_0

    move v1, v2

    :cond_0
    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x1

    .line 127
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 125
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method final a(III)Lcom/b/a/a/c/y;
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/b/a/a/c/y;->d:[I

    const/16 v0, 0xa

    if-lt p1, v0, :cond_0

    .line 108
    :goto_0
    return-object p0

    .line 94
    :cond_0
    const/4 v0, 0x1

    shl-int/2addr v0, p1

    .line 95
    iget v1, p0, Lcom/b/a/a/c/y;->a:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/b/a/a/c/y;->a:I

    .line 96
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    .line 97
    iget v1, p0, Lcom/b/a/a/c/y;->b:I

    or-int/2addr v1, v0

    iput v1, p0, Lcom/b/a/a/c/y;->b:I

    .line 101
    :goto_1
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_2

    .line 102
    iget v1, p0, Lcom/b/a/a/c/y;->c:I

    or-int/2addr v0, v1

    iput v0, p0, Lcom/b/a/a/c/y;->c:I

    .line 107
    :goto_2
    iget-object v0, p0, Lcom/b/a/a/c/y;->d:[I

    aput p3, v0, p1

    goto :goto_0

    .line 99
    :cond_1
    iget v1, p0, Lcom/b/a/a/c/y;->b:I

    xor-int/lit8 v2, v0, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lcom/b/a/a/c/y;->b:I

    goto :goto_1

    .line 104
    :cond_2
    iget v1, p0, Lcom/b/a/a/c/y;->c:I

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/b/a/a/c/y;->c:I

    goto :goto_2
.end method
