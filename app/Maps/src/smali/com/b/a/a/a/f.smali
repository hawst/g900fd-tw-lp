.class public final Lcom/b/a/a/a/f;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final g:[B

.field static final h:[B


# instance fields
.field final a:Lcom/b/a/g;

.field final b:Lcom/b/a/f;

.field public final c:Lc/j;

.field public final d:Lc/i;

.field e:I

.field f:I

.field private final i:Ljava/net/Socket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 318
    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/b/a/a/a/f;->g:[B

    .line 321
    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/b/a/a/a/f;->h:[B

    return-void

    .line 318
    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data

    .line 321
    :array_1
    .array-data 1
        0x30t
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Lcom/b/a/g;Lcom/b/a/f;Ljava/net/Socket;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput v0, p0, Lcom/b/a/a/a/f;->e:I

    .line 80
    iput v0, p0, Lcom/b/a/a/a/f;->f:I

    .line 84
    iput-object p1, p0, Lcom/b/a/a/a/f;->a:Lcom/b/a/g;

    .line 85
    iput-object p2, p0, Lcom/b/a/a/a/f;->b:Lcom/b/a/f;

    .line 86
    iput-object p3, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    .line 87
    invoke-static {p3}, Lc/p;->b(Ljava/net/Socket;)Lc/aa;

    move-result-object v0

    invoke-static {v0}, Lc/p;->a(Lc/aa;)Lc/j;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/f;->c:Lc/j;

    .line 88
    invoke-static {p3}, Lc/p;->a(Ljava/net/Socket;)Lc/z;

    move-result-object v0

    invoke-static {v0}, Lc/p;->a(Lc/z;)Lc/i;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/f;->d:Lc/i;

    .line 89
    return-void
.end method


# virtual methods
.method public final a(Lcom/b/a/a/a/b;J)Lc/aa;
    .locals 4

    .prologue
    .line 254
    iget v0, p0, Lcom/b/a/a/a/f;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lcom/b/a/a/a/f;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 255
    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/b/a/a/a/f;->e:I

    .line 256
    new-instance v0, Lcom/b/a/a/a/k;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/b/a/a/a/k;-><init>(Lcom/b/a/a/a/f;Lcom/b/a/a/a/b;J)V

    return-object v0
.end method

.method public final a(II)V
    .locals 4

    .prologue
    .line 92
    if-eqz p1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v0}, Lc/j;->a()Lc/ab;

    move-result-object v0

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lc/ab;->a(JLjava/util/concurrent/TimeUnit;)Lc/ab;

    .line 95
    :cond_0
    if-eqz p2, :cond_1

    .line 96
    iget-object v0, p0, Lcom/b/a/a/a/f;->d:Lc/i;

    invoke-interface {v0}, Lc/i;->a()Lc/ab;

    move-result-object v0

    int-to-long v2, p2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lc/ab;->a(JLjava/util/concurrent/TimeUnit;)Lc/ab;

    .line 98
    :cond_1
    return-void
.end method

.method public final a(Lcom/b/a/l;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 168
    iget v0, p0, Lcom/b/a/a/a/f;->e:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lcom/b/a/a/a/f;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/f;->d:Lc/i;

    invoke-interface {v0, p2}, Lc/i;->b(Ljava/lang/String;)Lc/i;

    move-result-object v0

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lc/i;->b(Ljava/lang/String;)Lc/i;

    .line 170
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_5

    .line 171
    iget-object v3, p0, Lcom/b/a/a/a/f;->d:Lc/i;

    shl-int/lit8 v1, v0, 0x1

    if-ltz v1, :cond_1

    iget-object v4, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_3

    :cond_1
    move-object v1, v2

    :goto_1
    invoke-interface {v3, v1}, Lc/i;->b(Ljava/lang/String;)Lc/i;

    move-result-object v1

    const-string v3, ": "

    invoke-interface {v1, v3}, Lc/i;->b(Ljava/lang/String;)Lc/i;

    move-result-object v3

    shl-int/lit8 v1, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    if-ltz v1, :cond_2

    iget-object v4, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v4, v4

    if-lt v1, v4, :cond_4

    :cond_2
    move-object v1, v2

    :goto_2
    invoke-interface {v3, v1}, Lc/i;->b(Ljava/lang/String;)Lc/i;

    move-result-object v1

    const-string v3, "\r\n"

    invoke-interface {v1, v3}, Lc/i;->b(Ljava/lang/String;)Lc/i;

    .line 170
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_3
    iget-object v4, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v1, v4, v1

    goto :goto_1

    :cond_4
    iget-object v4, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v1, v4, v1

    goto :goto_2

    .line 176
    :cond_5
    iget-object v0, p0, Lcom/b/a/a/a/f;->d:Lc/i;

    const-string v1, "\r\n"

    invoke-interface {v0, v1}, Lc/i;->b(Ljava/lang/String;)Lc/i;

    .line 177
    const/4 v0, 0x1

    iput v0, p0, Lcom/b/a/a/a/f;->e:I

    .line 178
    return-void
.end method

.method public final a(Lcom/b/a/m;)V
    .locals 2

    .prologue
    .line 209
    :goto_0
    iget-object v0, p0, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v0}, Lc/j;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 210
    sget-object v1, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    invoke-virtual {v1, p1, v0}, Lcom/b/a/a/a;->a(Lcom/b/a/m;Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 149
    :try_start_0
    iget-object v2, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    .line 151
    :try_start_1
    iget-object v2, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 152
    iget-object v2, p0, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v2}, Lc/j;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    :try_start_2
    iget-object v2, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 162
    :goto_0
    return v0

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v4, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v4, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v2
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 160
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 162
    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public final a(Lc/aa;I)Z
    .locals 3

    .prologue
    .line 222
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getSoTimeout()I

    move-result v1

    .line 223
    iget-object v0, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    :try_start_1
    invoke-static {p1, p2}, Lcom/b/a/a/i;->a(Lc/aa;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 227
    :try_start_2
    iget-object v2, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v2, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    .line 230
    :goto_0
    return v0

    .line 227
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/b/a/a/a/f;->i:Ljava/net/Socket;

    invoke-virtual {v2, v1}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 230
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/b/a/y;
    .locals 5

    .prologue
    .line 182
    iget v0, p0, Lcom/b/a/a/a/f;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/b/a/a/a/f;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lcom/b/a/a/a/f;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v0}, Lc/j;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/a/z;->a(Ljava/lang/String;)Lcom/b/a/a/a/z;

    move-result-object v0

    .line 189
    new-instance v1, Lcom/b/a/y;

    invoke-direct {v1}, Lcom/b/a/y;-><init>()V

    iget-object v2, v0, Lcom/b/a/a/a/z;->a:Lcom/b/a/t;

    iput-object v2, v1, Lcom/b/a/y;->b:Lcom/b/a/t;

    iget v2, v0, Lcom/b/a/a/a/z;->b:I

    iput v2, v1, Lcom/b/a/y;->c:I

    iget-object v2, v0, Lcom/b/a/a/a/z;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/b/a/y;->d:Ljava/lang/String;

    .line 194
    new-instance v2, Lcom/b/a/m;

    invoke-direct {v2}, Lcom/b/a/m;-><init>()V

    .line 195
    invoke-virtual {p0, v2}, Lcom/b/a/a/a/f;->a(Lcom/b/a/m;)V

    .line 196
    sget-object v3, Lcom/b/a/a/a/s;->d:Ljava/lang/String;

    iget-object v4, v0, Lcom/b/a/a/a/z;->a:Lcom/b/a/t;

    invoke-virtual {v4}, Lcom/b/a/t;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    .line 197
    new-instance v3, Lcom/b/a/l;

    invoke-direct {v3, v2}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    invoke-virtual {v1, v3}, Lcom/b/a/y;->a(Lcom/b/a/l;)Lcom/b/a/y;

    .line 199
    iget v0, v0, Lcom/b/a/a/a/z;->b:I

    const/16 v2, 0x64

    if-eq v0, v2, :cond_0

    .line 200
    const/4 v0, 0x4

    iput v0, p0, Lcom/b/a/a/a/f;->e:I

    .line 201
    return-object v1
.end method
