.class public final Lcom/b/a/a/a/o;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/b/a/z;


# instance fields
.field public final b:Lcom/b/a/p;

.field public c:Lcom/b/a/f;

.field public d:Lcom/b/a/a/a/w;

.field public e:Lcom/b/a/aa;

.field public final f:Lcom/b/a/x;

.field public g:Lcom/b/a/a/a/aa;

.field public h:J

.field public i:Z

.field public final j:Z

.field public final k:Lcom/b/a/u;

.field public l:Lcom/b/a/u;

.field public m:Lcom/b/a/x;

.field public n:Lcom/b/a/x;

.field public o:Lcom/b/a/x;

.field public p:Lc/z;

.field public q:Lc/i;

.field public r:Lc/aa;

.field public s:Lc/j;

.field public t:Lcom/b/a/a/a/b;

.field public u:Lcom/b/a/a/a/c;

.field private v:Ljava/io/InputStream;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/b/a/a/a/p;

    invoke-direct {v0}, Lcom/b/a/a/a/p;-><init>()V

    sput-object v0, Lcom/b/a/a/a/o;->a:Lcom/b/a/z;

    return-void
.end method

.method public constructor <init>(Lcom/b/a/p;Lcom/b/a/u;ZLcom/b/a/f;Lcom/b/a/a/a/w;Lcom/b/a/a/a/v;Lcom/b/a/x;)V
    .locals 2

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/b/a/a/a/o;->h:J

    .line 188
    iput-object p1, p0, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    .line 189
    iput-object p2, p0, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    .line 190
    iput-boolean p3, p0, Lcom/b/a/a/a/o;->j:Z

    .line 191
    iput-object p4, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    .line 192
    iput-object p5, p0, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    .line 193
    iput-object p6, p0, Lcom/b/a/a/a/o;->p:Lc/z;

    .line 194
    iput-object p7, p0, Lcom/b/a/a/a/o;->f:Lcom/b/a/x;

    .line 196
    if-eqz p4, :cond_0

    .line 197
    sget-object v0, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    invoke-virtual {v0, p4, p0}, Lcom/b/a/a/a;->b(Lcom/b/a/f;Lcom/b/a/a/a/o;)V

    .line 198
    iget-object v0, p4, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iput-object v0, p0, Lcom/b/a/a/a/o;->e:Lcom/b/a/aa;

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/b/a/a/a/o;->e:Lcom/b/a/aa;

    goto :goto_0
.end method

.method public static a(Lcom/b/a/l;Lcom/b/a/l;)Lcom/b/a/l;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 729
    new-instance v5, Lcom/b/a/m;

    invoke-direct {v5}, Lcom/b/a/m;-><init>()V

    move v0, v1

    .line 731
    :goto_0
    iget-object v2, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v2, v2

    div-int/lit8 v2, v2, 0x2

    if-ge v0, v2, :cond_7

    .line 732
    shl-int/lit8 v2, v0, 0x1

    if-ltz v2, :cond_0

    iget-object v4, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v4, v4

    if-lt v2, v4, :cond_5

    :cond_0
    move-object v4, v3

    .line 733
    :goto_1
    shl-int/lit8 v2, v0, 0x1

    add-int/lit8 v2, v2, 0x1

    if-ltz v2, :cond_1

    iget-object v6, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v6, v6

    if-lt v2, v6, :cond_6

    :cond_1
    move-object v2, v3

    .line 734
    :goto_2
    const-string v6, "Warning"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "1"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 735
    :cond_2
    invoke-static {v4}, Lcom/b/a/a/a/s;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {p1, v4}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_4

    .line 738
    :cond_3
    invoke-virtual {v5, v4, v2}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    .line 731
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 732
    :cond_5
    iget-object v4, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v2, v4, v2

    move-object v4, v2

    goto :goto_1

    .line 733
    :cond_6
    iget-object v6, p0, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v2, v6, v2

    goto :goto_2

    .line 742
    :cond_7
    :goto_3
    iget-object v0, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    if-ge v1, v0, :cond_d

    .line 743
    shl-int/lit8 v0, v1, 0x1

    if-ltz v0, :cond_8

    iget-object v2, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v2, v2

    if-lt v0, v2, :cond_b

    :cond_8
    move-object v0, v3

    .line 744
    :goto_4
    invoke-static {v0}, Lcom/b/a/a/a/s;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 745
    shl-int/lit8 v2, v1, 0x1

    add-int/lit8 v2, v2, 0x1

    if-ltz v2, :cond_9

    iget-object v4, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v4, v4

    if-lt v2, v4, :cond_c

    :cond_9
    move-object v2, v3

    :goto_5
    invoke-virtual {v5, v0, v2}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    .line 742
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 743
    :cond_b
    iget-object v2, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v0, v2, v0

    goto :goto_4

    .line 745
    :cond_c
    iget-object v4, p1, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v2, v4, v2

    goto :goto_5

    .line 749
    :cond_d
    new-instance v0, Lcom/b/a/l;

    invoke-direct {v0, v5}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    return-object v0
.end method

.method public static a(Lcom/b/a/x;)Lcom/b/a/x;
    .locals 2

    .prologue
    .line 280
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/b/a/y;

    invoke-direct {v0, p0}, Lcom/b/a/y;-><init>(Lcom/b/a/x;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/b/a/y;->g:Lcom/b/a/z;

    invoke-virtual {v0}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static a(Ljava/net/URL;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 594
    invoke-static {p0}, Lcom/b/a/a/i;->a(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/io/IOException;)Lcom/b/a/a/a/o;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 384
    iget-object v6, p0, Lcom/b/a/a/a/o;->p:Lc/z;

    iget-object v0, p0, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    iget-object v0, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    sget-object v4, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    invoke-virtual {v4, v0}, Lcom/b/a/a/a;->b(Lcom/b/a/f;)I

    move-result v4

    if-gtz v4, :cond_2

    iget-object v0, v0, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v4, v0, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    invoke-virtual {v4}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v4

    sget-object v5, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v4, v5, :cond_0

    iget-object v4, v3, Lcom/b/a/a/a/w;->d:Ljava/net/ProxySelector;

    if-eqz v4, :cond_0

    iget-object v4, v3, Lcom/b/a/a/a/w;->d:Ljava/net/ProxySelector;

    iget-object v5, v3, Lcom/b/a/a/a/w;->b:Ljava/net/URI;

    iget-object v7, v0, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    invoke-virtual {v7}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v7

    invoke-virtual {v4, v5, v7, p1}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    :cond_0
    iget-object v4, v3, Lcom/b/a/a/a/w;->e:Lcom/b/a/a/h;

    invoke-virtual {v4, v0}, Lcom/b/a/a/h;->a(Lcom/b/a/aa;)V

    instance-of v0, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-nez v0, :cond_2

    instance-of v0, p1, Ljavax/net/ssl/SSLProtocolException;

    if-nez v0, :cond_2

    :goto_0
    iget-object v0, v3, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Lcom/b/a/aa;

    iget-object v4, v3, Lcom/b/a/a/a/w;->a:Lcom/b/a/a;

    iget-object v5, v3, Lcom/b/a/a/a/w;->g:Ljava/net/Proxy;

    iget-object v7, v3, Lcom/b/a/a/a/w;->h:Ljava/net/InetSocketAddress;

    invoke-virtual {v3}, Lcom/b/a/a/a/w;->b()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v0, v4, v5, v7, v8}, Lcom/b/a/aa;-><init>(Lcom/b/a/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Ljava/lang/String;)V

    iget-object v4, v3, Lcom/b/a/a/a/w;->e:Lcom/b/a/a/h;

    invoke-virtual {v4, v0}, Lcom/b/a/a/h;->a(Lcom/b/a/aa;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    if-eqz v6, :cond_3

    instance-of v0, v6, Lcom/b/a/a/a/v;

    if-eqz v0, :cond_8

    :cond_3
    move v0, v2

    :goto_2
    iget-object v3, p0, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    if-nez v3, :cond_4

    iget-object v3, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v3, :cond_7

    :cond_4
    iget-object v3, p0, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    if-eqz v3, :cond_6

    iget-object v4, p0, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    iget-object v3, v4, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    if-eqz v3, :cond_9

    move v3, v2

    :goto_3
    if-nez v3, :cond_5

    iget-object v3, v4, Lcom/b/a/a/a/w;->j:[Ljava/net/InetAddress;

    if-eqz v3, :cond_a

    move v3, v2

    :goto_4
    if-nez v3, :cond_5

    iget-boolean v3, v4, Lcom/b/a/a/a/w;->i:Z

    if-nez v3, :cond_5

    iget-object v3, v4, Lcom/b/a/a/a/w;->l:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_b

    move v3, v2

    :goto_5
    if-eqz v3, :cond_c

    :cond_5
    move v3, v2

    :goto_6
    if-eqz v3, :cond_7

    :cond_6
    instance-of v3, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v3, :cond_d

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    instance-of v3, v3, Ljava/security/cert/CertificateException;

    if-eqz v3, :cond_d

    move v3, v2

    :goto_7
    instance-of v4, p1, Ljava/net/ProtocolException;

    if-nez v3, :cond_e

    if-nez v4, :cond_e

    :goto_8
    if-eqz v2, :cond_7

    if-nez v0, :cond_f

    :cond_7
    const/4 v0, 0x0

    :goto_9
    return-object v0

    :cond_8
    move v0, v1

    goto :goto_2

    :cond_9
    move v3, v1

    goto :goto_3

    :cond_a
    move v3, v1

    goto :goto_4

    :cond_b
    move v3, v1

    goto :goto_5

    :cond_c
    move v3, v1

    goto :goto_6

    :cond_d
    move v3, v1

    goto :goto_7

    :cond_e
    move v2, v1

    goto :goto_8

    :cond_f
    invoke-virtual {p0}, Lcom/b/a/a/a/o;->d()Lcom/b/a/f;

    move-result-object v4

    new-instance v0, Lcom/b/a/a/a/o;

    iget-object v1, p0, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-object v2, p0, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iget-boolean v3, p0, Lcom/b/a/a/a/o;->j:Z

    iget-object v5, p0, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    check-cast v6, Lcom/b/a/a/a/v;

    iget-object v7, p0, Lcom/b/a/a/a/o;->f:Lcom/b/a/x;

    invoke-direct/range {v0 .. v7}, Lcom/b/a/a/a/o;-><init>(Lcom/b/a/p;Lcom/b/a/u;ZLcom/b/a/f;Lcom/b/a/a/a/w;Lcom/b/a/a/a/v;Lcom/b/a/x;)V

    goto :goto_9
.end method

.method public a(Lc/aa;)V
    .locals 4

    .prologue
    .line 510
    iput-object p1, p0, Lcom/b/a/a/a/o;->r:Lc/aa;

    .line 511
    iget-boolean v0, p0, Lcom/b/a/a/a/o;->i:Z

    if-eqz v0, :cond_1

    const-string v2, "gzip"

    iget-object v0, p0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    const-string v3, "Content-Encoding"

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/b/a/x;->f:Lcom/b/a/l;

    invoke-virtual {v0, v3}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 512
    iget-object v0, p0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    new-instance v1, Lcom/b/a/y;

    invoke-direct {v1, v0}, Lcom/b/a/y;-><init>(Lcom/b/a/x;)V

    const-string v0, "Content-Encoding"

    iget-object v2, v1, Lcom/b/a/y;->f:Lcom/b/a/m;

    invoke-virtual {v2, v0}, Lcom/b/a/m;->a(Ljava/lang/String;)Lcom/b/a/m;

    const-string v0, "Content-Length"

    iget-object v2, v1, Lcom/b/a/y;->f:Lcom/b/a/m;

    invoke-virtual {v2, v0}, Lcom/b/a/m;->a(Ljava/lang/String;)Lcom/b/a/m;

    invoke-virtual {v1}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    .line 516
    new-instance v0, Lc/n;

    invoke-direct {v0, p1}, Lc/n;-><init>(Lc/aa;)V

    invoke-static {v0}, Lc/p;->a(Lc/aa;)Lc/j;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/o;->s:Lc/j;

    .line 520
    :goto_1
    return-void

    :cond_0
    move-object v0, v1

    .line 511
    goto :goto_0

    .line 518
    :cond_1
    invoke-static {p1}, Lc/p;->a(Lc/aa;)Lc/j;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/o;->s:Lc/j;

    goto :goto_1
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 307
    iget-object v0, p0, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iget-object v0, v0, Lcom/b/a/u;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/b/a/a/a/q;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/b/a/a/i;->a()Lcom/b/a/a/a/v;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/a/a/o;->p:Lc/z;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/b/a/a/a/o;->v:Ljava/io/InputStream;

    .line 348
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/b/a/a/a/o;->s:Lc/j;

    invoke-static {v0}, Lc/p;->a(Lc/aa;)Lc/j;

    move-result-object v0

    invoke-interface {v0}, Lc/j;->f()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/o;->v:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public final b(Ljava/net/URL;)Z
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    invoke-virtual {v0}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v0

    .line 834
    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/URL;)I

    move-result v1

    invoke-static {p1}, Lcom/b/a/a/i;->a(Ljava/net/URL;)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 3

    .prologue
    .line 405
    sget-object v0, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v1, p0, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    invoke-virtual {v0, v1}, Lcom/b/a/a/a;->a(Lcom/b/a/p;)Lcom/b/a/a/b;

    move-result-object v0

    .line 406
    if-nez v0, :cond_1

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 409
    :cond_1
    iget-object v1, p0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget-object v2, p0, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    invoke-static {v1, v2}, Lcom/b/a/a/a/c;->a(Lcom/b/a/x;Lcom/b/a/u;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 410
    iget-object v0, p0, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    iget-object v0, v0, Lcom/b/a/u;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/b/a/a/a/q;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 412
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 421
    :cond_2
    iget-object v1, p0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    invoke-static {v1}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    invoke-interface {v0}, Lcom/b/a/a/b;->b()Lcom/b/a/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/o;->t:Lcom/b/a/a/a/b;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()Lcom/b/a/f;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 456
    iget-object v1, p0, Lcom/b/a/a/a/o;->q:Lc/i;

    if-eqz v1, :cond_2

    .line 458
    iget-object v1, p0, Lcom/b/a/a/a/o;->q:Lc/i;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    .line 464
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/b/a/a/a/o;->s:Lc/j;

    if-nez v1, :cond_3

    .line 465
    iget-object v1, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v1, v1, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/net/Socket;)V

    .line 466
    :cond_1
    iput-object v0, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    .line 490
    :goto_1
    return-object v0

    .line 459
    :cond_2
    iget-object v1, p0, Lcom/b/a/a/a/o;->p:Lc/z;

    if-eqz v1, :cond_0

    .line 460
    iget-object v1, p0, Lcom/b/a/a/a/o;->p:Lc/z;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 471
    :cond_3
    iget-object v1, p0, Lcom/b/a/a/a/o;->s:Lc/j;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    .line 474
    iget-object v1, p0, Lcom/b/a/a/a/o;->v:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    .line 477
    iget-object v1, p0, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v1}, Lcom/b/a/a/a/aa;->d()Z

    move-result v1

    if-nez v1, :cond_4

    .line 478
    iget-object v1, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v1, v1, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/net/Socket;)V

    .line 479
    iput-object v0, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    goto :goto_1

    .line 484
    :cond_4
    iget-object v1, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v1, :cond_5

    sget-object v1, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v2, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    invoke-virtual {v1, v2}, Lcom/b/a/a/a;->a(Lcom/b/a/f;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 485
    iput-object v0, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    .line 488
    :cond_5
    iget-object v1, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    .line 489
    iput-object v0, p0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    move-object v0, v1

    .line 490
    goto :goto_1
.end method

.method public final e()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 528
    iget-object v0, p0, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iget-object v0, v0, Lcom/b/a/u;->b:Ljava/lang/String;

    const-string v1, "HEAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 547
    :goto_0
    return v0

    .line 532
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget v0, v0, Lcom/b/a/x;->c:I

    .line 533
    const/16 v1, 0x64

    if-lt v0, v1, :cond_1

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_2

    :cond_1
    const/16 v1, 0xcc

    if-eq v0, v1, :cond_2

    const/16 v1, 0x130

    if-eq v0, v1, :cond_2

    move v0, v3

    .line 536
    goto :goto_0

    .line 542
    :cond_2
    iget-object v0, p0, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    invoke-static {v0}, Lcom/b/a/a/a/s;->a(Lcom/b/a/x;)J

    move-result-wide v0

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    const-string v4, "chunked"

    iget-object v0, p0, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    const-string v5, "Transfer-Encoding"

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/b/a/x;->f:Lcom/b/a/l;

    invoke-virtual {v0, v5}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_1
    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v3

    .line 544
    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 542
    goto :goto_1

    :cond_5
    move v0, v2

    .line 547
    goto :goto_0
.end method
