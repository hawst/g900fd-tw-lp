.class Lcom/b/a/a/a/i;
.super Lcom/b/a/a/a/g;
.source "PG"

# interfaces
.implements Lc/aa;


# instance fields
.field final synthetic d:Lcom/b/a/a/a/f;

.field private e:I

.field private f:Z

.field private final g:Lcom/b/a/a/a/o;


# direct methods
.method constructor <init>(Lcom/b/a/a/a/f;Lcom/b/a/a/a/b;Lcom/b/a/a/a/o;)V
    .locals 1

    .prologue
    .line 492
    iput-object p1, p0, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/f;

    .line 493
    invoke-direct {p0, p1, p2}, Lcom/b/a/a/a/g;-><init>(Lcom/b/a/a/a/f;Lcom/b/a/a/a/b;)V

    .line 488
    const/4 v0, -0x1

    iput v0, p0, Lcom/b/a/a/a/i;->e:I

    .line 489
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/b/a/a/a/i;->f:Z

    .line 494
    iput-object p3, p0, Lcom/b/a/a/a/i;->g:Lcom/b/a/a/a/o;

    .line 495
    return-void
.end method


# virtual methods
.method public final a()Lc/ab;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/f;

    iget-object v0, v0, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v0}, Lc/j;->a()Lc/ab;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lc/f;J)J
    .locals 6

    .prologue
    const/4 v5, 0x0

    const-wide/16 v0, -0x1

    const/4 v4, -0x1

    .line 499
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 500
    :cond_0
    iget-boolean v2, p0, Lcom/b/a/a/a/i;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 501
    :cond_1
    iget-boolean v2, p0, Lcom/b/a/a/a/i;->f:Z

    if-nez v2, :cond_3

    .line 515
    :cond_2
    :goto_0
    return-wide v0

    .line 503
    :cond_3
    iget v2, p0, Lcom/b/a/a/a/i;->e:I

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/b/a/a/a/i;->e:I

    if-ne v2, v4, :cond_9

    .line 504
    :cond_4
    iget v2, p0, Lcom/b/a/a/a/i;->e:I

    if-eq v2, v4, :cond_5

    iget-object v2, p0, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/f;

    iget-object v2, v2, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v2}, Lc/j;->l()Ljava/lang/String;

    :cond_5
    iget-object v2, p0, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/f;

    iget-object v2, v2, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v2}, Lc/j;->l()Ljava/lang/String;

    move-result-object v2

    const-string v3, ";"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-eq v3, v4, :cond_6

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :cond_6
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x10

    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/b/a/a/a/i;->e:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    iget v2, p0, Lcom/b/a/a/a/i;->e:I

    if-nez v2, :cond_8

    iput-boolean v5, p0, Lcom/b/a/a/a/i;->f:Z

    new-instance v2, Lcom/b/a/m;

    invoke-direct {v2}, Lcom/b/a/m;-><init>()V

    iget-object v3, p0, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/f;

    invoke-virtual {v3, v2}, Lcom/b/a/a/a/f;->a(Lcom/b/a/m;)V

    iget-object v3, p0, Lcom/b/a/a/a/i;->g:Lcom/b/a/a/a/o;

    new-instance v4, Lcom/b/a/l;

    invoke-direct {v4, v2}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    iget-object v2, v3, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-object v2, v2, Lcom/b/a/p;->e:Ljava/net/CookieHandler;

    if-eqz v2, :cond_7

    iget-object v3, v3, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    invoke-virtual {v3}, Lcom/b/a/u;->b()Ljava/net/URI;

    move-result-object v3

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/b/a/a/a/s;->a(Lcom/b/a/l;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    :cond_7
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/b/a/a/a/i;->a(Z)V

    .line 505
    :cond_8
    iget-boolean v2, p0, Lcom/b/a/a/a/i;->f:Z

    if-eqz v2, :cond_2

    .line 508
    :cond_9
    iget-object v2, p0, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/f;

    iget-object v2, v2, Lcom/b/a/a/a/f;->c:Lc/j;

    iget v3, p0, Lcom/b/a/a/a/i;->e:I

    int-to-long v4, v3

    invoke-static {p2, p3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    invoke-interface {v2, p1, v4, v5}, Lc/j;->b(Lc/f;J)J

    move-result-wide v2

    .line 509
    cmp-long v0, v2, v0

    if-nez v0, :cond_b

    .line 510
    invoke-virtual {p0}, Lcom/b/a/a/a/i;->at_()V

    .line 511
    new-instance v0, Ljava/io/IOException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 504
    :catch_0
    move-exception v0

    new-instance v1, Ljava/net/ProtocolException;

    const-string v3, "Expected a hex chunk size but was "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 513
    :cond_b
    iget v0, p0, Lcom/b/a/a/a/i;->e:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/b/a/a/a/i;->e:I

    .line 514
    invoke-virtual {p0, p1, v2, v3}, Lcom/b/a/a/a/i;->a(Lc/f;J)V

    move-wide v0, v2

    .line 515
    goto/16 :goto_0
.end method

.method public close()V
    .locals 2

    .prologue
    .line 547
    iget-boolean v0, p0, Lcom/b/a/a/a/i;->b:Z

    if-eqz v0, :cond_0

    .line 552
    :goto_0
    return-void

    .line 548
    :cond_0
    iget-boolean v0, p0, Lcom/b/a/a/a/i;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/b/a/a/a/i;->d:Lcom/b/a/a/a/f;

    const/16 v1, 0x64

    invoke-virtual {v0, p0, v1}, Lcom/b/a/a/a/f;->a(Lc/aa;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 549
    invoke-virtual {p0}, Lcom/b/a/a/a/i;->at_()V

    .line 551
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/b/a/a/a/i;->b:Z

    goto :goto_0
.end method
