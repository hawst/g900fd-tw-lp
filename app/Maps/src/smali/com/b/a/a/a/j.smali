.class final Lcom/b/a/a/a/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lc/z;


# instance fields
.field final synthetic a:Lcom/b/a/a/a/f;

.field private b:Z

.field private c:J


# direct methods
.method constructor <init>(Lcom/b/a/a/a/f;J)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/b/a/a/a/j;->a:Lcom/b/a/a/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    iput-wide p2, p0, Lcom/b/a/a/a/j;->c:J

    .line 287
    return-void
.end method


# virtual methods
.method public final a()Lc/ab;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/b/a/a/a/j;->a:Lcom/b/a/a/a/f;

    iget-object v0, v0, Lcom/b/a/a/a/f;->d:Lc/i;

    invoke-interface {v0}, Lc/i;->a()Lc/ab;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lc/f;J)V
    .locals 6

    .prologue
    .line 294
    iget-boolean v0, p0, Lcom/b/a/a/a/j;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_0
    iget-wide v0, p1, Lc/f;->b:J

    const-wide/16 v2, 0x0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lcom/b/a/a/i;->a(JJJ)V

    .line 296
    iget-wide v0, p0, Lcom/b/a/a/a/j;->c:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 297
    new-instance v0, Ljava/net/ProtocolException;

    iget-wide v2, p0, Lcom/b/a/a/a/j;->c:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x45

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "expected "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes but received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/a/j;->a:Lcom/b/a/a/a/f;

    iget-object v0, v0, Lcom/b/a/a/a/f;->d:Lc/i;

    invoke-interface {v0, p1, p2, p3}, Lc/i;->a(Lc/f;J)V

    .line 301
    iget-wide v0, p0, Lcom/b/a/a/a/j;->c:J

    sub-long/2addr v0, p2

    iput-wide v0, p0, Lcom/b/a/a/a/j;->c:J

    .line 302
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 305
    iget-boolean v0, p0, Lcom/b/a/a/a/j;->b:Z

    if-eqz v0, :cond_0

    .line 307
    :goto_0
    return-void

    .line 306
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/j;->a:Lcom/b/a/a/a/f;

    iget-object v0, v0, Lcom/b/a/a/a/f;->d:Lc/i;

    invoke-interface {v0}, Lc/i;->b()V

    goto :goto_0
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/b/a/a/a/j;->b:Z

    if-eqz v0, :cond_0

    .line 314
    :goto_0
    return-void

    .line 311
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/b/a/a/a/j;->b:Z

    .line 312
    iget-wide v0, p0, Lcom/b/a/a/a/j;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 313
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/a/j;->a:Lcom/b/a/a/a/f;

    const/4 v1, 0x3

    iput v1, v0, Lcom/b/a/a/a/f;->e:I

    goto :goto_0
.end method
