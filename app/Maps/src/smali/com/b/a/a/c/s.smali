.class Lcom/b/a/a/c/s;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field final b:Lc/j;

.field private final c:Lc/o;


# direct methods
.method public constructor <init>(Lc/j;)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/b/a/a/c/t;

    invoke-direct {v0, p0, p1}, Lcom/b/a/a/c/t;-><init>(Lcom/b/a/a/c/s;Lc/aa;)V

    .line 65
    new-instance v1, Lcom/b/a/a/c/u;

    invoke-direct {v1, p0}, Lcom/b/a/a/c/u;-><init>(Lcom/b/a/a/c/s;)V

    .line 77
    new-instance v2, Lc/o;

    invoke-direct {v2, v0, v1}, Lc/o;-><init>(Lc/aa;Ljava/util/zip/Inflater;)V

    iput-object v2, p0, Lcom/b/a/a/c/s;->c:Lc/o;

    .line 78
    iget-object v0, p0, Lcom/b/a/a/c/s;->c:Lc/o;

    invoke-static {v0}, Lc/p;->a(Lc/aa;)Lc/j;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/c/s;->b:Lc/j;

    .line 79
    return-void
.end method


# virtual methods
.method public final a(I)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 82
    iget v0, p0, Lcom/b/a/a/c/s;->a:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/b/a/a/c/s;->a:I

    .line 84
    iget-object v0, p0, Lcom/b/a/a/c/s;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->i()I

    move-result v1

    .line 85
    if-gez v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "numberOfPairs < 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_0
    const/16 v0, 0x400

    if-le v1, v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "numberOfPairs > 1024: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    .line 90
    iget-object v3, p0, Lcom/b/a/a/c/s;->b:Lc/j;

    invoke-interface {v3}, Lc/j;->i()I

    move-result v3

    iget-object v4, p0, Lcom/b/a/a/c/s;->b:Lc/j;

    int-to-long v6, v3

    invoke-interface {v4, v6, v7}, Lc/j;->c(J)Lc/k;

    move-result-object v3

    invoke-virtual {v3}, Lc/k;->c()Lc/k;

    move-result-object v3

    .line 91
    iget-object v4, p0, Lcom/b/a/a/c/s;->b:Lc/j;

    invoke-interface {v4}, Lc/j;->i()I

    move-result v4

    iget-object v5, p0, Lcom/b/a/a/c/s;->b:Lc/j;

    int-to-long v6, v4

    invoke-interface {v5, v6, v7}, Lc/j;->c(J)Lc/k;

    move-result-object v4

    .line 92
    iget-object v5, v3, Lc/k;->b:[B

    array-length v5, v5

    if-nez v5, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "name.size == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_2
    new-instance v5, Lcom/b/a/a/c/e;

    invoke-direct {v5, v3, v4}, Lcom/b/a/a/c/e;-><init>(Lc/k;Lc/k;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_3
    iget v0, p0, Lcom/b/a/a/c/s;->a:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/b/a/a/c/s;->c:Lc/o;

    invoke-virtual {v0}, Lc/o;->b()Z

    iget v0, p0, Lcom/b/a/a/c/s;->a:I

    if-eqz v0, :cond_4

    new-instance v0, Ljava/io/IOException;

    iget v1, p0, Lcom/b/a/a/c/s;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "compressedLimit > 0: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 97
    :cond_4
    return-object v2
.end method
