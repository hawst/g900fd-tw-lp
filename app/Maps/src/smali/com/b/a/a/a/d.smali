.class public Lcom/b/a/a/a/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:J

.field public final b:Lcom/b/a/u;

.field public final c:Lcom/b/a/x;

.field public d:Ljava/util/Date;

.field public e:Ljava/lang/String;

.field public f:Ljava/util/Date;

.field public g:Ljava/lang/String;

.field public h:Ljava/util/Date;

.field public i:J

.field public j:J

.field public k:Ljava/lang/String;

.field public l:I


# direct methods
.method public constructor <init>(JLcom/b/a/u;Lcom/b/a/x;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lcom/b/a/a/a/d;->l:I

    .line 102
    iput-wide p1, p0, Lcom/b/a/a/a/d;->a:J

    .line 103
    iput-object p3, p0, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    .line 104
    iput-object p4, p0, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    .line 106
    if-eqz p4, :cond_b

    .line 107
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p4, Lcom/b/a/x;->f:Lcom/b/a/l;

    iget-object v1, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_b

    .line 108
    iget-object v1, p4, Lcom/b/a/x;->f:Lcom/b/a/l;

    shl-int/lit8 v3, v0, 0x1

    if-ltz v3, :cond_0

    iget-object v4, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v4, v4

    if-lt v3, v4, :cond_3

    :cond_0
    move-object v1, v2

    .line 109
    :goto_1
    iget-object v3, p4, Lcom/b/a/x;->f:Lcom/b/a/l;

    shl-int/lit8 v4, v0, 0x1

    add-int/lit8 v4, v4, 0x1

    if-ltz v4, :cond_1

    iget-object v5, v3, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v5, v5

    if-lt v4, v5, :cond_4

    :cond_1
    move-object v3, v2

    .line 110
    :goto_2
    const-string v4, "Date"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 111
    invoke-static {v3}, Lcom/b/a/a/a/m;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    .line 112
    iput-object v3, p0, Lcom/b/a/a/a/d;->e:Ljava/lang/String;

    .line 107
    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_3
    iget-object v1, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v1, v1, v3

    goto :goto_1

    .line 109
    :cond_4
    iget-object v3, v3, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v3, v3, v4

    goto :goto_2

    .line 113
    :cond_5
    const-string v4, "Expires"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 114
    invoke-static {v3}, Lcom/b/a/a/a/m;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/b/a/a/a/d;->h:Ljava/util/Date;

    goto :goto_3

    .line 115
    :cond_6
    const-string v4, "Last-Modified"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 116
    invoke-static {v3}, Lcom/b/a/a/a/m;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/b/a/a/a/d;->f:Ljava/util/Date;

    .line 117
    iput-object v3, p0, Lcom/b/a/a/a/d;->g:Ljava/lang/String;

    goto :goto_3

    .line 118
    :cond_7
    const-string v4, "ETag"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 119
    iput-object v3, p0, Lcom/b/a/a/a/d;->k:Ljava/lang/String;

    goto :goto_3

    .line 120
    :cond_8
    const-string v4, "Age"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 121
    invoke-static {v3}, Lcom/b/a/a/a/e;->a(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/b/a/a/a/d;->l:I

    goto :goto_3

    .line 122
    :cond_9
    sget-object v4, Lcom/b/a/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 123
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/b/a/a/a/d;->i:J

    goto :goto_3

    .line 124
    :cond_a
    sget-object v4, Lcom/b/a/a/a/s;->c:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 125
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/b/a/a/a/d;->j:J

    goto :goto_3

    .line 129
    :cond_b
    return-void
.end method

.method public static a(Lcom/b/a/u;)Z
    .locals 2

    .prologue
    .line 278
    const-string v0, "If-Modified-Since"

    iget-object v1, p0, Lcom/b/a/u;->c:Lcom/b/a/l;

    invoke-virtual {v1, v0}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "If-None-Match"

    iget-object v1, p0, Lcom/b/a/u;->c:Lcom/b/a/l;

    invoke-virtual {v1, v0}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
