.class Lcom/b/a/a/c/al;
.super Lcom/b/a/a/c;
.source "PG"

# interfaces
.implements Lcom/b/a/a/c/c;


# instance fields
.field a:Lcom/b/a/a/c/b;

.field final synthetic b:Lcom/b/a/a/c/ac;


# direct methods
.method constructor <init>(Lcom/b/a/a/c/ac;)V
    .locals 4

    .prologue
    .line 564
    iput-object p1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    .line 565
    const-string v0, "OkHttp %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/b/a/a/c;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 566
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    .line 569
    sget-object v0, Lcom/b/a/a/c/a;->g:Lcom/b/a/a/c/a;

    .line 570
    sget-object v2, Lcom/b/a/a/c/a;->g:Lcom/b/a/a/c/a;

    .line 572
    :try_start_0
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v1, v1, Lcom/b/a/a/c/ac;->q:Lcom/b/a/a/c/as;

    iget-object v3, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v3, v3, Lcom/b/a/a/c/ac;->r:Ljava/net/Socket;

    invoke-static {v3}, Lc/p;->b(Ljava/net/Socket;)Lc/aa;

    move-result-object v3

    invoke-static {v3}, Lc/p;->a(Lc/aa;)Lc/j;

    move-result-object v3

    iget-object v4, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-boolean v4, v4, Lcom/b/a/a/c/ac;->c:Z

    invoke-interface {v1, v3, v4}, Lcom/b/a/a/c/as;->a(Lc/j;Z)Lcom/b/a/a/c/b;

    move-result-object v1

    iput-object v1, p0, Lcom/b/a/a/c/al;->a:Lcom/b/a/a/c/b;

    .line 573
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-boolean v1, v1, Lcom/b/a/a/c/ac;->c:Z

    if-nez v1, :cond_0

    .line 574
    iget-object v1, p0, Lcom/b/a/a/c/al;->a:Lcom/b/a/a/c/b;

    invoke-interface {v1}, Lcom/b/a/a/c/b;->a()V

    .line 576
    :cond_0
    iget-object v1, p0, Lcom/b/a/a/c/al;->a:Lcom/b/a/a/c/b;

    invoke-interface {v1, p0}, Lcom/b/a/a/c/b;->a(Lcom/b/a/a/c/c;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 578
    sget-object v0, Lcom/b/a/a/c/a;->a:Lcom/b/a/a/c/a;

    .line 579
    sget-object v1, Lcom/b/a/a/c/a;->l:Lcom/b/a/a/c/a;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 585
    :try_start_1
    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v2, v0, v1}, Lcom/b/a/a/c/ac;->a(Lcom/b/a/a/c/a;Lcom/b/a/a/c/a;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 588
    :goto_0
    iget-object v0, p0, Lcom/b/a/a/c/al;->a:Lcom/b/a/a/c/b;

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    .line 589
    :goto_1
    return-void

    .line 581
    :catch_0
    move-exception v1

    :try_start_2
    sget-object v1, Lcom/b/a/a/c/a;->b:Lcom/b/a/a/c/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 582
    :try_start_3
    sget-object v0, Lcom/b/a/a/c/a;->b:Lcom/b/a/a/c/a;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 585
    :try_start_4
    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v2, v1, v0}, Lcom/b/a/a/c/ac;->a(Lcom/b/a/a/c/a;Lcom/b/a/a/c/a;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    .line 588
    :goto_2
    iget-object v0, p0, Lcom/b/a/a/c/al;->a:Lcom/b/a/a/c/b;

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    goto :goto_1

    .line 584
    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    .line 585
    :goto_3
    :try_start_5
    iget-object v3, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v3, v1, v2}, Lcom/b/a/a/c/ac;->a(Lcom/b/a/a/c/a;Lcom/b/a/a/c/a;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 588
    :goto_4
    iget-object v1, p0, Lcom/b/a/a/c/al;->a:Lcom/b/a/a/c/b;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    throw v0

    :catch_1
    move-exception v1

    goto :goto_4

    .line 584
    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public final a(IJ)V
    .locals 4

    .prologue
    .line 755
    if-nez p1, :cond_1

    .line 756
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    monitor-enter v1

    .line 757
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-wide v2, v0, Lcom/b/a/a/c/ac;->m:J

    add-long/2addr v2, p2

    iput-wide v2, v0, Lcom/b/a/a/c/ac;->m:J

    .line 758
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 759
    monitor-exit v1

    .line 768
    :cond_0
    :goto_0
    return-void

    .line 759
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 761
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v0, p1}, Lcom/b/a/a/c/ac;->a(I)Lcom/b/a/a/c/ao;

    move-result-object v1

    .line 762
    if-eqz v1, :cond_0

    .line 763
    monitor-enter v1

    .line 764
    :try_start_1
    iget-wide v2, v1, Lcom/b/a/a/c/ao;->b:J

    add-long/2addr v2, p2

    iput-wide v2, v1, Lcom/b/a/a/c/ao;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-lez v0, :cond_2

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 765
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public final a(ILc/k;)V
    .locals 4

    .prologue
    .line 736
    iget-object v0, p2, Lc/k;->b:[B

    array-length v0, v0

    .line 738
    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    monitor-enter v2

    .line 739
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/b/a/a/c/ac;->i:Z

    .line 742
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 743
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 744
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 745
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 746
    if-le v1, p1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/b/a/a/c/ao;

    invoke-virtual {v1}, Lcom/b/a/a/c/ao;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 747
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/ao;

    sget-object v1, Lcom/b/a/a/c/a;->k:Lcom/b/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/b/a/a/c/ao;->c(Lcom/b/a/a/c/a;)V

    .line 748
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 751
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(ILcom/b/a/a/c/a;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 667
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    sget-object v1, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v0, v1, :cond_1

    if-eqz p1, :cond_1

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_1

    move v0, v4

    :goto_0
    if-eqz v0, :cond_2

    .line 668
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v6, v1, Lcom/b/a/a/c/ac;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/b/a/a/c/aj;

    const-string v2, "OkHttp %s Push Reset[%s]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v7, v1, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v7, v3, v5

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/b/a/a/c/aj;-><init>(Lcom/b/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILcom/b/a/a/c/a;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 675
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v5

    .line 667
    goto :goto_0

    .line 671
    :cond_2
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v0, p1}, Lcom/b/a/a/c/ac;->b(I)Lcom/b/a/a/c/ao;

    move-result-object v0

    .line 672
    if-eqz v0, :cond_0

    .line 673
    invoke-virtual {v0, p2}, Lcom/b/a/a/c/ao;->c(Lcom/b/a/a/c/a;)V

    goto :goto_1
.end method

.method public final a(ILjava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 777
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    monitor-enter v1

    :try_start_0
    iget-object v0, v1, Lcom/b/a/a/c/ac;->u:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/b/a/a/c/a;->b:Lcom/b/a/a/c/a;

    invoke-virtual {v1, p1, v0}, Lcom/b/a/a/c/ac;->a(ILcom/b/a/a/c/a;)V

    monitor-exit v1

    .line 778
    :goto_0
    return-void

    .line 777
    :cond_0
    iget-object v0, v1, Lcom/b/a/a/c/ac;->u:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v6, v1, Lcom/b/a/a/c/ac;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/b/a/a/c/ag;

    const-string v2, "OkHttp %s Push Request[%s]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/b/a/a/c/ag;-><init>(Lcom/b/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(ZII)V
    .locals 9

    .prologue
    const-wide/16 v6, -0x1

    const/4 v4, 0x1

    .line 724
    if-eqz p1, :cond_3

    .line 725
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v0, p2}, Lcom/b/a/a/c/ac;->c(I)Lcom/b/a/a/c/v;

    move-result-object v0

    .line 726
    if-eqz v0, :cond_2

    .line 727
    iget-wide v2, v0, Lcom/b/a/a/c/v;->c:J

    cmp-long v1, v2, v6

    if-nez v1, :cond_0

    iget-wide v2, v0, Lcom/b/a/a/c/v;->b:J

    cmp-long v1, v2, v6

    if-nez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/b/a/a/c/v;->c:J

    iget-object v0, v0, Lcom/b/a/a/c/v;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 733
    :cond_2
    :goto_0
    return-void

    .line 731
    :cond_3
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    const/4 v7, 0x0

    sget-object v8, Lcom/b/a/a/c/ac;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/b/a/a/c/af;

    const-string v2, "OkHttp %s ping %08x%08x"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v6, v3, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v5, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/b/a/a/c/af;-><init>(Lcom/b/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ZIILcom/b/a/a/c/v;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method public final a(ZILc/j;I)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 594
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    sget-object v1, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v0, v1, :cond_0

    if-eqz p2, :cond_0

    and-int/lit8 v0, p2, 0x1

    if-nez v0, :cond_0

    move v0, v4

    :goto_0
    if-eqz v0, :cond_3

    .line 595
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    new-instance v5, Lc/f;

    invoke-direct {v5}, Lc/f;-><init>()V

    int-to-long v2, p4

    invoke-interface {p3, v2, v3}, Lc/j;->a(J)V

    int-to-long v2, p4

    invoke-interface {p3, v5, v2, v3}, Lc/j;->b(Lc/f;J)J

    iget-wide v2, v5, Lc/f;->b:J

    int-to-long v8, p4

    cmp-long v0, v2, v8

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    iget-wide v2, v5, Lc/f;->b:J

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x23

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v6

    .line 594
    goto :goto_0

    .line 595
    :cond_1
    iget-object v8, v1, Lcom/b/a/a/c/ac;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/b/a/a/c/ai;

    const-string v2, "OkHttp %s Push Data[%s]"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v7, v1, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v7, v3, v6

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    move v4, p2

    move v6, p4

    move v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/b/a/a/c/ai;-><init>(Lcom/b/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILc/f;IZ)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 608
    :cond_2
    :goto_1
    return-void

    .line 598
    :cond_3
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v0, p2}, Lcom/b/a/a/c/ac;->a(I)Lcom/b/a/a/c/ao;

    move-result-object v0

    .line 599
    if-nez v0, :cond_4

    .line 600
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    sget-object v1, Lcom/b/a/a/c/a;->c:Lcom/b/a/a/c/a;

    invoke-virtual {v0, p2, v1}, Lcom/b/a/a/c/ac;->a(ILcom/b/a/a/c/a;)V

    .line 601
    int-to-long v0, p4

    invoke-interface {p3, v0, v1}, Lc/j;->f(J)V

    goto :goto_1

    .line 604
    :cond_4
    sget-boolean v1, Lcom/b/a/a/c/ao;->k:Z

    if-nez v1, :cond_5

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_5
    iget-object v1, v0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    int-to-long v2, p4

    invoke-virtual {v1, p3, v2, v3}, Lcom/b/a/a/c/aq;->a(Lc/j;J)V

    .line 605
    if-eqz p1, :cond_2

    .line 606
    invoke-virtual {v0}, Lcom/b/a/a/c/ao;->e()V

    goto :goto_1
.end method

.method public final a(ZLcom/b/a/a/c/y;)V
    .locals 12

    .prologue
    const/high16 v1, 0x10000

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 678
    .line 679
    const/4 v0, 0x0

    .line 680
    iget-object v9, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    monitor-enter v9

    .line 681
    :try_start_0
    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v2, v2, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    iget v7, v2, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v7, v7, 0x80

    if-eqz v7, :cond_2

    iget-object v2, v2, Lcom/b/a/a/c/y;->d:[I

    const/4 v7, 0x7

    aget v2, v2, v7

    move v8, v2

    .line 682
    :goto_0
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v2, v2, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    const/4 v7, 0x0

    iput v7, v2, Lcom/b/a/a/c/y;->c:I

    iput v7, v2, Lcom/b/a/a/c/y;->b:I

    iput v7, v2, Lcom/b/a/a/c/y;->a:I

    iget-object v2, v2, Lcom/b/a/a/c/y;->d:[I

    const/4 v7, 0x0

    invoke-static {v2, v7}, Ljava/util/Arrays;->fill([II)V

    .line 683
    :cond_0
    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v10, v2, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    move v7, v6

    :goto_1
    const/16 v2, 0xa

    if-ge v7, v2, :cond_4

    shl-int v2, v3, v7

    iget v11, p2, Lcom/b/a/a/c/y;->a:I

    and-int/2addr v2, v11

    if-eqz v2, :cond_3

    move v2, v3

    :goto_2
    if-eqz v2, :cond_1

    invoke-virtual {p2, v7}, Lcom/b/a/a/c/y;->a(I)I

    move-result v2

    iget-object v11, p2, Lcom/b/a/a/c/y;->d:[I

    aget v11, v11, v7

    invoke-virtual {v10, v7, v2, v11}, Lcom/b/a/a/c/y;->a(III)Lcom/b/a/a/c/y;

    :cond_1
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_1

    :cond_2
    move v8, v1

    .line 681
    goto :goto_0

    :cond_3
    move v2, v6

    .line 683
    goto :goto_2

    .line 684
    :cond_4
    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v2, v2, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    sget-object v3, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v2, v3, :cond_5

    .line 685
    sget-object v2, Lcom/b/a/a/c/ac;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/b/a/a/c/an;

    const-string v6, "OkHttp %s ACK Settings"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v11, v11, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v11, v7, v10

    invoke-direct {v3, p0, v6, v7, p2}, Lcom/b/a/a/c/an;-><init>(Lcom/b/a/a/c/al;Ljava/lang/String;[Ljava/lang/Object;Lcom/b/a/a/c/y;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 687
    :cond_5
    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v2, v2, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    iget v3, v2, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_6

    iget-object v1, v2, Lcom/b/a/a/c/y;->d:[I

    const/4 v2, 0x7

    aget v1, v1, v2

    .line 688
    :cond_6
    const/4 v2, -0x1

    if-eq v1, v2, :cond_c

    if-eq v1, v8, :cond_c

    .line 689
    sub-int/2addr v1, v8

    int-to-long v2, v1

    .line 690
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-boolean v1, v1, Lcom/b/a/a/c/ac;->p:Z

    if-nez v1, :cond_8

    .line 691
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-wide v6, v1, Lcom/b/a/a/c/ac;->m:J

    add-long/2addr v6, v2

    iput-wide v6, v1, Lcom/b/a/a/c/ac;->m:J

    cmp-long v6, v2, v4

    if-lez v6, :cond_7

    invoke-virtual {v1}, Ljava/lang/Object;->notifyAll()V

    .line 692
    :cond_7
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    const/4 v6, 0x1

    iput-boolean v6, v1, Lcom/b/a/a/c/ac;->p:Z

    .line 694
    :cond_8
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v1, v1, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    .line 695
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v1, v1, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v1, v1, [Lcom/b/a/a/c/ao;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/a/a/c/ao;

    .line 698
    :cond_9
    :goto_3
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 699
    if-eqz v0, :cond_b

    cmp-long v0, v2, v4

    if-eqz v0, :cond_b

    .line 700
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/ao;

    .line 701
    monitor-enter v0

    .line 702
    :try_start_1
    iget-wide v6, v0, Lcom/b/a/a/c/ao;->b:J

    add-long/2addr v6, v2

    iput-wide v6, v0, Lcom/b/a/a/c/ao;->b:J

    cmp-long v6, v2, v4

    if-lez v6, :cond_a

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 703
    :cond_a
    monitor-exit v0

    goto :goto_4

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 698
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 704
    :cond_b
    return-void

    :cond_c
    move-wide v2, v4

    goto :goto_3
.end method

.method public final a(ZZILjava/util/List;Lcom/b/a/a/c/f;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZI",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;",
            "Lcom/b/a/a/c/f;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 612
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    sget-object v1, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v0, v1, :cond_1

    if-eqz p3, :cond_1

    and-int/lit8 v0, p3, 0x1

    if-nez v0, :cond_1

    move v0, v4

    :goto_0
    if-eqz v0, :cond_2

    .line 613
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v7, v1, Lcom/b/a/a/c/ac;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/b/a/a/c/ah;

    const-string v2, "OkHttp %s Push Headers[%s]"

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v6, v1, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v6, v3, v5

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    move v4, p3

    move-object v5, p4

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/b/a/a/c/ah;-><init>(Lcom/b/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;Z)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 664
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v5

    .line 612
    goto :goto_0

    .line 617
    :cond_2
    iget-object v6, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    monitor-enter v6

    .line 619
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-boolean v0, v0, Lcom/b/a/a/c/ac;->i:Z

    if-eqz v0, :cond_3

    monitor-exit v6

    goto :goto_1

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 621
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v0, p3}, Lcom/b/a/a/c/ac;->a(I)Lcom/b/a/a/c/ao;

    move-result-object v2

    .line 623
    if-nez v2, :cond_9

    .line 625
    sget-object v0, Lcom/b/a/a/c/f;->b:Lcom/b/a/a/c/f;

    if-eq p5, v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/f;->c:Lcom/b/a/a/c/f;

    if-ne p5, v0, :cond_5

    :cond_4
    move v5, v4

    :cond_5
    if-eqz v5, :cond_6

    .line 626
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    sget-object v1, Lcom/b/a/a/c/a;->c:Lcom/b/a/a/c/a;

    invoke-virtual {v0, p3, v1}, Lcom/b/a/a/c/ac;->a(ILcom/b/a/a/c/a;)V

    .line 627
    monitor-exit v6

    goto :goto_1

    .line 631
    :cond_6
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget v0, v0, Lcom/b/a/a/c/ac;->g:I

    if-gt p3, v0, :cond_7

    monitor-exit v6

    goto :goto_1

    .line 634
    :cond_7
    rem-int/lit8 v0, p3, 0x2

    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget v1, v1, Lcom/b/a/a/c/ac;->h:I

    rem-int/lit8 v1, v1, 0x2

    if-ne v0, v1, :cond_8

    monitor-exit v6

    goto :goto_1

    .line 637
    :cond_8
    new-instance v0, Lcom/b/a/a/c/ao;

    iget-object v2, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    move v1, p3

    move v3, p1

    move v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/b/a/a/c/ao;-><init>(ILcom/b/a/a/c/ac;ZZLjava/util/List;)V

    .line 639
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iput p3, v1, Lcom/b/a/a/c/ac;->g:I

    .line 640
    iget-object v1, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v1, v1, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 641
    sget-object v1, Lcom/b/a/a/c/ac;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/b/a/a/c/am;

    const-string v3, "OkHttp %s stream %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v7, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    iget-object v7, v7, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v7, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v5

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/b/a/a/c/am;-><init>(Lcom/b/a/a/c/al;Ljava/lang/String;[Ljava/lang/Object;Lcom/b/a/a/c/ao;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 650
    monitor-exit v6

    goto :goto_1

    .line 652
    :cond_9
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 655
    sget-object v0, Lcom/b/a/a/c/f;->a:Lcom/b/a/a/c/f;

    if-ne p5, v0, :cond_b

    move v0, v4

    :goto_2
    if-eqz v0, :cond_c

    .line 656
    sget-object v0, Lcom/b/a/a/c/a;->b:Lcom/b/a/a/c/a;

    invoke-virtual {v2, v0}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/a;)Z

    move-result v1

    if-eqz v1, :cond_a

    iget-object v1, v2, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v2, v2, Lcom/b/a/a/c/ao;->c:I

    invoke-virtual {v1, v2, v0}, Lcom/b/a/a/c/ac;->a(ILcom/b/a/a/c/a;)V

    .line 657
    :cond_a
    iget-object v0, p0, Lcom/b/a/a/c/al;->b:Lcom/b/a/a/c/ac;

    invoke-virtual {v0, p3}, Lcom/b/a/a/c/ac;->b(I)Lcom/b/a/a/c/ao;

    goto/16 :goto_1

    :cond_b
    move v0, v5

    .line 655
    goto :goto_2

    .line 662
    :cond_c
    sget-boolean v0, Lcom/b/a/a/c/ao;->k:Z

    if-nez v0, :cond_d

    invoke-static {v2}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_d
    const/4 v1, 0x0

    monitor-enter v2

    :try_start_2
    iget-object v0, v2, Lcom/b/a/a/c/ao;->e:Ljava/util/List;

    if-nez v0, :cond_11

    sget-object v0, Lcom/b/a/a/c/f;->c:Lcom/b/a/a/c/f;

    if-ne p5, v0, :cond_e

    move v5, v4

    :cond_e
    if-eqz v5, :cond_10

    sget-object v0, Lcom/b/a/a/c/a;->b:Lcom/b/a/a/c/a;

    move-object v1, v0

    move v0, v4

    :goto_3
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_14

    invoke-virtual {v2, v1}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/a;)Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, v2, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v3, v2, Lcom/b/a/a/c/ao;->c:I

    invoke-virtual {v0, v3, v1}, Lcom/b/a/a/c/ac;->a(ILcom/b/a/a/c/a;)V

    .line 663
    :cond_f
    :goto_4
    if-eqz p2, :cond_0

    invoke-virtual {v2}, Lcom/b/a/a/c/ao;->e()V

    goto/16 :goto_1

    .line 662
    :cond_10
    :try_start_3
    iput-object p4, v2, Lcom/b/a/a/c/ao;->e:Ljava/util/List;

    invoke-virtual {v2}, Lcom/b/a/a/c/ao;->a()Z

    move-result v0

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    goto :goto_3

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_11
    :try_start_4
    sget-object v0, Lcom/b/a/a/c/f;->b:Lcom/b/a/a/c/f;

    if-ne p5, v0, :cond_12

    move v0, v4

    :goto_5
    if-eqz v0, :cond_13

    sget-object v0, Lcom/b/a/a/c/a;->e:Lcom/b/a/a/c/a;

    move-object v1, v0

    move v0, v4

    goto :goto_3

    :cond_12
    move v0, v5

    goto :goto_5

    :cond_13
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v2, Lcom/b/a/a/c/ao;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0, p4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v0, v2, Lcom/b/a/a/c/ao;->e:Ljava/util/List;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move v0, v4

    goto :goto_3

    :cond_14
    if-nez v0, :cond_f

    iget-object v0, v2, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v1, v2, Lcom/b/a/a/c/ao;->c:I

    invoke-virtual {v0, v1}, Lcom/b/a/a/c/ac;->b(I)Lcom/b/a/a/c/ao;

    goto :goto_4
.end method
