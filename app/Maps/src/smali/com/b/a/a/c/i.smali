.class final Lcom/b/a/a/c/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lc/f;


# direct methods
.method constructor <init>(Lc/f;)V
    .locals 0

    .prologue
    .line 372
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 373
    iput-object p1, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    .line 374
    return-void
.end method

.method private a(III)V
    .locals 3

    .prologue
    .line 398
    if-ge p1, p2, :cond_0

    .line 399
    iget-object v0, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    or-int v1, p3, p1

    invoke-virtual {v0, v1}, Lc/f;->a(I)Lc/f;

    .line 414
    :goto_0
    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    or-int v1, p3, p2

    invoke-virtual {v0, v1}, Lc/f;->a(I)Lc/f;

    .line 405
    sub-int v0, p1, p2

    .line 408
    :goto_1
    const/16 v1, 0x80

    if-lt v0, v1, :cond_1

    .line 409
    and-int/lit8 v1, v0, 0x7f

    .line 410
    iget-object v2, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {v2, v1}, Lc/f;->a(I)Lc/f;

    .line 411
    ushr-int/lit8 v0, v0, 0x7

    .line 412
    goto :goto_1

    .line 413
    :cond_1
    iget-object v1, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    invoke-virtual {v1, v0}, Lc/f;->a(I)Lc/f;

    goto :goto_0
.end method

.method private a(Lc/k;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 417
    iget-object v0, p1, Lc/k;->b:[B

    array-length v0, v0

    const/16 v1, 0x7f

    invoke-direct {p0, v0, v1, v3}, Lcom/b/a/a/c/i;->a(III)V

    .line 418
    iget-object v0, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byteString == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p1, Lc/k;->b:[B

    iget-object v2, p1, Lc/k;->b:[B

    array-length v2, v2

    invoke-virtual {v0, v1, v3, v2}, Lc/f;->b([BII)Lc/f;

    .line 419
    return-void
.end method


# virtual methods
.method final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 380
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    .line 381
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/e;

    iget-object v0, v0, Lcom/b/a/a/c/e;->h:Lc/k;

    invoke-virtual {v0}, Lc/k;->c()Lc/k;

    move-result-object v4

    .line 382
    sget-object v0, Lcom/b/a/a/c/g;->b:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 383
    if-eqz v0, :cond_1

    .line 385
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v4, 0xf

    invoke-direct {p0, v0, v4, v2}, Lcom/b/a/a/c/i;->a(III)V

    .line 386
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/e;

    iget-object v0, v0, Lcom/b/a/a/c/e;->i:Lc/k;

    iget-object v4, v0, Lc/k;->b:[B

    array-length v4, v4

    const/16 v5, 0x7f

    invoke-direct {p0, v4, v5, v2}, Lcom/b/a/a/c/i;->a(III)V

    iget-object v4, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "byteString == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v5, v0, Lc/k;->b:[B

    iget-object v0, v0, Lc/k;->b:[B

    array-length v0, v0

    invoke-virtual {v4, v5, v2, v0}, Lc/f;->b([BII)Lc/f;

    .line 380
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 388
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/c/i;->a:Lc/f;

    invoke-virtual {v0, v2}, Lc/f;->a(I)Lc/f;

    .line 389
    invoke-direct {p0, v4}, Lcom/b/a/a/c/i;->a(Lc/k;)V

    .line 390
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/e;

    iget-object v0, v0, Lcom/b/a/a/c/e;->i:Lc/k;

    invoke-direct {p0, v0}, Lcom/b/a/a/c/i;->a(Lc/k;)V

    goto :goto_1

    .line 393
    :cond_2
    return-void
.end method
