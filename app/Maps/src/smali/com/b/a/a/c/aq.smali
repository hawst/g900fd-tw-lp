.class final Lcom/b/a/a/c/aq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lc/aa;


# static fields
.field static final synthetic c:Z


# instance fields
.field a:Z

.field b:Z

.field final synthetic d:Lcom/b/a/a/c/ao;

.field private final e:Lc/f;

.field private final f:Lc/f;

.field private final g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 314
    const-class v0, Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/b/a/a/c/aq;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/b/a/a/c/ao;J)V
    .locals 2

    .prologue
    .line 333
    iput-object p1, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 316
    new-instance v0, Lc/f;

    invoke-direct {v0}, Lc/f;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/aq;->e:Lc/f;

    .line 319
    new-instance v0, Lc/f;

    invoke-direct {v0}, Lc/f;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    .line 334
    iput-wide p2, p0, Lcom/b/a/a/c/aq;->g:J

    .line 335
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 374
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    invoke-virtual {v0}, Lcom/b/a/a/c/ar;->b()V

    .line 376
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    iget-wide v2, v0, Lc/f;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/b/a/a/c/aq;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/b/a/a/c/aq;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    if-nez v0, :cond_0

    .line 377
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 380
    :catchall_0
    move-exception v0

    iget-object v2, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v2, v2, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    iget-boolean v3, v2, Lc/a;->b:Z

    if-nez v3, :cond_2

    :goto_1
    if-eqz v1, :cond_3

    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    iget-boolean v2, v0, Lc/a;->b:Z

    if-nez v2, :cond_1

    :goto_2
    if-eqz v1, :cond_4

    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-boolean v1, v0, Lc/a;->b:Z

    invoke-static {v0}, Lc/a;->a(Lc/a;)Z

    move-result v1

    goto :goto_2

    :cond_2
    iput-boolean v1, v2, Lc/a;->b:Z

    invoke-static {v2}, Lc/a;->a(Lc/a;)Z

    move-result v1

    goto :goto_1

    :cond_3
    throw v0

    .line 381
    :cond_4
    return-void
.end method


# virtual methods
.method public final a()Lc/ab;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    return-object v0
.end method

.method final a(Lc/j;J)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 385
    sget-boolean v0, Lcom/b/a/a/c/aq;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 411
    :cond_0
    sub-long/2addr p2, v4

    .line 414
    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    monitor-enter v3

    .line 415
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    iget-wide v4, v0, Lc/f;->b:J

    cmp-long v0, v4, v10

    if-nez v0, :cond_7

    move v0, v1

    .line 416
    :goto_0
    iget-object v4, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    iget-object v5, p0, Lcom/b/a/a/c/aq;->e:Lc/f;

    invoke-virtual {v4, v5}, Lc/f;->a(Lc/aa;)J

    .line 417
    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 420
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 387
    :cond_2
    cmp-long v0, p2, v10

    if-lez v0, :cond_3

    .line 390
    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    monitor-enter v3

    .line 391
    :try_start_1
    iget-boolean v4, p0, Lcom/b/a/a/c/aq;->b:Z

    .line 392
    iget-object v0, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    iget-wide v6, v0, Lc/f;->b:J

    add-long/2addr v6, p2

    iget-wide v8, p0, Lcom/b/a/a/c/aq;->g:J

    cmp-long v0, v6, v8

    if-lez v0, :cond_4

    move v0, v1

    .line 393
    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 396
    if-eqz v0, :cond_5

    .line 397
    invoke-interface {p1, p2, p3}, Lc/j;->f(J)V

    .line 398
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    sget-object v1, Lcom/b/a/a/c/a;->h:Lcom/b/a/a/c/a;

    invoke-virtual {v0, v1}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v0, v0, Lcom/b/a/a/c/ao;->c:I

    invoke-virtual {v2, v0, v1}, Lcom/b/a/a/c/ac;->a(ILcom/b/a/a/c/a;)V

    .line 421
    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v2

    .line 392
    goto :goto_1

    .line 393
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 403
    :cond_5
    if-eqz v4, :cond_6

    .line 404
    invoke-interface {p1, p2, p3}, Lc/j;->f(J)V

    goto :goto_2

    .line 409
    :cond_6
    iget-object v0, p0, Lcom/b/a/a/c/aq;->e:Lc/f;

    invoke-interface {p1, v0, p2, p3}, Lc/j;->b(Lc/f;J)J

    move-result-wide v4

    .line 410
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    .line 415
    goto :goto_0

    .line 420
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final b(Lc/f;J)J
    .locals 10

    .prologue
    const/high16 v2, 0x10000

    const-wide/16 v6, 0x0

    .line 339
    cmp-long v0, p2, v6

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 342
    :cond_0
    iget-object v4, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    monitor-enter v4

    .line 343
    :try_start_0
    invoke-direct {p0}, Lcom/b/a/a/c/aq;->b()V

    .line 344
    iget-boolean v0, p0, Lcom/b/a/a/c/aq;->a:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 344
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v1, v1, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stream was reset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    iget-wide v0, v0, Lc/f;->b:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_3

    const-wide/16 v0, -0x1

    monitor-exit v4

    .line 369
    :goto_0
    return-wide v0

    .line 348
    :cond_3
    iget-object v0, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    iget-object v1, p0, Lcom/b/a/a/c/aq;->f:Lc/f;

    iget-wide v6, v1, Lc/f;->b:J

    invoke-static {p2, p3, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    invoke-virtual {v0, p1, v6, v7}, Lc/f;->b(Lc/f;J)J

    move-result-wide v0

    .line 351
    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-wide v6, v3, Lcom/b/a/a/c/ao;->a:J

    add-long/2addr v6, v0

    iput-wide v6, v3, Lcom/b/a/a/c/ao;->a:J

    .line 352
    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-wide v6, v3, Lcom/b/a/a/c/ao;->a:J

    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v3, v3, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-object v3, v3, Lcom/b/a/a/c/ac;->n:Lcom/b/a/a/c/y;

    iget v5, v3, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v5, v5, 0x80

    if-eqz v5, :cond_7

    iget-object v3, v3, Lcom/b/a/a/c/y;->d:[I

    const/4 v5, 0x7

    aget v3, v3, v5

    :goto_1
    div-int/lit8 v3, v3, 0x2

    int-to-long v8, v3

    cmp-long v3, v6, v8

    if-ltz v3, :cond_4

    .line 354
    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v3, v3, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-object v5, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget v5, v5, Lcom/b/a/a/c/ao;->c:I

    iget-object v6, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-wide v6, v6, Lcom/b/a/a/c/ao;->a:J

    invoke-virtual {v3, v5, v6, v7}, Lcom/b/a/a/c/ac;->a(IJ)V

    .line 355
    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    const-wide/16 v6, 0x0

    iput-wide v6, v3, Lcom/b/a/a/c/ao;->a:J

    .line 357
    :cond_4
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 360
    iget-object v3, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v3, v3, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    monitor-enter v3

    .line 361
    :try_start_2
    iget-object v4, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v4, v4, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-wide v6, v4, Lcom/b/a/a/c/ac;->l:J

    add-long/2addr v6, v0

    iput-wide v6, v4, Lcom/b/a/a/c/ac;->l:J

    .line 362
    iget-object v4, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v4, v4, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-wide v4, v4, Lcom/b/a/a/c/ac;->l:J

    iget-object v6, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v6, v6, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-object v6, v6, Lcom/b/a/a/c/ac;->n:Lcom/b/a/a/c/y;

    iget v7, v6, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v7, v7, 0x80

    if-eqz v7, :cond_5

    iget-object v2, v6, Lcom/b/a/a/c/y;->d:[I

    const/4 v6, 0x7

    aget v2, v2, v6

    :cond_5
    div-int/lit8 v2, v2, 0x2

    int-to-long v6, v2

    cmp-long v2, v4, v6

    if-ltz v2, :cond_6

    .line 364
    iget-object v2, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v2, v2, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v5, v5, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-wide v6, v5, Lcom/b/a/a/c/ac;->l:J

    invoke-virtual {v2, v4, v6, v7}, Lcom/b/a/a/c/ac;->a(IJ)V

    .line 365
    iget-object v2, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    iget-object v2, v2, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    const-wide/16 v4, 0x0

    iput-wide v4, v2, Lcom/b/a/a/c/ac;->l:J

    .line 367
    :cond_6
    monitor-exit v3

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_7
    move v3, v2

    .line 352
    goto :goto_1
.end method

.method public final close()V
    .locals 4

    .prologue
    .line 429
    iget-object v1, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    monitor-enter v1

    .line 430
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/b/a/a/c/aq;->a:Z

    .line 431
    iget-object v0, p0, Lcom/b/a/a/c/aq;->f:Lc/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-wide v2, v0, Lc/f;->b:J

    invoke-virtual {v0, v2, v3}, Lc/f;->f(J)V
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 432
    :try_start_2
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 433
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 434
    iget-object v0, p0, Lcom/b/a/a/c/aq;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Lcom/b/a/a/c/ao;->a(Lcom/b/a/a/c/ao;)V

    .line 435
    return-void

    .line 431
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v2, Ljava/lang/AssertionError;

    invoke-direct {v2, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    .line 433
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0
.end method
