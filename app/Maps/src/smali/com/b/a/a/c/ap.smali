.class final Lcom/b/a/a/c/ap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lc/z;


# static fields
.field static final synthetic c:Z


# instance fields
.field a:Z

.field b:Z

.field final synthetic d:Lcom/b/a/a/c/ao;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 470
    const-class v0, Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/b/a/a/c/ap;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/b/a/a/c/ao;)V
    .locals 0

    .prologue
    .line 470
    iput-object p1, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lc/ab;
    .locals 1

    .prologue
    .line 512
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->i:Lcom/b/a/a/c/ar;

    return-object v0
.end method

.method public final a(Lc/f;J)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 480
    sget-boolean v0, Lcom/b/a/a/c/ap;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 493
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/ao;)V

    .line 494
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-wide v4, v0, Lcom/b/a/a/c/ao;->b:J

    invoke-static {v4, v5, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 495
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-wide v6, v0, Lcom/b/a/a/c/ao;->b:J

    sub-long/2addr v6, v4

    iput-wide v6, v0, Lcom/b/a/a/c/ao;->b:J

    .line 496
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 498
    sub-long/2addr p2, v4

    .line 499
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-object v1, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget v1, v1, Lcom/b/a/a/c/ao;->c:I

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/b/a/a/c/ac;->a(IZLc/f;J)V

    .line 481
    :cond_1
    cmp-long v0, p2, v8

    if-lez v0, :cond_6

    .line 483
    iget-object v1, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    monitor-enter v1

    .line 484
    :try_start_1
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->i:Lcom/b/a/a/c/ar;

    invoke-virtual {v0}, Lcom/b/a/a/c/ar;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 486
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-wide v4, v0, Lcom/b/a/a/c/ao;->b:J

    cmp-long v0, v4, v8

    if-gtz v0, :cond_2

    iget-boolean v0, p0, Lcom/b/a/a/c/ap;->b:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/b/a/a/c/ap;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    if-nez v0, :cond_2

    .line 487
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 490
    :catchall_0
    move-exception v0

    :try_start_5
    iget-object v3, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v3, v3, Lcom/b/a/a/c/ao;->i:Lcom/b/a/a/c/ar;

    iget-boolean v4, v3, Lc/a;->b:Z

    if-nez v4, :cond_4

    :goto_1
    if-eqz v2, :cond_5

    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v2, "timeout"

    invoke-direct {v0, v2}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 496
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 490
    :cond_2
    :try_start_6
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->i:Lcom/b/a/a/c/ar;

    iget-boolean v3, v0, Lc/a;->b:Z

    if-nez v3, :cond_3

    move v0, v2

    :goto_2
    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v2, "timeout"

    invoke-direct {v0, v2}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    const/4 v3, 0x0

    iput-boolean v3, v0, Lc/a;->b:Z

    invoke-static {v0}, Lc/a;->a(Lc/a;)Z

    move-result v0

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    iput-boolean v2, v3, Lc/a;->b:Z

    invoke-static {v3}, Lc/a;->a(Lc/a;)Z

    move-result v2

    goto :goto_1

    :cond_5
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 501
    :cond_6
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 504
    sget-boolean v0, Lcom/b/a/a/c/ap;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 505
    :cond_0
    iget-object v1, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    monitor-enter v1

    .line 506
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/ao;)V

    .line 507
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 508
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v0}, Lcom/b/a/a/c/d;->b()V

    .line 509
    return-void

    .line 507
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final close()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 516
    sget-boolean v0, Lcom/b/a/a/c/ap;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 517
    :cond_0
    iget-object v1, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    monitor-enter v1

    .line 518
    :try_start_0
    iget-boolean v0, p0, Lcom/b/a/a/c/ap;->a:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    .line 528
    :goto_0
    return-void

    .line 519
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 520
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v0, v0, Lcom/b/a/a/c/ap;->b:Z

    if-nez v0, :cond_2

    .line 521
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-object v1, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget v1, v1, Lcom/b/a/a/c/ao;->c:I

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/b/a/a/c/ac;->a(IZLc/f;J)V

    .line 523
    :cond_2
    iget-object v1, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    monitor-enter v1

    .line 524
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/b/a/a/c/ap;->a:Z

    .line 525
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 526
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    iget-object v0, v0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-object v0, v0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v0}, Lcom/b/a/a/c/d;->b()V

    .line 527
    iget-object v0, p0, Lcom/b/a/a/c/ap;->d:Lcom/b/a/a/c/ao;

    invoke-static {v0}, Lcom/b/a/a/c/ao;->a(Lcom/b/a/a/c/ao;)V

    goto :goto_0

    .line 519
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 525
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
