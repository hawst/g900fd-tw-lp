.class public final Lcom/b/a/a/c/ao;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final synthetic k:Z


# instance fields
.field a:J

.field b:J

.field public final c:I

.field public final d:Lcom/b/a/a/c/ac;

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/b/a/a/c/aq;

.field final g:Lcom/b/a/a/c/ap;

.field public final h:Lcom/b/a/a/c/ar;

.field final i:Lcom/b/a/a/c/ar;

.field j:Lcom/b/a/a/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/b/a/a/c/ao;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/b/a/a/c/ao;->k:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(ILcom/b/a/a/c/ac;ZZLjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/b/a/a/c/ac;",
            "ZZ",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/high16 v1, 0x10000

    const/4 v4, 0x7

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/b/a/a/c/ao;->a:J

    .line 56
    new-instance v0, Lcom/b/a/a/c/ar;

    invoke-direct {v0, p0}, Lcom/b/a/a/c/ar;-><init>(Lcom/b/a/a/c/ao;)V

    iput-object v0, p0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    .line 67
    new-instance v0, Lcom/b/a/a/c/ar;

    invoke-direct {v0, p0}, Lcom/b/a/a/c/ar;-><init>(Lcom/b/a/a/c/ao;)V

    iput-object v0, p0, Lcom/b/a/a/c/ao;->i:Lcom/b/a/a/c/ar;

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    .line 78
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    if-nez p5, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_1
    iput p1, p0, Lcom/b/a/a/c/ao;->c:I

    .line 81
    iput-object p2, p0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    .line 82
    iget-object v0, p2, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    iget v2, v0, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_3

    iget-object v0, v0, Lcom/b/a/a/c/y;->d:[I

    aget v0, v0, v4

    :goto_0
    int-to-long v2, v0

    iput-wide v2, p0, Lcom/b/a/a/c/ao;->b:J

    .line 84
    new-instance v0, Lcom/b/a/a/c/aq;

    iget-object v2, p2, Lcom/b/a/a/c/ac;->n:Lcom/b/a/a/c/y;

    iget v3, v2, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_2

    iget-object v1, v2, Lcom/b/a/a/c/y;->d:[I

    aget v1, v1, v4

    :cond_2
    int-to-long v2, v1

    invoke-direct {v0, p0, v2, v3}, Lcom/b/a/a/c/aq;-><init>(Lcom/b/a/a/c/ao;J)V

    iput-object v0, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    .line 86
    new-instance v0, Lcom/b/a/a/c/ap;

    invoke-direct {v0, p0}, Lcom/b/a/a/c/ap;-><init>(Lcom/b/a/a/c/ao;)V

    iput-object v0, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    .line 87
    iget-object v0, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    iput-boolean p4, v0, Lcom/b/a/a/c/aq;->b:Z

    .line 88
    iget-object v0, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iput-boolean p3, v0, Lcom/b/a/a/c/ap;->b:Z

    .line 89
    return-void

    :cond_3
    move v0, v1

    .line 82
    goto :goto_0
.end method

.method static synthetic a(Lcom/b/a/a/c/ao;)V
    .locals 2

    .prologue
    .line 34
    sget-boolean v0, Lcom/b/a/a/c/ao;->k:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    iget-boolean v0, v0, Lcom/b/a/a/c/aq;->b:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    iget-boolean v0, v0, Lcom/b/a/a/c/aq;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v0, v0, Lcom/b/a/a/c/ap;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v0, v0, Lcom/b/a/a/c/ap;->a:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/b/a/a/c/ao;->a()Z

    move-result v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/b/a/a/c/a;->l:Lcom/b/a/a/c/a;

    invoke-virtual {p0, v0}, Lcom/b/a/a/c/ao;->a(Lcom/b/a/a/c/a;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v1, p0, Lcom/b/a/a/c/ao;->c:I

    invoke-virtual {v0, v1}, Lcom/b/a/a/c/ac;->b(I)Lcom/b/a/a/c/ao;

    goto :goto_1
.end method

.method static synthetic b(Lcom/b/a/a/c/ao;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v0, v0, Lcom/b/a/a/c/ap;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v0, v0, Lcom/b/a/a/c/ap;->b:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stream was reset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Lcom/b/a/a/c/a;)V
    .locals 2

    .prologue
    .line 219
    invoke-virtual {p0, p1}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/a;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v1, p0, Lcom/b/a/a/c/ao;->c:I

    iget-object v0, v0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v0, v1, p1}, Lcom/b/a/a/c/d;->a(ILcom/b/a/a/c/a;)V

    goto :goto_0
.end method

.method public final declared-synchronized a()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 107
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 110
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    iget-boolean v1, v1, Lcom/b/a/a/c/aq;->b:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    iget-boolean v1, v1, Lcom/b/a/a/c/aq;->a:Z

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v1, v1, Lcom/b/a/a/c/ap;->b:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v1, v1, Lcom/b/a/a/c/ap;->a:Z

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/b/a/a/c/ao;->e:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    .line 115
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 120
    iget v0, p0, Lcom/b/a/a/c/ao;->c:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    .line 121
    :goto_0
    iget-object v3, p0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget-boolean v3, v3, Lcom/b/a/a/c/ac;->c:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 121
    goto :goto_1
.end method

.method public b(Lcom/b/a/a/c/a;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 238
    sget-boolean v1, Lcom/b/a/a/c/ao;->k:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 239
    :cond_0
    monitor-enter p0

    .line 240
    :try_start_0
    iget-object v1, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    if-eqz v1, :cond_1

    .line 241
    monitor-exit p0

    .line 250
    :goto_0
    return v0

    .line 243
    :cond_1
    iget-object v1, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    iget-boolean v1, v1, Lcom/b/a/a/c/aq;->b:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    iget-boolean v1, v1, Lcom/b/a/a/c/ap;->b:Z

    if-eqz v1, :cond_2

    .line 244
    monitor-exit p0

    goto :goto_0

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 246
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    .line 247
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 248
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 249
    iget-object v0, p0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v1, p0, Lcom/b/a/a/c/ao;->c:I

    invoke-virtual {v0, v1}, Lcom/b/a/a/c/ac;->b(I)Lcom/b/a/a/c/ao;

    .line 250
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized c()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 137
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    invoke-virtual {v0}, Lcom/b/a/a/c/ar;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 139
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/b/a/a/c/ao;->e:Ljava/util/List;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_0

    .line 140
    :try_start_2
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 143
    :catchall_0
    move-exception v0

    :try_start_4
    iget-object v2, p0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    iget-boolean v3, v2, Lc/a;->b:Z

    if-nez v3, :cond_2

    :goto_1
    if-eqz v1, :cond_3

    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 137
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 143
    :cond_0
    :try_start_5
    iget-object v0, p0, Lcom/b/a/a/c/ao;->h:Lcom/b/a/a/c/ar;

    iget-boolean v2, v0, Lc/a;->b:Z

    if-nez v2, :cond_1

    :goto_2
    if-eqz v1, :cond_4

    new-instance v0, Ljava/io/InterruptedIOException;

    const-string v1, "timeout"

    invoke-direct {v0, v1}, Ljava/io/InterruptedIOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, v0, Lc/a;->b:Z

    invoke-static {v0}, Lc/a;->a(Lc/a;)Z

    move-result v1

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, v2, Lc/a;->b:Z

    invoke-static {v2}, Lc/a;->a(Lc/a;)Z

    move-result v1

    goto :goto_1

    :cond_3
    throw v0

    .line 145
    :cond_4
    iget-object v0, p0, Lcom/b/a/a/c/ao;->e:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/b/a/a/c/ao;->e:Ljava/util/List;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    monitor-exit p0

    return-object v0

    .line 146
    :cond_5
    :try_start_6
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "stream was reset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1
.end method

.method final declared-synchronized c(Lcom/b/a/a/c/a;)V
    .locals 1

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    if-nez v0, :cond_0

    .line 304
    iput-object p1, p0, Lcom/b/a/a/c/ao;->j:Lcom/b/a/a/c/a;

    .line 305
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    :cond_0
    monitor-exit p0

    return-void

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Lc/z;
    .locals 2

    .prologue
    .line 206
    monitor-enter p0

    .line 207
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ao;->e:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/b/a/a/c/ao;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "reply before requesting the sink"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    iget-object v0, p0, Lcom/b/a/a/c/ao;->g:Lcom/b/a/a/c/ap;

    return-object v0
.end method

.method final e()V
    .locals 2

    .prologue
    .line 290
    sget-boolean v0, Lcom/b/a/a/c/ao;->k:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 292
    :cond_0
    monitor-enter p0

    .line 293
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ao;->f:Lcom/b/a/a/c/aq;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/b/a/a/c/aq;->b:Z

    .line 294
    invoke-virtual {p0}, Lcom/b/a/a/c/ao;->a()Z

    move-result v0

    .line 295
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 296
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    if-nez v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v1, p0, Lcom/b/a/a/c/ao;->c:I

    invoke-virtual {v0, v1}, Lcom/b/a/a/c/ac;->b(I)Lcom/b/a/a/c/ao;

    .line 300
    :cond_1
    return-void

    .line 296
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
