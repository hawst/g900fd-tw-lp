.class public Lcom/b/a/a/b/b;
.super Ljava/net/HttpURLConnection;
.source "PG"


# instance fields
.field final a:Lcom/b/a/p;

.field public b:Ljava/io/IOException;

.field public c:Lcom/b/a/a/a/o;

.field d:Lcom/b/a/k;

.field private e:Lcom/b/a/m;

.field private f:J

.field private g:I

.field private h:Lcom/b/a/l;

.field private i:Lcom/b/a/aa;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/b/a/p;)V
    .locals 2

    .prologue
    .line 97
    invoke-direct {p0, p1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    .line 74
    new-instance v0, Lcom/b/a/m;

    invoke-direct {v0}, Lcom/b/a/m;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    .line 77
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/b/a/a/b/b;->f:J

    .line 98
    iput-object p2, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    .line 99
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/b/a/f;Lcom/b/a/a/a/v;Lcom/b/a/x;)Lcom/b/a/a/a/o;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 317
    new-instance v0, Lcom/b/a/v;

    invoke-direct {v0}, Lcom/b/a/v;-><init>()V

    invoke-virtual {p0}, Lcom/b/a/a/b/b;->getURL()Ljava/net/URL;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v1, v0, Lcom/b/a/v;->b:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/b/a/v;->a:Ljava/lang/String;

    invoke-virtual {v0, p1, v5}, Lcom/b/a/v;->a(Ljava/lang/String;Lcom/b/a/w;)Lcom/b/a/v;

    move-result-object v4

    .line 320
    iget-object v0, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    new-instance v6, Lcom/b/a/l;

    invoke-direct {v6, v0}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    move v1, v2

    .line 321
    :goto_0
    iget-object v0, v6, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    if-ge v1, v0, :cond_5

    .line 322
    shl-int/lit8 v0, v1, 0x1

    if-ltz v0, :cond_1

    iget-object v3, v6, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v3, v3

    if-lt v0, v3, :cond_3

    :cond_1
    move-object v3, v5

    :goto_1
    shl-int/lit8 v0, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    if-ltz v0, :cond_2

    iget-object v7, v6, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v7, v7

    if-lt v0, v7, :cond_4

    :cond_2
    move-object v0, v5

    :goto_2
    iget-object v7, v4, Lcom/b/a/v;->d:Lcom/b/a/m;

    invoke-virtual {v7, v3, v0}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    .line 321
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 322
    :cond_3
    iget-object v3, v6, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v0, v3, v0

    move-object v3, v0

    goto :goto_1

    :cond_4
    iget-object v7, v6, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v0, v7, v0

    goto :goto_2

    .line 326
    :cond_5
    invoke-static {p1}, Lcom/b/a/a/a/q;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 328
    iget-wide v0, p0, Lcom/b/a/a/b/b;->f:J

    const-wide/16 v8, -0x1

    cmp-long v0, v0, v8

    if-eqz v0, :cond_8

    .line 329
    const-string v0, "Content-Length"

    iget-wide v8, p0, Lcom/b/a/a/b/b;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    .line 337
    :goto_3
    const-string v0, "Content-Type"

    invoke-virtual {v6, v0}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    .line 338
    const-string v0, "Content-Type"

    const-string v1, "application/x-www-form-urlencoded"

    invoke-virtual {v4, v0, v1}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_6
    move v3, v2

    .line 342
    const-string v0, "User-Agent"

    invoke-virtual {v6, v0}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    .line 343
    const-string v1, "User-Agent"

    const-string v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_a

    :goto_4
    invoke-virtual {v4, v1, v0}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    .line 346
    :cond_7
    iget-object v0, v4, Lcom/b/a/v;->a:Ljava/lang/String;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_8
    iget v0, p0, Lcom/b/a/a/b/b;->chunkLength:I

    if-lez v0, :cond_9

    .line 331
    const-string v0, "Transfer-Encoding"

    const-string v1, "chunked"

    invoke-virtual {v4, v0, v1}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    goto :goto_3

    .line 333
    :cond_9
    const/4 v2, 0x1

    goto :goto_3

    .line 343
    :cond_a
    const-string v2, "Java"

    const-string v0, "java.version"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_b

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    .line 346
    :cond_c
    new-instance v2, Lcom/b/a/u;

    invoke-direct {v2, v4}, Lcom/b/a/u;-><init>(Lcom/b/a/v;)V

    .line 349
    iget-object v1, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    .line 350
    sget-object v0, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    invoke-virtual {v0, v1}, Lcom/b/a/a/a;->a(Lcom/b/a/p;)Lcom/b/a/a/b;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-virtual {p0}, Lcom/b/a/a/b/b;->getUseCaches()Z

    move-result v0

    if-nez v0, :cond_d

    .line 351
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    invoke-virtual {v0}, Lcom/b/a/p;->b()Lcom/b/a/p;

    move-result-object v1

    iput-object v5, v1, Lcom/b/a/p;->g:Lcom/b/a/c;

    iput-object v5, v1, Lcom/b/a/p;->f:Lcom/b/a/a/b;

    .line 354
    :cond_d
    new-instance v0, Lcom/b/a/a/a/o;

    move-object v4, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/b/a/a/a/o;-><init>(Lcom/b/a/p;Lcom/b/a/u;ZLcom/b/a/f;Lcom/b/a/a/a/w;Lcom/b/a/a/a/v;Lcom/b/a/x;)V

    return-object v0
.end method

.method private a()Lcom/b/a/l;
    .locals 5

    .prologue
    .line 139
    iget-object v0, p0, Lcom/b/a/a/b/b;->h:Lcom/b/a/l;

    if-nez v0, :cond_1

    .line 140
    invoke-direct {p0}, Lcom/b/a/a/b/b;->c()Lcom/b/a/a/a/o;

    move-result-object v0

    iget-object v1, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    .line 141
    iget-object v1, v0, Lcom/b/a/x;->f:Lcom/b/a/l;

    .line 143
    new-instance v2, Lcom/b/a/m;

    invoke-direct {v2}, Lcom/b/a/m;-><init>()V

    iget-object v3, v2, Lcom/b/a/m;->a:Ljava/util/List;

    iget-object v1, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    invoke-static {}, Lcom/b/a/a/d;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "-Response-Source"

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lcom/b/a/x;->h:Lcom/b/a/x;

    if-nez v3, :cond_3

    iget-object v3, v0, Lcom/b/a/x;->i:Lcom/b/a/x;

    if-nez v3, :cond_2

    const-string v0, "NONE"

    :goto_0
    invoke-virtual {v2, v1, v0}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    move-result-object v0

    new-instance v1, Lcom/b/a/l;

    invoke-direct {v1, v0}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    iput-object v1, p0, Lcom/b/a/a/b/b;->h:Lcom/b/a/l;

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/b/b;->h:Lcom/b/a/l;

    return-object v0

    .line 143
    :cond_2
    iget v0, v0, Lcom/b/a/x;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x11

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "CACHE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v3, v0, Lcom/b/a/x;->i:Lcom/b/a/x;

    if-nez v3, :cond_4

    iget v0, v0, Lcom/b/a/x;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x13

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "NETWORK "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    iget-object v0, v0, Lcom/b/a/x;->h:Lcom/b/a/x;

    iget v0, v0, Lcom/b/a/x;->c:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "CONDITIONAL_CACHE "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 5

    .prologue
    .line 540
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 541
    if-eqz p2, :cond_0

    .line 542
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iget-object v0, v0, Lcom/b/a/p;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 544
    :cond_0
    const-string v0, ","

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 546
    :try_start_0
    invoke-static {v4}, Lcom/b/a/t;->a(Ljava/lang/String;)Lcom/b/a/t;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 547
    :catch_0
    move-exception v0

    .line 548
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 551
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/b/a/t;->b:Lcom/b/a/t;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "protocols doesn\'t contain http/1.1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "protocols must not contain null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/b/a/p;->c:Ljava/util/List;

    .line 552
    return-void
.end method

.method private a(Z)Z
    .locals 20

    .prologue
    .line 425
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v2, v11, Lcom/b/a/a/a/o;->u:Lcom/b/a/a/a/c;

    if-nez v2, :cond_2b

    iget-object v2, v11, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :catch_0
    move-exception v2

    .line 436
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    invoke-virtual {v3, v2}, Lcom/b/a/a/a/o;->a(Ljava/io/IOException;)Lcom/b/a/a/a/o;

    move-result-object v3

    .line 437
    if-eqz v3, :cond_48

    .line 438
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    .line 439
    const/4 v2, 0x0

    :goto_0
    return v2

    .line 425
    :cond_0
    :try_start_1
    iget-object v2, v11, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    new-instance v3, Lcom/b/a/v;

    invoke-direct {v3, v2}, Lcom/b/a/v;-><init>(Lcom/b/a/u;)V

    const-string v4, "Host"

    invoke-virtual {v2, v4}, Lcom/b/a/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const-string v4, "Host"

    invoke-virtual {v2}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v5

    invoke-static {v5}, Lcom/b/a/a/a/o;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_1
    iget-object v4, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v4, :cond_2

    iget-object v4, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v4, v4, Lcom/b/a/f;->g:Lcom/b/a/t;

    sget-object v5, Lcom/b/a/t;->a:Lcom/b/a/t;

    if-eq v4, v5, :cond_3

    :cond_2
    const-string v4, "Connection"

    invoke-virtual {v2, v4}, Lcom/b/a/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_3

    const-string v4, "Connection"

    const-string v5, "Keep-Alive"

    invoke-virtual {v3, v4, v5}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_3
    const-string v4, "Accept-Encoding"

    invoke-virtual {v2, v4}, Lcom/b/a/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    iput-boolean v4, v11, Lcom/b/a/a/a/o;->i:Z

    const-string v4, "Accept-Encoding"

    const-string v5, "gzip"

    invoke-virtual {v3, v4, v5}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_4
    iget-object v4, v11, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-object v4, v4, Lcom/b/a/p;->e:Ljava/net/CookieHandler;

    if-eqz v4, :cond_6

    iget-object v5, v3, Lcom/b/a/v;->a:Ljava/lang/String;

    if-nez v5, :cond_5

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "url == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_5
    new-instance v5, Lcom/b/a/u;

    invoke-direct {v5, v3}, Lcom/b/a/u;-><init>(Lcom/b/a/v;)V

    iget-object v5, v5, Lcom/b/a/u;->c:Lcom/b/a/l;

    const/4 v6, 0x0

    invoke-static {v5, v6}, Lcom/b/a/a/a/s;->a(Lcom/b/a/l;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    invoke-virtual {v2}, Lcom/b/a/u;->b()Ljava/net/URI;

    move-result-object v6

    invoke-virtual {v4, v6, v5}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/b/a/a/a/s;->a(Lcom/b/a/v;Ljava/util/Map;)V

    :cond_6
    const-string v4, "User-Agent"

    invoke-virtual {v2, v4}, Lcom/b/a/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_7

    const-string v2, "User-Agent"

    const-string v4, "okhttp/2.0.1-SNAPSHOT"

    invoke-virtual {v3, v2, v4}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_7
    iget-object v2, v3, Lcom/b/a/v;->a:Ljava/lang/String;

    if-nez v2, :cond_8

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "url == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    new-instance v12, Lcom/b/a/u;

    invoke-direct {v12, v3}, Lcom/b/a/u;-><init>(Lcom/b/a/v;)V

    sget-object v2, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v3, v11, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    invoke-virtual {v2, v3}, Lcom/b/a/a/a;->a(Lcom/b/a/p;)Lcom/b/a/a/b;

    move-result-object v9

    if-eqz v9, :cond_c

    invoke-interface {v9}, Lcom/b/a/a/b;->a()Lcom/b/a/x;

    move-result-object v2

    move-object v8, v2

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v10, Lcom/b/a/a/a/d;

    invoke-direct {v10, v2, v3, v12, v8}, Lcom/b/a/a/a/d;-><init>(JLcom/b/a/u;Lcom/b/a/x;)V

    iget-object v2, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    if-nez v2, :cond_d

    new-instance v2, Lcom/b/a/a/a/c;

    iget-object v3, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    :goto_2
    iget-object v3, v2, Lcom/b/a/a/a/c;->a:Lcom/b/a/u;

    if-eqz v3, :cond_9

    iget-object v3, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    invoke-virtual {v3}, Lcom/b/a/u;->c()Lcom/b/a/d;

    move-result-object v3

    iget-boolean v3, v3, Lcom/b/a/d;->i:Z

    if-eqz v3, :cond_9

    new-instance v2, Lcom/b/a/a/a/c;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    :cond_9
    iput-object v2, v11, Lcom/b/a/a/a/o;->u:Lcom/b/a/a/a/c;

    iget-object v2, v11, Lcom/b/a/a/a/o;->u:Lcom/b/a/a/a/c;

    iget-object v2, v2, Lcom/b/a/a/a/c;->a:Lcom/b/a/u;

    iput-object v2, v11, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    iget-object v2, v11, Lcom/b/a/a/a/o;->u:Lcom/b/a/a/a/c;

    iget-object v2, v2, Lcom/b/a/a/a/c;->b:Lcom/b/a/x;

    iput-object v2, v11, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    if-eqz v9, :cond_a

    iget-object v2, v11, Lcom/b/a/a/a/o;->u:Lcom/b/a/a/a/c;

    :cond_a
    if-eqz v8, :cond_b

    iget-object v2, v11, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    if-nez v2, :cond_b

    iget-object v2, v8, Lcom/b/a/x;->g:Lcom/b/a/z;

    invoke-static {v2}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    :cond_b
    iget-object v2, v11, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    if-eqz v2, :cond_2c

    iget-object v2, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-nez v2, :cond_2a

    iget-object v13, v11, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    iget-object v2, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v2, :cond_25

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_c
    const/4 v2, 0x0

    move-object v8, v2

    goto :goto_1

    :cond_d
    iget-object v2, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    invoke-virtual {v2}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    const-string v3, "https"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->e:Lcom/b/a/k;

    if-nez v2, :cond_e

    new-instance v2, Lcom/b/a/a/a/c;

    iget-object v3, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    goto :goto_2

    :cond_e
    iget-object v2, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    iget-object v3, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    invoke-static {v2, v3}, Lcom/b/a/a/a/c;->a(Lcom/b/a/x;Lcom/b/a/u;)Z

    move-result v2

    if-nez v2, :cond_f

    new-instance v2, Lcom/b/a/a/a/c;

    iget-object v3, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    goto :goto_2

    :cond_f
    iget-object v2, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    invoke-virtual {v2}, Lcom/b/a/u;->c()Lcom/b/a/d;

    move-result-object v13

    iget-boolean v2, v13, Lcom/b/a/d;->a:Z

    if-nez v2, :cond_10

    iget-object v2, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    invoke-static {v2}, Lcom/b/a/a/a/d;->a(Lcom/b/a/u;)Z

    move-result v2

    if-eqz v2, :cond_11

    :cond_10
    new-instance v2, Lcom/b/a/a/a/c;

    iget-object v3, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    goto/16 :goto_2

    :cond_11
    iget-object v2, v10, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    if-eqz v2, :cond_17

    const-wide/16 v2, 0x0

    iget-wide v4, v10, Lcom/b/a/a/a/d;->j:J

    iget-object v6, v10, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    :goto_3
    iget v4, v10, Lcom/b/a/a/a/d;->l:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_12

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v5, v10, Lcom/b/a/a/a/d;->l:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    :cond_12
    iget-wide v4, v10, Lcom/b/a/a/a/d;->j:J

    iget-wide v6, v10, Lcom/b/a/a/a/d;->i:J

    sub-long/2addr v4, v6

    iget-wide v6, v10, Lcom/b/a/a/a/d;->a:J

    iget-wide v14, v10, Lcom/b/a/a/a/d;->j:J

    sub-long/2addr v6, v14

    add-long/2addr v2, v4

    add-long v14, v2, v6

    iget-object v2, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    invoke-virtual {v2}, Lcom/b/a/x;->b()Lcom/b/a/d;

    move-result-object v2

    iget v3, v2, Lcom/b/a/d;->c:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_18

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v2, v2, Lcom/b/a/d;->c:I

    int-to-long v4, v2

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    :cond_13
    :goto_4
    iget v4, v13, Lcom/b/a/d;->c:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_4a

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v5, v13, Lcom/b/a/d;->c:I

    int-to-long v6, v5

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    move-wide v6, v2

    :goto_5
    const-wide/16 v2, 0x0

    iget v4, v13, Lcom/b/a/d;->h:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_49

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, v13, Lcom/b/a/d;->h:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    move-wide v4, v2

    :goto_6
    const-wide/16 v2, 0x0

    iget-object v0, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/b/a/x;->b()Lcom/b/a/d;

    move-result-object v16

    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/b/a/d;->f:Z

    move/from16 v17, v0

    if-nez v17, :cond_14

    iget v0, v13, Lcom/b/a/d;->g:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_14

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, v13, Lcom/b/a/d;->g:I

    int-to-long v0, v3

    move-wide/from16 v18, v0

    move-wide/from16 v0, v18

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    :cond_14
    move-object/from16 v0, v16

    iget-boolean v13, v0, Lcom/b/a/d;->a:Z

    if-nez v13, :cond_1f

    add-long v16, v14, v4

    add-long/2addr v2, v6

    cmp-long v2, v16, v2

    if-gez v2, :cond_1f

    iget-object v2, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    new-instance v3, Lcom/b/a/y;

    invoke-direct {v3, v2}, Lcom/b/a/y;-><init>(Lcom/b/a/x;)V

    add-long/2addr v4, v14

    cmp-long v2, v4, v6

    if-ltz v2, :cond_15

    const-string v2, "Warning"

    const-string v4, "110 HttpURLConnection \"Response is stale\""

    iget-object v5, v3, Lcom/b/a/y;->f:Lcom/b/a/m;

    invoke-virtual {v5, v2, v4}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    :cond_15
    const-wide/32 v4, 0x5265c00

    cmp-long v2, v14, v4

    if-lez v2, :cond_16

    iget-object v2, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    invoke-virtual {v2}, Lcom/b/a/x;->b()Lcom/b/a/d;

    move-result-object v2

    iget v2, v2, Lcom/b/a/d;->c:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1e

    iget-object v2, v10, Lcom/b/a/a/a/d;->h:Ljava/util/Date;

    if-nez v2, :cond_1e

    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_16

    const-string v2, "Warning"

    const-string v4, "113 HttpURLConnection \"Heuristic expiration\""

    iget-object v5, v3, Lcom/b/a/y;->f:Lcom/b/a/m;

    invoke-virtual {v5, v2, v4}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    :cond_16
    new-instance v2, Lcom/b/a/a/a/c;

    const/4 v4, 0x0

    invoke-virtual {v3}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v3

    invoke-direct {v2, v4, v3}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    goto/16 :goto_2

    :cond_17
    const-wide/16 v2, 0x0

    goto/16 :goto_3

    :cond_18
    iget-object v2, v10, Lcom/b/a/a/a/d;->h:Ljava/util/Date;

    if-eqz v2, :cond_1a

    iget-object v2, v10, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    if-eqz v2, :cond_19

    iget-object v2, v10, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    :goto_8
    iget-object v4, v10, Lcom/b/a/a/a/d;->h:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v2, v4, v2

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_13

    const-wide/16 v2, 0x0

    goto/16 :goto_4

    :cond_19
    iget-wide v2, v10, Lcom/b/a/a/a/d;->j:J

    goto :goto_8

    :cond_1a
    iget-object v2, v10, Lcom/b/a/a/a/d;->f:Ljava/util/Date;

    if-eqz v2, :cond_1d

    iget-object v2, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->a:Lcom/b/a/u;

    invoke-virtual {v2}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1d

    iget-object v2, v10, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    if-eqz v2, :cond_1b

    iget-object v2, v10, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    :goto_9
    iget-object v4, v10, Lcom/b/a/a/a/d;->f:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_1c

    const-wide/16 v4, 0xa

    div-long/2addr v2, v4

    goto/16 :goto_4

    :cond_1b
    iget-wide v2, v10, Lcom/b/a/a/a/d;->i:J

    goto :goto_9

    :cond_1c
    const-wide/16 v2, 0x0

    goto/16 :goto_4

    :cond_1d
    const-wide/16 v2, 0x0

    goto/16 :goto_4

    :cond_1e
    const/4 v2, 0x0

    goto :goto_7

    :cond_1f
    iget-object v2, v10, Lcom/b/a/a/a/d;->b:Lcom/b/a/u;

    new-instance v3, Lcom/b/a/v;

    invoke-direct {v3, v2}, Lcom/b/a/v;-><init>(Lcom/b/a/u;)V

    iget-object v2, v10, Lcom/b/a/a/a/d;->f:Ljava/util/Date;

    if-eqz v2, :cond_22

    const-string v2, "If-Modified-Since"

    iget-object v4, v10, Lcom/b/a/a/a/d;->g:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_20
    :goto_a
    iget-object v2, v10, Lcom/b/a/a/a/d;->k:Ljava/lang/String;

    if-eqz v2, :cond_21

    const-string v2, "If-None-Match"

    iget-object v4, v10, Lcom/b/a/a/a/d;->k:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_21
    iget-object v2, v3, Lcom/b/a/v;->a:Ljava/lang/String;

    if-nez v2, :cond_23

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "url == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_22
    iget-object v2, v10, Lcom/b/a/a/a/d;->d:Ljava/util/Date;

    if-eqz v2, :cond_20

    const-string v2, "If-Modified-Since"

    iget-object v4, v10, Lcom/b/a/a/a/d;->e:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    goto :goto_a

    :cond_23
    new-instance v4, Lcom/b/a/u;

    invoke-direct {v4, v3}, Lcom/b/a/u;-><init>(Lcom/b/a/v;)V

    invoke-static {v4}, Lcom/b/a/a/a/d;->a(Lcom/b/a/u;)Z

    move-result v2

    if-eqz v2, :cond_24

    new-instance v2, Lcom/b/a/a/a/c;

    iget-object v3, v10, Lcom/b/a/a/a/d;->c:Lcom/b/a/x;

    invoke-direct {v2, v4, v3}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    goto/16 :goto_2

    :cond_24
    new-instance v2, Lcom/b/a/a/a/c;

    const/4 v3, 0x0

    invoke-direct {v2, v4, v3}, Lcom/b/a/a/a/c;-><init>(Lcom/b/a/u;Lcom/b/a/x;)V

    goto/16 :goto_2

    :cond_25
    iget-object v2, v11, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    if-nez v2, :cond_29

    iget-object v14, v11, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    invoke-virtual {v13}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_26

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_27

    :cond_26
    new-instance v2, Ljava/net/UnknownHostException;

    invoke-virtual {v13}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_27
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v13}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    const-string v4, "https"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    iget-object v6, v14, Lcom/b/a/p;->i:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v7, v14, Lcom/b/a/p;->j:Ljavax/net/ssl/HostnameVerifier;

    :cond_28
    new-instance v2, Lcom/b/a/a;

    invoke-virtual {v13}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v4

    invoke-static {v4}, Lcom/b/a/a/i;->a(Ljava/net/URL;)I

    move-result v4

    iget-object v5, v14, Lcom/b/a/p;->h:Ljavax/net/SocketFactory;

    iget-object v8, v14, Lcom/b/a/p;->k:Lcom/b/a/b;

    iget-object v9, v14, Lcom/b/a/p;->b:Ljava/net/Proxy;

    iget-object v10, v14, Lcom/b/a/p;->c:Ljava/util/List;

    invoke-direct/range {v2 .. v10}, Lcom/b/a/a;-><init>(Ljava/lang/String;ILjavax/net/SocketFactory;Ljavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lcom/b/a/b;Ljava/net/Proxy;Ljava/util/List;)V

    new-instance v3, Lcom/b/a/a/a/w;

    invoke-virtual {v13}, Lcom/b/a/u;->b()Ljava/net/URI;

    move-result-object v4

    invoke-direct {v3, v2, v4, v14, v13}, Lcom/b/a/a/a/w;-><init>(Lcom/b/a/a;Ljava/net/URI;Lcom/b/a/p;Lcom/b/a/u;)V

    iput-object v3, v11, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    :cond_29
    iget-object v2, v11, Lcom/b/a/a/a/o;->d:Lcom/b/a/a/a/w;

    invoke-virtual {v2}, Lcom/b/a/a/a/w;->a()Lcom/b/a/f;

    move-result-object v3

    sget-object v4, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v5, v2, Lcom/b/a/a/a/w;->c:Lcom/b/a/p;

    iget-object v2, v2, Lcom/b/a/a/a/w;->f:Lcom/b/a/u;

    invoke-virtual {v4, v5, v3, v11, v2}, Lcom/b/a/a/a;->a(Lcom/b/a/p;Lcom/b/a/f;Lcom/b/a/a/a/o;Lcom/b/a/u;)V

    iput-object v3, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v2, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v2, v2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iput-object v2, v11, Lcom/b/a/a/a/o;->e:Lcom/b/a/aa;

    :cond_2a
    sget-object v2, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v3, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    invoke-virtual {v2, v3, v11}, Lcom/b/a/a/a;->a(Lcom/b/a/f;Lcom/b/a/a/a/o;)Lcom/b/a/a/a/aa;

    move-result-object v2

    iput-object v2, v11, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-virtual {v11}, Lcom/b/a/a/a/o;->a()Z

    move-result v2

    if-eqz v2, :cond_2b

    iget-object v2, v11, Lcom/b/a/a/a/o;->p:Lc/z;

    if-nez v2, :cond_2b

    iget-object v2, v11, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v2, v12}, Lcom/b/a/a/a/aa;->a(Lcom/b/a/u;)Lc/z;

    move-result-object v2

    iput-object v2, v11, Lcom/b/a/a/a/o;->p:Lc/z;

    .line 426
    :cond_2b
    :goto_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v2, v2, Lcom/b/a/a/a/o;->e:Lcom/b/a/aa;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/b/a/a/b/b;->i:Lcom/b/a/aa;

    .line 427
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v2, v2, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v2, :cond_32

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v2, v2, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v2, v2, Lcom/b/a/f;->i:Lcom/b/a/k;

    :goto_c
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/b/a/a/b/b;->d:Lcom/b/a/k;

    .line 430
    if-eqz p1, :cond_41

    .line 431
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v2, v3, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v2, :cond_41

    iget-object v2, v3, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    if-nez v2, :cond_33

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    if-nez v2, :cond_33

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "call sendRequest() first!"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 425
    :cond_2c
    iget-object v2, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v2, :cond_2d

    sget-object v2, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v3, v11, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-object v3, v3, Lcom/b/a/p;->l:Lcom/b/a/g;

    iget-object v4, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    invoke-virtual {v2, v3, v4}, Lcom/b/a/a/a;->a(Lcom/b/a/g;Lcom/b/a/f;)V

    const/4 v2, 0x0

    iput-object v2, v11, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    :cond_2d
    iget-object v2, v11, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    if-eqz v2, :cond_30

    iget-object v2, v11, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    new-instance v3, Lcom/b/a/y;

    invoke-direct {v3, v2}, Lcom/b/a/y;-><init>(Lcom/b/a/x;)V

    iget-object v2, v11, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iput-object v2, v3, Lcom/b/a/y;->a:Lcom/b/a/u;

    iget-object v2, v11, Lcom/b/a/a/a/o;->f:Lcom/b/a/x;

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v2

    if-eqz v2, :cond_2e

    iget-object v4, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v4, :cond_2e

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "priorResponse.body != null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2e
    iput-object v2, v3, Lcom/b/a/y;->j:Lcom/b/a/x;

    iget-object v2, v11, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v2

    if-eqz v2, :cond_2f

    const-string v4, "cacheResponse"

    invoke-static {v4, v2}, Lcom/b/a/y;->a(Ljava/lang/String;Lcom/b/a/x;)V

    :cond_2f
    iput-object v2, v3, Lcom/b/a/y;->i:Lcom/b/a/x;

    invoke-virtual {v3}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v2

    iput-object v2, v11, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    :goto_d
    iget-object v2, v11, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v2, :cond_2b

    iget-object v2, v11, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    invoke-virtual {v2}, Lcom/b/a/z;->a()Lc/j;

    move-result-object v2

    invoke-virtual {v11, v2}, Lcom/b/a/a/a/o;->a(Lc/aa;)V

    goto/16 :goto_b

    :cond_30
    new-instance v2, Lcom/b/a/y;

    invoke-direct {v2}, Lcom/b/a/y;-><init>()V

    iget-object v3, v11, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iput-object v3, v2, Lcom/b/a/y;->a:Lcom/b/a/u;

    iget-object v3, v11, Lcom/b/a/a/a/o;->f:Lcom/b/a/x;

    invoke-static {v3}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v3

    if-eqz v3, :cond_31

    iget-object v4, v3, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v4, :cond_31

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "priorResponse.body != null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_31
    iput-object v3, v2, Lcom/b/a/y;->j:Lcom/b/a/x;

    sget-object v3, Lcom/b/a/t;->b:Lcom/b/a/t;

    iput-object v3, v2, Lcom/b/a/y;->b:Lcom/b/a/t;

    const/16 v3, 0x1f8

    iput v3, v2, Lcom/b/a/y;->c:I

    const-string v3, "Unsatisfiable Request (only-if-cached)"

    iput-object v3, v2, Lcom/b/a/y;->d:Ljava/lang/String;

    sget-object v3, Lcom/b/a/a/a/o;->a:Lcom/b/a/z;

    iput-object v3, v2, Lcom/b/a/y;->g:Lcom/b/a/z;

    invoke-virtual {v2}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v2

    iput-object v2, v11, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    goto :goto_d

    .line 427
    :cond_32
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 431
    :cond_33
    iget-object v2, v3, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    if-eqz v2, :cond_41

    iget-object v2, v3, Lcom/b/a/a/a/o;->q:Lc/i;

    if-eqz v2, :cond_34

    iget-object v2, v3, Lcom/b/a/a/a/o;->q:Lc/i;

    invoke-interface {v2}, Lc/i;->c()Lc/f;

    move-result-object v2

    iget-wide v4, v2, Lc/f;->b:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_34

    iget-object v2, v3, Lcom/b/a/a/a/o;->q:Lc/i;

    invoke-interface {v2}, Lc/i;->b()V

    :cond_34
    iget-wide v4, v3, Lcom/b/a/a/a/o;->h:J

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_37

    iget-object v2, v3, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    invoke-static {v2}, Lcom/b/a/a/a/s;->a(Lcom/b/a/u;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v2, v4, v6

    if-nez v2, :cond_36

    iget-object v2, v3, Lcom/b/a/a/a/o;->p:Lc/z;

    instance-of v2, v2, Lcom/b/a/a/a/v;

    if-eqz v2, :cond_36

    iget-object v2, v3, Lcom/b/a/a/a/o;->p:Lc/z;

    check-cast v2, Lcom/b/a/a/a/v;

    iget-object v2, v2, Lcom/b/a/a/a/v;->a:Lc/f;

    iget-wide v4, v2, Lc/f;->b:J

    iget-object v2, v3, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    new-instance v6, Lcom/b/a/v;

    invoke-direct {v6, v2}, Lcom/b/a/v;-><init>(Lcom/b/a/u;)V

    const-string v2, "Content-Length"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v2, v4}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    move-result-object v2

    iget-object v4, v2, Lcom/b/a/v;->a:Ljava/lang/String;

    if-nez v4, :cond_35

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "url == null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_35
    new-instance v4, Lcom/b/a/u;

    invoke-direct {v4, v2}, Lcom/b/a/u;-><init>(Lcom/b/a/v;)V

    iput-object v4, v3, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    :cond_36
    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    iget-object v4, v3, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    invoke-interface {v2, v4}, Lcom/b/a/a/a/aa;->b(Lcom/b/a/u;)V

    :cond_37
    iget-object v2, v3, Lcom/b/a/a/a/o;->p:Lc/z;

    if-eqz v2, :cond_38

    iget-object v2, v3, Lcom/b/a/a/a/o;->q:Lc/i;

    if-eqz v2, :cond_3a

    iget-object v2, v3, Lcom/b/a/a/a/o;->q:Lc/i;

    invoke-interface {v2}, Lc/i;->close()V

    :goto_e
    iget-object v2, v3, Lcom/b/a/a/a/o;->p:Lc/z;

    instance-of v2, v2, Lcom/b/a/a/a/v;

    if-eqz v2, :cond_38

    invoke-static {}, Lcom/b/a/a/i;->a()Lcom/b/a/a/a/v;

    move-result-object v2

    iget-object v4, v3, Lcom/b/a/a/a/o;->p:Lc/z;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_38

    iget-object v4, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    iget-object v2, v3, Lcom/b/a/a/a/o;->p:Lc/z;

    check-cast v2, Lcom/b/a/a/a/v;

    invoke-interface {v4, v2}, Lcom/b/a/a/a/aa;->a(Lcom/b/a/a/a/v;)V

    :cond_38
    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v2}, Lcom/b/a/a/a/aa;->a()V

    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v2}, Lcom/b/a/a/a/aa;->b()Lcom/b/a/y;

    move-result-object v2

    iget-object v4, v3, Lcom/b/a/a/a/o;->l:Lcom/b/a/u;

    iput-object v4, v2, Lcom/b/a/y;->a:Lcom/b/a/u;

    iget-object v4, v3, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v4, v4, Lcom/b/a/f;->i:Lcom/b/a/k;

    iput-object v4, v2, Lcom/b/a/y;->e:Lcom/b/a/k;

    sget-object v4, Lcom/b/a/a/a/s;->b:Ljava/lang/String;

    iget-wide v6, v3, Lcom/b/a/a/a/o;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/b/a/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/y;

    move-result-object v2

    sget-object v4, Lcom/b/a/a/a/s;->c:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/b/a/y;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/y;

    move-result-object v2

    invoke-virtual {v2}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v2

    iput-object v2, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    sget-object v2, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v4, v3, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    iget-object v5, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    iget-object v5, v5, Lcom/b/a/x;->b:Lcom/b/a/t;

    invoke-virtual {v2, v4, v5}, Lcom/b/a/a/a;->a(Lcom/b/a/f;Lcom/b/a/t;)V

    iget-object v2, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->f:Lcom/b/a/l;

    iget-object v4, v3, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-object v4, v4, Lcom/b/a/p;->e:Ljava/net/CookieHandler;

    if-eqz v4, :cond_39

    iget-object v5, v3, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    invoke-virtual {v5}, Lcom/b/a/u;->b()Ljava/net/URI;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v2, v6}, Lcom/b/a/a/a/s;->a(Lcom/b/a/l;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    :cond_39
    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    if-eqz v2, :cond_43

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    iget-object v4, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    iget v5, v4, Lcom/b/a/x;->c:I

    const/16 v6, 0x130

    if-ne v5, v6, :cond_3b

    const/4 v2, 0x1

    :goto_f
    if-eqz v2, :cond_42

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    new-instance v4, Lcom/b/a/y;

    invoke-direct {v4, v2}, Lcom/b/a/y;-><init>(Lcom/b/a/x;)V

    iget-object v2, v3, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iput-object v2, v4, Lcom/b/a/y;->a:Lcom/b/a/u;

    iget-object v2, v3, Lcom/b/a/a/a/o;->f:Lcom/b/a/x;

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v2

    if-eqz v2, :cond_3d

    iget-object v5, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v5, :cond_3d

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "priorResponse.body != null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3a
    iget-object v2, v3, Lcom/b/a/a/a/o;->p:Lc/z;

    invoke-interface {v2}, Lc/z;->close()V

    goto/16 :goto_e

    :cond_3b
    iget-object v2, v2, Lcom/b/a/x;->f:Lcom/b/a/l;

    const-string v5, "Last-Modified"

    invoke-virtual {v2, v5}, Lcom/b/a/l;->b(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    if-eqz v2, :cond_3c

    iget-object v4, v4, Lcom/b/a/x;->f:Lcom/b/a/l;

    const-string v5, "Last-Modified"

    invoke-virtual {v4, v5}, Lcom/b/a/l;->b(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    if-eqz v4, :cond_3c

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-gez v2, :cond_3c

    const/4 v2, 0x1

    goto :goto_f

    :cond_3c
    const/4 v2, 0x0

    goto :goto_f

    :cond_3d
    iput-object v2, v4, Lcom/b/a/y;->j:Lcom/b/a/x;

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->f:Lcom/b/a/l;

    iget-object v5, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    iget-object v5, v5, Lcom/b/a/x;->f:Lcom/b/a/l;

    invoke-static {v2, v5}, Lcom/b/a/a/a/o;->a(Lcom/b/a/l;Lcom/b/a/l;)Lcom/b/a/l;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/b/a/y;->a(Lcom/b/a/l;)Lcom/b/a/y;

    move-result-object v2

    iget-object v4, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    invoke-static {v4}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v4

    if-eqz v4, :cond_3e

    const-string v5, "cacheResponse"

    invoke-static {v5, v4}, Lcom/b/a/y;->a(Ljava/lang/String;Lcom/b/a/x;)V

    :cond_3e
    iput-object v4, v2, Lcom/b/a/y;->i:Lcom/b/a/x;

    iget-object v4, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    invoke-static {v4}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v4

    if-eqz v4, :cond_3f

    const-string v5, "networkResponse"

    invoke-static {v5, v4}, Lcom/b/a/y;->a(Ljava/lang/String;Lcom/b/a/x;)V

    :cond_3f
    iput-object v4, v2, Lcom/b/a/y;->h:Lcom/b/a/x;

    invoke-virtual {v2}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v2

    iput-object v2, v3, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v2}, Lcom/b/a/a/a/aa;->e()V

    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    if-eqz v2, :cond_40

    iget-object v2, v3, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v2, :cond_40

    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v2}, Lcom/b/a/a/a/aa;->c()V

    :cond_40
    const/4 v2, 0x0

    iput-object v2, v3, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    sget-object v2, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v4, v3, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    invoke-virtual {v2, v4}, Lcom/b/a/a/a;->a(Lcom/b/a/p;)Lcom/b/a/a/b;

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    iget-object v2, v3, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v2, :cond_41

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    invoke-virtual {v2}, Lcom/b/a/z;->a()Lc/j;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/b/a/a/a/o;->a(Lc/aa;)V

    .line 434
    :cond_41
    :goto_10
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 431
    :cond_42
    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    iget-object v2, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    invoke-static {v2}, Lcom/b/a/a/i;->a(Ljava/io/Closeable;)V

    :cond_43
    iget-object v2, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    new-instance v4, Lcom/b/a/y;

    invoke-direct {v4, v2}, Lcom/b/a/y;-><init>(Lcom/b/a/x;)V

    iget-object v2, v3, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iput-object v2, v4, Lcom/b/a/y;->a:Lcom/b/a/u;

    iget-object v2, v3, Lcom/b/a/a/a/o;->f:Lcom/b/a/x;

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v2

    if-eqz v2, :cond_44

    iget-object v5, v2, Lcom/b/a/x;->g:Lcom/b/a/z;

    if-eqz v5, :cond_44

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "priorResponse.body != null"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_44
    iput-object v2, v4, Lcom/b/a/y;->j:Lcom/b/a/x;

    iget-object v2, v3, Lcom/b/a/a/a/o;->m:Lcom/b/a/x;

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v2

    if-eqz v2, :cond_45

    const-string v5, "cacheResponse"

    invoke-static {v5, v2}, Lcom/b/a/y;->a(Ljava/lang/String;Lcom/b/a/x;)V

    :cond_45
    iput-object v2, v4, Lcom/b/a/y;->i:Lcom/b/a/x;

    iget-object v2, v3, Lcom/b/a/a/a/o;->n:Lcom/b/a/x;

    invoke-static {v2}, Lcom/b/a/a/a/o;->a(Lcom/b/a/x;)Lcom/b/a/x;

    move-result-object v2

    if-eqz v2, :cond_46

    const-string v5, "networkResponse"

    invoke-static {v5, v2}, Lcom/b/a/y;->a(Ljava/lang/String;Lcom/b/a/x;)V

    :cond_46
    iput-object v2, v4, Lcom/b/a/y;->h:Lcom/b/a/x;

    invoke-virtual {v4}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v2

    iput-object v2, v3, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    invoke-virtual {v3}, Lcom/b/a/a/a/o;->e()Z

    move-result v2

    if-nez v2, :cond_47

    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    iget-object v4, v3, Lcom/b/a/a/a/o;->t:Lcom/b/a/a/a/b;

    invoke-interface {v2, v4}, Lcom/b/a/a/a/aa;->a(Lcom/b/a/a/a/b;)Lc/aa;

    move-result-object v2

    iput-object v2, v3, Lcom/b/a/a/a/o;->r:Lc/aa;

    iget-object v2, v3, Lcom/b/a/a/a/o;->r:Lc/aa;

    invoke-static {v2}, Lc/p;->a(Lc/aa;)Lc/j;

    move-result-object v2

    iput-object v2, v3, Lcom/b/a/a/a/o;->s:Lc/j;

    goto :goto_10

    :cond_47
    invoke-virtual {v3}, Lcom/b/a/a/a/o;->c()V

    iget-object v2, v3, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    iget-object v4, v3, Lcom/b/a/a/a/o;->t:Lcom/b/a/a/a/b;

    invoke-interface {v2, v4}, Lcom/b/a/a/a/aa;->a(Lcom/b/a/a/a/b;)Lc/aa;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/b/a/a/a/o;->a(Lc/aa;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_10

    .line 443
    :cond_48
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/b/a/a/b/b;->b:Ljava/io/IOException;

    .line 444
    throw v2

    :cond_49
    move-wide v4, v2

    goto/16 :goto_6

    :cond_4a
    move-wide v6, v2

    goto/16 :goto_5
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 289
    iget-object v1, p0, Lcom/b/a/a/b/b;->b:Ljava/io/IOException;

    if-eqz v1, :cond_0

    .line 290
    iget-object v0, p0, Lcom/b/a/a/b/b;->b:Ljava/io/IOException;

    throw v0

    .line 291
    :cond_0
    iget-object v1, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    if-eqz v1, :cond_1

    .line 312
    :goto_0
    return-void

    .line 295
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/b/a/a/b/b;->connected:Z

    .line 297
    :try_start_0
    iget-boolean v1, p0, Lcom/b/a/a/b/b;->doOutput:Z

    if-eqz v1, :cond_2

    .line 298
    iget-object v1, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    const-string v2, "GET"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 300
    const-string v1, "POST"

    iput-object v1, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    .line 307
    :cond_2
    iget-boolean v1, p0, Lcom/b/a/a/b/b;->doOutput:Z

    if-eqz v1, :cond_3

    iget-wide v2, p0, Lcom/b/a/a/b/b;->f:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    invoke-static {}, Lcom/b/a/a/i;->a()Lcom/b/a/a/a/v;

    move-result-object v0

    .line 308
    :cond_3
    iget-object v1, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/b/a/a/b/b;->a(Ljava/lang/String;Lcom/b/a/f;Lcom/b/a/a/a/v;Lcom/b/a/x;)Lcom/b/a/a/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 309
    :catch_0
    move-exception v0

    .line 310
    iput-object v0, p0, Lcom/b/a/a/b/b;->b:Ljava/io/IOException;

    .line 311
    throw v0

    .line 301
    :cond_4
    :try_start_1
    iget-object v1, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    invoke-static {v1}, Lcom/b/a/a/a/q;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 303
    new-instance v0, Ljava/net/ProtocolException;

    iget-object v1, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, " does not support writing"

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private c()Lcom/b/a/a/a/o;
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 369
    invoke-direct {p0}, Lcom/b/a/a/b/b;->b()V

    .line 371
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    .line 372
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    .line 385
    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    .line 371
    goto :goto_0

    .line 408
    :cond_1
    iget-object v6, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    invoke-virtual {v4}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/b/a/a/a/o;->b(Ljava/net/URL;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 409
    iget-object v6, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v7, v6, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    if-eqz v7, :cond_2

    iget-object v7, v6, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v7, :cond_2

    iget-object v7, v6, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v7}, Lcom/b/a/a/a/aa;->c()V

    :cond_2
    iput-object v1, v6, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    .line 412
    :cond_3
    iget-object v6, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    invoke-virtual {v6}, Lcom/b/a/a/a/o;->d()Lcom/b/a/f;

    move-result-object v6

    .line 413
    iget-object v4, v4, Lcom/b/a/u;->b:Ljava/lang/String;

    check-cast v0, Lcom/b/a/a/a/v;

    invoke-direct {p0, v4, v6, v0, v5}, Lcom/b/a/a/b/b;->a(Ljava/lang/String;Lcom/b/a/f;Lcom/b/a/a/a/v;Lcom/b/a/x;)Lcom/b/a/a/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    .line 376
    :cond_4
    invoke-direct {p0, v2}, Lcom/b/a/a/b/b;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 377
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v4, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v4, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_5
    iget-object v5, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    .line 381
    iget-object v4, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v0, v4, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    iget-object v0, v4, Lcom/b/a/a/a/o;->e:Lcom/b/a/aa;

    if-eqz v0, :cond_8

    iget-object v0, v4, Lcom/b/a/a/a/o;->e:Lcom/b/a/aa;

    iget-object v0, v0, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    :goto_2
    iget-object v6, v4, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget v6, v6, Lcom/b/a/x;->c:I

    sparse-switch v6, :sswitch_data_0

    move-object v4, v1

    .line 383
    :goto_3
    if-nez v4, :cond_13

    .line 384
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v2, v0, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    if-eqz v2, :cond_7

    iget-object v2, v0, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v2}, Lcom/b/a/a/a/aa;->c()V

    :cond_7
    iput-object v1, v0, Lcom/b/a/a/a/o;->c:Lcom/b/a/f;

    .line 385
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    goto :goto_1

    .line 381
    :cond_8
    iget-object v0, v4, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-object v0, v0, Lcom/b/a/p;->b:Ljava/net/Proxy;

    goto :goto_2

    :sswitch_0
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v6

    sget-object v7, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v6, v7, :cond_9

    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    :sswitch_1
    iget-object v6, v4, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-object v6, v6, Lcom/b/a/p;->k:Lcom/b/a/b;

    iget-object v4, v4, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    invoke-static {v6, v4, v0}, Lcom/b/a/a/a/s;->a(Lcom/b/a/b;Lcom/b/a/x;Ljava/net/Proxy;)Lcom/b/a/u;

    move-result-object v0

    move-object v4, v0

    goto :goto_3

    :sswitch_2
    iget-object v0, v4, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iget-object v0, v0, Lcom/b/a/u;->b:Ljava/lang/String;

    const-string v6, "GET"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, v4, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iget-object v0, v0, Lcom/b/a/u;->b:Ljava/lang/String;

    const-string v6, "HEAD"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    move-object v4, v1

    goto :goto_3

    :cond_a
    :sswitch_3
    iget-object v0, v4, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-boolean v0, v0, Lcom/b/a/p;->o:Z

    if-nez v0, :cond_b

    move-object v4, v1

    goto :goto_3

    :cond_b
    iget-object v0, v4, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    const-string v6, "Location"

    invoke-virtual {v0, v6}, Lcom/b/a/x;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    move-object v4, v1

    goto :goto_3

    :cond_c
    new-instance v6, Ljava/net/URL;

    iget-object v7, v4, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    invoke-virtual {v7}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v7

    invoke-direct {v6, v7, v0}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v7, "https"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-virtual {v6}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string v7, "http"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    move-object v4, v1

    goto/16 :goto_3

    :cond_d
    invoke-virtual {v6}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iget-object v7, v4, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    invoke-virtual {v7}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v7

    invoke-virtual {v7}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, v4, Lcom/b/a/a/a/o;->b:Lcom/b/a/p;

    iget-boolean v0, v0, Lcom/b/a/p;->n:Z

    if-nez v0, :cond_e

    move-object v4, v1

    goto/16 :goto_3

    :cond_e
    iget-object v0, v4, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    new-instance v7, Lcom/b/a/v;

    invoke-direct {v7, v0}, Lcom/b/a/v;-><init>(Lcom/b/a/u;)V

    iget-object v0, v4, Lcom/b/a/a/a/o;->k:Lcom/b/a/u;

    iget-object v0, v0, Lcom/b/a/u;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/b/a/a/a/q;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    const-string v0, "GET"

    invoke-virtual {v7, v0, v1}, Lcom/b/a/v;->a(Ljava/lang/String;Lcom/b/a/w;)Lcom/b/a/v;

    const-string v0, "Transfer-Encoding"

    invoke-virtual {v7, v0}, Lcom/b/a/v;->a(Ljava/lang/String;)Lcom/b/a/v;

    const-string v0, "Content-Length"

    invoke-virtual {v7, v0}, Lcom/b/a/v;->a(Ljava/lang/String;)Lcom/b/a/v;

    const-string v0, "Content-Type"

    invoke-virtual {v7, v0}, Lcom/b/a/v;->a(Ljava/lang/String;)Lcom/b/a/v;

    :cond_f
    invoke-virtual {v4, v6}, Lcom/b/a/a/a/o;->b(Ljava/net/URL;)Z

    move-result v0

    if-nez v0, :cond_10

    const-string v0, "Authorization"

    invoke-virtual {v7, v0}, Lcom/b/a/v;->a(Ljava/lang/String;)Lcom/b/a/v;

    :cond_10
    if-nez v6, :cond_11

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iput-object v6, v7, Lcom/b/a/v;->b:Ljava/net/URL;

    invoke-virtual {v6}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/b/a/v;->a:Ljava/lang/String;

    iget-object v0, v7, Lcom/b/a/v;->a:Ljava/lang/String;

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    new-instance v0, Lcom/b/a/u;

    invoke-direct {v0, v7}, Lcom/b/a/u;-><init>(Lcom/b/a/v;)V

    move-object v4, v0

    goto/16 :goto_3

    .line 388
    :cond_13
    iget v0, v5, Lcom/b/a/x;->c:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v3

    :goto_4
    if-eqz v0, :cond_14

    iget v0, p0, Lcom/b/a/a/b/b;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/b/a/a/b/b;->g:I

    const/16 v6, 0x14

    if-le v0, v6, :cond_14

    .line 389
    new-instance v0, Ljava/net/ProtocolException;

    iget v1, p0, Lcom/b/a/a/b/b;->g:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Too many redirects: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move v0, v2

    .line 388
    goto :goto_4

    .line 393
    :cond_14
    invoke-virtual {v4}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/b/b;->url:Ljava/net/URL;

    .line 394
    iget-object v0, v4, Lcom/b/a/u;->c:Lcom/b/a/l;

    new-instance v6, Lcom/b/a/m;

    invoke-direct {v6}, Lcom/b/a/m;-><init>()V

    iget-object v7, v6, Lcom/b/a/m;->a:Ljava/util/List;

    iget-object v0, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v6, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    .line 399
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v6, v0, Lcom/b/a/a/a/o;->u:Lcom/b/a/a/a/c;

    if-nez v6, :cond_15

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_15
    iget-object v0, v0, Lcom/b/a/a/a/o;->p:Lc/z;

    .line 400
    iget-object v6, v4, Lcom/b/a/u;->b:Ljava/lang/String;

    iget-object v7, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_16

    move-object v0, v1

    .line 404
    :cond_16
    if-eqz v0, :cond_1

    instance-of v6, v0, Lcom/b/a/a/a/v;

    if-nez v6, :cond_1

    .line 405
    new-instance v0, Ljava/net/HttpRetryException;

    const-string v1, "Cannot retry streamed HTTP body"

    iget v2, p0, Lcom/b/a/a/b/b;->responseCode:I

    invoke-direct {v0, v1, v2}, Ljava/net/HttpRetryException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 381
    nop

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_3
        0x12d -> :sswitch_3
        0x12e -> :sswitch_3
        0x12f -> :sswitch_3
        0x133 -> :sswitch_2
        0x191 -> :sswitch_1
        0x197 -> :sswitch_0
    .end sparse-switch

    .line 388
    :pswitch_data_0
    .packed-switch 0x12c
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 509
    iget-boolean v0, p0, Lcom/b/a/a/b/b;->connected:Z

    if-eqz v0, :cond_0

    .line 510
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_0
    if-nez p1, :cond_1

    .line 513
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 515
    :cond_1
    if-nez p2, :cond_2

    .line 521
    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " because its value was null."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/d;->a(Ljava/lang/String;)V

    .line 531
    :goto_0
    return-void

    .line 526
    :cond_2
    const-string v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 527
    :cond_3
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/b/a/a/b/b;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 529
    :cond_4
    iget-object v0, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    invoke-virtual {v0, p1, p2}, Lcom/b/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    goto :goto_0
.end method

.method public final connect()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/b/a/a/b/b;->b()V

    .line 105
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/b/a/a/b/b;->a(Z)Z

    move-result v0

    .line 106
    if-eqz v0, :cond_0

    .line 107
    return-void
.end method

.method public final disconnect()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    if-nez v0, :cond_1

    .line 120
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v1, v0, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, v0, Lcom/b/a/a/a/o;->g:Lcom/b/a/a/a/aa;

    invoke-interface {v1, v0}, Lcom/b/a/a/a/aa;->a(Lcom/b/a/a/a/o;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public getConnectTimeout()I
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iget v0, v0, Lcom/b/a/p;->p:I

    return v0
.end method

.method public final getErrorStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 128
    :try_start_0
    invoke-direct {p0}, Lcom/b/a/a/b/b;->c()Lcom/b/a/a/a/o;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Lcom/b/a/a/a/o;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v2, :cond_1

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    throw v1

    .line 134
    :catch_0
    move-exception v1

    :cond_0
    :goto_0
    return-object v0

    .line 129
    :cond_1
    iget-object v2, v1, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget v2, v2, Lcom/b/a/x;->c:I

    const/16 v3, 0x190

    if-lt v2, v3, :cond_0

    .line 130
    invoke-virtual {v1}, Lcom/b/a/a/a/o;->b()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method public final getHeaderField(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 169
    :try_start_0
    invoke-direct {p0}, Lcom/b/a/a/b/b;->a()Lcom/b/a/l;

    move-result-object v1

    shl-int/lit8 v2, p1, 0x1

    add-int/lit8 v2, v2, 0x1

    if-ltz v2, :cond_0

    iget-object v3, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-object v0

    .line 169
    :cond_1
    iget-object v1, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v0, v1, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 171
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 182
    if-nez p1, :cond_1

    :try_start_0
    invoke-direct {p0}, Lcom/b/a/a/b/b;->c()Lcom/b/a/a/a/o;

    move-result-object v0

    iget-object v1, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 186
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 182
    :cond_0
    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    new-instance v1, Lcom/b/a/a/a/z;

    iget-object v2, v0, Lcom/b/a/x;->b:Lcom/b/a/t;

    iget v3, v0, Lcom/b/a/x;->c:I

    iget-object v0, v0, Lcom/b/a/x;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lcom/b/a/a/a/z;-><init>(Lcom/b/a/t;ILjava/lang/String;)V

    invoke-virtual {v1}, Lcom/b/a/a/a/z;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/b/a/a/b/b;->a()Lcom/b/a/l;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method public final getHeaderFieldKey(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 192
    :try_start_0
    invoke-direct {p0}, Lcom/b/a/a/b/b;->a()Lcom/b/a/l;

    move-result-object v1

    shl-int/lit8 v2, p1, 0x1

    if-ltz v2, :cond_0

    iget-object v3, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v3, v3

    if-lt v2, v3, :cond_1

    .line 194
    :cond_0
    :goto_0
    return-object v0

    .line 192
    :cond_1
    iget-object v1, v1, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v0, v1, v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 194
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getHeaderFields()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 200
    :try_start_0
    invoke-direct {p0}, Lcom/b/a/a/b/b;->a()Lcom/b/a/l;

    move-result-object v0

    invoke-direct {p0}, Lcom/b/a/a/b/b;->c()Lcom/b/a/a/a/o;

    move-result-object v1

    iget-object v2, v1, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 203
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    .line 200
    :cond_0
    :try_start_1
    iget-object v1, v1, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    new-instance v2, Lcom/b/a/a/a/z;

    iget-object v3, v1, Lcom/b/a/x;->b:Lcom/b/a/t;

    iget v4, v1, Lcom/b/a/x;->c:I

    iget-object v1, v1, Lcom/b/a/x;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1}, Lcom/b/a/a/a/z;-><init>(Lcom/b/a/t;ILjava/lang/String;)V

    invoke-virtual {v2}, Lcom/b/a/a/a/z;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/b/a/a/a/s;->a(Lcom/b/a/l;Ljava/lang/String;)Ljava/util/Map;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 4

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/b/a/a/b/b;->doInput:Z

    if-nez v0, :cond_0

    .line 218
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "This protocol does not support input"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    invoke-direct {p0}, Lcom/b/a/a/b/b;->c()Lcom/b/a/a/a/o;

    move-result-object v0

    .line 227
    invoke-virtual {p0}, Lcom/b/a/a/b/b;->getResponseCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_1

    .line 228
    new-instance v0, Ljava/io/FileNotFoundException;

    iget-object v1, p0, Lcom/b/a/a/b/b;->url:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 231
    :cond_1
    invoke-virtual {v0}, Lcom/b/a/a/a/o;->b()Ljava/io/InputStream;

    move-result-object v0

    .line 232
    if-nez v0, :cond_2

    .line 233
    new-instance v0, Ljava/net/ProtocolException;

    invoke-virtual {p0}, Lcom/b/a/a/b/b;->getResponseCode()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No response body exists; responseCode="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 235
    :cond_2
    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 4

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/b/a/a/b/b;->connect()V

    .line 241
    iget-object v1, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v0, v1, Lcom/b/a/a/a/o;->q:Lc/i;

    if-eqz v0, :cond_0

    move-object v1, v0

    .line 242
    :goto_0
    if-nez v1, :cond_4

    .line 243
    new-instance v1, Ljava/net/ProtocolException;

    const-string v2, "method does not support a request body: "

    iget-object v0, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 241
    :cond_0
    iget-object v0, v1, Lcom/b/a/a/a/o;->u:Lcom/b/a/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, v1, Lcom/b/a/a/a/o;->p:Lc/z;

    if-eqz v0, :cond_2

    invoke-static {v0}, Lc/p;->a(Lc/z;)Lc/i;

    move-result-object v0

    iput-object v0, v1, Lcom/b/a/a/a/o;->q:Lc/i;

    move-object v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 243
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 244
    :cond_4
    iget-object v0, p0, Lcom/b/a/a/b/b;->c:Lcom/b/a/a/a/o;

    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_6

    .line 245
    new-instance v0, Ljava/net/ProtocolException;

    const-string v1, "cannot write request body after response has been read"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 244
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 248
    :cond_6
    invoke-interface {v1}, Lc/i;->d()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final getPermission()Ljava/security/Permission;
    .locals 5

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/b/a/a/b/b;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    .line 253
    invoke-virtual {p0}, Lcom/b/a/a/b/b;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/URL;)I

    move-result v0

    .line 254
    invoke-virtual {p0}, Lcom/b/a/a/b/b;->usingProxy()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 255
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iget-object v0, v0, Lcom/b/a/p;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    .line 256
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    .line 259
    :cond_0
    new-instance v2, Ljava/net/SocketPermission;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "connect, resolve"

    invoke-direct {v2, v0, v1}, Ljava/net/SocketPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getReadTimeout()I
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iget v0, v0, Lcom/b/a/p;->q:I

    return v0
.end method

.method public final getRequestProperties()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/b/a/a/b/b;->connected:Z

    if-eqz v0, :cond_0

    .line 209
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot access request header fields after connection is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    new-instance v1, Lcom/b/a/l;

    invoke-direct {v1, v0}, Lcom/b/a/l;-><init>(Lcom/b/a/m;)V

    const/4 v0, 0x0

    invoke-static {v1, v0}, Lcom/b/a/a/a/s;->a(Lcom/b/a/l;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 263
    if-nez p1, :cond_0

    move-object v0, v1

    .line 264
    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    iget-object v0, v3, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    move v2, v0

    :goto_1
    if-ltz v2, :cond_2

    iget-object v0, v3, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v3, Lcom/b/a/m;->a:Ljava/util/List;

    add-int/lit8 v1, v2, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v2, -0x2

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final getResponseCode()I
    .locals 2

    .prologue
    .line 471
    invoke-direct {p0}, Lcom/b/a/a/b/b;->c()Lcom/b/a/a/a/o;

    move-result-object v0

    iget-object v1, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget v0, v0, Lcom/b/a/x;->c:I

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 2

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/b/a/a/b/b;->c()Lcom/b/a/a/a/o;

    move-result-object v0

    iget-object v1, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/b/a/a/a/o;->o:Lcom/b/a/x;

    iget-object v0, v0, Lcom/b/a/x;->d:Ljava/lang/String;

    return-object v0
.end method

.method public setConnectTimeout(I)V
    .locals 6

    .prologue
    .line 268
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    long-to-int v1, v2

    iput v1, v0, Lcom/b/a/p;->p:I

    .line 269
    return-void
.end method

.method public setFixedLengthStreamingMode(I)V
    .locals 2

    .prologue
    .line 563
    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/b/a/a/b/b;->setFixedLengthStreamingMode(J)V

    .line 564
    return-void
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 3

    .prologue
    .line 567
    iget-boolean v0, p0, Ljava/net/HttpURLConnection;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :cond_0
    iget v0, p0, Lcom/b/a/a/b/b;->chunkLength:I

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Already in chunked mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "contentLength < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 570
    :cond_2
    iput-wide p1, p0, Lcom/b/a/a/b/b;->f:J

    .line 571
    const-wide/32 v0, 0x7fffffff

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Ljava/net/HttpURLConnection;->fixedContentLength:I

    .line 572
    return-void
.end method

.method public setIfModifiedSince(J)V
    .locals 7

    .prologue
    .line 500
    invoke-super {p0, p1, p2}, Ljava/net/HttpURLConnection;->setIfModifiedSince(J)V

    .line 501
    iget-wide v0, p0, Lcom/b/a/a/b/b;->ifModifiedSince:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    const-string v1, "If-Modified-Since"

    new-instance v2, Ljava/util/Date;

    iget-wide v4, p0, Lcom/b/a/a/b/b;->ifModifiedSince:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lcom/b/a/a/a/m;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/b/a/m;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    .line 506
    :goto_0
    return-void

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    const-string v1, "If-Modified-Since"

    invoke-virtual {v0, v1}, Lcom/b/a/m;->a(Ljava/lang/String;)Lcom/b/a/m;

    goto :goto_0
.end method

.method public setInstanceFollowRedirects(Z)V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iput-boolean p1, v0, Lcom/b/a/p;->o:Z

    .line 274
    return-void
.end method

.method public setReadTimeout(I)V
    .locals 6

    .prologue
    .line 281
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    int-to-long v2, p1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    const-wide/32 v4, 0x7fffffff

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    long-to-int v1, v2

    iput v1, v0, Lcom/b/a/p;->q:I

    .line 282
    return-void
.end method

.method public setRequestMethod(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 555
    sget-object v0, Lcom/b/a/a/a/q;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 556
    new-instance v0, Ljava/net/ProtocolException;

    sget-object v1, Lcom/b/a/a/a/q;->a:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Expected one of "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but was "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 559
    :cond_0
    iput-object p1, p0, Lcom/b/a/a/b/b;->method:Ljava/lang/String;

    .line 560
    return-void
.end method

.method public final setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/b/a/a/b/b;->connected:Z

    if-eqz v0, :cond_0

    .line 476
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 478
    :cond_0
    if-nez p1, :cond_1

    .line 479
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 481
    :cond_1
    if-nez p2, :cond_2

    .line 487
    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " because its value was null."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/d;->a(Ljava/lang/String;)V

    .line 497
    :goto_0
    return-void

    .line 492
    :cond_2
    const-string v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 493
    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/b/a/a/b/b;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 495
    :cond_4
    iget-object v0, p0, Lcom/b/a/a/b/b;->e:Lcom/b/a/m;

    invoke-virtual {v0, p1, p2}, Lcom/b/a/m;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/m;

    goto :goto_0
.end method

.method public final usingProxy()Z
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/b/a/a/b/b;->i:Lcom/b/a/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/b/a/a/b/b;->i:Lcom/b/a/aa;

    iget-object v0, v0, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    .line 463
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/b/b;->a:Lcom/b/a/p;

    iget-object v0, v0, Lcom/b/a/p;->b:Ljava/net/Proxy;

    goto :goto_0

    .line 463
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
