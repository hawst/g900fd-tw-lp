.class final Lcom/b/a/a/c/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/b/a/a/c/b;


# instance fields
.field final a:Lcom/b/a/a/c/h;

.field private final b:Lc/j;

.field private final c:Lcom/b/a/a/c/k;

.field private final d:Z


# direct methods
.method constructor <init>(Lc/j;IZ)V
    .locals 2

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    .line 97
    iput-boolean p3, p0, Lcom/b/a/a/c/m;->d:Z

    .line 98
    new-instance v0, Lcom/b/a/a/c/k;

    iget-object v1, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-direct {v0, v1}, Lcom/b/a/a/c/k;-><init>(Lc/j;)V

    iput-object v0, p0, Lcom/b/a/a/c/m;->c:Lcom/b/a/a/c/k;

    .line 99
    new-instance v0, Lcom/b/a/a/c/h;

    iget-object v1, p0, Lcom/b/a/a/c/m;->c:Lcom/b/a/a/c/k;

    invoke-direct {v0, p2, v1}, Lcom/b/a/a/c/h;-><init>(ILc/aa;)V

    iput-object v0, p0, Lcom/b/a/a/c/m;->a:Lcom/b/a/a/c/h;

    .line 100
    return-void
.end method

.method private a(ISBI)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ISBI)",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v7, 0x80

    const/16 v6, 0x40

    const/4 v5, -0x1

    .line 205
    iget-object v0, p0, Lcom/b/a/a/c/m;->c:Lcom/b/a/a/c/k;

    iget-object v1, p0, Lcom/b/a/a/c/m;->c:Lcom/b/a/a/c/k;

    iput p1, v1, Lcom/b/a/a/c/k;->d:I

    iput p1, v0, Lcom/b/a/a/c/k;->a:I

    .line 206
    iget-object v0, p0, Lcom/b/a/a/c/m;->c:Lcom/b/a/a/c/k;

    iput-short p2, v0, Lcom/b/a/a/c/k;->e:S

    .line 207
    iget-object v0, p0, Lcom/b/a/a/c/m;->c:Lcom/b/a/a/c/k;

    iput-byte p3, v0, Lcom/b/a/a/c/k;->b:B

    .line 208
    iget-object v0, p0, Lcom/b/a/a/c/m;->c:Lcom/b/a/a/c/k;

    iput p4, v0, Lcom/b/a/a/c/k;->c:I

    .line 212
    iget-object v1, p0, Lcom/b/a/a/c/m;->a:Lcom/b/a/a/c/h;

    :goto_0
    iget-object v0, v1, Lcom/b/a/a/c/h;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->e()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, v1, Lcom/b/a/a/c/h;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->g()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-ne v0, v7, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "index == 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    and-int/lit16 v2, v0, 0x80

    if-ne v2, v7, :cond_5

    const/16 v2, 0x7f

    invoke-virtual {v1, v0, v2}, Lcom/b/a/a/c/h;->a(II)I

    move-result v0

    add-int/lit8 v2, v0, -0x1

    if-ltz v2, :cond_1

    sget-object v0, Lcom/b/a/a/c/g;->a:[Lcom/b/a/a/c/e;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-gt v2, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    sget-object v0, Lcom/b/a/a/c/g;->a:[Lcom/b/a/a/c/e;

    aget-object v0, v0, v2

    iget-object v2, v1, Lcom/b/a/a/c/h;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/b/a/a/c/g;->a:[Lcom/b/a/a/c/e;

    array-length v0, v0

    sub-int v0, v2, v0

    iget v3, v1, Lcom/b/a/a/c/h;->f:I

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v0, v3

    if-ltz v0, :cond_3

    iget-object v3, v1, Lcom/b/a/a/c/h;->e:[Lcom/b/a/a/c/e;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    if-le v0, v3, :cond_4

    :cond_3
    new-instance v0, Ljava/io/IOException;

    add-int/lit8 v1, v2, 0x1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Header index too large "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v2, v1, Lcom/b/a/a/c/h;->a:Ljava/util/List;

    iget-object v3, v1, Lcom/b/a/a/c/h;->e:[Lcom/b/a/a/c/e;

    aget-object v0, v3, v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    if-ne v0, v6, :cond_6

    invoke-virtual {v1}, Lcom/b/a/a/c/h;->b()Lc/k;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/c/g;->a(Lc/k;)Lc/k;

    move-result-object v0

    invoke-virtual {v1}, Lcom/b/a/a/c/h;->b()Lc/k;

    move-result-object v2

    new-instance v3, Lcom/b/a/a/c/e;

    invoke-direct {v3, v0, v2}, Lcom/b/a/a/c/e;-><init>(Lc/k;Lc/k;)V

    invoke-virtual {v1, v5, v3}, Lcom/b/a/a/c/h;->a(ILcom/b/a/a/c/e;)V

    goto/16 :goto_0

    :cond_6
    and-int/lit8 v2, v0, 0x40

    if-ne v2, v6, :cond_7

    const/16 v2, 0x3f

    invoke-virtual {v1, v0, v2}, Lcom/b/a/a/c/h;->a(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/b/a/a/c/h;->a(I)Lc/k;

    move-result-object v0

    invoke-virtual {v1}, Lcom/b/a/a/c/h;->b()Lc/k;

    move-result-object v2

    new-instance v3, Lcom/b/a/a/c/e;

    invoke-direct {v3, v0, v2}, Lcom/b/a/a/c/e;-><init>(Lc/k;Lc/k;)V

    invoke-virtual {v1, v5, v3}, Lcom/b/a/a/c/h;->a(ILcom/b/a/a/c/e;)V

    goto/16 :goto_0

    :cond_7
    and-int/lit8 v2, v0, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_a

    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, Lcom/b/a/a/c/h;->a(II)I

    move-result v0

    iput v0, v1, Lcom/b/a/a/c/h;->d:I

    iget v0, v1, Lcom/b/a/a/c/h;->d:I

    if-ltz v0, :cond_8

    iget v0, v1, Lcom/b/a/a/c/h;->d:I

    iget v2, v1, Lcom/b/a/a/c/h;->c:I

    if-le v0, v2, :cond_9

    :cond_8
    new-instance v0, Ljava/io/IOException;

    iget v1, v1, Lcom/b/a/a/c/h;->d:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid header table byte count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    invoke-virtual {v1}, Lcom/b/a/a/c/h;->a()V

    goto/16 :goto_0

    :cond_a
    const/16 v2, 0x10

    if-eq v0, v2, :cond_b

    if-nez v0, :cond_c

    :cond_b
    invoke-virtual {v1}, Lcom/b/a/a/c/h;->b()Lc/k;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/c/g;->a(Lc/k;)Lc/k;

    move-result-object v0

    invoke-virtual {v1}, Lcom/b/a/a/c/h;->b()Lc/k;

    move-result-object v2

    iget-object v3, v1, Lcom/b/a/a/c/h;->a:Ljava/util/List;

    new-instance v4, Lcom/b/a/a/c/e;

    invoke-direct {v4, v0, v2}, Lcom/b/a/a/c/e;-><init>(Lc/k;Lc/k;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_c
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, Lcom/b/a/a/c/h;->a(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Lcom/b/a/a/c/h;->a(I)Lc/k;

    move-result-object v0

    invoke-virtual {v1}, Lcom/b/a/a/c/h;->b()Lc/k;

    move-result-object v2

    iget-object v3, v1, Lcom/b/a/a/c/h;->a:Ljava/util/List;

    new-instance v4, Lcom/b/a/a/c/e;

    invoke-direct {v4, v0, v2}, Lcom/b/a/a/c/e;-><init>(Lc/k;Lc/k;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 213
    :cond_d
    iget-object v0, p0, Lcom/b/a/a/c/m;->a:Lcom/b/a/a/c/h;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/b/a/a/c/h;->a:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v0, v0, Lcom/b/a/a/c/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-object v1
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 103
    iget-boolean v0, p0, Lcom/b/a/a/c/m;->d:Z

    if-eqz v0, :cond_1

    .line 109
    :cond_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    sget-object v1, Lcom/b/a/a/c/j;->b:Lc/k;

    iget-object v1, v1, Lc/k;->b:[B

    array-length v1, v1

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, Lc/j;->c(J)Lc/k;

    move-result-object v0

    .line 105
    sget-object v1, Lcom/b/a/a/c/j;->a:Ljava/util/logging/Logger;

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/b/a/a/c/j;->a:Ljava/util/logging/Logger;

    const-string v2, "<< CONNECTION %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lc/k;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 106
    :cond_2
    sget-object v1, Lcom/b/a/a/c/j;->b:Lc/k;

    invoke-virtual {v1, v0}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 107
    const-string v1, "Expected a connection header but was %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lc/k;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v5

    new-instance v0, Ljava/io/IOException;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/b/a/a/c/c;)Z
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v5, -0x1

    const/4 v4, 0x4

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    const-wide/16 v2, 0x9

    invoke-interface {v0, v2, v3}, Lc/j;->a(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-static {v0}, Lcom/b/a/a/c/j;->a(Lc/j;)I

    move-result v0

    .line 131
    if-ltz v0, :cond_0

    const/16 v2, 0x4000

    if-le v0, v2, :cond_2

    .line 132
    :cond_0
    const-string v2, "FRAME_SIZE_ERROR: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :catch_0
    move-exception v0

    move v6, v1

    .line 180
    :cond_1
    :goto_0
    return v6

    .line 134
    :cond_2
    iget-object v2, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v2}, Lc/j;->g()B

    move-result v2

    int-to-byte v2, v2

    .line 135
    iget-object v3, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v3}, Lc/j;->g()B

    move-result v3

    int-to-byte v7, v3

    .line 136
    iget-object v3, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v3}, Lc/j;->i()I

    move-result v3

    const v8, 0x7fffffff

    and-int/2addr v3, v8

    .line 137
    sget-object v8, Lcom/b/a/a/c/j;->a:Ljava/util/logging/Logger;

    sget-object v9, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    invoke-virtual {v8, v9}, Ljava/util/logging/Logger;->isLoggable(Ljava/util/logging/Level;)Z

    move-result v8

    if-eqz v8, :cond_3

    sget-object v8, Lcom/b/a/a/c/j;->a:Ljava/util/logging/Logger;

    invoke-static {v6, v3, v0, v2, v7}, Lcom/b/a/a/c/l;->a(ZIIBB)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/logging/Logger;->fine(Ljava/lang/String;)V

    .line 139
    :cond_3
    packed-switch v2, :pswitch_data_0

    .line 178
    iget-object v1, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    int-to-long v2, v0

    invoke-interface {v1, v2, v3}, Lc/j;->f(J)V

    goto :goto_0

    .line 141
    :pswitch_0
    and-int/lit8 v2, v7, 0x1

    if-eqz v2, :cond_4

    move v4, v6

    :goto_1
    and-int/lit8 v2, v7, 0x20

    if-eqz v2, :cond_5

    move v2, v6

    :goto_2
    if-eqz v2, :cond_6

    const-string v0, "PROTOCOL_ERROR: FLAG_COMPRESSED without SETTINGS_COMPRESS_DATA"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    move v4, v1

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2

    :cond_6
    and-int/lit8 v2, v7, 0x8

    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v1}, Lc/j;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-short v1, v1

    :cond_7
    invoke-static {v0, v7, v1}, Lcom/b/a/a/c/j;->a(IBS)I

    move-result v0

    iget-object v2, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {p1, v4, v3, v2, v0}, Lcom/b/a/a/c/c;->a(ZILc/j;I)V

    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    int-to-long v2, v1

    invoke-interface {v0, v2, v3}, Lc/j;->f(J)V

    goto :goto_0

    .line 145
    :pswitch_1
    if-nez v3, :cond_8

    const-string v0, "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_8
    and-int/lit8 v2, v7, 0x1

    if-eqz v2, :cond_a

    move v2, v6

    :goto_3
    and-int/lit8 v4, v7, 0x8

    if-eqz v4, :cond_b

    iget-object v4, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v4}, Lc/j;->g()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    :goto_4
    and-int/lit8 v5, v7, 0x20

    if-eqz v5, :cond_9

    iget-object v5, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v5}, Lc/j;->i()I

    iget-object v5, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v5}, Lc/j;->g()B

    add-int/lit8 v0, v0, -0x5

    :cond_9
    invoke-static {v0, v7, v4}, Lcom/b/a/a/c/j;->a(IBS)I

    move-result v0

    invoke-direct {p0, v0, v4, v7, v3}, Lcom/b/a/a/c/m;->a(ISBI)Ljava/util/List;

    move-result-object v4

    sget-object v5, Lcom/b/a/a/c/f;->d:Lcom/b/a/a/c/f;

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, Lcom/b/a/a/c/c;->a(ZZILjava/util/List;Lcom/b/a/a/c/f;)V

    goto/16 :goto_0

    :cond_a
    move v2, v1

    goto :goto_3

    :cond_b
    move v4, v1

    goto :goto_4

    .line 149
    :pswitch_2
    const/4 v2, 0x5

    if-eq v0, v2, :cond_c

    const-string v2, "TYPE_PRIORITY length: %d != 5"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    if-nez v3, :cond_d

    const-string v0, "TYPE_PRIORITY streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_d
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->i()I

    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->g()B

    goto/16 :goto_0

    .line 153
    :pswitch_3
    if-eq v0, v4, :cond_e

    const-string v2, "TYPE_RST_STREAM length: %d != 4"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    if-nez v3, :cond_f

    const-string v0, "TYPE_RST_STREAM streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_f
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->i()I

    move-result v0

    invoke-static {v0}, Lcom/b/a/a/c/a;->b(I)Lcom/b/a/a/c/a;

    move-result-object v2

    if-nez v2, :cond_10

    const-string v2, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    invoke-interface {p1, v3, v2}, Lcom/b/a/a/c/c;->a(ILcom/b/a/a/c/a;)V

    goto/16 :goto_0

    .line 157
    :pswitch_4
    if-eqz v3, :cond_11

    const-string v0, "TYPE_SETTINGS streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    and-int/lit8 v2, v7, 0x1

    if-eqz v2, :cond_12

    if-eqz v0, :cond_1

    const-string v0, "FRAME_SIZE_ERROR ack frame should be empty!"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_12
    rem-int/lit8 v2, v0, 0x6

    if-eqz v2, :cond_13

    const-string v2, "TYPE_SETTINGS length %% 6 != 0: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    new-instance v7, Lcom/b/a/a/c/y;

    invoke-direct {v7}, Lcom/b/a/a/c/y;-><init>()V

    move v3, v1

    :goto_5
    if-ge v3, v0, :cond_16

    iget-object v2, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v2}, Lc/j;->h()S

    move-result v2

    iget-object v8, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v8}, Lc/j;->i()I

    move-result v8

    packed-switch v2, :pswitch_data_1

    const-string v0, "PROTOCOL_ERROR invalid settings id: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v3, v1

    new-instance v1, Ljava/io/IOException;

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_5
    if-eqz v8, :cond_14

    if-eq v8, v6, :cond_14

    const-string v0, "PROTOCOL_ERROR SETTINGS_ENABLE_PUSH != 0 or 1"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_6
    move v2, v4

    :cond_14
    :pswitch_7
    invoke-virtual {v7, v2, v1, v8}, Lcom/b/a/a/c/y;->a(III)Lcom/b/a/a/c/y;

    add-int/lit8 v2, v3, 0x6

    move v3, v2

    goto :goto_5

    :pswitch_8
    const/4 v2, 0x7

    if-gez v8, :cond_14

    const-string v0, "PROTOCOL_ERROR SETTINGS_INITIAL_WINDOW_SIZE > 2^31 - 1"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :pswitch_9
    const/16 v9, 0x4000

    if-lt v8, v9, :cond_15

    const v9, 0xffffff

    if-le v8, v9, :cond_14

    :cond_15
    const-string v0, "PROTOCOL_ERROR SETTINGS_MAX_FRAME_SIZE: %s"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    new-instance v1, Ljava/io/IOException;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_16
    invoke-interface {p1, v1, v7}, Lcom/b/a/a/c/c;->a(ZLcom/b/a/a/c/y;)V

    iget v0, v7, Lcom/b/a/a/c/y;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_17

    iget-object v0, v7, Lcom/b/a/a/c/y;->d:[I

    aget v0, v0, v6

    :goto_6
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/b/a/a/c/m;->a:Lcom/b/a/a/c/h;

    iget v0, v7, Lcom/b/a/a/c/y;->a:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_18

    iget-object v0, v7, Lcom/b/a/a/c/y;->d:[I

    aget v0, v0, v6

    :goto_7
    iput v0, v1, Lcom/b/a/a/c/h;->c:I

    iget v0, v1, Lcom/b/a/a/c/h;->c:I

    iput v0, v1, Lcom/b/a/a/c/h;->d:I

    invoke-virtual {v1}, Lcom/b/a/a/c/h;->a()V

    goto/16 :goto_0

    :cond_17
    move v0, v5

    goto :goto_6

    :cond_18
    move v0, v5

    goto :goto_7

    .line 161
    :pswitch_a
    if-nez v3, :cond_19

    const-string v0, "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_19
    and-int/lit8 v2, v7, 0x8

    if-eqz v2, :cond_1a

    iget-object v1, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v1}, Lc/j;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    int-to-short v1, v1

    :cond_1a
    iget-object v2, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v2}, Lc/j;->i()I

    move-result v2

    const v4, 0x7fffffff

    and-int/2addr v2, v4

    add-int/lit8 v0, v0, -0x4

    invoke-static {v0, v7, v1}, Lcom/b/a/a/c/j;->a(IBS)I

    move-result v0

    invoke-direct {p0, v0, v1, v7, v3}, Lcom/b/a/a/c/m;->a(ISBI)Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Lcom/b/a/a/c/c;->a(ILjava/util/List;)V

    goto/16 :goto_0

    .line 165
    :pswitch_b
    if-eq v0, v10, :cond_1b

    const-string v2, "TYPE_PING length != 8: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    if-eqz v3, :cond_1c

    const-string v0, "TYPE_PING streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1c
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->i()I

    move-result v0

    iget-object v2, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v2}, Lc/j;->i()I

    move-result v2

    and-int/lit8 v3, v7, 0x1

    if-eqz v3, :cond_1d

    move v1, v6

    :cond_1d
    invoke-interface {p1, v1, v0, v2}, Lcom/b/a/a/c/c;->a(ZII)V

    goto/16 :goto_0

    .line 169
    :pswitch_c
    if-ge v0, v10, :cond_1e

    const-string v2, "TYPE_GOAWAY length < 8: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1e
    if-eqz v3, :cond_1f

    const-string v0, "TYPE_GOAWAY streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1f
    iget-object v2, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v2}, Lc/j;->i()I

    move-result v2

    iget-object v3, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v3}, Lc/j;->i()I

    move-result v3

    add-int/lit8 v4, v0, -0x8

    invoke-static {v3}, Lcom/b/a/a/c/a;->b(I)Lcom/b/a/a/c/a;

    move-result-object v0

    if-nez v0, :cond_20

    const-string v0, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    new-instance v1, Ljava/io/IOException;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_20
    sget-object v0, Lc/k;->a:Lc/k;

    if-lez v4, :cond_21

    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    int-to-long v4, v4

    invoke-interface {v0, v4, v5}, Lc/j;->c(J)Lc/k;

    move-result-object v0

    :cond_21
    invoke-interface {p1, v2, v0}, Lcom/b/a/a/c/c;->a(ILc/k;)V

    goto/16 :goto_0

    .line 173
    :pswitch_d
    if-eq v0, v4, :cond_22

    const-string v2, "TYPE_WINDOW_UPDATE length !=4: %s"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v1

    new-instance v0, Ljava/io/IOException;

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_22
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->i()I

    move-result v0

    int-to-long v4, v0

    const-wide/32 v8, 0x7fffffff

    and-long/2addr v4, v8

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-nez v0, :cond_23

    const-string v0, "windowSizeIncrement was 0"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v1

    new-instance v1, Ljava/io/IOException;

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_23
    invoke-interface {p1, v3, v4, v5}, Lcom/b/a/a/c/c;->a(IJ)V

    goto/16 :goto_0

    .line 139
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    .line 157
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_5
        :pswitch_6
        :pswitch_8
        :pswitch_9
        :pswitch_7
    .end packed-switch
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/b/a/a/c/m;->b:Lc/j;

    invoke-interface {v0}, Lc/j;->close()V

    .line 360
    return-void
.end method
