.class public final Lcom/b/a/a/c/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final a:Ljava/util/concurrent/ExecutorService;

.field static final synthetic v:Z


# instance fields
.field public final b:Lcom/b/a/t;

.field final c:Z

.field final d:Lcom/b/a/a/c/q;

.field final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/b/a/a/c/ao;",
            ">;"
        }
    .end annotation
.end field

.field final f:Ljava/lang/String;

.field g:I

.field h:I

.field i:Z

.field final j:Ljava/util/concurrent/ExecutorService;

.field final k:Lcom/b/a/a/c/w;

.field l:J

.field m:J

.field public final n:Lcom/b/a/a/c/y;

.field final o:Lcom/b/a/a/c/y;

.field p:Z

.field final q:Lcom/b/a/a/c/as;

.field final r:Ljava/net/Socket;

.field public final s:Lcom/b/a/a/c/d;

.field final t:Lcom/b/a/a/c/al;

.field final u:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private w:J


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 53
    const-class v0, Lcom/b/a/a/c/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v8

    :goto_0
    sput-boolean v0, Lcom/b/a/a/c/ac;->v:Z

    .line 67
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const v3, 0x7fffffff

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v7}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string v0, "OkHttp SpdyConnection"

    invoke-static {v0, v8}, Lcom/b/a/a/i;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v1, Lcom/b/a/a/c/ac;->a:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    move v0, v2

    .line 53
    goto :goto_0
.end method

.method public constructor <init>(Lcom/b/a/a/c/ak;)V
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v10, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    .line 87
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/b/a/a/c/ac;->w:J

    .line 103
    iput-wide v4, p0, Lcom/b/a/a/c/ac;->l:J

    .line 114
    new-instance v0, Lcom/b/a/a/c/y;

    invoke-direct {v0}, Lcom/b/a/a/c/y;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/ac;->n:Lcom/b/a/a/c/y;

    .line 120
    new-instance v0, Lcom/b/a/a/c/y;

    invoke-direct {v0}, Lcom/b/a/a/c/y;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    .line 122
    iput-boolean v2, p0, Lcom/b/a/a/c/ac;->p:Z

    .line 792
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/ac;->u:Ljava/util/Set;

    .line 131
    iget-object v0, p1, Lcom/b/a/a/c/ak;->d:Lcom/b/a/t;

    iput-object v0, p0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    .line 132
    iget-object v0, p1, Lcom/b/a/a/c/ak;->e:Lcom/b/a/a/c/w;

    iput-object v0, p0, Lcom/b/a/a/c/ac;->k:Lcom/b/a/a/c/w;

    .line 133
    iget-boolean v0, p1, Lcom/b/a/a/c/ak;->f:Z

    iput-boolean v0, p0, Lcom/b/a/a/c/ac;->c:Z

    .line 134
    iget-object v0, p1, Lcom/b/a/a/c/ak;->c:Lcom/b/a/a/c/q;

    iput-object v0, p0, Lcom/b/a/a/c/ac;->d:Lcom/b/a/a/c/q;

    .line 136
    iget-boolean v0, p1, Lcom/b/a/a/c/ak;->f:Z

    if-eqz v0, :cond_3

    move v0, v3

    :goto_0
    iput v0, p0, Lcom/b/a/a/c/ac;->h:I

    .line 137
    iget-boolean v0, p1, Lcom/b/a/a/c/ak;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    sget-object v1, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v0, v1, :cond_0

    .line 138
    iget v0, p0, Lcom/b/a/a/c/ac;->h:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/b/a/a/c/ac;->h:I

    .line 141
    :cond_0
    iget-boolean v0, p1, Lcom/b/a/a/c/ak;->f:Z

    .line 147
    iget-boolean v0, p1, Lcom/b/a/a/c/ak;->f:Z

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/b/a/a/c/ac;->n:Lcom/b/a/a/c/y;

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v10, v2, v1}, Lcom/b/a/a/c/y;->a(III)Lcom/b/a/a/c/y;

    .line 151
    :cond_1
    iget-object v0, p1, Lcom/b/a/a/c/ak;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    sget-object v1, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v0, v1, :cond_4

    .line 154
    new-instance v0, Lcom/b/a/a/c/j;

    invoke-direct {v0}, Lcom/b/a/a/c/j;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/ac;->q:Lcom/b/a/a/c/as;

    .line 156
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const-string v0, "OkHttp %s Push Observer"

    new-array v8, v3, [Ljava/lang/Object;

    iget-object v9, p0, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v9, v8, v2

    invoke-static {v0, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/b/a/a/i;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v8

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v1, p0, Lcom/b/a/a/c/ac;->j:Ljava/util/concurrent/ExecutorService;

    .line 161
    iget-object v0, p0, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    const v1, 0xffff

    invoke-virtual {v0, v10, v2, v1}, Lcom/b/a/a/c/y;->a(III)Lcom/b/a/a/c/y;

    .line 162
    iget-object v0, p0, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    const/4 v1, 0x5

    const/16 v3, 0x4000

    invoke-virtual {v0, v1, v2, v3}, Lcom/b/a/a/c/y;->a(III)Lcom/b/a/a/c/y;

    .line 169
    :goto_1
    iget-object v1, p0, Lcom/b/a/a/c/ac;->o:Lcom/b/a/a/c/y;

    const/high16 v0, 0x10000

    iget v2, v1, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v2, v2, 0x80

    if-eqz v2, :cond_2

    iget-object v0, v1, Lcom/b/a/a/c/y;->d:[I

    aget v0, v0, v10

    :cond_2
    int-to-long v0, v0

    iput-wide v0, p0, Lcom/b/a/a/c/ac;->m:J

    .line 170
    iget-object v0, p1, Lcom/b/a/a/c/ak;->b:Ljava/net/Socket;

    iput-object v0, p0, Lcom/b/a/a/c/ac;->r:Ljava/net/Socket;

    .line 171
    iget-object v0, p0, Lcom/b/a/a/c/ac;->q:Lcom/b/a/a/c/as;

    iget-object v1, p1, Lcom/b/a/a/c/ak;->b:Ljava/net/Socket;

    invoke-static {v1}, Lc/p;->a(Ljava/net/Socket;)Lc/z;

    move-result-object v1

    invoke-static {v1}, Lc/p;->a(Lc/z;)Lc/i;

    move-result-object v1

    iget-boolean v2, p0, Lcom/b/a/a/c/ac;->c:Z

    invoke-interface {v0, v1, v2}, Lcom/b/a/a/c/as;->a(Lc/i;Z)Lcom/b/a/a/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    .line 173
    new-instance v0, Lcom/b/a/a/c/al;

    invoke-direct {v0, p0}, Lcom/b/a/a/c/al;-><init>(Lcom/b/a/a/c/ac;)V

    iput-object v0, p0, Lcom/b/a/a/c/ac;->t:Lcom/b/a/a/c/al;

    .line 174
    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/b/a/a/c/ac;->t:Lcom/b/a/a/c/al;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 175
    return-void

    .line 136
    :cond_3
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 163
    :cond_4
    iget-object v0, p0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    sget-object v1, Lcom/b/a/t;->c:Lcom/b/a/t;

    if-ne v0, v1, :cond_5

    .line 164
    new-instance v0, Lcom/b/a/a/c/z;

    invoke-direct {v0}, Lcom/b/a/a/c/z;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/c/ac;->q:Lcom/b/a/a/c/as;

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/b/a/a/c/ac;->j:Ljava/util/concurrent/ExecutorService;

    goto :goto_1

    .line 167
    :cond_5
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/b/a/a/c/ac;->b:Lcom/b/a/t;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 203
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/b/a/a/c/ac;->w:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 204
    monitor-exit p0

    return-void

    .line 203
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method final declared-synchronized a(I)Lcom/b/a/a/c/ao;
    .locals 2

    .prologue
    .line 191
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/ao;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(ILjava/util/List;ZZ)Lcom/b/a/a/c/ao;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcom/b/a/a/c/e;",
            ">;ZZ)",
            "Lcom/b/a/a/c/ao;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 249
    if-nez p3, :cond_0

    move v3, v4

    .line 250
    :goto_0
    if-nez p4, :cond_1

    .line 254
    :goto_1
    iget-object v8, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    monitor-enter v8

    .line 255
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 256
    :try_start_1
    iget-boolean v0, p0, Lcom/b/a/a/c/ac;->i:Z

    if-eqz v0, :cond_2

    .line 257
    new-instance v0, Ljava/io/IOException;

    const-string v1, "shutdown"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 266
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 275
    :catchall_1
    move-exception v0

    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_0
    move v3, v0

    .line 249
    goto :goto_0

    :cond_1
    move v4, v0

    .line 250
    goto :goto_1

    .line 259
    :cond_2
    :try_start_3
    iget v1, p0, Lcom/b/a/a/c/ac;->h:I

    .line 260
    iget v0, p0, Lcom/b/a/a/c/ac;->h:I

    add-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/b/a/a/c/ac;->h:I

    .line 261
    new-instance v0, Lcom/b/a/a/c/ao;

    move-object v2, p0

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/b/a/a/c/ao;-><init>(ILcom/b/a/a/c/ac;ZZLjava/util/List;)V

    .line 262
    invoke-virtual {v0}, Lcom/b/a/a/c/ao;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 263
    iget-object v2, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/b/a/a/c/ac;->a(Z)V

    .line 266
    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 267
    if-nez p1, :cond_5

    .line 268
    :try_start_4
    iget-object v2, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    move v5, v1

    move v6, p1

    move-object v7, p2

    invoke-interface/range {v2 .. v7}, Lcom/b/a/a/c/d;->a(ZZIILjava/util/List;)V

    .line 275
    :goto_2
    monitor-exit v8
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 277
    if-nez p3, :cond_4

    .line 278
    iget-object v1, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v1}, Lcom/b/a/a/c/d;->b()V

    .line 281
    :cond_4
    return-object v0

    .line 270
    :cond_5
    :try_start_5
    iget-boolean v2, p0, Lcom/b/a/a/c/ac;->c:Z

    if-eqz v2, :cond_6

    .line 271
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "client streams shouldn\'t have associated stream IDs"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 273
    :cond_6
    iget-object v2, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v2, p1, v1, p2}, Lcom/b/a/a/c/d;->a(IILjava/util/List;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2
.end method

.method final a(IJ)V
    .locals 8

    .prologue
    .line 357
    sget-object v0, Lcom/b/a/a/c/ac;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/b/a/a/c/ae;

    const-string v3, "OkHttp Window Update %s stream %d"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v5, v4, v2

    const/4 v2, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    move-object v2, p0

    move v5, p1

    move-wide v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/b/a/a/c/ae;-><init>(Lcom/b/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;IJ)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 365
    return-void
.end method

.method public final a(ILcom/b/a/a/c/a;)V
    .locals 7

    .prologue
    .line 342
    sget-object v6, Lcom/b/a/a/c/ac;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/b/a/a/c/ad;

    const-string v2, "OkHttp %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/b/a/a/c/ac;->f:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/b/a/a/c/ad;-><init>(Lcom/b/a/a/c/ac;Ljava/lang/String;[Ljava/lang/Object;ILcom/b/a/a/c/a;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 350
    return-void
.end method

.method public final a(IZLc/f;J)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const-wide/16 v8, 0x0

    .line 306
    cmp-long v0, p4, v8

    if-nez v0, :cond_2

    .line 307
    iget-object v0, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v0, p2, p1, p3, v1}, Lcom/b/a/a/c/d;->a(ZILc/f;I)V

    .line 330
    :cond_0
    return-void

    .line 322
    :cond_1
    :try_start_0
    iget-wide v2, p0, Lcom/b/a/a/c/ac;->m:J

    invoke-static {p4, p5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v0, v2

    .line 323
    iget-object v2, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v2}, Lcom/b/a/a/c/d;->c()I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 324
    iget-wide v4, p0, Lcom/b/a/a/c/ac;->m:J

    int-to-long v6, v2

    sub-long/2addr v4, v6

    iput-wide v4, p0, Lcom/b/a/a/c/ac;->m:J

    .line 325
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    int-to-long v4, v2

    sub-long/2addr p4, v4

    .line 328
    iget-object v3, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    if-eqz p2, :cond_3

    cmp-long v0, p4, v8

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v3, v0, p1, p3, v2}, Lcom/b/a/a/c/d;->a(ZILc/f;I)V

    .line 311
    :cond_2
    cmp-long v0, p4, v8

    if-lez v0, :cond_0

    .line 313
    monitor-enter p0

    .line 315
    :goto_1
    :try_start_1
    iget-wide v2, p0, Lcom/b/a/a/c/ac;->m:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_1

    .line 316
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 319
    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    .line 325
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    .line 328
    goto :goto_0
.end method

.method a(Lcom/b/a/a/c/a;Lcom/b/a/a/c/a;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 447
    sget-boolean v1, Lcom/b/a/a/c/ac;->v:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 450
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    monitor-enter p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-boolean v1, p0, Lcom/b/a/a/c/ac;->i:Z

    if-eqz v1, :cond_2

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v0

    .line 456
    :goto_0
    monitor-enter p0

    .line 458
    :try_start_4
    iget-object v3, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_7

    .line 459
    iget-object v0, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v3, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Lcom/b/a/a/c/ao;

    invoke-interface {v0, v3}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/a/a/c/ao;

    .line 460
    iget-object v3, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 461
    const/4 v3, 0x0

    invoke-direct {p0, v3}, Lcom/b/a/a/c/ac;->a(Z)V

    move-object v3, v0

    .line 463
    :goto_1
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 469
    if-eqz v3, :cond_4

    .line 470
    array-length v4, v3

    move-object v0, v1

    :goto_2
    if-ge v2, v4, :cond_3

    aget-object v1, v3, v2

    .line 472
    :try_start_5
    invoke-virtual {v1, p2}, Lcom/b/a/a/c/ao;->b(Lcom/b/a/a/c/a;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v1, Lcom/b/a/a/c/ao;->d:Lcom/b/a/a/c/ac;

    iget v1, v1, Lcom/b/a/a/c/ao;->c:I

    iget-object v5, v5, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v5, v1, p2}, Lcom/b/a/a/c/d;->a(ILcom/b/a/a/c/a;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    .line 470
    :cond_1
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 450
    :cond_2
    const/4 v1, 0x1

    :try_start_6
    iput-boolean v1, p0, Lcom/b/a/a/c/ac;->i:Z

    iget v1, p0, Lcom/b/a/a/c/ac;->g:I

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v4, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    sget-object v5, Lcom/b/a/a/i;->a:[B

    invoke-interface {v4, v1, p1, v5}, Lcom/b/a/a/c/d;->a(ILcom/b/a/a/c/a;[B)V

    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v1

    :catchall_1
    move-exception v1

    monitor-exit v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    throw v1
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    .line 453
    :catch_0
    move-exception v1

    goto :goto_0

    .line 463
    :catchall_2
    move-exception v0

    :try_start_b
    monitor-exit p0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    throw v0

    .line 473
    :catch_1
    move-exception v1

    .line 474
    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_3

    :cond_3
    move-object v1, v0

    .line 479
    :cond_4
    :try_start_c
    iget-object v0, p0, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v0}, Lcom/b/a/a/c/d;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_2

    move-object v0, v1

    .line 494
    :cond_5
    :goto_4
    :try_start_d
    iget-object v1, p0, Lcom/b/a/a/c/ac;->r:Ljava/net/Socket;

    invoke-virtual {v1}, Ljava/net/Socket;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_3

    .line 499
    :goto_5
    if-eqz v0, :cond_6

    throw v0

    .line 488
    :catch_2
    move-exception v0

    .line 489
    if-eqz v1, :cond_5

    move-object v0, v1

    goto :goto_4

    .line 500
    :cond_6
    return-void

    .line 495
    :catch_3
    move-exception v0

    goto :goto_5

    :cond_7
    move-object v3, v0

    goto :goto_1
.end method

.method public final declared-synchronized a()Z
    .locals 4

    .prologue
    .line 208
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/b/a/a/c/ac;->w:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()J
    .locals 2

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/b/a/a/c/ac;->w:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(I)Lcom/b/a/a/c/ao;
    .locals 2

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/a/c/ao;

    .line 196
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/b/a/a/c/ac;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 197
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/b/a/a/c/ac;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 199
    :cond_0
    monitor-exit p0

    return-object v0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized c(I)Lcom/b/a/a/c/v;
    .locals 1

    .prologue
    .line 409
    monitor-enter p0

    const/4 v0, 0x0

    monitor-exit p0

    return-object v0
.end method

.method public final close()V
    .locals 2

    .prologue
    .line 443
    sget-object v0, Lcom/b/a/a/c/a;->a:Lcom/b/a/a/c/a;

    sget-object v1, Lcom/b/a/a/c/a;->l:Lcom/b/a/a/c/a;

    invoke-virtual {p0, v0, v1}, Lcom/b/a/a/c/ac;->a(Lcom/b/a/a/c/a;Lcom/b/a/a/c/a;)V

    .line 444
    return-void
.end method
