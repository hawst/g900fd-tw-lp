.class public final Lcom/b/a/a/c/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lc/k;

.field public static final b:Lc/k;

.field public static final c:Lc/k;

.field public static final d:Lc/k;

.field public static final e:Lc/k;

.field public static final f:Lc/k;

.field public static final g:Lc/k;


# instance fields
.field public final h:Lc/k;

.field public final i:Lc/k;

.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const-string v0, ":status"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/e;->a:Lc/k;

    .line 9
    const-string v0, ":method"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/e;->b:Lc/k;

    .line 10
    const-string v0, ":path"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/e;->c:Lc/k;

    .line 11
    const-string v0, ":scheme"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/e;->d:Lc/k;

    .line 12
    const-string v0, ":authority"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/e;->e:Lc/k;

    .line 13
    const-string v0, ":host"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/e;->f:Lc/k;

    .line 14
    const-string v0, ":version"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/e;->g:Lc/k;

    return-void
.end method

.method public constructor <init>(Lc/k;Lc/k;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/b/a/a/c/e;->h:Lc/k;

    .line 33
    iput-object p2, p0, Lcom/b/a/a/c/e;->i:Lc/k;

    .line 34
    iget-object v0, p1, Lc/k;->b:[B

    array-length v0, v0

    add-int/lit8 v0, v0, 0x20

    iget-object v1, p2, Lc/k;->b:[B

    array-length v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/b/a/a/c/e;->j:I

    .line 35
    return-void
.end method

.method public constructor <init>(Lc/k;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 28
    invoke-static {p2}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/b/a/a/c/e;-><init>(Lc/k;Lc/k;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 24
    invoke-static {p1}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    invoke-static {p2}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/b/a/a/c/e;-><init>(Lc/k;Lc/k;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    instance-of v1, p1, Lcom/b/a/a/c/e;

    if-eqz v1, :cond_0

    .line 39
    check-cast p1, Lcom/b/a/a/c/e;

    .line 40
    iget-object v1, p0, Lcom/b/a/a/c/e;->h:Lc/k;

    iget-object v2, p1, Lcom/b/a/a/c/e;->h:Lc/k;

    invoke-virtual {v1, v2}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/b/a/a/c/e;->i:Lc/k;

    iget-object v2, p1, Lcom/b/a/a/c/e;->i:Lc/k;

    invoke-virtual {v1, v2}, Lc/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 43
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/b/a/a/c/e;->h:Lc/k;

    invoke-virtual {v0}, Lc/k;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 49
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/b/a/a/c/e;->i:Lc/k;

    invoke-virtual {v1}, Lc/k;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 50
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 54
    const-string v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/b/a/a/c/e;->h:Lc/k;

    invoke-virtual {v3}, Lc/k;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/b/a/a/c/e;->i:Lc/k;

    invoke-virtual {v3}, Lc/k;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
