.class Lcom/b/a/a/a/g;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lc/z;

.field public b:Z

.field final synthetic c:Lcom/b/a/a/a/f;

.field private final d:Lcom/b/a/a/a/b;


# direct methods
.method constructor <init>(Lcom/b/a/a/a/f;Lcom/b/a/a/a/b;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 377
    iput-object p1, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 379
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/b/a/a/a/b;->a()Lc/z;

    move-result-object v1

    .line 380
    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    .line 384
    :cond_0
    iput-object v1, p0, Lcom/b/a/a/a/g;->a:Lc/z;

    .line 385
    iput-object p2, p0, Lcom/b/a/a/a/g;->d:Lcom/b/a/a/a/b;

    .line 386
    return-void

    :cond_1
    move-object v1, v0

    .line 379
    goto :goto_0
.end method


# virtual methods
.method protected final a(Lc/f;J)V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lcom/b/a/a/a/g;->a:Lc/z;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Lcom/b/a/a/a/g;->a:Lc/z;

    invoke-virtual {p1}, Lc/f;->n()Lc/f;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Lc/z;->a(Lc/f;J)V

    .line 394
    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 401
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget v0, v0, Lcom/b/a/a/a/f;->e:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget v1, v1, Lcom/b/a/a/a/f;->e:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x12

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/g;->d:Lcom/b/a/a/a/b;

    if-eqz v0, :cond_1

    .line 404
    iget-object v0, p0, Lcom/b/a/a/a/g;->a:Lc/z;

    invoke-interface {v0}, Lc/z;->close()V

    .line 407
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iput v2, v0, Lcom/b/a/a/a/f;->e:I

    .line 408
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget v0, v0, Lcom/b/a/a/a/f;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 409
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iput v2, v0, Lcom/b/a/a/a/f;->f:I

    .line 410
    sget-object v0, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    iget-object v1, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget-object v1, v1, Lcom/b/a/a/a/f;->a:Lcom/b/a/g;

    iget-object v2, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget-object v2, v2, Lcom/b/a/a/a/f;->b:Lcom/b/a/f;

    invoke-virtual {v0, v1, v2}, Lcom/b/a/a/a;->a(Lcom/b/a/g;Lcom/b/a/f;)V

    .line 415
    :cond_2
    :goto_0
    return-void

    .line 411
    :cond_3
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget v0, v0, Lcom/b/a/a/a/f;->f:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 412
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    const/4 v1, 0x6

    iput v1, v0, Lcom/b/a/a/a/f;->e:I

    .line 413
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget-object v0, v0, Lcom/b/a/a/a/f;->b:Lcom/b/a/f;

    iget-object v0, v0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0
.end method

.method protected final at_()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/b/a/a/a/g;->d:Lcom/b/a/a/a/b;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/b/a/a/a/g;->d:Lcom/b/a/a/a/b;

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    iget-object v0, v0, Lcom/b/a/a/a/f;->b:Lcom/b/a/f;

    iget-object v0, v0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/Socket;)V

    .line 434
    iget-object v0, p0, Lcom/b/a/a/a/g;->c:Lcom/b/a/a/a/f;

    const/4 v1, 0x6

    iput v1, v0, Lcom/b/a/a/a/f;->e:I

    .line 435
    return-void
.end method
