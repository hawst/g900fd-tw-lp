.class public final Lcom/b/a/a/a/w;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/b/a/a;

.field final b:Ljava/net/URI;

.field public final c:Lcom/b/a/p;

.field final d:Ljava/net/ProxySelector;

.field final e:Lcom/b/a/a/h;

.field public final f:Lcom/b/a/u;

.field g:Ljava/net/Proxy;

.field h:Ljava/net/InetSocketAddress;

.field i:Z

.field j:[Ljava/net/InetAddress;

.field k:Ljava/lang/String;

.field final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/b/a/aa;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/b/a/n;

.field private final n:Lcom/b/a/g;

.field private o:Ljava/net/Proxy;

.field private p:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljava/net/Proxy;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field private r:I


# direct methods
.method public constructor <init>(Lcom/b/a/a;Ljava/net/URI;Lcom/b/a/p;Lcom/b/a/u;)V
    .locals 2

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/b/a/a/a/w;->l:Ljava/util/List;

    .line 85
    iput-object p1, p0, Lcom/b/a/a/a/w;->a:Lcom/b/a/a;

    .line 86
    iput-object p2, p0, Lcom/b/a/a/a/w;->b:Ljava/net/URI;

    .line 87
    iput-object p3, p0, Lcom/b/a/a/a/w;->c:Lcom/b/a/p;

    .line 88
    iget-object v0, p3, Lcom/b/a/p;->d:Ljava/net/ProxySelector;

    iput-object v0, p0, Lcom/b/a/a/a/w;->d:Ljava/net/ProxySelector;

    .line 89
    iget-object v0, p3, Lcom/b/a/p;->l:Lcom/b/a/g;

    iput-object v0, p0, Lcom/b/a/a/a/w;->n:Lcom/b/a/g;

    .line 90
    sget-object v0, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    invoke-virtual {v0, p3}, Lcom/b/a/a/a;->b(Lcom/b/a/p;)Lcom/b/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/w;->e:Lcom/b/a/a/h;

    .line 91
    iget-object v0, p3, Lcom/b/a/p;->m:Lcom/b/a/n;

    iput-object v0, p0, Lcom/b/a/a/a/w;->m:Lcom/b/a/n;

    .line 92
    iput-object p4, p0, Lcom/b/a/a/a/w;->f:Lcom/b/a/u;

    .line 94
    iget-object v0, p1, Lcom/b/a/a;->a:Ljava/net/Proxy;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/b/a/a/a/w;->i:Z

    if-eqz v0, :cond_1

    iput-object v0, p0, Lcom/b/a/a/a/w;->o:Ljava/net/Proxy;

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/a/w;->d:Ljava/net/ProxySelector;

    invoke-virtual {v0, p2}, Ljava/net/ProxySelector;->select(Ljava/net/URI;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/w;->p:Ljava/util/Iterator;

    goto :goto_0
.end method

.method private c()Ljava/net/Proxy;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 223
    iget-object v0, p0, Lcom/b/a/a/a/w;->o:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    .line 224
    iput-boolean v3, p0, Lcom/b/a/a/a/w;->i:Z

    .line 225
    iget-object v0, p0, Lcom/b/a/a/a/w;->o:Ljava/net/Proxy;

    .line 241
    :goto_0
    return-object v0

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/w;->p:Ljava/util/Iterator;

    if-eqz v0, :cond_2

    .line 231
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/a/w;->p:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 232
    iget-object v0, p0, Lcom/b/a/a/a/w;->p:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Proxy;

    .line 233
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_1

    goto :goto_0

    .line 240
    :cond_2
    iput-boolean v3, p0, Lcom/b/a/a/a/w;->i:Z

    .line 241
    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/b/a/f;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 142
    :goto_0
    iget-object v0, p0, Lcom/b/a/a/a/w;->n:Lcom/b/a/g;

    iget-object v1, p0, Lcom/b/a/a/a/w;->a:Lcom/b/a/a;

    invoke-virtual {v0, v1}, Lcom/b/a/g;->a(Lcom/b/a/a;)Lcom/b/a/f;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 143
    iget-object v1, p0, Lcom/b/a/a/a/w;->f:Lcom/b/a/u;

    iget-object v1, v1, Lcom/b/a/u;->b:Ljava/lang/String;

    const-string v4, "GET"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    invoke-virtual {v1, v0}, Lcom/b/a/a/a;->c(Lcom/b/a/f;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 172
    :cond_0
    :goto_1
    return-object v0

    .line 144
    :cond_1
    iget-object v0, v0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    if-nez v0, :cond_a

    .line 149
    iget-object v0, p0, Lcom/b/a/a/a/w;->j:[Ljava/net/InetAddress;

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_8

    .line 150
    iget-boolean v0, p0, Lcom/b/a/a/a/w;->i:Z

    if-nez v0, :cond_7

    .line 151
    iget-object v0, p0, Lcom/b/a/a/a/w;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    :goto_4
    if-nez v0, :cond_6

    .line 152
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_3
    move v0, v3

    .line 148
    goto :goto_2

    :cond_4
    move v0, v3

    .line 149
    goto :goto_3

    :cond_5
    move v0, v3

    .line 151
    goto :goto_4

    .line 154
    :cond_6
    new-instance v1, Lcom/b/a/f;

    iget-object v2, p0, Lcom/b/a/a/a/w;->n:Lcom/b/a/g;

    iget-object v0, p0, Lcom/b/a/a/a/w;->l:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/aa;

    invoke-direct {v1, v2, v0}, Lcom/b/a/f;-><init>(Lcom/b/a/g;Lcom/b/a/aa;)V

    move-object v0, v1

    goto :goto_1

    .line 156
    :cond_7
    invoke-direct {p0}, Lcom/b/a/a/a/w;->c()Ljava/net/Proxy;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/w;->g:Ljava/net/Proxy;

    .line 157
    iget-object v0, p0, Lcom/b/a/a/a/w;->g:Ljava/net/Proxy;

    iput-object v7, p0, Lcom/b/a/a/a/w;->j:[Ljava/net/InetAddress;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v4, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v1, v4, :cond_b

    iget-object v0, p0, Lcom/b/a/a/a/w;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/b/a/a/a/w;->b:Ljava/net/URI;

    invoke-static {v1}, Lcom/b/a/a/i;->a(Ljava/net/URI;)I

    move-result v1

    iput v1, p0, Lcom/b/a/a/a/w;->r:I

    :goto_5
    iget-object v1, p0, Lcom/b/a/a/a/w;->m:Lcom/b/a/n;

    invoke-interface {v1, v0}, Lcom/b/a/n;->a(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/a/a/w;->j:[Ljava/net/InetAddress;

    iput v3, p0, Lcom/b/a/a/a/w;->q:I

    .line 159
    :cond_8
    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/b/a/a/a/w;->j:[Ljava/net/InetAddress;

    iget v4, p0, Lcom/b/a/a/a/w;->q:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/b/a/a/a/w;->q:I

    aget-object v1, v1, v4

    iget v4, p0, Lcom/b/a/a/a/w;->r:I

    invoke-direct {v0, v1, v4}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iget v1, p0, Lcom/b/a/a/a/w;->q:I

    iget-object v4, p0, Lcom/b/a/a/a/w;->j:[Ljava/net/InetAddress;

    array-length v4, v4

    if-ne v1, v4, :cond_9

    iput-object v7, p0, Lcom/b/a/a/a/w;->j:[Ljava/net/InetAddress;

    iput v3, p0, Lcom/b/a/a/a/w;->q:I

    :cond_9
    iput-object v0, p0, Lcom/b/a/a/a/w;->h:Ljava/net/InetSocketAddress;

    .line 160
    iget-object v0, p0, Lcom/b/a/a/a/w;->a:Lcom/b/a/a;

    iget-object v0, v0, Lcom/b/a/a;->e:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_d

    const-string v0, "TLSv1"

    :goto_6
    iput-object v0, p0, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    .line 163
    :cond_a
    invoke-virtual {p0}, Lcom/b/a/a/a/w;->b()Ljava/lang/String;

    move-result-object v0

    .line 164
    new-instance v1, Lcom/b/a/aa;

    iget-object v4, p0, Lcom/b/a/a/a/w;->a:Lcom/b/a/a;

    iget-object v5, p0, Lcom/b/a/a/a/w;->g:Ljava/net/Proxy;

    iget-object v6, p0, Lcom/b/a/a/a/w;->h:Ljava/net/InetSocketAddress;

    invoke-direct {v1, v4, v5, v6, v0}, Lcom/b/a/aa;-><init>(Lcom/b/a/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Ljava/lang/String;)V

    .line 165
    iget-object v0, p0, Lcom/b/a/a/a/w;->e:Lcom/b/a/a/h;

    invoke-virtual {v0, v1}, Lcom/b/a/a/h;->c(Lcom/b/a/aa;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 166
    iget-object v0, p0, Lcom/b/a/a/a/w;->l:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 157
    :cond_b
    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    instance-of v1, v0, Ljava/net/InetSocketAddress;

    if-nez v1, :cond_c

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Proxy.address() is not an InetSocketAddress: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_c
    check-cast v0, Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    iput v0, p0, Lcom/b/a/a/a/w;->r:I

    move-object v0, v1

    goto/16 :goto_5

    .line 160
    :cond_d
    const-string v0, "SSLv3"

    goto :goto_6

    .line 172
    :cond_e
    new-instance v0, Lcom/b/a/f;

    iget-object v2, p0, Lcom/b/a/a/a/w;->n:Lcom/b/a/g;

    invoke-direct {v0, v2, v1}, Lcom/b/a/f;-><init>(Lcom/b/a/g;Lcom/b/a/aa;)V

    goto/16 :goto_1
.end method

.method b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 301
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No next TLS version"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    const-string v1, "TLSv1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 303
    const-string v0, "SSLv3"

    iput-object v0, p0, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    .line 304
    const-string v0, "TLSv1"

    .line 307
    :goto_0
    return-object v0

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    const-string v1, "SSLv3"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 306
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/b/a/a/a/w;->k:Ljava/lang/String;

    .line 307
    const-string v0, "SSLv3"

    goto :goto_0

    .line 309
    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
