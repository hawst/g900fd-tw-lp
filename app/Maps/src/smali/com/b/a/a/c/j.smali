.class public final Lcom/b/a/a/c/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/b/a/a/c/as;


# static fields
.field static final a:Ljava/util/logging/Logger;

.field static final b:Lc/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/b/a/a/c/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/j;->a:Ljava/util/logging/Logger;

    .line 49
    const-string v0, "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"

    invoke-static {v0}, Lc/k;->a(Ljava/lang/String;)Lc/k;

    move-result-object v0

    sput-object v0, Lcom/b/a/a/c/j;->b:Lc/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 670
    return-void
.end method

.method static synthetic a(IBS)I
    .locals 4

    .prologue
    .line 42
    and-int/lit8 v0, p1, 0x8

    if-eqz v0, :cond_0

    add-int/lit8 p0, p0, -0x1

    :cond_0
    if-le p2, p0, :cond_1

    const-string v0, "PROTOCOL_ERROR padding %s > remaining length %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    new-instance v2, Ljava/io/IOException;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    sub-int v0, p0, p2

    int-to-short v0, v0

    return v0
.end method

.method static synthetic a(Lc/j;)I
    .locals 2

    .prologue
    .line 42
    invoke-interface {p0}, Lc/j;->g()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    invoke-interface {p0}, Lc/j;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    invoke-interface {p0}, Lc/j;->g()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a(Lc/j;Z)Lcom/b/a/a/c/b;
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lcom/b/a/a/c/m;

    const/16 v1, 0x1000

    invoke-direct {v0, p1, v1, p2}, Lcom/b/a/a/c/m;-><init>(Lc/j;IZ)V

    return-object v0
.end method

.method public final a(Lc/i;Z)Lcom/b/a/a/c/d;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/b/a/a/c/n;

    invoke-direct {v0, p1, p2}, Lcom/b/a/a/c/n;-><init>(Lc/i;Z)V

    return-object v0
.end method
