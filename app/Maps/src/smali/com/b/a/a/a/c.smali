.class public final Lcom/b/a/a/a/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/b/a/u;

.field public final b:Lcom/b/a/x;


# direct methods
.method public constructor <init>(Lcom/b/a/u;Lcom/b/a/x;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/b/a/a/a/c;->a:Lcom/b/a/u;

    .line 28
    iput-object p2, p0, Lcom/b/a/a/a/c;->b:Lcom/b/a/x;

    .line 29
    return-void
.end method

.method public static a(Lcom/b/a/x;Lcom/b/a/u;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38
    iget v0, p0, Lcom/b/a/x;->c:I

    .line 39
    const/16 v2, 0xc8

    if-eq v0, v2, :cond_0

    const/16 v2, 0xcb

    if-eq v0, v2, :cond_0

    const/16 v2, 0x12c

    if-eq v0, v2, :cond_0

    const/16 v2, 0x12d

    if-eq v0, v2, :cond_0

    const/16 v2, 0x19a

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 61
    :goto_0
    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/b/a/x;->k:Lcom/b/a/d;

    if-eqz v0, :cond_1

    .line 50
    :goto_1
    const-string v2, "Authorization"

    iget-object v3, p1, Lcom/b/a/u;->c:Lcom/b/a/l;

    invoke-virtual {v3, v2}, Lcom/b/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-boolean v2, v0, Lcom/b/a/d;->e:Z

    if-nez v2, :cond_2

    iget-boolean v2, v0, Lcom/b/a/d;->f:Z

    if-nez v2, :cond_2

    iget v2, v0, Lcom/b/a/d;->d:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 54
    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p0, Lcom/b/a/x;->f:Lcom/b/a/l;

    invoke-static {v0}, Lcom/b/a/d;->a(Lcom/b/a/l;)Lcom/b/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/b/a/x;->k:Lcom/b/a/d;

    goto :goto_1

    .line 57
    :cond_2
    iget-boolean v0, v0, Lcom/b/a/d;->b:Z

    if-eqz v0, :cond_3

    move v0, v1

    .line 58
    goto :goto_0

    .line 61
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
