.class Lcom/b/a/a/a/l;
.super Lcom/b/a/a/a/g;
.source "PG"

# interfaces
.implements Lc/aa;


# instance fields
.field final synthetic d:Lcom/b/a/a/a/f;

.field private e:Z


# direct methods
.method constructor <init>(Lcom/b/a/a/a/f;Lcom/b/a/a/a/b;)V
    .locals 0

    .prologue
    .line 559
    iput-object p1, p0, Lcom/b/a/a/a/l;->d:Lcom/b/a/a/a/f;

    .line 560
    invoke-direct {p0, p1, p2}, Lcom/b/a/a/a/g;-><init>(Lcom/b/a/a/a/f;Lcom/b/a/a/a/b;)V

    .line 561
    return-void
.end method


# virtual methods
.method public final a()Lc/ab;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/b/a/a/a/l;->d:Lcom/b/a/a/a/f;

    iget-object v0, v0, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v0}, Lc/j;->a()Lc/ab;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lc/f;J)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 565
    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x23

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 566
    :cond_0
    iget-boolean v2, p0, Lcom/b/a/a/a/l;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 567
    :cond_1
    iget-boolean v2, p0, Lcom/b/a/a/a/l;->e:Z

    if-eqz v2, :cond_2

    .line 576
    :goto_0
    return-wide v0

    .line 569
    :cond_2
    iget-object v2, p0, Lcom/b/a/a/a/l;->d:Lcom/b/a/a/a/f;

    iget-object v2, v2, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v2, p1, p2, p3}, Lc/j;->b(Lc/f;J)J

    move-result-wide v2

    .line 570
    cmp-long v4, v2, v0

    if-nez v4, :cond_3

    .line 571
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/b/a/a/a/l;->e:Z

    .line 572
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/b/a/a/a/l;->a(Z)V

    goto :goto_0

    .line 575
    :cond_3
    invoke-virtual {p0, p1, v2, v3}, Lcom/b/a/a/a/l;->a(Lc/f;J)V

    move-wide v0, v2

    .line 576
    goto :goto_0
.end method

.method public close()V
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Lcom/b/a/a/a/l;->b:Z

    if-eqz v0, :cond_0

    .line 590
    :goto_0
    return-void

    .line 586
    :cond_0
    iget-boolean v0, p0, Lcom/b/a/a/a/l;->e:Z

    if-nez v0, :cond_1

    .line 587
    invoke-virtual {p0}, Lcom/b/a/a/a/l;->at_()V

    .line 589
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/b/a/a/a/l;->b:Z

    goto :goto_0
.end method
