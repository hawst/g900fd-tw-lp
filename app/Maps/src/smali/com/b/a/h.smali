.class Lcom/b/a/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/b/a/g;


# direct methods
.method constructor <init>(Lcom/b/a/g;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    const/4 v14, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 87
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    iget-object v8, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    monitor-enter v8

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    iget-object v0, v0, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    iget-object v1, v1, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v9

    move v1, v3

    .line 91
    :goto_0
    invoke-interface {v9}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    invoke-interface {v9}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/f;

    .line 93
    invoke-virtual {v0}, Lcom/b/a/f;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    iget-wide v10, v2, Lcom/b/a/g;->b:J

    iget-object v2, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-nez v2, :cond_3

    iget-wide v6, v0, Lcom/b/a/f;->h:J

    :goto_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v12

    sub-long v10, v12, v10

    cmp-long v2, v6, v10

    if-gez v2, :cond_4

    move v2, v4

    :goto_2
    if-eqz v2, :cond_5

    .line 94
    :cond_0
    invoke-interface {v9}, Ljava/util/ListIterator;->remove()V

    .line 95
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v14, :cond_c

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    iget-object v0, v0, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    iget-object v2, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    iget-object v2, v2, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v6

    .line 103
    :goto_3
    invoke-interface {v6}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/b/a/h;->a:Lcom/b/a/g;

    iget v0, v0, Lcom/b/a/g;->a:I

    if-le v1, v0, :cond_9

    .line 104
    invoke-interface {v6}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/f;

    .line 105
    iget-object v2, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    invoke-virtual {v2}, Lcom/b/a/a/c/ac;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_2
    move v2, v4

    :goto_4
    if-eqz v2, :cond_b

    .line 106
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-interface {v6}, Ljava/util/ListIterator;->remove()V

    .line 108
    add-int/lit8 v0, v1, -0x1

    :goto_5
    move v1, v0

    .line 110
    goto :goto_3

    .line 93
    :cond_3
    iget-object v2, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    invoke-virtual {v2}, Lcom/b/a/a/c/ac;->b()J

    move-result-wide v6

    goto :goto_1

    :cond_4
    move v2, v3

    goto :goto_2

    .line 97
    :cond_5
    iget-object v2, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v2, :cond_6

    iget-object v0, v0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    invoke-virtual {v0}, Lcom/b/a/a/c/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v4

    :goto_6
    if-eqz v0, :cond_c

    .line 98
    add-int/lit8 v0, v1, 0x1

    :goto_7
    move v1, v0

    .line 100
    goto/16 :goto_0

    :cond_7
    move v0, v3

    .line 97
    goto :goto_6

    :cond_8
    move v2, v3

    .line 105
    goto :goto_4

    .line 111
    :cond_9
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/f;

    .line 113
    iget-object v0, v0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/Socket;)V

    goto :goto_8

    .line 111
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 115
    :cond_a
    return-void

    :cond_b
    move v0, v1

    goto :goto_5

    :cond_c
    move v0, v1

    goto :goto_7
.end method
