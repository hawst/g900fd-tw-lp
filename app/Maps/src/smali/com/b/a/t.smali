.class public final enum Lcom/b/a/t;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/b/a/t;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/b/a/t;

.field public static final enum b:Lcom/b/a/t;

.field public static final enum c:Lcom/b/a/t;

.field public static final enum d:Lcom/b/a/t;

.field private static final synthetic f:[Lcom/b/a/t;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 37
    new-instance v0, Lcom/b/a/t;

    const-string v1, "HTTP_1_0"

    const-string v2, "http/1.0"

    invoke-direct {v0, v1, v3, v2}, Lcom/b/a/t;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/b/a/t;->a:Lcom/b/a/t;

    .line 46
    new-instance v0, Lcom/b/a/t;

    const-string v1, "HTTP_1_1"

    const-string v2, "http/1.1"

    invoke-direct {v0, v1, v4, v2}, Lcom/b/a/t;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/b/a/t;->b:Lcom/b/a/t;

    .line 58
    new-instance v0, Lcom/b/a/t;

    const-string v1, "SPDY_3"

    const-string v2, "spdy/3.1"

    invoke-direct {v0, v1, v5, v2}, Lcom/b/a/t;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/b/a/t;->c:Lcom/b/a/t;

    .line 72
    new-instance v0, Lcom/b/a/t;

    const-string v1, "HTTP_2"

    const-string v2, "h2-14"

    invoke-direct {v0, v1, v6, v2}, Lcom/b/a/t;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/b/a/t;->d:Lcom/b/a/t;

    .line 32
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/b/a/t;

    sget-object v1, Lcom/b/a/t;->a:Lcom/b/a/t;

    aput-object v1, v0, v3

    sget-object v1, Lcom/b/a/t;->b:Lcom/b/a/t;

    aput-object v1, v0, v4

    sget-object v1, Lcom/b/a/t;->c:Lcom/b/a/t;

    aput-object v1, v0, v5

    sget-object v1, Lcom/b/a/t;->d:Lcom/b/a/t;

    aput-object v1, v0, v6

    sput-object v0, Lcom/b/a/t;->f:[Lcom/b/a/t;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput-object p3, p0, Lcom/b/a/t;->e:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/b/a/t;
    .locals 4

    .prologue
    .line 86
    sget-object v0, Lcom/b/a/t;->a:Lcom/b/a/t;

    iget-object v0, v0, Lcom/b/a/t;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/b/a/t;->a:Lcom/b/a/t;

    .line 89
    :goto_0
    return-object v0

    .line 87
    :cond_0
    sget-object v0, Lcom/b/a/t;->b:Lcom/b/a/t;

    iget-object v0, v0, Lcom/b/a/t;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/b/a/t;->b:Lcom/b/a/t;

    goto :goto_0

    .line 88
    :cond_1
    sget-object v0, Lcom/b/a/t;->d:Lcom/b/a/t;

    iget-object v0, v0, Lcom/b/a/t;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/b/a/t;->d:Lcom/b/a/t;

    goto :goto_0

    .line 89
    :cond_2
    sget-object v0, Lcom/b/a/t;->c:Lcom/b/a/t;

    iget-object v0, v0, Lcom/b/a/t;->e:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/b/a/t;->c:Lcom/b/a/t;

    goto :goto_0

    .line 90
    :cond_3
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Unexpected protocol: "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/b/a/t;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/b/a/t;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/b/a/t;

    return-object v0
.end method

.method public static values()[Lcom/b/a/t;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/b/a/t;->f:[Lcom/b/a/t;

    invoke-virtual {v0}, [Lcom/b/a/t;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/b/a/t;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/b/a/t;->e:Ljava/lang/String;

    return-object v0
.end method
