.class public final Lcom/b/a/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/b/a/g;

.field public final b:Lcom/b/a/aa;

.field public c:Ljava/net/Socket;

.field d:Z

.field e:Lcom/b/a/a/a/f;

.field f:Lcom/b/a/a/c/ac;

.field public g:Lcom/b/a/t;

.field h:J

.field public i:Lcom/b/a/k;

.field j:I

.field k:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/b/a/g;Lcom/b/a/aa;)V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/b/a/f;->d:Z

    .line 72
    sget-object v0, Lcom/b/a/t;->b:Lcom/b/a/t;

    iput-object v0, p0, Lcom/b/a/f;->g:Lcom/b/a/t;

    .line 85
    iput-object p1, p0, Lcom/b/a/f;->a:Lcom/b/a/g;

    .line 86
    iput-object p2, p0, Lcom/b/a/f;->b:Lcom/b/a/aa;

    .line 87
    return-void
.end method


# virtual methods
.method a(Lcom/b/a/u;II)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 386
    new-instance v0, Lcom/b/a/a/a/f;

    iget-object v1, p0, Lcom/b/a/f;->a:Lcom/b/a/g;

    iget-object v2, p0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-direct {v0, v1, p0, v2}, Lcom/b/a/a/a/f;-><init>(Lcom/b/a/g;Lcom/b/a/f;Ljava/net/Socket;)V

    .line 387
    invoke-virtual {v0, p2, p3}, Lcom/b/a/a/a/f;->a(II)V

    .line 388
    invoke-virtual {p1}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v1

    .line 389
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Ljava/net/URL;->getPort()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "CONNECT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " HTTP/1.1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 391
    :cond_0
    iget-object v2, p1, Lcom/b/a/u;->c:Lcom/b/a/l;

    invoke-virtual {v0, v2, v1}, Lcom/b/a/a/a/f;->a(Lcom/b/a/l;Ljava/lang/String;)V

    .line 392
    iget-object v2, v0, Lcom/b/a/a/a/f;->d:Lc/i;

    invoke-interface {v2}, Lc/i;->b()V

    .line 393
    invoke-virtual {v0}, Lcom/b/a/a/a/f;->b()Lcom/b/a/y;

    move-result-object v2

    iput-object p1, v2, Lcom/b/a/y;->a:Lcom/b/a/u;

    invoke-virtual {v2}, Lcom/b/a/y;->a()Lcom/b/a/x;

    move-result-object v2

    .line 394
    const/4 v3, 0x0

    invoke-virtual {v0, v3, v6, v7}, Lcom/b/a/a/a/f;->a(Lcom/b/a/a/a/b;J)Lc/aa;

    .line 396
    iget v3, v2, Lcom/b/a/x;->c:I

    sparse-switch v3, :sswitch_data_0

    .line 412
    new-instance v0, Ljava/io/IOException;

    iget v1, v2, Lcom/b/a/x;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x31

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected response code for CONNECT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 400
    :sswitch_0
    iget-object v0, v0, Lcom/b/a/a/a/f;->c:Lc/j;

    invoke-interface {v0}, Lc/j;->c()Lc/f;

    move-result-object v0

    iget-wide v0, v0, Lc/f;->b:J

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    .line 401
    new-instance v0, Ljava/io/IOException;

    const-string v1, "TLS tunnel buffered too many bytes!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :sswitch_1
    iget-object v3, p0, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v3, v3, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v3, v3, Lcom/b/a/a;->g:Lcom/b/a/b;

    iget-object v4, p0, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v4, v4, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    invoke-static {v3, v2, v4}, Lcom/b/a/a/a/s;->a(Lcom/b/a/b;Lcom/b/a/x;Ljava/net/Proxy;)Lcom/b/a/u;

    move-result-object p1

    .line 408
    if-nez p1, :cond_0

    .line 409
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Failed to authenticate with proxy"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_1
    return-void

    .line 396
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x197 -> :sswitch_1
    .end sparse-switch
.end method

.method final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 96
    iget-object v0, p0, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 100
    :goto_1
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 97
    :cond_1
    iget-object v1, p0, Lcom/b/a/f;->a:Lcom/b/a/g;

    monitor-enter v1

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/b/a/f;->k:Ljava/lang/Object;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Connection already has an owner!"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 99
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/b/a/f;->k:Ljava/lang/Object;

    .line 100
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method final a()Z
    .locals 2

    .prologue
    .line 110
    iget-object v1, p0, Lcom/b/a/f;->a:Lcom/b/a/g;

    monitor-enter v1

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/b/a/f;->k:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 113
    const/4 v0, 0x0

    monitor-exit v1

    .line 117
    :goto_0
    return v0

    .line 116
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/b/a/f;->k:Ljava/lang/Object;

    .line 117
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
