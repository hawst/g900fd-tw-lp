.class final Lcom/b/a/q;
.super Lcom/b/a/a/a;
.source "PG"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/b/a/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/b/a/f;Lcom/b/a/a/a/o;)Lcom/b/a/a/a/aa;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p1, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/b/a/a/a/x;

    iget-object v1, p1, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    invoke-direct {v0, p2, v1}, Lcom/b/a/a/a/x;-><init>(Lcom/b/a/a/a/o;Lcom/b/a/a/c/ac;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/b/a/a/a/r;

    iget-object v1, p1, Lcom/b/a/f;->e:Lcom/b/a/a/a/f;

    invoke-direct {v0, p2, v1}, Lcom/b/a/a/a/r;-><init>(Lcom/b/a/a/a/o;Lcom/b/a/a/a/f;)V

    goto :goto_0
.end method

.method public final a(Lcom/b/a/p;)Lcom/b/a/a/b;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p1, Lcom/b/a/p;->f:Lcom/b/a/a/b;

    return-object v0
.end method

.method public final a(Lcom/b/a/f;Lcom/b/a/t;)V
    .locals 2

    .prologue
    .line 71
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "protocol == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p1, Lcom/b/a/f;->g:Lcom/b/a/t;

    .line 72
    return-void
.end method

.method public final a(Lcom/b/a/f;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p1, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lcom/b/a/f;->a:Lcom/b/a/g;

    monitor-enter v1

    :try_start_0
    iget-object v0, p1, Lcom/b/a/f;->k:Ljava/lang/Object;

    if-eq v0, p2, :cond_2

    monitor-exit v1

    .line 64
    :goto_1
    return-void

    .line 63
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p1, Lcom/b/a/f;->k:Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p1, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/b/a/g;Lcom/b/a/f;)V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p2, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/b/a/f;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/b/a/f;->b()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/Socket;)V

    .line 96
    :cond_0
    :goto_1
    return-void

    .line 95
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    move-result-object v0

    iget-object v1, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v0, v1}, Lcom/b/a/a/d;->b(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    monitor-enter p1

    :try_start_1
    iget-object v0, p1, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    iget v0, p2, Lcom/b/a/f;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/b/a/f;->j:I

    iget-object v0, p2, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "spdyConnection != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unable to untagSocket(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/d;->a(Ljava/lang/String;)V

    iget-object v0, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/Socket;)V

    goto :goto_1

    :cond_3
    :try_start_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p2, Lcom/b/a/f;->h:J

    monitor-exit p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p1, Lcom/b/a/g;->d:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p1, Lcom/b/a/g;->e:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(Lcom/b/a/m;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 83
    const-string v0, ":"

    invoke-virtual {p2, v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v1, p1, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :goto_0
    return-void

    .line 83
    :cond_0
    const-string v0, ":"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, ""

    invoke-virtual {p2, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    const-string v0, ""

    iget-object v1, p1, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p1, Lcom/b/a/m;->a:Ljava/util/List;

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/b/a/p;Lcom/b/a/f;Lcom/b/a/a/a/o;Lcom/b/a/u;)V
    .locals 10

    .prologue
    const/high16 v4, 0x10000

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 104
    invoke-virtual {p2, p3}, Lcom/b/a/f;->a(Ljava/lang/Object;)V

    iget-boolean v0, p2, Lcom/b/a/f;->d:Z

    if-nez v0, :cond_18

    iget-object v0, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v1, v0, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v1, v1, Lcom/b/a/a;->e:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    iget v1, p1, Lcom/b/a/p;->p:I

    iget v5, p1, Lcom/b/a/p;->q:I

    iget v6, p1, Lcom/b/a/p;->r:I

    iget-boolean v7, p2, Lcom/b/a/f;->d:Z

    if-eqz v7, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p4}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lcom/b/a/u;->a()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/net/URL;)I

    move-result v5

    const-string v0, "https"

    invoke-static {v0}, Lcom/b/a/a/i;->a(Ljava/lang/String;)I

    move-result v0

    if-ne v5, v0, :cond_2

    move-object v0, v1

    :goto_2
    new-instance v6, Lcom/b/a/v;

    invoke-direct {v6}, Lcom/b/a/v;-><init>()V

    new-instance v7, Ljava/net/URL;

    const-string v8, "https"

    const-string v9, "/"

    invoke-direct {v7, v8, v1, v5, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    if-nez v7, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0xc

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, ":"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_3
    iput-object v7, v6, Lcom/b/a/v;->b:Ljava/net/URL;

    invoke-virtual {v7}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lcom/b/a/v;->a:Ljava/lang/String;

    const-string v1, "Host"

    invoke-virtual {v6, v1, v0}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    move-result-object v0

    const-string v1, "Proxy-Connection"

    const-string v5, "Keep-Alive"

    invoke-virtual {v0, v1, v5}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    move-result-object v1

    const-string v0, "User-Agent"

    invoke-virtual {p4, v0}, Lcom/b/a/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v5, "User-Agent"

    invoke-virtual {v1, v5, v0}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_4
    const-string v0, "Proxy-Authorization"

    invoke-virtual {p4, v0}, Lcom/b/a/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const-string v5, "Proxy-Authorization"

    invoke-virtual {v1, v5, v0}, Lcom/b/a/v;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/b/a/v;

    :cond_5
    iget-object v0, v1, Lcom/b/a/v;->a:Ljava/lang/String;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Lcom/b/a/u;

    invoke-direct {v0, v1}, Lcom/b/a/u;-><init>(Lcom/b/a/v;)V

    goto/16 :goto_1

    :cond_7
    iget-object v7, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v7, v7, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    invoke-virtual {v7}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v7

    sget-object v8, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v7, v8, :cond_8

    iget-object v7, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v7, v7, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    invoke-virtual {v7}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v7

    sget-object v8, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v7, v8, :cond_b

    :cond_8
    iget-object v7, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v7, v7, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v7, v7, Lcom/b/a/a;->d:Ljavax/net/SocketFactory;

    invoke-virtual {v7}, Ljavax/net/SocketFactory;->createSocket()Ljava/net/Socket;

    move-result-object v7

    iput-object v7, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    :goto_3
    iget-object v7, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v7, v5}, Ljava/net/Socket;->setSoTimeout(I)V

    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    move-result-object v7

    iget-object v8, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    iget-object v9, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v9, v9, Lcom/b/a/aa;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v7, v8, v9, v1}, Lcom/b/a/a/d;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V

    iget-object v1, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v1, v1, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v1, v1, Lcom/b/a/a;->e:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v1, :cond_13

    invoke-static {}, Lcom/b/a/a/d;->a()Lcom/b/a/a/d;

    move-result-object v7

    if-eqz v0, :cond_9

    invoke-virtual {p2, v0, v5, v6}, Lcom/b/a/f;->a(Lcom/b/a/u;II)V

    :cond_9
    iget-object v0, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v0, v0, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v0, v0, Lcom/b/a/a;->e:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v1, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    iget-object v5, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v5, v5, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v5, v5, Lcom/b/a/a;->b:Ljava/lang/String;

    iget-object v6, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v6, v6, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget v6, v6, Lcom/b/a/a;->c:I

    invoke-virtual {v0, v1, v5, v6, v2}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    iget-object v0, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iget-object v1, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v1, v1, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v1, v1, Lcom/b/a/a;->b:Ljava/lang/String;

    iget-object v5, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v5, v5, Lcom/b/a/aa;->d:Ljava/lang/String;

    invoke-virtual {v7, v0, v1, v5}, Lcom/b/a/a/d;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v1, v1, Lcom/b/a/aa;->d:Ljava/lang/String;

    const-string v5, "SSLv3"

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    move v1, v2

    :goto_4
    if-eqz v1, :cond_a

    iget-object v5, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v5, v5, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v5, v5, Lcom/b/a/a;->h:Ljava/util/List;

    invoke-virtual {v7, v0, v5}, Lcom/b/a/a/d;->a(Ljavax/net/ssl/SSLSocket;Ljava/util/List;)V

    :cond_a
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    iget-object v5, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v5, v5, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v5, v5, Lcom/b/a/a;->f:Ljavax/net/ssl/HostnameVerifier;

    iget-object v6, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v6, v6, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v6, v6, Lcom/b/a/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v8

    invoke-interface {v5, v6, v8}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v5

    if-nez v5, :cond_d

    new-instance v0, Ljava/io/IOException;

    iget-object v1, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v1, v1, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v1, v1, Lcom/b/a/a;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Hostname \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' was not verified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    new-instance v7, Ljava/net/Socket;

    iget-object v8, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v8, v8, Lcom/b/a/aa;->b:Ljava/net/Proxy;

    invoke-direct {v7, v8}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    iput-object v7, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    goto/16 :goto_3

    :cond_c
    move v1, v3

    goto :goto_4

    :cond_d
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v5

    invoke-static {v5}, Lcom/b/a/k;->a(Ljavax/net/ssl/SSLSession;)Lcom/b/a/k;

    move-result-object v5

    iput-object v5, p2, Lcom/b/a/f;->i:Lcom/b/a/k;

    if-eqz v1, :cond_e

    invoke-virtual {v7, v0}, Lcom/b/a/a/d;->a(Ljavax/net/ssl/SSLSocket;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-static {v1}, Lcom/b/a/t;->a(Ljava/lang/String;)Lcom/b/a/t;

    move-result-object v1

    iput-object v1, p2, Lcom/b/a/f;->g:Lcom/b/a/t;

    :cond_e
    iget-object v1, p2, Lcom/b/a/f;->g:Lcom/b/a/t;

    sget-object v5, Lcom/b/a/t;->c:Lcom/b/a/t;

    if-eq v1, v5, :cond_f

    iget-object v1, p2, Lcom/b/a/f;->g:Lcom/b/a/t;

    sget-object v5, Lcom/b/a/t;->d:Lcom/b/a/t;

    if-ne v1, v5, :cond_12

    :cond_f
    invoke-virtual {v0, v3}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    new-instance v0, Lcom/b/a/a/c/ak;

    iget-object v1, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    iget-object v1, v1, Lcom/b/a/aa;->a:Lcom/b/a/a;

    iget-object v1, v1, Lcom/b/a/a;->b:Ljava/lang/String;

    iget-object v5, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-direct {v0, v1, v2, v5}, Lcom/b/a/a/c/ak;-><init>(Ljava/lang/String;ZLjava/net/Socket;)V

    iget-object v1, p2, Lcom/b/a/f;->g:Lcom/b/a/t;

    iput-object v1, v0, Lcom/b/a/a/c/ak;->d:Lcom/b/a/t;

    new-instance v1, Lcom/b/a/a/c/ac;

    invoke-direct {v1, v0}, Lcom/b/a/a/c/ac;-><init>(Lcom/b/a/a/c/ak;)V

    iput-object v1, p2, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    iget-object v1, p2, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    iget-object v0, v1, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    invoke-interface {v0}, Lcom/b/a/a/c/d;->a()V

    iget-object v0, v1, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    iget-object v5, v1, Lcom/b/a/a/c/ac;->n:Lcom/b/a/a/c/y;

    invoke-interface {v0, v5}, Lcom/b/a/a/c/d;->b(Lcom/b/a/a/c/y;)V

    iget-object v0, v1, Lcom/b/a/a/c/ac;->n:Lcom/b/a/a/c/y;

    iget v5, v0, Lcom/b/a/a/c/y;->a:I

    and-int/lit16 v5, v5, 0x80

    if-eqz v5, :cond_11

    iget-object v0, v0, Lcom/b/a/a/c/y;->d:[I

    const/4 v5, 0x7

    aget v0, v0, v5

    :goto_5
    if-eq v0, v4, :cond_10

    iget-object v1, v1, Lcom/b/a/a/c/ac;->s:Lcom/b/a/a/c/d;

    sub-int/2addr v0, v4

    int-to-long v4, v0

    invoke-interface {v1, v3, v4, v5}, Lcom/b/a/a/c/d;->a(IJ)V

    :cond_10
    :goto_6
    iput-boolean v2, p2, Lcom/b/a/f;->d:Z

    iget-object v0, p2, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_14

    move v0, v2

    :goto_7
    if-eqz v0, :cond_17

    iget-object v1, p1, Lcom/b/a/p;->l:Lcom/b/a/g;

    iget-object v0, p2, Lcom/b/a/f;->f:Lcom/b/a/a/c/ac;

    if-eqz v0, :cond_15

    move v0, v2

    :goto_8
    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_11
    move v0, v4

    goto :goto_5

    :cond_12
    new-instance v0, Lcom/b/a/a/a/f;

    iget-object v1, p2, Lcom/b/a/f;->a:Lcom/b/a/g;

    iget-object v4, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-direct {v0, v1, p2, v4}, Lcom/b/a/a/a/f;-><init>(Lcom/b/a/g;Lcom/b/a/f;Ljava/net/Socket;)V

    iput-object v0, p2, Lcom/b/a/f;->e:Lcom/b/a/a/a/f;

    goto :goto_6

    :cond_13
    new-instance v0, Lcom/b/a/a/a/f;

    iget-object v1, p2, Lcom/b/a/f;->a:Lcom/b/a/g;

    iget-object v4, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-direct {v0, v1, p2, v4}, Lcom/b/a/a/a/f;-><init>(Lcom/b/a/g;Lcom/b/a/f;Ljava/net/Socket;)V

    iput-object v0, p2, Lcom/b/a/f;->e:Lcom/b/a/a/a/f;

    goto :goto_6

    :cond_14
    move v0, v3

    goto :goto_7

    :cond_15
    move v0, v3

    goto :goto_8

    :cond_16
    iget-object v0, v1, Lcom/b/a/g;->d:Ljava/util/concurrent/ExecutorService;

    iget-object v2, v1, Lcom/b/a/g;->e:Ljava/lang/Runnable;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {p2}, Lcom/b/a/f;->b()Z

    move-result v0

    if-eqz v0, :cond_17

    monitor-enter v1

    :try_start_0
    iget-object v0, v1, Lcom/b/a/g;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_17
    iget-object v0, p1, Lcom/b/a/p;->a:Lcom/b/a/a/h;

    iget-object v1, p2, Lcom/b/a/f;->b:Lcom/b/a/aa;

    invoke-virtual {v0, v1}, Lcom/b/a/a/h;->b(Lcom/b/a/aa;)V

    :cond_18
    iget v0, p1, Lcom/b/a/p;->q:I

    iget v1, p1, Lcom/b/a/p;->r:I

    iget-boolean v2, p2, Lcom/b/a/f;->d:Z

    if-nez v2, :cond_19

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setTimeouts - not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_19
    iget-object v2, p2, Lcom/b/a/f;->e:Lcom/b/a/a/a/f;

    if-eqz v2, :cond_1a

    iget-object v2, p2, Lcom/b/a/f;->c:Ljava/net/Socket;

    invoke-virtual {v2, v0}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v2, p2, Lcom/b/a/f;->e:Lcom/b/a/a/a/f;

    invoke-virtual {v2, v0, v1}, Lcom/b/a/a/a/f;->a(II)V

    .line 105
    :cond_1a
    return-void
.end method

.method public final a(Lcom/b/a/f;)Z
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/b/a/f;->a()Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/b/a/f;)I
    .locals 1

    .prologue
    .line 67
    iget v0, p1, Lcom/b/a/f;->j:I

    return v0
.end method

.method public final b(Lcom/b/a/p;)Lcom/b/a/a/h;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p1, Lcom/b/a/p;->a:Lcom/b/a/a/h;

    return-object v0
.end method

.method public final b(Lcom/b/a/f;Lcom/b/a/a/a/o;)V
    .locals 0

    .prologue
    .line 75
    invoke-virtual {p1, p2}, Lcom/b/a/f;->a(Ljava/lang/Object;)V

    .line 76
    return-void
.end method

.method public final c(Lcom/b/a/f;)Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p1, Lcom/b/a/f;->e:Lcom/b/a/a/a/f;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/b/a/f;->e:Lcom/b/a/a/a/f;

    invoke-virtual {v0}, Lcom/b/a/a/a/f;->a()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
