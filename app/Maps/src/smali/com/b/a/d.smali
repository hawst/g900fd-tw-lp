.class public final Lcom/b/a/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:I

.field public final e:Z

.field public final f:Z

.field public final g:I

.field public final h:I

.field public final i:Z


# direct methods
.method private constructor <init>(ZZIIZZIIZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-boolean p1, p0, Lcom/b/a/d;->a:Z

    .line 28
    iput-boolean p2, p0, Lcom/b/a/d;->b:Z

    .line 29
    iput p3, p0, Lcom/b/a/d;->c:I

    .line 30
    iput p4, p0, Lcom/b/a/d;->d:I

    .line 31
    iput-boolean p5, p0, Lcom/b/a/d;->e:Z

    .line 32
    iput-boolean p6, p0, Lcom/b/a/d;->f:Z

    .line 33
    iput p7, p0, Lcom/b/a/d;->g:I

    .line 34
    iput p8, p0, Lcom/b/a/d;->h:I

    .line 35
    iput-boolean p9, p0, Lcom/b/a/d;->i:Z

    .line 36
    return-void
.end method

.method public static a(Lcom/b/a/l;)Lcom/b/a/d;
    .locals 20

    .prologue
    .line 104
    const/4 v3, 0x0

    .line 105
    const/4 v4, 0x0

    .line 106
    const/4 v5, -0x1

    .line 107
    const/4 v6, -0x1

    .line 108
    const/4 v7, 0x0

    .line 109
    const/4 v8, 0x0

    .line 110
    const/4 v9, -0x1

    .line 111
    const/4 v10, -0x1

    .line 112
    const/4 v11, 0x0

    .line 114
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v12, v12

    div-int/lit8 v12, v12, 0x2

    if-ge v2, v12, :cond_18

    .line 115
    shl-int/lit8 v12, v2, 0x1

    if-ltz v12, :cond_0

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v13, v13

    if-lt v12, v13, :cond_5

    :cond_0
    const/4 v12, 0x0

    :goto_1
    const-string v13, "Cache-Control"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_2

    shl-int/lit8 v12, v2, 0x1

    if-ltz v12, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v13, v13

    if-lt v12, v13, :cond_6

    :cond_1
    const/4 v12, 0x0

    :goto_2
    const-string v13, "Pragma"

    invoke-virtual {v12, v13}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_17

    .line 117
    :cond_2
    shl-int/lit8 v12, v2, 0x1

    add-int/lit8 v12, v12, 0x1

    if-ltz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    array-length v13, v13

    if-lt v12, v13, :cond_7

    :cond_3
    const/4 v12, 0x0

    .line 121
    :goto_3
    const/4 v13, 0x0

    move v14, v3

    move/from16 v19, v4

    move v4, v13

    move/from16 v13, v19

    .line 122
    :cond_4
    :goto_4
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v4, v3, :cond_16

    .line 124
    const-string v15, "=,;"

    move v3, v4

    :goto_5
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v16

    move/from16 v0, v16

    if-ge v3, v0, :cond_8

    invoke-virtual {v12, v3}, Ljava/lang/String;->charAt(I)C

    move-result v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->indexOf(I)I

    move-result v16

    const/16 v17, -0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_8

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 115
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v12, v13, v12

    goto :goto_1

    :cond_6
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v12, v13, v12

    goto :goto_2

    .line 117
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/b/a/l;->a:[Ljava/lang/String;

    aget-object v12, v13, v12

    goto :goto_3

    .line 125
    :cond_8
    invoke-virtual {v12, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    .line 128
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v4

    if-eq v3, v4, :cond_9

    invoke-virtual {v12, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v16, 0x2c

    move/from16 v0, v16

    if-eq v4, v0, :cond_9

    invoke-virtual {v12, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v16, 0x3b

    move/from16 v0, v16

    if-ne v4, v0, :cond_a

    .line 129
    :cond_9
    add-int/lit8 v4, v3, 0x1

    .line 130
    const/4 v3, 0x0

    .line 151
    :goto_6
    const-string v16, "no-cache"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_e

    .line 152
    const/4 v3, 0x1

    move v14, v3

    goto :goto_4

    .line 132
    :cond_a
    add-int/lit8 v3, v3, 0x1

    .line 133
    invoke-static {v12, v3}, Lcom/b/a/a/a/e;->a(Ljava/lang/String;I)I

    move-result v4

    .line 136
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v4, v3, :cond_c

    invoke-virtual {v12, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v16, 0x22

    move/from16 v0, v16

    if-ne v3, v0, :cond_c

    .line 137
    add-int/lit8 v4, v4, 0x1

    .line 139
    const-string v16, "\""

    move v3, v4

    :goto_7
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v3, v0, :cond_b

    invoke-virtual {v12, v3}, Ljava/lang/String;->charAt(I)C

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->indexOf(I)I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_b

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 140
    :cond_b
    invoke-virtual {v12, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 141
    add-int/lit8 v3, v3, 0x1

    move-object/from16 v19, v4

    move v4, v3

    move-object/from16 v3, v19

    .line 144
    goto :goto_6

    .line 146
    :cond_c
    const-string v16, ",;"

    move v3, v4

    :goto_8
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    if-ge v3, v0, :cond_d

    invoke-virtual {v12, v3}, Ljava/lang/String;->charAt(I)C

    move-result v17

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->indexOf(I)I

    move-result v17

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_d

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    .line 147
    :cond_d
    invoke-virtual {v12, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v19, v4

    move v4, v3

    move-object/from16 v3, v19

    goto :goto_6

    .line 153
    :cond_e
    const-string v16, "no-store"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_f

    .line 154
    const/4 v3, 0x1

    move v13, v3

    goto/16 :goto_4

    .line 155
    :cond_f
    const-string v16, "max-age"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_10

    .line 156
    invoke-static {v3}, Lcom/b/a/a/a/e;->a(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_4

    .line 157
    :cond_10
    const-string v16, "s-maxage"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_11

    .line 158
    invoke-static {v3}, Lcom/b/a/a/a/e;->a(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_4

    .line 159
    :cond_11
    const-string v16, "public"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_12

    .line 160
    const/4 v7, 0x1

    goto/16 :goto_4

    .line 161
    :cond_12
    const-string v16, "must-revalidate"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_13

    .line 162
    const/4 v8, 0x1

    goto/16 :goto_4

    .line 163
    :cond_13
    const-string v16, "max-stale"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_14

    .line 164
    invoke-static {v3}, Lcom/b/a/a/a/e;->a(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_4

    .line 165
    :cond_14
    const-string v16, "min-fresh"

    move-object/from16 v0, v16

    invoke-virtual {v0, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_15

    .line 166
    invoke-static {v3}, Lcom/b/a/a/a/e;->a(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_4

    .line 167
    :cond_15
    const-string v3, "only-if-cached"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 168
    const/4 v11, 0x1

    goto/16 :goto_4

    :cond_16
    move v4, v13

    move v3, v14

    .line 114
    :cond_17
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 173
    :cond_18
    new-instance v2, Lcom/b/a/d;

    invoke-direct/range {v2 .. v11}, Lcom/b/a/d;-><init>(ZZIIZZIIZ)V

    return-object v2
.end method
