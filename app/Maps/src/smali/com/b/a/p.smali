.class public Lcom/b/a/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field private static s:Ljavax/net/ssl/SSLSocketFactory;


# instance fields
.field final a:Lcom/b/a/a/h;

.field public b:Ljava/net/Proxy;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/b/a/t;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/net/ProxySelector;

.field public e:Ljava/net/CookieHandler;

.field public f:Lcom/b/a/a/b;

.field public g:Lcom/b/a/c;

.field public h:Ljavax/net/SocketFactory;

.field public i:Ljavax/net/ssl/SSLSocketFactory;

.field public j:Ljavax/net/ssl/HostnameVerifier;

.field public k:Lcom/b/a/b;

.field public l:Lcom/b/a/g;

.field public m:Lcom/b/a/n;

.field public n:Z

.field public o:Z

.field public p:I

.field public q:I

.field r:I

.field private t:Lcom/b/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcom/b/a/q;

    invoke-direct {v0}, Lcom/b/a/q;-><init>()V

    sput-object v0, Lcom/b/a/a/a;->a:Lcom/b/a/a/a;

    .line 107
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-boolean v0, p0, Lcom/b/a/p;->n:Z

    .line 130
    iput-boolean v0, p0, Lcom/b/a/p;->o:Z

    .line 136
    new-instance v0, Lcom/b/a/a/h;

    invoke-direct {v0}, Lcom/b/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/b/a/p;->a:Lcom/b/a/a/h;

    .line 137
    new-instance v0, Lcom/b/a/j;

    invoke-direct {v0}, Lcom/b/a/j;-><init>()V

    iput-object v0, p0, Lcom/b/a/p;->t:Lcom/b/a/j;

    .line 138
    return-void
.end method

.method constructor <init>(Lcom/b/a/p;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-boolean v0, p0, Lcom/b/a/p;->n:Z

    .line 130
    iput-boolean v0, p0, Lcom/b/a/p;->o:Z

    .line 141
    iget-object v0, p1, Lcom/b/a/p;->a:Lcom/b/a/a/h;

    iput-object v0, p0, Lcom/b/a/p;->a:Lcom/b/a/a/h;

    .line 142
    iget-object v0, p1, Lcom/b/a/p;->t:Lcom/b/a/j;

    iput-object v0, p0, Lcom/b/a/p;->t:Lcom/b/a/j;

    .line 143
    iget-object v0, p1, Lcom/b/a/p;->b:Ljava/net/Proxy;

    iput-object v0, p0, Lcom/b/a/p;->b:Ljava/net/Proxy;

    .line 144
    iget-object v0, p1, Lcom/b/a/p;->c:Ljava/util/List;

    iput-object v0, p0, Lcom/b/a/p;->c:Ljava/util/List;

    .line 145
    iget-object v0, p1, Lcom/b/a/p;->d:Ljava/net/ProxySelector;

    iput-object v0, p0, Lcom/b/a/p;->d:Ljava/net/ProxySelector;

    .line 146
    iget-object v0, p1, Lcom/b/a/p;->e:Ljava/net/CookieHandler;

    iput-object v0, p0, Lcom/b/a/p;->e:Ljava/net/CookieHandler;

    .line 147
    iget-object v0, p1, Lcom/b/a/p;->g:Lcom/b/a/c;

    iput-object v0, p0, Lcom/b/a/p;->g:Lcom/b/a/c;

    .line 148
    iget-object v0, p0, Lcom/b/a/p;->g:Lcom/b/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/b/a/p;->g:Lcom/b/a/c;

    iget-object v0, v0, Lcom/b/a/c;->a:Lcom/b/a/a/b;

    :goto_0
    iput-object v0, p0, Lcom/b/a/p;->f:Lcom/b/a/a/b;

    .line 149
    iget-object v0, p1, Lcom/b/a/p;->h:Ljavax/net/SocketFactory;

    iput-object v0, p0, Lcom/b/a/p;->h:Ljavax/net/SocketFactory;

    .line 150
    iget-object v0, p1, Lcom/b/a/p;->i:Ljavax/net/ssl/SSLSocketFactory;

    iput-object v0, p0, Lcom/b/a/p;->i:Ljavax/net/ssl/SSLSocketFactory;

    .line 151
    iget-object v0, p1, Lcom/b/a/p;->j:Ljavax/net/ssl/HostnameVerifier;

    iput-object v0, p0, Lcom/b/a/p;->j:Ljavax/net/ssl/HostnameVerifier;

    .line 152
    iget-object v0, p1, Lcom/b/a/p;->k:Lcom/b/a/b;

    iput-object v0, p0, Lcom/b/a/p;->k:Lcom/b/a/b;

    .line 153
    iget-object v0, p1, Lcom/b/a/p;->l:Lcom/b/a/g;

    iput-object v0, p0, Lcom/b/a/p;->l:Lcom/b/a/g;

    .line 154
    iget-boolean v0, p1, Lcom/b/a/p;->n:Z

    iput-boolean v0, p0, Lcom/b/a/p;->n:Z

    .line 155
    iget-boolean v0, p1, Lcom/b/a/p;->o:Z

    iput-boolean v0, p0, Lcom/b/a/p;->o:Z

    .line 156
    iget v0, p1, Lcom/b/a/p;->p:I

    iput v0, p0, Lcom/b/a/p;->p:I

    .line 157
    iget v0, p1, Lcom/b/a/p;->q:I

    iput v0, p0, Lcom/b/a/p;->q:I

    .line 158
    iget v0, p1, Lcom/b/a/p;->r:I

    iput v0, p0, Lcom/b/a/p;->r:I

    .line 159
    return-void

    .line 148
    :cond_0
    iget-object v0, p1, Lcom/b/a/p;->f:Lcom/b/a/a/b;

    goto :goto_0
.end method


# virtual methods
.method declared-synchronized a()Ljavax/net/ssl/SSLSocketFactory;
    .locals 4

    .prologue
    .line 526
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/b/a/p;->s:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 528
    :try_start_1
    const-string v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    .line 529
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    .line 530
    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    sput-object v0, Lcom/b/a/p;->s:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 535
    :cond_0
    :try_start_2
    sget-object v0, Lcom/b/a/p;->s:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 532
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 526
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)V
    .locals 5

    .prologue
    .line 201
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 203
    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 204
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_2
    long-to-int v0, v0

    iput v0, p0, Lcom/b/a/p;->r:I

    .line 206
    return-void
.end method

.method public final b()Lcom/b/a/p;
    .locals 1

    .prologue
    .line 541
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/b/a/p;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 543
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/b/a/p;->b()Lcom/b/a/p;

    move-result-object v0

    return-object v0
.end method
