.class public final Lcom/b/a/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/net/URLStreamHandlerFactory;


# instance fields
.field final a:Lcom/b/a/p;


# direct methods
.method public constructor <init>(Lcom/b/a/p;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/b/a/r;->a:Lcom/b/a/p;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/b/a/r;->a:Lcom/b/a/p;

    iget-object v0, v0, Lcom/b/a/p;->b:Ljava/net/Proxy;

    invoke-virtual {p0, p1, v0}, Lcom/b/a/r;->a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;
    .locals 5

    .prologue
    .line 65
    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    .line 66
    iget-object v1, p0, Lcom/b/a/r;->a:Lcom/b/a/p;

    new-instance v2, Lcom/b/a/p;

    invoke-direct {v2, v1}, Lcom/b/a/p;-><init>(Lcom/b/a/p;)V

    iget-object v3, v2, Lcom/b/a/p;->d:Ljava/net/ProxySelector;

    if-nez v3, :cond_0

    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v3

    iput-object v3, v2, Lcom/b/a/p;->d:Ljava/net/ProxySelector;

    :cond_0
    iget-object v3, v2, Lcom/b/a/p;->e:Ljava/net/CookieHandler;

    if-nez v3, :cond_1

    invoke-static {}, Ljava/net/CookieHandler;->getDefault()Ljava/net/CookieHandler;

    move-result-object v3

    iput-object v3, v2, Lcom/b/a/p;->e:Ljava/net/CookieHandler;

    :cond_1
    iget-object v3, v2, Lcom/b/a/p;->h:Ljavax/net/SocketFactory;

    if-nez v3, :cond_2

    invoke-static {}, Ljavax/net/SocketFactory;->getDefault()Ljavax/net/SocketFactory;

    move-result-object v3

    iput-object v3, v2, Lcom/b/a/p;->h:Ljavax/net/SocketFactory;

    :cond_2
    iget-object v3, v2, Lcom/b/a/p;->i:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v3, :cond_3

    invoke-virtual {v1}, Lcom/b/a/p;->a()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    iput-object v1, v2, Lcom/b/a/p;->i:Ljavax/net/ssl/SSLSocketFactory;

    :cond_3
    iget-object v1, v2, Lcom/b/a/p;->j:Ljavax/net/ssl/HostnameVerifier;

    if-nez v1, :cond_4

    sget-object v1, Lcom/b/a/a/d/b;->a:Lcom/b/a/a/d/b;

    iput-object v1, v2, Lcom/b/a/p;->j:Ljavax/net/ssl/HostnameVerifier;

    :cond_4
    iget-object v1, v2, Lcom/b/a/p;->k:Lcom/b/a/b;

    if-nez v1, :cond_5

    sget-object v1, Lcom/b/a/a/a/a;->a:Lcom/b/a/b;

    iput-object v1, v2, Lcom/b/a/p;->k:Lcom/b/a/b;

    :cond_5
    iget-object v1, v2, Lcom/b/a/p;->l:Lcom/b/a/g;

    if-nez v1, :cond_6

    invoke-static {}, Lcom/b/a/g;->a()Lcom/b/a/g;

    move-result-object v1

    iput-object v1, v2, Lcom/b/a/p;->l:Lcom/b/a/g;

    :cond_6
    iget-object v1, v2, Lcom/b/a/p;->c:Ljava/util/List;

    if-nez v1, :cond_7

    const/4 v1, 0x3

    new-array v1, v1, [Lcom/b/a/t;

    const/4 v3, 0x0

    sget-object v4, Lcom/b/a/t;->d:Lcom/b/a/t;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/b/a/t;->c:Lcom/b/a/t;

    aput-object v4, v1, v3

    const/4 v3, 0x2

    sget-object v4, Lcom/b/a/t;->b:Lcom/b/a/t;

    aput-object v4, v1, v3

    invoke-static {v1}, Lcom/b/a/a/i;->a([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v2, Lcom/b/a/p;->c:Ljava/util/List;

    :cond_7
    iget-object v1, v2, Lcom/b/a/p;->m:Lcom/b/a/n;

    if-nez v1, :cond_8

    sget-object v1, Lcom/b/a/n;->a:Lcom/b/a/n;

    iput-object v1, v2, Lcom/b/a/p;->m:Lcom/b/a/n;

    .line 67
    :cond_8
    iput-object p2, v2, Lcom/b/a/p;->b:Ljava/net/Proxy;

    .line 69
    const-string v1, "http"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v0, Lcom/b/a/a/b/b;

    invoke-direct {v0, p1, v2}, Lcom/b/a/a/b/b;-><init>(Ljava/net/URL;Lcom/b/a/p;)V

    .line 70
    :goto_0
    return-object v0

    :cond_9
    const-string v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v0, Lcom/b/a/a/b/c;

    invoke-direct {v0, p1, v2}, Lcom/b/a/a/b/c;-><init>(Ljava/net/URL;Lcom/b/a/p;)V

    goto :goto_0

    .line 71
    :cond_a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Unexpected protocol: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final synthetic clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/b/a/r;

    iget-object v1, p0, Lcom/b/a/r;->a:Lcom/b/a/p;

    invoke-virtual {v1}, Lcom/b/a/p;->b()Lcom/b/a/p;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/b/a/r;-><init>(Lcom/b/a/p;)V

    return-object v0
.end method

.method public final createURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;
    .locals 1

    .prologue
    .line 85
    const-string v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/b/a/s;

    invoke-direct {v0, p0, p1}, Lcom/b/a/s;-><init>(Lcom/b/a/r;Ljava/lang/String;)V

    goto :goto_0
.end method
