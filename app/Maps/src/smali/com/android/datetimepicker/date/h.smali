.class public Lcom/android/datetimepicker/date/h;
.super Landroid/support/v4/widget/ab;
.source "PG"


# instance fields
.field final synthetic b:Lcom/android/datetimepicker/date/MonthView;

.field private final c:Landroid/graphics/Rect;

.field private final d:Ljava/util/Calendar;


# direct methods
.method public constructor <init>(Lcom/android/datetimepicker/date/MonthView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 720
    iput-object p1, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    .line 721
    invoke-direct {p0, p2}, Landroid/support/v4/widget/ab;-><init>(Landroid/view/View;)V

    .line 717
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/h;->c:Landroid/graphics/Rect;

    .line 718
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/datetimepicker/date/h;->d:Ljava/util/Calendar;

    .line 722
    return-void
.end method


# virtual methods
.method protected final a(FF)I
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    invoke-virtual {v0, p1, p2}, Lcom/android/datetimepicker/date/MonthView;->a(FF)I

    move-result v0

    .line 742
    if-ltz v0, :cond_0

    .line 745
    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method public a(I)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 816
    iget-object v0, p0, Lcom/android/datetimepicker/date/h;->d:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v1, v1, Lcom/android/datetimepicker/date/MonthView;->n:I

    iget-object v2, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v2, v2, Lcom/android/datetimepicker/date/MonthView;->m:I

    invoke-virtual {v0, v1, v2, p1}, Ljava/util/Calendar;->set(III)V

    .line 817
    const-string v0, "dd MMMM yyyy"

    iget-object v1, p0, Lcom/android/datetimepicker/date/h;->d:Ljava/util/Calendar;

    .line 818
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 817
    invoke-static {v0, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    .line 820
    iget-object v1, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v1, v1, Lcom/android/datetimepicker/date/MonthView;->r:I

    if-ne p1, v1, :cond_0

    .line 821
    iget-object v1, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    invoke-virtual {v1}, Lcom/android/datetimepicker/date/MonthView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/android/datetimepicker/i;->h:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 824
    :cond_0
    return-object v0
.end method

.method public a(ILandroid/graphics/Rect;)V
    .locals 7

    .prologue
    .line 794
    iget-object v0, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v0, v0, Lcom/android/datetimepicker/date/MonthView;->g:I

    .line 795
    iget-object v1, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    invoke-virtual {v1}, Lcom/android/datetimepicker/date/MonthView;->b()I

    move-result v1

    .line 796
    iget-object v2, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v2, v2, Lcom/android/datetimepicker/date/MonthView;->p:I

    .line 797
    iget-object v3, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v3, v3, Lcom/android/datetimepicker/date/MonthView;->o:I

    iget-object v4, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v4, v4, Lcom/android/datetimepicker/date/MonthView;->g:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v4, v4, Lcom/android/datetimepicker/date/MonthView;->u:I

    div-int/2addr v3, v4

    .line 798
    add-int/lit8 v4, p1, -0x1

    iget-object v5, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    invoke-virtual {v5}, Lcom/android/datetimepicker/date/MonthView;->c()I

    move-result v5

    add-int/2addr v4, v5

    .line 799
    iget-object v5, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v5, v5, Lcom/android/datetimepicker/date/MonthView;->u:I

    div-int v5, v4, v5

    .line 800
    iget-object v6, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v6, v6, Lcom/android/datetimepicker/date/MonthView;->u:I

    rem-int/2addr v4, v6

    .line 801
    mul-int/2addr v4, v3

    add-int/2addr v0, v4

    .line 802
    mul-int v4, v5, v2

    add-int/2addr v1, v4

    .line 804
    add-int/2addr v3, v0

    add-int/2addr v2, v1

    invoke-virtual {p2, v0, v1, v3, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 805
    return-void
.end method

.method protected final a(ILandroid/support/v4/view/a/h;)V
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lcom/android/datetimepicker/date/h;->c:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v0}, Lcom/android/datetimepicker/date/h;->a(ILandroid/graphics/Rect;)V

    .line 765
    invoke-virtual {p0, p1}, Lcom/android/datetimepicker/date/h;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/h;->a(Ljava/lang/CharSequence;)V

    .line 766
    iget-object v0, p0, Lcom/android/datetimepicker/date/h;->c:Landroid/graphics/Rect;

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/h;->a(Landroid/graphics/Rect;)V

    .line 767
    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/h;->a(I)V

    .line 769
    iget-object v0, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v0, v0, Lcom/android/datetimepicker/date/MonthView;->r:I

    if-ne p1, v0, :cond_0

    .line 770
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Landroid/support/v4/view/a/h;->a(Z)V

    .line 773
    :cond_0
    return-void
.end method

.method protected final a(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 757
    invoke-virtual {p0, p1}, Lcom/android/datetimepicker/date/h;->a(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 758
    return-void
.end method

.method protected final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 750
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    iget v1, v1, Lcom/android/datetimepicker/date/MonthView;->v:I

    if-gt v0, v1, :cond_0

    .line 751
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 750
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 753
    :cond_0
    return-void
.end method

.method protected final b(II)Z
    .locals 1

    .prologue
    .line 778
    packed-switch p2, :pswitch_data_0

    .line 784
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 780
    :pswitch_0
    iget-object v0, p0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    invoke-static {v0, p1}, Lcom/android/datetimepicker/date/MonthView;->a(Lcom/android/datetimepicker/date/MonthView;I)V

    .line 781
    const/4 v0, 0x1

    goto :goto_0

    .line 778
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method
