.class public Lcom/android/datetimepicker/date/YearPickerView;
.super Landroid/widget/ListView;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/android/datetimepicker/date/c;


# instance fields
.field final a:Lcom/android/datetimepicker/date/a;

.field b:Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;

.field private c:Lcom/android/datetimepicker/date/l;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/datetimepicker/date/a;)V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 54
    iput-object p2, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    .line 55
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    invoke-interface {v0, p0}, Lcom/android/datetimepicker/date/a;->a(Lcom/android/datetimepicker/date/c;)V

    .line 56
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 58
    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/YearPickerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 60
    sget v1, Lcom/android/datetimepicker/e;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/YearPickerView;->d:I

    .line 61
    sget v1, Lcom/android/datetimepicker/e;->h:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->e:I

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/YearPickerView;->setVerticalFadingEdgeEnabled(Z)V

    .line 63
    iget v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->e:I

    div-int/lit8 v0, v0, 0x3

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/YearPickerView;->setFadingEdgeLength(I)V

    .line 64
    invoke-direct {p0, p1}, Lcom/android/datetimepicker/date/YearPickerView;->a(Landroid/content/Context;)V

    .line 65
    invoke-virtual {p0, p0}, Lcom/android/datetimepicker/date/YearPickerView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 66
    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/YearPickerView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/YearPickerView;->setDividerHeight(I)V

    .line 68
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/YearPickerView;->a()V

    .line 69
    return-void
.end method

.method static a(Landroid/widget/TextView;)I
    .locals 1

    .prologue
    .line 100
    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private a(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 72
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 73
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    invoke-interface {v0}, Lcom/android/datetimepicker/date/a;->c()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    invoke-interface {v2}, Lcom/android/datetimepicker/date/a;->d()I

    move-result v2

    if-gt v0, v2, :cond_0

    .line 74
    const-string v2, "%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    :cond_0
    new-instance v0, Lcom/android/datetimepicker/date/l;

    sget v2, Lcom/android/datetimepicker/h;->c:I

    invoke-direct {v0, p0, p1, v2, v1}, Lcom/android/datetimepicker/date/l;-><init>(Lcom/android/datetimepicker/date/YearPickerView;Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->c:Lcom/android/datetimepicker/date/l;

    .line 77
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->c:Lcom/android/datetimepicker/date/l;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/YearPickerView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 78
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->c:Lcom/android/datetimepicker/date/l;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/l;->notifyDataSetChanged()V

    .line 150
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    invoke-interface {v0}, Lcom/android/datetimepicker/date/a;->a()Lcom/android/datetimepicker/date/g;

    move-result-object v0

    iget v0, v0, Lcom/android/datetimepicker/date/g;->a:I

    iget-object v1, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    invoke-interface {v1}, Lcom/android/datetimepicker/date/a;->c()I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/android/datetimepicker/date/YearPickerView;->d:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/android/datetimepicker/date/YearPickerView;->e:I

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    new-instance v2, Lcom/android/datetimepicker/date/k;

    invoke-direct {v2, p0, v0, v1}, Lcom/android/datetimepicker/date/k;-><init>(Lcom/android/datetimepicker/date/YearPickerView;II)V

    invoke-virtual {p0, v2}, Lcom/android/datetimepicker/date/YearPickerView;->post(Ljava/lang/Runnable;)Z

    .line 151
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 155
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 156
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_0

    .line 157
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 158
    invoke-virtual {p1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 160
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    invoke-interface {v0}, Lcom/android/datetimepicker/date/a;->g()V

    .line 83
    check-cast p2, Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;

    .line 84
    if-eqz p2, :cond_2

    .line 85
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->b:Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;

    if-eq p2, v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->b:Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->b:Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;->b:Z

    .line 88
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->b:Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;->requestLayout()V

    .line 90
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p2, Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;->b:Z

    .line 91
    invoke-virtual {p2}, Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;->requestLayout()V

    .line 92
    iput-object p2, p0, Lcom/android/datetimepicker/date/YearPickerView;->b:Lcom/android/datetimepicker/date/TextViewWithCircularIndicator;

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->a:Lcom/android/datetimepicker/date/a;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/android/datetimepicker/date/a;->a(I)V

    .line 95
    iget-object v0, p0, Lcom/android/datetimepicker/date/YearPickerView;->c:Lcom/android/datetimepicker/date/l;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/l;->notifyDataSetChanged()V

    .line 97
    :cond_2
    return-void
.end method
