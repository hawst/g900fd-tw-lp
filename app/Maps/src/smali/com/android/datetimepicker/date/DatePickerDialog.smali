.class public Lcom/android/datetimepicker/date/DatePickerDialog;
.super Lcom/android/datetimepicker/DialogFragmentWithListener;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/android/datetimepicker/date/a;


# static fields
.field private static c:Ljava/text/SimpleDateFormat;

.field private static d:Ljava/text/SimpleDateFormat;


# instance fields
.field final a:Ljava/util/Calendar;

.field b:Lcom/android/datetimepicker/a;

.field private e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/android/datetimepicker/date/c;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/android/datetimepicker/date/DayPickerView;

.field private m:Lcom/android/datetimepicker/date/YearPickerView;

.field private n:Landroid/widget/Button;

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:Z

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 79
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/datetimepicker/date/DatePickerDialog;->c:Ljava/text/SimpleDateFormat;

    .line 80
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/datetimepicker/date/DatePickerDialog;->d:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/android/datetimepicker/DialogFragmentWithListener;-><init>()V

    .line 82
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    .line 84
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->e:Ljava/util/HashSet;

    .line 97
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    .line 99
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->p:I

    .line 100
    const/16 v0, 0x76c

    iput v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->q:I

    .line 101
    const/16 v0, 0x834

    iput v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->r:I

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->s:Z

    .line 141
    return-void
.end method

.method private a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    .line 337
    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->g:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 338
    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->g:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v3, 0x7

    .line 339
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    .line 338
    invoke-virtual {v2, v3, v5, v4}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 339
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 338
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    .line 343
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 342
    invoke-virtual {v2, v5, v0, v3}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 343
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 342
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 344
    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->j:Landroid/widget/TextView;

    sget-object v2, Lcom/android/datetimepicker/date/DatePickerDialog;->d:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 345
    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->k:Landroid/widget/TextView;

    sget-object v2, Lcom/android/datetimepicker/date/DatePickerDialog;->c:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 348
    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 349
    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iput-wide v2, v1, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->a:J

    .line 350
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v4, 0x18

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    .line 352
    iget-object v4, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 354
    if-eqz p1, :cond_1

    .line 355
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/16 v4, 0x14

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v1

    .line 357
    iget-object v2, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_2

    :goto_0
    if-eqz v0, :cond_1

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 359
    :cond_1
    return-void

    .line 357
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x1f4

    const/16 v8, 0x10

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 290
    iget-object v2, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 292
    packed-switch p1, :pswitch_data_0

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 294
    :pswitch_0
    iget-object v4, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->h:Landroid/widget/LinearLayout;

    const v5, 0x3f666666    # 0.9f

    const v6, 0x3f866666    # 1.05f

    invoke-static {v4, v5, v6}, Lcom/android/datetimepicker/j;->a(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 296
    iget-boolean v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->s:Z

    if-eqz v5, :cond_1

    .line 297
    invoke-virtual {v4, v10, v11}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 298
    iput-boolean v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->s:Z

    .line 300
    :cond_1
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->l:Lcom/android/datetimepicker/date/DayPickerView;

    invoke-virtual {v5}, Lcom/android/datetimepicker/date/DayPickerView;->a()V

    .line 301
    iget v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    if-eq v5, p1, :cond_2

    .line 302
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 303
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->k:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 304
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v5, v1}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setDisplayedChild(I)V

    .line 305
    iput p1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    .line 307
    :cond_2
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 309
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v2, v3, v8}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v2

    .line 311
    iget-object v3, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v4, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->t:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 312
    iget-object v2, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v3, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->u:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v8, :cond_3

    :goto_1
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 315
    :pswitch_1
    iget-object v4, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->k:Landroid/widget/TextView;

    const v5, 0x3f59999a    # 0.85f

    const v6, 0x3f8ccccd    # 1.1f

    invoke-static {v4, v5, v6}, Lcom/android/datetimepicker/j;->a(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 316
    iget-boolean v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->s:Z

    if-eqz v5, :cond_4

    .line 317
    invoke-virtual {v4, v10, v11}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 318
    iput-boolean v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->s:Z

    .line 320
    :cond_4
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->m:Lcom/android/datetimepicker/date/YearPickerView;

    invoke-virtual {v5}, Lcom/android/datetimepicker/date/YearPickerView;->a()V

    .line 321
    iget v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    if-eq v5, p1, :cond_5

    .line 322
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->setSelected(Z)V

    .line 323
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->k:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setSelected(Z)V

    .line 324
    iget-object v5, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v5, v0}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setDisplayedChild(I)V

    .line 325
    iput p1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    .line 327
    :cond_5
    invoke-virtual {v4}, Landroid/animation/ObjectAnimator;->start()V

    .line 329
    sget-object v4, Lcom/android/datetimepicker/date/DatePickerDialog;->c:Ljava/text/SimpleDateFormat;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 330
    iget-object v3, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v4, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->v:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 331
    iget-object v2, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v3, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->w:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v8, :cond_6

    :goto_2
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_2

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private h()V
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 471
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 472
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/datetimepicker/date/c;

    invoke-interface {v0}, Lcom/android/datetimepicker/date/c;->a()V

    goto :goto_0

    .line 474
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/android/datetimepicker/date/g;
    .locals 2

    .prologue
    .line 479
    new-instance v0, Lcom/android/datetimepicker/date/g;

    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-direct {v0, v1}, Lcom/android/datetimepicker/date/g;-><init>(Ljava/util/Calendar;)V

    return-object v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x1

    .line 453
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-static {v0, p1}, Lcom/android/datetimepicker/j;->a(II)I

    move-result v0

    if-le v1, v0, :cond_0

    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v3, v0}, Ljava/util/Calendar;->set(II)V

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->set(II)V

    .line 455
    invoke-direct {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->h()V

    .line 456
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/datetimepicker/date/DatePickerDialog;->b(I)V

    .line 457
    invoke-direct {p0, v2}, Lcom/android/datetimepicker/date/DatePickerDialog;->a(Z)V

    .line 458
    return-void
.end method

.method public final a(III)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 462
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v0, v2, p1}, Ljava/util/Calendar;->set(II)V

    .line 463
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 464
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 465
    invoke-direct {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->h()V

    .line 466
    invoke-direct {p0, v2}, Lcom/android/datetimepicker/date/DatePickerDialog;->a(Z)V

    .line 467
    return-void
.end method

.method public final a(Lcom/android/datetimepicker/date/c;)V
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 500
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 494
    iget v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->p:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 484
    iget v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->q:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 489
    iget v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->r:I

    return v0
.end method

.method public final e()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 422
    const/4 v0, 0x0

    return-object v0
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 509
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->b:Lcom/android/datetimepicker/a;

    invoke-virtual {v0}, Lcom/android/datetimepicker/a;->b()V

    .line 510
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 443
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->b:Lcom/android/datetimepicker/a;

    invoke-virtual {v0}, Lcom/android/datetimepicker/a;->b()V

    .line 444
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/android/datetimepicker/g;->h:I

    if-ne v0, v1, :cond_1

    .line 445
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/android/datetimepicker/date/DatePickerDialog;->b(I)V

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/android/datetimepicker/g;->g:I

    if-ne v0, v1, :cond_0

    .line 447
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/datetimepicker/date/DatePickerDialog;->b(I)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 166
    invoke-super {p0, p1}, Lcom/android/datetimepicker/DialogFragmentWithListener;->onCreate(Landroid/os/Bundle;)V

    .line 167
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 170
    if-eqz p1, :cond_0

    .line 171
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v1, 0x1

    const-string v2, "year"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 172
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v1, 0x2

    const-string v2, "month"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 173
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v1, 0x5

    const-string v2, "day"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 175
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    .line 200
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->requestFeature(I)Z

    .line 203
    sget v0, Lcom/android/datetimepicker/h;->a:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 205
    sget v0, Lcom/android/datetimepicker/g;->e:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->g:Landroid/widget/TextView;

    .line 206
    sget v0, Lcom/android/datetimepicker/g;->g:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->h:Landroid/widget/LinearLayout;

    .line 207
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 208
    sget v0, Lcom/android/datetimepicker/g;->f:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->i:Landroid/widget/TextView;

    .line 209
    sget v0, Lcom/android/datetimepicker/g;->d:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->j:Landroid/widget/TextView;

    .line 210
    sget v0, Lcom/android/datetimepicker/g;->h:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->k:Landroid/widget/TextView;

    .line 211
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->k:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    const/4 v2, -0x1

    .line 214
    const/4 v1, 0x0

    .line 215
    const/4 v0, 0x0

    .line 216
    if-eqz p3, :cond_2

    .line 217
    const-string v0, "week_start"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->p:I

    .line 218
    const-string v0, "year_start"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->q:I

    .line 219
    const-string v0, "year_end"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->r:I

    .line 220
    const-string v0, "current_view"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 221
    const-string v1, "list_position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 222
    const-string v1, "list_position_offset"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 225
    :goto_0
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    .line 226
    new-instance v0, Lcom/android/datetimepicker/date/SimpleDayPickerView;

    invoke-direct {v0, v5, p0}, Lcom/android/datetimepicker/date/SimpleDayPickerView;-><init>(Landroid/content/Context;Lcom/android/datetimepicker/date/a;)V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->l:Lcom/android/datetimepicker/date/DayPickerView;

    .line 227
    new-instance v0, Lcom/android/datetimepicker/date/YearPickerView;

    invoke-direct {v0, v5, p0}, Lcom/android/datetimepicker/date/YearPickerView;-><init>(Landroid/content/Context;Lcom/android/datetimepicker/date/a;)V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->m:Lcom/android/datetimepicker/date/YearPickerView;

    .line 229
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DatePickerDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 230
    sget v6, Lcom/android/datetimepicker/i;->e:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->t:Ljava/lang/String;

    .line 231
    sget v6, Lcom/android/datetimepicker/i;->o:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->u:Ljava/lang/String;

    .line 232
    sget v6, Lcom/android/datetimepicker/i;->x:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->v:Ljava/lang/String;

    .line 233
    sget v6, Lcom/android/datetimepicker/i;->r:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->w:Ljava/lang/String;

    .line 235
    sget v0, Lcom/android/datetimepicker/g;->c:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    .line 236
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->l:Lcom/android/datetimepicker/date/DayPickerView;

    invoke-virtual {v0, v6}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->addView(Landroid/view/View;)V

    .line 237
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->m:Lcom/android/datetimepicker/date/YearPickerView;

    invoke-virtual {v0, v6}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->addView(Landroid/view/View;)V

    .line 238
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    iget-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v6

    iput-wide v6, v0, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->a:J

    .line 240
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 241
    const-wide/16 v6, 0x12c

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 242
    iget-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v6, v0}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 244
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    invoke-direct {v0, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 245
    const-wide/16 v6, 0x12c

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 246
    iget-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->f:Lcom/android/datetimepicker/date/AccessibleDateAnimator;

    invoke-virtual {v6, v0}, Lcom/android/datetimepicker/date/AccessibleDateAnimator;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 248
    sget v0, Lcom/android/datetimepicker/g;->i:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->n:Landroid/widget/Button;

    .line 249
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->n:Landroid/widget/Button;

    new-instance v6, Lcom/android/datetimepicker/date/b;

    invoke-direct {v6, p0}, Lcom/android/datetimepicker/date/b;-><init>(Lcom/android/datetimepicker/date/DatePickerDialog;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 262
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/datetimepicker/date/DatePickerDialog;->a(Z)V

    .line 263
    invoke-direct {p0, v1}, Lcom/android/datetimepicker/date/DatePickerDialog;->b(I)V

    .line 265
    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    .line 266
    if-nez v1, :cond_1

    .line 267
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->l:Lcom/android/datetimepicker/date/DayPickerView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/DayPickerView;->clearFocus()V

    new-instance v1, Lcom/android/datetimepicker/date/d;

    invoke-direct {v1, v0, v3}, Lcom/android/datetimepicker/date/d;-><init>(Lcom/android/datetimepicker/date/DayPickerView;I)V

    invoke-virtual {v0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->post(Ljava/lang/Runnable;)Z

    const/4 v1, 0x0

    invoke-virtual {v0, v0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 273
    :cond_0
    :goto_1
    new-instance v0, Lcom/android/datetimepicker/a;

    invoke-direct {v0, v5}, Lcom/android/datetimepicker/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->b:Lcom/android/datetimepicker/a;

    .line 274
    return-object v4

    .line 268
    :cond_1
    const/4 v0, 0x1

    if-ne v1, v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->m:Lcom/android/datetimepicker/date/YearPickerView;

    new-instance v1, Lcom/android/datetimepicker/date/k;

    invoke-direct {v1, v0, v3, v2}, Lcom/android/datetimepicker/date/k;-><init>(Lcom/android/datetimepicker/date/YearPickerView;II)V

    invoke-virtual {v0, v1}, Lcom/android/datetimepicker/date/YearPickerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_2
    move v3, v2

    move v2, v1

    move v1, v0

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 285
    invoke-super {p0}, Lcom/android/datetimepicker/DialogFragmentWithListener;->onPause()V

    .line 286
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->b:Lcom/android/datetimepicker/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/datetimepicker/a;->c:Landroid/os/Vibrator;

    iget-object v1, v0, Lcom/android/datetimepicker/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, v0, Lcom/android/datetimepicker/a;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 287
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 279
    invoke-super {p0}, Lcom/android/datetimepicker/DialogFragmentWithListener;->onResume()V

    .line 280
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->b:Lcom/android/datetimepicker/a;

    invoke-virtual {v0}, Lcom/android/datetimepicker/a;->a()V

    .line 281
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 179
    invoke-super {p0, p1}, Lcom/android/datetimepicker/DialogFragmentWithListener;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 180
    const-string v0, "year"

    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 181
    const-string v0, "month"

    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 182
    const-string v0, "day"

    iget-object v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->a:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 183
    const-string v0, "week_start"

    iget v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 184
    const-string v0, "year_start"

    iget v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 185
    const-string v0, "year_end"

    iget v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 186
    const-string v0, "current_view"

    iget v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 187
    const/4 v0, -0x1

    .line 188
    iget v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    if-nez v1, :cond_2

    .line 189
    iget-object v6, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->l:Lcom/android/datetimepicker/date/DayPickerView;

    invoke-virtual {v6}, Lcom/android/datetimepicker/date/DayPickerView;->getFirstVisiblePosition()I

    move-result v7

    invoke-virtual {v6}, Lcom/android/datetimepicker/date/DayPickerView;->getHeight()I

    move-result v8

    move v2, v4

    move v1, v4

    move v0, v4

    move v3, v4

    :goto_0
    if-ge v2, v8, :cond_0

    invoke-virtual {v6, v1}, Lcom/android/datetimepicker/date/DayPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-static {v5, v8}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    sub-int v2, v9, v2

    if-le v2, v3, :cond_4

    move v0, v1

    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    move v2, v5

    goto :goto_0

    :cond_0
    add-int/2addr v0, v7

    .line 194
    :cond_1
    :goto_2
    const-string v1, "list_position"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 195
    return-void

    .line 190
    :cond_2
    iget v1, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->o:I

    if-ne v1, v3, :cond_1

    .line 191
    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->m:Lcom/android/datetimepicker/date/YearPickerView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/YearPickerView;->getFirstVisiblePosition()I

    move-result v1

    .line 192
    const-string v2, "list_position_offset"

    iget-object v0, p0, Lcom/android/datetimepicker/date/DatePickerDialog;->m:Lcom/android/datetimepicker/date/YearPickerView;

    invoke-virtual {v0, v4}, Lcom/android/datetimepicker/date/YearPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v4

    :goto_3
    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v0, v1

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_3

    :cond_4
    move v2, v3

    goto :goto_1
.end method
