.class public abstract Lcom/android/datetimepicker/date/MonthView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field public A:I

.field public B:I

.field public C:I

.field public D:I

.field private E:Ljava/lang/String;

.field private F:Ljava/lang/String;

.field private final G:Ljava/util/Formatter;

.field private final H:Ljava/lang/StringBuilder;

.field private final I:Ljava/util/Calendar;

.field private J:Z

.field private K:I

.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:Lcom/android/datetimepicker/date/a;

.field public g:I

.field public h:Landroid/graphics/Paint;

.field public i:Landroid/graphics/Paint;

.field public j:Landroid/graphics/Paint;

.field public k:Landroid/graphics/Paint;

.field public l:Landroid/graphics/Paint;

.field public m:I

.field public n:I

.field public o:I

.field public p:I

.field public q:Z

.field public r:I

.field public s:I

.field public t:I

.field public u:I

.field public v:I

.field public final w:Ljava/util/Calendar;

.field final x:Lcom/android/datetimepicker/date/h;

.field public y:I

.field public z:Lcom/android/datetimepicker/date/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 193
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/android/datetimepicker/date/MonthView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 194
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 197
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 126
    iput v5, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    .line 141
    const/16 v0, 0x20

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    .line 155
    iput-boolean v5, p0, Lcom/android/datetimepicker/date/MonthView;->q:Z

    .line 157
    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->r:I

    .line 159
    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->s:I

    .line 161
    iput v4, p0, Lcom/android/datetimepicker/date/MonthView;->t:I

    .line 163
    const/4 v0, 0x7

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    .line 165
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->v:I

    .line 167
    const/4 v0, 0x6

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->y:I

    .line 331
    iput v5, p0, Lcom/android/datetimepicker/date/MonthView;->K:I

    .line 198
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 200
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->w:Ljava/util/Calendar;

    .line 201
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->I:Ljava/util/Calendar;

    .line 203
    sget v1, Lcom/android/datetimepicker/i;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->E:Ljava/lang/String;

    .line 204
    sget v1, Lcom/android/datetimepicker/i;->n:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->F:Ljava/lang/String;

    .line 206
    sget v1, Lcom/android/datetimepicker/d;->f:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->A:I

    .line 207
    sget v1, Lcom/android/datetimepicker/d;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->B:I

    .line 208
    sget v1, Lcom/android/datetimepicker/d;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->C:I

    .line 209
    sget v1, Lcom/android/datetimepicker/d;->o:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    .line 210
    sget v1, Lcom/android/datetimepicker/d;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->D:I

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x32

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->H:Ljava/lang/StringBuilder;

    .line 213
    new-instance v1, Ljava/util/Formatter;

    iget-object v2, p0, Lcom/android/datetimepicker/date/MonthView;->H:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->G:Ljava/util/Formatter;

    .line 215
    sget v1, Lcom/android/datetimepicker/e;->c:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->a:I

    .line 216
    sget v1, Lcom/android/datetimepicker/e;->e:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->b:I

    .line 217
    sget v1, Lcom/android/datetimepicker/e;->d:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->c:I

    .line 218
    sget v1, Lcom/android/datetimepicker/e;->f:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->d:I

    .line 219
    sget v1, Lcom/android/datetimepicker/e;->b:I

    .line 220
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/date/MonthView;->e:I

    .line 222
    sget v1, Lcom/android/datetimepicker/e;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 223
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->b()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x6

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    .line 226
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->a()Lcom/android/datetimepicker/date/h;

    move-result-object v0

    iput-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    .line 227
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    invoke-static {p0, v0}, Landroid/support/v4/view/at;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 228
    invoke-static {p0, v4}, Landroid/support/v4/view/at;->c(Landroid/view/View;I)V

    .line 229
    iput-boolean v4, p0, Lcom/android/datetimepicker/date/MonthView;->J:Z

    .line 232
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->F:Ljava/lang/String;

    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->A:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->j:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->j:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->j:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->D:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->j:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->k:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->k:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->k:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->B:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->k:Landroid/graphics/Paint;

    const/16 v1, 0x3c

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->c:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->A:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->E:Ljava/lang/String;

    invoke-static {v1, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->h:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->h:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->a:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 233
    return-void
.end method

.method private a(I)V
    .locals 4

    .prologue
    .line 591
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->n:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->m:I

    invoke-virtual {p0, v0, v1, p1}, Lcom/android/datetimepicker/date/MonthView;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 602
    :goto_0
    return-void

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->z:Lcom/android/datetimepicker/date/i;

    if-eqz v0, :cond_1

    .line 597
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->z:Lcom/android/datetimepicker/date/i;

    new-instance v1, Lcom/android/datetimepicker/date/g;

    iget v2, p0, Lcom/android/datetimepicker/date/MonthView;->n:I

    iget v3, p0, Lcom/android/datetimepicker/date/MonthView;->m:I

    invoke-direct {v1, v2, v3, p1}, Lcom/android/datetimepicker/date/g;-><init>(III)V

    invoke-interface {v0, v1}, Lcom/android/datetimepicker/date/i;->a(Lcom/android/datetimepicker/date/g;)V

    .line 601
    :cond_1
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/android/datetimepicker/date/h;->a(II)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/android/datetimepicker/date/MonthView;I)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/android/datetimepicker/date/MonthView;->a(I)V

    return-void
.end method


# virtual methods
.method public a(FF)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 553
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    int-to-float v2, v0

    cmpg-float v2, p1, v2

    if-ltz v2, :cond_0

    iget v2, p0, Lcom/android/datetimepicker/date/MonthView;->o:I

    iget v3, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    cmpl-float v2, p1, v2

    if-lez v2, :cond_3

    :cond_0
    move v0, v1

    .line 554
    :goto_0
    if-lez v0, :cond_1

    iget v2, p0, Lcom/android/datetimepicker/date/MonthView;->v:I

    if-le v0, v2, :cond_2

    :cond_1
    move v0, v1

    .line 557
    :cond_2
    return v0

    .line 553
    :cond_3
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->b()I

    move-result v2

    int-to-float v2, v2

    sub-float v2, p2, v2

    float-to-int v2, v2

    iget v3, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    div-int/2addr v2, v3

    int-to-float v3, v0

    sub-float v3, p1, v3

    iget v4, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/android/datetimepicker/date/MonthView;->o:I

    sub-int v0, v4, v0

    iget v4, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    div-float v0, v3, v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->c()I

    move-result v3

    sub-int/2addr v0, v3

    add-int/lit8 v0, v0, 0x1

    iget v3, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    mul-int/2addr v2, v3

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method public a()Lcom/android/datetimepicker/date/h;
    .locals 1

    .prologue
    .line 240
    new-instance v0, Lcom/android/datetimepicker/date/h;

    invoke-direct {v0, p0, p0}, Lcom/android/datetimepicker/date/h;-><init>(Lcom/android/datetimepicker/date/MonthView;Landroid/view/View;)V

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 472
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->o:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    div-int/lit8 v8, v0, 0x2

    .line 473
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->b()I

    move-result v0

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->c:I

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->b:I

    div-int/lit8 v1, v1, 0x3

    add-int v9, v0, v1

    .line 474
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->H:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->I:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/datetimepicker/date/MonthView;->G:Ljava/util/Formatter;

    const/16 v6, 0x34

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v7

    :cond_0
    move-wide v4, v2

    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v1, v8

    int-to-float v2, v9

    iget-object v3, p0, Lcom/android/datetimepicker/date/MonthView;->i:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 475
    return-void
.end method

.method public abstract a(Landroid/graphics/Canvas;IIIIIIII)V
.end method

.method protected final a(III)Z
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 610
    iget-object v2, p0, Lcom/android/datetimepicker/date/MonthView;->f:Lcom/android/datetimepicker/date/a;

    if-nez v2, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_8

    .line 616
    :cond_0
    :goto_1
    return v0

    .line 610
    :cond_1
    iget-object v2, p0, Lcom/android/datetimepicker/date/MonthView;->f:Lcom/android/datetimepicker/date/a;

    invoke-interface {v2}, Lcom/android/datetimepicker/date/a;->e()Ljava/util/Calendar;

    move-result-object v2

    if-nez v2, :cond_2

    move v2, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge p1, v3, :cond_3

    move v2, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-le p1, v3, :cond_4

    move v2, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge p2, v3, :cond_5

    move v2, v0

    goto :goto_0

    :cond_5
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-le p2, v3, :cond_6

    move v2, v1

    goto :goto_0

    :cond_6
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ge p3, v2, :cond_7

    move v2, v0

    goto :goto_0

    :cond_7
    move v2, v1

    goto :goto_0

    .line 612
    :cond_8
    iget-object v2, p0, Lcom/android/datetimepicker/date/MonthView;->f:Lcom/android/datetimepicker/date/a;

    if-nez v2, :cond_9

    move v2, v1

    :goto_2
    if-nez v2, :cond_0

    move v0, v1

    .line 616
    goto :goto_1

    .line 612
    :cond_9
    iget-object v2, p0, Lcom/android/datetimepicker/date/MonthView;->f:Lcom/android/datetimepicker/date/a;

    invoke-interface {v2}, Lcom/android/datetimepicker/date/a;->f()Ljava/util/Calendar;

    move-result-object v2

    if-nez v2, :cond_a

    move v2, v1

    goto :goto_2

    :cond_a
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-le p1, v3, :cond_b

    move v2, v0

    goto :goto_2

    :cond_b
    invoke-virtual {v2, v0}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge p1, v3, :cond_c

    move v2, v1

    goto :goto_2

    :cond_c
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-le p2, v3, :cond_d

    move v2, v0

    goto :goto_2

    :cond_d
    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ge p2, v3, :cond_e

    move v2, v1

    goto :goto_2

    :cond_e
    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-le p3, v2, :cond_f

    move v2, v0

    goto :goto_2

    :cond_f
    move v2, v1

    goto :goto_2
.end method

.method public b()I
    .locals 1

    .prologue
    .line 459
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->d:I

    return v0
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x7

    .line 478
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->b()I

    move-result v0

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->c:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v0, v1

    .line 479
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->o:I

    iget v2, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    shl-int/lit8 v2, v2, 0x1

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    shl-int/lit8 v2, v2, 0x1

    div-int v2, v0, v2

    .line 481
    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    if-ge v0, v3, :cond_0

    .line 482
    iget v3, p0, Lcom/android/datetimepicker/date/MonthView;->t:I

    add-int/2addr v3, v0

    iget v4, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    rem-int/2addr v3, v4

    .line 483
    mul-int/lit8 v4, v0, 0x2

    add-int/lit8 v4, v4, 0x1

    mul-int/2addr v4, v2

    iget v5, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    add-int/2addr v4, v5

    .line 484
    iget-object v5, p0, Lcom/android/datetimepicker/date/MonthView;->w:Ljava/util/Calendar;

    invoke-virtual {v5, v7, v3}, Ljava/util/Calendar;->set(II)V

    .line 485
    iget-object v3, p0, Lcom/android/datetimepicker/date/MonthView;->w:Ljava/util/Calendar;

    const/4 v5, 0x1

    .line 486
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    .line 485
    invoke-virtual {v3, v7, v5, v6}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 486
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    int-to-float v4, v4

    int-to-float v5, v1

    iget-object v6, p0, Lcom/android/datetimepicker/date/MonthView;->l:Landroid/graphics/Paint;

    .line 485
    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 489
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 540
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->K:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->t:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->K:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    add-int/2addr v0, v1

    :goto_0
    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->t:I

    sub-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->K:I

    goto :goto_0
.end method

.method public c(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 498
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->a:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    .line 499
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->b()I

    move-result v1

    add-int v6, v0, v1

    .line 500
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->o:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    shl-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    div-float v11, v0, v1

    .line 501
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->c()I

    move-result v0

    .line 502
    const/4 v4, 0x1

    move v10, v0

    :goto_0
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->v:I

    if-gt v4, v0, :cond_1

    .line 503
    mul-int/lit8 v0, v10, 0x2

    add-int/lit8 v0, v0, 0x1

    int-to-float v0, v0

    mul-float/2addr v0, v11

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->g:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v5, v0

    .line 505
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->a:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    .line 507
    int-to-float v1, v5

    sub-float/2addr v1, v11

    float-to-int v7, v1

    .line 508
    sub-int v8, v6, v0

    .line 510
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    add-int v9, v8, v0

    .line 512
    iget v2, p0, Lcom/android/datetimepicker/date/MonthView;->n:I

    iget v3, p0, Lcom/android/datetimepicker/date/MonthView;->m:I

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/android/datetimepicker/date/MonthView;->a(Landroid/graphics/Canvas;IIIIIIII)V

    .line 514
    add-int/lit8 v0, v10, 0x1

    .line 515
    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    if-ne v0, v1, :cond_0

    .line 516
    const/4 v0, 0x0

    .line 517
    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    add-int/2addr v6, v1

    .line 502
    :cond_0
    add-int/lit8 v4, v4, 0x1

    move v10, v0

    goto :goto_0

    .line 520
    :cond_1
    return-void
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    invoke-virtual {v0, p1}, Lcom/android/datetimepicker/date/h;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 260
    const/4 v0, 0x1

    .line 262
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 326
    invoke-virtual {p0, p1}, Lcom/android/datetimepicker/date/MonthView;->a(Landroid/graphics/Canvas;)V

    .line 327
    invoke-virtual {p0, p1}, Lcom/android/datetimepicker/date/MonthView;->b(Landroid/graphics/Canvas;)V

    .line 328
    invoke-virtual {p0, p1}, Lcom/android/datetimepicker/date/MonthView;->c(Landroid/graphics/Canvas;)V

    .line 329
    return-void
.end method

.method public onMeasure(II)V
    .locals 3

    .prologue
    .line 435
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    iget v2, p0, Lcom/android/datetimepicker/date/MonthView;->y:I

    mul-int/2addr v1, v2

    .line 436
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->b()I

    move-result v2

    add-int/2addr v1, v2

    .line 435
    invoke-virtual {p0, v0, v1}, Lcom/android/datetimepicker/date/MonthView;->setMeasuredDimension(II)V

    .line 437
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 441
    iput p1, p0, Lcom/android/datetimepicker/date/MonthView;->o:I

    .line 444
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/h;->a()V

    .line 445
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 267
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 275
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 269
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/android/datetimepicker/date/MonthView;->a(FF)I

    move-result v0

    .line 270
    if-ltz v0, :cond_0

    .line 271
    invoke-direct {p0, v0}, Lcom/android/datetimepicker/date/MonthView;->a(I)V

    goto :goto_0

    .line 267
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V
    .locals 1

    .prologue
    .line 247
    iget-boolean v0, p0, Lcom/android/datetimepicker/date/MonthView;->J:Z

    if-nez v0, :cond_0

    .line 248
    invoke-super {p0, p1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 250
    :cond_0
    return-void
.end method

.method public final setMonthParams(Ljava/util/HashMap;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 361
    const-string v0, "month"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "year"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 362
    new-instance v0, Ljava/security/InvalidParameterException;

    const-string v1, "You must specify month and year for this view"

    invoke-direct {v0, v1}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    invoke-virtual {p0, p1}, Lcom/android/datetimepicker/date/MonthView;->setTag(Ljava/lang/Object;)V

    .line 366
    const-string v0, "height"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    const-string v0, "height"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    .line 368
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    if-ge v0, v4, :cond_1

    .line 369
    iput v4, p0, Lcom/android/datetimepicker/date/MonthView;->p:I

    .line 372
    :cond_1
    const-string v0, "selected_day"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 373
    const-string v0, "selected_day"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->r:I

    .line 377
    :cond_2
    const-string v0, "month"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->m:I

    .line 378
    const-string v0, "year"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->n:I

    .line 381
    new-instance v4, Landroid/text/format/Time;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v4, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 382
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 383
    iput-boolean v2, p0, Lcom/android/datetimepicker/date/MonthView;->q:Z

    .line 384
    const/4 v0, -0x1

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->s:I

    .line 386
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->I:Ljava/util/Calendar;

    const/4 v1, 0x2

    iget v5, p0, Lcom/android/datetimepicker/date/MonthView;->m:I

    invoke-virtual {v0, v1, v5}, Ljava/util/Calendar;->set(II)V

    .line 387
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->I:Ljava/util/Calendar;

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->n:I

    invoke-virtual {v0, v3, v1}, Ljava/util/Calendar;->set(II)V

    .line 388
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->I:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 389
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->I:Ljava/util/Calendar;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->K:I

    .line 391
    const-string v0, "week_start"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 392
    const-string v0, "week_start"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->t:I

    .line 397
    :goto_1
    iget v0, p0, Lcom/android/datetimepicker/date/MonthView;->m:I

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->n:I

    invoke-static {v0, v1}, Lcom/android/datetimepicker/j;->a(II)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->v:I

    move v0, v2

    .line 398
    :goto_2
    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->v:I

    if-ge v0, v1, :cond_7

    .line 399
    add-int/lit8 v5, v0, 0x1

    .line 400
    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->n:I

    iget v6, v4, Landroid/text/format/Time;->year:I

    if-ne v1, v6, :cond_6

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->m:I

    iget v6, v4, Landroid/text/format/Time;->month:I

    if-ne v1, v6, :cond_6

    iget v1, v4, Landroid/text/format/Time;->monthDay:I

    if-ne v5, v1, :cond_6

    move v1, v3

    :goto_3
    if-eqz v1, :cond_3

    .line 401
    iput-boolean v3, p0, Lcom/android/datetimepicker/date/MonthView;->q:Z

    .line 402
    iput v5, p0, Lcom/android/datetimepicker/date/MonthView;->s:I

    .line 398
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 381
    goto :goto_0

    .line 394
    :cond_5
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->I:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->t:I

    goto :goto_1

    :cond_6
    move v1, v2

    .line 400
    goto :goto_3

    .line 405
    :cond_7
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/MonthView;->c()I

    move-result v0

    iget v1, p0, Lcom/android/datetimepicker/date/MonthView;->v:I

    add-int/2addr v1, v0

    iget v4, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    div-int/2addr v1, v4

    iget v4, p0, Lcom/android/datetimepicker/date/MonthView;->v:I

    add-int/2addr v0, v4

    iget v4, p0, Lcom/android/datetimepicker/date/MonthView;->u:I

    rem-int/2addr v0, v4

    if-lez v0, :cond_8

    move v2, v3

    :cond_8
    add-int v0, v1, v2

    iput v0, p0, Lcom/android/datetimepicker/date/MonthView;->y:I

    .line 408
    iget-object v0, p0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/h;->a()V

    .line 409
    return-void
.end method
