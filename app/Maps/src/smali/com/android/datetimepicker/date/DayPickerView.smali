.class public abstract Lcom/android/datetimepicker/date/DayPickerView;
.super Landroid/widget/ListView;
.source "PG"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/android/datetimepicker/date/c;


# static fields
.field public static a:I

.field private static j:Ljava/text/SimpleDateFormat;


# instance fields
.field public b:F

.field public c:Landroid/os/Handler;

.field public d:Lcom/android/datetimepicker/date/g;

.field public e:Lcom/android/datetimepicker/date/f;

.field public f:Lcom/android/datetimepicker/date/g;

.field public g:I

.field public h:I

.field public i:Lcom/android/datetimepicker/date/e;

.field private k:Lcom/android/datetimepicker/date/a;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    const/4 v0, -0x1

    sput v0, Lcom/android/datetimepicker/date/DayPickerView;->a:I

    .line 65
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/datetimepicker/date/DayPickerView;->j:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->b:F

    .line 74
    new-instance v0, Lcom/android/datetimepicker/date/g;

    invoke-direct {v0}, Lcom/android/datetimepicker/date/g;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->d:Lcom/android/datetimepicker/date/g;

    .line 77
    new-instance v0, Lcom/android/datetimepicker/date/g;

    invoke-direct {v0}, Lcom/android/datetimepicker/date/g;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->f:Lcom/android/datetimepicker/date/g;

    .line 88
    iput v1, p0, Lcom/android/datetimepicker/date/DayPickerView;->g:I

    .line 90
    iput v1, p0, Lcom/android/datetimepicker/date/DayPickerView;->h:I

    .line 283
    new-instance v0, Lcom/android/datetimepicker/date/e;

    invoke-direct {v0, p0}, Lcom/android/datetimepicker/date/e;-><init>(Lcom/android/datetimepicker/date/DayPickerView;)V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->i:Lcom/android/datetimepicker/date/e;

    .line 97
    invoke-direct {p0, p1}, Lcom/android/datetimepicker/date/DayPickerView;->a(Landroid/content/Context;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/datetimepicker/date/a;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 62
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->b:F

    .line 74
    new-instance v0, Lcom/android/datetimepicker/date/g;

    invoke-direct {v0}, Lcom/android/datetimepicker/date/g;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->d:Lcom/android/datetimepicker/date/g;

    .line 77
    new-instance v0, Lcom/android/datetimepicker/date/g;

    invoke-direct {v0}, Lcom/android/datetimepicker/date/g;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->f:Lcom/android/datetimepicker/date/g;

    .line 88
    iput v1, p0, Lcom/android/datetimepicker/date/DayPickerView;->g:I

    .line 90
    iput v1, p0, Lcom/android/datetimepicker/date/DayPickerView;->h:I

    .line 283
    new-instance v0, Lcom/android/datetimepicker/date/e;

    invoke-direct {v0, p0}, Lcom/android/datetimepicker/date/e;-><init>(Lcom/android/datetimepicker/date/DayPickerView;)V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->i:Lcom/android/datetimepicker/date/e;

    .line 102
    invoke-direct {p0, p1}, Lcom/android/datetimepicker/date/DayPickerView;->a(Landroid/content/Context;)V

    .line 103
    iput-object p2, p0, Lcom/android/datetimepicker/date/DayPickerView;->k:Lcom/android/datetimepicker/date/a;

    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->k:Lcom/android/datetimepicker/date/a;

    invoke-interface {v0, p0}, Lcom/android/datetimepicker/date/a;->a(Lcom/android/datetimepicker/date/c;)V

    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->e:Lcom/android/datetimepicker/date/f;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/android/datetimepicker/date/DayPickerView;->k:Lcom/android/datetimepicker/date/a;

    invoke-virtual {p0, v0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->a(Landroid/content/Context;Lcom/android/datetimepicker/date/a;)Lcom/android/datetimepicker/date/f;

    move-result-object v0

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->e:Lcom/android/datetimepicker/date/f;

    :goto_0
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->e:Lcom/android/datetimepicker/date/f;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->a()V

    .line 104
    return-void

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->e:Lcom/android/datetimepicker/date/f;

    iget-object v1, p0, Lcom/android/datetimepicker/date/DayPickerView;->d:Lcom/android/datetimepicker/date/g;

    iput-object v1, v0, Lcom/android/datetimepicker/date/f;->b:Lcom/android/datetimepicker/date/g;

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/f;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 114
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->c:Landroid/os/Handler;

    .line 115
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v0, v2, v2}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 116
    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->setDrawSelectorOnTop(Z)V

    .line 118
    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->setCacheColorHint(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->setItemsCanFocus(Z)V

    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->setFastScrollEnabled(Z)V

    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->setVerticalScrollBarEnabled(Z)V

    invoke-virtual {p0, p0}, Lcom/android/datetimepicker/date/DayPickerView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->setFadingEdgeLength(I)V

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    iget v1, p0, Lcom/android/datetimepicker/date/DayPickerView;->b:F

    mul-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->setFriction(F)V

    .line 120
    return-void
.end method

.method private a(Lcom/android/datetimepicker/date/g;ZZZ)Z
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v1, 0x0

    .line 181
    if-eqz p3, :cond_0

    .line 182
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->d:Lcom/android/datetimepicker/date/g;

    iget v2, p1, Lcom/android/datetimepicker/date/g;->a:I

    iput v2, v0, Lcom/android/datetimepicker/date/g;->a:I

    iget v2, p1, Lcom/android/datetimepicker/date/g;->b:I

    iput v2, v0, Lcom/android/datetimepicker/date/g;->b:I

    iget v2, p1, Lcom/android/datetimepicker/date/g;->c:I

    iput v2, v0, Lcom/android/datetimepicker/date/g;->c:I

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->f:Lcom/android/datetimepicker/date/g;

    iget v2, p1, Lcom/android/datetimepicker/date/g;->a:I

    iput v2, v0, Lcom/android/datetimepicker/date/g;->a:I

    iget v2, p1, Lcom/android/datetimepicker/date/g;->b:I

    iput v2, v0, Lcom/android/datetimepicker/date/g;->b:I

    iget v2, p1, Lcom/android/datetimepicker/date/g;->c:I

    iput v2, v0, Lcom/android/datetimepicker/date/g;->c:I

    .line 186
    iget v0, p1, Lcom/android/datetimepicker/date/g;->a:I

    iget-object v2, p0, Lcom/android/datetimepicker/date/DayPickerView;->k:Lcom/android/datetimepicker/date/a;

    invoke-interface {v2}, Lcom/android/datetimepicker/date/a;->c()I

    move-result v2

    sub-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0xc

    iget v2, p1, Lcom/android/datetimepicker/date/g;->b:I

    add-int v3, v0, v2

    move v0, v1

    .line 191
    :goto_0
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_2

    .line 196
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    .line 199
    const-string v5, "MonthFragment"

    invoke-static {v5, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 200
    add-int/lit8 v5, v2, -0x1

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x28

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "child at "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has top "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 202
    :cond_1
    if-ltz v4, :cond_a

    .line 206
    :cond_2
    if-eqz v0, :cond_7

    .line 207
    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->getPositionForView(Landroid/view/View;)I

    move-result v0

    .line 212
    :goto_1
    if-eqz p3, :cond_3

    .line 213
    iget-object v2, p0, Lcom/android/datetimepicker/date/DayPickerView;->e:Lcom/android/datetimepicker/date/f;

    iget-object v4, p0, Lcom/android/datetimepicker/date/DayPickerView;->d:Lcom/android/datetimepicker/date/g;

    iput-object v4, v2, Lcom/android/datetimepicker/date/f;->b:Lcom/android/datetimepicker/date/g;

    invoke-virtual {v2}, Lcom/android/datetimepicker/date/f;->notifyDataSetChanged()V

    .line 216
    :cond_3
    const-string v2, "MonthFragment"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 217
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x19

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "GoTo position "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 221
    :cond_4
    if-ne v3, v0, :cond_5

    if-eqz p4, :cond_9

    .line 222
    :cond_5
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->f:Lcom/android/datetimepicker/date/g;

    iget v0, v0, Lcom/android/datetimepicker/date/g;->b:I

    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->invalidateViews()V

    .line 223
    const/4 v0, 0x2

    iput v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->g:I

    .line 224
    if-eqz p2, :cond_8

    .line 225
    sget v0, Lcom/android/datetimepicker/date/DayPickerView;->a:I

    const/16 v1, 0xfa

    invoke-virtual {p0, v3, v0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->smoothScrollToPositionFromTop(III)V

    .line 227
    const/4 v1, 0x1

    .line 234
    :cond_6
    :goto_2
    return v1

    :cond_7
    move v0, v1

    .line 209
    goto :goto_1

    .line 229
    :cond_8
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->clearFocus()V

    new-instance v0, Lcom/android/datetimepicker/date/d;

    invoke-direct {v0, p0, v3}, Lcom/android/datetimepicker/date/d;-><init>(Lcom/android/datetimepicker/date/DayPickerView;I)V

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/date/DayPickerView;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p0, p0, v1}, Lcom/android/datetimepicker/date/DayPickerView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_2

    .line 231
    :cond_9
    if-eqz p3, :cond_6

    .line 232
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->d:Lcom/android/datetimepicker/date/g;

    iget v0, v0, Lcom/android/datetimepicker/date/g;->b:I

    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->invalidateViews()V

    goto :goto_2

    :cond_a
    move v0, v2

    goto/16 :goto_0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;Lcom/android/datetimepicker/date/a;)Lcom/android/datetimepicker/date/f;
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 370
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->k:Lcom/android/datetimepicker/date/a;

    invoke-interface {v0}, Lcom/android/datetimepicker/date/a;->a()Lcom/android/datetimepicker/date/g;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/android/datetimepicker/date/DayPickerView;->a(Lcom/android/datetimepicker/date/g;ZZZ)Z

    .line 371
    return-void
.end method

.method protected layoutChildren()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 425
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->getChildCount()I

    move-result v6

    move v5, v3

    :goto_0
    if-ge v5, v6, :cond_4

    invoke-virtual {p0, v5}, Lcom/android/datetimepicker/date/DayPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Lcom/android/datetimepicker/date/MonthView;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Lcom/android/datetimepicker/date/MonthView;

    iget-object v4, v0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    invoke-virtual {v4}, Lcom/android/datetimepicker/date/h;->b()I

    move-result v7

    if-ltz v7, :cond_2

    new-instance v4, Lcom/android/datetimepicker/date/g;

    iget v8, v0, Lcom/android/datetimepicker/date/MonthView;->n:I

    iget v0, v0, Lcom/android/datetimepicker/date/MonthView;->m:I

    invoke-direct {v4, v8, v0, v7}, Lcom/android/datetimepicker/date/g;-><init>(III)V

    move-object v0, v4

    :goto_1
    if-eqz v0, :cond_3

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x11

    if-ne v4, v5, :cond_0

    check-cast v1, Lcom/android/datetimepicker/date/MonthView;

    iget-object v1, v1, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    invoke-virtual {v1}, Lcom/android/datetimepicker/date/h;->b()I

    move-result v4

    const/high16 v5, -0x80000000

    if-eq v4, v5, :cond_0

    iget-object v5, v1, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    invoke-virtual {v1, v5}, Lcom/android/datetimepicker/date/h;->a(Landroid/view/View;)Landroid/support/v4/view/a/r;

    move-result-object v1

    const/16 v5, 0x80

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v4/view/a/r;->a(IILandroid/os/Bundle;)Z

    :cond_0
    move-object v1, v0

    .line 426
    :goto_2
    invoke-super {p0}, Landroid/widget/ListView;->layoutChildren()V

    .line 427
    iget-boolean v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->l:Z

    if-eqz v0, :cond_5

    .line 428
    iput-boolean v3, p0, Lcom/android/datetimepicker/date/DayPickerView;->l:Z

    .line 432
    :cond_1
    return-void

    :cond_2
    move-object v0, v2

    .line 425
    goto :goto_1

    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    :cond_4
    move-object v1, v2

    goto :goto_2

    .line 430
    :cond_5
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->getChildCount()I

    move-result v5

    move v4, v3

    :goto_3
    if-ge v4, v5, :cond_1

    invoke-virtual {p0, v4}, Lcom/android/datetimepicker/date/DayPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v6, v0, Lcom/android/datetimepicker/date/MonthView;

    if-eqz v6, :cond_7

    check-cast v0, Lcom/android/datetimepicker/date/MonthView;

    iget v6, v1, Lcom/android/datetimepicker/date/g;->a:I

    iget v7, v0, Lcom/android/datetimepicker/date/MonthView;->n:I

    if-ne v6, v7, :cond_6

    iget v6, v1, Lcom/android/datetimepicker/date/g;->b:I

    iget v7, v0, Lcom/android/datetimepicker/date/MonthView;->m:I

    if-ne v6, v7, :cond_6

    iget v6, v1, Lcom/android/datetimepicker/date/g;->c:I

    iget v7, v0, Lcom/android/datetimepicker/date/MonthView;->v:I

    if-le v6, v7, :cond_8

    :cond_6
    move v0, v3

    :goto_4
    if-nez v0, :cond_1

    :cond_7
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    :cond_8
    iget-object v0, v0, Lcom/android/datetimepicker/date/MonthView;->x:Lcom/android/datetimepicker/date/h;

    iget v6, v1, Lcom/android/datetimepicker/date/g;->c:I

    iget-object v7, v0, Lcom/android/datetimepicker/date/h;->b:Lcom/android/datetimepicker/date/MonthView;

    invoke-virtual {v0, v7}, Lcom/android/datetimepicker/date/h;->a(Landroid/view/View;)Landroid/support/v4/view/a/r;

    move-result-object v0

    const/16 v7, 0x40

    invoke-virtual {v0, v6, v7, v2}, Landroid/support/v4/view/a/r;->a(IILandroid/os/Bundle;)Z

    const/4 v0, 0x1

    goto :goto_4
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 436
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 437
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 438
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 457
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 458
    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 459
    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 460
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 256
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/datetimepicker/date/MonthView;

    .line 257
    if-nez v0, :cond_0

    .line 265
    :goto_0
    return-void

    .line 262
    :cond_0
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/MonthView;->getHeight()I

    invoke-virtual {v0}, Lcom/android/datetimepicker/date/MonthView;->getBottom()I

    .line 263
    iget v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->h:I

    iput v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->g:I

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 4

    .prologue
    .line 280
    iget-object v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->i:Lcom/android/datetimepicker/date/e;

    iget-object v1, v0, Lcom/android/datetimepicker/date/e;->b:Lcom/android/datetimepicker/date/DayPickerView;

    iget-object v1, v1, Lcom/android/datetimepicker/date/DayPickerView;->c:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput p2, v0, Lcom/android/datetimepicker/date/e;->a:I

    iget-object v1, v0, Lcom/android/datetimepicker/date/e;->b:Lcom/android/datetimepicker/date/DayPickerView;

    iget-object v1, v1, Lcom/android/datetimepicker/date/DayPickerView;->c:Landroid/os/Handler;

    const-wide/16 v2, 0x28

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 281
    return-void
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/16 v6, 0x1000

    const/4 v7, 0x2

    const/4 v5, -0x1

    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 468
    if-eq p1, v6, :cond_0

    const/16 v1, 0x2000

    if-eq p1, v1, :cond_0

    .line 470
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v0

    .line 505
    :goto_0
    return v0

    .line 474
    :cond_0
    invoke-virtual {p0}, Lcom/android/datetimepicker/date/DayPickerView;->getFirstVisiblePosition()I

    move-result v1

    .line 475
    rem-int/lit8 v3, v1, 0xc

    .line 476
    div-int/lit8 v1, v1, 0xc

    iget-object v4, p0, Lcom/android/datetimepicker/date/DayPickerView;->k:Lcom/android/datetimepicker/date/a;

    invoke-interface {v4}, Lcom/android/datetimepicker/date/a;->c()I

    move-result v4

    add-int/2addr v1, v4

    .line 477
    new-instance v4, Lcom/android/datetimepicker/date/g;

    invoke-direct {v4, v1, v3, v0}, Lcom/android/datetimepicker/date/g;-><init>(III)V

    .line 480
    if-ne p1, v6, :cond_3

    .line 481
    iget v1, v4, Lcom/android/datetimepicker/date/g;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v4, Lcom/android/datetimepicker/date/g;->b:I

    .line 482
    iget v1, v4, Lcom/android/datetimepicker/date/g;->b:I

    const/16 v3, 0xc

    if-ne v1, v3, :cond_1

    .line 483
    iput v2, v4, Lcom/android/datetimepicker/date/g;->b:I

    .line 484
    iget v1, v4, Lcom/android/datetimepicker/date/g;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v4, Lcom/android/datetimepicker/date/g;->a:I

    .line 502
    :cond_1
    :goto_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iget v3, v4, Lcom/android/datetimepicker/date/g;->a:I

    iget v5, v4, Lcom/android/datetimepicker/date/g;->b:I

    iget v6, v4, Lcom/android/datetimepicker/date/g;->c:I

    invoke-virtual {v1, v3, v5, v6}, Ljava/util/Calendar;->set(III)V

    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v1, v7, v7, v5}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v5, " "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    sget-object v5, Lcom/android/datetimepicker/date/DayPickerView;->j:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v1, v5, :cond_4

    move v1, v0

    :goto_2
    if-eqz v1, :cond_2

    if-eqz p0, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {p0, v3}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 503
    :cond_2
    invoke-direct {p0, v4, v0, v2, v0}, Lcom/android/datetimepicker/date/DayPickerView;->a(Lcom/android/datetimepicker/date/g;ZZZ)Z

    .line 504
    iput-boolean v0, p0, Lcom/android/datetimepicker/date/DayPickerView;->l:Z

    goto :goto_0

    .line 486
    :cond_3
    const/16 v1, 0x2000

    if-ne p1, v1, :cond_1

    .line 487
    invoke-virtual {p0, v2}, Lcom/android/datetimepicker/date/DayPickerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 490
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-lt v1, v5, :cond_1

    .line 493
    iget v1, v4, Lcom/android/datetimepicker/date/g;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v4, Lcom/android/datetimepicker/date/g;->b:I

    .line 494
    iget v1, v4, Lcom/android/datetimepicker/date/g;->b:I

    if-ne v1, v5, :cond_1

    .line 495
    const/16 v1, 0xb

    iput v1, v4, Lcom/android/datetimepicker/date/g;->b:I

    .line 496
    iget v1, v4, Lcom/android/datetimepicker/date/g;->a:I

    add-int/lit8 v1, v1, -0x1

    iput v1, v4, Lcom/android/datetimepicker/date/g;->a:I

    goto :goto_1

    :cond_4
    move v1, v2

    .line 502
    goto :goto_2
.end method
