.class Lcom/android/datetimepicker/time/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/android/datetimepicker/time/TimePickerDialog;


# direct methods
.method constructor <init>(Lcom/android/datetimepicker/time/TimePickerDialog;)V
    .locals 0

    .prologue
    .line 280
    iput-object p1, p0, Lcom/android/datetimepicker/time/i;->a:Lcom/android/datetimepicker/time/TimePickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 283
    iget-object v2, p0, Lcom/android/datetimepicker/time/i;->a:Lcom/android/datetimepicker/time/TimePickerDialog;

    iget-object v2, v2, Lcom/android/datetimepicker/time/TimePickerDialog;->a:Lcom/android/datetimepicker/a;

    invoke-virtual {v2}, Lcom/android/datetimepicker/a;->b()V

    .line 284
    iget-object v2, p0, Lcom/android/datetimepicker/time/i;->a:Lcom/android/datetimepicker/time/TimePickerDialog;

    iget-object v2, v2, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget v3, v2, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    const/16 v4, 0xc

    if-ge v3, v4, :cond_0

    move v2, v1

    .line 285
    :goto_0
    if-nez v2, :cond_2

    .line 290
    :goto_1
    iget-object v1, p0, Lcom/android/datetimepicker/time/i;->a:Lcom/android/datetimepicker/time/TimePickerDialog;

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(I)V

    .line 291
    iget-object v1, p0, Lcom/android/datetimepicker/time/i;->a:Lcom/android/datetimepicker/time/TimePickerDialog;

    iget-object v1, v1, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v2, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    iput v0, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->k:I

    iget-object v2, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v2}, Lcom/android/datetimepicker/time/AmPmCirclesView;->invalidate()V

    const/4 v2, 0x2

    invoke-virtual {v1, v2, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 292
    return-void

    .line 284
    :cond_0
    iget v2, v2, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    const/16 v3, 0x18

    if-ge v2, v3, :cond_1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v2, -0x1

    goto :goto_0

    .line 287
    :cond_2
    if-ne v2, v0, :cond_3

    move v0, v1

    .line 288
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1
.end method
