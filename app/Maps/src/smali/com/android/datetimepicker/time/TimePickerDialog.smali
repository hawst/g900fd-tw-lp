.class public Lcom/android/datetimepicker/time/TimePickerDialog;
.super Lcom/android/datetimepicker/DialogFragmentWithListener;
.source "PG"

# interfaces
.implements Lcom/android/datetimepicker/time/c;


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private D:Ljava/lang/String;

.field a:Lcom/android/datetimepicker/a;

.field b:Lcom/android/datetimepicker/time/RadialPickerLayout;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Z

.field f:Ljava/lang/String;

.field g:Z

.field h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Landroid/widget/TextView;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;

.field private o:Landroid/view/View;

.field private p:I

.field private q:I

.field private r:Z

.field private s:I

.field private t:I

.field private u:Z

.field private v:C

.field private w:Ljava/lang/String;

.field private x:Lcom/android/datetimepicker/time/k;

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/android/datetimepicker/DialogFragmentWithListener;-><init>()V

    .line 129
    return-void
.end method

.method private a(IZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 418
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-eqz v0, :cond_2

    .line 419
    const-string v0, "%02d"

    .line 428
    :cond_0
    :goto_0
    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 429
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 430
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 431
    if-eqz p2, :cond_1

    .line 432
    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v0, v5, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    invoke-virtual {v4, v3}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 434
    :cond_1
    return-void

    .line 421
    :cond_2
    const-string v0, "%d"

    .line 422
    rem-int/lit8 p1, p1, 0xc

    .line 423
    if-nez p1, :cond_0

    .line 424
    const/16 p1, 0xc

    goto :goto_0

    :cond_3
    move v0, v2

    .line 432
    goto :goto_1
.end method

.method private a([Ljava/lang/Boolean;)[I
    .locals 10

    .prologue
    const/4 v5, 0x2

    const/4 v3, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 734
    .line 736
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 737
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 738
    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v4

    if-ne v0, v4, :cond_1

    move v0, v1

    :goto_0
    move v4, v5

    move v6, v0

    :goto_1
    move v7, v3

    move v8, v3

    move v3, v4

    .line 747
    :goto_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-gt v3, v0, :cond_5

    .line 748
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    iget-object v9, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    sub-int/2addr v9, v3

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/android/datetimepicker/time/TimePickerDialog;->d(I)I

    move-result v0

    .line 749
    if-ne v3, v4, :cond_2

    move v8, v0

    .line 747
    :cond_0
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 740
    :cond_1
    invoke-virtual {p0, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v4

    if-ne v0, v4, :cond_6

    move v0, v2

    .line 741
    goto :goto_0

    .line 751
    :cond_2
    add-int/lit8 v9, v4, 0x1

    if-ne v3, v9, :cond_3

    .line 752
    mul-int/lit8 v9, v0, 0xa

    add-int/2addr v8, v9

    .line 753
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 754
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p1, v2

    goto :goto_3

    .line 756
    :cond_3
    add-int/lit8 v9, v4, 0x2

    if-ne v3, v9, :cond_4

    move v7, v0

    .line 757
    goto :goto_3

    .line 758
    :cond_4
    add-int/lit8 v9, v4, 0x3

    if-ne v3, v9, :cond_0

    .line 759
    mul-int/lit8 v9, v0, 0xa

    add-int/2addr v7, v9

    .line 760
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 761
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, p1, v1

    goto :goto_3

    .line 766
    :cond_5
    const/4 v0, 0x3

    new-array v0, v0, [I

    aput v7, v0, v1

    aput v8, v0, v2

    aput v6, v0, v5

    .line 767
    return-object v0

    :cond_6
    move v0, v3

    goto :goto_0

    :cond_7
    move v4, v2

    move v6, v3

    goto :goto_1
.end method

.method static d(I)I
    .locals 1

    .prologue
    .line 699
    packed-switch p0, :pswitch_data_0

    .line 721
    const/4 v0, -0x1

    :goto_0
    return v0

    .line 701
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 703
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 705
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 707
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 709
    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    .line 711
    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    .line 713
    :pswitch_6
    const/4 v0, 0x6

    goto :goto_0

    .line 715
    :pswitch_7
    const/4 v0, 0x7

    goto :goto_0

    .line 717
    :pswitch_8
    const/16 v0, 0x8

    goto :goto_0

    .line 719
    :pswitch_9
    const/16 v0, 0x9

    goto :goto_0

    .line 699
    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private f(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 437
    const/16 v2, 0x3c

    if-ne p1, v2, :cond_0

    move p1, v0

    .line 440
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string v3, "%02d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 441
    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_1

    move v0, v1

    :cond_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v3, v2}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 442
    :cond_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 444
    return-void
.end method


# virtual methods
.method a(I)V
    .locals 6

    .prologue
    const/16 v5, 0x10

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 359
    if-nez p1, :cond_2

    .line 360
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->n:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->c:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v5, :cond_1

    :goto_0
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v2, v3}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 362
    :cond_0
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->o:Landroid/view/View;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 370
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 361
    goto :goto_0

    .line 363
    :cond_2
    if-ne p1, v0, :cond_5

    .line 364
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->n:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->d:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v5, :cond_4

    :goto_2
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v3}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 366
    :cond_3
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->o:Landroid/view/View;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    move v0, v1

    .line 365
    goto :goto_2

    .line 368
    :cond_5
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->n:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(IIZ)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 392
    if-nez p1, :cond_3

    .line 393
    invoke-direct {p0, p2, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(IZ)V

    .line 394
    const-string v0, "%d"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 395
    iget-boolean v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->r:Z

    if-eqz v3, :cond_1

    if-eqz p3, :cond_1

    .line 396
    invoke-virtual {p0, v1, v1, v1, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(IZZZ)V

    .line 397
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->D:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ". "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 402
    :goto_0
    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_2

    :goto_1
    if-eqz v1, :cond_0

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v3, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 414
    :cond_0
    :goto_2
    return-void

    .line 399
    :cond_1
    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->A:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xd

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    move v1, v2

    .line 402
    goto :goto_1

    .line 403
    :cond_3
    if-ne p1, v1, :cond_4

    .line 404
    invoke-direct {p0, p2}, Lcom/android/datetimepicker/time/TimePickerDialog;->f(I)V

    .line 405
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->C:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 406
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    .line 407
    invoke-virtual {p0, p2}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(I)V

    goto :goto_2

    .line 408
    :cond_5
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 409
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a()Z

    move-result v0

    if-nez v0, :cond_6

    .line 410
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 412
    :cond_6
    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(Z)V

    goto :goto_2
.end method

.method a(IZZZ)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 449
    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    if-eqz p1, :cond_4

    if-eq p1, v3, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x35

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "TimePicker does not support view at index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 452
    :goto_0
    if-nez p1, :cond_13

    .line 453
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget v0, v0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    .line 454
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v1, :cond_0

    .line 455
    rem-int/lit8 v0, v0, 0xc

    .line 457
    :cond_0
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->A:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xd

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 458
    if-eqz p4, :cond_2

    .line 459
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->B:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_1

    move v2, v3

    :cond_1
    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v0, v1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 461
    :cond_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    move-object v2, v0

    .line 471
    :goto_1
    if-nez p1, :cond_16

    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->p:I

    move v1, v0

    .line 472
    :goto_2
    if-ne p1, v3, :cond_17

    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->p:I

    .line 473
    :goto_3
    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 474
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 476
    const v0, 0x3f59999a    # 0.85f

    const v1, 0x3f8ccccd    # 1.1f

    invoke-static {v2, v0, v1}, Lcom/android/datetimepicker/j;->a(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 477
    if-eqz p3, :cond_3

    .line 478
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 480
    :cond_3
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 481
    return-void

    .line 449
    :cond_4
    invoke-virtual {v4}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v0

    iput p1, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->e:I

    if-eqz p2, :cond_10

    if-eq p1, v0, :cond_10

    const/4 v0, 0x4

    new-array v5, v0, [Landroid/animation/ObjectAnimator;

    if-ne p1, v3, :cond_b

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->b:Z

    if-eqz v6, :cond_5

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->a:Z

    if-eqz v6, :cond_5

    iget-object v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->c:Landroid/animation/ObjectAnimator;

    if-nez v6, :cond_9

    :cond_5
    move-object v0, v1

    :goto_4
    aput-object v0, v5, v2

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v5, v3

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->b:Z

    if-eqz v6, :cond_6

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->a:Z

    if-eqz v6, :cond_6

    iget-object v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->d:Landroid/animation/ObjectAnimator;

    if-nez v6, :cond_a

    :cond_6
    :goto_5
    aput-object v1, v5, v7

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->b()Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v5, v8

    :cond_7
    :goto_6
    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->o:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_8

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->o:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->o:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->end()V

    :cond_8
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->o:Landroid/animation/AnimatorSet;

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->o:Landroid/animation/AnimatorSet;

    invoke-virtual {v0, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->o:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto/16 :goto_0

    :cond_9
    iget-object v0, v0, Lcom/android/datetimepicker/time/RadialTextsView;->c:Landroid/animation/ObjectAnimator;

    goto :goto_4

    :cond_a
    iget-object v1, v0, Lcom/android/datetimepicker/time/RadialTextsView;->d:Landroid/animation/ObjectAnimator;

    goto :goto_5

    :cond_b
    if-nez p1, :cond_7

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->b:Z

    if-eqz v6, :cond_c

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->a:Z

    if-eqz v6, :cond_c

    iget-object v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->d:Landroid/animation/ObjectAnimator;

    if-nez v6, :cond_e

    :cond_c
    move-object v0, v1

    :goto_7
    aput-object v0, v5, v2

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->b()Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v5, v3

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->b:Z

    if-eqz v6, :cond_d

    iget-boolean v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->a:Z

    if-eqz v6, :cond_d

    iget-object v6, v0, Lcom/android/datetimepicker/time/RadialTextsView;->c:Landroid/animation/ObjectAnimator;

    if-nez v6, :cond_f

    :cond_d
    :goto_8
    aput-object v1, v5, v7

    iget-object v0, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    aput-object v0, v5, v8

    goto :goto_6

    :cond_e
    iget-object v0, v0, Lcom/android/datetimepicker/time/RadialTextsView;->d:Landroid/animation/ObjectAnimator;

    goto :goto_7

    :cond_f
    iget-object v1, v0, Lcom/android/datetimepicker/time/RadialTextsView;->c:Landroid/animation/ObjectAnimator;

    goto :goto_8

    :cond_10
    if-nez p1, :cond_11

    const/16 v0, 0xff

    move v1, v0

    :goto_9
    if-ne p1, v3, :cond_12

    const/16 v0, 0xff

    :goto_a
    iget-object v5, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    int-to-float v6, v1

    invoke-virtual {v5, v6}, Lcom/android/datetimepicker/time/RadialTextsView;->setAlpha(F)V

    iget-object v5, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    int-to-float v1, v1

    invoke-virtual {v5, v1}, Lcom/android/datetimepicker/time/RadialSelectorView;->setAlpha(F)V

    iget-object v1, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    int-to-float v5, v0

    invoke-virtual {v1, v5}, Lcom/android/datetimepicker/time/RadialTextsView;->setAlpha(F)V

    iget-object v1, v4, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->setAlpha(F)V

    goto/16 :goto_0

    :cond_11
    move v1, v2

    goto :goto_9

    :cond_12
    move v0, v2

    goto :goto_a

    .line 463
    :cond_13
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget v0, v0, Lcom/android/datetimepicker/time/RadialPickerLayout;->d:I

    .line 464
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->C:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xd

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 465
    if-eqz p4, :cond_15

    .line 466
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->D:Ljava/lang/String;

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_14

    move v2, v3

    :cond_14
    if-eqz v2, :cond_15

    if-eqz v0, :cond_15

    if-eqz v1, :cond_15

    invoke-virtual {v0, v1}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 468
    :cond_15
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    move-object v2, v0

    goto/16 :goto_1

    .line 471
    :cond_16
    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->q:I

    move v1, v0

    goto/16 :goto_2

    .line 472
    :cond_17
    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->q:I

    goto/16 :goto_3
.end method

.method a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 644
    iput-boolean v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    .line 645
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 646
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a([Ljava/lang/Boolean;)[I

    move-result-object v0

    .line 647
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    aget v2, v0, v4

    aget v3, v0, v5

    invoke-virtual {v1, v4, v2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(II)V

    invoke-virtual {v1, v5, v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(II)V

    .line 648
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v1, :cond_0

    .line 649
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    aget v0, v0, v6

    iget-object v2, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    iput v0, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->k:I

    iget-object v2, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v2}, Lcom/android/datetimepicker/time/AmPmCirclesView;->invalidate()V

    invoke-virtual {v1, v6, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 653
    :cond_1
    if-eqz p1, :cond_2

    .line 654
    invoke-virtual {p0, v4}, Lcom/android/datetimepicker/time/TimePickerDialog;->b(Z)V

    .line 655
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0, v5}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(Z)Z

    .line 657
    :cond_2
    return-void
.end method

.method a()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 618
    iget-boolean v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-eqz v2, :cond_2

    .line 621
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->a([Ljava/lang/Boolean;)[I

    move-result-object v2

    .line 622
    aget v3, v2, v1

    if-ltz v3, :cond_1

    aget v3, v2, v0

    if-ltz v3, :cond_1

    aget v2, v2, v0

    const/16 v3, 0x3c

    if-ge v2, v3, :cond_1

    .line 627
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 622
    goto :goto_0

    .line 626
    :cond_2
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    .line 627
    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method b()I
    .locals 3

    .prologue
    .line 632
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 633
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 634
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 636
    :cond_0
    return v0
.end method

.method b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 563
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 564
    invoke-virtual {p0, p1}, Lcom/android/datetimepicker/time/TimePickerDialog;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 565
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    .line 566
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 567
    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->b(Z)V

    .line 569
    :cond_1
    return-void
.end method

.method b(Z)V
    .locals 10

    .prologue
    const/16 v9, 0x20

    const/4 v8, 0x2

    const/4 v7, -0x1

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 667
    if-nez p1, :cond_3

    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 668
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget v2, v2, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    .line 669
    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget v3, v3, Lcom/android/datetimepicker/time/RadialPickerLayout;->d:I

    .line 670
    invoke-direct {p0, v2, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(IZ)V

    .line 671
    invoke-direct {p0, v3}, Lcom/android/datetimepicker/time/TimePickerDialog;->f(I)V

    .line 672
    iget-boolean v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v3, :cond_0

    .line 673
    const/16 v3, 0xc

    if-ge v2, v3, :cond_2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(I)V

    .line 675
    :cond_0
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v0

    invoke-virtual {p0, v0, v1, v1, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(IZZZ)V

    .line 676
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 696
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 673
    goto :goto_0

    .line 678
    :cond_3
    new-array v3, v8, [Ljava/lang/Boolean;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v3, v1

    .line 679
    invoke-direct {p0, v3}, Lcom/android/datetimepicker/time/TimePickerDialog;->a([Ljava/lang/Boolean;)[I

    move-result-object v4

    .line 680
    aget-object v2, v3, v0

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "%02d"

    .line 681
    :goto_2
    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v3, "%02d"

    .line 682
    :goto_3
    aget v5, v4, v0

    if-ne v5, v7, :cond_6

    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->w:Ljava/lang/String;

    .line 684
    :goto_4
    aget v5, v4, v1

    if-ne v5, v7, :cond_7

    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->w:Ljava/lang/String;

    .line 686
    :goto_5
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 687
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 688
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    iget v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->q:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 689
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 690
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 691
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    iget v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->q:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 692
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v0, :cond_1

    .line 693
    aget v0, v4, v8

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(I)V

    goto :goto_1

    .line 680
    :cond_4
    const-string v2, "%2d"

    goto :goto_2

    .line 681
    :cond_5
    const-string v3, "%2d"

    goto :goto_3

    .line 682
    :cond_6
    new-array v5, v1, [Ljava/lang/Object;

    aget v6, v4, v0

    .line 683
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v0

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-char v5, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->v:C

    invoke-virtual {v2, v9, v5}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 684
    :cond_7
    new-array v5, v1, [Ljava/lang/Object;

    aget v1, v4, v1

    .line 685
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-char v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->v:C

    invoke-virtual {v0, v9, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method c(I)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 574
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v0, :cond_2

    .line 575
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 596
    :cond_1
    :goto_0
    return v2

    .line 579
    :cond_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->x:Lcom/android/datetimepicker/time/k;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v0, v1, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_3

    move-object v0, v4

    :goto_2
    if-nez v0, :cond_8

    move v0, v2

    :goto_3
    if-nez v0, :cond_a

    .line 581
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->b()I

    goto :goto_0

    .line 580
    :cond_3
    iget-object v0, v1, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/datetimepicker/time/k;

    move v1, v2

    :goto_4
    iget-object v8, v0, Lcom/android/datetimepicker/time/k;->a:[I

    array-length v8, v8

    if-ge v1, v8, :cond_6

    iget-object v8, v0, Lcom/android/datetimepicker/time/k;->a:[I

    aget v8, v8, v1

    if-ne v8, v6, :cond_5

    move v1, v3

    :goto_5
    if-eqz v1, :cond_4

    goto :goto_2

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_5

    :cond_7
    move-object v0, v4

    goto :goto_2

    :cond_8
    move-object v1, v0

    goto :goto_1

    :cond_9
    move v0, v3

    goto :goto_3

    .line 585
    :cond_a
    invoke-static {p1}, Lcom/android/datetimepicker/time/TimePickerDialog;->d(I)I

    move-result v0

    .line 586
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    const-string v4, "%d"

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x10

    if-lt v4, v5, :cond_b

    move v2, v3

    :cond_b
    if-eqz v2, :cond_c

    if-eqz v1, :cond_c

    if-eqz v0, :cond_c

    invoke-virtual {v1, v0}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 588
    :cond_c
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->a()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 589
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_d

    .line 590
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 591
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 593
    :cond_d
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEnabled(Z)V

    :cond_e
    move v2, v3

    .line 596
    goto/16 :goto_0
.end method

.method e(I)I
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    const/4 v2, -0x1

    .line 775
    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->y:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->z:I

    if-ne v0, v2, :cond_1

    .line 777
    :cond_0
    invoke-static {v2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v3

    move v0, v1

    .line 780
    :goto_0
    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->c:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget-object v5, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->d:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 781
    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->c:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 782
    iget-object v5, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->d:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 783
    if-eq v4, v5, :cond_2

    .line 784
    new-array v0, v8, [C

    aput-char v4, v0, v1

    aput-char v5, v0, v7

    invoke-virtual {v3, v0}, Landroid/view/KeyCharacterMap;->getEvents([C)[Landroid/view/KeyEvent;

    move-result-object v0

    .line 786
    if-eqz v0, :cond_1

    array-length v3, v0

    const/4 v4, 0x4

    if-ne v3, v4, :cond_1

    .line 787
    aget-object v1, v0, v1

    invoke-virtual {v1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->y:I

    .line 788
    aget-object v0, v0, v8

    invoke-virtual {v0}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->z:I

    .line 796
    :cond_1
    if-nez p1, :cond_3

    .line 797
    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->y:I

    .line 802
    :goto_1
    return v0

    .line 780
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 798
    :cond_3
    if-ne p1, v7, :cond_4

    .line 799
    iget v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->z:I

    goto :goto_1

    :cond_4
    move v0, v2

    .line 802
    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0, p1}, Lcom/android/datetimepicker/DialogFragmentWithListener;->onCreate(Landroid/os/Bundle;)V

    .line 178
    if-eqz p1, :cond_0

    const-string v0, "hour_of_day"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "minute"

    .line 179
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "is_24_hour_view"

    .line 180
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    const-string v0, "hour_of_day"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->s:I

    .line 182
    const-string v0, "minute"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->t:I

    .line 183
    const-string v0, "is_24_hour_view"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    .line 184
    const-string v0, "in_kb_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    .line 185
    const-string v0, "dark_theme"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    .line 187
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 15

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 194
    sget v1, Lcom/android/datetimepicker/h;->b:I

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v13

    .line 195
    new-instance v7, Lcom/android/datetimepicker/time/j;

    invoke-direct {v7, p0}, Lcom/android/datetimepicker/time/j;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;)V

    .line 196
    sget v1, Lcom/android/datetimepicker/g;->t:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 198
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 199
    sget v1, Lcom/android/datetimepicker/i;->g:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->A:Ljava/lang/String;

    .line 200
    sget v1, Lcom/android/datetimepicker/i;->p:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->B:Ljava/lang/String;

    .line 201
    sget v1, Lcom/android/datetimepicker/i;->i:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->C:Ljava/lang/String;

    .line 202
    sget v1, Lcom/android/datetimepicker/i;->q:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->D:Ljava/lang/String;

    .line 203
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v1, :cond_2

    sget v1, Lcom/android/datetimepicker/d;->m:I

    :goto_0
    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->p:I

    .line 204
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v1, :cond_3

    sget v1, Lcom/android/datetimepicker/d;->o:I

    :goto_1
    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->q:I

    .line 206
    sget v1, Lcom/android/datetimepicker/g;->l:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    .line 207
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 208
    sget v1, Lcom/android/datetimepicker/g;->k:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->k:Landroid/widget/TextView;

    .line 209
    sget v1, Lcom/android/datetimepicker/g;->o:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->m:Landroid/widget/TextView;

    .line 210
    sget v1, Lcom/android/datetimepicker/g;->n:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    .line 211
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 212
    sget v1, Lcom/android/datetimepicker/g;->b:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->n:Landroid/widget/TextView;

    .line 213
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 214
    new-instance v1, Ljava/text/DateFormatSymbols;

    invoke-direct {v1}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v1}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v1

    .line 215
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iput-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->c:Ljava/lang/String;

    .line 216
    const/4 v2, 0x1

    aget-object v1, v1, v2

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->d:Ljava/lang/String;

    .line 218
    new-instance v1, Lcom/android/datetimepicker/a;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/android/datetimepicker/a;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->a:Lcom/android/datetimepicker/a;

    .line 220
    sget v1, Lcom/android/datetimepicker/g;->s:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/datetimepicker/time/RadialPickerLayout;

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    .line 221
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iput-object p0, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->b:Lcom/android/datetimepicker/time/c;

    .line 222
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v1, v7}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 223
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->a:Lcom/android/datetimepicker/a;

    iget v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->s:I

    iget v5, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->t:I

    iget-boolean v6, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    invoke-virtual/range {v1 .. v6}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(Landroid/content/Context;Lcom/android/datetimepicker/a;IIZ)V

    .line 226
    const/4 v1, 0x0

    .line 227
    if-eqz p3, :cond_0

    const-string v2, "current_item_showing"

    .line 228
    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 229
    const-string v1, "current_item_showing"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 231
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(IZZZ)V

    .line 232
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->invalidate()V

    .line 234
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    new-instance v2, Lcom/android/datetimepicker/time/f;

    invoke-direct {v2, p0}, Lcom/android/datetimepicker/time/f;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 241
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->l:Landroid/widget/TextView;

    new-instance v2, Lcom/android/datetimepicker/time/g;

    invoke-direct {v2, p0}, Lcom/android/datetimepicker/time/g;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    sget v1, Lcom/android/datetimepicker/g;->j:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    .line 250
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    new-instance v2, Lcom/android/datetimepicker/time/h;

    invoke-direct {v2, p0}, Lcom/android/datetimepicker/time/h;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 265
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 268
    sget v1, Lcom/android/datetimepicker/g;->a:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->o:Landroid/view/View;

    .line 269
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-eqz v1, :cond_4

    .line 270
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->n:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 272
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v3, -0x2

    invoke-direct {v2, v1, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 274
    const/16 v1, 0xd

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 275
    sget v1, Lcom/android/datetimepicker/g;->p:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 276
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 296
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->r:Z

    .line 297
    iget v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->s:I

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(IZ)V

    .line 298
    iget v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->t:I

    invoke-direct {p0, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->f(I)V

    .line 301
    sget v1, Lcom/android/datetimepicker/i;->w:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->w:Ljava/lang/String;

    .line 302
    sget v1, Lcom/android/datetimepicker/i;->f:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->f:Ljava/lang/String;

    .line 303
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->w:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    iput-char v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->v:C

    .line 304
    const/4 v1, -0x1

    iput v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->z:I

    iput v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->y:I

    .line 305
    new-instance v1, Lcom/android/datetimepicker/time/k;

    const/4 v2, 0x0

    new-array v2, v2, [I

    invoke-direct {v1, p0, v2}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->x:Lcom/android/datetimepicker/time/k;

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-eqz v1, :cond_6

    new-instance v1, Lcom/android/datetimepicker/time/k;

    const/4 v2, 0x6

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-direct {v1, p0, v2}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    new-instance v2, Lcom/android/datetimepicker/time/k;

    const/16 v3, 0xa

    new-array v3, v3, [I

    fill-array-data v3, :array_1

    invoke-direct {v2, p0, v3}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, v1, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/datetimepicker/time/k;

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    invoke-direct {v3, p0, v4}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->x:Lcom/android/datetimepicker/time/k;

    iget-object v4, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/android/datetimepicker/time/k;

    const/4 v5, 0x6

    new-array v5, v5, [I

    fill-array-data v5, :array_3

    invoke-direct {v4, p0, v5}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v5, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/android/datetimepicker/time/k;

    const/4 v6, 0x4

    new-array v6, v6, [I

    fill-array-data v6, :array_4

    invoke-direct {v5, p0, v6}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v4, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/android/datetimepicker/time/k;

    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_5

    invoke-direct {v4, p0, v5}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/datetimepicker/time/k;

    const/4 v4, 0x1

    new-array v4, v4, [I

    const/4 v5, 0x0

    const/16 v6, 0x9

    aput v6, v4, v5

    invoke-direct {v3, p0, v4}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v4, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->x:Lcom/android/datetimepicker/time/k;

    iget-object v4, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/android/datetimepicker/time/k;

    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_6

    invoke-direct {v4, p0, v5}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v5, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/android/datetimepicker/time/k;

    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_7

    invoke-direct {v4, p0, v5}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/datetimepicker/time/k;

    const/4 v3, 0x7

    new-array v3, v3, [I

    fill-array-data v3, :array_8

    invoke-direct {v2, p0, v3}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->x:Lcom/android/datetimepicker/time/k;

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 306
    :goto_3
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    if-eqz v1, :cond_7

    .line 307
    const-string v1, "typed_times"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    .line 308
    const/4 v1, -0x1

    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->b(I)V

    .line 309
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->j:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->invalidate()V

    .line 315
    :cond_1
    :goto_4
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/TimePickerDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    iget-object v4, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->f:Lcom/android/datetimepicker/time/CircleView;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v3, :cond_8

    sget v6, Lcom/android/datetimepicker/d;->d:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, v4, Lcom/android/datetimepicker/time/CircleView;->b:I

    sget v6, Lcom/android/datetimepicker/d;->i:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, v4, Lcom/android/datetimepicker/time/CircleView;->c:I

    :goto_5
    iget-object v4, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    if-eqz v3, :cond_9

    sget v6, Lcom/android/datetimepicker/d;->d:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->c:I

    sget v6, Lcom/android/datetimepicker/d;->m:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->e:I

    sget v6, Lcom/android/datetimepicker/d;->o:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->d:I

    const/16 v5, 0x66

    iput v5, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->b:I

    :goto_6
    iget-object v4, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-virtual {v4, v2, v3}, Lcom/android/datetimepicker/time/RadialTextsView;->a(Landroid/content/Context;Z)V

    iget-object v4, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-virtual {v4, v2, v3}, Lcom/android/datetimepicker/time/RadialTextsView;->a(Landroid/content/Context;Z)V

    iget-object v4, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v4, v2, v3}, Lcom/android/datetimepicker/time/RadialSelectorView;->a(Landroid/content/Context;Z)V

    iget-object v1, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v1, v2, v3}, Lcom/android/datetimepicker/time/RadialSelectorView;->a(Landroid/content/Context;Z)V

    .line 317
    sget v1, Lcom/android/datetimepicker/d;->o:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 318
    sget v1, Lcom/android/datetimepicker/d;->c:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v10

    .line 319
    sget v1, Lcom/android/datetimepicker/d;->j:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 320
    sget v1, Lcom/android/datetimepicker/d;->l:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 321
    sget v1, Lcom/android/datetimepicker/d;->g:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v8

    .line 322
    sget v12, Lcom/android/datetimepicker/f;->a:I

    .line 324
    sget v1, Lcom/android/datetimepicker/d;->d:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 325
    sget v1, Lcom/android/datetimepicker/d;->i:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    .line 326
    sget v1, Lcom/android/datetimepicker/d;->k:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 327
    sget v1, Lcom/android/datetimepicker/d;->h:I

    invoke-virtual {v11, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v7

    .line 328
    sget v11, Lcom/android/datetimepicker/f;->b:I

    .line 331
    sget v1, Lcom/android/datetimepicker/g;->r:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v1, :cond_a

    move v1, v2

    :goto_7
    invoke-virtual {v14, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 332
    sget v1, Lcom/android/datetimepicker/g;->q:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v14, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v14, :cond_b

    :goto_8
    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 333
    sget v1, Lcom/android/datetimepicker/g;->p:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v2, :cond_c

    move v2, v3

    :goto_9
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 334
    sget v1, Lcom/android/datetimepicker/g;->b:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-boolean v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v2, :cond_d

    :goto_a
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 335
    sget v1, Lcom/android/datetimepicker/g;->m:I

    invoke-virtual {v13, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v1, :cond_e

    move v1, v5

    :goto_b
    invoke-virtual {v2, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 336
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v1, :cond_f

    move-object v1, v7

    :goto_c
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 337
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v1, :cond_10

    move v1, v9

    :goto_d
    invoke-virtual {v2, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setBackgroundColor(I)V

    .line 338
    iget-object v2, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->i:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    if-eqz v1, :cond_11

    move v1, v11

    :goto_e
    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 339
    return-object v13

    .line 203
    :cond_2
    sget v1, Lcom/android/datetimepicker/d;->b:I

    goto/16 :goto_0

    .line 204
    :cond_3
    sget v1, Lcom/android/datetimepicker/d;->l:I

    goto/16 :goto_1

    .line 278
    :cond_4
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->n:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 279
    iget v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->s:I

    const/16 v2, 0xc

    if-ge v1, v2, :cond_5

    const/4 v1, 0x0

    :goto_f
    invoke-virtual {p0, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(I)V

    .line 280
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->o:Landroid/view/View;

    new-instance v2, Lcom/android/datetimepicker/time/i;

    invoke-direct {v2, p0}, Lcom/android/datetimepicker/time/i;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    .line 279
    :cond_5
    const/4 v1, 0x1

    goto :goto_f

    .line 305
    :cond_6
    new-instance v1, Lcom/android/datetimepicker/time/k;

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v4

    aput v4, v2, v3

    invoke-direct {v1, p0, v2}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    new-instance v2, Lcom/android/datetimepicker/time/k;

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const/16 v5, 0x8

    aput v5, v3, v4

    invoke-direct {v2, p0, v3}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->x:Lcom/android/datetimepicker/time/k;

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/datetimepicker/time/k;

    const/4 v4, 0x3

    new-array v4, v4, [I

    fill-array-data v4, :array_9

    invoke-direct {v3, p0, v4}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v4, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/android/datetimepicker/time/k;

    const/4 v5, 0x6

    new-array v5, v5, [I

    fill-array-data v5, :array_a

    invoke-direct {v4, p0, v5}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v5, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v5, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/android/datetimepicker/time/k;

    const/16 v6, 0xa

    new-array v6, v6, [I

    fill-array-data v6, :array_b

    invoke-direct {v5, p0, v6}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v4, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v4, v5, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/android/datetimepicker/time/k;

    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_c

    invoke-direct {v4, p0, v5}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v4, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/datetimepicker/time/k;

    const/4 v4, 0x3

    new-array v4, v4, [I

    fill-array-data v4, :array_d

    invoke-direct {v3, p0, v4}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v2, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/datetimepicker/time/k;

    const/16 v4, 0xa

    new-array v4, v4, [I

    fill-array-data v4, :array_e

    invoke-direct {v2, p0, v4}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/datetimepicker/time/k;

    const/16 v3, 0x8

    new-array v3, v3, [I

    fill-array-data v3, :array_f

    invoke-direct {v2, p0, v3}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->x:Lcom/android/datetimepicker/time/k;

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/android/datetimepicker/time/k;

    const/4 v4, 0x6

    new-array v4, v4, [I

    fill-array-data v4, :array_10

    invoke-direct {v3, p0, v4}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v2, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/android/datetimepicker/time/k;

    const/16 v4, 0xa

    new-array v4, v4, [I

    fill-array-data v4, :array_11

    invoke-direct {v2, p0, v4}, Lcom/android/datetimepicker/time/k;-><init>(Lcom/android/datetimepicker/time/TimePickerDialog;[I)V

    iget-object v3, v3, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v2, Lcom/android/datetimepicker/time/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 310
    :cond_7
    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 311
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    goto/16 :goto_4

    .line 315
    :cond_8
    sget v6, Lcom/android/datetimepicker/d;->o:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, v4, Lcom/android/datetimepicker/time/CircleView;->b:I

    sget v6, Lcom/android/datetimepicker/d;->l:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, v4, Lcom/android/datetimepicker/time/CircleView;->c:I

    goto/16 :goto_5

    :cond_9
    sget v6, Lcom/android/datetimepicker/d;->o:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->c:I

    sget v6, Lcom/android/datetimepicker/d;->b:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    iput v6, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->e:I

    sget v6, Lcom/android/datetimepicker/d;->a:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->d:I

    const/16 v5, 0x33

    iput v5, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->b:I

    goto/16 :goto_6

    :cond_a
    move v1, v3

    .line 331
    goto/16 :goto_7

    :cond_b
    move v2, v3

    .line 332
    goto/16 :goto_8

    :cond_c
    move v2, v4

    .line 333
    goto/16 :goto_9

    :cond_d
    move v3, v4

    .line 334
    goto/16 :goto_a

    :cond_e
    move v1, v6

    .line 335
    goto/16 :goto_b

    :cond_f
    move-object v1, v8

    .line 336
    goto/16 :goto_c

    :cond_10
    move v1, v10

    .line 337
    goto/16 :goto_d

    :cond_11
    move v1, v12

    .line 338
    goto/16 :goto_e

    .line 305
    nop

    :array_0
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_1
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_2
    .array-data 4
        0x7
        0x8
    .end array-data

    :array_3
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_4
    .array-data 4
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_5
    .array-data 4
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_6
    .array-data 4
        0x7
        0x8
        0x9
        0xa
    .end array-data

    :array_7
    .array-data 4
        0xb
        0xc
    .end array-data

    :array_8
    .array-data 4
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_9
    .array-data 4
        0x7
        0x8
        0x9
    .end array-data

    :array_a
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_b
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_c
    .array-data 4
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_d
    .array-data 4
        0xa
        0xb
        0xc
    .end array-data

    :array_e
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_f
    .array-data 4
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data

    :array_10
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
    .end array-data

    :array_11
    .array-data 4
        0x7
        0x8
        0x9
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        0x10
    .end array-data
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 350
    invoke-super {p0}, Lcom/android/datetimepicker/DialogFragmentWithListener;->onPause()V

    .line 351
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->a:Lcom/android/datetimepicker/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/android/datetimepicker/a;->c:Landroid/os/Vibrator;

    iget-object v1, v0, Lcom/android/datetimepicker/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v0, v0, Lcom/android/datetimepicker/a;->b:Landroid/database/ContentObserver;

    invoke-virtual {v1, v0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 352
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 344
    invoke-super {p0}, Lcom/android/datetimepicker/DialogFragmentWithListener;->onResume()V

    .line 345
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->a:Lcom/android/datetimepicker/a;

    invoke-virtual {v0}, Lcom/android/datetimepicker/a;->a()V

    .line 346
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    if-eqz v0, :cond_1

    .line 375
    const-string v0, "hour_of_day"

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget v1, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 376
    const-string v0, "minute"

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget v1, v1, Lcom/android/datetimepicker/time/RadialPickerLayout;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 377
    const-string v0, "is_24_hour_view"

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 378
    const-string v0, "current_item_showing"

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 379
    const-string v0, "in_kb_mode"

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 380
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    if-eqz v0, :cond_0

    .line 381
    const-string v0, "typed_times"

    iget-object v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 383
    :cond_0
    const-string v0, "dark_theme"

    iget-boolean v1, p0, Lcom/android/datetimepicker/time/TimePickerDialog;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 385
    :cond_1
    return-void
.end method
