.class public Lcom/android/datetimepicker/time/RadialPickerLayout;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private A:F

.field private B:Landroid/view/accessibility/AccessibilityManager;

.field private C:Landroid/os/Handler;

.field a:I

.field b:Lcom/android/datetimepicker/time/c;

.field c:I

.field d:I

.field e:I

.field f:Lcom/android/datetimepicker/time/CircleView;

.field g:Lcom/android/datetimepicker/time/AmPmCirclesView;

.field h:Lcom/android/datetimepicker/time/RadialTextsView;

.field i:Lcom/android/datetimepicker/time/RadialTextsView;

.field j:Lcom/android/datetimepicker/time/RadialSelectorView;

.field k:Lcom/android/datetimepicker/time/RadialSelectorView;

.field l:I

.field m:Z

.field n:I

.field o:Landroid/animation/AnimatorSet;

.field private final p:I

.field private final q:I

.field private r:Lcom/android/datetimepicker/a;

.field private s:Z

.field private t:Z

.field private u:Z

.field private v:Landroid/view/View;

.field private w:[I

.field private x:Z

.field private y:Z

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 101
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    iput v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    .line 94
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->C:Landroid/os/Handler;

    .line 103
    invoke-virtual {p0, p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 104
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->p:I

    .line 106
    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->q:I

    .line 107
    iput-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->m:Z

    .line 109
    new-instance v0, Lcom/android/datetimepicker/time/CircleView;

    invoke-direct {v0, p1}, Lcom/android/datetimepicker/time/CircleView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->f:Lcom/android/datetimepicker/time/CircleView;

    .line 110
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->f:Lcom/android/datetimepicker/time/CircleView;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 112
    new-instance v0, Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-direct {v0, p1}, Lcom/android/datetimepicker/time/AmPmCirclesView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    .line 113
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 115
    new-instance v0, Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-direct {v0, p1}, Lcom/android/datetimepicker/time/RadialTextsView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    .line 116
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 117
    new-instance v0, Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-direct {v0, p1}, Lcom/android/datetimepicker/time/RadialTextsView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    .line 118
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 120
    new-instance v0, Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-direct {v0, p1}, Lcom/android/datetimepicker/time/RadialSelectorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    .line 121
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 122
    new-instance v0, Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-direct {v0, p1}, Lcom/android/datetimepicker/time/RadialSelectorView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    .line 123
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 126
    invoke-direct {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b()V

    .line 128
    iput v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->a:I

    .line 130
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->x:Z

    .line 131
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->v:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->v:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 134
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->v:Landroid/view/View;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/android/datetimepicker/d;->n:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 135
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->v:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 136
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->v:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->addView(Landroid/view/View;)V

    .line 138
    const-string v0, "accessibility"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->B:Landroid/view/accessibility/AccessibilityManager;

    .line 140
    iput-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->s:Z

    .line 141
    return-void
.end method

.method private a(FFZ[Ljava/lang/Boolean;)I
    .locals 2

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v0

    .line 496
    if-nez v0, :cond_0

    .line 497
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/datetimepicker/time/RadialSelectorView;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    .line 503
    :goto_0
    return v0

    .line 499
    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 500
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/datetimepicker/time/RadialSelectorView;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    goto :goto_0

    .line 503
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private b()V
    .locals 7

    .prologue
    const/16 v6, 0x169

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 344
    new-array v1, v6, [I

    iput-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->w:[I

    .line 353
    const/16 v1, 0x8

    move v4, v0

    move v3, v0

    move v0, v1

    move v1, v2

    .line 355
    :goto_0
    if-ge v4, v6, :cond_3

    .line 357
    iget-object v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->w:[I

    aput v3, v5, v4

    .line 360
    if-ne v1, v0, :cond_2

    .line 361
    add-int/lit8 v1, v3, 0x6

    .line 362
    const/16 v0, 0x168

    if-ne v1, v0, :cond_0

    .line 363
    const/4 v0, 0x7

    :goto_1
    move v3, v1

    move v1, v2

    .line 355
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 364
    :cond_0
    rem-int/lit8 v0, v1, 0x1e

    if-nez v0, :cond_1

    .line 365
    const/16 v0, 0xe

    goto :goto_1

    .line 367
    :cond_1
    const/4 v0, 0x4

    goto :goto_1

    .line 371
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 374
    :cond_3
    return-void
.end method

.method private static c(II)I
    .locals 4

    .prologue
    .line 400
    div-int/lit8 v0, p0, 0x1e

    mul-int/lit8 v0, v0, 0x1e

    .line 402
    add-int/lit8 v1, v0, 0x1e

    .line 403
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    move v0, v1

    .line 417
    :cond_0
    :goto_0
    return v0

    .line 405
    :cond_1
    const/4 v2, -0x1

    if-ne p1, v2, :cond_2

    .line 406
    if-ne p0, v0, :cond_0

    .line 407
    add-int/lit8 v0, v0, -0x1e

    goto :goto_0

    .line 411
    :cond_2
    sub-int v2, p0, v0

    sub-int v3, v1, p0

    if-lt v2, v3, :cond_0

    move v0, v1

    .line 414
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 511
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->e:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 512
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->e:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x39

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Current item showing was unfortunately set to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 513
    const/4 v0, -0x1

    .line 515
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->e:I

    goto :goto_0
.end method

.method a(IZZZ)I
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v0, -0x1

    const/16 v2, 0x168

    const/4 v3, 0x0

    .line 435
    if-ne p1, v0, :cond_0

    .line 478
    :goto_0
    return v0

    .line 438
    :cond_0
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v6

    .line 441
    if-nez p3, :cond_1

    if-ne v6, v5, :cond_1

    move v1, v5

    .line 442
    :goto_1
    if-eqz v1, :cond_3

    .line 443
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->w:[I

    if-nez v1, :cond_2

    :goto_2
    move v4, v0

    .line 449
    :goto_3
    if-nez v6, :cond_4

    .line 450
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    .line 451
    const/16 v1, 0x1e

    .line 456
    :goto_4
    invoke-virtual {v0, v4, p2, p4}, Lcom/android/datetimepicker/time/RadialSelectorView;->setSelection(IZZ)V

    .line 457
    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->invalidate()V

    .line 460
    if-nez v6, :cond_7

    .line 461
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    if-eqz v0, :cond_6

    .line 462
    if-nez v4, :cond_5

    if-eqz p2, :cond_5

    move v0, v2

    .line 474
    :goto_5
    div-int v1, v0, v1

    .line 475
    if-nez v6, :cond_8

    iget-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    if-eqz v2, :cond_8

    if-nez p2, :cond_8

    if-eqz v0, :cond_8

    .line 476
    add-int/lit8 v0, v1, 0xc

    goto :goto_0

    :cond_1
    move v1, v3

    .line 441
    goto :goto_1

    .line 443
    :cond_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->w:[I

    aget v0, v0, p1

    goto :goto_2

    .line 445
    :cond_3
    invoke-static {p1, v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->c(II)I

    move-result v4

    goto :goto_3

    .line 453
    :cond_4
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    .line 454
    const/4 v1, 0x6

    goto :goto_4

    .line 464
    :cond_5
    if-ne v4, v2, :cond_9

    if-nez p2, :cond_9

    move v0, v3

    .line 465
    goto :goto_5

    .line 467
    :cond_6
    if-nez v4, :cond_9

    move v0, v2

    .line 468
    goto :goto_5

    .line 470
    :cond_7
    if-ne v4, v2, :cond_9

    if-ne v6, v5, :cond_9

    move v0, v3

    .line 471
    goto :goto_5

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    move v0, v4

    goto :goto_5
.end method

.method a(II)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 239
    if-nez p1, :cond_2

    .line 240
    invoke-virtual {p0, v1, p2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 241
    rem-int/lit8 v2, p2, 0xc

    mul-int/lit8 v2, v2, 0x1e

    .line 242
    iget-object v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    iget-boolean v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    if-eqz v4, :cond_1

    const/16 v4, 0xc

    if-gt p2, v4, :cond_1

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {v3, v2, v0, v1}, Lcom/android/datetimepicker/time/RadialSelectorView;->setSelection(IZZ)V

    .line 243
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->invalidate()V

    .line 250
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 242
    goto :goto_0

    .line 244
    :cond_2
    if-ne p1, v0, :cond_0

    .line 245
    invoke-virtual {p0, v0, p2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 246
    mul-int/lit8 v0, p2, 0x6

    .line 247
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v2, v0, v1, v1}, Lcom/android/datetimepicker/time/RadialSelectorView;->setSelection(IZZ)V

    .line 248
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/RadialSelectorView;->invalidate()V

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/android/datetimepicker/a;IIZ)V
    .locals 13

    .prologue
    .line 171
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->s:Z

    if-eqz v1, :cond_0

    .line 219
    :goto_0
    return-void

    .line 176
    :cond_0
    iput-object p2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->r:Lcom/android/datetimepicker/a;

    .line 177
    move/from16 v0, p5

    iput-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    .line 178
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->B:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    .line 181
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->f:Lcom/android/datetimepicker/time/CircleView;

    iget-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    iget-boolean v3, v1, Lcom/android/datetimepicker/time/CircleView;->f:Z

    if-nez v3, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-boolean v2, v1, Lcom/android/datetimepicker/time/CircleView;->a:Z

    if-eqz v2, :cond_5

    sget v2, Lcom/android/datetimepicker/i;->c:I

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v1, Lcom/android/datetimepicker/time/CircleView;->d:F

    :goto_2
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/android/datetimepicker/time/CircleView;->f:Z

    .line 182
    :cond_1
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->f:Lcom/android/datetimepicker/time/CircleView;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/CircleView;->invalidate()V

    .line 183
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    if-nez v1, :cond_3

    .line 184
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    const/16 v1, 0xc

    move/from16 v0, p3

    if-ge v0, v1, :cond_6

    const/4 v1, 0x0

    :goto_3
    iget-boolean v3, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->j:Z

    if-nez v3, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/android/datetimepicker/d;->o:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->c:I

    sget v4, Lcom/android/datetimepicker/d;->b:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->e:I

    sget v4, Lcom/android/datetimepicker/d;->a:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iput v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->d:I

    const/16 v4, 0x33

    iput v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->b:I

    sget v4, Lcom/android/datetimepicker/i;->n:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v5}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v4

    iget-object v5, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->a:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->a:Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->a:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    sget v4, Lcom/android/datetimepicker/i;->b:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    iput v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->f:F

    sget v4, Lcom/android/datetimepicker/i;->a:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    iput v3, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->g:F

    new-instance v3, Ljava/text/DateFormatSymbols;

    invoke-direct {v3}, Ljava/text/DateFormatSymbols;-><init>()V

    invoke-virtual {v3}, Ljava/text/DateFormatSymbols;->getAmPmStrings()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v4, v3, v4

    iput-object v4, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->h:Ljava/lang/String;

    const/4 v4, 0x1

    aget-object v3, v3, v4

    iput-object v3, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->i:Ljava/lang/String;

    iput v1, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->k:I

    const/4 v1, -0x1

    iput v1, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->l:I

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/android/datetimepicker/time/AmPmCirclesView;->j:Z

    .line 185
    :cond_2
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/AmPmCirclesView;->invalidate()V

    .line 189
    :cond_3
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 190
    const/16 v1, 0xc

    new-array v6, v1, [I

    fill-array-data v6, :array_0

    .line 191
    const/16 v1, 0xc

    new-array v7, v1, [I

    fill-array-data v7, :array_1

    .line 192
    const/16 v1, 0xc

    new-array v8, v1, [I

    fill-array-data v8, :array_2

    .line 193
    const/16 v1, 0xc

    new-array v3, v1, [Ljava/lang/String;

    .line 194
    const/16 v1, 0xc

    new-array v4, v1, [Ljava/lang/String;

    .line 195
    const/16 v1, 0xc

    new-array v9, v1, [Ljava/lang/String;

    .line 196
    const/4 v1, 0x0

    move v5, v1

    :goto_4
    const/16 v1, 0xc

    if-ge v5, v1, :cond_8

    .line 197
    if-eqz p5, :cond_7

    const-string v1, "%02d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v7, v5

    .line 198
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    aput-object v1, v3, v5

    .line 199
    const-string v1, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v6, v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 200
    const-string v1, "%02d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v8, v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v5

    .line 196
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_4

    .line 178
    :cond_4
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    goto/16 :goto_1

    .line 181
    :cond_5
    sget v2, Lcom/android/datetimepicker/i;->b:I

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v1, Lcom/android/datetimepicker/time/CircleView;->d:F

    sget v2, Lcom/android/datetimepicker/i;->a:I

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v1, Lcom/android/datetimepicker/time/CircleView;->e:F

    goto/16 :goto_2

    .line 184
    :cond_6
    const/4 v1, 0x1

    goto/16 :goto_3

    .line 198
    :cond_7
    const-string v1, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aget v12, v6, v5

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    .line 202
    :cond_8
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    if-eqz p5, :cond_9

    :goto_6
    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Lcom/android/datetimepicker/time/RadialTextsView;->a(Landroid/content/res/Resources;[Ljava/lang/String;[Ljava/lang/String;ZZ)V

    .line 204
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->h:Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/RadialTextsView;->invalidate()V

    .line 205
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    const/4 v6, 0x0

    move-object v3, v9

    invoke-virtual/range {v1 .. v6}, Lcom/android/datetimepicker/time/RadialTextsView;->a(Landroid/content/res/Resources;[Ljava/lang/String;[Ljava/lang/String;ZZ)V

    .line 206
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->i:Lcom/android/datetimepicker/time/RadialTextsView;

    invoke-virtual {v1}, Lcom/android/datetimepicker/time/RadialTextsView;->invalidate()V

    .line 209
    const/4 v1, 0x0

    move/from16 v0, p3

    invoke-virtual {p0, v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 210
    const/4 v1, 0x1

    move/from16 v0, p4

    invoke-virtual {p0, v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 211
    rem-int/lit8 v1, p3, 0xc

    mul-int/lit8 v6, v1, 0x1e

    .line 212
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->j:Lcom/android/datetimepicker/time/RadialSelectorView;

    iget-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    const/4 v5, 0x1

    .line 213
    iget-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    if-eqz v2, :cond_a

    const/16 v2, 0xc

    move/from16 v0, p3

    if-gt v0, v2, :cond_a

    if-eqz p3, :cond_a

    const/4 v7, 0x1

    :goto_7
    move-object v2, p1

    move/from16 v4, p5

    .line 212
    invoke-virtual/range {v1 .. v7}, Lcom/android/datetimepicker/time/RadialSelectorView;->a(Landroid/content/Context;ZZZIZ)V

    .line 214
    mul-int/lit8 v6, p4, 0x6

    .line 215
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->k:Lcom/android/datetimepicker/time/RadialSelectorView;

    iget-boolean v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    invoke-virtual/range {v1 .. v7}, Lcom/android/datetimepicker/time/RadialSelectorView;->a(Landroid/content/Context;ZZZIZ)V

    .line 218
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->s:Z

    goto/16 :goto_0

    .line 202
    :cond_9
    const/4 v4, 0x0

    goto :goto_6

    .line 213
    :cond_a
    const/4 v7, 0x0

    goto :goto_7

    .line 190
    nop

    :array_0
    .array-data 4
        0xc
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        0xa
        0xb
    .end array-data

    .line 191
    :array_1
    .array-data 4
        0x0
        0xd
        0xe
        0xf
        0x10
        0x11
        0x12
        0x13
        0x14
        0x15
        0x16
        0x17
    .end array-data

    .line 192
    :array_2
    .array-data 4
        0x0
        0x5
        0xa
        0xf
        0x14
        0x19
        0x1e
        0x23
        0x28
        0x2d
        0x32
        0x37
    .end array-data
.end method

.method public final a(Z)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 728
    iget-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->y:Z

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    .line 735
    :goto_0
    return v0

    .line 733
    :cond_0
    iput-boolean p1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->x:Z

    .line 734
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->v:Landroid/view/View;

    if-eqz p1, :cond_1

    const/4 v0, 0x4

    :cond_1
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 735
    const/4 v0, 0x1

    goto :goto_0
.end method

.method b(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 297
    if-nez p1, :cond_1

    .line 298
    iput p2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    if-ne p1, v1, :cond_2

    .line 300
    iput p2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->d:I

    goto :goto_0

    .line 301
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 302
    if-nez p2, :cond_3

    .line 303
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    rem-int/lit8 v0, v0, 0xc

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    goto :goto_0

    .line 304
    :cond_3
    if-ne p2, v1, :cond_0

    .line 305
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    rem-int/lit8 v0, v0, 0xc

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    goto :goto_0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 754
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v2, 0x20

    if-ne v0, v2, :cond_0

    .line 756
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 757
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 758
    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    iput v2, v0, Landroid/text/format/Time;->hour:I

    .line 759
    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->d:I

    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 760
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 762
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    if-eqz v0, :cond_1

    .line 763
    const/16 v0, 0x81

    .line 765
    :goto_0
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, v2, v3, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 766
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 769
    :goto_1
    return v1

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 744
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 745
    const/16 v0, 0x1000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 746
    const/16 v0, 0x2000

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 747
    return-void
.end method

.method public onMeasure(II)V
    .locals 4

    .prologue
    .line 148
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 149
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 150
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 151
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 152
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 154
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 155
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 154
    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 156
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/16 v7, 0xc

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 564
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 565
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    .line 568
    new-array v5, v1, [Ljava/lang/Boolean;

    .line 569
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    .line 571
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    move v1, v2

    .line 721
    :cond_1
    :goto_1
    return v1

    .line 573
    :pswitch_0
    iget-boolean v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->x:Z

    if-eqz v6, :cond_1

    .line 577
    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->z:F

    .line 578
    iput v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->A:F

    .line 580
    iput v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->a:I

    .line 581
    iput-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->m:Z

    .line 582
    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->y:Z

    .line 584
    iget-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->u:Z

    if-nez v2, :cond_3

    .line 585
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v2, v0, v4}, Lcom/android/datetimepicker/time/AmPmCirclesView;->a(FF)I

    move-result v2

    iput v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    .line 589
    :goto_2
    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-ne v2, v1, :cond_4

    .line 592
    :cond_2
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->r:Lcom/android/datetimepicker/a;

    invoke-virtual {v0}, Lcom/android/datetimepicker/a;->b()V

    .line 593
    iput v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->n:I

    .line 594
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->C:Landroid/os/Handler;

    new-instance v2, Lcom/android/datetimepicker/time/a;

    invoke-direct {v2, p0}, Lcom/android/datetimepicker/time/a;-><init>(Lcom/android/datetimepicker/time/RadialPickerLayout;)V

    iget v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->q:I

    int-to-long v4, v3

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 587
    :cond_3
    iput v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    goto :goto_2

    .line 604
    :cond_4
    iget-object v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->B:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v2}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v2

    .line 606
    invoke-direct {p0, v0, v4, v2, v5}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->n:I

    .line 607
    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->n:I

    if-eq v0, v3, :cond_1

    .line 610
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->r:Lcom/android/datetimepicker/a;

    invoke-virtual {v0}, Lcom/android/datetimepicker/a;->b()V

    .line 611
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->C:Landroid/os/Handler;

    new-instance v2, Lcom/android/datetimepicker/time/b;

    invoke-direct {v2, p0, v5}, Lcom/android/datetimepicker/time/b;-><init>(Lcom/android/datetimepicker/time/RadialPickerLayout;[Ljava/lang/Boolean;)V

    iget v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->q:I

    int-to-long v4, v3

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 625
    :pswitch_1
    iget-boolean v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->x:Z

    if-eqz v6, :cond_1

    .line 631
    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->A:F

    sub-float v6, v4, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 632
    iget v7, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->z:F

    sub-float v7, v0, v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    .line 634
    iget-boolean v8, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->m:Z

    if-nez v8, :cond_5

    iget v8, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->p:I

    int-to-float v8, v8

    cmpg-float v7, v7, v8

    if-gtz v7, :cond_5

    iget v7, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->p:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-lez v6, :cond_0

    .line 636
    :cond_5
    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-eqz v6, :cond_6

    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-ne v6, v1, :cond_7

    .line 643
    :cond_6
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->C:Landroid/os/Handler;

    invoke-virtual {v1, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 644
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v1, v0, v4}, Lcom/android/datetimepicker/time/AmPmCirclesView;->a(FF)I

    move-result v0

    .line 645
    iget v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-eq v0, v1, :cond_0

    .line 646
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    iput v3, v0, Lcom/android/datetimepicker/time/AmPmCirclesView;->l:I

    .line 647
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v0}, Lcom/android/datetimepicker/time/AmPmCirclesView;->invalidate()V

    .line 648
    iput v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    goto/16 :goto_0

    .line 653
    :cond_7
    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->n:I

    if-eq v6, v3, :cond_0

    .line 655
    iput-boolean v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->m:Z

    .line 660
    iget-object v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->C:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 661
    invoke-direct {p0, v0, v4, v1, v5}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v0

    .line 662
    if-eq v0, v3, :cond_1

    .line 663
    aget-object v3, v5, v2

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {p0, v0, v3, v2, v1}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(IZZZ)I

    move-result v0

    .line 664
    iget v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->a:I

    if-eq v0, v3, :cond_1

    .line 665
    iget-object v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->r:Lcom/android/datetimepicker/a;

    invoke-virtual {v3}, Lcom/android/datetimepicker/a;->b()V

    .line 666
    iput v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->a:I

    .line 667
    iget-object v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->b:Lcom/android/datetimepicker/time/c;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v4

    invoke-interface {v3, v4, v0, v2}, Lcom/android/datetimepicker/time/c;->a(IIZ)V

    goto/16 :goto_1

    .line 672
    :pswitch_2
    iget-boolean v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->x:Z

    if-nez v6, :cond_8

    .line 674
    iget-object v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->b:Lcom/android/datetimepicker/time/c;

    const/4 v3, 0x3

    invoke-interface {v0, v3, v1, v2}, Lcom/android/datetimepicker/time/c;->a(IIZ)V

    goto/16 :goto_1

    .line 679
    :cond_8
    iget-object v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->C:Landroid/os/Handler;

    invoke-virtual {v6, v9}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 680
    iput-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->y:Z

    .line 683
    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-eqz v6, :cond_9

    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-ne v6, v1, :cond_d

    .line 684
    :cond_9
    iget-object v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v5, v0, v4}, Lcom/android/datetimepicker/time/AmPmCirclesView;->a(FF)I

    move-result v0

    .line 685
    iget-object v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    iput v3, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->l:I

    .line 686
    iget-object v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    invoke-virtual {v4}, Lcom/android/datetimepicker/time/AmPmCirclesView;->invalidate()V

    .line 688
    iget v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    if-ne v0, v4, :cond_b

    .line 689
    iget-object v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->g:Lcom/android/datetimepicker/time/AmPmCirclesView;

    iput v0, v4, Lcom/android/datetimepicker/time/AmPmCirclesView;->k:I

    .line 690
    iget v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    if-ge v4, v7, :cond_c

    move v1, v2

    :cond_a
    :goto_3
    if-eq v1, v0, :cond_b

    .line 691
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->b:Lcom/android/datetimepicker/time/c;

    const/4 v4, 0x2

    iget v5, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    invoke-interface {v1, v4, v5, v2}, Lcom/android/datetimepicker/time/c;->a(IIZ)V

    .line 692
    const/4 v1, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 695
    :cond_b
    iput v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->l:I

    goto/16 :goto_0

    .line 690
    :cond_c
    iget v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    const/16 v5, 0x18

    if-lt v4, v5, :cond_a

    move v1, v3

    goto :goto_3

    .line 700
    :cond_d
    iget v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->n:I

    if-eq v6, v3, :cond_10

    .line 701
    iget-boolean v6, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->m:Z

    invoke-direct {p0, v0, v4, v6, v5}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(FFZ[Ljava/lang/Boolean;)I

    move-result v4

    .line 702
    if-eq v4, v3, :cond_10

    .line 703
    aget-object v0, v5, v2

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->m:Z

    if-nez v0, :cond_11

    move v0, v1

    :goto_4
    invoke-virtual {p0, v4, v5, v0, v2}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(IZZZ)I

    move-result v0

    .line 704
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v4

    if-nez v4, :cond_f

    iget-boolean v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    if-nez v4, :cond_f

    .line 705
    iget v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    if-ge v4, v7, :cond_12

    move v3, v2

    .line 706
    :cond_e
    :goto_5
    if-nez v3, :cond_13

    if-ne v0, v7, :cond_13

    move v0, v2

    .line 712
    :cond_f
    :goto_6
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v3

    invoke-virtual {p0, v3, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->b(II)V

    .line 713
    iget-object v3, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->b:Lcom/android/datetimepicker/time/c;

    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v4

    invoke-interface {v3, v4, v0, v1}, Lcom/android/datetimepicker/time/c;->a(IIZ)V

    .line 716
    :cond_10
    iput-boolean v2, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->m:Z

    goto/16 :goto_1

    :cond_11
    move v0, v2

    .line 703
    goto :goto_4

    .line 705
    :cond_12
    iget v4, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    const/16 v5, 0x18

    if-ge v4, v5, :cond_e

    move v3, v1

    goto :goto_5

    .line 708
    :cond_13
    if-ne v3, v1, :cond_f

    if-eq v0, v7, :cond_f

    .line 709
    add-int/lit8 v0, v0, 0xc

    goto :goto_6

    .line 571
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public performAccessibilityAction(ILandroid/os/Bundle;)Z
    .locals 7
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 779
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v2, v4

    .line 827
    :cond_0
    :goto_0
    return v2

    .line 784
    :cond_1
    const/16 v1, 0x1000

    if-ne p1, v1, :cond_3

    move v3, v4

    .line 789
    :goto_1
    if-eqz v3, :cond_0

    .line 790
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v1

    if-nez v1, :cond_4

    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->c:I

    .line 792
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a()I

    move-result v5

    .line 793
    if-nez v5, :cond_5

    .line 794
    const/16 v1, 0x1e

    .line 795
    rem-int/lit8 v0, v0, 0xc

    move v6, v1

    move v1, v0

    move v0, v6

    .line 800
    :goto_3
    mul-int/2addr v1, v0

    .line 801
    invoke-static {v1, v3}, Lcom/android/datetimepicker/time/RadialPickerLayout;->c(II)I

    move-result v1

    .line 802
    div-int v3, v1, v0

    .line 805
    if-nez v5, :cond_7

    .line 806
    iget-boolean v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->t:Z

    if-eqz v0, :cond_6

    .line 807
    const/16 v1, 0x17

    move v0, v2

    .line 815
    :goto_4
    if-le v3, v1, :cond_8

    .line 822
    :goto_5
    invoke-virtual {p0, v5, v0}, Lcom/android/datetimepicker/time/RadialPickerLayout;->a(II)V

    .line 823
    iget-object v1, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->b:Lcom/android/datetimepicker/time/c;

    invoke-interface {v1, v5, v0, v2}, Lcom/android/datetimepicker/time/c;->a(IIZ)V

    move v2, v4

    .line 824
    goto :goto_0

    .line 786
    :cond_3
    const/16 v1, 0x2000

    if-ne p1, v1, :cond_b

    move v3, v0

    .line 787
    goto :goto_1

    .line 790
    :cond_4
    if-ne v1, v4, :cond_2

    iget v0, p0, Lcom/android/datetimepicker/time/RadialPickerLayout;->d:I

    goto :goto_2

    .line 796
    :cond_5
    if-ne v5, v4, :cond_a

    .line 797
    const/4 v1, 0x6

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_3

    .line 809
    :cond_6
    const/16 v1, 0xc

    move v0, v4

    .line 810
    goto :goto_4

    .line 813
    :cond_7
    const/16 v1, 0x37

    move v0, v2

    goto :goto_4

    .line 818
    :cond_8
    if-ge v3, v0, :cond_9

    move v0, v1

    .line 820
    goto :goto_5

    :cond_9
    move v0, v3

    goto :goto_5

    :cond_a
    move v1, v0

    move v0, v2

    goto :goto_3

    :cond_b
    move v3, v2

    goto :goto_1
.end method
