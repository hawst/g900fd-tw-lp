.class Lcom/android/datetimepicker/time/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# instance fields
.field final synthetic a:Lcom/android/datetimepicker/time/TimePickerDialog;


# direct methods
.method constructor <init>(Lcom/android/datetimepicker/time/TimePickerDialog;)V
    .locals 0

    .prologue
    .line 976
    iput-object p1, p0, Lcom/android/datetimepicker/time/j;->a:Lcom/android/datetimepicker/time/TimePickerDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 979
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_11

    .line 980
    iget-object v3, p0, Lcom/android/datetimepicker/time/j;->a:Lcom/android/datetimepicker/time/TimePickerDialog;

    const/16 v0, 0x6f

    if-eq p2, v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    :cond_0
    invoke-virtual {v3}, Lcom/android/datetimepicker/time/TimePickerDialog;->dismiss()V

    move v0, v1

    .line 982
    :goto_0
    return v0

    .line 980
    :cond_1
    const/16 v0, 0x3d

    if-ne p2, v0, :cond_3

    iget-boolean v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    if-eqz v0, :cond_8

    invoke-virtual {v3}, Lcom/android/datetimepicker/time/TimePickerDialog;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(Z)V

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    const/16 v0, 0x42

    if-ne p2, v0, :cond_6

    iget-boolean v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    if-eqz v0, :cond_5

    invoke-virtual {v3}, Lcom/android/datetimepicker/time/TimePickerDialog;->a()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v3, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->a(Z)V

    :cond_5
    invoke-virtual {v3}, Lcom/android/datetimepicker/time/TimePickerDialog;->dismiss()V

    move v0, v1

    goto :goto_0

    :cond_6
    const/16 v0, 0x43

    if-ne p2, v0, :cond_c

    iget-boolean v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    if-eqz v0, :cond_8

    iget-object v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {v3}, Lcom/android/datetimepicker/time/TimePickerDialog;->b()I

    move-result v0

    invoke-virtual {v3, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v4

    if-ne v0, v4, :cond_9

    iget-object v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->c:Ljava/lang/String;

    :goto_1
    iget-object v4, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    iget-object v5, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->f:Ljava/lang/String;

    new-array v6, v1, [Ljava/lang/Object;

    aput-object v0, v6, v2

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v7, :cond_b

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    if-eqz v4, :cond_7

    if-eqz v5, :cond_7

    invoke-virtual {v4, v5}, Landroid/view/View;->announceForAccessibility(Ljava/lang/CharSequence;)V

    :cond_7
    invoke-virtual {v3, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->b(Z)V

    :cond_8
    move v0, v2

    goto :goto_0

    :cond_9
    invoke-virtual {v3, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v4

    if-ne v0, v4, :cond_a

    iget-object v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->d:Ljava/lang/String;

    goto :goto_1

    :cond_a
    const-string v4, "%d"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/datetimepicker/time/TimePickerDialog;->d(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_b
    move v0, v2

    goto :goto_2

    :cond_c
    const/4 v0, 0x7

    if-eq p2, v0, :cond_d

    const/16 v0, 0x8

    if-eq p2, v0, :cond_d

    const/16 v0, 0x9

    if-eq p2, v0, :cond_d

    const/16 v0, 0xa

    if-eq p2, v0, :cond_d

    const/16 v0, 0xb

    if-eq p2, v0, :cond_d

    const/16 v0, 0xc

    if-eq p2, v0, :cond_d

    const/16 v0, 0xd

    if-eq p2, v0, :cond_d

    const/16 v0, 0xe

    if-eq p2, v0, :cond_d

    const/16 v0, 0xf

    if-eq p2, v0, :cond_d

    if-eq p2, v7, :cond_d

    iget-boolean v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->e:Z

    if-nez v0, :cond_8

    invoke-virtual {v3, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v0

    if-eq p2, v0, :cond_d

    invoke-virtual {v3, v1}, Lcom/android/datetimepicker/time/TimePickerDialog;->e(I)I

    move-result v0

    if-ne p2, v0, :cond_8

    :cond_d
    iget-boolean v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->g:Z

    if-nez v0, :cond_f

    iget-object v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->b:Lcom/android/datetimepicker/time/RadialPickerLayout;

    if-nez v0, :cond_e

    move v0, v1

    goto/16 :goto_0

    :cond_e
    iget-object v0, v3, Lcom/android/datetimepicker/time/TimePickerDialog;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v3, p2}, Lcom/android/datetimepicker/time/TimePickerDialog;->b(I)V

    move v0, v1

    goto/16 :goto_0

    :cond_f
    invoke-virtual {v3, p2}, Lcom/android/datetimepicker/time/TimePickerDialog;->c(I)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {v3, v2}, Lcom/android/datetimepicker/time/TimePickerDialog;->b(Z)V

    :cond_10
    move v0, v1

    goto/16 :goto_0

    :cond_11
    move v0, v2

    .line 982
    goto/16 :goto_0
.end method
