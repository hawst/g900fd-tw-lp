.class Lcom/a/a/m;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "PG"


# static fields
.field private static final D:Landroid/graphics/Matrix;


# instance fields
.field private A:I

.field private B:Z

.field private C:Z

.field a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field b:Landroid/graphics/Picture;

.field c:Landroid/graphics/Canvas;

.field d:Landroid/graphics/Paint;

.field e:Z

.field f:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field h:Landroid/graphics/Paint;

.field i:Z

.field j:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field

.field k:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field l:F

.field m:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field n:Landroid/graphics/RectF;

.field o:Landroid/graphics/RectF;

.field p:Landroid/graphics/RectF;

.field q:Ljava/lang/Integer;

.field r:Ljava/lang/Integer;

.field s:Z

.field t:F

.field u:I

.field v:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Shader;",
            ">;"
        }
    .end annotation
.end field

.field w:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/a/a/h;",
            ">;"
        }
    .end annotation
.end field

.field x:Lcom/a/a/h;

.field y:Lcom/a/a/n;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1492
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcom/a/a/m;->D:Landroid/graphics/Matrix;

    return-void
.end method

.method constructor <init>(Landroid/graphics/Picture;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/high16 v4, 0x7f800000    # Float.POSITIVE_INFINITY

    const/high16 v3, -0x800000    # Float.NEGATIVE_INFINITY

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1179
    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    .line 1136
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->a:Ljava/util/HashMap;

    .line 1142
    iput-boolean v1, p0, Lcom/a/a/m;->e:Z

    .line 1143
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->f:Ljava/util/Stack;

    .line 1144
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->g:Ljava/util/Stack;

    .line 1147
    iput-boolean v1, p0, Lcom/a/a/m;->i:Z

    .line 1148
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->j:Ljava/util/Stack;

    .line 1149
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->k:Ljava/util/Stack;

    .line 1151
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/a/a/m;->l:F

    .line 1152
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->m:Ljava/util/Stack;

    .line 1155
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    .line 1156
    iput-object v2, p0, Lcom/a/a/m;->o:Landroid/graphics/RectF;

    .line 1157
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v4, v4, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    .line 1159
    iput-object v2, p0, Lcom/a/a/m;->q:Ljava/lang/Integer;

    .line 1160
    iput-object v2, p0, Lcom/a/a/m;->r:Ljava/lang/Integer;

    .line 1162
    iput-boolean v1, p0, Lcom/a/a/m;->s:Z

    .line 1164
    sget v0, Lcom/a/a/e;->a:F

    iput v0, p0, Lcom/a/a/m;->t:F

    .line 1166
    iput v1, p0, Lcom/a/a/m;->u:I

    .line 1168
    iput-boolean v1, p0, Lcom/a/a/m;->z:Z

    .line 1169
    iput v1, p0, Lcom/a/a/m;->A:I

    .line 1170
    iput-boolean v1, p0, Lcom/a/a/m;->B:Z

    .line 1172
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    .line 1173
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->w:Ljava/util/HashMap;

    .line 1174
    iput-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    .line 1175
    iput-object v2, p0, Lcom/a/a/m;->y:Lcom/a/a/n;

    .line 1177
    iput-boolean v1, p0, Lcom/a/a/m;->C:Z

    .line 1180
    iput-object p1, p0, Lcom/a/a/m;->b:Landroid/graphics/Picture;

    .line 1181
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    .line 1182
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1183
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1184
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    .line 1185
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1186
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1187
    return-void
.end method

.method private static a(Ljava/lang/String;F)F
    .locals 1

    .prologue
    .line 1457
    .line 1459
    :try_start_0
    invoke-static {p0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p1

    .line 1463
    :goto_0
    return p1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(I)I
    .locals 4

    .prologue
    .line 1403
    const v0, 0xffffff

    and-int/2addr v0, p1

    .line 1404
    iget-object v1, p0, Lcom/a/a/m;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/a/a/m;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/a/a/m;->r:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1405
    const-string v1, "Replacing color: 0x%x->0x%x"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/a/a/m;->r:Ljava/lang/Integer;

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 1406
    iget-object v0, p0, Lcom/a/a/m;->r:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1408
    :cond_0
    return v0
.end method

.method private a(ZLorg/xml/sax/Attributes;)Lcom/a/a/h;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1360
    new-instance v1, Lcom/a/a/h;

    invoke-direct {v1}, Lcom/a/a/h;-><init>()V

    .line 1361
    const-string v0, "id"

    invoke-static {v0, p2}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/a/a/h;->a:Ljava/lang/String;

    .line 1362
    iput-boolean p1, v1, Lcom/a/a/h;->c:Z

    .line 1363
    if-eqz p1, :cond_3

    .line 1364
    const-string v0, "x1"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Lcom/a/a/h;->d:F

    .line 1365
    const-string v0, "x2"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Lcom/a/a/h;->f:F

    .line 1366
    const-string v0, "y1"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Lcom/a/a/h;->e:F

    .line 1367
    const-string v0, "y2"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Lcom/a/a/h;->g:F

    .line 1373
    :goto_0
    const-string v0, "gradientTransform"

    invoke-static {v0, p2}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 1374
    if-eqz v0, :cond_0

    .line 1375
    invoke-static {v0}, Lcom/a/a/e;->b(Ljava/lang/String;)Landroid/graphics/Matrix;

    move-result-object v0

    iput-object v0, v1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    .line 1377
    :cond_0
    const-string v0, "href"

    invoke-static {v0, p2}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 1378
    if-eqz v0, :cond_2

    .line 1379
    const-string v2, "#"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1380
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 1382
    :cond_1
    iput-object v0, v1, Lcom/a/a/h;->b:Ljava/lang/String;

    .line 1384
    :cond_2
    return-object v1

    .line 1369
    :cond_3
    const-string v0, "cx"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Lcom/a/a/h;->h:F

    .line 1370
    const-string v0, "cy"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Lcom/a/a/h;->i:F

    .line 1371
    const-string v0, "r"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v0, p2, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Lcom/a/a/h;->j:F

    goto :goto_0
.end method

.method static synthetic a(Lcom/a/a/m;Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 1133
    invoke-direct {p0, p1, p2, p3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;
    .locals 6

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 2069
    iget v0, p0, Lcom/a/a/m;->t:F

    invoke-static {p1, p2}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 2070
    :goto_0
    if-nez v0, :cond_e

    :goto_1
    return-object p3

    .line 2069
    :cond_0
    const-string v2, "px"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x2

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v2, "pt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x42900000    # 72.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v2, "pc"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x40c00000    # 6.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-string v2, "cm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    const v1, 0x40228f5c    # 2.54f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const-string v2, "mm"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    const/high16 v1, 0x437e0000    # 254.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const-string v2, "in"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    const-string v0, "em"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const-string v0, "ex"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_8
    const-string v0, "%"

    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v1

    const-string v0, "x"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_9

    const-string v0, "width"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v5

    :goto_2
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_a
    const-string v0, "y"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_b

    const-string v0, "height"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_b
    iget-object v0, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v5

    goto :goto_2

    :cond_c
    iget-object v0, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    add-int/2addr v0, v2

    int-to-float v0, v0

    div-float/2addr v0, v4

    goto :goto_2

    :cond_d
    invoke-static {v1}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    goto/16 :goto_0

    :cond_e
    move-object p3, v0

    .line 2070
    goto/16 :goto_1
.end method

.method private a(FF)V
    .locals 1

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 1468
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iput p1, v0, Landroid/graphics/RectF;->left:F

    .line 1470
    :cond_0
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 1471
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iput p1, v0, Landroid/graphics/RectF;->right:F

    .line 1473
    :cond_1
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_2

    .line 1474
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iput p2, v0, Landroid/graphics/RectF;->top:F

    .line 1476
    :cond_2
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_3

    .line 1477
    iget-object v0, p0, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iput p2, v0, Landroid/graphics/RectF;->bottom:F

    .line 1479
    :cond_3
    return-void
.end method

.method private a(Landroid/graphics/Path;)V
    .locals 2

    .prologue
    .line 1487
    iget-object v0, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    .line 1488
    iget-object v0, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, v0, v1}, Lcom/a/a/m;->a(FF)V

    .line 1489
    iget-object v0, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, v0, v1}, Lcom/a/a/m;->a(FF)V

    .line 1490
    return-void
.end method

.method private a(Lcom/a/a/l;Ljava/lang/Integer;ZLandroid/graphics/Paint;)V
    .locals 2

    .prologue
    .line 1388
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(I)I

    move-result v0

    const/high16 v1, -0x1000000

    or-int/2addr v0, v1

    .line 1389
    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1390
    const-string v0, "opacity"

    invoke-virtual {p1, v0}, Lcom/a/a/l;->d(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    .line 1391
    if-nez v0, :cond_0

    .line 1392
    if-eqz p3, :cond_2

    const-string v0, "fill-opacity"

    :goto_0
    invoke-virtual {p1, v0}, Lcom/a/a/l;->d(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    .line 1394
    :cond_0
    if-nez v0, :cond_1

    .line 1395
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 1398
    :cond_1
    const/high16 v1, 0x437f0000    # 255.0f

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/a/a/m;->l:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {p4, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1399
    return-void

    .line 1392
    :cond_2
    const-string v0, "stroke-opacity"

    goto :goto_0
.end method

.method private a(Lorg/xml/sax/Attributes;)V
    .locals 2

    .prologue
    .line 1496
    const-string v0, "transform"

    invoke-static {v0, p1}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 1497
    if-nez v0, :cond_0

    sget-object v0, Lcom/a/a/m;->D:Landroid/graphics/Matrix;

    .line 1498
    :goto_0
    iget v1, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/a/a/m;->u:I

    .line 1499
    iget-object v1, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->save()I

    .line 1505
    iget-object v1, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 1507
    return-void

    .line 1497
    :cond_0
    invoke-static {v0}, Lcom/a/a/e;->b(Ljava/lang/String;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/a/a/l;Ljava/util/HashMap;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/a/a/l;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Shader;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/high16 v5, -0x1000000

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 1220
    const-string v2, "none"

    const-string v3, "display"

    invoke-virtual {p1, v3}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1267
    :cond_0
    :goto_0
    return v0

    .line 1223
    :cond_1
    iget-boolean v2, p0, Lcom/a/a/m;->s:Z

    if-eqz v2, :cond_2

    .line 1224
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1225
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    move v0, v1

    .line 1226
    goto :goto_0

    .line 1228
    :cond_2
    const-string v2, "fill"

    invoke-virtual {p1, v2}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1229
    if-eqz v2, :cond_9

    .line 1230
    const-string v3, "url(#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 1232
    const/4 v0, 0x5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 1233
    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Shader;

    .line 1234
    if-eqz v0, :cond_3

    .line 1235
    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    move v0, v1

    .line 1236
    goto :goto_0

    .line 1238
    :cond_3
    const-string v0, "Didn\'t find shader, using black: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1239
    :goto_1
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1240
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/lang/Integer;ZLandroid/graphics/Paint;)V

    move v0, v1

    .line 1241
    goto :goto_0

    .line 1238
    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 1243
    :cond_5
    const-string v3, "none"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1244
    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1245
    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    move v0, v1

    .line 1246
    goto :goto_0

    .line 1248
    :cond_6
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1249
    const-string v0, "fill"

    invoke-virtual {p1, v0}, Lcom/a/a/l;->c(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 1250
    if-eqz v0, :cond_7

    .line 1251
    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/lang/Integer;ZLandroid/graphics/Paint;)V

    move v0, v1

    .line 1252
    goto/16 :goto_0

    .line 1254
    :cond_7
    const-string v0, "Unrecognized fill color, using black: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1255
    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/lang/Integer;ZLandroid/graphics/Paint;)V

    move v0, v1

    .line 1256
    goto/16 :goto_0

    .line 1254
    :cond_8
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 1260
    :cond_9
    iget-boolean v2, p0, Lcom/a/a/m;->i:Z

    if-eqz v2, :cond_a

    .line 1262
    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 1265
    :cond_a
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 1266
    iget-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    move v0, v1

    .line 1267
    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/a/a/m;Lcom/a/a/l;Ljava/util/HashMap;)Z
    .locals 1

    .prologue
    .line 1133
    invoke-direct {p0, p1, p2}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/util/HashMap;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/a/a/m;Lorg/xml/sax/Attributes;Landroid/graphics/Paint;)Z
    .locals 1

    .prologue
    .line 1133
    invoke-direct {p0, p1, p2}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;Landroid/graphics/Paint;)Z

    move-result v0

    return v0
.end method

.method private a(Lorg/xml/sax/Attributes;Landroid/graphics/Paint;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1274
    const-string v1, "none"

    const-string v2, "display"

    invoke-interface {p1, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1288
    :goto_0
    return v0

    .line 1277
    :cond_0
    const-string v1, "font-size"

    invoke-interface {p1, v1}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1278
    const-string v1, "font-size"

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-direct {p0, v1, p1, v2}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1280
    :cond_1
    const-string v1, "font-family"

    invoke-static {v1, p1}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "font-style"

    invoke-static {v2, p1}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "font-weight"

    invoke-static {v3, p1}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v3

    if-nez v1, :cond_4

    if-nez v2, :cond_4

    if-nez v3, :cond_4

    const/4 v0, 0x0

    .line 1281
    :goto_1
    if-eqz v0, :cond_2

    .line 1282
    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1284
    :cond_2
    invoke-static {p1}, Lcom/a/a/m;->c(Lorg/xml/sax/Attributes;)Landroid/graphics/Paint$Align;

    move-result-object v0

    .line 1285
    if-eqz v0, :cond_3

    .line 1286
    invoke-static {p1}, Lcom/a/a/m;->c(Lorg/xml/sax/Attributes;)Landroid/graphics/Paint$Align;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1288
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 1280
    :cond_4
    const-string v4, "italic"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v0, 0x2

    :cond_5
    const-string v2, "bold"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    or-int/lit8 v0, v0, 0x1

    :cond_6
    invoke-static {v1, v0}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_1
.end method

.method private static b(Lorg/xml/sax/Attributes;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 1846
    const-string v1, ""

    .line 1847
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1848
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0, v0}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1847
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1850
    :cond_0
    return-object v1
.end method

.method private static c(Lorg/xml/sax/Attributes;)Landroid/graphics/Paint$Align;
    .locals 2

    .prologue
    .line 2031
    const-string v0, "text-anchor"

    invoke-static {v0, p0}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 2032
    if-nez v0, :cond_0

    .line 2033
    const/4 v0, 0x0

    .line 2040
    :goto_0
    return-object v0

    .line 2035
    :cond_0
    const-string v1, "middle"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2036
    sget-object v0, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    goto :goto_0

    .line 2037
    :cond_1
    const-string v1, "end"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2038
    sget-object v0, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    goto :goto_0

    .line 2040
    :cond_2
    sget-object v0, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    goto :goto_0
.end method


# virtual methods
.method a(Lcom/a/a/l;)Z
    .locals 11

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 1292
    iget-boolean v0, p0, Lcom/a/a/m;->s:Z

    if-eqz v0, :cond_1

    .line 1354
    :cond_0
    :goto_0
    return v1

    .line 1296
    :cond_1
    const-string v0, "none"

    const-string v2, "display"

    invoke-virtual {p1, v2}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1301
    const-string v0, "stroke-width"

    invoke-virtual {p1, v0}, Lcom/a/a/l;->d(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    .line 1302
    if-eqz v0, :cond_2

    .line 1303
    iget-object v2, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1307
    :cond_2
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    cmpg-float v0, v0, v5

    if-lez v0, :cond_0

    .line 1311
    const-string v0, "stroke-linecap"

    invoke-virtual {p1, v0}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1312
    const-string v2, "round"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1313
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 1320
    :cond_3
    :goto_1
    const-string v0, "stroke-linejoin"

    invoke-virtual {p1, v0}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1321
    const-string v2, "miter"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1322
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 1329
    :cond_4
    :goto_2
    const-string v0, "stroke-dasharray"

    invoke-virtual {p1, v0}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "stroke-dashoffset"

    invoke-virtual {p1, v2}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v0, :cond_5

    const-string v2, "none"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 1331
    :cond_5
    :goto_3
    const-string v0, "stroke"

    invoke-virtual {p1, v0}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1332
    if-eqz v0, :cond_12

    .line 1333
    const-string v2, "none"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 1334
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 1314
    :cond_6
    const-string v2, "square"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1315
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_1

    .line 1316
    :cond_7
    const-string v2, "butt"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1317
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_1

    .line 1323
    :cond_8
    const-string v2, "round"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1324
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_2

    .line 1325
    :cond_9
    const-string v2, "bevel"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1326
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_2

    .line 1329
    :cond_a
    new-instance v8, Ljava/util/StringTokenizer;

    const-string v2, " ,"

    invoke-direct {v8, v0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    and-int/lit8 v2, v0, 0x1

    if-ne v2, v6, :cond_b

    shl-int/lit8 v0, v0, 0x1

    :cond_b
    new-array v9, v0, [F

    const/high16 v0, 0x3f800000    # 1.0f

    move v3, v0

    move v4, v5

    move v0, v1

    :goto_4
    invoke-virtual {v8}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v2

    if-eqz v2, :cond_c

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v8}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10, v3}, Lcom/a/a/m;->a(Ljava/lang/String;F)F

    move-result v3

    aput v3, v9, v0

    add-float v0, v4, v3

    move v4, v0

    move v0, v2

    goto :goto_4

    :cond_c
    move v2, v0

    move v0, v1

    :goto_5
    array-length v3, v9

    if-ge v2, v3, :cond_d

    aget v3, v9, v0

    aput v3, v9, v2

    add-float/2addr v4, v3

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_d
    if-eqz v7, :cond_e

    :try_start_0
    invoke-static {v7}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    rem-float v5, v0, v4

    :cond_e
    :goto_6
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/DashPathEffect;

    invoke-direct {v2, v9, v5}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    goto/16 :goto_3

    .line 1337
    :cond_f
    const-string v2, "stroke"

    invoke-virtual {p1, v2}, Lcom/a/a/l;->c(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    .line 1338
    if-eqz v2, :cond_10

    .line 1339
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v2, v1, v0}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/lang/Integer;ZLandroid/graphics/Paint;)V

    move v1, v6

    .line 1340
    goto/16 :goto_0

    .line 1342
    :cond_10
    const-string v2, "Unrecognized stroke color, using none: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1343
    :goto_7
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 1342
    :cond_11
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_7

    .line 1348
    :cond_12
    iget-boolean v0, p0, Lcom/a/a/m;->e:Z

    if-eqz v0, :cond_13

    .line 1350
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eqz v0, :cond_0

    move v1, v6

    goto/16 :goto_0

    .line 1353
    :cond_13
    iget-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto :goto_6
.end method

.method public characters([CII)V
    .locals 6

    .prologue
    .line 1856
    iget-object v0, p0, Lcom/a/a/m;->y:Lcom/a/a/n;

    if-eqz v0, :cond_0

    .line 1857
    iget-object v1, p0, Lcom/a/a/m;->y:Lcom/a/a/n;

    iget-boolean v0, v1, Lcom/a/a/n;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/a/a/n;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    iput-object v0, v1, Lcom/a/a/n;->e:Ljava/lang/String;

    :goto_0
    iget v0, v1, Lcom/a/a/n;->g:I

    if-lez v0, :cond_0

    iget-object v0, v1, Lcom/a/a/n;->a:Landroid/graphics/Paint;

    if-nez v0, :cond_3

    iget-object v0, v1, Lcom/a/a/n;->b:Landroid/graphics/Paint;

    :goto_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v3, v1, Lcom/a/a/n;->e:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, v1, Lcom/a/a/n;->e:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v3, v4, v5, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget v3, v1, Lcom/a/a/n;->d:F

    iget v0, v1, Lcom/a/a/n;->g:I

    const/4 v4, 0x1

    if-ne v0, v4, :cond_4

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    :goto_2
    add-float/2addr v0, v3

    iput v0, v1, Lcom/a/a/n;->d:F

    .line 1859
    :cond_0
    return-void

    .line 1857
    :cond_1
    iget-object v0, v1, Lcom/a/a/n;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, v1, Lcom/a/a/n;->e:Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    :cond_3
    iget-object v0, v1, Lcom/a/a/n;->a:Landroid/graphics/Paint;

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    goto :goto_2
.end method

.method public endDocument()V
    .locals 0

    .prologue
    .line 1217
    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1868
    iget-boolean v0, p0, Lcom/a/a/m;->C:Z

    if-eqz v0, :cond_1

    .line 1869
    const-string v0, "defs"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1870
    iput-boolean v2, p0, Lcom/a/a/m;->C:Z

    .line 1954
    :cond_0
    :goto_0
    return-void

    .line 1875
    :cond_1
    const-string v0, "svg"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1876
    iget-object v0, p0, Lcom/a/a/m;->b:Landroid/graphics/Picture;

    invoke-virtual {v0}, Landroid/graphics/Picture;->endRecording()V

    goto :goto_0

    .line 1877
    :cond_2
    iget-boolean v0, p0, Lcom/a/a/m;->z:Z

    if-nez v0, :cond_6

    const-string v0, "text"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1878
    iget-object v0, p0, Lcom/a/a/m;->y:Lcom/a/a/n;

    if-eqz v0, :cond_5

    .line 1879
    iget-object v0, p0, Lcom/a/a/m;->y:Lcom/a/a/n;

    iget-object v1, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v3, v0, Lcom/a/a/n;->b:Landroid/graphics/Paint;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/a/a/n;->e:Ljava/lang/String;

    iget v4, v0, Lcom/a/a/n;->c:F

    iget v5, v0, Lcom/a/a/n;->d:F

    iget-object v6, v0, Lcom/a/a/n;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_3
    iget-object v3, v0, Lcom/a/a/n;->a:Landroid/graphics/Paint;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/a/a/n;->e:Ljava/lang/String;

    iget v4, v0, Lcom/a/a/n;->c:F

    iget v5, v0, Lcom/a/a/n;->d:F

    iget-object v0, v0, Lcom/a/a/n;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1880
    :cond_4
    iget-object v0, p0, Lcom/a/a/m;->y:Lcom/a/a/n;

    iput-boolean v2, v0, Lcom/a/a/n;->f:Z

    .line 1882
    :cond_5
    iget-object v0, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    iget v0, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/a/m;->u:I

    goto :goto_0

    .line 1883
    :cond_6
    const-string v0, "linearGradient"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1884
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1885
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->b:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 1886
    iget-object v0, p0, Lcom/a/a/m;->w:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/h;

    .line 1887
    if-eqz v0, :cond_7

    .line 1888
    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    invoke-virtual {v0, v1}, Lcom/a/a/h;->a(Lcom/a/a/h;)Lcom/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    .line 1891
    :cond_7
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [I

    move v1, v2

    .line 1892
    :goto_1
    array-length v0, v5

    if-ge v1, v0, :cond_8

    .line 1893
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v1

    .line 1892
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1895
    :cond_8
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v6, v0, [F

    .line 1896
    :goto_2
    array-length v0, v6

    if-ge v2, v0, :cond_9

    .line 1897
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v6, v2

    .line 1896
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1899
    :cond_9
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget v1, v1, Lcom/a/a/h;->d:F

    iget-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget v2, v2, Lcom/a/a/h;->e:F

    iget-object v3, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget v3, v3, Lcom/a/a/h;->f:F

    iget-object v4, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget v4, v4, Lcom/a/a/h;->g:F

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1903
    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    if-eqz v1, :cond_a

    .line 1904
    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/LinearGradient;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 1906
    :cond_a
    iget-object v1, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v2, v2, Lcom/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1907
    iget-object v0, p0, Lcom/a/a/m;->w:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1909
    :cond_b
    const-string v0, "radialGradient"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1910
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1911
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->b:Ljava/lang/String;

    if-eqz v0, :cond_c

    .line 1912
    iget-object v0, p0, Lcom/a/a/m;->w:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/h;

    .line 1913
    if-eqz v0, :cond_c

    .line 1914
    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    invoke-virtual {v0, v1}, Lcom/a/a/h;->a(Lcom/a/a/h;)Lcom/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    .line 1918
    :cond_c
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v2

    .line 1919
    :goto_3
    array-length v0, v4

    if-ge v1, v0, :cond_d

    .line 1920
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v1

    .line 1919
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1922
    :cond_d
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [F

    .line 1923
    :goto_4
    array-length v0, v5

    if-ge v2, v0, :cond_e

    .line 1924
    iget-object v0, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v0, v0, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v2

    .line 1923
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1926
    :cond_e
    new-instance v0, Landroid/graphics/RadialGradient;

    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget v1, v1, Lcom/a/a/h;->h:F

    iget-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget v2, v2, Lcom/a/a/h;->i:F

    iget-object v3, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget v3, v3, Lcom/a/a/h;->j:F

    sget-object v6, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v6}, Landroid/graphics/RadialGradient;-><init>(FFF[I[FLandroid/graphics/Shader$TileMode;)V

    .line 1927
    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    if-eqz v1, :cond_f

    .line 1928
    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/RadialGradient;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 1930
    :cond_f
    iget-object v1, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v2, v2, Lcom/a/a/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931
    iget-object v0, p0, Lcom/a/a/m;->w:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v1, v1, Lcom/a/a/h;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1933
    :cond_10
    const-string v0, "g"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1934
    iget-boolean v0, p0, Lcom/a/a/m;->B:Z

    if-eqz v0, :cond_11

    .line 1935
    iput-boolean v2, p0, Lcom/a/a/m;->B:Z

    .line 1938
    :cond_11
    iget-boolean v0, p0, Lcom/a/a/m;->z:Z

    if-eqz v0, :cond_12

    .line 1939
    iget v0, p0, Lcom/a/a/m;->A:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/a/m;->A:I

    .line 1941
    iget v0, p0, Lcom/a/a/m;->A:I

    if-nez v0, :cond_12

    .line 1942
    iput-boolean v2, p0, Lcom/a/a/m;->z:Z

    .line 1946
    :cond_12
    iget-object v0, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1947
    iget-object v0, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v0}, Landroid/graphics/Canvas;->restore()V

    iget v0, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/a/a/m;->u:I

    .line 1948
    iget-object v0, p0, Lcom/a/a/m;->j:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    .line 1949
    iget-object v0, p0, Lcom/a/a/m;->k:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/a/a/m;->i:Z

    .line 1950
    iget-object v0, p0, Lcom/a/a/m;->f:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    .line 1951
    iget-object v0, p0, Lcom/a/a/m;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/a/a/m;->e:Z

    .line 1952
    iget-object v0, p0, Lcom/a/a/m;->m:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/a/a/m;->l:F

    goto/16 :goto_0
.end method

.method public startDocument()V
    .locals 0

    .prologue
    .line 1205
    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 15

    .prologue
    .line 1534
    iget-boolean v2, p0, Lcom/a/a/m;->e:Z

    if-nez v2, :cond_0

    .line 1535
    iget-object v2, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1537
    :cond_0
    iget-boolean v2, p0, Lcom/a/a/m;->i:Z

    if-nez v2, :cond_1

    .line 1538
    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1541
    :cond_1
    iget-boolean v2, p0, Lcom/a/a/m;->B:Z

    if-eqz v2, :cond_5

    .line 1542
    const-string v2, "rect"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1543
    const-string v2, "x"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v2

    .line 1544
    if-nez v2, :cond_2

    .line 1545
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 1547
    :cond_2
    const-string v3, "y"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v3

    .line 1548
    if-nez v3, :cond_3

    .line 1549
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 1551
    :cond_3
    const-string v4, "width"

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v4, v0, v5}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v4

    .line 1552
    const-string v5, "height"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v5, v0, v6}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v5

    .line 1553
    new-instance v6, Landroid/graphics/RectF;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v2, v4

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v3, v4

    invoke-direct {v6, v7, v8, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v6, p0, Lcom/a/a/m;->o:Landroid/graphics/RectF;

    .line 1835
    :cond_4
    :goto_0
    return-void

    .line 1558
    :cond_5
    iget-boolean v2, p0, Lcom/a/a/m;->C:Z

    if-nez v2, :cond_4

    .line 1562
    const-string v2, "svg"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1563
    const-string v2, "width"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 1564
    const-string v3, "height"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 1565
    iget-object v4, p0, Lcom/a/a/m;->b:Landroid/graphics/Picture;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Picture;->beginRecording(II)Landroid/graphics/Canvas;

    move-result-object v2

    iput-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    goto :goto_0

    .line 1566
    :cond_6
    const-string v2, "defs"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1567
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/a/a/m;->C:Z

    goto :goto_0

    .line 1568
    :cond_7
    const-string v2, "linearGradient"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 1569
    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0}, Lcom/a/a/m;->a(ZLorg/xml/sax/Attributes;)Lcom/a/a/h;

    move-result-object v2

    iput-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    goto :goto_0

    .line 1570
    :cond_8
    const-string v2, "radialGradient"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1571
    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0}, Lcom/a/a/m;->a(ZLorg/xml/sax/Attributes;)Lcom/a/a/h;

    move-result-object v2

    iput-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    goto :goto_0

    .line 1572
    :cond_9
    const-string v2, "stop"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1573
    iget-object v2, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    if-eqz v2, :cond_4

    .line 1574
    const-string v2, "offset"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 1575
    const-string v2, "style"

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v2

    .line 1576
    new-instance v5, Lcom/a/a/o;

    invoke-direct {v5, v2}, Lcom/a/a/o;-><init>(Ljava/lang/String;)V

    .line 1577
    const-string v2, "stop-color"

    iget-object v3, v5, Lcom/a/a/o;->a:Ljava/util/HashMap;

    invoke-virtual {v3, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1578
    const/high16 v3, -0x1000000

    .line 1579
    if-eqz v2, :cond_36

    .line 1580
    const-string v3, "#"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1581
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    .line 1586
    :goto_1
    invoke-direct {p0, v2}, Lcom/a/a/m;->a(I)I

    move-result v3

    .line 1587
    const-string v2, "stop-opacity"

    iget-object v5, v5, Lcom/a/a/o;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1588
    if-eqz v2, :cond_b

    .line 1589
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v2

    .line 1590
    const/high16 v5, 0x437f0000    # 255.0f

    mul-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1591
    shl-int/lit8 v2, v2, 0x18

    or-int/2addr v2, v3

    .line 1595
    :goto_2
    iget-object v3, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v3, v3, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1596
    iget-object v3, p0, Lcom/a/a/m;->x:Lcom/a/a/h;

    iget-object v3, v3, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1583
    :cond_a
    const/16 v3, 0x10

    invoke-static {v2, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_1

    .line 1593
    :cond_b
    const/high16 v2, -0x1000000

    or-int/2addr v2, v3

    goto :goto_2

    .line 1598
    :cond_c
    const-string v2, "use"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 1599
    const-string v2, "xlink:href"

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1600
    const-string v2, "transform"

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1601
    const-string v4, "x"

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1602
    const-string v5, "y"

    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Lorg/xml/sax/Attributes;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1604
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1605
    const-string v7, "<g"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1606
    const-string v7, " xmlns=\'http://www.w3.org/2000/svg\' xmlns:xlink=\'http://www.w3.org/1999/xlink\' version=\'1.1\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1607
    if-nez v2, :cond_d

    if-nez v4, :cond_d

    if-eqz v5, :cond_11

    .line 1608
    :cond_d
    const-string v7, " transform=\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1609
    if-eqz v2, :cond_e

    .line 1610
    invoke-static {v2}, Lcom/a/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1612
    :cond_e
    if-nez v4, :cond_f

    if-eqz v5, :cond_10

    .line 1613
    :cond_f
    const-string v2, "translate("

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1614
    if-eqz v4, :cond_13

    invoke-static {v4}, Lcom/a/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1615
    const-string v2, ","

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1616
    if-eqz v5, :cond_14

    invoke-static {v5}, Lcom/a/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1617
    const-string v2, ")"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1619
    :cond_10
    const-string v2, "\'"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1622
    :cond_11
    const/4 v2, 0x0

    :goto_5
    invoke-interface/range {p4 .. p4}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v4

    if-ge v2, v4, :cond_15

    .line 1623
    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lorg/xml/sax/Attributes;->getQName(I)Ljava/lang/String;

    move-result-object v4

    .line 1624
    const-string v5, "x"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "y"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "width"

    .line 1625
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "height"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "xlink:href"

    .line 1626
    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    const-string v5, "transform"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_12

    .line 1628
    const-string v5, " "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1629
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1630
    const-string v4, "=\'"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1631
    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/a/a/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1632
    const-string v4, "\'"

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1622
    :cond_12
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1614
    :cond_13
    const-string v2, "0"

    goto :goto_3

    .line 1616
    :cond_14
    const-string v2, "0"

    goto :goto_4

    .line 1636
    :cond_15
    const-string v2, ">"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1638
    iget-object v2, p0, Lcom/a/a/m;->a:Ljava/util/HashMap;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1640
    const-string v2, "</g>"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1644
    new-instance v2, Lorg/xml/sax/InputSource;

    new-instance v3, Ljava/io/StringReader;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/Reader;)V

    .line 1646
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v3

    .line 1647
    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v3

    .line 1648
    invoke-virtual {v3}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v3

    .line 1649
    invoke-interface {v3, p0}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 1650
    invoke-interface {v3, v2}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1651
    :catch_0
    move-exception v2

    .line 1653
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 1655
    :cond_16
    const-string v2, "g"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 1657
    const-string v2, "bounds"

    const-string v3, "id"

    move-object/from16 v0, p4

    invoke-static {v3, v0}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 1658
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/a/a/m;->B:Z

    .line 1660
    :cond_17
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-eqz v2, :cond_18

    .line 1661
    iget v2, p0, Lcom/a/a/m;->A:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/a/a/m;->A:I

    .line 1665
    :cond_18
    const-string v2, "none"

    const-string v3, "display"

    move-object/from16 v0, p4

    invoke-static {v3, v0}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 1666
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_19

    .line 1667
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/a/a/m;->z:Z

    .line 1668
    const/4 v2, 0x1

    iput v2, p0, Lcom/a/a/m;->A:I

    .line 1672
    :cond_19
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1673
    new-instance v3, Lcom/a/a/l;

    move-object/from16 v0, p4

    invoke-direct {v3, v0}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1675
    iget-object v2, p0, Lcom/a/a/m;->j:Ljava/util/Stack;

    new-instance v4, Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v2, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676
    iget-object v2, p0, Lcom/a/a/m;->f:Ljava/util/Stack;

    new-instance v4, Landroid/graphics/Paint;

    iget-object v5, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-direct {v4, v5}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v2, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1677
    iget-object v2, p0, Lcom/a/a/m;->k:Ljava/util/Stack;

    iget-boolean v4, p0, Lcom/a/a/m;->i:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1678
    iget-object v2, p0, Lcom/a/a/m;->g:Ljava/util/Stack;

    iget-boolean v4, p0, Lcom/a/a/m;->e:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1679
    iget-object v2, p0, Lcom/a/a/m;->m:Ljava/util/Stack;

    iget v4, p0, Lcom/a/a/m;->l:F

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1681
    const-string v2, "opacity"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v2

    .line 1682
    if-eqz v2, :cond_1a

    .line 1683
    iget v4, p0, Lcom/a/a/m;->l:F

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    mul-float/2addr v2, v4

    iput v2, p0, Lcom/a/a/m;->l:F

    .line 1686
    :cond_1a
    iget-object v2, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    move-object/from16 v0, p4

    invoke-direct {p0, v0, v2}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;Landroid/graphics/Paint;)Z

    .line 1687
    iget-object v2, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    move-object/from16 v0, p4

    invoke-direct {p0, v0, v2}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;Landroid/graphics/Paint;)Z

    .line 1688
    iget-object v2, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-direct {p0, v3, v2}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/util/HashMap;)Z

    .line 1689
    invoke-virtual {p0, v3}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    .line 1691
    iget-boolean v4, p0, Lcom/a/a/m;->i:Z

    const-string v2, "fill"

    invoke-virtual {v3, v2}, Lcom/a/a/l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1b

    const/4 v2, 0x1

    :goto_6
    or-int/2addr v2, v4

    iput-boolean v2, p0, Lcom/a/a/m;->i:Z

    .line 1692
    iget-boolean v4, p0, Lcom/a/a/m;->e:Z

    const-string v2, "stroke"

    invoke-virtual {v3, v2}, Lcom/a/a/l;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1c

    const/4 v2, 0x1

    :goto_7
    or-int/2addr v2, v4

    iput-boolean v2, p0, Lcom/a/a/m;->e:Z

    goto/16 :goto_0

    .line 1691
    :cond_1b
    const/4 v2, 0x0

    goto :goto_6

    .line 1692
    :cond_1c
    const/4 v2, 0x0

    goto :goto_7

    .line 1693
    :cond_1d
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_22

    const-string v2, "rect"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 1694
    const-string v2, "x"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v8

    .line 1695
    const-string v2, "y"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v9

    .line 1696
    const-string v2, "width"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v10

    .line 1697
    const-string v2, "height"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v11

    .line 1698
    const-string v2, "rx"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v12

    .line 1699
    const-string v2, "ry"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v13

    .line 1700
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1701
    new-instance v14, Lcom/a/a/l;

    move-object/from16 v0, p4

    invoke-direct {v14, v0}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1702
    iget-object v2, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-direct {p0, v14, v2}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/util/HashMap;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 1703
    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-direct {p0, v2, v3}, Lcom/a/a/m;->a(FF)V

    add-float/2addr v2, v4

    add-float/2addr v3, v5

    invoke-direct {p0, v2, v3}, Lcom/a/a/m;->a(FF)V

    .line 1704
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_20

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_20

    .line 1705
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1711
    :cond_1e
    :goto_8
    invoke-virtual {p0, v14}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 1712
    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_21

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_21

    .line 1713
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v6, v7

    iget-object v7, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1719
    :cond_1f
    :goto_9
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    iget v2, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/a/m;->u:I

    goto/16 :goto_0

    .line 1707
    :cond_20
    iget-object v2, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1708
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget-object v6, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_8

    .line 1715
    :cond_21
    iget-object v2, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v6

    add-float/2addr v5, v6

    invoke-virtual {v9}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1716
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    invoke-virtual {v12}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v13}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget-object v6, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_9

    .line 1720
    :cond_22
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_24

    const-string v2, "image"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 1722
    const-string v2, "href"

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v2

    .line 1723
    const-string v3, "data"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "base64"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_4

    .line 1724
    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 1725
    const-string v3, "x"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p4

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v3

    .line 1726
    const-string v4, "y"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    move-object/from16 v0, p4

    invoke-direct {p0, v4, v0, v5}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v4

    .line 1727
    const-string v5, "width"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    move-object/from16 v0, p4

    invoke-direct {p0, v5, v0, v6}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v5

    .line 1728
    const-string v6, "height"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    move-object/from16 v0, p4

    invoke-direct {p0, v6, v0, v7}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v6

    .line 1729
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1730
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v10

    invoke-direct {p0, v7, v8}, Lcom/a/a/m;->a(FF)V

    add-float/2addr v7, v9

    add-float/2addr v8, v10

    invoke-direct {p0, v7, v8}, Lcom/a/a/m;->a(FF)V

    .line 1731
    iget-object v7, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    const/4 v8, 0x0

    invoke-static {v2, v8}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    const/4 v8, 0x0

    array-length v9, v2

    invoke-static {v2, v8, v9}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_23

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->prepareToDraw()V

    new-instance v8, Landroid/graphics/Paint;

    const/4 v9, 0x3

    invoke-direct {v8, v9}, Landroid/graphics/Paint;-><init>(I)V

    new-instance v9, Landroid/graphics/RectF;

    add-float/2addr v5, v3

    add-float/2addr v6, v4

    invoke-direct {v9, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v3, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {v7, v9, v3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    const/4 v3, 0x0

    invoke-virtual {v7, v2, v3, v9, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1732
    :cond_23
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    iget v2, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/a/m;->u:I

    goto/16 :goto_0

    .line 1734
    :cond_24
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_25

    const-string v2, "line"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 1735
    const-string v2, "x1"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v3

    .line 1736
    const-string v2, "x2"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v5

    .line 1737
    const-string v2, "y1"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v4

    .line 1738
    const-string v2, "y2"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v6}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v6

    .line 1739
    new-instance v2, Lcom/a/a/l;

    move-object/from16 v0, p4

    invoke-direct {v2, v0}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1740
    invoke-virtual {p0, v2}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1741
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1742
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-direct {p0, v2, v7}, Lcom/a/a/m;->a(FF)V

    .line 1743
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-direct {p0, v2, v7}, Lcom/a/a/m;->a(FF)V

    .line 1744
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    iget-object v7, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1745
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    iget v2, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/a/m;->u:I

    goto/16 :goto_0

    .line 1747
    :cond_25
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_28

    const-string v2, "circle"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 1748
    const-string v2, "cx"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v2

    .line 1749
    const-string v3, "cy"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v3

    .line 1750
    const-string v4, "r"

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v4, v0, v5}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v4

    .line 1751
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    .line 1752
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1753
    new-instance v5, Lcom/a/a/l;

    move-object/from16 v0, p4

    invoke-direct {v5, v0}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1754
    iget-object v6, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-direct {p0, v5, v6}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/util/HashMap;)Z

    move-result v6

    if-eqz v6, :cond_26

    .line 1755
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v7

    sub-float/2addr v6, v7

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v8

    sub-float/2addr v7, v8

    invoke-direct {p0, v6, v7}, Lcom/a/a/m;->a(FF)V

    .line 1756
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v7

    add-float/2addr v6, v7

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v8

    add-float/2addr v7, v8

    invoke-direct {p0, v6, v7}, Lcom/a/a/m;->a(FF)V

    .line 1757
    iget-object v6, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v9

    iget-object v10, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1759
    :cond_26
    invoke-virtual {p0, v5}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    move-result v5

    if-eqz v5, :cond_27

    .line 1760
    iget-object v5, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    iget-object v6, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v5, v2, v3, v4, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1762
    :cond_27
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    iget v2, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/a/m;->u:I

    goto/16 :goto_0

    .line 1764
    :cond_28
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_2b

    const-string v2, "ellipse"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 1765
    const-string v2, "cx"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v2, v0, v3}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v2

    .line 1766
    const-string v3, "cy"

    const/4 v4, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v3, v0, v4}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v3

    .line 1767
    const-string v4, "rx"

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v4, v0, v5}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v4

    .line 1768
    const-string v5, "ry"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-direct {p0, v5, v0, v6}, Lcom/a/a/m;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v5

    .line 1769
    if-eqz v2, :cond_4

    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    if-eqz v5, :cond_4

    .line 1770
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1771
    new-instance v6, Lcom/a/a/l;

    move-object/from16 v0, p4

    invoke-direct {v6, v0}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1772
    iget-object v7, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v10

    sub-float/2addr v9, v10

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v10

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v11

    add-float/2addr v10, v11

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v11

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v12

    add-float/2addr v11, v12

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;->set(FFFF)V

    .line 1773
    iget-object v7, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-direct {p0, v6, v7}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/util/HashMap;)Z

    move-result v7

    if-eqz v7, :cond_29

    .line 1774
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v8

    sub-float/2addr v7, v8

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-direct {p0, v7, v8}, Lcom/a/a/m;->a(FF)V

    .line 1775
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v2, v4

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v3, v4

    invoke-direct {p0, v2, v3}, Lcom/a/a/m;->a(FF)V

    .line 1776
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1778
    :cond_29
    invoke-virtual {p0, v6}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 1779
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/a/a/m;->n:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 1781
    :cond_2a
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    iget v2, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/a/m;->u:I

    goto/16 :goto_0

    .line 1783
    :cond_2b
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_31

    const-string v2, "polygon"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2c

    const-string v2, "polyline"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 1784
    :cond_2c
    const-string v2, "points"

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/a/a/e;->b(Ljava/lang/String;Lorg/xml/sax/Attributes;)Lcom/a/a/k;

    move-result-object v2

    .line 1785
    if-eqz v2, :cond_4

    .line 1786
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 1787
    iget-object v5, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    .line 1788
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_4

    .line 1789
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1790
    new-instance v6, Lcom/a/a/l;

    move-object/from16 v0, p4

    invoke-direct {v6, v0}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1791
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v4, v3, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1792
    const/4 v2, 0x2

    move v3, v2

    :goto_a
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_2d

    .line 1793
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v7

    .line 1794
    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 1795
    invoke-virtual {v4, v7, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1792
    add-int/lit8 v2, v3, 0x2

    move v3, v2

    goto :goto_a

    .line 1798
    :cond_2d
    const-string v2, "polygon"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 1799
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 1801
    :cond_2e
    iget-object v2, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-direct {p0, v6, v2}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/util/HashMap;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 1802
    invoke-direct {p0, v4}, Lcom/a/a/m;->a(Landroid/graphics/Path;)V

    .line 1805
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1807
    :cond_2f
    invoke-virtual {p0, v6}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 1809
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v4, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1811
    :cond_30
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    iget v2, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/a/m;->u:I

    goto/16 :goto_0

    .line 1814
    :cond_31
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_34

    const-string v2, "path"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 1815
    const-string v2, "d"

    move-object/from16 v0, p4

    invoke-static {v2, v0}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/a/a/e;->c(Ljava/lang/String;)Landroid/graphics/Path;

    move-result-object v2

    .line 1816
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1817
    new-instance v3, Lcom/a/a/l;

    move-object/from16 v0, p4

    invoke-direct {v3, v0}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1818
    iget-object v4, p0, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-direct {p0, v3, v4}, Lcom/a/a/m;->a(Lcom/a/a/l;Ljava/util/HashMap;)Z

    move-result v4

    if-eqz v4, :cond_32

    .line 1820
    invoke-direct {p0, v2}, Lcom/a/a/m;->a(Landroid/graphics/Path;)V

    .line 1822
    iget-object v4, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v5, p0, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-virtual {v4, v2, v5}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1824
    :cond_32
    invoke-virtual {p0, v3}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 1826
    iget-object v3, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    iget-object v4, p0, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-virtual {v3, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1828
    :cond_33
    iget-object v2, p0, Lcom/a/a/m;->c:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->restore()V

    iget v2, p0, Lcom/a/a/m;->u:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/a/a/m;->u:I

    goto/16 :goto_0

    .line 1829
    :cond_34
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_35

    const-string v2, "text"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 1830
    move-object/from16 v0, p4

    invoke-direct {p0, v0}, Lcom/a/a/m;->a(Lorg/xml/sax/Attributes;)V

    .line 1831
    new-instance v2, Lcom/a/a/n;

    move-object/from16 v0, p4

    invoke-direct {v2, p0, v0}, Lcom/a/a/n;-><init>(Lcom/a/a/m;Lorg/xml/sax/Attributes;)V

    iput-object v2, p0, Lcom/a/a/m;->y:Lcom/a/a/n;

    goto/16 :goto_0

    .line 1832
    :cond_35
    iget-boolean v2, p0, Lcom/a/a/m;->z:Z

    if-nez v2, :cond_4

    .line 1833
    const-string v2, "Unrecognized tag: %s (%s)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    invoke-static/range {p4 .. p4}, Lcom/a/a/m;->b(Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_36
    move v2, v3

    goto/16 :goto_1
.end method
