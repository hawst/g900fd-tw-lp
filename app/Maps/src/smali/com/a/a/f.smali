.class public Lcom/a/a/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/io/InputStream;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Z

.field public e:Z

.field private f:Z

.field private g:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 232
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    iput-object v0, p0, Lcom/a/a/f;->a:Ljava/io/InputStream;

    .line 234
    iput-object v0, p0, Lcom/a/a/f;->b:Ljava/lang/Integer;

    .line 235
    iput-object v0, p0, Lcom/a/a/f;->c:Ljava/lang/Integer;

    .line 236
    iput-boolean v1, p0, Lcom/a/a/f;->f:Z

    .line 237
    iput-boolean v1, p0, Lcom/a/a/f;->d:Z

    .line 238
    sget v0, Lcom/a/a/e;->a:F

    iput v0, p0, Lcom/a/a/f;->g:F

    .line 239
    iput-boolean v1, p0, Lcom/a/a/f;->e:Z

    .line 240
    return-void
.end method


# virtual methods
.method public final a()Lcom/a/a/b;
    .locals 6

    .prologue
    .line 297
    iget-object v0, p0, Lcom/a/a/f;->a:Ljava/io/InputStream;

    if-nez v0, :cond_0

    .line 298
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No input SVG provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/a/a/f;->a:Ljava/io/InputStream;

    iget-object v1, p0, Lcom/a/a/f;->b:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/a/a/f;->c:Ljava/lang/Integer;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/a/a/f;->d:Z

    iget v5, p0, Lcom/a/a/f;->g:F

    invoke-static/range {v0 .. v5}, Lcom/a/a/e;->a(Ljava/io/InputStream;Ljava/lang/Integer;Ljava/lang/Integer;ZZF)Lcom/a/a/b;

    move-result-object v1

    .line 301
    iget-boolean v0, p0, Lcom/a/a/f;->e:Z

    if-eqz v0, :cond_1

    .line 303
    :try_start_0
    iget-object v0, p0, Lcom/a/a/f;->a:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :cond_1
    :goto_0
    return-object v1

    .line 304
    :catch_0
    move-exception v0

    .line 305
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
