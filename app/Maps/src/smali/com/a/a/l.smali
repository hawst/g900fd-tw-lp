.class Lcom/a/a/l;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/a/a/o;

.field b:Lorg/xml/sax/Attributes;


# direct methods
.method constructor <init>(Lorg/xml/sax/Attributes;)V
    .locals 2

    .prologue
    .line 996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 993
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/l;->a:Lcom/a/a/o;

    .line 997
    iput-object p1, p0, Lcom/a/a/l;->b:Lorg/xml/sax/Attributes;

    .line 998
    const-string v0, "style"

    invoke-static {v0, p1}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 999
    if-eqz v0, :cond_0

    .line 1000
    new-instance v1, Lcom/a/a/o;

    invoke-direct {v1, v0}, Lcom/a/a/o;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/a/a/l;->a:Lcom/a/a/o;

    .line 1002
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1005
    const/4 v0, 0x0

    .line 1006
    iget-object v1, p0, Lcom/a/a/l;->a:Lcom/a/a/o;

    if-eqz v1, :cond_0

    .line 1007
    iget-object v0, p0, Lcom/a/a/l;->a:Lcom/a/a/o;

    iget-object v0, v0, Lcom/a/a/o;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1009
    :cond_0
    if-nez v0, :cond_1

    .line 1010
    iget-object v0, p0, Lcom/a/a/l;->b:Lorg/xml/sax/Attributes;

    invoke-static {p1, v0}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 1012
    :cond_1
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1016
    invoke-virtual {p0, p1}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x4

    .line 1020
    invoke-virtual {p0, p1}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1021
    if-nez v2, :cond_0

    .line 1031
    :goto_0
    return-object v0

    .line 1023
    :cond_0
    const-string v1, "#"

    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-eq v1, v4, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v3, 0x7

    if-ne v1, v3, :cond_3

    .line 1025
    :cond_1
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {v2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x10

    invoke-static {v1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    .line 1026
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-ne v2, v4, :cond_2

    and-int/lit16 v2, v1, 0xf00

    shl-int/lit8 v2, v2, 0x8

    and-int/lit16 v3, v1, 0xf00

    shl-int/lit8 v3, v3, 0xc

    or-int/2addr v2, v3

    and-int/lit16 v3, v1, 0xf0

    shl-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    and-int/lit16 v3, v1, 0xf0

    shl-int/lit8 v3, v3, 0x8

    or-int/2addr v2, v3

    and-int/lit8 v3, v1, 0xf

    shl-int/lit8 v3, v3, 0x4

    or-int/2addr v2, v3

    and-int/lit8 v1, v1, 0xf

    or-int/2addr v1, v2

    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1031
    :cond_3
    invoke-static {v2}, Lcom/a/a/c;->a(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 1028
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/Float;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1053
    invoke-virtual {p0, p1}, Lcom/a/a/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1054
    if-nez v1, :cond_0

    .line 1060
    :goto_0
    return-object v0

    .line 1058
    :cond_0
    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1060
    :catch_0
    move-exception v1

    goto :goto_0
.end method
