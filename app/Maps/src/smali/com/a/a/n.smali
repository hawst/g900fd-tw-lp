.class Lcom/a/a/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/graphics/Paint;

.field b:Landroid/graphics/Paint;

.field c:F

.field d:F

.field e:Ljava/lang/String;

.field f:Z

.field g:I


# direct methods
.method public constructor <init>(Lcom/a/a/m;Lorg/xml/sax/Attributes;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1961
    iput-object v2, p0, Lcom/a/a/n;->a:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/a/a/n;->b:Landroid/graphics/Paint;

    .line 1965
    const/4 v0, 0x0

    iput v0, p0, Lcom/a/a/n;->g:I

    .line 1969
    const-string v0, "x"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, p2, v1}, Lcom/a/a/m;->a(Lcom/a/a/m;Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/a/a/n;->c:F

    .line 1970
    const-string v0, "y"

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p1, v0, p2, v1}, Lcom/a/a/m;->a(Lcom/a/a/m;Ljava/lang/String;Lorg/xml/sax/Attributes;Ljava/lang/Float;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/a/a/n;->d:F

    .line 1971
    iput-object v2, p0, Lcom/a/a/n;->e:Ljava/lang/String;

    .line 1972
    iput-boolean v4, p0, Lcom/a/a/n;->f:Z

    .line 1974
    new-instance v0, Lcom/a/a/l;

    invoke-direct {v0, p2}, Lcom/a/a/l;-><init>(Lorg/xml/sax/Attributes;)V

    .line 1975
    iget-object v1, p1, Lcom/a/a/m;->v:Ljava/util/HashMap;

    invoke-static {p1, v0, v1}, Lcom/a/a/m;->a(Lcom/a/a/m;Lcom/a/a/l;Ljava/util/HashMap;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1976
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p1, Lcom/a/a/m;->h:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lcom/a/a/n;->b:Landroid/graphics/Paint;

    .line 1977
    iget-object v1, p0, Lcom/a/a/n;->b:Landroid/graphics/Paint;

    invoke-static {p1, p2, v1}, Lcom/a/a/m;->a(Lcom/a/a/m;Lorg/xml/sax/Attributes;Landroid/graphics/Paint;)Z

    .line 1979
    :cond_0
    invoke-virtual {p1, v0}, Lcom/a/a/m;->a(Lcom/a/a/l;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1980
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p1, Lcom/a/a/m;->d:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lcom/a/a/n;->a:Landroid/graphics/Paint;

    .line 1981
    iget-object v0, p0, Lcom/a/a/n;->a:Landroid/graphics/Paint;

    invoke-static {p1, p2, v0}, Lcom/a/a/m;->a(Lcom/a/a/m;Lorg/xml/sax/Attributes;Landroid/graphics/Paint;)Z

    .line 1984
    :cond_1
    const-string v0, "alignment-baseline"

    invoke-static {v0, p2}, Lcom/a/a/e;->a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;

    move-result-object v0

    .line 1985
    const-string v1, "middle"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1986
    iput v4, p0, Lcom/a/a/n;->g:I

    .line 1990
    :cond_2
    :goto_0
    return-void

    .line 1987
    :cond_3
    const-string v1, "top"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1988
    const/4 v0, 0x2

    iput v0, p0, Lcom/a/a/n;->g:I

    goto :goto_0
.end method
