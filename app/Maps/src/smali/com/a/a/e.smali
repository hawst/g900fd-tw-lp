.class public Lcom/a/a/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 93
    const/high16 v0, 0x42900000    # 72.0f

    sput v0, Lcom/a/a/e;->a:F

    return-void
.end method

.method static a(Ljava/io/InputStream;Ljava/lang/Integer;Ljava/lang/Integer;ZZF)Lcom/a/a/b;
    .locals 10

    .prologue
    .line 315
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 318
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    .line 319
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v2

    .line 320
    invoke-virtual {v2}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v2

    .line 321
    new-instance v3, Landroid/graphics/Picture;

    invoke-direct {v3}, Landroid/graphics/Picture;-><init>()V

    .line 322
    new-instance v4, Lcom/a/a/m;

    invoke-direct {v4, v3}, Lcom/a/a/m;-><init>(Landroid/graphics/Picture;)V

    .line 323
    iput-object p1, v4, Lcom/a/a/m;->q:Ljava/lang/Integer;

    iput-object p2, v4, Lcom/a/a/m;->r:Ljava/lang/Integer;

    .line 324
    iput-boolean p3, v4, Lcom/a/a/m;->s:Z

    .line 325
    iput p5, v4, Lcom/a/a/m;->t:F

    .line 326
    if-eqz p4, :cond_1

    .line 327
    invoke-interface {v2, v4}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 328
    new-instance v5, Lorg/xml/sax/InputSource;

    invoke-direct {v5, p0}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v2, v5}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 342
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v0, v6, v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v5, 0x30

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Parsing complete in "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " millis."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    new-instance v0, Lcom/a/a/b;

    iget-object v1, v4, Lcom/a/a/m;->o:Landroid/graphics/RectF;

    invoke-direct {v0, v3, v1}, Lcom/a/a/b;-><init>(Landroid/graphics/Picture;Landroid/graphics/RectF;)V

    .line 345
    iget-object v1, v4, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-static {v1}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_0

    .line 346
    iget-object v1, v4, Lcom/a/a/m;->p:Landroid/graphics/RectF;

    .line 348
    :cond_0
    return-object v0

    .line 330
    :cond_1
    new-instance v5, Lcom/a/a/g;

    invoke-direct {v5, p0}, Lcom/a/a/g;-><init>(Ljava/io/InputStream;)V

    .line 332
    new-instance v6, Lcom/a/a/i;

    invoke-direct {v6}, Lcom/a/a/i;-><init>()V

    .line 333
    invoke-interface {v2, v6}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 335
    new-instance v7, Ljava/io/ByteArrayInputStream;

    iget-object v8, v5, Lcom/a/a/g;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 336
    new-instance v7, Lorg/xml/sax/InputSource;

    new-instance v8, Ljava/io/ByteArrayInputStream;

    iget-object v9, v5, Lcom/a/a/g;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v7, v8}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v2, v7}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V

    .line 337
    iget-object v6, v6, Lcom/a/a/i;->a:Ljava/util/HashMap;

    iput-object v6, v4, Lcom/a/a/m;->a:Ljava/util/HashMap;

    .line 339
    invoke-interface {v2, v4}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    .line 340
    new-instance v6, Lorg/xml/sax/InputSource;

    new-instance v7, Ljava/io/ByteArrayInputStream;

    iget-object v5, v5, Lcom/a/a/g;->a:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v7, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v6, v7}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v2, v6}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 349
    :catch_0
    move-exception v0

    .line 350
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xd

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Parse error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    new-instance v1, Lcom/a/a/d;

    invoke-direct {v1, v0}, Lcom/a/a/d;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    const-string v0, "\""

    const-string v1, "&quot;"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\'"

    const-string v2, "&apos"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "<"

    const-string v2, "&lt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ">"

    const-string v2, "&gt;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "&"

    const-string v2, "&amp;"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;Lorg/xml/sax/Attributes;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 91
    invoke-interface {p1}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Ljava/lang/String;)Landroid/graphics/Matrix;
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x6

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 91
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    :goto_0
    const-string v0, "matrix("

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/e;->d(Ljava/lang/String;)Lcom/a/a/k;

    move-result-object v2

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v10, :cond_0

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    const/16 v0, 0x9

    new-array v5, v0, [F

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v8

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v9

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    const/4 v6, 0x4

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v11

    const/4 v6, 0x3

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v6

    const/4 v6, 0x4

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    const/4 v7, 0x3

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v6

    const/4 v6, 0x5

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    aput v0, v5, v6

    aput v1, v5, v10

    const/4 v0, 0x7

    aput v1, v5, v0

    const/16 v0, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v5, v0

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->setValues([F)V

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    :cond_0
    :goto_1
    const-string v0, ")"

    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v4, v0, 0x1

    if-le v2, v4, :cond_7

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "[\\s,]*"

    const-string v4, ""

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    goto/16 :goto_0

    :cond_1
    const-string v0, "translate("

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/e;->d(Ljava/lang/String;)Lcom/a/a/k;

    move-result-object v2

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v9, :cond_a

    iget-object v0, v2, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_2
    invoke-virtual {v3, v4, v0}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto :goto_1

    :cond_2
    const-string v0, "scale("

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/e;->d(Ljava/lang/String;)Lcom/a/a/k;

    move-result-object v4

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v9, :cond_9

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_3
    invoke-virtual {v3, v2, v0}, Landroid/graphics/Matrix;->preScale(FF)Z

    goto/16 :goto_1

    :cond_3
    const-string v0, "skewX("

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/e;->d(Ljava/lang/String;)Lcom/a/a/k;

    move-result-object v0

    iget-object v2, v0, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v0, v0, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->tan(D)D

    move-result-wide v4

    double-to-float v0, v4

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Matrix;->preSkew(FF)Z

    goto/16 :goto_1

    :cond_4
    const-string v0, "skewY("

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/e;->d(Ljava/lang/String;)Lcom/a/a/k;

    move-result-object v0

    iget-object v2, v0, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    iget-object v0, v0, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->tan(D)D

    move-result-wide v4

    double-to-float v0, v4

    invoke-virtual {v3, v1, v0}, Landroid/graphics/Matrix;->preSkew(FF)Z

    goto/16 :goto_1

    :cond_5
    const-string v0, "rotate("

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/e;->d(Ljava/lang/String;)Lcom/a/a/k;

    move-result-object v4

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v5

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v11, :cond_8

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v0, v4, Lcom/a/a/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    :goto_4
    invoke-virtual {v3, v2, v0}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    invoke-virtual {v3, v5}, Landroid/graphics/Matrix;->preRotate(F)Z

    neg-float v2, v2

    neg-float v0, v0

    invoke-virtual {v3, v2, v0}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    goto/16 :goto_1

    :cond_6
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid transform ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_7
    return-object v3

    :cond_8
    move v0, v1

    move v2, v1

    goto :goto_4

    :cond_9
    move v0, v2

    goto/16 :goto_3

    :cond_a
    move v0, v1

    goto/16 :goto_2
.end method

.method static synthetic b(Ljava/lang/String;Lorg/xml/sax/Attributes;)Lcom/a/a/k;
    .locals 3

    .prologue
    .line 91
    invoke-interface {p1}, Lorg/xml/sax/Attributes;->getLength()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getLocalName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1, v0}, Lorg/xml/sax/Attributes;->getValue(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/a/a/e;->d(Ljava/lang/String;)Lcom/a/a/k;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic c(Ljava/lang/String;)Landroid/graphics/Path;
    .locals 54

    .prologue
    .line 91
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v27

    new-instance v28, Lcom/a/a/a;

    const/4 v4, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v4}, Lcom/a/a/a;-><init>(Ljava/lang/CharSequence;I)V

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->b()V

    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    new-instance v29, Landroid/graphics/RectF;

    invoke-direct/range {v29 .. v29}, Landroid/graphics/RectF;-><init>()V

    const/16 v5, 0x78

    move v12, v6

    move v13, v7

    move v14, v8

    move v15, v9

    move v7, v5

    move v6, v10

    move v5, v11

    :goto_0
    move-object/from16 v0, v28

    iget v8, v0, Lcom/a/a/a;->b:I

    move/from16 v0, v27

    if-ge v8, v0, :cond_13

    move-object/from16 v0, v28

    iget v8, v0, Lcom/a/a/a;->b:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v11

    invoke-static {v11}, Ljava/lang/Character;->isDigit(C)Z

    move-result v8

    if-nez v8, :cond_1

    const/16 v8, 0x2e

    if-eq v11, v8, :cond_1

    const/16 v8, 0x2d

    if-eq v11, v8, :cond_1

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->a()C

    move-result v7

    move-object/from16 v0, v28

    iput-char v7, v0, Lcom/a/a/a;->a:C

    :goto_1
    const/4 v7, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v4, v0, v7}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    const/4 v9, 0x0

    sparse-switch v11, :sswitch_data_0

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x17

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Invalid path command: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->a()C

    move-result v7

    move-object/from16 v0, v28

    iput-char v7, v0, Lcom/a/a/a;->a:C

    move v7, v14

    move v8, v15

    move v10, v5

    move v5, v12

    move v12, v9

    move v9, v6

    move v6, v13

    :goto_2
    if-nez v12, :cond_0

    move v7, v9

    move v8, v10

    :cond_0
    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->b()V

    move v12, v5

    move v13, v6

    move v14, v7

    move v15, v8

    move v7, v11

    move v6, v9

    move v5, v10

    goto :goto_0

    :cond_1
    const/16 v8, 0x4d

    if-ne v7, v8, :cond_2

    const/16 v11, 0x4c

    goto :goto_1

    :cond_2
    const/16 v8, 0x6d

    if-ne v7, v8, :cond_16

    const/16 v11, 0x6c

    goto :goto_1

    :sswitch_0
    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v8

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v7

    const/16 v10, 0x6d

    if-ne v11, v10, :cond_3

    invoke-virtual {v4, v8, v7}, Landroid/graphics/Path;->rMoveTo(FF)V

    add-float/2addr v8, v5

    add-float v5, v6, v7

    move v6, v8

    :goto_3
    move v12, v9

    move v7, v14

    move v8, v15

    move v10, v6

    move v9, v5

    goto :goto_2

    :cond_3
    invoke-virtual {v4, v8, v7}, Landroid/graphics/Path;->moveTo(FF)V

    move v5, v7

    move v6, v8

    goto :goto_3

    :sswitch_1
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    move v5, v12

    move v6, v13

    move v7, v14

    move v8, v15

    move v10, v13

    move/from16 v52, v12

    move v12, v9

    move/from16 v9, v52

    goto :goto_2

    :sswitch_2
    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v7

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v8, 0x6c

    if-ne v11, v8, :cond_4

    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->rLineTo(FF)V

    add-float/2addr v5, v7

    add-float/2addr v10, v6

    move v6, v13

    move v7, v14

    move v8, v15

    move/from16 v52, v12

    move v12, v9

    move v9, v10

    move v10, v5

    move/from16 v5, v52

    goto :goto_2

    :cond_4
    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->lineTo(FF)V

    move v5, v12

    move v6, v13

    move v8, v15

    move v12, v9

    move v9, v10

    move v10, v7

    move v7, v14

    goto :goto_2

    :sswitch_3
    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v7, 0x68

    if-ne v11, v7, :cond_5

    const/4 v7, 0x0

    invoke-virtual {v4, v10, v7}, Landroid/graphics/Path;->rLineTo(FF)V

    add-float/2addr v5, v10

    move v7, v14

    move v8, v15

    move v10, v5

    move v5, v12

    move v12, v9

    move v9, v6

    move v6, v13

    goto/16 :goto_2

    :cond_5
    invoke-virtual {v4, v10, v6}, Landroid/graphics/Path;->lineTo(FF)V

    move v5, v12

    move v7, v14

    move v8, v15

    move v12, v9

    move v9, v6

    move v6, v13

    goto/16 :goto_2

    :sswitch_4
    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v7, 0x76

    if-ne v11, v7, :cond_6

    const/4 v7, 0x0

    invoke-virtual {v4, v7, v10}, Landroid/graphics/Path;->rLineTo(FF)V

    add-float/2addr v10, v6

    move v6, v13

    move v7, v14

    move v8, v15

    move/from16 v52, v12

    move v12, v9

    move v9, v10

    move v10, v5

    move/from16 v5, v52

    goto/16 :goto_2

    :cond_6
    invoke-virtual {v4, v5, v10}, Landroid/graphics/Path;->lineTo(FF)V

    move v6, v13

    move v7, v14

    move v8, v15

    move/from16 v52, v12

    move v12, v9

    move v9, v10

    move v10, v5

    move/from16 v5, v52

    goto/16 :goto_2

    :sswitch_5
    const/16 v16, 0x1

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v15

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v14

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v7

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v8

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v9

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v17, 0x63

    move/from16 v0, v17

    if-ne v11, v0, :cond_15

    add-float/2addr v15, v5

    add-float/2addr v7, v5

    add-float/2addr v9, v5

    add-float v5, v14, v6

    add-float/2addr v8, v6

    add-float/2addr v10, v6

    move v6, v5

    move v5, v15

    :goto_4
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move v5, v12

    move v6, v13

    move/from16 v12, v16

    move/from16 v52, v8

    move v8, v7

    move/from16 v7, v52

    move/from16 v53, v10

    move v10, v9

    move/from16 v9, v53

    goto/16 :goto_2

    :sswitch_6
    const/16 v16, 0x1

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v7

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v8

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v9

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v17, 0x73

    move/from16 v0, v17

    if-ne v11, v0, :cond_7

    add-float/2addr v7, v5

    add-float/2addr v9, v5

    add-float/2addr v8, v6

    add-float/2addr v10, v6

    :cond_7
    const/high16 v17, 0x40000000    # 2.0f

    mul-float v5, v5, v17

    sub-float/2addr v5, v15

    const/high16 v15, 0x40000000    # 2.0f

    mul-float/2addr v6, v15

    sub-float/2addr v6, v14

    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move v5, v12

    move v6, v13

    move/from16 v12, v16

    move/from16 v52, v8

    move v8, v7

    move/from16 v7, v52

    move/from16 v53, v10

    move v10, v9

    move/from16 v9, v53

    goto/16 :goto_2

    :sswitch_7
    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v7

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v16

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v17

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v8

    float-to-int v0, v8

    move/from16 v18, v0

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v8

    float-to-int v0, v8

    move/from16 v19, v0

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v8

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v20, 0x61

    move/from16 v0, v20

    if-ne v11, v0, :cond_8

    add-float/2addr v8, v5

    add-float/2addr v10, v6

    :cond_8
    float-to-double v0, v5

    move-wide/from16 v30, v0

    float-to-double v0, v6

    move-wide/from16 v32, v0

    float-to-double v0, v8

    move-wide/from16 v34, v0

    float-to-double v0, v10

    move-wide/from16 v36, v0

    float-to-double v6, v7

    move/from16 v0, v16

    float-to-double v0, v0

    move-wide/from16 v22, v0

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v16, v0

    const/4 v5, 0x1

    move/from16 v0, v18

    if-ne v0, v5, :cond_b

    const/4 v5, 0x1

    move/from16 v26, v5

    :goto_5
    const/4 v5, 0x1

    move/from16 v0, v19

    if-ne v0, v5, :cond_c

    const/4 v5, 0x1

    :goto_6
    sub-double v18, v30, v34

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    div-double v18, v18, v20

    sub-double v20, v32, v36

    const-wide/high16 v24, 0x4000000000000000L    # 2.0

    div-double v20, v20, v24

    const-wide v24, 0x4076800000000000L    # 360.0

    rem-double v16, v16, v24

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v38

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v40

    mul-double v16, v38, v18

    mul-double v24, v40, v20

    add-double v42, v16, v24

    move-wide/from16 v0, v40

    neg-double v0, v0

    move-wide/from16 v16, v0

    mul-double v16, v16, v18

    mul-double v18, v38, v20

    add-double v44, v16, v18

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v18

    mul-double v16, v20, v20

    mul-double v6, v18, v18

    mul-double v46, v42, v42

    mul-double v48, v44, v44

    div-double v22, v46, v16

    div-double v24, v48, v6

    add-double v22, v22, v24

    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    cmpl-double v24, v22, v24

    if-lez v24, :cond_14

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double v20, v20, v6

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double v18, v18, v6

    mul-double v16, v20, v20

    mul-double v6, v18, v18

    move-wide/from16 v22, v18

    move-wide/from16 v24, v20

    move-wide/from16 v18, v6

    move-wide/from16 v20, v16

    :goto_7
    move/from16 v0, v26

    if-ne v0, v5, :cond_d

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    move-wide/from16 v16, v6

    :goto_8
    mul-double v6, v20, v18

    mul-double v50, v20, v48

    sub-double v6, v6, v50

    mul-double v50, v18, v46

    sub-double v6, v6, v50

    mul-double v20, v20, v48

    mul-double v18, v18, v46

    add-double v18, v18, v20

    div-double v6, v6, v18

    const-wide/16 v18, 0x0

    cmpg-double v18, v6, v18

    if-gez v18, :cond_9

    const-wide/16 v6, 0x0

    :cond_9
    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double v6, v6, v16

    mul-double v16, v24, v44

    div-double v16, v16, v22

    mul-double v16, v16, v6

    mul-double v18, v22, v42

    div-double v18, v18, v24

    move-wide/from16 v0, v18

    neg-double v0, v0

    move-wide/from16 v18, v0

    mul-double v6, v6, v18

    add-double v18, v30, v34

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    div-double v18, v18, v20

    add-double v20, v32, v36

    const-wide/high16 v30, 0x4000000000000000L    # 2.0

    div-double v20, v20, v30

    mul-double v30, v38, v16

    mul-double v32, v40, v6

    sub-double v30, v30, v32

    add-double v18, v18, v30

    mul-double v30, v40, v16

    mul-double v32, v38, v6

    add-double v30, v30, v32

    add-double v20, v20, v30

    sub-double v30, v42, v16

    div-double v30, v30, v24

    sub-double v32, v44, v6

    div-double v32, v32, v22

    move-wide/from16 v0, v42

    neg-double v0, v0

    move-wide/from16 v34, v0

    sub-double v16, v34, v16

    div-double v16, v16, v24

    move-wide/from16 v0, v44

    neg-double v0, v0

    move-wide/from16 v34, v0

    sub-double v6, v34, v6

    div-double v34, v6, v22

    mul-double v6, v30, v30

    mul-double v36, v32, v32

    add-double v6, v6, v36

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    const-wide/16 v6, 0x0

    cmpg-double v6, v32, v6

    if-gez v6, :cond_e

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    :goto_9
    div-double v36, v30, v36

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->acos(D)D

    move-result-wide v36

    mul-double v6, v6, v36

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v36

    mul-double v6, v30, v30

    mul-double v38, v32, v32

    add-double v6, v6, v38

    mul-double v38, v16, v16

    mul-double v40, v34, v34

    add-double v38, v38, v40

    mul-double v6, v6, v38

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    mul-double v6, v30, v16

    mul-double v40, v32, v34

    add-double v40, v40, v6

    mul-double v6, v30, v34

    mul-double v16, v16, v32

    sub-double v6, v6, v16

    const-wide/16 v16, 0x0

    cmpg-double v6, v6, v16

    if-gez v6, :cond_f

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    :goto_a
    div-double v16, v40, v38

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->acos(D)D

    move-result-wide v16

    mul-double v6, v6, v16

    invoke-static {v6, v7}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v6

    if-nez v5, :cond_10

    const-wide/16 v16, 0x0

    cmpl-double v16, v6, v16

    if-lez v16, :cond_10

    const-wide v16, 0x4076800000000000L    # 360.0

    sub-double v6, v6, v16

    :cond_a
    :goto_b
    const-wide v16, 0x4076800000000000L    # 360.0

    rem-double v6, v6, v16

    const-wide v16, 0x4076800000000000L    # 360.0

    rem-double v16, v36, v16

    new-instance v5, Landroid/graphics/RectF;

    sub-double v30, v18, v24

    move-wide/from16 v0, v30

    double-to-float v0, v0

    move/from16 v26, v0

    sub-double v30, v20, v22

    move-wide/from16 v0, v30

    double-to-float v0, v0

    move/from16 v30, v0

    add-double v18, v18, v24

    move-wide/from16 v0, v18

    double-to-float v0, v0

    move/from16 v18, v0

    add-double v20, v20, v22

    move-wide/from16 v0, v20

    double-to-float v0, v0

    move/from16 v19, v0

    move/from16 v0, v26

    move/from16 v1, v30

    move/from16 v2, v18

    move/from16 v3, v19

    invoke-direct {v5, v0, v1, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    double-to-float v6, v6

    move/from16 v0, v16

    invoke-virtual {v4, v5, v0, v6}, Landroid/graphics/Path;->addArc(Landroid/graphics/RectF;FF)V

    move v5, v12

    move v6, v13

    move v7, v14

    move v12, v9

    move v9, v10

    move v10, v8

    move v8, v15

    goto/16 :goto_2

    :cond_b
    const/4 v5, 0x0

    move/from16 v26, v5

    goto/16 :goto_5

    :cond_c
    const/4 v5, 0x0

    goto/16 :goto_6

    :cond_d
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v16, v6

    goto/16 :goto_8

    :cond_e
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    goto/16 :goto_9

    :cond_f
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    goto :goto_a

    :cond_10
    if-eqz v5, :cond_a

    const-wide/16 v16, 0x0

    cmpg-double v5, v6, v16

    if-gez v5, :cond_a

    const-wide v16, 0x4076800000000000L    # 360.0

    add-double v6, v6, v16

    goto :goto_b

    :sswitch_8
    const/16 v16, 0x1

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v9

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v7, 0x74

    if-ne v11, v7, :cond_11

    add-float/2addr v9, v5

    add-float/2addr v10, v6

    :cond_11
    const/high16 v7, 0x40000000    # 2.0f

    mul-float/2addr v7, v5

    sub-float/2addr v7, v15

    const/high16 v8, 0x40000000    # 2.0f

    mul-float/2addr v8, v6

    sub-float/2addr v8, v14

    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move v5, v12

    move v6, v13

    move/from16 v12, v16

    move/from16 v52, v8

    move v8, v7

    move/from16 v7, v52

    move/from16 v53, v10

    move v10, v9

    move/from16 v9, v53

    goto/16 :goto_2

    :sswitch_9
    const/4 v14, 0x1

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v7

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v8

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v9

    invoke-virtual/range {v28 .. v28}, Lcom/a/a/a;->c()F

    move-result v10

    const/16 v15, 0x71

    if-ne v11, v15, :cond_12

    add-float/2addr v9, v5

    add-float/2addr v10, v6

    add-float/2addr v7, v5

    add-float/2addr v8, v6

    :cond_12
    invoke-virtual/range {v4 .. v10}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    move v5, v12

    move v6, v13

    move v12, v14

    move/from16 v52, v8

    move v8, v7

    move/from16 v7, v52

    move/from16 v53, v10

    move v10, v9

    move/from16 v9, v53

    goto/16 :goto_2

    :cond_13
    return-object v4

    :cond_14
    move-wide/from16 v22, v18

    move-wide/from16 v24, v20

    move-wide/from16 v18, v6

    move-wide/from16 v20, v16

    goto/16 :goto_7

    :cond_15
    move v6, v14

    move v5, v15

    goto/16 :goto_4

    :cond_16
    move v11, v7

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_7
        0x43 -> :sswitch_5
        0x48 -> :sswitch_3
        0x4c -> :sswitch_2
        0x4d -> :sswitch_0
        0x51 -> :sswitch_9
        0x53 -> :sswitch_6
        0x54 -> :sswitch_8
        0x56 -> :sswitch_4
        0x5a -> :sswitch_1
        0x61 -> :sswitch_7
        0x63 -> :sswitch_5
        0x68 -> :sswitch_3
        0x6c -> :sswitch_2
        0x6d -> :sswitch_0
        0x71 -> :sswitch_9
        0x73 -> :sswitch_6
        0x74 -> :sswitch_8
        0x76 -> :sswitch_4
        0x7a -> :sswitch_1
    .end sparse-switch
.end method

.method private static d(Ljava/lang/String;)Lcom/a/a/k;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 368
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 370
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v4, v2

    move v0, v1

    move v3, v1

    .line 372
    :goto_0
    if-ge v4, v5, :cond_4

    .line 373
    if-eqz v0, :cond_0

    move v0, v1

    .line 430
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 377
    :cond_0
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 378
    sparse-switch v7, :sswitch_data_0

    goto :goto_1

    .line 415
    :sswitch_0
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 417
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_3

    .line 419
    invoke-static {v8}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 420
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    const/16 v3, 0x2d

    if-ne v7, v3, :cond_2

    move v3, v4

    .line 422
    goto :goto_1

    .line 401
    :sswitch_1
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 402
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 404
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 405
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_1
    new-instance v0, Lcom/a/a/k;

    invoke-direct {v0, v6, v4}, Lcom/a/a/k;-><init>(Ljava/util/ArrayList;I)V

    .line 444
    :goto_2
    return-object v0

    .line 424
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v3, v0

    move v0, v2

    .line 427
    goto :goto_1

    .line 428
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 434
    :cond_4
    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 435
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_5

    .line 438
    :try_start_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 442
    :goto_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    .line 444
    :cond_5
    new-instance v0, Lcom/a/a/k;

    invoke-direct {v0, v6, v3}, Lcom/a/a/k;-><init>(Ljava/util/ArrayList;I)V

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3

    .line 378
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0x20 -> :sswitch_0
        0x29 -> :sswitch_1
        0x2c -> :sswitch_0
        0x2d -> :sswitch_0
        0x41 -> :sswitch_1
        0x43 -> :sswitch_1
        0x48 -> :sswitch_1
        0x4c -> :sswitch_1
        0x4d -> :sswitch_1
        0x51 -> :sswitch_1
        0x53 -> :sswitch_1
        0x54 -> :sswitch_1
        0x56 -> :sswitch_1
        0x5a -> :sswitch_1
        0x61 -> :sswitch_1
        0x63 -> :sswitch_1
        0x68 -> :sswitch_1
        0x6c -> :sswitch_1
        0x6d -> :sswitch_1
        0x71 -> :sswitch_1
        0x73 -> :sswitch_1
        0x74 -> :sswitch_1
        0x76 -> :sswitch_1
        0x7a -> :sswitch_1
    .end sparse-switch
.end method
