.class Lcom/a/a/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Z

.field d:F

.field e:F

.field f:F

.field g:F

.field h:F

.field i:F

.field j:F

.field k:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field m:Landroid/graphics/Matrix;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 936
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 942
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    .line 943
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    .line 944
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    return-void
.end method


# virtual methods
.method public final a(Lcom/a/a/h;)Lcom/a/a/h;
    .locals 3

    .prologue
    .line 947
    new-instance v0, Lcom/a/a/h;

    invoke-direct {v0}, Lcom/a/a/h;-><init>()V

    .line 948
    iget-object v1, p1, Lcom/a/a/h;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/a/a/h;->a:Ljava/lang/String;

    .line 949
    iget-object v1, p0, Lcom/a/a/h;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/a/a/h;->b:Ljava/lang/String;

    .line 950
    iget-boolean v1, p1, Lcom/a/a/h;->c:Z

    iput-boolean v1, v0, Lcom/a/a/h;->c:Z

    .line 951
    iget v1, p1, Lcom/a/a/h;->d:F

    iput v1, v0, Lcom/a/a/h;->d:F

    .line 952
    iget v1, p1, Lcom/a/a/h;->f:F

    iput v1, v0, Lcom/a/a/h;->f:F

    .line 953
    iget v1, p1, Lcom/a/a/h;->e:F

    iput v1, v0, Lcom/a/a/h;->e:F

    .line 954
    iget v1, p1, Lcom/a/a/h;->g:F

    iput v1, v0, Lcom/a/a/h;->g:F

    .line 955
    iget v1, p1, Lcom/a/a/h;->h:F

    iput v1, v0, Lcom/a/a/h;->h:F

    .line 956
    iget v1, p1, Lcom/a/a/h;->i:F

    iput v1, v0, Lcom/a/a/h;->i:F

    .line 957
    iget v1, p1, Lcom/a/a/h;->j:F

    iput v1, v0, Lcom/a/a/h;->j:F

    .line 958
    iget-object v1, p0, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/a/a/h;->k:Ljava/util/ArrayList;

    .line 959
    iget-object v1, p0, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    iput-object v1, v0, Lcom/a/a/h;->l:Ljava/util/ArrayList;

    .line 960
    iget-object v1, p0, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    iput-object v1, v0, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    .line 961
    iget-object v1, p1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    if-eqz v1, :cond_0

    .line 962
    iget-object v1, p0, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    if-nez v1, :cond_1

    .line 963
    iget-object v1, p1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    iput-object v1, v0, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    .line 970
    :cond_0
    :goto_0
    return-object v0

    .line 965
    :cond_1
    new-instance v1, Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    invoke-direct {v1, v2}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 966
    iget-object v2, p1, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 967
    iput-object v1, v0, Lcom/a/a/h;->m:Landroid/graphics/Matrix;

    goto :goto_0
.end method
