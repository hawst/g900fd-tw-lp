.class public Lcom/google/android/libraries/curvular/t;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 571
    const/16 v0, 0x8

    .line 572
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    const/4 v1, 0x0

    .line 573
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 571
    invoke-static {p0, v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    sget-object v0, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/au;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/au;",
            ">;"
        }
    .end annotation

    .prologue
    .line 707
    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/cf;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/cg",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1258
    const/4 v0, 0x0

    .line 1259
    if-eqz p0, :cond_0

    .line 1260
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Class;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v1

    .line 1261
    new-instance v0, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    .line 1268
    :cond_0
    sget-object v1, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/h;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/h;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1844
    sget-object v0, Lcom/google/android/libraries/curvular/g;->aZ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559
    const/16 v0, 0x8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a(Ljava/lang/CharSequence;[Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/CharSequence;",
            "[",
            "Ljava/lang/Object;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1624
    .line 1625
    invoke-static {v4, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 1626
    array-length v0, p1

    new-array v2, v0, [Lcom/google/android/libraries/curvular/ah;

    .line 1627
    const/4 v0, 0x0

    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 1628
    aget-object v3, p1, v0

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v0

    .line 1627
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1631
    :cond_0
    new-instance v0, Lcom/google/android/libraries/curvular/y;

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/libraries/curvular/y;-><init>(Ljava/lang/Enum;[Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    sget-object v0, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;Lcom/google/android/libraries/curvular/cs;)Lcom/google/android/libraries/curvular/ah;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            "Lcom/google/android/libraries/curvular/cs;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 902
    new-instance v0, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p1, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    .line 903
    sget-object v1, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static varargs a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 3

    .prologue
    .line 1890
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.FrameLayout"

    .line 1891
    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 1890
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 582
    const/4 v0, 0x0

    .line 583
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    const/16 v1, 0x8

    .line 584
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 582
    invoke-static {p0, v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/am;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/au;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/au;",
            ">;"
        }
    .end annotation

    .prologue
    .line 933
    sget-object v0, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/cf;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/cf;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/ch",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1290
    const/4 v0, 0x0

    .line 1291
    if-eqz p0, :cond_0

    .line 1292
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Landroid/widget/CompoundButton;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Ljava/lang/Boolean;

    aput-object v2, v0, v1

    .line 1293
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v1

    .line 1294
    new-instance v0, Lcom/google/android/libraries/curvular/v;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/v;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    .line 1301
    :cond_0
    sget-object v1, Lcom/google/android/libraries/curvular/g;->aS:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 577
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 595
    sget-object v0, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static varargs b([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 3

    .prologue
    .line 1909
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    .line 1910
    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 1909
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static c(Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/au;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/au;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1394
    sget-object v0, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;+",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 702
    sget-object v0, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static varargs c([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 3

    .prologue
    .line 1938
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.RelativeLayout"

    .line 1939
    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 1938
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/google/android/libraries/curvular/au;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/au;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/au;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1421
    sget-object v0, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/ah;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 890
    new-instance v0, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    .line 891
    sget-object v1, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static varargs d([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 3

    .prologue
    .line 1990
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.view.View"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/ah;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Lcom/google/android/libraries/curvular/i;",
            ">;"
        }
    .end annotation

    .prologue
    .line 896
    new-instance v0, Lcom/google/android/libraries/curvular/i;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/i;-><init>(ILcom/google/android/libraries/curvular/bk;Z)V

    .line 897
    sget-object v1, Lcom/google/android/libraries/curvular/g;->bq:Lcom/google/android/libraries/curvular/g;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 928
    sget-object v0, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static g(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Integer;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1355
    sget-object v0, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method
