.class public Lcom/google/android/libraries/curvular/bm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/b/c/ak;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/ak",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/bn",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Lcom/google/android/libraries/curvular/bd;

.field d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    invoke-static {}, Lcom/google/b/c/bl;->b()Lcom/google/b/c/bl;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    .line 44
    iput-object p1, p0, Lcom/google/android/libraries/curvular/bm;->c:Lcom/google/android/libraries/curvular/bd;

    .line 45
    return-void
.end method

.method public static a(I)J
    .locals 2

    .prologue
    .line 98
    int-to-long v0, p0

    return-wide v0
.end method


# virtual methods
.method public final a(Landroid/view/View;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bn;

    .line 149
    const-class v1, Lcom/google/android/libraries/curvular/av;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/bn;->a:Ljava/lang/Class;

    invoke-virtual {v1, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    .line 150
    :goto_0
    if-nez v1, :cond_1

    sget v1, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/cm;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/cm;->b:Lcom/google/android/libraries/curvular/ce;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/bn;->b:Lcom/google/android/libraries/curvular/ce;

    if-ne v1, v2, :cond_1

    .line 160
    :goto_1
    return-object p1

    .line 149
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 155
    :cond_1
    sget-object v1, Lcom/google/android/libraries/curvular/g;->bZ:Lcom/google/android/libraries/curvular/g;

    .line 156
    iget-object v2, v0, Lcom/google/android/libraries/curvular/bn;->b:Lcom/google/android/libraries/curvular/ce;

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 157
    invoke-static {p1, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ah;)V

    .line 159
    iget-object v0, v0, Lcom/google/android/libraries/curvular/bn;->b:Lcom/google/android/libraries/curvular/ce;

    invoke-static {p1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>;TT;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 51
    const-string v0, "Null layout provided"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 52
    :cond_0
    const-string v0, "Null model provided"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 53
    :cond_1
    iget-object v3, p0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    const-class v0, Lcom/google/android/libraries/curvular/av;

    invoke-virtual {v0, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    new-instance v4, Lcom/google/android/libraries/curvular/bn;

    invoke-direct {v4, p1, p2, v0}, Lcom/google/android/libraries/curvular/bn;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;Z)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    iget v0, p0, Lcom/google/android/libraries/curvular/bm;->d:I

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    invoke-interface {v0, p1}, Lcom/google/b/c/ak;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v3, p0, Lcom/google/android/libraries/curvular/bm;->d:I

    if-ge v0, v3, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    const-string v0, "Cannot add a new layout type once viewTypeCount is evaluated!"

    if-nez v2, :cond_5

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move v0, v2

    .line 53
    goto :goto_0

    .line 54
    :cond_5
    sget-object v0, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    invoke-interface {v0, p1}, Lcom/google/b/c/ak;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    sget-object v1, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    invoke-interface {v1}, Lcom/google/b/c/ak;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/b/c/ak;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    :cond_6
    return-void
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bn;

    .line 107
    iget-boolean v1, v0, Lcom/google/android/libraries/curvular/bn;->c:Z

    if-nez v1, :cond_0

    .line 109
    neg-int v0, p1

    add-int/lit8 v0, v0, -0x1

    .line 111
    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bn;->a:Ljava/lang/Class;

    invoke-interface {v1, v0}, Lcom/google/b/c/ak;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final c(I)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(I)",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 126
    if-gez p1, :cond_0

    .line 130
    neg-int v0, p1

    add-int/lit8 v0, v0, -0x1

    .line 131
    iget-object v1, p0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bn;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bn;->a:Ljava/lang/Class;

    .line 133
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    invoke-interface {v0}, Lcom/google/b/c/ak;->ap_()Lcom/google/b/c/ak;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/b/c/ak;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    goto :goto_0
.end method
