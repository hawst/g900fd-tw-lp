.class public Lcom/google/android/libraries/curvular/z;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Landroid/graphics/Typeface;

.field public static final b:Landroid/graphics/Typeface;

.field public static final c:Landroid/graphics/Typeface;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 530
    const-string v0, "sans-serif"

    const/4 v1, 0x1

    .line 531
    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/z;->a:Landroid/graphics/Typeface;

    .line 532
    const-string v0, "sans-serif-condensed"

    .line 533
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/z;->b:Landroid/graphics/Typeface;

    .line 534
    const-string v0, "sans-serif-light"

    .line 535
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    .line 536
    const-string v0, "sans-serif-medium"

    .line 537
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    .line 538
    const-string v0, "sans-serif"

    .line 539
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/z;->c:Landroid/graphics/Typeface;

    .line 540
    const-string v0, "sans-serif-thin"

    .line 541
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    .line 540
    return-void
.end method
