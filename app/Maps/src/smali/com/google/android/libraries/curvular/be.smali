.class Lcom/google/android/libraries/curvular/be;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ComponentCallbacks2;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/curvular/bd;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/curvular/bd;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->f:Landroid/content/res/Configuration;

    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    move-result v0

    .line 79
    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    monitor-enter v2

    .line 87
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    .line 88
    invoke-interface {v0}, Lcom/google/b/c/am;->size()I

    move-result v3

    if-ltz v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    .line 88
    goto :goto_1

    :cond_2
    :try_start_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v0}, Lcom/google/b/c/am;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 90
    iget-object v5, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v5, v5, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v5, v0}, Lcom/google/b/c/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ay;

    .line 91
    invoke-static {}, Lcom/google/android/libraries/curvular/ay;->m()Z

    move-result v5

    if-nez v5, :cond_3

    .line 92
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 97
    :cond_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    :goto_3
    if-ge v1, v3, :cond_5

    .line 98
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ay;

    .line 99
    iget-object v5, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v5, v5, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/b/c/am;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    iget-object v5, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v5, v5, Lcom/google/android/libraries/curvular/bd;->d:Lcom/google/b/c/er;

    invoke-interface {v5, v0}, Lcom/google/b/c/er;->b(Ljava/lang/Object;)Ljava/util/List;

    .line 97
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 102
    :cond_5
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/curvular/be;->a:Lcom/google/android/libraries/curvular/bd;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->f:Landroid/content/res/Configuration;

    invoke-virtual {v0, p1}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    .line 108
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->a()V

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 114
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0

    .prologue
    .line 119
    return-void
.end method
