.class public Lcom/google/android/libraries/curvular/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cp;


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/bd;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/libraries/curvular/c/b;->a:Lcom/google/android/libraries/curvular/bd;

    .line 32
    return-void
.end method

.method public static a(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/libraries/curvular/bc;Landroid/support/v7/widget/RecyclerView;)Lcom/google/android/libraries/curvular/c/j;
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->a()Landroid/support/v7/widget/bk;

    move-result-object v0

    .line 85
    instance-of v1, v0, Lcom/google/android/libraries/curvular/c/j;

    if-eqz v1, :cond_0

    .line 88
    check-cast v0, Lcom/google/android/libraries/curvular/c/j;

    .line 89
    iget-object v1, v0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 90
    iget-object v1, p1, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/c/b;->a(Ljava/util/List;Lcom/google/android/libraries/curvular/c/j;)V

    .line 91
    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/c/j;->b()V

    .line 97
    :goto_0
    return-object v0

    .line 95
    :cond_0
    iget-object v0, p1, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/libraries/curvular/c/j;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/curvular/c/j;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c/b;->a(Ljava/util/List;Lcom/google/android/libraries/curvular/c/j;)V

    .line 96
    invoke-virtual {p2, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/bk;)V

    invoke-static {p2}, Lcom/google/android/libraries/curvular/c/i;->a(Landroid/support/v7/widget/RecyclerView;)Lcom/google/android/libraries/curvular/c/i;

    move-result-object v2

    instance-of v0, v1, Lcom/google/android/libraries/curvular/c/j;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/google/android/libraries/curvular/c/j;

    iput-object v2, v0, Lcom/google/android/libraries/curvular/c/j;->d:Lcom/google/android/libraries/curvular/c/k;

    :cond_1
    move-object v0, v1

    .line 97
    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/google/android/libraries/curvular/c/j;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<*>;>;>;>;",
            "Lcom/google/android/libraries/curvular/c/j;",
            ")V"
        }
    .end annotation

    .prologue
    .line 128
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    .line 129
    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    .line 131
    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    .line 132
    iget-object v3, p1, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v3, v0, v1}, Lcom/google/android/libraries/curvular/bm;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 134
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;Ljava/lang/Object;Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Ljava/lang/Object;",
            "TT;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 37
    instance-of v2, p1, Lcom/google/android/libraries/curvular/c/a;

    if-eqz v2, :cond_c

    .line 38
    sget-object v2, Lcom/google/android/libraries/curvular/c/c;->a:[I

    check-cast p1, Lcom/google/android/libraries/curvular/c/a;

    invoke-virtual {p1}, Lcom/google/android/libraries/curvular/c/a;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    move v0, v1

    .line 66
    :cond_1
    :goto_1
    return v0

    .line 40
    :pswitch_0
    instance-of v2, p2, Lcom/google/android/libraries/curvular/au;

    if-eqz v2, :cond_2

    instance-of v2, p4, Landroid/support/v7/widget/CardView;

    if-eqz v2, :cond_2

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    check-cast p4, Landroid/support/v7/widget/CardView;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/CardView;->setRadius(F)V

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1

    .line 42
    :pswitch_1
    instance-of v2, p2, Lcom/google/android/libraries/curvular/au;

    if-eqz v2, :cond_3

    instance-of v2, p4, Landroid/support/v7/widget/CardView;

    if-eqz v2, :cond_3

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    check-cast p4, Landroid/support/v7/widget/CardView;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/CardView;->setCardElevation(F)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_1

    .line 44
    :pswitch_2
    instance-of v2, p2, Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    instance-of v2, p4, Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_4

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1

    .line 46
    :pswitch_3
    instance-of v2, p2, Landroid/support/v7/widget/bs;

    if-eqz v2, :cond_5

    instance-of v2, p4, Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_5

    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    check-cast p2, Landroid/support/v7/widget/bs;

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/bs;)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_1

    .line 48
    :pswitch_4
    instance-of v2, p2, Lcom/google/android/libraries/curvular/au;

    if-eqz v2, :cond_6

    instance-of v2, p4, Landroid/support/v7/widget/CardView;

    if-eqz v2, :cond_6

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    check-cast p4, Landroid/support/v7/widget/CardView;

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/CardView;->setMaxCardElevation(F)V

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1

    .line 50
    :pswitch_5
    instance-of v2, p4, Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_8

    if-eqz p2, :cond_7

    instance-of v2, p2, Landroid/support/v7/widget/bu;

    if-eqz v2, :cond_8

    :cond_7
    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    check-cast p2, Landroid/support/v7/widget/bu;

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(Landroid/support/v7/widget/bu;)V

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto/16 :goto_1

    .line 52
    :pswitch_6
    instance-of v2, p2, Lcom/google/android/libraries/curvular/c/k;

    if-nez v2, :cond_9

    move v0, v1

    goto/16 :goto_1

    :cond_9
    instance-of v2, p4, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_a

    move v0, v1

    goto/16 :goto_1

    :cond_a
    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p4}, Lcom/google/android/libraries/curvular/c/i;->a(Landroid/support/v7/widget/RecyclerView;)Lcom/google/android/libraries/curvular/c/i;

    move-result-object v1

    check-cast p2, Lcom/google/android/libraries/curvular/c/k;

    iput-object p2, v1, Lcom/google/android/libraries/curvular/c/i;->a:Lcom/google/android/libraries/curvular/c/k;

    goto/16 :goto_1

    .line 54
    :pswitch_7
    instance-of v2, p2, Ljava/lang/Boolean;

    if-eqz v2, :cond_b

    instance-of v2, p4, Landroid/support/v7/widget/CardView;

    if-eqz v2, :cond_b

    check-cast p4, Landroid/support/v7/widget/CardView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p4, v1}, Landroid/support/v7/widget/CardView;->setUseCompatPadding(Z)V

    goto/16 :goto_1

    :cond_b
    move v0, v1

    goto/16 :goto_1

    .line 58
    :cond_c
    instance-of v2, p1, Lcom/google/android/libraries/curvular/g;

    if-eqz v2, :cond_0

    .line 59
    sget-object v2, Lcom/google/android/libraries/curvular/c/c;->b:[I

    check-cast p1, Lcom/google/android/libraries/curvular/g;

    invoke-virtual {p1}, Lcom/google/android/libraries/curvular/g;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto/16 :goto_0

    .line 61
    :pswitch_8
    instance-of v2, p4, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_d

    move v0, v1

    goto/16 :goto_1

    :cond_d
    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    instance-of v2, p2, Landroid/support/v7/widget/bk;

    if-eqz v2, :cond_e

    check-cast p2, Landroid/support/v7/widget/bk;

    invoke-virtual {p4, p2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/bk;)V

    invoke-static {p4}, Lcom/google/android/libraries/curvular/c/i;->a(Landroid/support/v7/widget/RecyclerView;)Lcom/google/android/libraries/curvular/c/i;

    move-result-object v1

    instance-of v2, p2, Lcom/google/android/libraries/curvular/c/j;

    if-eqz v2, :cond_1

    check-cast p2, Lcom/google/android/libraries/curvular/c/j;

    iput-object v1, p2, Lcom/google/android/libraries/curvular/c/j;->d:Lcom/google/android/libraries/curvular/c/k;

    goto/16 :goto_1

    :cond_e
    instance-of v2, p2, Lcom/google/android/libraries/curvular/bc;

    if-eqz v2, :cond_f

    iget-object v1, p0, Lcom/google/android/libraries/curvular/c/b;->a:Lcom/google/android/libraries/curvular/bd;

    check-cast p2, Lcom/google/android/libraries/curvular/bc;

    invoke-static {v1, p2, p4}, Lcom/google/android/libraries/curvular/c/b;->a(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/libraries/curvular/bc;Landroid/support/v7/widget/RecyclerView;)Lcom/google/android/libraries/curvular/c/j;

    goto/16 :goto_1

    :cond_f
    move v0, v1

    goto/16 :goto_1

    .line 38
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 59
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
    .end packed-switch
.end method
