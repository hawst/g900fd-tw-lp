.class public Lcom/google/android/libraries/curvular/bd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/b/c/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/am",
            "<",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation
.end field

.field final b:Lcom/google/android/libraries/curvular/cc;

.field public c:Lcom/google/android/libraries/curvular/cp;

.field final d:Lcom/google/b/c/er;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/er",
            "<",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public e:Landroid/content/Context;

.field final f:Landroid/content/res/Configuration;

.field public g:Landroid/content/ComponentCallbacks2;

.field private final h:Lcom/google/b/c/er;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/er",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cc;)V
    .locals 2

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-static {}, Lcom/google/b/c/hj;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    .line 41
    invoke-static {v0}, Lcom/google/b/c/ii;->a(Ljava/util/Map;)Lcom/google/b/c/ii;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    .line 52
    invoke-static {}, Lcom/google/b/c/aj;->m()Lcom/google/b/c/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bd;->d:Lcom/google/b/c/er;

    .line 58
    invoke-static {}, Lcom/google/b/c/aj;->m()Lcom/google/b/c/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bd;->h:Lcom/google/b/c/er;

    .line 70
    new-instance v0, Lcom/google/android/libraries/curvular/be;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/be;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bd;->g:Landroid/content/ComponentCallbacks2;

    .line 123
    iput-object p1, p0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    .line 124
    iput-object p2, p0, Lcom/google/android/libraries/curvular/bd;->b:Lcom/google/android/libraries/curvular/cc;

    .line 125
    new-instance v0, Landroid/content/res/Configuration;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bd;->f:Landroid/content/res/Configuration;

    .line 126
    return-void
.end method

.method private b(Ljava/lang/Class;)Lcom/google/android/libraries/curvular/ay;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;)",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation

    .prologue
    .line 261
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ay;

    .line 262
    iget-object v2, p0, Lcom/google/android/libraries/curvular/bd;->f:Landroid/content/res/Configuration;

    iget-object v1, v0, Lcom/google/android/libraries/curvular/ay;->o:Landroid/content/res/Configuration;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "Already initialized."

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 266
    :catch_0
    move-exception v1

    .line 267
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 262
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    iput-object v2, v0, Lcom/google/android/libraries/curvular/ay;->o:Landroid/content/res/Configuration;

    .line 263
    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/ay;->j()Lcom/google/android/libraries/curvular/cq;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/libraries/curvular/cq;->k:Lcom/google/android/libraries/curvular/ay;

    .line 264
    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/ay;->j()Lcom/google/android/libraries/curvular/cq;

    move-result-object v1

    iput-object p0, v1, Lcom/google/android/libraries/curvular/cq;->l:Lcom/google/android/libraries/curvular/bd;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 265
    return-object v0

    :cond_2
    move-object v0, v1

    .line 267
    check-cast v0, Ljava/lang/Throwable;

    const-class v2, Ljava/lang/Error;

    if-eqz v0, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_3
    const-class v2, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            "U:",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>(",
            "Ljava/lang/Class",
            "<TU;>;",
            "Landroid/view/ViewGroup;",
            ")",
            "Lcom/google/android/libraries/curvular/ae",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 180
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            "U:",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>(",
            "Ljava/lang/Class",
            "<TU;>;",
            "Landroid/view/ViewGroup;",
            "Z)",
            "Lcom/google/android/libraries/curvular/ae",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 185
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/libraries/curvular/bd;->b(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 186
    if-eqz v0, :cond_0

    .line 210
    :goto_0
    return-object v0

    .line 192
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    .line 195
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;)Lcom/google/android/libraries/curvular/ay;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/curvular/ay;->j()Lcom/google/android/libraries/curvular/cq;

    move-result-object v2

    .line 196
    invoke-virtual {v2, p0, p2, p3}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/bd;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 199
    const-string v3, "created: "

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 202
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v4

    sub-long v0, v4, v0

    .line 203
    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x23

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "created: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " in "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    sget-boolean v0, Lcom/google/android/libraries/curvular/bo;->c:Z

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bd;->h:Lcom/google/b/c/er;

    invoke-interface {v0, p1}, Lcom/google/b/c/er;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {v2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    new-instance v0, Ljava/lang/Throwable;

    invoke-direct {v0}, Ljava/lang/Throwable;-><init>()V

    .line 210
    :cond_1
    new-instance v0, Lcom/google/android/libraries/curvular/ae;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/ae;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 199
    :cond_2
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Ljava/lang/Class;)Lcom/google/android/libraries/curvular/ay;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;)",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v0, p1}, Lcom/google/b/c/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ay;

    .line 246
    if-nez v0, :cond_1

    .line 247
    iget-object v1, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    monitor-enter v1

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v0, p1}, Lcom/google/b/c/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ay;

    .line 249
    if-nez v0, :cond_0

    .line 250
    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/bd;->b(Ljava/lang/Class;)Lcom/google/android/libraries/curvular/ay;

    move-result-object v0

    .line 251
    iget-object v2, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v2, p1, v0}, Lcom/google/b/c/am;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 253
    :cond_0
    monitor-exit v1

    .line 255
    :cond_1
    return-object v0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 278
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-nez v0, :cond_2

    :cond_0
    :goto_0
    if-eqz v1, :cond_d

    .line 304
    :cond_1
    return-void

    .line 278
    :cond_2
    iget-boolean v3, v0, Lcom/google/android/libraries/curvular/cm;->g:Z

    if-nez v3, :cond_0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    iget-object v3, v0, Lcom/google/android/libraries/curvular/cq;->k:Lcom/google/android/libraries/curvular/ay;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v5, v0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_3

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_3
    invoke-static {p1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;)V

    sget-boolean v0, Lcom/google/android/libraries/curvular/bo;->c:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/libraries/curvular/bd;->h:Lcom/google/b/c/er;

    invoke-interface {v0, v4}, Lcom/google/b/c/er;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    move v0, v1

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v6

    if-ne v6, p1, :cond_4

    invoke-interface {v5}, Ljava/util/ListIterator;->remove()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    if-ne v0, v2, :cond_6

    move v0, v2

    :goto_2
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v0, v4}, Lcom/google/b/c/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, v3, :cond_9

    const-string v0, "dropped because of configuration change: "

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_8
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bd;->d:Lcom/google/b/c/er;

    invoke-interface {v0, v3}, Lcom/google/b/c/er;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {p0, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;)Lcom/google/android/libraries/curvular/ay;

    invoke-static {}, Lcom/google/android/libraries/curvular/ay;->k()I

    move-result v5

    if-ge v3, v5, :cond_b

    const-string v1, "cached: "

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_a

    invoke-virtual {v1, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_3
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ah;)V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v1, v2

    goto/16 :goto_0

    :cond_a
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    :cond_b
    const-string v0, "dropped: "

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_c
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 283
    :cond_d
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    .line 288
    instance-of v0, p1, Landroid/widget/AdapterView;

    if-nez v0, :cond_1

    .line 293
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-nez v0, :cond_1

    .line 299
    check-cast p1, Landroid/view/ViewGroup;

    .line 301
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_4
    if-ltz v0, :cond_1

    .line 302
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/libraries/curvular/bd;->a(Landroid/view/View;)V

    .line 301
    add-int/lit8 v0, v0, -0x1

    goto :goto_4
.end method

.method public final a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 159
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 160
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;)Lcom/google/android/libraries/curvular/ay;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/ay;->j()Lcom/google/android/libraries/curvular/cq;

    move-result-object v0

    invoke-virtual {v0, p0, p2}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/bd;Landroid/view/View;)V

    .line 161
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/libraries/curvular/cm;->g:Z

    .line 162
    return-void
.end method

.method public final b(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>;",
            "Landroid/view/ViewGroup;",
            "Z)",
            "Lcom/google/android/libraries/curvular/ae",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bd;->d:Lcom/google/b/c/er;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;)Lcom/google/android/libraries/curvular/ay;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/b/c/er;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 222
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 223
    const/4 v0, 0x0

    .line 236
    :goto_0
    return-object v0

    .line 226
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 227
    invoke-static {p2, v0, p3}, Lcom/google/android/libraries/curvular/cd;->a(Landroid/view/ViewGroup;Landroid/view/View;Z)V

    .line 230
    const-string v1, "reused: "

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 232
    :goto_1
    sget-boolean v1, Lcom/google/android/libraries/curvular/bo;->c:Z

    if-eqz v1, :cond_1

    .line 233
    iget-object v1, p0, Lcom/google/android/libraries/curvular/bd;->h:Lcom/google/b/c/er;

    invoke-interface {v1, p1}, Lcom/google/b/c/er;->a(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    :cond_1
    new-instance v1, Lcom/google/android/libraries/curvular/ae;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/ae;-><init>(Landroid/view/View;)V

    move-object v0, v1

    goto :goto_0

    .line 230
    :cond_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b(Ljava/lang/Class;Landroid/view/View;)Z
    .locals 5
    .param p2    # Landroid/view/View;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 393
    if-nez p2, :cond_0

    .line 412
    :goto_0
    return v0

    .line 398
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    monitor-enter v1

    .line 399
    :try_start_0
    iget-object v2, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v2, p1}, Lcom/google/b/c/am;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 400
    monitor-exit v1

    goto :goto_0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 403
    :cond_1
    :try_start_1
    invoke-static {p2}, Lcom/google/android/libraries/curvular/cq;->e(Landroid/view/View;)Lcom/google/android/libraries/curvular/ay;

    move-result-object v2

    .line 404
    if-nez v2, :cond_2

    .line 405
    monitor-exit v1

    goto :goto_0

    .line 408
    :cond_2
    iget-object v3, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/b/c/am;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 409
    monitor-exit v1

    goto :goto_0

    .line 412
    :cond_3
    iget-object v3, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-interface {v3, v2}, Lcom/google/b/c/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v3, p1}, Lcom/google/b/c/am;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-ne v2, v3, :cond_4

    const/4 v0, 0x1

    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
