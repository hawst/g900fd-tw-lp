.class public final Lcom/google/android/libraries/curvular/bc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<*>;>;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+TT;>;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 168
    const-string v0, "Null layout provided"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 169
    :cond_0
    const-string v0, "Null viewModel provided"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170
    :cond_1
    new-instance v0, Lcom/google/b/a/an;

    invoke-direct {v0, p2, p1}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 172
    iget-object v1, p0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+TT;>;>;",
            "Ljava/util/List",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 177
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    .line 178
    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 180
    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 204
    instance-of v0, p1, Lcom/google/android/libraries/curvular/bc;

    if-eqz v0, :cond_0

    .line 205
    check-cast p1, Lcom/google/android/libraries/curvular/bc;

    iget-object v0, p1, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 207
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
