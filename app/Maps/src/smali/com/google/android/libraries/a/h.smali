.class public Lcom/google/android/libraries/a/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/a/a;


# instance fields
.field final a:Lcom/google/android/libraries/a/a;

.field b:Z

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/a/a;Landroid/os/Handler;Landroid/webkit/WebView;)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/a/h;->b:Z

    .line 65
    iput-object p1, p0, Lcom/google/android/libraries/a/h;->a:Lcom/google/android/libraries/a/a;

    .line 66
    iput-object p2, p0, Lcom/google/android/libraries/a/h;->c:Landroid/os/Handler;

    .line 67
    return-void
.end method


# virtual methods
.method public onSurveyCanceled()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/libraries/a/h;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/a/m;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/a/m;-><init>(Lcom/google/android/libraries/a/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 138
    return-void
.end method

.method public onSurveyComplete(ZZ)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/a/h;->b:Z

    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/a/h;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/a/k;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/libraries/a/k;-><init>(Lcom/google/android/libraries/a/h;ZZ)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 108
    return-void
.end method

.method public onSurveyReady()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/a/h;->b:Z

    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/a/h;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/a/j;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/a/j;-><init>(Lcom/google/android/libraries/a/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 96
    return-void
.end method

.method public onSurveyResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 118
    const-string v0, "t=a"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 127
    :goto_0
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/a/h;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/a/l;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/libraries/a/l;-><init>(Lcom/google/android/libraries/a/h;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onWindowError()V
    .locals 2
    .annotation runtime Landroid/webkit/JavascriptInterface;
    .end annotation

    .prologue
    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/a/h;->b:Z

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/a/h;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/libraries/a/i;

    invoke-direct {v1, p0}, Lcom/google/android/libraries/a/i;-><init>(Lcom/google/android/libraries/a/h;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 84
    return-void
.end method
