.class final Lcom/google/android/libraries/memorymonitor/h;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "PG"


# instance fields
.field a:F

.field b:F

.field final synthetic c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

.field private d:Landroid/widget/Toast;

.field private e:F


# direct methods
.method constructor <init>(Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/h;->d:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/h;->d:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    .line 445
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    invoke-virtual {v0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/memorymonitor/h;->d:Landroid/widget/Toast;

    .line 446
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/h;->d:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 447
    return-void
.end method

.method public final onDown(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 405
    invoke-static {}, Lcom/google/android/libraries/memorymonitor/b;->a()F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/h;->a:F

    .line 406
    iget v0, p0, Lcom/google/android/libraries/memorymonitor/h;->a:F

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    .line 407
    iget v0, p0, Lcom/google/android/libraries/memorymonitor/h;->a:F

    iget-object v1, p0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    .line 408
    iget-object v1, v1, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->h:Lcom/google/android/libraries/memorymonitor/b;

    iget-wide v2, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    long-to-double v2, v2

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-float v1, v2

    sget v2, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/h;->e:F

    .line 409
    const/4 v0, 0x1

    return v0
.end method

.method public final onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 414
    iget v0, p0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    iget-object v1, p0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    iget v1, v1, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->e:F

    div-float v1, p4, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    .line 415
    iget v0, p0, Lcom/google/android/libraries/memorymonitor/h;->e:F

    iget v1, p0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    .line 417
    const-string v0, "Target heap usage: %.2f%% (%.2f MB)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    iget v2, p0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    .line 418
    sget v3, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v1, v5

    .line 417
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/memorymonitor/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 419
    return v5
.end method
