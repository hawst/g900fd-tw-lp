.class public abstract Lcom/google/android/libraries/curvular/ay;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Lcom/google/android/libraries/curvular/ce;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/libraries/curvular/cq;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final n:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field o:Landroid/content/res/Configuration;

.field public p:Lcom/google/android/libraries/curvular/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/am",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public q:Lcom/google/android/libraries/curvular/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/am",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/libraries/curvular/ay;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/ay;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/libraries/curvular/ay;->b()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    iput-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    .line 162
    return-void
.end method

.method private static a(Ljava/lang/Class;)Ljava/lang/reflect/Type;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/reflect/Type;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lcom/google/android/libraries/curvular/ay;

    if-ne v0, v2, :cond_1

    .line 95
    invoke-virtual {p0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 96
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    .line 114
    :cond_0
    :goto_0
    return-object v0

    .line 98
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/ay;->a(Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    .line 100
    instance-of v2, v0, Ljava/lang/Class;

    if-nez v2, :cond_0

    .line 103
    instance-of v2, v0, Ljava/lang/reflect/TypeVariable;

    if-eqz v2, :cond_4

    .line 107
    check-cast v0, Ljava/lang/reflect/TypeVariable;

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v2

    .line 109
    invoke-virtual {p0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getTypeParameters()[Ljava/lang/reflect/TypeVariable;

    move-result-object v3

    .line 110
    :goto_1
    array-length v0, v3

    if-ge v1, v0, :cond_3

    .line 111
    aget-object v0, v3, v1

    invoke-interface {v0}, Ljava/lang/reflect/TypeVariable;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 113
    invoke-virtual {p0}, Ljava/lang/Class;->getGenericSuperclass()Ljava/lang/reflect/Type;

    move-result-object v0

    check-cast v0, Ljava/lang/reflect/ParameterizedType;

    .line 114
    invoke-interface {v0}, Ljava/lang/reflect/ParameterizedType;->getActualTypeArguments()[Ljava/lang/reflect/Type;

    move-result-object v0

    aget-object v0, v0, v1

    goto :goto_0

    .line 110
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 117
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to resolve type variable."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public static e(Landroid/content/res/Configuration;)Z
    .locals 2

    .prologue
    .line 312
    iget v0, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static k()I
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x4

    return v0
.end method

.method protected static m()Z
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method protected final a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Lcom/google/android/libraries/curvular/bc;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/libraries/curvular/bc;"
        }
    .end annotation

    .prologue
    .line 240
    new-instance v0, Lcom/google/android/libraries/curvular/bc;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bc;-><init>()V

    .line 241
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/libraries/curvular/ay;->a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V

    .line 242
    return-object v0
.end method

.method public abstract a()Lcom/google/android/libraries/curvular/cs;
.end method

.method public a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITT;",
            "Landroid/content/Context;",
            "Lcom/google/android/libraries/curvular/bc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 252
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "If layoutViewBinderListAdapter() is called, one of the createLayoutListAdapter methods needs to be overridden."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Ljava/lang/reflect/Type;
    .locals 10

    .prologue
    .line 69
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 70
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/ay;->a(Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v2

    .line 71
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 72
    sub-long v6, v4, v0

    const-wide/16 v8, 0x32

    cmp-long v3, v6, v8

    if-lez v3, :cond_0

    .line 73
    sget-object v3, Lcom/google/android/libraries/curvular/ay;->a:Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sub-long v0, v4, v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x49

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Default getViewModelType implementation for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " takes "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :cond_0
    return-object v2
.end method

.method public final i()Lcom/google/android/libraries/curvular/ce;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cq;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->o:Landroid/content/res/Configuration;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->b:Lcom/google/android/libraries/curvular/cq;

    if-nez v0, :cond_2

    .line 140
    invoke-virtual {p0}, Lcom/google/android/libraries/curvular/ay;->a()Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/cs;->a()Lcom/google/android/libraries/curvular/cq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/ay;->b:Lcom/google/android/libraries/curvular/cq;

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->b:Lcom/google/android/libraries/curvular/cq;

    return-object v0
.end method

.method public final l()Lcom/google/android/libraries/curvular/am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Lcom/google/android/libraries/curvular/bc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 215
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/libraries/curvular/az;

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/curvular/az;-><init>(Lcom/google/android/libraries/curvular/ay;I)V

    return-object v1
.end method

.method public final n()Lcom/google/android/libraries/curvular/am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/am",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    if-nez v0, :cond_0

    .line 293
    new-instance v0, Lcom/google/android/libraries/curvular/bb;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/bb;-><init>(Lcom/google/android/libraries/curvular/ay;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->q:Lcom/google/android/libraries/curvular/am;

    return-object v0
.end method

.method public final o()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 304
    iget-object v2, p0, Lcom/google/android/libraries/curvular/ay;->o:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v3, 0x258

    if-lt v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->o:Landroid/content/res/Configuration;

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
