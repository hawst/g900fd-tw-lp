.class public Lcom/google/android/libraries/curvular/br;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/curvular/am",
            "<**>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lcom/google/b/c/fd;

    invoke-direct {v0}, Lcom/google/b/c/fd;-><init>()V

    .line 24
    sget-object v1, Lcom/google/b/c/gr;->c:Lcom/google/b/c/gr;

    invoke-virtual {v0, v1}, Lcom/google/b/c/fd;->a(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lcom/google/b/c/fd;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/br;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 23
    return-void
.end method

.method public static a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 372
    new-instance v0, Lcom/google/android/libraries/curvular/bz;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/bz;-><init>(Lcom/google/android/libraries/curvular/ah;)V

    .line 378
    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    invoke-interface {p0}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 195
    :cond_1
    new-instance v0, Lcom/google/android/libraries/curvular/bx;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/curvular/bx;-><init>(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)V

    .line 201
    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<-TT;TV;>;",
            "Lcom/google/android/libraries/curvular/ah",
            "<-TT;TV;>;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;TV;>;"
        }
    .end annotation

    .prologue
    .line 140
    const-string v0, "ifThenElse thenProperty cannot be null"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 141
    :cond_0
    const-string v0, "ifThenElse elseProperty cannot be null"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 142
    :cond_1
    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :cond_3
    new-instance v0, Lcom/google/android/libraries/curvular/bw;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/libraries/curvular/bw;-><init>(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)V

    .line 153
    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v1
.end method

.method public static a(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 174
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/am;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v1, v0, p0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    .line 222
    invoke-static {v1, p1}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/am;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<-TT;TV;>;",
            "Lcom/google/android/libraries/curvular/ah",
            "<-TT;TV;>;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;TV;>;"
        }
    .end annotation

    .prologue
    .line 128
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v1, v0, p0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    .line 129
    invoke-static {v1, p1, p2}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/libraries/curvular/am;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 210
    new-instance v0, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    .line 211
    invoke-static {v1, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 212
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 363
    invoke-static {v2, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 364
    new-instance v1, Lcom/google/android/libraries/curvular/bz;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/bz;-><init>(Lcom/google/android/libraries/curvular/ah;)V

    new-instance v0, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v0, v2, v1}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Boolean;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;TV;>;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;TV;>;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;TV;>;"
        }
    .end annotation

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 117
    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-static {v1, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 163
    invoke-static {v1, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 164
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V::",
            "Lcom/google/android/libraries/curvular/ce;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;TT;>;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;TT;>;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/libraries/curvular/bt;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/curvular/bt;-><init>(Lcom/google/android/libraries/curvular/ah;)V

    .line 65
    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v1
.end method

.method public static a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V::",
            "Lcom/google/android/libraries/curvular/ce;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Lcom/google/android/libraries/curvular/am",
            "<TV;TT;>;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;TT;>;"
        }
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V::",
            "Lcom/google/android/libraries/curvular/ce;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;TT;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;TT;>;"
        }
    .end annotation

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/libraries/curvular/br;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/am;

    .line 83
    if-eqz v0, :cond_0

    .line 84
    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v1, p0, v0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    move-object v0, v1

    .line 104
    :goto_0
    return-object v0

    .line 85
    :cond_0
    invoke-static {p1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Class;

    invoke-static {p1, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v0

    .line 88
    new-instance v1, Lcom/google/android/libraries/curvular/bu;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/bu;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    .line 94
    new-instance v0, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    goto :goto_0

    .line 95
    :cond_1
    instance-of v0, p1, Lcom/google/android/libraries/curvular/ax;

    if-eqz v0, :cond_2

    .line 96
    new-instance v1, Lcom/google/android/libraries/curvular/bv;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/curvular/bv;-><init>(Ljava/lang/Object;)V

    .line 102
    new-instance v0, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    goto :goto_0

    .line 104
    :cond_2
    new-instance v0, Lcom/google/android/libraries/curvular/aj;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/curvular/aj;-><init>(Ljava/lang/Enum;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<V::",
            "Lcom/google/android/libraries/curvular/ce;",
            "T:",
            "Ljava/lang/Object;",
            ">(TT;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 406
    invoke-static {v2, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    new-instance v1, Lcom/google/android/libraries/curvular/ca;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/ca;-><init>(Lcom/google/android/libraries/curvular/ah;)V

    new-instance v0, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v0, v2, v1}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 37
    const/4 v0, 0x0

    .line 38
    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 40
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 41
    sget-object v2, Lcom/google/android/libraries/curvular/br;->a:Ljava/util/concurrent/ConcurrentMap;

    new-instance v3, Lcom/google/android/libraries/curvular/bs;

    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/bs;-><init>(Lcom/google/android/libraries/curvular/ah;)V

    invoke-interface {v2, v1, v3}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-object v1
.end method

.method public static varargs a([Lcom/google/android/libraries/curvular/cu;[Lcom/google/android/libraries/curvular/cu;)[Lcom/google/android/libraries/curvular/cu;
    .locals 1

    .prologue
    .line 399
    const-class v0, Lcom/google/android/libraries/curvular/cu;

    invoke-static {p0, p1, v0}, Lcom/google/b/c/in;->a([Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/curvular/cu;

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 294
    invoke-interface {p0}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 296
    :cond_1
    new-instance v0, Lcom/google/android/libraries/curvular/by;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/curvular/by;-><init>(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)V

    .line 302
    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    return-object v1
.end method

.method public static b(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 274
    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 275
    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/am;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    .line 322
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v1, v0, p0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    .line 323
    invoke-static {v1, p1}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/libraries/curvular/am;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Lcom/google/android/libraries/curvular/am",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 311
    new-instance v0, Lcom/google/android/libraries/curvular/ak;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/ak;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V

    .line 312
    invoke-static {v1, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 313
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Boolean;",
            "Ljava/lang/Boolean;",
            ")",
            "Lcom/google/android/libraries/curvular/ah",
            "<TT;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 263
    invoke-static {v1, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    .line 264
    invoke-static {v1, p1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    .line 265
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    return-object v0
.end method
