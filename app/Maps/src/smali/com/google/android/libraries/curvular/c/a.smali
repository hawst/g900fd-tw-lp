.class public final enum Lcom/google/android/libraries/curvular/c/a;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/libraries/curvular/c/a;",
        ">;",
        "Lcom/google/android/libraries/curvular/cn;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/libraries/curvular/c/a;

.field public static final enum b:Lcom/google/android/libraries/curvular/c/a;

.field public static final enum c:Lcom/google/android/libraries/curvular/c/a;

.field public static final enum d:Lcom/google/android/libraries/curvular/c/a;

.field public static final enum e:Lcom/google/android/libraries/curvular/c/a;

.field public static final enum f:Lcom/google/android/libraries/curvular/c/a;

.field public static final enum g:Lcom/google/android/libraries/curvular/c/a;

.field public static final enum h:Lcom/google/android/libraries/curvular/c/a;

.field private static final synthetic i:[Lcom/google/android/libraries/curvular/c/a;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 12
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "CARD_CORNER_RADIUS"

    const-string v2, "card_corner_radius"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->a:Lcom/google/android/libraries/curvular/c/a;

    .line 13
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "CARD_ELEVATION"

    const-string v2, "card_elevation"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->b:Lcom/google/android/libraries/curvular/c/a;

    .line 14
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "HAS_FIXED_SIZE"

    const-string v2, "has_fixed_size"

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->c:Lcom/google/android/libraries/curvular/c/a;

    .line 15
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "LAYOUT_MANAGER"

    const-string v2, "layout_manager"

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->d:Lcom/google/android/libraries/curvular/c/a;

    .line 16
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "MAX_CARD_ELEVATION"

    const-string v2, "max_card_elevation"

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->e:Lcom/google/android/libraries/curvular/c/a;

    .line 17
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "ON_SCROLL_LISTENER"

    const/4 v2, 0x5

    const-string v3, "on_scroll_listener"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->f:Lcom/google/android/libraries/curvular/c/a;

    .line 18
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "ON_VIEW_ATTACHED_TO_WINDOW"

    const/4 v2, 0x6

    const-string v3, "on_view_attached_to_window"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->g:Lcom/google/android/libraries/curvular/c/a;

    .line 19
    new-instance v0, Lcom/google/android/libraries/curvular/c/a;

    const-string v1, "USE_COMPAT_PADDING"

    const/4 v2, 0x7

    const-string v3, "use_compat_padding"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/curvular/c/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->h:Lcom/google/android/libraries/curvular/c/a;

    .line 10
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/libraries/curvular/c/a;

    sget-object v1, Lcom/google/android/libraries/curvular/c/a;->a:Lcom/google/android/libraries/curvular/c/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/libraries/curvular/c/a;->b:Lcom/google/android/libraries/curvular/c/a;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/libraries/curvular/c/a;->c:Lcom/google/android/libraries/curvular/c/a;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/libraries/curvular/c/a;->d:Lcom/google/android/libraries/curvular/c/a;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/libraries/curvular/c/a;->e:Lcom/google/android/libraries/curvular/c/a;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/libraries/curvular/c/a;->f:Lcom/google/android/libraries/curvular/c/a;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/libraries/curvular/c/a;->g:Lcom/google/android/libraries/curvular/c/a;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/libraries/curvular/c/a;->h:Lcom/google/android/libraries/curvular/c/a;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/curvular/c/a;->i:[Lcom/google/android/libraries/curvular/c/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 30
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/libraries/curvular/c/a;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/google/android/libraries/curvular/c/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/c/a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/libraries/curvular/c/a;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/libraries/curvular/c/a;->i:[Lcom/google/android/libraries/curvular/c/a;

    invoke-virtual {v0}, [Lcom/google/android/libraries/curvular/c/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/curvular/c/a;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/co;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/libraries/curvular/co;->a:Lcom/google/android/libraries/curvular/co;

    return-object v0
.end method
