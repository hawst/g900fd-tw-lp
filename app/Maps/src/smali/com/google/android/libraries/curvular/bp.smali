.class public Lcom/google/android/libraries/curvular/bp;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/widget/ListAdapter;


# instance fields
.field a:Lcom/google/android/libraries/curvular/bm;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 19
    new-instance v0, Lcom/google/android/libraries/curvular/bm;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/curvular/bm;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    .line 20
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bn;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bn;->b:Lcom/google/android/libraries/curvular/ce;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    invoke-static {p1}, Lcom/google/android/libraries/curvular/bm;->a(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/bm;->b(I)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 71
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/bp;->getItemViewType(I)I

    move-result v0

    .line 72
    iget-object v1, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v2, v1, Lcom/google/android/libraries/curvular/bm;->c:Lcom/google/android/libraries/curvular/bd;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/bm;->c(I)Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v2, v1, p2}, Lcom/google/android/libraries/curvular/bd;->b(Ljava/lang/Class;Landroid/view/View;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 73
    iget-object v1, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v2, v1, Lcom/google/android/libraries/curvular/bm;->c:Lcom/google/android/libraries/curvular/bd;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/bm;->c(I)Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v2, v0, p3, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object p2, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v0, p2, p1}, Lcom/google/android/libraries/curvular/bm;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget v1, v0, Lcom/google/android/libraries/curvular/bm;->d:I

    if-nez v1, :cond_0

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/libraries/curvular/bm;->a:Lcom/google/b/c/ak;

    invoke-interface {v2}, Lcom/google/b/c/ak;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/google/android/libraries/curvular/bm;->d:I

    :cond_0
    iget v0, v0, Lcom/google/android/libraries/curvular/bm;->d:I

    return v0
.end method
