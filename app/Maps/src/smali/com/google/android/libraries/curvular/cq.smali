.class public Lcom/google/android/libraries/curvular/cq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cv;


# static fields
.field static final n:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private static o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final p:Ljava/lang/String;

.field private static final q:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static r:Ljava/lang/reflect/Method;


# instance fields
.field a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field b:[Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "*>;"
        }
    .end annotation
.end field

.field c:[Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "*>;"
        }
    .end annotation
.end field

.field d:Z

.field e:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field g:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field h:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "*>;"
        }
    .end annotation
.end field

.field i:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/libraries/curvular/ct",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation
.end field

.field public j:[Lcom/google/android/libraries/curvular/cv;

.field public k:Lcom/google/android/libraries/curvular/ay;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation
.end field

.field l:Lcom/google/android/libraries/curvular/bd;

.field public final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/cq;->o:Ljava/util/List;

    .line 451
    const-class v0, Lcom/google/android/libraries/curvular/cq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/cq;->p:Ljava/lang/String;

    .line 491
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/libraries/curvular/cq;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 497
    :try_start_0
    const-class v0, Landroid/view/View;

    const-string v1, "generateViewId"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/cq;->r:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 504
    :goto_0
    sget-object v0, Lcom/google/android/libraries/curvular/cq;->r:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 506
    :try_start_1
    const-class v0, Lcom/google/android/libraries/curvular/cq;

    const-string v1, "c"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/cq;->r:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    .line 799
    :cond_0
    new-instance v0, Lcom/google/b/c/fd;

    invoke-direct {v0}, Lcom/google/b/c/fd;-><init>()V

    .line 801
    sget-object v1, Lcom/google/b/c/gr;->c:Lcom/google/b/c/gr;

    invoke-virtual {v0, v1}, Lcom/google/b/c/fd;->a(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;

    move-result-object v0

    .line 802
    invoke-virtual {v0}, Lcom/google/b/c/fd;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/cq;->n:Ljava/util/concurrent/ConcurrentMap;

    .line 799
    return-void

    .line 500
    :catch_0
    move-exception v0

    .line 501
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 507
    :catch_1
    move-exception v0

    .line 509
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 502
    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 461
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/cq;->d:Z

    .line 489
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/curvular/cq;->m:I

    .line 905
    return-void
.end method

.method public static a(Lcom/google/android/libraries/curvular/ce;)I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 915
    .line 917
    invoke-static {p0, v3}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;Z)Ljava/util/List;

    move-result-object v0

    .line 919
    if-eqz v0, :cond_1

    .line 924
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/ref/WeakReference;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/ref/WeakReference;

    .line 925
    array-length v5, v0

    move v4, v3

    move v2, v3

    :goto_0
    if-ge v4, v5, :cond_2

    aget-object v1, v0, v4

    .line 926
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 927
    if-eqz v1, :cond_0

    sget v6, Lcom/google/android/libraries/curvular/bh;->f:I

    invoke-virtual {v1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v6

    if-ne v6, p0, :cond_0

    .line 928
    add-int/lit8 v2, v2, 0x1

    .line 929
    const/4 v6, 0x0

    invoke-static {v1, p0, v6, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;Z)V

    :cond_0
    move v1, v2

    .line 925
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_0

    :cond_1
    move v2, v3

    .line 932
    :cond_2
    sget-object v0, Lcom/google/android/libraries/curvular/cq;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 939
    :cond_3
    return v2
.end method

.method private static a(Lcom/google/android/libraries/curvular/ce;Z)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/ce;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/view/View;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 812
    sget-object v0, Lcom/google/android/libraries/curvular/cq;->n:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 813
    if-nez v0, :cond_2

    if-eqz p1, :cond_2

    .line 814
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 815
    sget-object v1, Lcom/google/android/libraries/curvular/cq;->n:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p0, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    .line 819
    :goto_0
    if-eqz v2, :cond_1

    .line 820
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_1

    .line 821
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 822
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->isEnqueued()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 823
    invoke-interface {v2, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 820
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 828
    :cond_1
    return-object v2

    :cond_2
    move-object v2, v0

    goto :goto_0
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 406
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    sget-object v0, Lcom/google/android/libraries/curvular/cq;->n:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 407
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;ZZ)V

    goto :goto_1

    .line 409
    :cond_3
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 251
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    .line 253
    if-eqz v0, :cond_0

    move v1, v2

    :goto_0
    const-string v3, "Provided view is not a viewbinder view."

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 256
    :cond_1
    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->b:Lcom/google/android/libraries/curvular/ce;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;Z)V

    .line 257
    return-void
.end method

.method static a(Landroid/view/View;Lcom/google/android/libraries/curvular/ah;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 786
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    iput-object p1, v0, Lcom/google/android/libraries/curvular/cm;->d:Lcom/google/android/libraries/curvular/ah;

    .line 787
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 381
    invoke-static {p0, p1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 382
    if-eqz v0, :cond_0

    .line 383
    invoke-static {v0, v1, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;ZZ)V

    .line 385
    :cond_0
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;Ljava/util/Collection;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 752
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 754
    :cond_0
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    .line 755
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cq;->a:Lcom/google/android/libraries/curvular/bk;

    if-ne v0, p1, :cond_1

    .line 756
    invoke-interface {p2, p0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 759
    :cond_1
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_3

    .line 760
    check-cast p0, Landroid/view/ViewGroup;

    .line 761
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_3

    .line 762
    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, p1, p2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;Ljava/util/Collection;)V

    .line 761
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 754
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 765
    :cond_3
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;)V

    .line 261
    return-void
.end method

.method public static a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;)V
    .locals 2

    .prologue
    .line 273
    if-nez p1, :cond_1

    .line 274
    const-string v0, "viewModel is null. Use ViewPropertyBinder#unbindModel to unbind view models"

    .line 275
    sget-boolean v1, Lcom/google/android/libraries/curvular/bo;->a:Z

    if-eqz v1, :cond_0

    .line 276
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 278
    :cond_0
    sget-object v0, Lcom/google/android/libraries/curvular/cq;->p:Ljava/lang/String;

    .line 280
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;)V

    .line 285
    :goto_0
    return-void

    .line 282
    :cond_1
    invoke-static {p0, p1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 283
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;Z)V

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;Z)V
    .locals 9
    .param p2    # Lcom/google/android/libraries/curvular/ce;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 315
    const-string v0, "Can\'t bind to a null view."

    if-nez p0, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 316
    :cond_0
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    .line 318
    if-eqz p3, :cond_1

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/google/android/libraries/curvular/cm;->d:Lcom/google/android/libraries/curvular/ah;

    if-eqz v1, :cond_1

    .line 320
    if-eqz p1, :cond_9

    .line 325
    iget-object v1, v0, Lcom/google/android/libraries/curvular/cm;->d:Lcom/google/android/libraries/curvular/ah;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    .line 327
    :goto_0
    invoke-static {p0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    move-object p1, v1

    .line 330
    :cond_1
    instance-of v1, p0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    move-object v1, p0

    .line 331
    check-cast v1, Landroid/view/ViewGroup;

    .line 332
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    move v4, v5

    :goto_1
    if-ge v4, v6, :cond_3

    .line 333
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 337
    if-ne p2, p1, :cond_2

    move-object v2, v3

    :goto_2
    invoke-static {v7, p1, v2, v8}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;Z)V

    .line 332
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_2
    move-object v2, p2

    .line 337
    goto :goto_2

    .line 342
    :cond_3
    if-eqz v0, :cond_4

    .line 343
    iput-object p1, v0, Lcom/google/android/libraries/curvular/cm;->b:Lcom/google/android/libraries/curvular/ce;

    .line 346
    :cond_4
    if-eqz p2, :cond_5

    if-ne p2, p1, :cond_8

    .line 347
    :cond_5
    if-eqz p1, :cond_7

    .line 348
    if-eqz v0, :cond_6

    iget-object v3, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    .line 349
    :cond_6
    if-eqz v3, :cond_7

    .line 350
    iget-object v0, v3, Lcom/google/android/libraries/curvular/cq;->i:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_7

    .line 352
    iget-object v0, v3, Lcom/google/android/libraries/curvular/cq;->i:Lcom/google/android/libraries/curvular/ah;

    .line 353
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ct;

    .line 354
    invoke-interface {v0, p0, v3, p1}, Lcom/google/android/libraries/curvular/ct;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/cq;Lcom/google/android/libraries/curvular/ce;)V

    .line 359
    :cond_7
    invoke-static {p0, v5, v8}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;ZZ)V

    .line 361
    :cond_8
    return-void

    :cond_9
    move-object v1, p1

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Lcom/google/android/libraries/curvular/cm;[Lcom/google/android/libraries/curvular/ah;Z[Ljava/lang/Object;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/google/android/libraries/curvular/cm;",
            "[",
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "*>;Z[",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 628
    array-length v0, p2

    if-nez v0, :cond_1

    .line 696
    :cond_0
    return-void

    .line 632
    :cond_1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 633
    iget-object v5, p1, Lcom/google/android/libraries/curvular/cm;->b:Lcom/google/android/libraries/curvular/ce;

    .line 634
    iget-object v6, p1, Lcom/google/android/libraries/curvular/cm;->c:Lcom/google/android/libraries/curvular/cp;

    move v0, v1

    .line 636
    :goto_0
    array-length v2, p2

    if-ge v0, v2, :cond_0

    .line 637
    aget-object v7, p2, v0

    .line 643
    invoke-interface {v7, v5, v4}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v8

    .line 646
    if-eqz p3, :cond_7

    aget-object v2, p4, v0

    if-eq v8, v2, :cond_2

    if-eqz v8, :cond_4

    invoke-virtual {v8, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    move v2, v3

    :goto_1
    if-eqz v2, :cond_7

    move v2, v1

    .line 651
    :goto_2
    if-eqz v2, :cond_6

    .line 652
    if-eqz p4, :cond_3

    .line 658
    aput-object v8, p4, v0

    .line 664
    :cond_3
    :try_start_0
    invoke-interface {v7}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v2

    .line 665
    invoke-interface {v6, v2, v8, v5, p0}, Lcom/google/android/libraries/curvular/cp;->a(Ljava/lang/Enum;Ljava/lang/Object;Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)Z

    move-result v9

    if-nez v9, :cond_6

    .line 666
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Property declaration"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    .line 667
    invoke-interface {v7}, Lcom/google/android/libraries/curvular/ah;->b()[Ljava/lang/StackTraceElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Throwable;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 669
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 670
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "ViewPropertyBinder was unable to apply property after trying all possible appliers. This probably means you are trying to apply a property to a kind of view that doesn\'t support it.  [Property type = "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x19

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; value = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "; View type = "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 676
    throw v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 678
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 683
    :goto_3
    if-eqz v1, :cond_5

    .line 684
    sget-object v2, Lcom/google/android/libraries/curvular/cq;->p:Ljava/lang/String;

    .line 685
    sget-object v2, Lcom/google/android/libraries/curvular/cq;->p:Ljava/lang/String;

    .line 686
    invoke-virtual {v1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    goto :goto_3

    :cond_4
    move v2, v1

    .line 646
    goto/16 :goto_1

    .line 688
    :cond_5
    sget-object v1, Lcom/google/android/libraries/curvular/cq;->p:Ljava/lang/String;

    .line 690
    new-instance v1, Lcom/google/android/libraries/curvular/a/d;

    invoke-direct {v1, v0, v7, v8, p0}, Lcom/google/android/libraries/curvular/a/d;-><init>(Ljava/lang/Exception;Lcom/google/android/libraries/curvular/ah;Ljava/lang/Object;Landroid/view/View;)V

    .line 692
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/a/d;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 693
    throw v1

    .line 636
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_7
    move v2, v3

    goto/16 :goto_2
.end method

.method private static a(Landroid/view/View;ZZ)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 412
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    move-object v3, v0

    .line 414
    :goto_0
    if-eqz v3, :cond_0

    iget-object v0, v3, Lcom/google/android/libraries/curvular/cq;->h:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, v3, Lcom/google/android/libraries/curvular/cq;->h:Lcom/google/android/libraries/curvular/ah;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v0, v4, v2}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    .line 418
    :cond_0
    if-eqz p1, :cond_2

    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 419
    check-cast v0, Landroid/view/ViewGroup;

    .line 420
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    .line 421
    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-static {v6, p1, p2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;ZZ)V

    .line 420
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v3, v4

    .line 412
    goto :goto_0

    .line 425
    :cond_2
    if-eqz v3, :cond_3

    .line 426
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    .line 428
    iget-object v2, v0, Lcom/google/android/libraries/curvular/cm;->b:Lcom/google/android/libraries/curvular/ce;

    if-nez v2, :cond_4

    .line 430
    iput-object v4, v0, Lcom/google/android/libraries/curvular/cm;->e:[Ljava/lang/Object;

    .line 431
    iput v1, v0, Lcom/google/android/libraries/curvular/cm;->f:I

    .line 449
    :cond_3
    :goto_2
    return-void

    .line 435
    :cond_4
    iget-object v2, v0, Lcom/google/android/libraries/curvular/cm;->e:[Ljava/lang/Object;

    if-nez v2, :cond_5

    .line 436
    iget-object v2, v3, Lcom/google/android/libraries/curvular/cq;->c:[Lcom/google/android/libraries/curvular/ah;

    array-length v2, v2

    new-array v2, v2, [Ljava/lang/Object;

    iput-object v2, v0, Lcom/google/android/libraries/curvular/cm;->e:[Ljava/lang/Object;

    move p2, v1

    .line 440
    :cond_5
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    .line 441
    iget v4, v0, Lcom/google/android/libraries/curvular/cm;->f:I

    if-eq v2, v4, :cond_6

    .line 442
    iput v2, v0, Lcom/google/android/libraries/curvular/cm;->f:I

    .line 446
    :goto_3
    iget-object v2, v3, Lcom/google/android/libraries/curvular/cq;->c:[Lcom/google/android/libraries/curvular/ah;

    iget-object v3, v0, Lcom/google/android/libraries/curvular/cm;->e:[Ljava/lang/Object;

    invoke-static {p0, v0, v2, v1, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/cm;[Lcom/google/android/libraries/curvular/ah;Z[Ljava/lang/Object;)V

    goto :goto_2

    :cond_6
    move v1, p2

    goto :goto_3
.end method

.method public static b()I
    .locals 3

    .prologue
    .line 523
    :try_start_0
    sget-object v0, Lcom/google/android/libraries/curvular/cq;->r:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 524
    :catch_0
    move-exception v0

    .line 525
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 716
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 719
    :cond_0
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    .line 720
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cq;->a:Lcom/google/android/libraries/curvular/bk;

    if-ne v0, p1, :cond_2

    .line 735
    :goto_1
    return-object p0

    :cond_1
    move-object v0, v1

    .line 719
    goto :goto_0

    .line 725
    :cond_2
    instance-of v0, p0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_4

    .line 726
    check-cast p0, Landroid/view/ViewGroup;

    .line 727
    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v3, v0

    move-object v0, v1

    move v1, v3

    :goto_2
    if-ge v1, v2, :cond_5

    .line 728
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 729
    if-eqz v0, :cond_3

    move-object p0, v0

    .line 730
    goto :goto_1

    .line 727
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    move-object v0, v1

    :cond_5
    move-object p0, v0

    .line 735
    goto :goto_1
.end method

.method public static b(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 292
    invoke-static {p0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 293
    const/4 v0, 0x1

    invoke-static {p0, v1, v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;Z)V

    .line 294
    return-void
.end method

.method private static b(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 854
    sget v0, Lcom/google/android/libraries/curvular/bh;->f:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    .line 856
    if-ne v0, p1, :cond_0

    .line 895
    :goto_0
    return-void

    .line 862
    :cond_0
    if-eqz v0, :cond_1

    .line 863
    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;Z)Ljava/util/List;

    move-result-object v4

    .line 866
    if-eqz v4, :cond_1

    .line 867
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_1

    .line 868
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_3

    .line 869
    invoke-interface {v4, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 877
    :cond_1
    if-eqz p1, :cond_2

    .line 878
    invoke-static {p1, v1}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;Z)Ljava/util/List;

    move-result-object v4

    .line 883
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_5

    .line 884
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p0, :cond_4

    move v0, v1

    .line 889
    :goto_3
    if-nez v0, :cond_2

    .line 890
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 894
    :cond_2
    sget v0, Lcom/google/android/libraries/curvular/bh;->f:I

    invoke-virtual {p0, v0, p1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_0

    .line 867
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 883
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3
.end method

.method public static c()I
    .locals 3

    .prologue
    .line 533
    :cond_0
    sget-object v0, Lcom/google/android/libraries/curvular/cq;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    .line 535
    add-int/lit8 v0, v1, 0x1

    .line 536
    const v2, 0xffffff

    if-le v0, v2, :cond_1

    .line 537
    const/4 v0, 0x1

    .line 539
    :cond_1
    sget-object v2, Lcom/google/android/libraries/curvular/cq;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    return v1
.end method

.method static c(Landroid/view/View;)Lcom/google/android/libraries/curvular/cq;
    .locals 1

    .prologue
    .line 372
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    .line 373
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/view/View;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 391
    invoke-static {p0, v0, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;ZZ)V

    .line 392
    return-void
.end method

.method public static e(Landroid/view/View;)Lcom/google/android/libraries/curvular/ay;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            ")",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 772
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    .line 773
    :goto_0
    if-nez v0, :cond_1

    move-object v0, v1

    .line 776
    :goto_1
    return-object v0

    :cond_0
    move-object v0, v1

    .line 772
    goto :goto_0

    .line 776
    :cond_1
    iget-object v0, v0, Lcom/google/android/libraries/curvular/cq;->k:Lcom/google/android/libraries/curvular/ay;

    goto :goto_1
.end method

.method public static f(Landroid/view/View;)Lcom/google/android/libraries/curvular/ce;
    .locals 1

    .prologue
    .line 780
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    .line 781
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->b:Lcom/google/android/libraries/curvular/ce;

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/curvular/bd;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 555
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cq;->f:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_1

    .line 556
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cq;->f:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v0, v2, v2}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 557
    iget-object v1, p0, Lcom/google/android/libraries/curvular/cq;->g:Lcom/google/android/libraries/curvular/ah;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    .line 558
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 559
    :goto_0
    iget-object v2, p1, Lcom/google/android/libraries/curvular/bd;->b:Lcom/google/android/libraries/curvular/cc;

    invoke-interface {v2, v0, v1, p2, p3}, Lcom/google/android/libraries/curvular/cc;->a(Ljava/lang/String;Ljava/lang/Integer;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 567
    :goto_1
    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/bd;Landroid/view/View;)V

    .line 568
    return-object v0

    .line 558
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/curvular/cq;->g:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v1, v2, v2}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    goto :goto_0

    .line 560
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cq;->e:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_2

    .line 561
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cq;->e:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v0, v2, v2}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 562
    iget-object v1, p1, Lcom/google/android/libraries/curvular/bd;->b:Lcom/google/android/libraries/curvular/cc;

    invoke-interface {v1, v0, p2, p3}, Lcom/google/android/libraries/curvular/cc;->a(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 564
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method final a(Lcom/google/android/libraries/curvular/bd;Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 575
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    :goto_0
    if-nez v0, :cond_1

    move v0, v4

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move-object v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    .line 577
    :cond_2
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/libraries/curvular/cm;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/cm;-><init>()V

    sget v3, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p2, v3, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 578
    :cond_3
    iput-object p0, v0, Lcom/google/android/libraries/curvular/cm;->a:Lcom/google/android/libraries/curvular/cq;

    .line 579
    iget-object v3, p1, Lcom/google/android/libraries/curvular/bd;->c:Lcom/google/android/libraries/curvular/cp;

    iput-object v3, v0, Lcom/google/android/libraries/curvular/cm;->c:Lcom/google/android/libraries/curvular/cp;

    .line 582
    iget-object v3, p0, Lcom/google/android/libraries/curvular/cq;->b:[Lcom/google/android/libraries/curvular/ah;

    invoke-static {p2, v0, v3, v1, v2}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/cm;[Lcom/google/android/libraries/curvular/ah;Z[Ljava/lang/Object;)V

    .line 588
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_4

    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/cq;->d:Z

    if-nez v0, :cond_4

    .line 590
    iget v0, p0, Lcom/google/android/libraries/curvular/cq;->m:I

    invoke-virtual {p2, v0}, Landroid/view/View;->setId(I)V

    .line 596
    invoke-virtual {p2, v1}, Landroid/view/View;->setSaveEnabled(Z)V

    .line 599
    :cond_4
    iget-object v5, p0, Lcom/google/android/libraries/curvular/cq;->j:[Lcom/google/android/libraries/curvular/cv;

    array-length v6, v5

    move v3, v1

    :goto_2
    if-ge v3, v6, :cond_9

    aget-object v0, v5, v3

    .line 601
    instance-of v1, v0, Lcom/google/android/libraries/curvular/ao;

    if-eqz v1, :cond_5

    move-object v1, v0

    .line 603
    check-cast v1, Lcom/google/android/libraries/curvular/ao;

    .line 605
    iget-object v2, v1, Lcom/google/android/libraries/curvular/ao;->a:Ljava/lang/Class;

    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p1, v2, v0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 606
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {v2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ao;->b:Lcom/google/android/libraries/curvular/ah;

    iput-object v1, v0, Lcom/google/android/libraries/curvular/cm;->d:Lcom/google/android/libraries/curvular/ah;

    move-object v1, v2

    .line 613
    :goto_3
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p2, :cond_8

    .line 614
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-nez v0, :cond_7

    .line 615
    new-instance v0, Lcom/google/android/libraries/curvular/a/g;

    .line 616
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit16 v3, v3, 0x9c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not add child View to View of type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - this is not a ViewGroup. Check your brackets. You may be including a member of a ViewGroup in a previous member."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/a/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 607
    :cond_5
    instance-of v1, v0, Lcom/google/android/libraries/curvular/cq;

    if-eqz v1, :cond_6

    .line 608
    check-cast v0, Lcom/google/android/libraries/curvular/cq;

    move-object v1, p2

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, v1, v4}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/bd;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_3

    .line 611
    :cond_6
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Internal error, child type not supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object v0, p2

    .line 619
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 599
    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_2

    .line 622
    :cond_9
    return-void
.end method
