.class public abstract Lcom/google/android/libraries/curvular/ab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/au;
.implements Lcom/google/android/libraries/curvular/ax;


# instance fields
.field public final a:[Ljava/lang/Object;


# direct methods
.method public constructor <init>([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, [Ljava/lang/Object;

    iput-object p1, p0, Lcom/google/android/libraries/curvular/ab;->a:[Ljava/lang/Object;

    .line 23
    return-void
.end method


# virtual methods
.method public final b_(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/ab;->a(Landroid/content/Context;)F

    move-result v0

    float-to-int v0, v0

    return v0
.end method

.method public final c_(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/ab;->a(Landroid/content/Context;)F

    move-result v1

    .line 33
    const/high16 v0, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 34
    if-eqz v0, :cond_0

    .line 37
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 44
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ab;->a:[Ljava/lang/Object;

    check-cast p1, Lcom/google/android/libraries/curvular/ab;

    iget-object v1, p1, Lcom/google/android/libraries/curvular/ab;->a:[Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    .line 47
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ab;->a:[Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
