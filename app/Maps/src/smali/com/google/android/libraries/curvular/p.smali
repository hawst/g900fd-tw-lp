.class Lcom/google/android/libraries/curvular/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/google/android/libraries/curvular/h;

.field final synthetic c:Z


# direct methods
.method constructor <init>(Lcom/google/android/libraries/curvular/j;Landroid/view/View;Lcom/google/android/libraries/curvular/h;Z)V
    .locals 0

    .prologue
    .line 2200
    iput-object p2, p0, Lcom/google/android/libraries/curvular/p;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/google/android/libraries/curvular/p;->b:Lcom/google/android/libraries/curvular/h;

    iput-boolean p4, p0, Lcom/google/android/libraries/curvular/p;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2203
    iget-object v0, p0, Lcom/google/android/libraries/curvular/p;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2204
    iget-object v0, p0, Lcom/google/android/libraries/curvular/p;->a:Landroid/view/View;

    sget v2, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    sget-object v2, Lcom/google/android/libraries/curvular/j;->b:Lcom/google/android/libraries/curvular/bk;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    .line 2205
    iget-object v2, p0, Lcom/google/android/libraries/curvular/p;->b:Lcom/google/android/libraries/curvular/h;

    iget-object v3, p0, Lcom/google/android/libraries/curvular/p;->a:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/p;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v2, v3, v0}, Lcom/google/android/libraries/curvular/h;->a(Landroid/view/View;Z)V

    .line 2206
    return v1

    .line 2205
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
