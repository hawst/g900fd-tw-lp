.class Lcom/google/android/libraries/curvular/b/a;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/reflect/InvocationHandler;


# instance fields
.field final a:I

.field final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field c:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/libraries/curvular/b/b;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/b/b;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/b/a;->e:Ljava/lang/reflect/InvocationHandler;

    return-void
.end method

.method constructor <init>(ILjava/lang/Class;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lcom/google/android/libraries/curvular/b/a;->a:I

    .line 34
    iput-object p2, p0, Lcom/google/android/libraries/curvular/b/a;->b:Ljava/lang/Class;

    .line 35
    invoke-virtual {p2}, Ljava/lang/Class;->isInterface()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    .line 38
    invoke-virtual {p2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    .line 39
    sget-object v2, Lcom/google/android/libraries/curvular/b/a;->e:Ljava/lang/reflect/InvocationHandler;

    invoke-static {v1, v0, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/b/a;->c:Ljava/lang/Object;

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-static {p2}, Lcom/google/android/libraries/curvular/b/d;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/b/a;->c:Ljava/lang/Object;

    goto :goto_0
.end method
