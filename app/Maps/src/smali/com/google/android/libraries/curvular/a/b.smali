.class public Lcom/google/android/libraries/curvular/a/b;
.super Lcom/google/android/libraries/curvular/a/g;
.source "PG"


# direct methods
.method public constructor <init>(Ljava/lang/Class;ILjava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;I",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 10
    const-string v0, "A place holder of type <%s> was used for parameter <%s>,  however the parameter given for this index is of the incompatible type <%s>"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    .line 13
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    .line 10
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/curvular/a/g;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method
