.class public Lcom/google/android/libraries/curvular/ap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/aw;
.implements Lcom/google/android/libraries/curvular/ax;


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/aw;

.field private final b:Lcom/google/android/libraries/curvular/aq;

.field private final c:Landroid/graphics/PorterDuff$Mode;

.field private d:Landroid/graphics/ColorFilter;

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/libraries/curvular/ap;->a:Lcom/google/android/libraries/curvular/aw;

    .line 28
    iput-object p2, p0, Lcom/google/android/libraries/curvular/ap;->b:Lcom/google/android/libraries/curvular/aq;

    .line 29
    iput-object p3, p0, Lcom/google/android/libraries/curvular/ap;->c:Landroid/graphics/PorterDuff$Mode;

    .line 30
    return-void
.end method


# virtual methods
.method public final d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ap;->a:Lcom/google/android/libraries/curvular/aw;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 35
    if-nez v0, :cond_0

    .line 36
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    .line 38
    :cond_0
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 39
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ap;->b:Lcom/google/android/libraries/curvular/aq;

    invoke-interface {v1, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/curvular/ap;->d:Landroid/graphics/ColorFilter;

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/libraries/curvular/ap;->e:I

    if-eq v1, v2, :cond_2

    :cond_1
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    iget-object v3, p0, Lcom/google/android/libraries/curvular/ap;->c:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v1, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    iput-object v2, p0, Lcom/google/android/libraries/curvular/ap;->d:Landroid/graphics/ColorFilter;

    iput v1, p0, Lcom/google/android/libraries/curvular/ap;->e:I

    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/curvular/ap;->d:Landroid/graphics/ColorFilter;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method
