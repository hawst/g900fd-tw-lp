.class public Lcom/google/android/libraries/curvular/cm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/libraries/curvular/cq;

.field b:Lcom/google/android/libraries/curvular/ce;

.field c:Lcom/google/android/libraries/curvular/cp;

.field d:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;"
        }
    .end annotation
.end field

.field e:[Ljava/lang/Object;

.field f:I

.field g:Z

.field public h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/libraries/curvular/bk",
            "<*>;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cm;
    .locals 1

    .prologue
    .line 66
    sget v0, Lcom/google/android/libraries/curvular/bh;->g:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cm;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/curvular/bk;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/libraries/curvular/bk",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 91
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/libraries/curvular/bk",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 99
    if-eqz p2, :cond_2

    .line 101
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 102
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    :cond_1
    :goto_0
    return-void

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
