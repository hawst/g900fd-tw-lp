.class public Lcom/google/android/libraries/curvular/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/au;


# instance fields
.field private final a:Landroid/util/TypedValue;


# direct methods
.method public constructor <init>(Landroid/util/TypedValue;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/libraries/curvular/b;->a:Landroid/util/TypedValue;

    .line 19
    return-void
.end method

.method public static a(III)Landroid/util/TypedValue;
    .locals 3

    .prologue
    .line 92
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 93
    shl-int/lit8 v1, p1, 0x4

    or-int/2addr v1, p0

    const v2, 0xffffff

    and-int/2addr v2, p2

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    iput v1, v0, Landroid/util/TypedValue;->data:I

    .line 96
    return-object v0
.end method

.method public static a(D)Lcom/google/android/libraries/curvular/b;
    .locals 6

    .prologue
    const v4, 0xffffff

    .line 61
    new-instance v1, Lcom/google/android/libraries/curvular/b;

    invoke-static {p0, p1}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v2, p0

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v4

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    return-object v1

    :cond_0
    const-wide/high16 v2, 0x4060000000000000L    # 128.0

    mul-double/2addr v2, p0

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v2, v3, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v2

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v2, v4

    shl-int/lit8 v2, v2, 0x8

    or-int/lit8 v2, v2, 0x11

    iput v2, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)F
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b;->a:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 34
    invoke-static {v0, v1}, Landroid/util/TypedValue;->complexToDimension(ILandroid/util/DisplayMetrics;)F

    move-result v0

    return v0
.end method

.method public final b_(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b;->a:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 40
    invoke-static {v0, v1}, Landroid/util/TypedValue;->complexToDimensionPixelOffset(ILandroid/util/DisplayMetrics;)I

    move-result v0

    return v0
.end method

.method public final c_(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b;->a:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 46
    invoke-static {v0, v1}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 23
    instance-of v0, p1, Lcom/google/android/libraries/curvular/b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/libraries/curvular/b;

    iget-object v0, p1, Lcom/google/android/libraries/curvular/b;->a:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    iget-object v1, p0, Lcom/google/android/libraries/curvular/b;->a:Landroid/util/TypedValue;

    iget v1, v1, Landroid/util/TypedValue;->data:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b;->a:Landroid/util/TypedValue;

    iget v0, v0, Landroid/util/TypedValue;->data:I

    return v0
.end method
