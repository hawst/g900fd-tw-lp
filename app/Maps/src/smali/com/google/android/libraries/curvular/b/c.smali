.class Lcom/google/android/libraries/curvular/b/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/b/i;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/libraries/curvular/b/i",
        "<TT;TV;>;"
    }
.end annotation


# static fields
.field static final a:Lcom/google/android/libraries/curvular/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/c",
            "<**>;"
        }
    .end annotation
.end field


# instance fields
.field b:Lcom/google/android/libraries/curvular/b/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/c",
            "<",
            "Ljava/lang/Object;",
            "TT;>;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/reflect/Method;

.field private d:[Ljava/lang/Object;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private e:[I
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/libraries/curvular/b/c;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/b/c;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/b/c;->a:Lcom/google/android/libraries/curvular/b/c;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method constructor <init>(Ljava/lang/reflect/Method;[Ljava/lang/Object;Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Method;",
            "[",
            "Ljava/lang/Object;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/curvular/b/a",
            "<*>;>;)V"
        }
    .end annotation

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/google/android/libraries/curvular/b/c;->c:Ljava/lang/reflect/Method;

    .line 86
    iput-object p2, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    .line 87
    invoke-direct {p0, p3}, Lcom/google/android/libraries/curvular/b/c;->a(Ljava/util/Map;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    .line 88
    return-void
.end method

.method private a(Ljava/util/Map;)[I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/curvular/b/a",
            "<*>;>;)[I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 103
    iget-object v1, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-object v0

    .line 108
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->c:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v6

    .line 115
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    array-length v0, v0

    new-array v3, v0, [I

    move v1, v2

    .line 116
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 118
    const/4 v0, -0x1

    aput v0, v3, v1

    .line 120
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 124
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/b/a;

    .line 125
    if-eqz v0, :cond_5

    .line 126
    iget-object v4, p0, Lcom/google/android/libraries/curvular/b/c;->c:Ljava/lang/reflect/Method;

    aget-object v7, v6, v1

    iget-object v8, v0, Lcom/google/android/libraries/curvular/b/a;->b:Ljava/lang/Class;

    invoke-virtual {v7, v8}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v7

    const-string v8, "Can\'t use argument place holder of type <%s> for the (%s)-th argument of method <%s>."

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    iget-object v10, v0, Lcom/google/android/libraries/curvular/b/a;->b:Ljava/lang/Class;

    aput-object v10, v9, v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v5

    const/4 v10, 0x2

    aput-object v4, v9, v10

    if-nez v7, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v8, v9}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_2
    iget-boolean v4, v0, Lcom/google/android/libraries/curvular/b/a;->d:Z

    if-nez v4, :cond_3

    move v4, v5

    :goto_2
    if-nez v4, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v4, v2

    goto :goto_2

    :cond_4
    iput-boolean v5, v0, Lcom/google/android/libraries/curvular/b/a;->d:Z

    .line 128
    iget v0, v0, Lcom/google/android/libraries/curvular/b/a;->a:I

    aput v0, v3, v1

    .line 116
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_6
    move-object v0, v3

    .line 133
    goto :goto_0
.end method


# virtual methods
.method public final varargs a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;[",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 175
    sget-object v0, Lcom/google/android/libraries/curvular/b/c;->a:Lcom/google/android/libraries/curvular/b/c;

    if-ne p0, v0, :cond_0

    .line 190
    :goto_0
    return-object p1

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->b:Lcom/google/android/libraries/curvular/b/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/libraries/curvular/b/c;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .line 181
    :try_start_0
    iget-object v1, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    if-eqz v5, :cond_1

    if-eqz p2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    const-string v3, "This method expected argument substitutions, but was not provided with any."

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 183
    :catch_0
    move-exception v0

    .line 184
    new-instance v1, Lcom/google/android/libraries/curvular/a/f;

    invoke-direct {v1, v0, p0}, Lcom/google/android/libraries/curvular/a/f;-><init>(Ljava/lang/reflect/InvocationTargetException;Lcom/google/android/libraries/curvular/b/i;)V

    throw v1

    :cond_2
    move v0, v2

    .line 181
    goto :goto_1

    :cond_3
    if-eqz v1, :cond_4

    if-eqz v5, :cond_4

    if-nez p2, :cond_6

    :cond_4
    move-object v0, v1

    .line 182
    :cond_5
    :try_start_1
    iget-object v1, p0, Lcom/google/android/libraries/curvular/b/c;->c:Ljava/lang/reflect/Method;

    invoke-virtual {v1, v4, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    .line 181
    :cond_6
    array-length v0, v1

    new-array v0, v0, [Ljava/lang/Object;

    move v3, v2

    :goto_2
    array-length v2, v0

    if-ge v3, v2, :cond_5

    aget v2, v5, v3

    const/4 v6, -0x1

    if-ne v2, v6, :cond_7

    aget-object v2, v1, v3

    :goto_3
    aput-object v2, v0, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_7
    aget v2, v5, v3

    aget-object v2, p2, v2
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    .line 185
    :catch_1
    move-exception v0

    .line 186
    new-instance v1, Lcom/google/android/libraries/curvular/a/f;

    invoke-direct {v1, v0, p0}, Lcom/google/android/libraries/curvular/a/f;-><init>(Ljava/lang/Exception;Lcom/google/android/libraries/curvular/b/i;)V

    throw v1
.end method

.method final a([Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/libraries/curvular/b/c;->a:Lcom/google/android/libraries/curvular/b/c;

    if-ne p0, v0, :cond_1

    .line 169
    :cond_0
    return-void

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->b:Lcom/google/android/libraries/curvular/b/c;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/b/c;->a([Ljava/lang/Class;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    array-length v0, v0

    if-eqz v0, :cond_0

    .line 147
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->c:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v1

    .line 152
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 153
    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 154
    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    aget v2, v2, v0

    .line 160
    array-length v3, p1

    if-lt v2, v3, :cond_2

    .line 161
    new-instance v1, Lcom/google/android/libraries/curvular/a/c;

    array-length v2, p1

    invoke-direct {v1, v0, v2}, Lcom/google/android/libraries/curvular/a/c;-><init>(II)V

    throw v1

    .line 164
    :cond_2
    aget-object v2, p1, v2

    .line 165
    aget-object v3, v1, v0

    invoke-virtual {v3, v2}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 166
    new-instance v3, Lcom/google/android/libraries/curvular/a/b;

    aget-object v1, v1, v0

    invoke-direct {v3, v2, v0, v1}, Lcom/google/android/libraries/curvular/a/b;-><init>(Ljava/lang/Class;ILjava/lang/Class;)V

    throw v3

    .line 152
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 226
    sget-object v0, Lcom/google/android/libraries/curvular/b/c;->a:Lcom/google/android/libraries/curvular/b/c;

    if-ne p0, v0, :cond_0

    .line 227
    const-string v0, ""

    .line 250
    :goto_0
    return-object v0

    .line 230
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->b:Lcom/google/android/libraries/curvular/b/c;

    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 231
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    const-string v0, "."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 235
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/b/c;->c:Ljava/lang/reflect/Method;

    invoke-virtual {v0}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, "("

    .line 236
    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 237
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 238
    if-lez v0, :cond_2

    .line 239
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 241
    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    aget v2, v2, v0

    const/4 v3, -0x1

    if-eq v2, v3, :cond_3

    .line 242
    const-string v2, "arg<"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/curvular/b/c;->e:[I

    aget v3, v3, v0

    .line 243
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, ">"

    .line 244
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 237
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 246
    :cond_3
    iget-object v2, p0, Lcom/google/android/libraries/curvular/b/c;->d:[Ljava/lang/Object;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 249
    :cond_4
    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 250
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
