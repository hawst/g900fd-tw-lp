.class public Lcom/google/android/libraries/curvular/c/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/c/k;


# static fields
.field private static final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/support/v7/widget/RecyclerView;",
            "Lcom/google/android/libraries/curvular/c/i;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Lcom/google/android/libraries/curvular/c/k;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/google/b/c/fd;

    invoke-direct {v0}, Lcom/google/b/c/fd;-><init>()V

    .line 21
    sget-object v1, Lcom/google/b/c/gr;->c:Lcom/google/b/c/gr;

    invoke-virtual {v0, v1}, Lcom/google/b/c/fd;->a(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lcom/google/b/c/fd;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/c/i;->b:Ljava/util/Map;

    .line 20
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final a(Landroid/support/v7/widget/RecyclerView;)Lcom/google/android/libraries/curvular/c/i;
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/libraries/curvular/c/i;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/c/i;

    .line 28
    if-nez v0, :cond_1

    .line 29
    new-instance v1, Lcom/google/android/libraries/curvular/c/i;

    invoke-direct {v1}, Lcom/google/android/libraries/curvular/c/i;-><init>()V

    .line 30
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->a()Landroid/support/v7/widget/bk;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/libraries/curvular/c/j;

    if-eqz v2, :cond_0

    check-cast v0, Lcom/google/android/libraries/curvular/c/j;

    iput-object v1, v0, Lcom/google/android/libraries/curvular/c/j;->d:Lcom/google/android/libraries/curvular/c/k;

    .line 31
    :cond_0
    sget-object v0, Lcom/google/android/libraries/curvular/c/i;->b:Ljava/util/Map;

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v0, v1

    .line 33
    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;",
            "Lcom/google/android/libraries/curvular/ce;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/i;->a:Lcom/google/android/libraries/curvular/c/k;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/i;->a:Lcom/google/android/libraries/curvular/c/k;

    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/curvular/c/k;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 55
    :cond_0
    return-void
.end method
