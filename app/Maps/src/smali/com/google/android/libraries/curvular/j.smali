.class public Lcom/google/android/libraries/curvular/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cp;


# static fields
.field public static final a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field static final b:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Landroid/view/ViewTreeObserver$OnPreDrawListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Ljava/lang/String;


# instance fields
.field c:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private final h:Lcom/google/android/libraries/curvular/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/google/android/libraries/curvular/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/curvular/j;->d:Ljava/lang/String;

    .line 75
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/j;->a:Lcom/google/android/libraries/curvular/bk;

    .line 78
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/j;->b:Lcom/google/android/libraries/curvular/bk;

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;)V
    .locals 4

    .prologue
    const/16 v3, 0x11

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/j;->e:Z

    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/j;->f:Z

    .line 83
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/j;->g:Z

    .line 86
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/libraries/curvular/j;->c:Z

    .line 204
    iput-object p1, p0, Lcom/google/android/libraries/curvular/j;->h:Lcom/google/android/libraries/curvular/bd;

    .line 205
    return-void

    :cond_0
    move v0, v2

    .line 80
    goto :goto_0

    :cond_1
    move v0, v2

    .line 81
    goto :goto_1

    :cond_2
    move v0, v2

    .line 83
    goto :goto_2

    :cond_3
    move v1, v2

    .line 86
    goto :goto_3
.end method

.method private a(Landroid/widget/TextView;)F
    .locals 2

    .prologue
    .line 922
    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/j;->c:Z

    if-eqz v0, :cond_0

    .line 925
    :try_start_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineSpacingExtra()F
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 930
    :goto_0
    return v0

    .line 927
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/j;->c:Z

    .line 930
    :cond_0
    sget v0, Lcom/google/android/libraries/curvular/bh;->c:I

    iget-boolean v1, p0, Lcom/google/android/libraries/curvular/j;->c:Z

    if-nez v1, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 2259
    instance-of v0, p0, Lcom/google/android/libraries/curvular/bi;

    if-eqz v0, :cond_0

    .line 2260
    check-cast p0, Lcom/google/android/libraries/curvular/bi;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/bi;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    .line 2266
    :goto_0
    return-object p0

    .line 2261
    :cond_0
    instance-of v0, p0, Ljava/lang/CharSequence;

    if-eqz v0, :cond_1

    .line 2262
    check-cast p0, Ljava/lang/CharSequence;

    goto :goto_0

    .line 2263
    :cond_1
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2264
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 2265
    :cond_2
    if-nez p0, :cond_3

    .line 2266
    const/4 p0, 0x0

    goto :goto_0

    .line 2268
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 2269
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1c

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to resolve String: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Ljava/lang/Object;Landroid/widget/TextView;I)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 795
    invoke-virtual {p1}, Landroid/widget/TextView;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 796
    invoke-static {p0, p1}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, p2

    .line 797
    aget-object v1, v0, p2

    if-eqz v1, :cond_0

    .line 798
    aget-object v1, v0, p2

    aget-object v2, v0, p2

    .line 799
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    aget-object v3, v0, p2

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 798
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 801
    :cond_0
    aget-object v1, v0, v4

    aget-object v2, v0, v5

    const/4 v3, 0x2

    aget-object v3, v0, v3

    const/4 v4, 0x3

    aget-object v0, v0, v4

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 802
    return v5
.end method

.method private b(Landroid/widget/TextView;)F
    .locals 2

    .prologue
    .line 949
    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/j;->c:Z

    if-eqz v0, :cond_0

    .line 952
    :try_start_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getLineSpacingMultiplier()F
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 957
    :goto_0
    return v0

    .line 954
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/j;->c:Z

    .line 957
    :cond_0
    sget v0, Lcom/google/android/libraries/curvular/bh;->d:I

    iget-boolean v1, p0, Lcom/google/android/libraries/curvular/j;->c:Z

    if-nez v1, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public static b(Ljava/lang/Object;Landroid/view/View;)I
    .locals 4

    .prologue
    .line 2304
    instance-of v0, p0, Lcom/google/android/libraries/curvular/aq;

    if-eqz v0, :cond_0

    .line 2305
    check-cast p0, Lcom/google/android/libraries/curvular/aq;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    .line 2309
    :goto_0
    return v0

    .line 2306
    :cond_0
    instance-of v0, p0, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 2307
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0

    .line 2308
    :cond_1
    if-nez p0, :cond_2

    .line 2309
    const/4 v0, 0x0

    goto :goto_0

    .line 2311
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to resolve color: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Ljava/lang/Object;Landroid/widget/TextView;I)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 806
    invoke-virtual {p1}, Landroid/widget/TextView;->getCompoundDrawablesRelative()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 807
    invoke-static {p0, p1}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aput-object v1, v0, p2

    .line 808
    aget-object v1, v0, p2

    if-eqz v1, :cond_0

    .line 809
    aget-object v1, v0, p2

    aget-object v2, v0, p2

    .line 810
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    aget-object v3, v0, p2

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 809
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 812
    :cond_0
    aget-object v1, v0, v4

    aget-object v2, v0, v5

    const/4 v3, 0x2

    aget-object v3, v0, v3

    const/4 v4, 0x3

    aget-object v0, v0, v4

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 813
    return v5
.end method

.method public static c(Ljava/lang/Object;Landroid/view/View;)Landroid/content/res/ColorStateList;
    .locals 4

    .prologue
    .line 2316
    instance-of v0, p0, Lcom/google/android/libraries/curvular/aq;

    if-eqz v0, :cond_0

    .line 2317
    check-cast p0, Lcom/google/android/libraries/curvular/aq;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/aq;->c(Landroid/content/Context;)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 2321
    :goto_0
    return-object v0

    .line 2318
    :cond_0
    instance-of v0, p0, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 2319
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    .line 2320
    :cond_1
    if-nez p0, :cond_2

    .line 2321
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    .line 2323
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to resolve color state list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static d(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 604
    if-nez p0, :cond_0

    .line 605
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 620
    :goto_0
    return v0

    .line 607
    :cond_0
    instance-of v2, p0, Lcom/google/android/libraries/curvular/aw;

    if-eqz v2, :cond_1

    .line 608
    check-cast p0, Lcom/google/android/libraries/curvular/aw;

    .line 609
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 608
    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 611
    :cond_1
    instance-of v2, p0, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    .line 612
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 615
    :cond_2
    invoke-static {p0, p1}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 616
    if-eqz v2, :cond_3

    .line 617
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 620
    goto :goto_0
.end method

.method private static e(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 739
    instance-of v1, p0, Lcom/google/android/libraries/curvular/bk;

    if-eqz v1, :cond_0

    instance-of v1, p1, Landroid/widget/ViewSwitcher;

    if-eqz v1, :cond_0

    .line 741
    check-cast p0, Lcom/google/android/libraries/curvular/bk;

    .line 742
    check-cast p1, Landroid/widget/ViewSwitcher;

    .line 743
    invoke-virtual {p1}, Landroid/widget/ViewSwitcher;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_2

    .line 744
    invoke-virtual {p1, v0}, Landroid/widget/ViewSwitcher;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/cq;->c(Landroid/view/View;)Lcom/google/android/libraries/curvular/cq;

    move-result-object v2

    .line 745
    if-eqz v2, :cond_1

    iget-object v2, v2, Lcom/google/android/libraries/curvular/cq;->a:Lcom/google/android/libraries/curvular/bk;

    if-ne v2, p0, :cond_1

    .line 746
    invoke-virtual {p1, v0}, Landroid/widget/ViewSwitcher;->setDisplayedChild(I)V

    .line 747
    const/4 v0, 0x1

    .line 752
    :cond_0
    return v0

    .line 743
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 750
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to find view to switch to"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static f(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 847
    :try_start_0
    instance-of v1, p0, Lcom/google/android/libraries/curvular/au;

    if-eqz v1, :cond_0

    .line 848
    check-cast p0, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setElevation(F)V

    .line 859
    :goto_0
    return v0

    .line 850
    :cond_0
    instance-of v1, p0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 851
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setElevation(F)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 859
    :catch_0
    move-exception v1

    goto :goto_0

    .line 854
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static g(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 1029
    :try_start_0
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutDirection(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1035
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1033
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/libraries/curvular/j;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method private h(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1088
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1089
    if-nez v0, :cond_0

    .line 1107
    :goto_0
    return v3

    .line 1092
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    .line 1094
    iget-boolean v2, p0, Lcom/google/android/libraries/curvular/j;->e:Z

    if-eqz v2, :cond_1

    .line 1096
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1103
    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/libraries/curvular/j;->e:Z

    if-nez v2, :cond_2

    .line 1104
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 1106
    :cond_2
    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1099
    :catch_0
    move-exception v2

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/libraries/curvular/j;->e:Z

    goto :goto_1
.end method

.method private i(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1131
    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1132
    if-nez v0, :cond_0

    .line 1150
    :goto_0
    return v3

    .line 1135
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    .line 1137
    iget-boolean v2, p0, Lcom/google/android/libraries/curvular/j;->f:Z

    if-eqz v2, :cond_1

    .line 1139
    :try_start_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginStart(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1146
    :cond_1
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/libraries/curvular/j;->f:Z

    if-nez v2, :cond_2

    .line 1147
    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1149
    :cond_2
    invoke-virtual {p2, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1142
    :catch_0
    move-exception v2

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/libraries/curvular/j;->f:Z

    goto :goto_1
.end method

.method private j(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1220
    instance-of v0, p1, Landroid/widget/ListAdapter;

    if-eqz v0, :cond_1

    .line 1222
    check-cast p1, Landroid/widget/ListAdapter;

    move-object v1, p1

    .line 1229
    :goto_0
    instance-of v0, p2, Landroid/widget/ListView;

    if-eqz v0, :cond_9

    .line 1230
    check-cast p2, Landroid/widget/ListView;

    .line 1231
    invoke-virtual {p2}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 1232
    instance-of v2, v0, Lcom/google/android/libraries/curvular/bp;

    if-eqz v2, :cond_0

    instance-of v2, v1, Lcom/google/android/libraries/curvular/bp;

    if-nez v2, :cond_3

    :cond_0
    move v2, v3

    :goto_1
    if-eqz v2, :cond_8

    .line 1233
    check-cast v0, Lcom/google/android/libraries/curvular/bp;

    check-cast v1, Lcom/google/android/libraries/curvular/bp;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    invoke-virtual {v1}, Lcom/google/android/libraries/curvular/bp;->getCount()I

    move-result v5

    :goto_2
    if-ge v3, v5, :cond_6

    iget-object v6, v0, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v2, v1, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v7, v1, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v7, v3}, Lcom/google/android/libraries/curvular/bm;->b(I)I

    move-result v7

    invoke-virtual {v2, v7}, Lcom/google/android/libraries/curvular/bm;->c(I)Ljava/lang/Class;

    move-result-object v7

    iget-object v2, v1, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/bn;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bn;->b:Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {v6, v7, v2}, Lcom/google/android/libraries/curvular/bm;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1223
    :cond_1
    instance-of v0, p1, Lcom/google/android/libraries/curvular/bc;

    if-eqz v0, :cond_7

    .line 1224
    check-cast p1, Lcom/google/android/libraries/curvular/bc;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/j;->h:Lcom/google/android/libraries/curvular/bd;

    new-instance v2, Lcom/google/android/libraries/curvular/bp;

    invoke-direct {v2, v0}, Lcom/google/android/libraries/curvular/bp;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    iget-object v0, p1, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    iget-object v1, v0, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/libraries/curvular/ce;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    iget-object v7, v2, Lcom/google/android/libraries/curvular/bp;->a:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v7, v0, v1}, Lcom/google/android/libraries/curvular/bm;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v2}, Lcom/google/android/libraries/curvular/bp;->notifyDataSetChanged()V

    goto :goto_3

    :cond_2
    move-object v1, v2

    goto :goto_0

    .line 1232
    :cond_3
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v5

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v6

    move v2, v3

    :goto_4
    if-ge v2, v6, :cond_5

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v7

    if-lt v7, v5, :cond_4

    move v2, v3

    goto :goto_1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    move v2, v4

    goto :goto_1

    .line 1233
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/bp;->notifyDataSetChanged()V

    :goto_5
    move v3, v4

    .line 1260
    :cond_7
    :goto_6
    return v3

    .line 1235
    :cond_8
    invoke-virtual {p2, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_5

    .line 1239
    :cond_9
    instance-of v0, p2, Landroid/view/ViewGroup;

    if-eqz v0, :cond_7

    .line 1241
    check-cast p2, Landroid/view/ViewGroup;

    .line 1244
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_7
    if-ltz v2, :cond_c

    .line 1245
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1246
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 1247
    invoke-static {v6}, Lcom/google/android/libraries/curvular/cq;->c(Landroid/view/View;)Lcom/google/android/libraries/curvular/cq;

    move-result-object v0

    .line 1248
    if-nez v0, :cond_b

    move-object v0, v5

    .line 1249
    :goto_8
    if-eqz v0, :cond_a

    .line 1250
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/curvular/bd;->a(Landroid/view/View;)V

    .line 1244
    :cond_a
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_7

    .line 1248
    :cond_b
    iget-object v0, v0, Lcom/google/android/libraries/curvular/cq;->l:Lcom/google/android/libraries/curvular/bd;

    goto :goto_8

    :cond_c
    move v0, v3

    .line 1254
    :goto_9
    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_d

    .line 1255
    invoke-interface {v1, v0, v5, p2}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1256
    invoke-virtual {p2, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1254
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_d
    move v3, v4

    .line 1258
    goto :goto_6
.end method

.method private static k(Ljava/lang/Object;Landroid/view/View;)I
    .locals 2

    .prologue
    .line 1415
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1416
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1420
    :goto_0
    return v0

    .line 1417
    :cond_0
    instance-of v0, p0, Lcom/google/android/libraries/curvular/bk;

    if-eqz v0, :cond_1

    .line 1418
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    check-cast p0, Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    .line 1419
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    goto :goto_0

    .line 1422
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The value argument passed to getNextFocusId() was not ofexpected type Integer or Token."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static l(Ljava/lang/Object;Landroid/view/View;)I
    .locals 4

    .prologue
    .line 1696
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1697
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1705
    :goto_0
    return v0

    .line 1698
    :cond_0
    instance-of v0, p0, Lcom/google/android/libraries/curvular/am;

    if-eqz v0, :cond_1

    .line 1701
    check-cast p0, Lcom/google/android/libraries/curvular/am;

    .line 1703
    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lcom/google/android/libraries/curvular/am;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0

    .line 1704
    :cond_1
    instance-of v0, p0, Lcom/google/android/libraries/curvular/au;

    if-eqz v0, :cond_2

    .line 1705
    check-cast p0, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/au;->b_(Landroid/content/Context;)I

    move-result v0

    goto :goto_0

    .line 1707
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Can\'t handle padding object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static m(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Object;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    const/16 v7, 0x11

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1713
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1714
    if-nez v0, :cond_0

    .line 1738
    :goto_0
    return v4

    .line 1718
    :cond_0
    check-cast p0, Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 1719
    instance-of v2, v1, Lcom/google/android/libraries/curvular/i;

    if-eqz v2, :cond_1

    .line 1720
    check-cast v1, Lcom/google/android/libraries/curvular/i;

    .line 1721
    iget v2, v1, Lcom/google/android/libraries/curvular/i;->a:I

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v3, v7, :cond_2

    packed-switch v2, :pswitch_data_0

    :cond_2
    move v3, v2

    .line 1722
    :goto_2
    iget-boolean v2, v1, Lcom/google/android/libraries/curvular/i;->c:Z

    if-eqz v2, :cond_4

    .line 1723
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v7, :cond_3

    .line 1725
    invoke-virtual {v0, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1

    .line 1721
    :pswitch_0
    const/4 v2, 0x7

    move v3, v2

    goto :goto_2

    :pswitch_1
    const/16 v2, 0xb

    move v3, v2

    goto :goto_2

    :pswitch_2
    const/16 v2, 0x9

    move v3, v2

    goto :goto_2

    :pswitch_3
    const/4 v2, 0x5

    move v3, v2

    goto :goto_2

    :pswitch_4
    move v3, v4

    goto :goto_2

    :pswitch_5
    move v3, v5

    goto :goto_2

    .line 1727
    :cond_3
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto :goto_1

    .line 1729
    :cond_4
    iget-object v2, v1, Lcom/google/android/libraries/curvular/i;->b:Lcom/google/android/libraries/curvular/bk;

    if-eqz v2, :cond_5

    .line 1730
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/i;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    .line 1731
    invoke-virtual {v0, v3, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto :goto_1

    .line 1733
    :cond_5
    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    goto :goto_1

    .line 1737
    :cond_6
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1721
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static n(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1838
    check-cast p0, [Ljava/lang/Integer;

    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    move-object v0, p1

    .line 1839
    check-cast v0, Landroid/widget/TableLayout;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3, v4}, Landroid/widget/TableLayout;->setColumnShrinkable(IZ)V

    .line 1838
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1841
    :cond_0
    return v4
.end method

.method private static o(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1873
    if-nez p0, :cond_0

    .line 1874
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v1}, Landroid/view/View;->setStateListAnimator(Landroid/animation/StateListAnimator;)V

    .line 1885
    :goto_0
    return v0

    .line 1876
    :cond_0
    instance-of v1, p0, Landroid/animation/StateListAnimator;

    if-eqz v1, :cond_1

    .line 1877
    check-cast p0, Landroid/animation/StateListAnimator;

    invoke-virtual {p1, p0}, Landroid/view/View;->setStateListAnimator(Landroid/animation/StateListAnimator;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1885
    :catch_0
    move-exception v1

    goto :goto_0

    .line 1880
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static p(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1889
    check-cast p0, [Ljava/lang/Integer;

    array-length v2, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p0, v1

    move-object v0, p1

    .line 1890
    check-cast v0, Landroid/widget/TableLayout;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v0, v3, v4}, Landroid/widget/TableLayout;->setColumnStretchable(IZ)V

    .line 1889
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1892
    :cond_0
    return v4
.end method

.method private q(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 7

    .prologue
    const v3, 0x800005

    const v4, 0x800003

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1920
    iget-boolean v1, p0, Lcom/google/android/libraries/curvular/j;->g:Z

    if-eqz v1, :cond_1

    .line 1922
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljava/lang/Integer;

    move-object v1, v0

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p2, v1}, Landroid/view/View;->setTextAlignment(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1961
    :goto_0
    return v2

    .line 1925
    :catch_0
    move-exception v1

    const-string v1, "View#setTextAlignment does not exist on this platform. Consider using TextView#setGravity instead."

    .line 1927
    sget-boolean v6, Lcom/google/android/libraries/curvular/bo;->a:Z

    if-eqz v6, :cond_0

    .line 1928
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1930
    :cond_0
    sget-object v1, Lcom/google/android/libraries/curvular/j;->d:Ljava/lang/String;

    .line 1932
    iput-boolean v5, p0, Lcom/google/android/libraries/curvular/j;->g:Z

    .line 1937
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/libraries/curvular/j;->g:Z

    if-nez v1, :cond_2

    instance-of v1, p2, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1939
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move v2, v5

    .line 1956
    goto :goto_0

    :pswitch_0
    move v1, v2

    .line 1958
    :goto_1
    check-cast p2, Landroid/widget/TextView;

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0

    :pswitch_1
    move v1, v3

    .line 1945
    goto :goto_1

    :pswitch_2
    move v1, v4

    .line 1948
    goto :goto_1

    :pswitch_3
    move v1, v3

    .line 1951
    goto :goto_1

    :pswitch_4
    move v1, v4

    .line 1954
    goto :goto_1

    :cond_2
    move v2, v5

    .line 1961
    goto :goto_0

    .line 1939
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method private static r(Ljava/lang/Object;Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2087
    :try_start_0
    instance-of v1, p0, Lcom/google/android/libraries/curvular/au;

    if-eqz v1, :cond_0

    .line 2088
    check-cast p0, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p0, v1}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationZ(F)V

    .line 2099
    :goto_0
    return v0

    .line 2090
    :cond_0
    instance-of v1, p0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 2091
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationZ(F)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2099
    :catch_0
    move-exception v1

    goto :goto_0

    .line 2094
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 2243
    if-nez p0, :cond_0

    .line 2244
    const/4 p0, 0x0

    .line 2252
    :goto_0
    return-object p0

    .line 2245
    :cond_0
    instance-of v0, p0, Lcom/google/android/libraries/curvular/aw;

    if-eqz v0, :cond_1

    .line 2246
    check-cast p0, Lcom/google/android/libraries/curvular/aw;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    .line 2247
    :cond_1
    instance-of v0, p0, Landroid/graphics/Picture;

    if-eqz v0, :cond_2

    .line 2248
    new-instance v0, Landroid/graphics/drawable/PictureDrawable;

    check-cast p0, Landroid/graphics/Picture;

    invoke-direct {v0, p0}, Landroid/graphics/drawable/PictureDrawable;-><init>(Landroid/graphics/Picture;)V

    move-object p0, v0

    goto :goto_0

    .line 2249
    :cond_2
    instance-of v0, p0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    .line 2250
    check-cast p0, Landroid/graphics/drawable/Drawable;

    goto :goto_0

    .line 2251
    :cond_3
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 2252
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    goto :goto_0

    .line 2254
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to resolve Drawable: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static t(Ljava/lang/Object;Landroid/view/View;)F
    .locals 4

    .prologue
    .line 2274
    instance-of v0, p0, Lcom/google/android/libraries/curvular/au;

    if-eqz v0, :cond_0

    .line 2275
    check-cast p0, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v0

    .line 2277
    :goto_0
    return v0

    .line 2276
    :cond_0
    instance-of v0, p0, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 2277
    check-cast p0, Ljava/lang/Number;

    invoke-virtual {p0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    goto :goto_0

    .line 2279
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to resolve dimension: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static u(Ljava/lang/Object;Landroid/view/View;)I
    .locals 4

    .prologue
    .line 2284
    instance-of v0, p0, Lcom/google/android/libraries/curvular/au;

    if-eqz v0, :cond_0

    .line 2285
    check-cast p0, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/au;->b_(Landroid/content/Context;)I

    move-result v0

    .line 2287
    :goto_0
    return v0

    .line 2286
    :cond_0
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2287
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 2289
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to resolve dimension pixel offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static v(Ljava/lang/Object;Landroid/view/View;)I
    .locals 4

    .prologue
    .line 2294
    instance-of v0, p0, Lcom/google/android/libraries/curvular/au;

    if-eqz v0, :cond_0

    .line 2295
    check-cast p0, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v0

    .line 2297
    :goto_0
    return v0

    .line 2296
    :cond_0
    instance-of v0, p0, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2297
    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 2299
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unable to resolve dimension pixel size: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;Ljava/lang/Object;Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Ljava/lang/Object;",
            "TT;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 210
    instance-of v0, p1, Lcom/google/android/libraries/curvular/g;

    if-eqz v0, :cond_0

    .line 211
    sget-object v4, Lcom/google/android/libraries/curvular/q;->a:[I

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/curvular/g;

    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/g;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 527
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled property: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :pswitch_0
    check-cast p4, Landroid/widget/AdapterView;

    check-cast p2, Landroid/widget/Adapter;

    invoke-virtual {p4, p2}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    move v2, v3

    .line 530
    :cond_0
    :goto_0
    return v2

    .line 215
    :pswitch_1
    check-cast p4, Landroid/view/ViewGroup;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->setAddStatesFromChildren(Z)V

    move v2, v3

    goto :goto_0

    .line 217
    :pswitch_2
    check-cast p4, Landroid/widget/ImageView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    move v2, v3

    goto :goto_0

    .line 219
    :pswitch_3
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    move v2, v3

    goto :goto_0

    .line 221
    :pswitch_4
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setAllCaps(Z)V

    move v2, v3

    goto :goto_0

    .line 223
    :pswitch_5
    if-nez p2, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p4, v0}, Landroid/view/View;->setAlpha(F)V

    :goto_1
    move v2, v3

    goto :goto_0

    :cond_1
    instance-of v0, p2, Lcom/google/android/libraries/curvular/au;

    if-eqz v0, :cond_2

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    :cond_2
    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_1

    .line 225
    :pswitch_6
    instance-of v0, p4, Landroid/widget/ViewAnimator;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/ViewAnimator;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/ViewAnimator;->setAnimateFirstView(Z)V

    move v2, v3

    goto :goto_0

    .line 227
    :pswitch_7
    instance-of v0, p4, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Boolean;

    check-cast p4, Landroid/view/ViewGroup;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    :goto_2
    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    move v2, v3

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 229
    :pswitch_8
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    move v2, v3

    goto/16 :goto_0

    .line 231
    :pswitch_9
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->d(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 233
    :pswitch_a
    if-nez p2, :cond_4

    invoke-virtual {p4, v2}, Landroid/view/View;->setBackgroundResource(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_4
    instance-of v0, p2, Lcom/google/android/libraries/curvular/aq;

    if-eqz v0, :cond_5

    check-cast p2, Lcom/google/android/libraries/curvular/aq;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setBackgroundColor(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_5
    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setBackgroundColor(I)V

    move v2, v3

    goto/16 :goto_0

    .line 235
    :pswitch_b
    invoke-virtual {p4}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p4}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->d(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p4, v0, v1, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    goto/16 :goto_0

    .line 237
    :pswitch_c
    check-cast p4, Landroid/widget/LinearLayout;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/LinearLayout;->setBaselineAligned(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 239
    :pswitch_d
    instance-of v0, p4, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/ImageView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/ImageView;->setBaselineAlignBottom(Z)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_e
    move-object v0, p4

    .line 241
    check-cast v0, Landroid/widget/CompoundButton;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    .line 243
    :pswitch_f
    check-cast p4, Landroid/widget/Checkable;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {p4, v0}, Landroid/widget/Checkable;->setChecked(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 245
    :pswitch_10
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setClickable(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 247
    :pswitch_11
    instance-of v0, p4, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/view/ViewGroup;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 249
    :pswitch_12
    instance-of v0, p4, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    check-cast p4, Landroid/view/ViewGroup;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    :cond_6
    move v2, v3

    goto/16 :goto_0

    .line 251
    :pswitch_13
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/TextView;

    instance-of v1, p2, [Ljava/lang/Object;

    if-eqz v1, :cond_7

    check-cast p2, [Ljava/lang/Object;

    aget-object v1, p2, v2

    invoke-static {v1, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    aget-object v2, p2, v3

    invoke-static {v2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    aget-object v4, p2, v5

    invoke-static {v4, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    aget-object v5, p2, v6

    invoke-static {v5, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    :cond_7
    if-nez p2, :cond_0

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 253
    :pswitch_14
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    move v2, v3

    goto/16 :goto_0

    .line 255
    :pswitch_15
    check-cast p4, Landroid/widget/TimePicker;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p4, p2}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    move v2, v3

    goto/16 :goto_0

    .line 257
    :pswitch_16
    check-cast p4, Landroid/widget/TimePicker;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p4, p2}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    move v2, v3

    goto/16 :goto_0

    .line 259
    :pswitch_17
    check-cast p4, Landroid/view/ViewGroup;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    move v2, v3

    goto/16 :goto_0

    .line 261
    :pswitch_18
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->e(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 263
    :pswitch_19
    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    instance-of v1, p4, Landroid/widget/NumberPicker;

    if-eqz v1, :cond_0

    check-cast p4, Landroid/widget/NumberPicker;

    invoke-virtual {p4, v0}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    move v2, v3

    goto/16 :goto_0

    .line 265
    :pswitch_1a
    instance-of v0, p4, Landroid/widget/ListView;

    if-eqz v0, :cond_8

    move-object v0, p4

    check-cast v0, Landroid/widget/ListView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    :cond_8
    instance-of v0, p4, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDividerDrawable(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_1b
    move-object v0, p4

    .line 267
    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setDividerPadding(I)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_1c
    move-object v0, p4

    .line 269
    check-cast v0, Landroid/widget/ListView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    move v2, v3

    goto/16 :goto_0

    .line 271
    :pswitch_1d
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4, v6}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/widget/TextView;I)Z

    move-result v2

    goto/16 :goto_0

    .line 273
    :pswitch_1e
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4, v5}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/widget/TextView;I)Z

    move-result v2

    goto/16 :goto_0

    .line 275
    :pswitch_1f
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4, v2}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/widget/TextView;I)Z

    move-result v2

    goto/16 :goto_0

    :pswitch_20
    move-object v0, p4

    .line 277
    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    move v2, v3

    goto/16 :goto_0

    .line 279
    :pswitch_21
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4, v5}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/widget/TextView;I)Z

    move-result v2

    goto/16 :goto_0

    .line 281
    :pswitch_22
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4, v2}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/widget/TextView;I)Z

    move-result v2

    goto/16 :goto_0

    .line 283
    :pswitch_23
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4, v3}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/widget/TextView;I)Z

    move-result v2

    goto/16 :goto_0

    .line 285
    :pswitch_24
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setDuplicateParentStateEnabled(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 287
    :pswitch_25
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->f(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 289
    :pswitch_26
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    move v2, v3

    goto/16 :goto_0

    .line 291
    :pswitch_27
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setEnabled(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 293
    :pswitch_28
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setError(Ljava/lang/CharSequence;)V

    move v2, v3

    goto/16 :goto_0

    .line 295
    :pswitch_29
    check-cast p4, Landroid/widget/ScrollView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/ScrollView;->setFillViewport(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 297
    :pswitch_2a
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setFocusable(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 299
    :pswitch_2b
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 301
    :pswitch_2c
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Landroid/graphics/Typeface;

    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    move v2, v3

    goto/16 :goto_0

    .line 303
    :pswitch_2d
    instance-of v0, p4, Landroid/widget/NumberPicker;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/NumberPicker;

    check-cast p2, Landroid/widget/NumberPicker$Formatter;

    invoke-virtual {p4, p2}, Landroid/widget/NumberPicker;->setFormatter(Landroid/widget/NumberPicker$Formatter;)V

    move v2, v3

    goto/16 :goto_0

    .line 305
    :pswitch_2e
    instance-of v0, p4, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_9

    check-cast p4, Landroid/widget/LinearLayout;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/LinearLayout;->setGravity(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_9
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_a

    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setGravity(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_a
    instance-of v0, p4, Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/RelativeLayout;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/RelativeLayout;->setGravity(I)V

    move v2, v3

    goto/16 :goto_0

    .line 307
    :pswitch_2f
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    move v2, v3

    goto/16 :goto_0

    .line 309
    :pswitch_30
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setId(I)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_31
    move-object v0, p4

    .line 311
    check-cast v0, Landroid/widget/ImageView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    .line 313
    :pswitch_32
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setImeOptions(I)V

    move v2, v3

    goto/16 :goto_0

    .line 315
    :pswitch_33
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setImportantForAccessibility(I)V

    move v2, v3

    goto/16 :goto_0

    .line 317
    :pswitch_34
    instance-of v0, p4, Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/ViewSwitcher;

    instance-of v1, p2, Lcom/google/android/libraries/curvular/aa;

    if-eqz v1, :cond_b

    check-cast p2, Lcom/google/android/libraries/curvular/aa;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    invoke-interface {p2}, Lcom/google/android/libraries/curvular/aa;->a()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    move v2, v3

    goto/16 :goto_0

    :cond_b
    instance-of v1, p2, Landroid/view/animation/Animation;

    if-eqz v1, :cond_0

    check-cast p2, Landroid/view/animation/Animation;

    invoke-virtual {v0, p2}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    move v2, v3

    goto/16 :goto_0

    .line 319
    :pswitch_35
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 321
    :pswitch_36
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setRawInputType(I)V

    move v2, v3

    goto/16 :goto_0

    .line 323
    :pswitch_37
    check-cast p4, Landroid/widget/TimePicker;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p4, p2}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    move v2, v3

    goto/16 :goto_0

    .line 325
    :pswitch_38
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    move v2, v3

    goto/16 :goto_0

    .line 327
    :pswitch_39
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->g(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 329
    :pswitch_3a
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_c

    move v2, v3

    goto/16 :goto_0

    :cond_c
    instance-of v0, v1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_d

    move-object v0, v1

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-virtual {p4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_3
    move v2, v3

    goto/16 :goto_0

    :cond_d
    instance-of v0, v1, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v0, :cond_e

    move-object v0, v1

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    invoke-virtual {p4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_3

    :cond_e
    sget-object v0, Lcom/google/android/libraries/curvular/j;->d:Ljava/lang/String;

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Can\'t apply layout_gravity to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 331
    :pswitch_3b
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_f

    move v2, v3

    goto/16 :goto_0

    :cond_f
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    goto/16 :goto_0

    .line 333
    :pswitch_3c
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v0, :cond_10

    move v2, v3

    goto/16 :goto_0

    :cond_10
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    goto/16 :goto_0

    .line 335
    :pswitch_3d
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v0, :cond_11

    move v2, v3

    goto/16 :goto_0

    :cond_11
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    goto/16 :goto_0

    .line 337
    :pswitch_3e
    invoke-direct {p0, p2, p4}, Lcom/google/android/libraries/curvular/j;->h(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 339
    :pswitch_3f
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v0, :cond_12

    move v2, v3

    goto/16 :goto_0

    :cond_12
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    goto/16 :goto_0

    .line 341
    :pswitch_40
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v0, :cond_13

    move v2, v3

    goto/16 :goto_0

    :cond_13
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    goto/16 :goto_0

    .line 343
    :pswitch_41
    invoke-direct {p0, p2, p4}, Lcom/google/android/libraries/curvular/j;->i(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 345
    :pswitch_42
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v0, :cond_14

    move v2, v3

    goto/16 :goto_0

    :cond_14
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->u(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    goto/16 :goto_0

    .line 347
    :pswitch_43
    instance-of v0, p4, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    instance-of v0, p2, Landroid/animation/LayoutTransition;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/view/ViewGroup;

    check-cast p2, Landroid/animation/LayoutTransition;

    invoke-virtual {p4, p2}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    move v2, v3

    goto/16 :goto_0

    .line 349
    :pswitch_44
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-nez v1, :cond_15

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    :cond_15
    instance-of v0, v1, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v0, :cond_16

    move-object v0, v1

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {p4, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_4
    move v2, v3

    goto/16 :goto_0

    :cond_16
    sget-object v0, Lcom/google/android/libraries/curvular/j;->d:Ljava/lang/String;

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Can\'t apply layout_weight to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 351
    :pswitch_45
    invoke-virtual {p4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    if-nez v0, :cond_17

    move v2, v3

    goto/16 :goto_0

    :cond_17
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-virtual {p4, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move v2, v3

    goto/16 :goto_0

    .line 353
    :pswitch_46
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setLines(I)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_47
    move-object v0, p4

    .line 355
    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/libraries/curvular/bh;->c:I

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    invoke-direct {p0, v0}, Lcom/google/android/libraries/curvular/j;->b(Landroid/widget/TextView;)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->t(Ljava/lang/Object;Landroid/view/View;)F

    move-result v2

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    move v2, v3

    goto/16 :goto_0

    .line 357
    :pswitch_48
    check-cast p4, Landroid/widget/TextView;

    sget v0, Lcom/google/android/libraries/curvular/bh;->d:I

    invoke-virtual {p4, v0, p2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    invoke-direct {p0, p4}, Lcom/google/android/libraries/curvular/j;->a(Landroid/widget/TextView;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-virtual {p4, v0, v1}, Landroid/widget/TextView;->setLineSpacing(FF)V

    move v2, v3

    goto/16 :goto_0

    .line 359
    :pswitch_49
    invoke-direct {p0, p2, p4}, Lcom/google/android/libraries/curvular/j;->j(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 361
    :pswitch_4a
    instance-of v0, p4, Landroid/widget/ListView;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/ListView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    .line 363
    :pswitch_4b
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setLongClickable(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 365
    :pswitch_4c
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_18

    move-object v0, p4

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxHeight(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_18
    instance-of v0, p4, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMaxHeight(I)V

    move v2, v3

    goto/16 :goto_0

    .line 367
    :pswitch_4d
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_19

    move-object v0, p4

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_19
    instance-of v0, p4, Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setMaxWidth(I)V

    move v2, v3

    goto/16 :goto_0

    .line 369
    :pswitch_4e
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    move v2, v3

    goto/16 :goto_0

    .line 371
    :pswitch_4f
    instance-of v0, p4, Landroid/widget/NumberPicker;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/NumberPicker;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    move v2, v3

    goto/16 :goto_0

    .line 373
    :pswitch_50
    instance-of v0, p4, Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/FrameLayout;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/FrameLayout;->setMeasureAllChildren(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 375
    :pswitch_51
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_1a

    move-object v0, p4

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinHeight(I)V

    :goto_5
    move v2, v3

    goto/16 :goto_0

    :cond_1a
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setMinimumHeight(I)V

    goto :goto_5

    .line 377
    :pswitch_52
    instance-of v0, p4, Landroid/widget/NumberPicker;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/NumberPicker;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/NumberPicker;->setMinValue(I)V

    move v2, v3

    goto/16 :goto_0

    .line 379
    :pswitch_53
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_1b

    move-object v0, p4

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinWidth(I)V

    :goto_6
    move v2, v3

    goto/16 :goto_0

    :cond_1b
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->v(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setMinimumWidth(I)V

    goto :goto_6

    .line 381
    :pswitch_54
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    instance-of v0, p2, Landroid/text/method/MovementMethod;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Landroid/text/method/MovementMethod;

    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    move v2, v3

    goto/16 :goto_0

    .line 383
    :pswitch_55
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->k(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setNextFocusDownId(I)V

    move v2, v3

    goto/16 :goto_0

    .line 385
    :pswitch_56
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->k(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setNextFocusForwardId(I)V

    move v2, v3

    goto/16 :goto_0

    .line 387
    :pswitch_57
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->k(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setNextFocusLeftId(I)V

    move v2, v3

    goto/16 :goto_0

    .line 389
    :pswitch_58
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->k(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setNextFocusRightId(I)V

    move v2, v3

    goto/16 :goto_0

    .line 391
    :pswitch_59
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->k(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setNextFocusUpId(I)V

    move v2, v3

    goto/16 :goto_0

    .line 393
    :pswitch_5a
    invoke-static {p4}, Lcom/google/android/libraries/curvular/cb;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cb;

    move-result-object v0

    if-nez p2, :cond_1d

    sget-object v2, Lcom/google/android/libraries/curvular/j;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/cb;->a(Lcom/google/android/libraries/curvular/bk;Landroid/view/View$OnClickListener;)V

    :cond_1c
    :goto_7
    move v2, v3

    goto/16 :goto_0

    :cond_1d
    instance-of v1, p2, Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_1e

    sget-object v1, Lcom/google/android/libraries/curvular/j;->a:Lcom/google/android/libraries/curvular/bk;

    check-cast p2, Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/cb;->a(Lcom/google/android/libraries/curvular/bk;Landroid/view/View$OnClickListener;)V

    goto :goto_7

    :cond_1e
    instance-of v1, p2, Lcom/google/android/libraries/curvular/cg;

    if-eqz v1, :cond_1c

    sget-object v1, Lcom/google/android/libraries/curvular/j;->a:Lcom/google/android/libraries/curvular/bk;

    new-instance v2, Lcom/google/android/libraries/curvular/k;

    invoke-direct {v2, p0, p4, p2}, Lcom/google/android/libraries/curvular/k;-><init>(Lcom/google/android/libraries/curvular/j;Landroid/view/View;Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/cb;->a(Lcom/google/android/libraries/curvular/bk;Landroid/view/View$OnClickListener;)V

    goto :goto_7

    .line 395
    :pswitch_5b
    if-nez p2, :cond_1f

    check-cast p4, Landroid/widget/CompoundButton;

    invoke-virtual {p4, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v2, v3

    goto/16 :goto_0

    :cond_1f
    instance-of v0, p2, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    if-eqz v0, :cond_20

    check-cast p4, Landroid/widget/CompoundButton;

    check-cast p2, Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {p4, p2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v2, v3

    goto/16 :goto_0

    :cond_20
    instance-of v0, p2, Lcom/google/android/libraries/curvular/ch;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/CompoundButton;

    new-instance v0, Lcom/google/android/libraries/curvular/l;

    invoke-direct {v0, p0, p2}, Lcom/google/android/libraries/curvular/l;-><init>(Lcom/google/android/libraries/curvular/j;Ljava/lang/Object;)V

    invoke-virtual {p4, v0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    move v2, v3

    goto/16 :goto_0

    .line 397
    :pswitch_5c
    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    invoke-static {p4}, Lcom/google/android/libraries/curvular/r;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/r;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/libraries/curvular/r;->a:Lcom/google/android/libraries/curvular/b/i;

    move v2, v3

    goto/16 :goto_0

    .line 399
    :pswitch_5d
    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    invoke-static {p4}, Lcom/google/android/libraries/curvular/r;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/r;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/libraries/curvular/r;->b:Lcom/google/android/libraries/curvular/b/i;

    move v2, v3

    goto/16 :goto_0

    .line 401
    :pswitch_5e
    check-cast p4, Landroid/widget/AdapterView;

    check-cast p2, Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {p4, p2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    move v2, v3

    goto/16 :goto_0

    .line 403
    :pswitch_5f
    invoke-static {p4}, Lcom/google/android/libraries/curvular/cb;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cb;

    move-result-object v0

    if-nez p2, :cond_22

    sget-object v2, Lcom/google/android/libraries/curvular/j;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/cb;->a(Lcom/google/android/libraries/curvular/bk;Landroid/view/View$OnLongClickListener;)V

    :cond_21
    :goto_8
    move v2, v3

    goto/16 :goto_0

    :cond_22
    instance-of v1, p2, Landroid/view/View$OnLongClickListener;

    if-eqz v1, :cond_23

    sget-object v1, Lcom/google/android/libraries/curvular/j;->a:Lcom/google/android/libraries/curvular/bk;

    check-cast p2, Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/cb;->a(Lcom/google/android/libraries/curvular/bk;Landroid/view/View$OnLongClickListener;)V

    goto :goto_8

    :cond_23
    instance-of v1, p2, Lcom/google/android/libraries/curvular/ci;

    if-eqz v1, :cond_21

    sget-object v1, Lcom/google/android/libraries/curvular/j;->a:Lcom/google/android/libraries/curvular/bk;

    new-instance v2, Lcom/google/android/libraries/curvular/m;

    invoke-direct {v2, p0, p2, p4}, Lcom/google/android/libraries/curvular/m;-><init>(Lcom/google/android/libraries/curvular/j;Ljava/lang/Object;Landroid/view/View;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/cb;->a(Lcom/google/android/libraries/curvular/bk;Landroid/view/View$OnLongClickListener;)V

    goto :goto_8

    .line 405
    :pswitch_60
    instance-of v0, p4, Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    instance-of v0, p2, Landroid/support/v4/view/bz;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/support/v4/view/ViewPager;

    check-cast p2, Landroid/support/v4/view/bz;

    invoke-virtual {p4, p2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/bz;)V

    move v2, v3

    goto/16 :goto_0

    .line 407
    :pswitch_61
    invoke-virtual {p4}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_25

    move v4, v3

    :goto_9
    if-nez p2, :cond_26

    invoke-static {p4}, Lcom/google/android/libraries/curvular/cm;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cm;

    move-result-object v2

    sget-object v0, Lcom/google/android/libraries/curvular/j;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v0, :cond_24

    invoke-virtual {p4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    sget-object v0, Lcom/google/android/libraries/curvular/j;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    :cond_24
    move v2, v3

    goto/16 :goto_0

    :cond_25
    move v4, v2

    goto :goto_9

    :cond_26
    instance-of v0, p2, Lcom/google/android/libraries/curvular/h;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/libraries/curvular/h;

    invoke-static {p4}, Lcom/google/android/libraries/curvular/cm;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cm;

    move-result-object v0

    sget-object v1, Lcom/google/android/libraries/curvular/j;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v0, :cond_27

    invoke-virtual {p4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :cond_27
    new-instance v0, Lcom/google/android/libraries/curvular/p;

    invoke-direct {v0, p0, p4, p2, v4}, Lcom/google/android/libraries/curvular/p;-><init>(Lcom/google/android/libraries/curvular/j;Landroid/view/View;Lcom/google/android/libraries/curvular/h;Z)V

    invoke-virtual {p4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    invoke-static {p4}, Lcom/google/android/libraries/curvular/cm;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cm;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/j;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    invoke-virtual {p4}, Landroid/view/View;->invalidate()V

    move v2, v3

    goto/16 :goto_0

    .line 409
    :pswitch_62
    instance-of v0, p2, Landroid/widget/RadioGroup$OnCheckedChangeListener;

    if-eqz v0, :cond_28

    check-cast p4, Landroid/widget/RadioGroup;

    check-cast p2, Landroid/widget/RadioGroup$OnCheckedChangeListener;

    invoke-virtual {p4, p2}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    move v2, v3

    goto/16 :goto_0

    :cond_28
    instance-of v0, p2, Lcom/google/android/libraries/curvular/cj;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/RadioGroup;

    new-instance v0, Lcom/google/android/libraries/curvular/n;

    invoke-direct {v0, p0, p2}, Lcom/google/android/libraries/curvular/n;-><init>(Lcom/google/android/libraries/curvular/j;Ljava/lang/Object;)V

    invoke-virtual {p4, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    move v2, v3

    goto/16 :goto_0

    .line 411
    :pswitch_63
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    check-cast p4, Landroid/widget/TextView;

    invoke-static {p4}, Lcom/google/android/libraries/curvular/s;->a(Landroid/widget/TextView;)Lcom/google/android/libraries/curvular/s;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/libraries/curvular/s;->a:Lcom/google/android/libraries/curvular/b/i;

    move v2, v3

    goto/16 :goto_0

    .line 413
    :pswitch_64
    instance-of v0, p2, Landroid/widget/TimePicker$OnTimeChangedListener;

    if-eqz v0, :cond_29

    check-cast p4, Landroid/widget/TimePicker;

    check-cast p2, Landroid/widget/TimePicker$OnTimeChangedListener;

    invoke-virtual {p4, p2}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    move v2, v3

    goto/16 :goto_0

    :cond_29
    instance-of v0, p2, Lcom/google/android/libraries/curvular/ck;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/TimePicker;

    new-instance v0, Lcom/google/android/libraries/curvular/o;

    invoke-direct {v0, p0, p2}, Lcom/google/android/libraries/curvular/o;-><init>(Lcom/google/android/libraries/curvular/j;Ljava/lang/Object;)V

    invoke-virtual {p4, v0}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    move v2, v3

    goto/16 :goto_0

    .line 415
    :pswitch_65
    instance-of v0, p4, Landroid/widget/NumberPicker;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/NumberPicker;

    check-cast p2, Landroid/widget/NumberPicker$OnValueChangeListener;

    invoke-virtual {p4, p2}, Landroid/widget/NumberPicker;->setOnValueChangedListener(Landroid/widget/NumberPicker$OnValueChangeListener;)V

    move v2, v3

    goto/16 :goto_0

    .line 417
    :pswitch_66
    check-cast p4, Landroid/widget/LinearLayout;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    move v2, v3

    goto/16 :goto_0

    .line 419
    :pswitch_67
    instance-of v0, p4, Landroid/widget/ViewSwitcher;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/ViewSwitcher;

    instance-of v1, p2, Lcom/google/android/libraries/curvular/aa;

    if-eqz v1, :cond_2a

    check-cast p2, Lcom/google/android/libraries/curvular/aa;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    invoke-interface {p2}, Lcom/google/android/libraries/curvular/aa;->a()Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    move v2, v3

    goto/16 :goto_0

    :cond_2a
    instance-of v1, p2, Landroid/view/animation/Animation;

    if-eqz v1, :cond_0

    check-cast p2, Landroid/view/animation/Animation;

    invoke-virtual {v0, p2}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    move v2, v3

    goto/16 :goto_0

    .line 421
    :pswitch_68
    check-cast p2, [Ljava/lang/Object;

    aget-object v0, p2, v2

    invoke-static {v0, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    aget-object v1, p2, v3

    invoke-static {v1, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    aget-object v2, p2, v5

    invoke-static {v2, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v2

    aget-object v4, p2, v6

    invoke-static {v4, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v4

    invoke-virtual {p4, v0, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 423
    :pswitch_69
    invoke-virtual {p4}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v4

    invoke-virtual {p4, v0, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 425
    :pswitch_6a
    invoke-static {p4}, Landroid/support/v4/view/at;->m(Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-static {p4, v0, v1, v2, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 427
    :pswitch_6b
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p4, v0, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 429
    :pswitch_6c
    invoke-virtual {p4}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p4, v0, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 431
    :pswitch_6d
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v0

    invoke-virtual {p4}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-static {p4}, Landroid/support/v4/view/at;->n(Landroid/view/View;)I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-static {p4, v0, v1, v2, v4}, Landroid/support/v4/view/at;->b(Landroid/view/View;IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 433
    :pswitch_6e
    invoke-virtual {p4}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->l(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {p4}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p4}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p4, v0, v1, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    move v2, v3

    goto/16 :goto_0

    .line 435
    :pswitch_6f
    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    move v2, v3

    goto/16 :goto_0

    .line 437
    :pswitch_70
    instance-of v0, p4, Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/ProgressBar;

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    move v2, v3

    goto/16 :goto_0

    .line 439
    :pswitch_71
    instance-of v0, p4, Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    .line 441
    :pswitch_72
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->m(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 443
    :pswitch_73
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setRotation(F)V

    move v2, v3

    goto/16 :goto_0

    .line 445
    :pswitch_74
    check-cast p4, Landroid/widget/ImageView;

    check-cast p2, Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p4, p2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_75
    move-object v0, p2

    .line 447
    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setHorizontalScrollBarEnabled(Z)V

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setVerticalScrollBarEnabled(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 449
    :pswitch_76
    instance-of v0, p4, Landroid/widget/AdapterView;

    if-eqz v0, :cond_2b

    check-cast p4, Landroid/widget/AdapterView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/AdapterView;->setSelection(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_2b
    instance-of v0, p4, Landroid/widget/EditText;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/EditText;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/EditText;->setSelection(I)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_77
    move-object v0, p4

    .line 451
    check-cast v0, Landroid/widget/TextView;

    move-object v1, p4

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowRadius()F

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowDx()F

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowDy()F

    move-result v0

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v5

    invoke-virtual {v1, v2, v4, v0, v5}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_78
    move-object v0, p4

    .line 453
    check-cast v0, Landroid/widget/TextView;

    check-cast p4, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowRadius()F

    move-result v1

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowDy()F

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowColor()I

    move-result v0

    invoke-virtual {p4, v1, v2, v4, v0}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_79
    move-object v0, p4

    .line 455
    check-cast v0, Landroid/widget/TextView;

    check-cast p4, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowRadius()F

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowDy()F

    move-result v2

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowColor()I

    move-result v0

    invoke-virtual {p4, v1, v2, v4, v0}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_7a
    move-object v0, p4

    .line 457
    check-cast v0, Landroid/widget/TextView;

    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowDy()F

    move-result v2

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowDx()F

    move-result v4

    invoke-virtual {v0}, Landroid/widget/TextView;->getShadowColor()I

    move-result v0

    invoke-virtual {p4, v1, v2, v4, v0}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    move v2, v3

    goto/16 :goto_0

    .line 459
    :pswitch_7b
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 461
    :pswitch_7c
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setSelectAllOnFocus(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 463
    :pswitch_7d
    check-cast p4, Landroid/widget/LinearLayout;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/LinearLayout;->setShowDividers(I)V

    move v2, v3

    goto/16 :goto_0

    .line 465
    :pswitch_7e
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->n(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    :pswitch_7f
    move-object v0, p4

    .line 467
    check-cast v0, Landroid/widget/ImageView;

    if-nez p2, :cond_2c

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_2c
    instance-of v1, p2, Lcom/google/android/libraries/curvular/aw;

    if-eqz v1, :cond_2d

    check-cast p2, Lcom/google/android/libraries/curvular/aw;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    :cond_2d
    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_2e

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_2e
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->s(Ljava/lang/Object;Landroid/view/View;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move v2, v3

    goto/16 :goto_0

    .line 469
    :pswitch_80
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->o(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 471
    :pswitch_81
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->p(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 473
    :pswitch_82
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    instance-of v1, p4, Landroid/widget/TextView;

    if-eqz v1, :cond_2f

    check-cast p4, Landroid/widget/TextView;

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v2, v3

    goto/16 :goto_0

    :cond_2f
    instance-of v1, p4, Landroid/widget/TextSwitcher;

    if-eqz v1, :cond_0

    check-cast p4, Landroid/widget/TextSwitcher;

    invoke-virtual {p4, v0}, Landroid/widget/TextSwitcher;->setText(Ljava/lang/CharSequence;)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_83
    move-object v0, p4

    .line 475
    check-cast v0, Landroid/widget/ImageView;

    if-eqz p2, :cond_30

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setColorFilter(I)V

    :goto_a
    move v2, v3

    goto/16 :goto_0

    :cond_30
    invoke-virtual {v0}, Landroid/widget/ImageView;->clearColorFilter()V

    goto :goto_a

    .line 477
    :pswitch_84
    invoke-direct {p0, p2, p4}, Lcom/google/android/libraries/curvular/j;->q(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    :pswitch_85
    move-object v0, p4

    .line 479
    check-cast v0, Landroid/widget/TextView;

    instance-of v1, p2, Lcom/google/android/libraries/curvular/bj;

    if-eqz v1, :cond_31

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast p2, Lcom/google/android/libraries/curvular/bj;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {p2, v2}, Lcom/google/android/libraries/curvular/bj;->d(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    move v2, v3

    goto/16 :goto_0

    :cond_31
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    move v2, v3

    goto/16 :goto_0

    .line 481
    :pswitch_86
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/CharSequence;

    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_32

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_33

    :cond_32
    const/16 v0, 0x8

    :goto_b
    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    move v2, v3

    goto/16 :goto_0

    :cond_33
    move v0, v2

    goto :goto_b

    :pswitch_87
    move-object v0, p4

    .line 483
    check-cast v0, Landroid/widget/TextView;

    if-nez p2, :cond_34

    invoke-virtual {v0}, Landroid/widget/TextView;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_35

    :cond_34
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->c(Ljava/lang/Object;Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    :cond_35
    move v2, v3

    goto/16 :goto_0

    :pswitch_88
    move-object v0, p4

    .line 485
    check-cast v0, Landroid/widget/TextView;

    if-nez p2, :cond_36

    invoke-virtual {v0}, Landroid/widget/TextView;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_37

    :cond_36
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->c(Ljava/lang/Object;Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHintTextColor(Landroid/content/res/ColorStateList;)V

    :cond_37
    move v2, v3

    goto/16 :goto_0

    :pswitch_89
    move-object v0, p4

    .line 487
    check-cast v0, Landroid/widget/TextView;

    if-nez p2, :cond_38

    invoke-virtual {v0}, Landroid/widget/TextView;->getLinkTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    if-eqz v1, :cond_39

    :cond_38
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->c(Ljava/lang/Object;Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLinkTextColor(Landroid/content/res/ColorStateList;)V

    :cond_39
    move v2, v3

    goto/16 :goto_0

    .line 489
    :pswitch_8a
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextIsSelectable(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 491
    :pswitch_8b
    check-cast p4, Landroid/widget/TextView;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_3a

    const-string v0, ""

    :cond_3a
    invoke-virtual {p4, v0}, Landroid/widget/TextView;->setTextKeepState(Ljava/lang/CharSequence;)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_8c
    move-object v0, p4

    .line 493
    check-cast v0, Landroid/widget/ToggleButton;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextOff(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    move v2, v3

    goto/16 :goto_0

    :pswitch_8d
    move-object v0, p4

    .line 495
    check-cast v0, Landroid/widget/ToggleButton;

    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setTextOn(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/widget/ToggleButton;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 497
    :pswitch_8e
    instance-of v0, p2, Lcom/google/android/libraries/curvular/bl;

    if-eqz v0, :cond_0

    instance-of v0, p4, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    move-object v0, p4

    check-cast v0, Landroid/widget/TextView;

    check-cast p2, Lcom/google/android/libraries/curvular/bl;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {p2, v1}, Lcom/google/android/libraries/curvular/bl;->f(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    move v2, v3

    goto/16 :goto_0

    .line 499
    :pswitch_8f
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->t(Ljava/lang/Object;Landroid/view/View;)F

    move-result v0

    check-cast p4, Landroid/widget/TextView;

    invoke-virtual {p4, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    move v2, v3

    goto/16 :goto_0

    .line 501
    :pswitch_90
    check-cast p4, Landroid/widget/TextView;

    invoke-virtual {p4}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    move v2, v3

    goto/16 :goto_0

    .line 503
    :pswitch_91
    instance-of v0, p2, Lcom/google/android/libraries/curvular/au;

    if-eqz v0, :cond_3b

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setTranslationX(F)V

    move v2, v3

    goto/16 :goto_0

    :cond_3b
    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setTranslationX(F)V

    move v2, v3

    goto/16 :goto_0

    .line 505
    :pswitch_92
    instance-of v0, p2, Lcom/google/android/libraries/curvular/au;

    if-eqz v0, :cond_3c

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setTranslationY(F)V

    move v2, v3

    goto/16 :goto_0

    :cond_3c
    instance-of v0, p2, Ljava/lang/Number;

    if-eqz v0, :cond_0

    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setTranslationY(F)V

    move v2, v3

    goto/16 :goto_0

    .line 507
    :pswitch_93
    invoke-static {p2, p4}, Lcom/google/android/libraries/curvular/j;->r(Ljava/lang/Object;Landroid/view/View;)Z

    move-result v2

    goto/16 :goto_0

    .line 509
    :pswitch_94
    instance-of v0, p4, Landroid/widget/NumberPicker;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/NumberPicker;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/NumberPicker;->setValue(I)V

    move v2, v3

    goto/16 :goto_0

    .line 511
    :pswitch_95
    instance-of v0, p4, Lcom/google/android/libraries/curvular/cl;

    if-eqz v0, :cond_0

    check-cast p4, Lcom/google/android/libraries/curvular/cl;

    check-cast p2, Lcom/google/android/libraries/curvular/ce;

    invoke-interface {p4, p2}, Lcom/google/android/libraries/curvular/cl;->setViewModel(Lcom/google/android/libraries/curvular/ce;)V

    move v2, v3

    goto/16 :goto_0

    .line 513
    :pswitch_96
    instance-of v0, p4, Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Class;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/support/v4/view/ViewPager;

    check-cast p2, Ljava/lang/Class;

    new-instance v0, Lcom/google/android/libraries/curvular/bq;

    iget-object v1, p0, Lcom/google/android/libraries/curvular/j;->h:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v0, v1, p2}, Lcom/google/android/libraries/curvular/bq;-><init>(Lcom/google/android/libraries/curvular/bd;Ljava/lang/Class;)V

    invoke-virtual {p4, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/ag;)V

    move v2, v3

    goto/16 :goto_0

    .line 515
    :pswitch_97
    instance-of v0, p4, Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p4}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Landroid/support/v4/view/ag;->a(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p4, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    move v2, v3

    goto/16 :goto_0

    .line 517
    :pswitch_98
    instance-of v0, p4, Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/support/v4/view/ViewPager;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    move v2, v3

    goto/16 :goto_0

    .line 519
    :pswitch_99
    instance-of v0, p4, Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    instance-of v0, p2, Ljava/util/List;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/support/v4/view/ViewPager;

    invoke-virtual {p4}, Landroid/support/v4/view/ViewPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bq;

    check-cast p2, Ljava/util/List;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/curvular/bq;->a(Ljava/util/List;)V

    move v2, v3

    goto/16 :goto_0

    .line 521
    :pswitch_9a
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p4, v0}, Landroid/view/View;->setVisibility(I)V

    move v2, v3

    goto/16 :goto_0

    .line 523
    :pswitch_9b
    instance-of v0, p4, Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/LinearLayout;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p4, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    move v2, v3

    goto/16 :goto_0

    .line 525
    :pswitch_9c
    instance-of v0, p4, Landroid/widget/NumberPicker;

    if-eqz v0, :cond_0

    check-cast p4, Landroid/widget/NumberPicker;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p4, v0}, Landroid/widget/NumberPicker;->setWrapSelectorWheel(Z)V

    move v2, v3

    goto/16 :goto_0

    .line 211
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_6f
        :pswitch_70
        :pswitch_71
        :pswitch_72
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_77
        :pswitch_78
        :pswitch_79
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_80
        :pswitch_81
        :pswitch_82
        :pswitch_83
        :pswitch_84
        :pswitch_85
        :pswitch_86
        :pswitch_87
        :pswitch_88
        :pswitch_89
        :pswitch_8a
        :pswitch_8b
        :pswitch_8c
        :pswitch_8d
        :pswitch_8e
        :pswitch_8f
        :pswitch_90
        :pswitch_91
        :pswitch_92
        :pswitch_93
        :pswitch_94
        :pswitch_95
        :pswitch_96
        :pswitch_97
        :pswitch_98
        :pswitch_99
        :pswitch_9a
        :pswitch_9b
        :pswitch_9c
    .end packed-switch
.end method
