.class public final Lcom/google/android/libraries/memorymonitor/a;
.super Lcom/google/android/libraries/memorymonitor/c;
.source "PG"


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0xa

    .line 30
    invoke-direct {p0}, Lcom/google/android/libraries/memorymonitor/c;-><init>()V

    .line 31
    new-instance v0, Landroid/os/Debug$MemoryInfo;

    invoke-direct {v0}, Landroid/os/Debug$MemoryInfo;-><init>()V

    .line 32
    invoke-static {v0}, Landroid/os/Debug;->getMemoryInfo(Landroid/os/Debug$MemoryInfo;)V

    .line 34
    invoke-static {}, Landroid/os/Debug;->getNativeHeapAllocatedSize()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/libraries/memorymonitor/a;->a:J

    .line 35
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->otherPrivateDirty:I

    int-to-long v2, v1

    shl-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/libraries/memorymonitor/a;->b:J

    .line 36
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->otherPss:I

    int-to-long v2, v1

    shl-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/libraries/memorymonitor/a;->c:J

    .line 37
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->dalvikPrivateDirty:I

    int-to-long v2, v1

    shl-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/libraries/memorymonitor/a;->d:J

    .line 38
    iget v1, v0, Landroid/os/Debug$MemoryInfo;->dalvikPss:I

    int-to-long v2, v1

    shl-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/libraries/memorymonitor/a;->e:J

    .line 39
    invoke-virtual {v0}, Landroid/os/Debug$MemoryInfo;->getTotalPss()I

    .line 40
    return-void
.end method

.method constructor <init>(JJJJJJJJJ)V
    .locals 11

    .prologue
    .line 45
    move-object v3, p0

    move-wide/from16 v4, p9

    move-wide/from16 v6, p11

    move-wide/from16 v8, p13

    invoke-direct/range {v3 .. v9}, Lcom/google/android/libraries/memorymonitor/c;-><init>(JJJ)V

    .line 47
    iput-wide p1, p0, Lcom/google/android/libraries/memorymonitor/a;->b:J

    .line 48
    iput-wide p3, p0, Lcom/google/android/libraries/memorymonitor/a;->c:J

    .line 49
    move-wide/from16 v0, p5

    iput-wide v0, p0, Lcom/google/android/libraries/memorymonitor/a;->d:J

    .line 50
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lcom/google/android/libraries/memorymonitor/a;->e:J

    .line 51
    move-wide/from16 v0, p15

    iput-wide v0, p0, Lcom/google/android/libraries/memorymonitor/a;->a:J

    .line 54
    return-void
.end method
