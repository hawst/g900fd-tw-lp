.class public Lcom/google/android/libraries/curvular/cs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cv;


# instance fields
.field public a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Enum",
            "<*>;",
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "*>;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/cv;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/libraries/curvular/cq;


# direct methods
.method public varargs constructor <init>([Lcom/google/android/libraries/curvular/cu;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    new-instance v0, Lcom/google/android/libraries/curvular/cq;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/cq;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    .line 76
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    .line 79
    invoke-static {}, Lcom/google/b/c/hj;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/cs;->b:Ljava/util/Map;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/cs;->c:Ljava/util/List;

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    .line 84
    return-void
.end method

.method private a(Lcom/google/android/libraries/curvular/cu;Z)V
    .locals 4

    .prologue
    .line 95
    instance-of v0, p1, Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_1

    .line 97
    check-cast p1, Lcom/google/android/libraries/curvular/ah;

    .line 99
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    instance-of v0, p1, Lcom/google/android/libraries/curvular/ar;

    if-eqz v0, :cond_2

    .line 101
    check-cast p1, Lcom/google/android/libraries/curvular/ar;

    iget-object v0, p1, Lcom/google/android/libraries/curvular/ar;->a:[Lcom/google/android/libraries/curvular/cu;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    goto :goto_0

    .line 102
    :cond_2
    if-nez p2, :cond_3

    .line 103
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Can\'t handle non-attribute type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_3
    instance-of v0, p1, Lcom/google/android/libraries/curvular/cs;

    if-eqz v0, :cond_4

    .line 105
    check-cast p1, Lcom/google/android/libraries/curvular/cs;

    invoke-virtual {p1}, Lcom/google/android/libraries/curvular/cs;->a()Lcom/google/android/libraries/curvular/cq;

    move-result-object v0

    .line 106
    check-cast v0, Lcom/google/android/libraries/curvular/cv;

    iget-object v1, p0, Lcom/google/android/libraries/curvular/cs;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 107
    :cond_4
    instance-of v0, p1, Lcom/google/android/libraries/curvular/cv;

    if-eqz v0, :cond_5

    .line 108
    check-cast p1, Lcom/google/android/libraries/curvular/cv;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 109
    :cond_5
    if-eqz p1, :cond_0

    .line 112
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t handle child"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cq;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 188
    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ah;

    .line 190
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 191
    add-int/lit8 v1, v1, 0x1

    .line 193
    :cond_1
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v0

    sget-object v4, Lcom/google/android/libraries/curvular/g;->Y:Lcom/google/android/libraries/curvular/g;

    if-ne v0, v4, :cond_0

    .line 194
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/android/libraries/curvular/cq;->d:Z

    goto :goto_0

    .line 197
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    sub-int/2addr v0, v1

    .line 201
    new-array v1, v1, [Lcom/google/android/libraries/curvular/ah;

    .line 203
    iget-object v3, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iput-object v1, v3, Lcom/google/android/libraries/curvular/cq;->b:[Lcom/google/android/libraries/curvular/ah;

    .line 207
    new-array v0, v0, [Lcom/google/android/libraries/curvular/ah;

    .line 209
    iget-object v1, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iput-object v0, v1, Lcom/google/android/libraries/curvular/cq;->c:[Lcom/google/android/libraries/curvular/ah;

    .line 214
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ah;

    .line 215
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 216
    iget-object v1, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iget-object v5, v1, Lcom/google/android/libraries/curvular/cq;->b:[Lcom/google/android/libraries/curvular/ah;

    add-int/lit8 v1, v3, 0x1

    aput-object v0, v5, v3

    move v3, v1

    goto :goto_1

    .line 218
    :cond_3
    iget-object v1, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iget-object v5, v1, Lcom/google/android/libraries/curvular/cq;->c:[Lcom/google/android/libraries/curvular/ah;

    add-int/lit8 v1, v2, 0x1

    aput-object v0, v5, v2

    move v2, v1

    .line 220
    goto :goto_1

    .line 223
    :cond_4
    iget-object v1, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/libraries/curvular/cs;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/libraries/curvular/cv;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/curvular/cv;

    iput-object v0, v1, Lcom/google/android/libraries/curvular/cq;->j:[Lcom/google/android/libraries/curvular/cv;

    .line 226
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iget-object v1, p0, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    iput-object v1, v0, Lcom/google/android/libraries/curvular/cq;->a:Lcom/google/android/libraries/curvular/bk;

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    return-object v0
.end method

.method public final a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/ah",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            "*>;)",
            "Lcom/google/android/libraries/curvular/cs;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 133
    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/cn;

    .line 137
    instance-of v1, v0, Lcom/google/android/libraries/curvular/g;

    if-eqz v1, :cond_0

    .line 138
    sget-object v2, Lcom/google/android/libraries/curvular/cr;->a:[I

    move-object v1, v0

    check-cast v1, Lcom/google/android/libraries/curvular/g;

    invoke-virtual {v1}, Lcom/google/android/libraries/curvular/g;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 165
    :cond_0
    sget-object v1, Lcom/google/android/libraries/curvular/cr;->b:[I

    invoke-interface {v0}, Lcom/google/android/libraries/curvular/cn;->a()Lcom/google/android/libraries/curvular/co;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/curvular/co;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    .line 181
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 140
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iput-object p1, v0, Lcom/google/android/libraries/curvular/cq;->e:Lcom/google/android/libraries/curvular/ah;

    .line 183
    :goto_0
    return-object p0

    .line 144
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iput-object p1, v0, Lcom/google/android/libraries/curvular/cq;->g:Lcom/google/android/libraries/curvular/ah;

    goto :goto_0

    .line 148
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iput-object p1, v0, Lcom/google/android/libraries/curvular/cq;->f:Lcom/google/android/libraries/curvular/ah;

    goto :goto_0

    .line 151
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iput-object p1, v0, Lcom/google/android/libraries/curvular/cq;->h:Lcom/google/android/libraries/curvular/ah;

    goto :goto_0

    .line 154
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->d:Lcom/google/android/libraries/curvular/cq;

    iput-object p1, v0, Lcom/google/android/libraries/curvular/cq;->i:Lcom/google/android/libraries/curvular/ah;

    goto :goto_0

    .line 158
    :pswitch_5
    invoke-interface {p1, v3, v3}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bk;

    iput-object v0, p0, Lcom/google/android/libraries/curvular/cs;->a:Lcom/google/android/libraries/curvular/bk;

    goto :goto_0

    .line 167
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->b:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 171
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->b:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ah;

    .line 172
    if-eqz v0, :cond_1

    .line 173
    check-cast v0, Lcom/google/android/libraries/curvular/al;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/al;->a(Lcom/google/android/libraries/curvular/ah;)V

    goto :goto_0

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/cs;->b:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v1

    new-instance v2, Lcom/google/android/libraries/curvular/al;

    .line 176
    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Lcom/google/android/libraries/curvular/al;-><init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/ah;)V

    .line 175
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 165
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/cu;Z)V

    .line 128
    return-object p0
.end method

.method public final varargs a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    .line 123
    return-object p0
.end method

.method public a([Lcom/google/android/libraries/curvular/cu;Z)V
    .locals 3

    .prologue
    .line 89
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 90
    invoke-direct {p0, v2, p2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/cu;Z)V

    .line 89
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method
