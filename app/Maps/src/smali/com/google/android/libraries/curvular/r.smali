.class final Lcom/google/android/libraries/curvular/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field a:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field b:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/view/View;


# direct methods
.method private constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iput-object p1, p0, Lcom/google/android/libraries/curvular/r;->c:Landroid/view/View;

    .line 177
    return-void
.end method

.method static synthetic a(Landroid/view/View;)Lcom/google/android/libraries/curvular/r;
    .locals 2

    .prologue
    .line 169
    sget v0, Lcom/google/android/libraries/curvular/bh;->b:I

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/r;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/curvular/r;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/r;-><init>(Landroid/view/View;)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    sget v1, Lcom/google/android/libraries/curvular/bh;->b:I

    invoke-virtual {p0, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 181
    iget-object v0, p0, Lcom/google/android/libraries/curvular/r;->c:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->f(Landroid/view/View;)Lcom/google/android/libraries/curvular/ce;

    move-result-object v0

    .line 182
    if-nez v0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    if-eqz p2, :cond_2

    iget-object v1, p0, Lcom/google/android/libraries/curvular/r;->a:Lcom/google/android/libraries/curvular/b/i;

    if-eqz v1, :cond_2

    .line 186
    iget-object v1, p0, Lcom/google/android/libraries/curvular/r;->a:Lcom/google/android/libraries/curvular/b/i;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v1, v0, v2}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 187
    :cond_2
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/curvular/r;->b:Lcom/google/android/libraries/curvular/b/i;

    if-eqz v1, :cond_0

    .line 188
    iget-object v1, p0, Lcom/google/android/libraries/curvular/r;->b:Lcom/google/android/libraries/curvular/b/i;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v1, v0, v2}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
