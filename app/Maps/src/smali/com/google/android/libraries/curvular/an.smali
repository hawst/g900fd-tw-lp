.class public Lcom/google/android/libraries/curvular/an;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cp;


# instance fields
.field private final a:[Lcom/google/android/libraries/curvular/cp;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/cp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cp;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/curvular/cp;

    iput-object v0, p0, Lcom/google/android/libraries/curvular/an;->a:[Lcom/google/android/libraries/curvular/cp;

    .line 23
    return-void
.end method

.method public varargs constructor <init>([Lcom/google/android/libraries/curvular/cp;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    array-length v0, p1

    invoke-static {p1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/curvular/cp;

    iput-object v0, p0, Lcom/google/android/libraries/curvular/an;->a:[Lcom/google/android/libraries/curvular/cp;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;Ljava/lang/Object;Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Ljava/lang/Object;",
            "TT;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 28
    iget-object v2, p0, Lcom/google/android/libraries/curvular/an;->a:[Lcom/google/android/libraries/curvular/cp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 29
    invoke-interface {v4, p1, p2, p3, p4}, Lcom/google/android/libraries/curvular/cp;->a(Ljava/lang/Enum;Ljava/lang/Object;Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 30
    const/4 v0, 0x1

    .line 33
    :cond_0
    return v0

    .line 28
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
