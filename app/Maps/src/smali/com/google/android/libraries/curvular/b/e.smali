.class public Lcom/google/android/libraries/curvular/b/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/curvular/b/c",
            "<**>;>;>;"
        }
    .end annotation
.end field

.field static b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/curvular/b/a",
            "<*>;>;>;"
        }
    .end annotation
.end field

.field private static final c:Lcom/google/android/libraries/curvular/b/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/libraries/curvular/b/f;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/b/f;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/b/e;->a:Ljava/lang/ThreadLocal;

    .line 86
    new-instance v0, Lcom/google/android/libraries/curvular/b/g;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/b/g;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/b/e;->b:Ljava/lang/ThreadLocal;

    .line 175
    new-instance v0, Lcom/google/android/libraries/curvular/b/h;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/b/h;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/b/e;->c:Lcom/google/android/libraries/curvular/b/h;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 295
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 297
    return-void
.end method

.method public static varargs a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Object;",
            "[",
            "Ljava/lang/Class",
            "<*>;)",
            "Lcom/google/android/libraries/curvular/b/i",
            "<TT;TV;>;"
        }
    .end annotation

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/libraries/curvular/b/e;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/b/c;

    .line 245
    sget-boolean v1, Lcom/google/android/libraries/curvular/bo;->a:Z

    if-eqz v1, :cond_1

    .line 246
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/b/c;->a([Ljava/lang/Class;)V

    .line 248
    :cond_1
    return-object v0
.end method

.method public static a(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 98
    const-class v0, Ljava/lang/Integer;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/b/e;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public static a(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 167
    new-instance v1, Lcom/google/android/libraries/curvular/b/a;

    invoke-direct {v1, p0, p1}, Lcom/google/android/libraries/curvular/b/a;-><init>(ILjava/lang/Class;)V

    .line 168
    sget-object v0, Lcom/google/android/libraries/curvular/b/e;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iget-object v2, v1, Lcom/google/android/libraries/curvular/b/a;->c:Ljava/lang/Object;

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    iget-object v0, v1, Lcom/google/android/libraries/curvular/b/a;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            "V:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 256
    const-class v0, Lcom/google/android/libraries/curvular/b/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    sget-object v2, Lcom/google/android/libraries/curvular/b/e;->c:Lcom/google/android/libraries/curvular/b/h;

    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v1

    .line 257
    sget-object v0, Lcom/google/android/libraries/curvular/b/e;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    sget-object v2, Lcom/google/android/libraries/curvular/b/c;->a:Lcom/google/android/libraries/curvular/b/c;

    invoke-interface {v0, v1, v2}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    return-object v1
.end method

.method public static a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 222
    sget-object v0, Lcom/google/android/libraries/curvular/b/e;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(I)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 105
    const-class v0, Ljava/lang/Float;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/b/e;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    return-object v0
.end method

.method static b(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    .line 266
    const-class v0, Lcom/google/android/libraries/curvular/b/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    sget-object v2, Lcom/google/android/libraries/curvular/b/e;->c:Lcom/google/android/libraries/curvular/b/h;

    invoke-static {v0, v1, v2}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 235
    sget-object v0, Lcom/google/android/libraries/curvular/b/e;->a:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p0}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Argument shouldn\'t be a proxy object: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v1}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 237
    :cond_2
    return-object p0
.end method

.method public static c(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 112
    const-class v0, Ljava/lang/Boolean;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/b/e;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method

.method public static d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 119
    const-class v0, Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/b/e;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public static e(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 127
    const-class v0, Ljava/lang/CharSequence;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/b/e;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    return-object v0
.end method
