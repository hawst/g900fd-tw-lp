.class public Lcom/google/android/libraries/curvular/c/j;
.super Landroid/support/v7/widget/bk;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/bk",
        "<",
        "Lcom/google/android/libraries/curvular/c/l;",
        ">;"
    }
.end annotation


# instance fields
.field public final c:Lcom/google/android/libraries/curvular/bm;

.field d:Lcom/google/android/libraries/curvular/c/k;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v7/widget/bk;-><init>()V

    .line 29
    new-instance v0, Lcom/google/android/libraries/curvular/bm;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/curvular/bm;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    .line 30
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/curvular/c/j;->a(Z)V

    .line 31
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/bm;->b(I)I

    move-result v0

    return v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/ce;
    .locals 4

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/libraries/curvular/c/l;

    iget-object v1, p0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v2, v1, Lcom/google/android/libraries/curvular/bm;->c:Lcom/google/android/libraries/curvular/bd;

    invoke-virtual {v1, p2}, Lcom/google/android/libraries/curvular/bm;->c(I)Ljava/lang/Class;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v2, v1, p1, v3}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v2, p2}, Lcom/google/android/libraries/curvular/bm;->c(I)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/curvular/c/l;-><init>(Landroid/view/View;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final synthetic a(Landroid/support/v7/widget/ce;)V
    .locals 3

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/libraries/curvular/c/l;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/j;->d:Lcom/google/android/libraries/curvular/c/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/j;->d:Lcom/google/android/libraries/curvular/c/k;

    iget-object v1, p1, Lcom/google/android/libraries/curvular/c/l;->k:Ljava/lang/Class;

    iget-object v2, p1, Lcom/google/android/libraries/curvular/c/l;->a:Landroid/view/View;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/cq;->f(Landroid/view/View;)Lcom/google/android/libraries/curvular/ce;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/libraries/curvular/c/k;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Landroid/support/v7/widget/ce;I)V
    .locals 2

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/libraries/curvular/c/l;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v1, p1, Lcom/google/android/libraries/curvular/c/l;->a:Landroid/view/View;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bm;->a(Landroid/view/View;I)Landroid/view/View;

    return-void
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c/j;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/bn;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bn;->b:Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method
