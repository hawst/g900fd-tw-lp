.class public final enum Lcom/google/android/libraries/curvular/f;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/libraries/curvular/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/libraries/curvular/f;

.field public static final enum b:Lcom/google/android/libraries/curvular/f;

.field public static final enum c:Lcom/google/android/libraries/curvular/f;

.field public static final enum d:Lcom/google/android/libraries/curvular/f;

.field public static final enum e:Lcom/google/android/libraries/curvular/f;

.field public static final enum f:Lcom/google/android/libraries/curvular/f;

.field public static final enum g:Lcom/google/android/libraries/curvular/f;

.field private static final synthetic h:[Lcom/google/android/libraries/curvular/f;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/google/android/libraries/curvular/f;

    const-string v1, "COLOR"

    invoke-direct {v0, v1, v3}, Lcom/google/android/libraries/curvular/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/curvular/f;->a:Lcom/google/android/libraries/curvular/f;

    .line 32
    new-instance v0, Lcom/google/android/libraries/curvular/f;

    const-string v1, "DIMENSION"

    invoke-direct {v0, v1, v4}, Lcom/google/android/libraries/curvular/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/curvular/f;->b:Lcom/google/android/libraries/curvular/f;

    .line 33
    new-instance v0, Lcom/google/android/libraries/curvular/f;

    const-string v1, "DRAWABLE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/libraries/curvular/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    .line 34
    new-instance v0, Lcom/google/android/libraries/curvular/f;

    const-string v1, "LAYOUT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/libraries/curvular/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/curvular/f;->d:Lcom/google/android/libraries/curvular/f;

    .line 35
    new-instance v0, Lcom/google/android/libraries/curvular/f;

    const-string v1, "RAW"

    invoke-direct {v0, v1, v7}, Lcom/google/android/libraries/curvular/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/curvular/f;->e:Lcom/google/android/libraries/curvular/f;

    .line 36
    new-instance v0, Lcom/google/android/libraries/curvular/f;

    const-string v1, "STRING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/curvular/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/curvular/f;->f:Lcom/google/android/libraries/curvular/f;

    .line 37
    new-instance v0, Lcom/google/android/libraries/curvular/f;

    const-string v1, "TEXT_STYLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/curvular/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/libraries/curvular/f;->g:Lcom/google/android/libraries/curvular/f;

    .line 30
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->a:Lcom/google/android/libraries/curvular/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/libraries/curvular/f;->b:Lcom/google/android/libraries/curvular/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/libraries/curvular/f;->d:Lcom/google/android/libraries/curvular/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/libraries/curvular/f;->e:Lcom/google/android/libraries/curvular/f;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/libraries/curvular/f;->f:Lcom/google/android/libraries/curvular/f;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/libraries/curvular/f;->g:Lcom/google/android/libraries/curvular/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/libraries/curvular/f;->h:[Lcom/google/android/libraries/curvular/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/libraries/curvular/f;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/libraries/curvular/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/libraries/curvular/f;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/libraries/curvular/f;->h:[Lcom/google/android/libraries/curvular/f;

    invoke-virtual {v0}, [Lcom/google/android/libraries/curvular/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/curvular/f;

    return-object v0
.end method
