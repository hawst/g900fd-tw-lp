.class public Lcom/google/android/libraries/curvular/a/c;
.super Lcom/google/android/libraries/curvular/a/g;
.source "PG"


# direct methods
.method public constructor <init>(II)V
    .locals 4

    .prologue
    .line 10
    const-string v0, "Argument place holders up to index %s were specified when building the method chain, however only %s expected substitution types were provided."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 13
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 14
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 10
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/libraries/curvular/a/g;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method
