.class public final Lcom/google/android/libraries/stitch/a/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private volatile a:Lcom/google/android/libraries/stitch/a/b;

.field private final b:Ljava/lang/Object;

.field private final c:Lcom/google/android/libraries/stitch/a/f;

.field private final d:Z


# direct methods
.method constructor <init>(ZLcom/google/android/libraries/stitch/a/f;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/e;->b:Ljava/lang/Object;

    .line 51
    iput-boolean p1, p0, Lcom/google/android/libraries/stitch/a/e;->d:Z

    .line 52
    iput-object p2, p0, Lcom/google/android/libraries/stitch/a/e;->c:Lcom/google/android/libraries/stitch/a/f;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/google/android/libraries/stitch/a/b;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/e;->a:Lcom/google/android/libraries/stitch/a/b;

    if-nez v0, :cond_3

    .line 62
    iget-object v1, p0, Lcom/google/android/libraries/stitch/a/e;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/e;->a:Lcom/google/android/libraries/stitch/a/b;

    if-nez v0, :cond_2

    .line 64
    new-instance v0, Lcom/google/android/libraries/stitch/a/b;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/stitch/a/b;-><init>(Landroid/content/Context;)V

    .line 66
    iget-boolean v2, p0, Lcom/google/android/libraries/stitch/a/e;->d:Z

    if-eqz v2, :cond_0

    .line 67
    invoke-static {p1}, Lcom/google/android/libraries/stitch/a/b;->a(Landroid/content/Context;)Lcom/google/android/libraries/stitch/a/b;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/libraries/stitch/a/b;->b:Lcom/google/android/libraries/stitch/a/b;

    .line 70
    :cond_0
    iget-object v2, p0, Lcom/google/android/libraries/stitch/a/e;->c:Lcom/google/android/libraries/stitch/a/f;

    if-eqz v2, :cond_1

    .line 71
    iget-object v2, p0, Lcom/google/android/libraries/stitch/a/e;->c:Lcom/google/android/libraries/stitch/a/f;

    invoke-interface {v2, p1, v0}, Lcom/google/android/libraries/stitch/a/f;->a(Landroid/content/Context;Lcom/google/android/libraries/stitch/a/b;)V

    .line 74
    :cond_1
    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/e;->a:Lcom/google/android/libraries/stitch/a/b;

    .line 76
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/e;->a:Lcom/google/android/libraries/stitch/a/b;

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
