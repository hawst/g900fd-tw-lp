.class public Lcom/google/android/libraries/memorymonitor/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/Runtime;


# instance fields
.field public final f:J

.field public final g:J

.field public final h:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/memorymonitor/c;->a:Ljava/lang/Runtime;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    sget-object v0, Lcom/google/android/libraries/memorymonitor/c;->a:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/memorymonitor/c;->g:J

    .line 21
    sget-object v0, Lcom/google/android/libraries/memorymonitor/c;->a:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/memorymonitor/c;->h:J

    .line 22
    iget-wide v0, p0, Lcom/google/android/libraries/memorymonitor/c;->g:J

    sget-object v2, Lcom/google/android/libraries/memorymonitor/c;->a:Ljava/lang/Runtime;

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/libraries/memorymonitor/c;->f:J

    .line 23
    return-void
.end method

.method constructor <init>(JJJ)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-wide p1, p0, Lcom/google/android/libraries/memorymonitor/c;->f:J

    .line 28
    iput-wide p3, p0, Lcom/google/android/libraries/memorymonitor/c;->g:J

    .line 29
    iput-wide p5, p0, Lcom/google/android/libraries/memorymonitor/c;->h:J

    .line 30
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "dalvikHeapAllocatedB"

    iget-wide v2, p0, Lcom/google/android/libraries/memorymonitor/c;->f:J

    .line 51
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "dalvikHeapSizeB"

    iget-wide v2, p0, Lcom/google/android/libraries/memorymonitor/c;->g:J

    .line 52
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "dalvikMaxHeapSizeB"

    iget-wide v2, p0, Lcom/google/android/libraries/memorymonitor/c;->h:J

    .line 53
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 54
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
