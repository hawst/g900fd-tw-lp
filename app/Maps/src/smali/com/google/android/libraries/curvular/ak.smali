.class public Lcom/google/android/libraries/curvular/ak;
.super Lcom/google/android/libraries/curvular/ai;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/google/android/libraries/curvular/ce;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/libraries/curvular/ai",
        "<TV;TT;>;"
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/libraries/curvular/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/am",
            "<TV;TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Lcom/google/android/libraries/curvular/am",
            "<TV;TT;>;)V"
        }
    .end annotation

    .prologue
    .line 91
    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/ai;-><init>(Ljava/lang/Enum;)V

    .line 92
    iput-object p2, p0, Lcom/google/android/libraries/curvular/ak;->a:Lcom/google/android/libraries/curvular/am;

    .line 93
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Landroid/content/Context;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ak;->a:Lcom/google/android/libraries/curvular/am;

    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/curvular/am;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    new-instance v1, Lcom/google/android/libraries/curvular/a/e;

    invoke-direct {v1, v0, p0}, Lcom/google/android/libraries/curvular/a/e;-><init>(Ljava/lang/Exception;Lcom/google/android/libraries/curvular/ah;)V

    .line 102
    invoke-virtual {v0}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/a/e;->setStackTrace([Ljava/lang/StackTraceElement;)V

    .line 103
    throw v1
.end method
