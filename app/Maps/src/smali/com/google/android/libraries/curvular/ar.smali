.class public Lcom/google/android/libraries/curvular/ar;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cu;


# instance fields
.field final a:[Lcom/google/android/libraries/curvular/cu;


# direct methods
.method private constructor <init>([Lcom/google/android/libraries/curvular/cu;Z)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    if-eqz p2, :cond_0

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-static {v0, p1}, Lcom/google/android/libraries/curvular/ar;->a(Ljava/util/Map;[Lcom/google/android/libraries/curvular/cu;)V

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/android/libraries/curvular/cu;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/libraries/curvular/cu;

    :goto_0
    iput-object v0, p0, Lcom/google/android/libraries/curvular/ar;->a:[Lcom/google/android/libraries/curvular/cu;

    .line 26
    return-void

    :cond_0
    move-object v0, p1

    .line 24
    goto :goto_0
.end method

.method public static varargs a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/libraries/curvular/ar;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/curvular/ar;-><init>([Lcom/google/android/libraries/curvular/cu;Z)V

    return-object v0
.end method

.method private static a(Ljava/util/Map;[Lcom/google/android/libraries/curvular/cu;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Lcom/google/android/libraries/curvular/cu;",
            ">;[",
            "Lcom/google/android/libraries/curvular/cu;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 39
    move v1, v2

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_5

    .line 40
    aget-object v0, p1, v1

    instance-of v0, v0, Lcom/google/android/libraries/curvular/ar;

    if-eqz v0, :cond_1

    .line 41
    aget-object v0, p1, v1

    check-cast v0, Lcom/google/android/libraries/curvular/ar;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ar;->a:[Lcom/google/android/libraries/curvular/cu;

    invoke-static {p0, v0}, Lcom/google/android/libraries/curvular/ar;->a(Ljava/util/Map;[Lcom/google/android/libraries/curvular/cu;)V

    .line 39
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42
    :cond_1
    aget-object v0, p1, v1

    instance-of v0, v0, Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_4

    .line 43
    aget-object v0, p1, v1

    check-cast v0, Lcom/google/android/libraries/curvular/ah;

    .line 44
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0, v4, v4}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    .line 45
    :goto_2
    if-nez v3, :cond_3

    .line 46
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v3

    invoke-interface {p0, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    move v3, v2

    .line 44
    goto :goto_2

    .line 47
    :cond_3
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v3

    invoke-interface {p0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 48
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->a()Ljava/lang/Enum;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 51
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    aget-object v1, p1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x38

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "CompoundViewProperty cannot contain non-attribute type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_5
    return-void
.end method
