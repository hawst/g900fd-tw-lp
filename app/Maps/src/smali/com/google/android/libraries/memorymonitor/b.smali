.class public Lcom/google/android/libraries/memorymonitor/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Ljava/lang/Runtime;


# instance fields
.field final a:J

.field final b:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<[B>;"
        }
    .end annotation
.end field

.field c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/memorymonitor/b;->d:Ljava/lang/Runtime;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/libraries/memorymonitor/b;->a:J

    .line 27
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/memorymonitor/b;->b:Ljava/util/Stack;

    return-void
.end method

.method public static a()F
    .locals 4

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/libraries/memorymonitor/b;->d:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->totalMemory()J

    move-result-wide v0

    sget-object v2, Lcom/google/android/libraries/memorymonitor/b;->d:Ljava/lang/Runtime;

    invoke-virtual {v2}, Ljava/lang/Runtime;->freeMemory()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-float v0, v0

    sget-object v1, Lcom/google/android/libraries/memorymonitor/b;->d:Ljava/lang/Runtime;

    invoke-virtual {v1}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    long-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method
