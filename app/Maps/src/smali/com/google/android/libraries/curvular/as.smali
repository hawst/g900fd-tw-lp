.class public Lcom/google/android/libraries/curvular/as;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/au;
.implements Lcom/google/android/libraries/curvular/ax;


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/b;

.field private final b:Lcom/google/android/libraries/curvular/b;

.field private final c:Lcom/google/android/libraries/curvular/b;

.field private final d:Lcom/google/android/libraries/curvular/b;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/libraries/curvular/as;->a:Lcom/google/android/libraries/curvular/b;

    .line 26
    iput-object p2, p0, Lcom/google/android/libraries/curvular/as;->b:Lcom/google/android/libraries/curvular/b;

    .line 27
    iput-object p3, p0, Lcom/google/android/libraries/curvular/as;->c:Lcom/google/android/libraries/curvular/b;

    .line 28
    iput-object p4, p0, Lcom/google/android/libraries/curvular/as;->d:Lcom/google/android/libraries/curvular/b;

    .line 29
    return-void
.end method

.method private d(Landroid/content/Context;)Lcom/google/android/libraries/curvular/b;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 68
    iget v2, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v0, :cond_0

    .line 69
    :goto_0
    invoke-static {v1}, Lcom/google/android/libraries/curvular/ay;->e(Landroid/content/res/Configuration;)Z

    move-result v1

    .line 70
    if-eqz v1, :cond_2

    .line 71
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/curvular/as;->c:Lcom/google/android/libraries/curvular/b;

    .line 73
    :goto_1
    return-object v0

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/curvular/as;->d:Lcom/google/android/libraries/curvular/b;

    goto :goto_1

    .line 73
    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/curvular/as;->a:Lcom/google/android/libraries/curvular/b;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/curvular/as;->b:Lcom/google/android/libraries/curvular/b;

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;)F
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/as;->d(Landroid/content/Context;)Lcom/google/android/libraries/curvular/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/b;->a(Landroid/content/Context;)F

    move-result v0

    return v0
.end method

.method public final b_(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/as;->d(Landroid/content/Context;)Lcom/google/android/libraries/curvular/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/b;->b_(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public final c_(Landroid/content/Context;)I
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/as;->d(Landroid/content/Context;)Lcom/google/android/libraries/curvular/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/b;->c_(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 33
    if-ne p1, p0, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    instance-of v2, p1, Lcom/google/android/libraries/curvular/as;

    if-eqz v2, :cond_3

    .line 37
    check-cast p1, Lcom/google/android/libraries/curvular/as;

    .line 38
    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->a:Lcom/google/android/libraries/curvular/b;

    iget-object v3, p1, Lcom/google/android/libraries/curvular/as;->a:Lcom/google/android/libraries/curvular/b;

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->b:Lcom/google/android/libraries/curvular/b;

    iget-object v3, p1, Lcom/google/android/libraries/curvular/as;->b:Lcom/google/android/libraries/curvular/b;

    .line 39
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->c:Lcom/google/android/libraries/curvular/b;

    iget-object v3, p1, Lcom/google/android/libraries/curvular/as;->c:Lcom/google/android/libraries/curvular/b;

    .line 40
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->d:Lcom/google/android/libraries/curvular/b;

    iget-object v3, p1, Lcom/google/android/libraries/curvular/as;->d:Lcom/google/android/libraries/curvular/b;

    .line 41
    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 43
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 48
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->a:Lcom/google/android/libraries/curvular/b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->b:Lcom/google/android/libraries/curvular/b;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->c:Lcom/google/android/libraries/curvular/b;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/libraries/curvular/as;->d:Lcom/google/android/libraries/curvular/b;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
