.class Lcom/google/android/libraries/memorymonitor/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/libraries/memorymonitor/d;


# direct methods
.method constructor <init>(Lcom/google/android/libraries/memorymonitor/d;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/libraries/memorymonitor/g;->a:Lcom/google/android/libraries/memorymonitor/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 188
    move v0, v1

    .line 192
    :goto_0
    add-int/lit8 v2, v0, 0x1

    .line 193
    iget-object v3, p0, Lcom/google/android/libraries/memorymonitor/g;->a:Lcom/google/android/libraries/memorymonitor/d;

    monitor-enter v3

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/g;->a:Lcom/google/android/libraries/memorymonitor/d;

    iget-object v0, v0, Lcom/google/android/libraries/memorymonitor/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Lcom/google/android/libraries/memorymonitor/c;

    invoke-direct {v0}, Lcom/google/android/libraries/memorymonitor/c;-><init>()V

    .line 196
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/g;->a:Lcom/google/android/libraries/memorymonitor/d;

    iget-object v0, v0, Lcom/google/android/libraries/memorymonitor/d;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/memorymonitor/f;

    .line 197
    invoke-interface {v0}, Lcom/google/android/libraries/memorymonitor/f;->a()V

    goto :goto_1

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 201
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/g;->a:Lcom/google/android/libraries/memorymonitor/d;

    iget-object v0, v0, Lcom/google/android/libraries/memorymonitor/d;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 202
    const/4 v0, 0x4

    if-lt v2, v0, :cond_2

    .line 203
    new-instance v2, Lcom/google/android/libraries/memorymonitor/a;

    invoke-direct {v2}, Lcom/google/android/libraries/memorymonitor/a;-><init>()V

    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/g;->a:Lcom/google/android/libraries/memorymonitor/d;

    iget-object v0, v0, Lcom/google/android/libraries/memorymonitor/d;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/memorymonitor/e;

    .line 205
    invoke-interface {v0, v2}, Lcom/google/android/libraries/memorymonitor/e;->a(Lcom/google/android/libraries/memorymonitor/a;)V

    goto :goto_2

    :cond_1
    move v0, v1

    .line 210
    :goto_3
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 213
    const-wide/16 v2, 0x2ee

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    return-void

    :cond_2
    move v0, v2

    goto :goto_3
.end method
