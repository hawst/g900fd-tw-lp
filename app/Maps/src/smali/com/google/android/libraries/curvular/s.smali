.class final Lcom/google/android/libraries/curvular/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnAttachStateChangeListener;


# instance fields
.field a:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/widget/TextView;

.field private c:Z


# direct methods
.method private constructor <init>(Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lcom/google/android/libraries/curvular/s;->b:Landroid/widget/TextView;

    .line 112
    return-void
.end method

.method static final a(Landroid/widget/TextView;)Lcom/google/android/libraries/curvular/s;
    .locals 2

    .prologue
    .line 155
    sget v0, Lcom/google/android/libraries/curvular/bh;->e:I

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/s;

    .line 156
    if-nez v0, :cond_0

    .line 157
    new-instance v0, Lcom/google/android/libraries/curvular/s;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/s;-><init>(Landroid/widget/TextView;)V

    .line 158
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 159
    sget v1, Lcom/google/android/libraries/curvular/bh;->e:I

    invoke-virtual {p0, v1, v0}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 160
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 162
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 136
    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/s;->c:Z

    .line 137
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/s;->c:Z

    .line 117
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    .prologue
    .line 126
    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/s;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/curvular/s;->a:Lcom/google/android/libraries/curvular/b/i;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/curvular/s;->b:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->f(Landroid/view/View;)Lcom/google/android/libraries/curvular/ce;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_0

    .line 129
    iget-object v1, p0, Lcom/google/android/libraries/curvular/s;->a:Lcom/google/android/libraries/curvular/b/i;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v1, v0, v2}, Lcom/google/android/libraries/curvular/b/i;->a(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    :cond_0
    return-void
.end method

.method public final onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/s;->c:Z

    .line 147
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/s;->c:Z

    .line 152
    return-void
.end method
