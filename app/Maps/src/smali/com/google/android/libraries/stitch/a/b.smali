.class public Lcom/google/android/libraries/stitch/a/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/libraries/stitch/b/a;

.field private static final c:Lcom/google/android/libraries/stitch/a/e;


# instance fields
.field b:Lcom/google/android/libraries/stitch/a/b;

.field private d:Landroid/content/Context;

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Object;",
            "Ljava/util/List",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/stitch/a/i;",
            ">;"
        }
    .end annotation
.end field

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/libraries/stitch/b/a;

    const-string v1, "debug.binder.verification"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/stitch/b/a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/libraries/stitch/a/b;->a:Lcom/google/android/libraries/stitch/b/a;

    .line 34
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Lcom/google/android/libraries/stitch/a/e;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/libraries/stitch/a/k;

    invoke-direct {v2}, Lcom/google/android/libraries/stitch/a/k;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/libraries/stitch/a/e;-><init>(ZLcom/google/android/libraries/stitch/a/f;)V

    sput-object v0, Lcom/google/android/libraries/stitch/a/b;->c:Lcom/google/android/libraries/stitch/a/e;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->e:Ljava/util/Map;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->f:Ljava/util/Map;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->g:Ljava/util/HashSet;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->h:Ljava/util/ArrayList;

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/stitch/a/b;-><init>(Landroid/content/Context;Lcom/google/android/libraries/stitch/a/b;)V

    .line 72
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/stitch/a/b;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->e:Ljava/util/Map;

    .line 44
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->f:Ljava/util/Map;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->g:Ljava/util/HashSet;

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->h:Ljava/util/ArrayList;

    .line 80
    iput-object p1, p0, Lcom/google/android/libraries/stitch/a/b;->d:Landroid/content/Context;

    .line 81
    iput-object p2, p0, Lcom/google/android/libraries/stitch/a/b;->b:Lcom/google/android/libraries/stitch/a/b;

    .line 82
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 83
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/libraries/stitch/a/b;
    .locals 2

    .prologue
    .line 581
    sget-object v0, Lcom/google/android/libraries/stitch/a/b;->c:Lcom/google/android/libraries/stitch/a/e;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/stitch/a/e;->a(Landroid/content/Context;)Lcom/google/android/libraries/stitch/a/b;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 488
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    move v2, v3

    move-object v1, p0

    :goto_0
    instance-of v0, v1, Lcom/google/android/libraries/stitch/a/d;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Lcom/google/android/libraries/stitch/a/d;

    invoke-interface {v0}, Lcom/google/android/libraries/stitch/a/d;->a()Lcom/google/android/libraries/stitch/a/b;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "BinderContext must not return null Binder: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, v5

    :cond_1
    if-eqz v0, :cond_2

    .line 489
    :goto_1
    invoke-direct {v0, p1}, Lcom/google/android/libraries/stitch/a/b;->a(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 488
    :cond_2
    if-ne v1, v4, :cond_3

    const/4 v0, 0x1

    :goto_2
    or-int/2addr v0, v2

    instance-of v2, v1, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_4

    check-cast v1, Landroid/content/ContextWrapper;

    invoke-virtual {v1}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid ContextWrapper -- If this is a Robolectric test, have you called ActivityController.create()?"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    if-nez v0, :cond_6

    move-object v1, v4

    :cond_5
    :goto_3
    if-nez v1, :cond_7

    sget-object v0, Lcom/google/android/libraries/stitch/a/b;->c:Lcom/google/android/libraries/stitch/a/e;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/stitch/a/e;->a(Landroid/content/Context;)Lcom/google/android/libraries/stitch/a/b;

    move-result-object v0

    goto :goto_1

    :cond_6
    move-object v1, v5

    goto :goto_3

    :cond_7
    move v2, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Class;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 246
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/libraries/stitch/a/b;->b(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v1

    .line 247
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 248
    iget-object p0, p0, Lcom/google/android/libraries/stitch/a/b;->b:Lcom/google/android/libraries/stitch/a/b;

    .line 249
    if-nez p0, :cond_0

    .line 250
    return-object v0
.end method

.method private declared-synchronized b(Ljava/lang/Class;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 425
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Binder not initialized yet."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 428
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 429
    if-nez v0, :cond_4

    .line 430
    sget-object v0, Lcom/google/android/libraries/stitch/a/b;->a:Lcom/google/android/libraries/stitch/b/a;

    invoke-static {v0}, Lcom/google/android/libraries/stitch/b/c;->a(Lcom/google/android/libraries/stitch/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 432
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getAll() called for single-bound object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 437
    iget-object v2, p0, Lcom/google/android/libraries/stitch/a/b;->f:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    .line 441
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->g:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 442
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->g:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 443
    iget-boolean v3, p0, Lcom/google/android/libraries/stitch/a/b;->i:Z

    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/stitch/a/b;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 446
    :try_start_2
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 447
    :goto_1
    if-ge v1, v4, :cond_2

    .line 448
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/stitch/a/i;

    iget-object v5, p0, Lcom/google/android/libraries/stitch/a/b;->d:Landroid/content/Context;

    invoke-interface {v0, v5, p1, p0}, Lcom/google/android/libraries/stitch/a/i;->a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/stitch/a/b;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 447
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 451
    :cond_2
    :try_start_3
    iput-boolean v3, p0, Lcom/google/android/libraries/stitch/a/b;->i:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 455
    :cond_3
    monitor-exit p0

    return-object v2

    .line 451
    :catchall_1
    move-exception v0

    :try_start_4
    iput-boolean v3, p0, Lcom/google/android/libraries/stitch/a/b;->i:Z

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    move-object v2, v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/libraries/stitch/a/i;)Lcom/google/android/libraries/stitch/a/b;
    .locals 2

    .prologue
    .line 182
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/stitch/a/b;->i:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/stitch/a/c;

    const-string v1, "This binder is sealed for modification"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/stitch/a/c;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 184
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 185
    monitor-exit p0

    return-object p0
.end method

.method public declared-synchronized a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 361
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/libraries/stitch/a/b;->i:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/stitch/a/c;

    const-string v1, "This binder is sealed for modification"

    invoke-direct {v0, v1}, Lcom/google/android/libraries/stitch/a/c;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 363
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/libraries/stitch/a/b;->a:Lcom/google/android/libraries/stitch/b/a;

    invoke-static {v0}, Lcom/google/android/libraries/stitch/b/c;->a(Lcom/google/android/libraries/stitch/b/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 365
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempt to multibind single-bound object."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 369
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/stitch/a/b;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 370
    if-nez v0, :cond_2

    .line 371
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 372
    iget-object v1, p0, Lcom/google/android/libraries/stitch/a/b;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 374
    :cond_2
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 375
    monitor-exit p0

    return-void
.end method
