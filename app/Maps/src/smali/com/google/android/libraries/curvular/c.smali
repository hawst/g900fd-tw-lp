.class public Lcom/google/android/libraries/curvular/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/aq;
.implements Lcom/google/android/libraries/curvular/au;
.implements Lcom/google/android/libraries/curvular/aw;
.implements Lcom/google/android/libraries/curvular/bi;
.implements Lcom/google/android/libraries/curvular/bj;
.implements Lcom/google/android/libraries/curvular/bl;
.implements Lcom/google/android/libraries/curvular/cx;
.implements Ljava/io/Serializable;


# static fields
.field private static final d:Lcom/google/android/libraries/curvular/bf;


# instance fields
.field public final a:Lcom/google/android/libraries/curvular/f;

.field public final b:I

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/libraries/curvular/bf;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bf;-><init>()V

    sput-object v0, Lcom/google/android/libraries/curvular/c;->d:Lcom/google/android/libraries/curvular/bf;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/f;I)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    .line 46
    iput p2, p0, Lcom/google/android/libraries/curvular/c;->b:I

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/curvular/c;->c:Ljava/lang/String;

    .line 48
    return-void
.end method

.method private constructor <init>(Lcom/google/android/libraries/curvular/f;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/curvular/c;->b:I

    .line 53
    iput-object p2, p0, Lcom/google/android/libraries/curvular/c;->c:Ljava/lang/String;

    .line 54
    return-void
.end method

.method public static a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;
    .locals 4

    .prologue
    .line 215
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    new-instance v1, Lcom/google/android/libraries/curvular/ap;

    new-instance v2, Lcom/google/android/libraries/curvular/c;

    sget-object v3, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v2, v3, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    invoke-direct {v1, v2, p1, v0}, Lcom/google/android/libraries/curvular/ap;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;)V

    return-object v1
.end method

.method public static a(ILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;)Lcom/google/android/libraries/curvular/aw;
    .locals 3

    .prologue
    .line 236
    new-instance v0, Lcom/google/android/libraries/curvular/ap;

    new-instance v1, Lcom/google/android/libraries/curvular/c;

    sget-object v2, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v1, v2, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/libraries/curvular/ap;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;)V

    return-object v0
.end method

.method public static a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/google/android/libraries/curvular/d;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/curvular/d;-><init>(Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static varargs a(Ljava/lang/Integer;[Ljava/lang/Object;)Lcom/google/android/libraries/curvular/bi;
    .locals 1

    .prologue
    .line 260
    new-instance v0, Lcom/google/android/libraries/curvular/e;

    invoke-direct {v0, p0, p1}, Lcom/google/android/libraries/curvular/e;-><init>(Ljava/lang/Integer;[Ljava/lang/Object;)V

    return-object v0
.end method

.method public static a(I)Lcom/google/android/libraries/curvular/c;
    .locals 2

    .prologue
    .line 181
    new-instance v0, Lcom/google/android/libraries/curvular/c;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->a:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/libraries/curvular/c;
    .locals 2

    .prologue
    .line 206
    new-instance v0, Lcom/google/android/libraries/curvular/c;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;Ljava/lang/String;)V

    return-object v0
.end method

.method public static b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;
    .locals 4

    .prologue
    .line 227
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    new-instance v1, Lcom/google/android/libraries/curvular/ap;

    new-instance v2, Lcom/google/android/libraries/curvular/c;

    sget-object v3, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v2, v3, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    invoke-direct {v1, v2, p1, v0}, Lcom/google/android/libraries/curvular/ap;-><init>(Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;)V

    return-object v1
.end method

.method public static b(I)Lcom/google/android/libraries/curvular/c;
    .locals 2

    .prologue
    .line 191
    new-instance v0, Lcom/google/android/libraries/curvular/c;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->b:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    return-object v0
.end method

.method public static c(I)Lcom/google/android/libraries/curvular/c;
    .locals 2

    .prologue
    .line 201
    new-instance v0, Lcom/google/android/libraries/curvular/c;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    return-object v0
.end method

.method public static d(I)Lcom/google/android/libraries/curvular/c;
    .locals 2

    .prologue
    .line 275
    new-instance v0, Lcom/google/android/libraries/curvular/c;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->g:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    return-object v0
.end method

.method public static e(I)Lcom/google/android/libraries/curvular/bl;
    .locals 2

    .prologue
    .line 292
    new-instance v0, Lcom/google/android/libraries/curvular/c;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->e:Lcom/google/android/libraries/curvular/f;

    invoke-direct {v0, v1, p0}, Lcom/google/android/libraries/curvular/c;-><init>(Lcom/google/android/libraries/curvular/f;I)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)F
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->b:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_0

    .line 73
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    return v0

    .line 76
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public a_(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/libraries/curvular/c;->b:I

    .line 162
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/curvular/c;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->a:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_0

    .line 100
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    return v0

    .line 103
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public final b_(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->b:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_0

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    return v0

    .line 85
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public final c(Landroid/content/Context;)Landroid/content/res/ColorStateList;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->a:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_0

    .line 109
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public final c_(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->b:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_0

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0

    .line 94
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public final d(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->g:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_0

    .line 118
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v0

    return v0

    .line 121
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->a:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_2

    .line 136
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v0

    .line 137
    if-lez v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public final e(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->f:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_0

    .line 146
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 149
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    if-eqz p1, :cond_2

    instance-of v0, p1, Lcom/google/android/libraries/curvular/c;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/curvular/c;

    iget v0, v0, Lcom/google/android/libraries/curvular/c;->b:I

    iget v3, p0, Lcom/google/android/libraries/curvular/c;->b:I

    if-ne v0, v3, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/curvular/c;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    iget-object v3, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v3, :cond_2

    check-cast p1, Lcom/google/android/libraries/curvular/c;

    iget-object v0, p1, Lcom/google/android/libraries/curvular/c;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/libraries/curvular/c;->c:Ljava/lang/String;

    .line 62
    if-eq v0, v3, :cond_0

    if-eqz v0, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final f(Landroid/content/Context;)Landroid/graphics/Typeface;
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    sget-object v1, Lcom/google/android/libraries/curvular/f;->e:Lcom/google/android/libraries/curvular/f;

    if-ne v0, v1, :cond_1

    .line 155
    sget-object v1, Lcom/google/android/libraries/curvular/c;->d:Lcom/google/android/libraries/curvular/bf;

    invoke-virtual {p0, p1}, Lcom/google/android/libraries/curvular/c;->a_(Landroid/content/Context;)I

    move-result v2

    iget-object v0, v1, Lcom/google/android/libraries/curvular/bf;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    if-nez v0, :cond_0

    invoke-static {p1, v2}, Lcom/google/android/libraries/curvular/bf;->a(Landroid/content/Context;I)Landroid/graphics/Typeface;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/libraries/curvular/bf;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    .line 157
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/curvular/c;->a:Lcom/google/android/libraries/curvular/f;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/libraries/curvular/c;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/libraries/curvular/c;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
