.class public Lcom/google/android/libraries/curvular/DelegatingViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field public a:Lcom/google/android/libraries/curvular/at;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/libraries/curvular/DelegatingViewGroup;->a:Lcom/google/android/libraries/curvular/at;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/curvular/DelegatingViewGroup;->a:Lcom/google/android/libraries/curvular/at;

    .line 47
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/curvular/DelegatingViewGroup;->a:Lcom/google/android/libraries/curvular/at;

    if-nez v0, :cond_0

    .line 33
    invoke-virtual {p0, v2, v2}, Lcom/google/android/libraries/curvular/DelegatingViewGroup;->setMeasuredDimension(II)V

    .line 40
    :goto_0
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 38
    iget-object v1, p0, Lcom/google/android/libraries/curvular/DelegatingViewGroup;->a:Lcom/google/android/libraries/curvular/at;

    .line 39
    aget v1, v0, v2

    const/4 v2, 0x1

    aget v0, v0, v2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/curvular/DelegatingViewGroup;->setMeasuredDimension(II)V

    goto :goto_0
.end method
