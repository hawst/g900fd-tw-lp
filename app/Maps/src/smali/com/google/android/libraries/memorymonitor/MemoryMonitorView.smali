.class public Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;
.super Landroid/view/View;
.source "PG"


# static fields
.field static final a:F

.field private static final j:Ljava/lang/Runtime;


# instance fields
.field final b:J

.field final c:J

.field final d:J

.field final e:F

.field public f:Lcom/google/android/libraries/memorymonitor/d;

.field public g:Lcom/google/android/libraries/memorymonitor/e;

.field final h:Lcom/google/android/libraries/memorymonitor/b;

.field volatile i:Lcom/google/android/libraries/memorymonitor/a;

.field private k:Landroid/content/res/Resources;

.field private l:I

.field private m:I

.field private n:Landroid/graphics/Paint;

.field private o:Landroid/graphics/Paint;

.field private p:Landroid/graphics/Paint;

.field private q:F

.field private r:F

.field private s:F

.field private final t:Landroid/view/GestureDetector;

.field private final u:Lcom/google/android/libraries/memorymonitor/h;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 43
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 45
    sput-object v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->j:Ljava/lang/Runtime;

    invoke-virtual {v0}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v0

    long-to-double v0, v0

    const-wide/high16 v2, 0x4130000000000000L    # 1048576.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-float v0, v0

    sput v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 22

    .prologue
    .line 119
    invoke-direct/range {p0 .. p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 100
    new-instance v2, Lcom/google/android/libraries/memorymonitor/b;

    invoke-direct {v2}, Lcom/google/android/libraries/memorymonitor/b;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->h:Lcom/google/android/libraries/memorymonitor/b;

    .line 112
    new-instance v3, Lcom/google/android/libraries/memorymonitor/a;

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const-wide/16 v12, 0x0

    const-wide/16 v14, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    invoke-direct/range {v3 .. v21}, Lcom/google/android/libraries/memorymonitor/a;-><init>(JJJJJJJJJ)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->i:Lcom/google/android/libraries/memorymonitor/a;

    .line 115
    new-instance v2, Lcom/google/android/libraries/memorymonitor/h;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/libraries/memorymonitor/h;-><init>(Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->u:Lcom/google/android/libraries/memorymonitor/h;

    .line 121
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->k:Landroid/content/res/Resources;

    .line 123
    const/16 v2, 0x2d

    int-to-float v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->l:I

    .line 124
    const/16 v2, 0x8c

    int-to-float v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->m:I

    .line 126
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->n:Landroid/graphics/Paint;

    .line 128
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->o:Landroid/graphics/Paint;

    .line 129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->o:Landroid/graphics/Paint;

    const/4 v3, 0x1

    int-to-float v3, v3

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 131
    const/4 v2, 0x2

    int-to-float v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->r:F

    .line 132
    const/4 v2, 0x7

    int-to-float v2, v2

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->s:F

    .line 134
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->p:Landroid/graphics/Paint;

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->p:Landroid/graphics/Paint;

    const/high16 v3, -0x1000000

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->p:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->s:F

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 139
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "activity"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/ActivityManager;

    .line 140
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v3

    int-to-long v4, v3

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->b:J

    .line 141
    invoke-virtual {v2}, Landroid/app/ActivityManager;->getLargeMemoryClass()I

    move-result v2

    int-to-long v2, v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->c:J

    .line 143
    sget-object v2, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->j:Ljava/lang/Runtime;

    invoke-virtual {v2}, Ljava/lang/Runtime;->maxMemory()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->k:Landroid/content/res/Resources;

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->e:F

    .line 146
    new-instance v2, Landroid/view/GestureDetector;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->u:Lcom/google/android/libraries/memorymonitor/h;

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->t:Landroid/view/GestureDetector;

    .line 147
    return-void
.end method

.method private a(JIILandroid/graphics/Canvas;I)F
    .locals 9

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/high16 v6, 0x40400000    # 3.0f

    .line 313
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->n:Landroid/graphics/Paint;

    invoke-virtual {v0, p6}, Landroid/graphics/Paint;->setColor(I)V

    .line 315
    iget-wide v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 316
    :goto_0
    int-to-float v1, p3

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getWidth()I

    move-result v2

    mul-int/2addr v2, p4

    int-to-float v2, v2

    div-float/2addr v2, v6

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float v1, v3, v1

    sub-float/2addr v1, v0

    new-instance v3, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    add-float/2addr v4, v2

    add-float v5, v1, v0

    invoke-direct {v3, v2, v1, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v1, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->r:F

    iget v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->r:F

    iget-object v4, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->n:Landroid/graphics/Paint;

    invoke-virtual {p5, v3, v1, v2, v4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 320
    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getHeight()I

    move-result v1

    sub-int/2addr v1, p3

    int-to-float v1, v1

    sub-float/2addr v1, v0

    div-float v2, v0, v7

    add-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->s:F

    div-float/2addr v2, v7

    add-float/2addr v1, v2

    .line 321
    long-to-double v2, p1

    const-wide/high16 v4, 0x4130000000000000L    # 1048576.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x15

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "M"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getWidth()I

    move-result v3

    mul-int/2addr v3, p4

    int-to-float v3, v3

    div-float/2addr v3, v6

    iget-object v4, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->p:Landroid/graphics/Paint;

    invoke-virtual {p5, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 323
    return v0

    .line 315
    :cond_0
    long-to-float v0, p1

    iget-wide v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->q:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 195
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 196
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x2d

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->l:I

    .line 160
    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const/16 v1, 0x8c

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->m:I

    .line 161
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 202
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->g:Lcom/google/android/libraries/memorymonitor/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->f:Lcom/google/android/libraries/memorymonitor/d;

    iget-object v1, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->g:Lcom/google/android/libraries/memorymonitor/e;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/memorymonitor/d;->a(Lcom/google/android/libraries/memorymonitor/e;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->g:Lcom/google/android/libraries/memorymonitor/e;

    .line 203
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->n:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->q:F

    new-instance v0, Landroid/graphics/RectF;

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->q:F

    sub-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget v1, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->r:F

    iget v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->r:F

    iget-object v3, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 251
    iget-object v8, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->i:Lcom/google/android/libraries/memorymonitor/a;

    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->h:Lcom/google/android/libraries/memorymonitor/b;

    iget-wide v10, v0, Lcom/google/android/libraries/memorymonitor/b;->c:J

    const/4 v0, 0x0

    iget-wide v2, v8, Lcom/google/android/libraries/memorymonitor/a;->f:J

    sub-long/2addr v2, v10

    const/4 v4, 0x0

    const/4 v5, 0x0

    const v7, -0xff0100

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a(JIILandroid/graphics/Canvas;I)F

    move-result v1

    add-float/2addr v0, v1

    float-to-int v4, v0

    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-lez v0, :cond_0

    const/4 v5, 0x0

    const v7, -0x2dbfe3

    move-object v1, p0

    move-wide v2, v10

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a(JIILandroid/graphics/Canvas;I)F

    :cond_0
    iget-wide v2, v8, Lcom/google/android/libraries/memorymonitor/a;->a:J

    const/4 v5, 0x1

    const/16 v7, -0x100

    const/4 v4, 0x0

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a(JIILandroid/graphics/Canvas;I)F

    iget-wide v2, v8, Lcom/google/android/libraries/memorymonitor/a;->b:J

    const/4 v5, 0x2

    const v7, -0xc76804

    const/4 v4, 0x0

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a(JIILandroid/graphics/Canvas;I)F

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getHeight()I

    move-result v0

    int-to-float v1, v0

    iget-wide v2, v8, Lcom/google/android/libraries/memorymonitor/a;->g:J

    iget-wide v4, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    sub-float v2, v1, v0

    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->o:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x40400000    # 3.0f

    div-float v3, v0, v3

    iget-object v5, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->o:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getHeight()I

    move-result v0

    int-to-float v1, v0

    iget-wide v2, v8, Lcom/google/android/libraries/memorymonitor/a;->h:J

    iget-wide v4, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    sub-float v2, v1, v0

    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->o:Landroid/graphics/Paint;

    const v1, -0x2dbfe3

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x40400000    # 3.0f

    div-float v3, v0, v3

    iget-object v5, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->o:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 252
    return-void

    .line 251
    :cond_1
    long-to-float v0, v2

    iget-wide v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    long-to-float v2, v2

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->q:F

    mul-float/2addr v0, v2

    goto :goto_0

    :cond_2
    long-to-float v0, v2

    iget-wide v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    long-to-float v2, v2

    div-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->q:F

    mul-float/2addr v0, v2

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    const/high16 v4, -0x80000000

    .line 165
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 166
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 168
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 169
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 174
    if-ne v2, v5, :cond_0

    .line 182
    :goto_0
    if-ne v3, v5, :cond_2

    .line 190
    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->setMeasuredDimension(II)V

    .line 191
    return-void

    .line 176
    :cond_0
    if-ne v2, v4, :cond_1

    .line 177
    iget v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->l:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    goto :goto_0

    .line 179
    :cond_1
    iget v1, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->l:I

    goto :goto_0

    .line 184
    :cond_2
    if-ne v3, v4, :cond_3

    .line 185
    iget v2, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->m:I

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_1

    .line 187
    :cond_3
    iget v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->m:I

    goto :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    .line 236
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 239
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->u:Lcom/google/android/libraries/memorymonitor/h;

    iget v1, v0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    iget v2, v0, Lcom/google/android/libraries/memorymonitor/h;->a:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    const-string v1, "Inflating heap utilization to %.2f%% (%.2f MB)"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, v0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, v0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    sget v5, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->a:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/memorymonitor/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    iget-object v1, v1, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->h:Lcom/google/android/libraries/memorymonitor/b;

    iget v0, v0, Lcom/google/android/libraries/memorymonitor/h;->b:F

    invoke-static {}, Lcom/google/android/libraries/memorymonitor/b;->a()F

    move-result v2

    iget-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    sub-float/2addr v0, v2

    iget-wide v2, v1, Lcom/google/android/libraries/memorymonitor/b;->a:J

    long-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-long v2, v0

    add-long/2addr v2, v4

    :goto_0
    iget-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    cmp-long v0, v4, v2

    if-lez v0, :cond_0

    iget-object v0, v1, Lcom/google/android/libraries/memorymonitor/b;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    iget-object v0, v1, Lcom/google/android/libraries/memorymonitor/b;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v0, v0

    int-to-long v6, v0

    sub-long/2addr v4, v6

    iput-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    goto :goto_0

    :cond_0
    :goto_1
    iget-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_2

    iget-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    sub-long v4, v2, v4

    const-wide/32 v6, 0x100000

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    long-to-int v0, v4

    iget-object v4, v1, Lcom/google/android/libraries/memorymonitor/b;->b:Ljava/util/Stack;

    new-array v5, v0, [B

    invoke-virtual {v4, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, v1, Lcom/google/android/libraries/memorymonitor/b;->c:J

    goto :goto_1

    :cond_1
    iget-object v1, v0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    iget-object v1, v1, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->i:Lcom/google/android/libraries/memorymonitor/a;

    const-string v2, "Red: Artificially inflated Dalvik heap alloc.\nGreen: Dalvik heap alloc.\nYellow: Native heap alloc\nBlue: Other private dirty (GL RAM)\nBlack line: Dalvik heap size: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-wide v4, v1, Lcom/google/android/libraries/memorymonitor/a;->g:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    iget-wide v6, v1, Lcom/google/android/libraries/memorymonitor/a;->h:J

    long-to-double v6, v6

    const-wide/high16 v8, 0x4130000000000000L    # 1048576.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    iget-object v1, v0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    iget-wide v8, v1, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->d:J

    long-to-double v8, v8

    const-wide/high16 v10, 0x4130000000000000L    # 1048576.0

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    iget-object v1, v0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    iget-wide v10, v1, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->b:J

    iget-object v1, v0, Lcom/google/android/libraries/memorymonitor/h;->c:Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;

    iget-wide v12, v1, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->c:J

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit16 v3, v3, 0xff

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MB\nRed line: Max Dalvik heap memory: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MB\nGrey background bounds: large heap size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MB (should be the same as the red line)\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Default heap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MB; large heap: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " MB"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/memorymonitor/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/memorymonitor/MemoryMonitorView;->t:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
