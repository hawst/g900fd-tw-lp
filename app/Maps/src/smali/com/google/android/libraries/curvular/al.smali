.class public Lcom/google/android/libraries/curvular/al;
.super Lcom/google/android/libraries/curvular/ai;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/google/android/libraries/curvular/ce;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/libraries/curvular/ai",
        "<TV;",
        "Ljava/util/List",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;TT;>;>;"
        }
    .end annotation
.end field

.field private b:Z


# direct methods
.method private constructor <init>(Ljava/lang/Enum;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/ai;-><init>(Ljava/lang/Enum;)V

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/curvular/al;->a:Ljava/util/List;

    .line 147
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/al;->b:Z

    .line 157
    return-void
.end method

.method protected constructor <init>(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/ah;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;TT;>;)V"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/google/android/libraries/curvular/al;-><init>(Ljava/lang/Enum;)V

    .line 152
    invoke-virtual {p0, p2}, Lcom/google/android/libraries/curvular/al;->a(Lcom/google/android/libraries/curvular/ah;)V

    .line 153
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 143
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/libraries/curvular/al;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/libraries/curvular/al;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v0, p1, p2}, Lcom/google/android/libraries/curvular/ah;->a(Lcom/google/android/libraries/curvular/ce;Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final a(Lcom/google/android/libraries/curvular/ah;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/ah",
            "<TV;TT;>;)V"
        }
    .end annotation

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/google/android/libraries/curvular/al;->b:Z

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/curvular/al;->b:Z

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/curvular/al;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
