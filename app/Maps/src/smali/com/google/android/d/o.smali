.class public final Lcom/google/android/d/o;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/android/d/r;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/o;",
            ">;"
        }
    .end annotation
.end field

.field static final d:Lcom/google/android/d/o;

.field private static volatile g:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:B

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 3411
    new-instance v0, Lcom/google/android/d/p;

    invoke-direct {v0}, Lcom/google/android/d/p;-><init>()V

    sput-object v0, Lcom/google/android/d/o;->PARSER:Lcom/google/n/ax;

    .line 3534
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/d/o;->g:Lcom/google/n/aw;

    .line 3850
    new-instance v0, Lcom/google/android/d/o;

    invoke-direct {v0}, Lcom/google/android/d/o;-><init>()V

    sput-object v0, Lcom/google/android/d/o;->d:Lcom/google/android/d/o;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 3354
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 3485
    iput-byte v0, p0, Lcom/google/android/d/o;->e:B

    .line 3513
    iput v0, p0, Lcom/google/android/d/o;->f:I

    .line 3355
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/d/o;->b:J

    .line 3356
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    .line 3357
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v7, 0x2

    .line 3363
    invoke-direct {p0}, Lcom/google/android/d/o;-><init>()V

    .line 3366
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    move v1, v0

    .line 3369
    :cond_0
    :goto_0
    if-nez v1, :cond_3

    .line 3370
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v4

    .line 3371
    sparse-switch v4, :sswitch_data_0

    .line 3376
    invoke-virtual {v3, v4, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v4

    if-nez v4, :cond_0

    move v1, v2

    .line 3378
    goto :goto_0

    :sswitch_0
    move v1, v2

    .line 3374
    goto :goto_0

    .line 3383
    :sswitch_1
    iget v4, p0, Lcom/google/android/d/o;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/d/o;->a:I

    .line 3384
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/d/o;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 3399
    :catch_0
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 3400
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3405
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_1

    .line 3406
    iget-object v1, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    .line 3408
    :cond_1
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/o;->au:Lcom/google/n/bn;

    throw v0

    .line 3388
    :sswitch_2
    and-int/lit8 v4, v0, 0x2

    if-eq v4, v7, :cond_2

    .line 3389
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    .line 3391
    or-int/lit8 v0, v0, 0x2

    .line 3393
    :cond_2
    iget-object v4, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    .line 3394
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 3393
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 3401
    :catch_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    .line 3402
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 3403
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 3405
    :cond_3
    and-int/lit8 v0, v0, 0x2

    if-ne v0, v7, :cond_4

    .line 3406
    iget-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    .line 3408
    :cond_4
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/o;->au:Lcom/google/n/bn;

    .line 3409
    return-void

    .line 3405
    :catchall_1
    move-exception v1

    move-object v8, v1

    move v1, v0

    move-object v0, v8

    goto :goto_1

    .line 3371
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 3352
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 3485
    iput-byte v0, p0, Lcom/google/android/d/o;->e:B

    .line 3513
    iput v0, p0, Lcom/google/android/d/o;->f:I

    .line 3353
    return-void
.end method

.method public static d()Lcom/google/android/d/o;
    .locals 1

    .prologue
    .line 3853
    sget-object v0, Lcom/google/android/d/o;->d:Lcom/google/android/d/o;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/android/d/q;
    .locals 1

    .prologue
    .line 3596
    new-instance v0, Lcom/google/android/d/q;

    invoke-direct {v0}, Lcom/google/android/d/q;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/o;",
            ">;"
        }
    .end annotation

    .prologue
    .line 3423
    sget-object v0, Lcom/google/android/d/o;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 3503
    invoke-virtual {p0}, Lcom/google/android/d/o;->c()I

    .line 3504
    iget v1, p0, Lcom/google/android/d/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v4, :cond_0

    .line 3505
    iget-wide v2, p0, Lcom/google/android/d/o;->b:J

    invoke-static {v4, v0}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    :cond_0
    move v1, v0

    .line 3507
    :goto_0
    iget-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 3508
    iget-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3507
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3510
    :cond_1
    iget-object v0, p0, Lcom/google/android/d/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 3511
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3487
    iget-byte v0, p0, Lcom/google/android/d/o;->e:B

    .line 3488
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 3498
    :cond_0
    :goto_0
    return v2

    .line 3489
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 3491
    :goto_1
    iget-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 3492
    iget-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/android/d/l;->d()Lcom/google/android/d/l;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/l;

    invoke-virtual {v0}, Lcom/google/android/d/l;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3493
    iput-byte v2, p0, Lcom/google/android/d/o;->e:B

    goto :goto_0

    .line 3491
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 3497
    :cond_3
    iput-byte v3, p0, Lcom/google/android/d/o;->e:B

    move v2, v3

    .line 3498
    goto :goto_0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 3515
    iget v0, p0, Lcom/google/android/d/o;->f:I

    .line 3516
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 3529
    :goto_0
    return v0

    .line 3519
    :cond_0
    iget v0, p0, Lcom/google/android/d/o;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_2

    .line 3520
    iget-wide v2, p0, Lcom/google/android/d/o;->b:J

    .line 3521
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    :goto_1
    move v2, v1

    move v3, v0

    .line 3523
    :goto_2
    iget-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 3524
    const/4 v4, 0x2

    iget-object v0, p0, Lcom/google/android/d/o;->c:Ljava/util/List;

    .line 3525
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 3523
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 3527
    :cond_1
    iget-object v0, p0, Lcom/google/android/d/o;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 3528
    iput v0, p0, Lcom/google/android/d/o;->f:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3346
    invoke-static {}, Lcom/google/android/d/o;->newBuilder()Lcom/google/android/d/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/d/q;->a(Lcom/google/android/d/o;)Lcom/google/android/d/q;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3346
    invoke-static {}, Lcom/google/android/d/o;->newBuilder()Lcom/google/android/d/q;

    move-result-object v0

    return-object v0
.end method
