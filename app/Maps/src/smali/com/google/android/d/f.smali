.class public final Lcom/google/android/d/f;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/android/d/k;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/f;",
            ">;"
        }
    .end annotation
.end field

.field static final g:Lcom/google/android/d/f;

.field private static volatile j:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field public a:I

.field public b:Z

.field public c:Ljava/lang/Object;

.field d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field e:Z

.field public f:I

.field private h:B

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 163
    new-instance v0, Lcom/google/android/d/g;

    invoke-direct {v0}, Lcom/google/android/d/g;-><init>()V

    sput-object v0, Lcom/google/android/d/f;->PARSER:Lcom/google/n/ax;

    .line 459
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/d/f;->j:Lcom/google/n/aw;

    .line 942
    new-instance v0, Lcom/google/android/d/f;

    invoke-direct {v0}, Lcom/google/android/d/f;-><init>()V

    sput-object v0, Lcom/google/android/d/f;->g:Lcom/google/android/d/f;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 81
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 395
    iput-byte v0, p0, Lcom/google/android/d/f;->h:B

    .line 426
    iput v0, p0, Lcom/google/android/d/f;->i:I

    .line 82
    iput-boolean v1, p0, Lcom/google/android/d/f;->b:Z

    .line 83
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/f;->c:Ljava/lang/Object;

    .line 84
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    .line 85
    iput-boolean v1, p0, Lcom/google/android/d/f;->e:Z

    .line 86
    iput v1, p0, Lcom/google/android/d/f;->f:I

    .line 87
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v8, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 93
    invoke-direct {p0}, Lcom/google/android/d/f;-><init>()V

    .line 96
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v1, v3

    .line 99
    :cond_0
    :goto_0
    if-nez v4, :cond_6

    .line 100
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 101
    sparse-switch v0, :sswitch_data_0

    .line 106
    invoke-virtual {v5, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v4, v2

    .line 108
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 104
    goto :goto_0

    .line 113
    :sswitch_1
    iget v0, p0, Lcom/google/android/d/f;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/d/f;->a:I

    .line 114
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/d/f;->b:Z
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    :catchall_0
    move-exception v0

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_1

    .line 158
    iget-object v1, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    .line 160
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/f;->au:Lcom/google/n/bn;

    throw v0

    :cond_2
    move v0, v3

    .line 114
    goto :goto_1

    .line 118
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 119
    iget v6, p0, Lcom/google/android/d/f;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/android/d/f;->a:I

    .line 120
    iput-object v0, p0, Lcom/google/android/d/f;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 153
    :catch_1
    move-exception v0

    .line 154
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 155
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124
    :sswitch_3
    and-int/lit8 v0, v1, 0x4

    if-eq v0, v8, :cond_3

    .line 125
    :try_start_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    .line 127
    or-int/lit8 v1, v1, 0x4

    .line 129
    :cond_3
    iget-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 130
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 129
    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 134
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->l()I

    move-result v0

    .line 135
    invoke-static {v0}, Lcom/google/android/d/h;->a(I)Lcom/google/android/d/h;

    move-result-object v6

    .line 136
    if-nez v6, :cond_4

    .line 137
    const/4 v6, 0x4

    invoke-virtual {v5, v6, v0}, Lcom/google/n/bo;->a(II)Lcom/google/n/bo;

    goto/16 :goto_0

    .line 139
    :cond_4
    iget v6, p0, Lcom/google/android/d/f;->a:I

    or-int/lit8 v6, v6, 0x8

    iput v6, p0, Lcom/google/android/d/f;->a:I

    .line 140
    iput v0, p0, Lcom/google/android/d/f;->f:I

    goto/16 :goto_0

    .line 145
    :sswitch_5
    iget v0, p0, Lcom/google/android/d/f;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/d/f;->a:I

    .line 146
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/d/f;->e:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_5
    move v0, v3

    goto :goto_2

    .line 157
    :cond_6
    and-int/lit8 v0, v1, 0x4

    if-ne v0, v8, :cond_7

    .line 158
    iget-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    .line 160
    :cond_7
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/f;->au:Lcom/google/n/bn;

    .line 161
    return-void

    .line 101
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 79
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 395
    iput-byte v0, p0, Lcom/google/android/d/f;->h:B

    .line 426
    iput v0, p0, Lcom/google/android/d/f;->i:I

    .line 80
    return-void
.end method

.method public static a([B)Lcom/google/android/d/f;
    .locals 1

    .prologue
    .line 481
    sget-object v0, Lcom/google/android/d/f;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, p0}, Lcom/google/n/ax;->b([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/f;

    return-object v0
.end method

.method public static d()Lcom/google/android/d/f;
    .locals 1

    .prologue
    .line 945
    sget-object v0, Lcom/google/android/d/f;->g:Lcom/google/android/d/f;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/android/d/j;
    .locals 1

    .prologue
    .line 521
    new-instance v0, Lcom/google/android/d/j;

    invoke-direct {v0}, Lcom/google/android/d/j;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/f;",
            ">;"
        }
    .end annotation

    .prologue
    .line 175
    sget-object v0, Lcom/google/android/d/f;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 407
    invoke-virtual {p0}, Lcom/google/android/d/f;->c()I

    .line 408
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 409
    iget-boolean v0, p0, Lcom/google/android/d/f;->b:Z

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 411
    :cond_0
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v5, :cond_1

    .line 412
    iget-object v0, p0, Lcom/google/android/d/f;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/f;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v5, v5}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    :cond_1
    move v3, v2

    .line 414
    :goto_2
    iget-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 415
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 414
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    move v0, v2

    .line 409
    goto :goto_0

    .line 412
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 417
    :cond_4
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_5

    .line 418
    iget v0, p0, Lcom/google/android/d/f;->f:I

    invoke-static {v6, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    if-ltz v0, :cond_7

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    .line 420
    :cond_5
    :goto_3
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_6

    .line 421
    const/4 v0, 0x5

    iget-boolean v3, p0, Lcom/google/android/d/f;->e:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_8

    :goto_4
    invoke-virtual {p1, v1}, Lcom/google/n/l;->a(I)V

    .line 423
    :cond_6
    iget-object v0, p0, Lcom/google/android/d/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 424
    return-void

    .line 418
    :cond_7
    int-to-long v4, v0

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    goto :goto_3

    :cond_8
    move v1, v2

    .line 421
    goto :goto_4
.end method

.method public final b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 397
    iget-byte v1, p0, Lcom/google/android/d/f;->h:B

    .line 398
    if-ne v1, v0, :cond_0

    .line 402
    :goto_0
    return v0

    .line 399
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 401
    :cond_1
    iput-byte v0, p0, Lcom/google/android/d/f;->h:B

    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 428
    iget v0, p0, Lcom/google/android/d/f;->i:I

    .line 429
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 454
    :goto_0
    return v0

    .line 432
    :cond_0
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_7

    .line 433
    iget-boolean v0, p0, Lcom/google/android/d/f;->b:Z

    .line 434
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 436
    :goto_1
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 438
    iget-object v0, p0, Lcom/google/android/d/f;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/f;->c:Ljava/lang/Object;

    :goto_2
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    :cond_1
    move v3, v1

    move v1, v2

    .line 440
    :goto_3
    iget-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 441
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/android/d/f;->d:Ljava/util/List;

    .line 442
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 440
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 438
    :cond_2
    check-cast v0, Lcom/google/n/f;

    goto :goto_2

    .line 444
    :cond_3
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_4

    .line 445
    iget v0, p0, Lcom/google/android/d/f;->f:I

    .line 446
    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    if-ltz v0, :cond_6

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    :goto_4
    add-int/2addr v0, v1

    add-int/2addr v3, v0

    .line 448
    :cond_4
    iget v0, p0, Lcom/google/android/d/f;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_5

    .line 449
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/d/f;->e:Z

    .line 450
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    .line 452
    :cond_5
    iget-object v0, p0, Lcom/google/android/d/f;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 453
    iput v0, p0, Lcom/google/android/d/f;->i:I

    goto/16 :goto_0

    .line 446
    :cond_6
    const/16 v0, 0xa

    goto :goto_4

    :cond_7
    move v1, v2

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/android/d/f;->newBuilder()Lcom/google/android/d/j;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/d/j;->a(Lcom/google/android/d/f;)Lcom/google/android/d/j;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/android/d/f;->newBuilder()Lcom/google/android/d/j;

    move-result-object v0

    return-object v0
.end method
