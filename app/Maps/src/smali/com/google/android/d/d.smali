.class public final Lcom/google/android/d/d;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/android/d/e;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/android/d/b;",
        "Lcom/google/android/d/d;",
        ">;",
        "Lcom/google/android/d/e;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:J

.field public c:Ljava/lang/Object;

.field private d:Lcom/google/n/ao;

.field private e:Lcom/google/n/ao;

.field private f:Lcom/google/n/ao;

.field private g:Z


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 1401
    sget-object v0, Lcom/google/android/d/b;->h:Lcom/google/android/d/b;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 1541
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/d;->d:Lcom/google/n/ao;

    .line 1600
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/d;->e:Lcom/google/n/ao;

    .line 1659
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/d;->c:Ljava/lang/Object;

    .line 1735
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/d;->f:Lcom/google/n/ao;

    .line 1402
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/d/b;)Lcom/google/android/d/d;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1463
    invoke-static {}, Lcom/google/android/d/b;->d()Lcom/google/android/d/b;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 1488
    :goto_0
    return-object p0

    .line 1464
    :cond_0
    iget v2, p1, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_7

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 1465
    iget-wide v2, p1, Lcom/google/android/d/b;->b:J

    iget v4, p0, Lcom/google/android/d/d;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/d/d;->a:I

    iput-wide v2, p0, Lcom/google/android/d/d;->b:J

    .line 1467
    :cond_1
    iget v2, p1, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_8

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 1468
    iget-object v2, p0, Lcom/google/android/d/d;->d:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1469
    iget v2, p0, Lcom/google/android/d/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/d/d;->a:I

    .line 1471
    :cond_2
    iget v2, p1, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_9

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 1472
    iget-object v2, p0, Lcom/google/android/d/d;->e:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1473
    iget v2, p0, Lcom/google/android/d/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/d/d;->a:I

    .line 1475
    :cond_3
    iget v2, p1, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 1476
    iget v2, p0, Lcom/google/android/d/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/d/d;->a:I

    .line 1477
    iget-object v2, p1, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/d;->c:Ljava/lang/Object;

    .line 1480
    :cond_4
    iget v2, p1, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 1481
    iget-object v2, p0, Lcom/google/android/d/d;->f:Lcom/google/n/ao;

    iget-object v3, p1, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->a(Lcom/google/n/ao;)V

    .line 1482
    iget v2, p0, Lcom/google/android/d/d;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/android/d/d;->a:I

    .line 1484
    :cond_5
    iget v2, p1, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_c

    :goto_6
    if-eqz v0, :cond_6

    .line 1485
    iget-boolean v0, p1, Lcom/google/android/d/b;->g:Z

    iget v1, p0, Lcom/google/android/d/d;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/android/d/d;->a:I

    iput-boolean v0, p0, Lcom/google/android/d/d;->g:Z

    .line 1487
    :cond_6
    iget-object v0, p1, Lcom/google/android/d/b;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_7
    move v2, v1

    .line 1464
    goto/16 :goto_1

    :cond_8
    move v2, v1

    .line 1467
    goto :goto_2

    :cond_9
    move v2, v1

    .line 1471
    goto :goto_3

    :cond_a
    move v2, v1

    .line 1475
    goto :goto_4

    :cond_b
    move v2, v1

    .line 1480
    goto :goto_5

    :cond_c
    move v0, v1

    .line 1484
    goto :goto_6
.end method

.method public final a(Lcom/google/i/a/k;)Lcom/google/android/d/d;
    .locals 2

    .prologue
    .line 1556
    if-nez p1, :cond_0

    .line 1557
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1559
    :cond_0
    iget-object v0, p0, Lcom/google/android/d/d;->d:Lcom/google/n/ao;

    iget-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    .line 1561
    iget v0, p0, Lcom/google/android/d/d;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/d/d;->a:I

    .line 1562
    return-object p0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1393
    new-instance v2, Lcom/google/android/d/b;

    invoke-direct {v2, p0}, Lcom/google/android/d/b;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/android/d/d;->a:I

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_5

    :goto_0
    iget-wide v4, p0, Lcom/google/android/d/d;->b:J

    iput-wide v4, v2, Lcom/google/android/d/b;->b:J

    and-int/lit8 v4, v3, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v4, v2, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/android/d/d;->d:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/d/d;->d:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v4, v2, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/android/d/d;->e:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/d/d;->e:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v4, v3, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v4, p0, Lcom/google/android/d/d;->c:Ljava/lang/Object;

    iput-object v4, v2, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    and-int/lit8 v4, v3, 0x10

    const/16 v5, 0x10

    if-ne v4, v5, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v4, v2, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    iget-object v5, p0, Lcom/google/android/d/d;->f:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/d/d;->f:Lcom/google/n/ao;

    iget-object v6, v6, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-object v5, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v6, v4, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    and-int/lit8 v1, v3, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/d/d;->g:Z

    iput-boolean v1, v2, Lcom/google/android/d/b;->g:Z

    iput v0, v2, Lcom/google/android/d/b;->a:I

    return-object v2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 1393
    check-cast p1, Lcom/google/android/d/b;

    invoke-virtual {p0, p1}, Lcom/google/android/d/d;->a(Lcom/google/android/d/b;)Lcom/google/android/d/d;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1492
    iget v0, p0, Lcom/google/android/d/d;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 1493
    iget-object v0, p0, Lcom/google/android/d/d;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/k;->d()Lcom/google/i/a/k;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/k;

    invoke-virtual {v0}, Lcom/google/i/a/k;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1504
    :goto_1
    return v0

    :cond_0
    move v0, v1

    .line 1492
    goto :goto_0

    .line 1498
    :cond_1
    iget v0, p0, Lcom/google/android/d/d;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_2

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 1499
    iget-object v0, p0, Lcom/google/android/d/d;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/android/d/t;->d()Lcom/google/android/d/t;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/t;

    invoke-virtual {v0}, Lcom/google/android/d/t;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    .line 1501
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1498
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1504
    goto :goto_1
.end method
