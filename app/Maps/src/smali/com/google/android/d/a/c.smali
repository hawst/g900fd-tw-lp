.class Lcom/google/android/d/a/c;
.super Landroid/os/Handler;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/d/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/d/a/b;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/d/a/c;->a:Lcom/google/android/d/a/b;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    .line 62
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/d/a/c;->a:Lcom/google/android/d/a/b;

    iget-object v1, v0, Lcom/google/android/d/a/b;->b:Lcom/google/android/d/a/a;

    .line 64
    if-nez v1, :cond_0

    .line 67
    iget v0, p1, Landroid/os/Message;->what:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Message "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " received after unbind."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    .line 71
    iget-object v2, p0, Lcom/google/android/d/a/c;->a:Lcom/google/android/d/a/b;

    const-string v3, "ssb_service:ssb_state"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/d/a/b;->c:[B

    .line 72
    iget-object v0, p0, Lcom/google/android/d/a/c;->a:Lcom/google/android/d/a/b;

    iget-object v0, v0, Lcom/google/android/d/a/b;->c:[B

    invoke-interface {v1, v0}, Lcom/google/android/d/a/a;->a([B)V

    goto :goto_0

    .line 74
    :cond_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0
.end method
