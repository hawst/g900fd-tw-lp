.class Lcom/google/android/d/a/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/d/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/d/a/b;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/d/a/d;->a:Lcom/google/android/d/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/d/a/d;->a:Lcom/google/android/d/a/b;

    iget-object v0, v0, Lcom/google/android/d/a/b;->b:Lcom/google/android/d/a/a;

    if-nez v0, :cond_0

    .line 182
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lcom/google/android/d/a/d;->a:Lcom/google/android/d/a/b;

    new-instance v1, Landroid/os/Messenger;

    invoke-direct {v1, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    iput-object v1, v0, Lcom/google/android/d/a/b;->a:Landroid/os/Messenger;

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/google/android/d/a/d;->a:Lcom/google/android/d/a/b;

    .line 177
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lcom/google/android/d/a/d;->a:Lcom/google/android/d/a/b;

    iget-object v1, v1, Lcom/google/android/d/a/b;->d:Landroid/os/Messenger;

    iput-object v1, v0, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 179
    iget-object v1, p0, Lcom/google/android/d/a/d;->a:Lcom/google/android/d/a/b;

    invoke-virtual {v0}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "ssb_service:ssb_package_name"

    iget-object v4, v1, Lcom/google/android/d/a/b;->f:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/android/d/a/b;->a:Landroid/os/Messenger;

    invoke-virtual {v1, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 181
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/d/a/d;->a:Lcom/google/android/d/a/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/d/a/b;->a:Landroid/os/Messenger;

    .line 165
    return-void
.end method
