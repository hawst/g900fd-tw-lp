.class public final Lcom/google/android/d/t;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/android/d/w;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/t;",
            ">;"
        }
    .end annotation
.end field

.field static final f:Lcom/google/android/d/t;

.field private static volatile i:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:Ljava/lang/Object;

.field c:Ljava/lang/Object;

.field d:Z

.field e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private g:B

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2593
    new-instance v0, Lcom/google/android/d/u;

    invoke-direct {v0}, Lcom/google/android/d/u;-><init>()V

    sput-object v0, Lcom/google/android/d/t;->PARSER:Lcom/google/n/ax;

    .line 2814
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/d/t;->i:Lcom/google/n/aw;

    .line 3304
    new-instance v0, Lcom/google/android/d/t;

    invoke-direct {v0}, Lcom/google/android/d/t;-><init>()V

    sput-object v0, Lcom/google/android/d/t;->f:Lcom/google/android/d/t;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2522
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 2751
    iput-byte v0, p0, Lcom/google/android/d/t;->g:B

    .line 2785
    iput v0, p0, Lcom/google/android/d/t;->h:I

    .line 2523
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/t;->b:Ljava/lang/Object;

    .line 2524
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/t;->c:Ljava/lang/Object;

    .line 2525
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/d/t;->d:Z

    .line 2526
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    .line 2527
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 12

    .prologue
    const/16 v10, 0x8

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2533
    invoke-direct {p0}, Lcom/google/android/d/t;-><init>()V

    .line 2536
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v5

    move v4, v3

    move v0, v3

    .line 2539
    :cond_0
    :goto_0
    if-nez v4, :cond_4

    .line 2540
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v1

    .line 2541
    sparse-switch v1, :sswitch_data_0

    .line 2546
    invoke-virtual {v5, v1, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v1

    if-nez v1, :cond_0

    move v4, v2

    .line 2548
    goto :goto_0

    :sswitch_0
    move v4, v2

    .line 2544
    goto :goto_0

    .line 2553
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 2554
    iget v6, p0, Lcom/google/android/d/t;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, p0, Lcom/google/android/d/t;->a:I

    .line 2555
    iput-object v1, p0, Lcom/google/android/d/t;->b:Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_0

    .line 2581
    :catch_0
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 2582
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2587
    :catchall_0
    move-exception v0

    :goto_1
    and-int/lit8 v1, v1, 0x8

    if-ne v1, v10, :cond_1

    .line 2588
    iget-object v1, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    .line 2590
    :cond_1
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/t;->au:Lcom/google/n/bn;

    throw v0

    .line 2559
    :sswitch_2
    :try_start_2
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v1

    .line 2560
    iget v6, p0, Lcom/google/android/d/t;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/google/android/d/t;->a:I

    .line 2561
    iput-object v1, p0, Lcom/google/android/d/t;->c:Ljava/lang/Object;
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2583
    :catch_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 2584
    :try_start_3
    new-instance v2, Lcom/google/n/ak;

    .line 2585
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v2, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2565
    :sswitch_3
    :try_start_4
    iget v1, p0, Lcom/google/android/d/t;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/d/t;->a:I

    .line 2566
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-eqz v1, :cond_2

    move v1, v2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/d/t;->d:Z

    goto :goto_0

    .line 2587
    :catchall_1
    move-exception v1

    move-object v11, v1

    move v1, v0

    move-object v0, v11

    goto :goto_1

    :cond_2
    move v1, v3

    .line 2566
    goto :goto_2

    .line 2570
    :sswitch_4
    and-int/lit8 v1, v0, 0x8

    if-eq v1, v10, :cond_3

    .line 2571
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    .line 2573
    or-int/lit8 v0, v0, 0x8

    .line 2575
    :cond_3
    iget-object v1, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    .line 2576
    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v7

    invoke-direct {v6, p2, v7}, Lcom/google/n/ao;-><init>(Lcom/google/n/o;Lcom/google/n/f;)V

    .line 2575
    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 2587
    :cond_4
    and-int/lit8 v0, v0, 0x8

    if-ne v0, v10, :cond_5

    .line 2588
    iget-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    .line 2590
    :cond_5
    invoke-virtual {v5}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/t;->au:Lcom/google/n/bn;

    .line 2591
    return-void

    .line 2541
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2520
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 2751
    iput-byte v0, p0, Lcom/google/android/d/t;->g:B

    .line 2785
    iput v0, p0, Lcom/google/android/d/t;->h:I

    .line 2521
    return-void
.end method

.method public static d()Lcom/google/android/d/t;
    .locals 1

    .prologue
    .line 3307
    sget-object v0, Lcom/google/android/d/t;->f:Lcom/google/android/d/t;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/android/d/v;
    .locals 1

    .prologue
    .line 2876
    new-instance v0, Lcom/google/android/d/v;

    invoke-direct {v0}, Lcom/google/android/d/v;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/t;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2605
    sget-object v0, Lcom/google/android/d/t;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x2

    .line 2769
    invoke-virtual {p0}, Lcom/google/android/d/t;->c()I

    .line 2770
    iget v0, p0, Lcom/google/android/d/t;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 2771
    iget-object v0, p0, Lcom/google/android/d/t;->b:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/t;->b:Ljava/lang/Object;

    :goto_0
    invoke-static {v1, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2773
    :cond_0
    iget v0, p0, Lcom/google/android/d/t;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2774
    iget-object v0, p0, Lcom/google/android/d/t;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/t;->c:Ljava/lang/Object;

    :goto_1
    invoke-static {v4, v4}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2776
    :cond_1
    iget v0, p0, Lcom/google/android/d/t;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 2777
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/android/d/t;->d:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 2779
    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 2780
    iget-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v5, v4}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v1

    invoke-virtual {p1, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2779
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2771
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    .line 2774
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2777
    goto :goto_2

    .line 2782
    :cond_6
    iget-object v0, p0, Lcom/google/android/d/t;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 2783
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2753
    iget-byte v0, p0, Lcom/google/android/d/t;->g:B

    .line 2754
    if-ne v0, v3, :cond_1

    move v2, v3

    .line 2764
    :cond_0
    :goto_0
    return v2

    .line 2755
    :cond_1
    if-eqz v0, :cond_0

    move v1, v2

    .line 2757
    :goto_1
    iget-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2758
    iget-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/android/d/o;->d()Lcom/google/android/d/o;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/o;

    invoke-virtual {v0}, Lcom/google/android/d/o;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2759
    iput-byte v2, p0, Lcom/google/android/d/t;->g:B

    goto :goto_0

    .line 2757
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2763
    :cond_3
    iput-byte v3, p0, Lcom/google/android/d/t;->g:B

    move v2, v3

    .line 2764
    goto :goto_0
.end method

.method public final c()I
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2787
    iget v0, p0, Lcom/google/android/d/t;->h:I

    .line 2788
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2809
    :goto_0
    return v0

    .line 2791
    :cond_0
    iget v0, p0, Lcom/google/android/d/t;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_6

    .line 2793
    iget-object v0, p0, Lcom/google/android/d/t;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_3

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/t;->b:Ljava/lang/Object;

    :goto_1
    invoke-static {v3, v2}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x0

    move v1, v0

    .line 2795
    :goto_2
    iget v0, p0, Lcom/google/android/d/t;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 2797
    iget-object v0, p0, Lcom/google/android/d/t;->c:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/t;->c:Ljava/lang/Object;

    :goto_3
    invoke-static {v4, v2}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v1, v0

    .line 2799
    :cond_1
    iget v0, p0, Lcom/google/android/d/t;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v5, :cond_2

    .line 2800
    const/4 v0, 0x3

    iget-boolean v3, p0, Lcom/google/android/d/t;->d:Z

    .line 2801
    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    :cond_2
    move v3, v1

    move v1, v2

    .line 2803
    :goto_4
    iget-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 2804
    iget-object v0, p0, Lcom/google/android/d/t;->e:Ljava/util/List;

    .line 2805
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {v5, v2}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v0}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 2803
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 2793
    :cond_3
    check-cast v0, Lcom/google/n/f;

    goto :goto_1

    .line 2797
    :cond_4
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 2807
    :cond_5
    iget-object v0, p0, Lcom/google/android/d/t;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    .line 2808
    iput v0, p0, Lcom/google/android/d/t;->h:I

    goto/16 :goto_0

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2514
    invoke-static {}, Lcom/google/android/d/t;->newBuilder()Lcom/google/android/d/v;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/d/v;->a(Lcom/google/android/d/t;)Lcom/google/android/d/v;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 2514
    invoke-static {}, Lcom/google/android/d/t;->newBuilder()Lcom/google/android/d/v;

    move-result-object v0

    return-object v0
.end method
