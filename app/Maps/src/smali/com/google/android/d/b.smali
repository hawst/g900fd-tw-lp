.class public final Lcom/google/android/d/b;
.super Lcom/google/n/t;
.source "PG"

# interfaces
.implements Lcom/google/android/d/e;


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/b;",
            ">;"
        }
    .end annotation
.end field

.field static final h:Lcom/google/android/d/b;

.field private static volatile k:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:Lcom/google/n/ao;

.field d:Lcom/google/n/ao;

.field e:Ljava/lang/Object;

.field f:Lcom/google/n/ao;

.field g:Z

.field private i:B

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1102
    new-instance v0, Lcom/google/android/d/c;

    invoke-direct {v0}, Lcom/google/android/d/c;-><init>()V

    sput-object v0, Lcom/google/android/d/b;->PARSER:Lcom/google/n/ax;

    .line 1321
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/d/b;->k:Lcom/google/n/aw;

    .line 1830
    new-instance v0, Lcom/google/android/d/b;

    invoke-direct {v0}, Lcom/google/android/d/b;-><init>()V

    sput-object v0, Lcom/google/android/d/b;->h:Lcom/google/android/d/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1028
    invoke-direct {p0}, Lcom/google/n/t;-><init>()V

    .line 1134
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    .line 1150
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    .line 1208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    .line 1238
    iput-byte v1, p0, Lcom/google/android/d/b;->i:B

    .line 1284
    iput v1, p0, Lcom/google/android/d/b;->j:I

    .line 1029
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/d/b;->b:J

    .line 1030
    iget-object v0, p0, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1031
    iget-object v0, p0, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1032
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    .line 1033
    iget-object v0, p0, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    iput-object v2, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    iput-boolean v3, v0, Lcom/google/n/ao;->d:Z

    .line 1034
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/d/b;->g:Z

    .line 1035
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1041
    invoke-direct {p0}, Lcom/google/android/d/b;-><init>()V

    .line 1042
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v4

    move v3, v2

    .line 1047
    :cond_0
    :goto_0
    if-nez v3, :cond_2

    .line 1048
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v0

    .line 1049
    sparse-switch v0, :sswitch_data_0

    .line 1054
    invoke-virtual {v4, v0, p1}, Lcom/google/n/bo;->a(ILcom/google/n/j;)Z

    move-result v0

    if-nez v0, :cond_0

    move v3, v1

    .line 1056
    goto :goto_0

    :sswitch_0
    move v3, v1

    .line 1052
    goto :goto_0

    .line 1061
    :sswitch_1
    iget v0, p0, Lcom/google/android/d/b;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/d/b;->a:I

    .line 1062
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/d/b;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1093
    :catch_0
    move-exception v0

    .line 1094
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1099
    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/b;->au:Lcom/google/n/bn;

    throw v0

    .line 1066
    :sswitch_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 1067
    iget v0, p0, Lcom/google/android/d/b;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/d/b;->a:I
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1095
    :catch_1
    move-exception v0

    .line 1096
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 1097
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1071
    :sswitch_3
    :try_start_4
    iget-object v0, p0, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 1072
    iget v0, p0, Lcom/google/android/d/b;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/d/b;->a:I

    goto :goto_0

    .line 1076
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 1077
    iget v5, p0, Lcom/google/android/d/b;->a:I

    or-int/lit8 v5, v5, 0x8

    iput v5, p0, Lcom/google/android/d/b;->a:I

    .line 1078
    iput-object v0, p0, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    goto :goto_0

    .line 1082
    :sswitch_5
    iget-object v0, p0, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    invoke-virtual {p1}, Lcom/google/n/j;->g()Lcom/google/n/f;

    move-result-object v5

    iput-object v5, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-object p2, v0, Lcom/google/n/ao;->c:Lcom/google/n/o;

    const/4 v5, 0x0

    iput-boolean v5, v0, Lcom/google/n/ao;->d:Z

    .line 1083
    iget v0, p0, Lcom/google/android/d/b;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/d/b;->a:I

    goto/16 :goto_0

    .line 1087
    :sswitch_6
    iget v0, p0, Lcom/google/android/d/b;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/google/android/d/b;->a:I

    .line 1088
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/d/b;->g:Z
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    .line 1099
    :cond_2
    invoke-virtual {v4}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/b;->au:Lcom/google/n/bn;

    .line 1100
    return-void

    .line 1049
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/v;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1026
    invoke-direct {p0, p1}, Lcom/google/n/t;-><init>(Lcom/google/n/v;)V

    .line 1134
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    .line 1150
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    .line 1208
    new-instance v0, Lcom/google/n/ao;

    invoke-direct {v0}, Lcom/google/n/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    .line 1238
    iput-byte v1, p0, Lcom/google/android/d/b;->i:B

    .line 1284
    iput v1, p0, Lcom/google/android/d/b;->j:I

    .line 1027
    return-void
.end method

.method public static d()Lcom/google/android/d/b;
    .locals 1

    .prologue
    .line 1833
    sget-object v0, Lcom/google/android/d/b;->h:Lcom/google/android/d/b;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/android/d/d;
    .locals 1

    .prologue
    .line 1383
    new-instance v0, Lcom/google/android/d/d;

    invoke-direct {v0}, Lcom/google/android/d/d;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1114
    sget-object v0, Lcom/google/android/d/b;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 1262
    invoke-virtual {p0}, Lcom/google/android/d/b;->c()I

    .line 1263
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    .line 1264
    iget-wide v4, p0, Lcom/google/android/d/b;->b:J

    invoke-static {v1, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v4, v5}, Lcom/google/n/l;->a(J)V

    .line 1266
    :cond_0
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v6, :cond_1

    .line 1267
    iget-object v0, p0, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v0

    invoke-static {v6, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1269
    :cond_1
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 1270
    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1272
    :cond_2
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_3

    .line 1273
    iget-object v0, p0, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    :goto_0
    invoke-static {v7, v6}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-virtual {p1, v3}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1275
    :cond_3
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_4

    .line 1276
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/n/ao;->b()Lcom/google/n/f;

    move-result-object v3

    invoke-static {v0, v6}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v3}, Lcom/google/n/f;->b()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v3}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1278
    :cond_4
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_5

    .line 1279
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/android/d/b;->g:Z

    invoke-static {v0, v2}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    if-eqz v3, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(I)V

    .line 1281
    :cond_5
    iget-object v0, p0, Lcom/google/android/d/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 1282
    return-void

    .line 1273
    :cond_6
    check-cast v0, Lcom/google/n/f;

    goto :goto_0

    :cond_7
    move v0, v2

    .line 1279
    goto :goto_1
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1240
    iget-byte v0, p0, Lcom/google/android/d/b;->i:B

    .line 1241
    if-ne v0, v1, :cond_0

    move v0, v1

    .line 1257
    :goto_0
    return v0

    .line 1242
    :cond_0
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    .line 1244
    :cond_1
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 1245
    iget-object v0, p0, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/i/a/k;->d()Lcom/google/i/a/k;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/k;

    invoke-virtual {v0}, Lcom/google/i/a/k;->b()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1246
    iput-byte v2, p0, Lcom/google/android/d/b;->i:B

    move v0, v2

    .line 1247
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1244
    goto :goto_1

    .line 1250
    :cond_3
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v3, 0x4

    if-ne v0, v3, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_5

    .line 1251
    iget-object v0, p0, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/android/d/t;->d()Lcom/google/android/d/t;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/t;

    invoke-virtual {v0}, Lcom/google/android/d/t;->b()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1252
    iput-byte v2, p0, Lcom/google/android/d/b;->i:B

    move v0, v2

    .line 1253
    goto :goto_0

    :cond_4
    move v0, v2

    .line 1250
    goto :goto_2

    .line 1256
    :cond_5
    iput-byte v1, p0, Lcom/google/android/d/b;->i:B

    move v0, v1

    .line 1257
    goto :goto_0
.end method

.method public final c()I
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1286
    iget v0, p0, Lcom/google/android/d/b;->j:I

    .line 1287
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 1316
    :goto_0
    return v0

    .line 1290
    :cond_0
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_7

    .line 1291
    iget-wide v2, p0, Lcom/google/android/d/b;->b:J

    .line 1292
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 1294
    :goto_1
    iget v2, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    .line 1295
    iget-object v2, p0, Lcom/google/android/d/b;->c:Lcom/google/n/ao;

    .line 1296
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-static {v2}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1298
    :cond_1
    iget v2, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v6, :cond_6

    .line 1299
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/d/b;->d:Lcom/google/n/ao;

    .line 1300
    invoke-static {v2, v1}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-static {v2}, Lcom/google/n/l;->c(I)I

    move-result v2

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    move v2, v0

    .line 1302
    :goto_2
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_2

    .line 1304
    iget-object v0, p0, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/b;->e:Ljava/lang/Object;

    :goto_3
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1306
    :cond_2
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 1307
    const/4 v0, 0x5

    iget-object v3, p0, Lcom/google/android/d/b;->f:Lcom/google/n/ao;

    .line 1308
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v3}, Lcom/google/n/l;->a(Lcom/google/n/ao;)I

    move-result v3

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 1310
    :cond_3
    iget v0, p0, Lcom/google/android/d/b;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 1311
    const/4 v0, 0x6

    iget-boolean v3, p0, Lcom/google/android/d/b;->g:Z

    .line 1312
    invoke-static {v0, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v2, v0

    .line 1314
    :cond_4
    iget-object v0, p0, Lcom/google/android/d/b;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v2

    .line 1315
    iput v0, p0, Lcom/google/android/d/b;->j:I

    goto/16 :goto_0

    .line 1304
    :cond_5
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    :cond_6
    move v2, v0

    goto :goto_2

    :cond_7
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1020
    invoke-static {}, Lcom/google/android/d/b;->newBuilder()Lcom/google/android/d/d;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/d/d;->a(Lcom/google/android/d/b;)Lcom/google/android/d/d;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 1020
    invoke-static {}, Lcom/google/android/d/b;->newBuilder()Lcom/google/android/d/d;

    move-result-object v0

    return-object v0
.end method
