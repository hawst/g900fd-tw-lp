.class public final Lcom/google/android/d/n;
.super Lcom/google/n/w;
.source "PG"

# interfaces
.implements Lcom/google/android/d/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/w",
        "<",
        "Lcom/google/android/d/l;",
        "Lcom/google/android/d/n;",
        ">;",
        "Lcom/google/android/d/s;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:J

.field private c:J

.field private f:Ljava/lang/Object;

.field private g:Ljava/lang/Object;

.field private h:Ljava/lang/Object;

.field private i:Ljava/lang/Object;

.field private j:Ljava/lang/Object;

.field private k:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 4545
    sget-object v0, Lcom/google/android/d/l;->j:Lcom/google/android/d/l;

    invoke-direct {p0, v0}, Lcom/google/n/w;-><init>(Lcom/google/n/x;)V

    .line 4729
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/n;->f:Ljava/lang/Object;

    .line 4805
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/n;->g:Ljava/lang/Object;

    .line 4881
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/n;->h:Ljava/lang/Object;

    .line 4957
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/n;->i:Ljava/lang/Object;

    .line 5033
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/n;->j:Ljava/lang/Object;

    .line 5109
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/n;->k:Ljava/lang/Object;

    .line 4546
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/d/l;)Lcom/google/android/d/n;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4613
    invoke-static {}, Lcom/google/android/d/l;->d()Lcom/google/android/d/l;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 4652
    :goto_0
    return-object p0

    .line 4614
    :cond_0
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_9

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 4615
    iget-wide v2, p1, Lcom/google/android/d/l;->b:J

    iget v4, p0, Lcom/google/android/d/n;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/d/n;->a:I

    iput-wide v2, p0, Lcom/google/android/d/n;->b:J

    .line 4617
    :cond_1
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 4618
    iget-wide v2, p1, Lcom/google/android/d/l;->c:J

    iget v4, p0, Lcom/google/android/d/n;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, p0, Lcom/google/android/d/n;->a:I

    iput-wide v2, p0, Lcom/google/android/d/n;->c:J

    .line 4620
    :cond_2
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_b

    move v2, v0

    :goto_3
    if-eqz v2, :cond_3

    .line 4621
    iget v2, p0, Lcom/google/android/d/n;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/d/n;->a:I

    .line 4622
    iget-object v2, p1, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/n;->f:Ljava/lang/Object;

    .line 4625
    :cond_3
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_c

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 4626
    iget v2, p0, Lcom/google/android/d/n;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/d/n;->a:I

    .line 4627
    iget-object v2, p1, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/n;->g:Ljava/lang/Object;

    .line 4630
    :cond_4
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_d

    move v2, v0

    :goto_5
    if-eqz v2, :cond_5

    .line 4631
    iget v2, p0, Lcom/google/android/d/n;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/google/android/d/n;->a:I

    .line 4632
    iget-object v2, p1, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/n;->h:Ljava/lang/Object;

    .line 4635
    :cond_5
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_e

    move v2, v0

    :goto_6
    if-eqz v2, :cond_6

    .line 4636
    iget v2, p0, Lcom/google/android/d/n;->a:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/google/android/d/n;->a:I

    .line 4637
    iget-object v2, p1, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/n;->i:Ljava/lang/Object;

    .line 4640
    :cond_6
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_f

    move v2, v0

    :goto_7
    if-eqz v2, :cond_7

    .line 4641
    iget v2, p0, Lcom/google/android/d/n;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, p0, Lcom/google/android/d/n;->a:I

    .line 4642
    iget-object v2, p1, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/n;->j:Ljava/lang/Object;

    .line 4645
    :cond_7
    iget v2, p1, Lcom/google/android/d/l;->a:I

    and-int/lit16 v2, v2, 0x80

    const/16 v3, 0x80

    if-ne v2, v3, :cond_10

    :goto_8
    if-eqz v0, :cond_8

    .line 4646
    iget v0, p0, Lcom/google/android/d/n;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, p0, Lcom/google/android/d/n;->a:I

    .line 4647
    iget-object v0, p1, Lcom/google/android/d/l;->i:Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/d/n;->k:Ljava/lang/Object;

    .line 4650
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/android/d/n;->a(Lcom/google/n/x;)V

    .line 4651
    iget-object v0, p1, Lcom/google/android/d/l;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0

    :cond_9
    move v2, v1

    .line 4614
    goto/16 :goto_1

    :cond_a
    move v2, v1

    .line 4617
    goto/16 :goto_2

    :cond_b
    move v2, v1

    .line 4620
    goto/16 :goto_3

    :cond_c
    move v2, v1

    .line 4625
    goto :goto_4

    :cond_d
    move v2, v1

    .line 4630
    goto :goto_5

    :cond_e
    move v2, v1

    .line 4635
    goto :goto_6

    :cond_f
    move v2, v1

    .line 4640
    goto :goto_7

    :cond_10
    move v0, v1

    .line 4645
    goto :goto_8
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 4538
    new-instance v2, Lcom/google/android/d/l;

    invoke-direct {v2, p0}, Lcom/google/android/d/l;-><init>(Lcom/google/n/w;)V

    iget v3, p0, Lcom/google/android/d/n;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_7

    :goto_0
    iget-wide v4, p0, Lcom/google/android/d/n;->b:J

    iput-wide v4, v2, Lcom/google/android/d/l;->b:J

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-wide v4, p0, Lcom/google/android/d/n;->c:J

    iput-wide v4, v2, Lcom/google/android/d/l;->c:J

    and-int/lit8 v1, v3, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-object v1, p0, Lcom/google/android/d/n;->f:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x8

    :cond_2
    iget-object v1, p0, Lcom/google/android/d/n;->g:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x10

    const/16 v4, 0x10

    if-ne v1, v4, :cond_3

    or-int/lit8 v0, v0, 0x10

    :cond_3
    iget-object v1, p0, Lcom/google/android/d/n;->h:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x20

    const/16 v4, 0x20

    if-ne v1, v4, :cond_4

    or-int/lit8 v0, v0, 0x20

    :cond_4
    iget-object v1, p0, Lcom/google/android/d/n;->i:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x40

    const/16 v4, 0x40

    if-ne v1, v4, :cond_5

    or-int/lit8 v0, v0, 0x40

    :cond_5
    iget-object v1, p0, Lcom/google/android/d/n;->j:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    and-int/lit16 v1, v3, 0x80

    const/16 v3, 0x80

    if-ne v1, v3, :cond_6

    or-int/lit16 v0, v0, 0x80

    :cond_6
    iget-object v1, p0, Lcom/google/android/d/n;->k:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/l;->i:Ljava/lang/Object;

    iput v0, v2, Lcom/google/android/d/l;->a:I

    return-object v2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 4538
    check-cast p1, Lcom/google/android/d/l;

    invoke-virtual {p0, p1}, Lcom/google/android/d/n;->a(Lcom/google/android/d/l;)Lcom/google/android/d/n;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 4656
    iget-object v0, p0, Lcom/google/n/w;->d:Lcom/google/n/q;

    invoke-virtual {v0}, Lcom/google/n/q;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 4658
    const/4 v0, 0x0

    .line 4660
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
