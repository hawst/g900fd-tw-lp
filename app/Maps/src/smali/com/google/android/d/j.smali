.class public final Lcom/google/android/d/j;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/android/d/k;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/android/d/f;",
        "Lcom/google/android/d/j;",
        ">;",
        "Lcom/google/android/d/k;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Z

.field private c:Ljava/lang/Object;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field

.field private e:Z

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 539
    sget-object v0, Lcom/google/android/d/f;->g:Lcom/google/android/d/f;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 657
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/j;->c:Ljava/lang/Object;

    .line 734
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    .line 902
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/d/j;->f:I

    .line 540
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/d/f;)Lcom/google/android/d/j;
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 590
    invoke-static {}, Lcom/google/android/d/f;->d()Lcom/google/android/d/f;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 616
    :goto_0
    return-object p0

    .line 591
    :cond_0
    iget v2, p1, Lcom/google/android/d/f;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_6

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 592
    iget-boolean v2, p1, Lcom/google/android/d/f;->b:Z

    iget v3, p0, Lcom/google/android/d/j;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/d/j;->a:I

    iput-boolean v2, p0, Lcom/google/android/d/j;->b:Z

    .line 594
    :cond_1
    iget v2, p1, Lcom/google/android/d/f;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_7

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 595
    iget v2, p0, Lcom/google/android/d/j;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/d/j;->a:I

    .line 596
    iget-object v2, p1, Lcom/google/android/d/f;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/j;->c:Ljava/lang/Object;

    .line 599
    :cond_2
    iget-object v2, p1, Lcom/google/android/d/f;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 600
    iget-object v2, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 601
    iget-object v2, p1, Lcom/google/android/d/f;->d:Ljava/util/List;

    iput-object v2, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    .line 602
    iget v2, p0, Lcom/google/android/d/j;->a:I

    and-int/lit8 v2, v2, -0x5

    iput v2, p0, Lcom/google/android/d/j;->a:I

    .line 609
    :cond_3
    :goto_3
    iget v2, p1, Lcom/google/android/d/f;->a:I

    and-int/lit8 v2, v2, 0x4

    if-ne v2, v4, :cond_a

    move v2, v0

    :goto_4
    if-eqz v2, :cond_4

    .line 610
    iget-boolean v2, p1, Lcom/google/android/d/f;->e:Z

    iget v3, p0, Lcom/google/android/d/j;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, p0, Lcom/google/android/d/j;->a:I

    iput-boolean v2, p0, Lcom/google/android/d/j;->e:Z

    .line 612
    :cond_4
    iget v2, p1, Lcom/google/android/d/f;->a:I

    and-int/lit8 v2, v2, 0x8

    const/16 v3, 0x8

    if-ne v2, v3, :cond_b

    :goto_5
    if-eqz v0, :cond_d

    .line 613
    iget v0, p1, Lcom/google/android/d/f;->f:I

    invoke-static {v0}, Lcom/google/android/d/h;->a(I)Lcom/google/android/d/h;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    :cond_5
    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    move v2, v1

    .line 591
    goto :goto_1

    :cond_7
    move v2, v1

    .line 594
    goto :goto_2

    .line 604
    :cond_8
    iget v2, p0, Lcom/google/android/d/j;->a:I

    and-int/lit8 v2, v2, 0x4

    if-eq v2, v4, :cond_9

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    iget v2, p0, Lcom/google/android/d/j;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/d/j;->a:I

    .line 605
    :cond_9
    iget-object v2, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/d/f;->d:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_a
    move v2, v1

    .line 609
    goto :goto_4

    :cond_b
    move v0, v1

    .line 612
    goto :goto_5

    .line 613
    :cond_c
    iget v1, p0, Lcom/google/android/d/j;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/android/d/j;->a:I

    iget v0, v0, Lcom/google/android/d/h;->f:I

    iput v0, p0, Lcom/google/android/d/j;->f:I

    .line 615
    :cond_d
    iget-object v0, p1, Lcom/google/android/d/f;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto/16 :goto_0
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 531
    new-instance v2, Lcom/google/android/d/f;

    invoke-direct {v2, p0}, Lcom/google/android/d/f;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/android/d/j;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_4

    :goto_0
    iget-boolean v1, p0, Lcom/google/android/d/j;->b:Z

    iput-boolean v1, v2, Lcom/google/android/d/f;->b:Z

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/android/d/j;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/f;->c:Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/d/j;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    iget v1, p0, Lcom/google/android/d/j;->a:I

    and-int/lit8 v1, v1, -0x5

    iput v1, p0, Lcom/google/android/d/j;->a:I

    :cond_1
    iget-object v1, p0, Lcom/google/android/d/j;->d:Ljava/util/List;

    iput-object v1, v2, Lcom/google/android/d/f;->d:Ljava/util/List;

    and-int/lit8 v1, v3, 0x8

    const/16 v4, 0x8

    if-ne v1, v4, :cond_2

    or-int/lit8 v0, v0, 0x4

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/d/j;->e:Z

    iput-boolean v1, v2, Lcom/google/android/d/f;->e:Z

    and-int/lit8 v1, v3, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_3

    or-int/lit8 v0, v0, 0x8

    :cond_3
    iget v1, p0, Lcom/google/android/d/j;->f:I

    iput v1, v2, Lcom/google/android/d/f;->f:I

    iput v0, v2, Lcom/google/android/d/f;->a:I

    return-object v2

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 531
    check-cast p1, Lcom/google/android/d/f;

    invoke-virtual {p0, p1}, Lcom/google/android/d/j;->a(Lcom/google/android/d/f;)Lcom/google/android/d/j;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 620
    const/4 v0, 0x1

    return v0
.end method
