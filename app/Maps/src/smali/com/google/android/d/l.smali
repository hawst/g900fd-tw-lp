.class public final Lcom/google/android/d/l;
.super Lcom/google/n/x;
.source "PG"

# interfaces
.implements Lcom/google/android/d/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/x",
        "<",
        "Lcom/google/android/d/l;",
        ">;",
        "Lcom/google/android/d/s;"
    }
.end annotation


# static fields
.field public static final PARSER:Lcom/google/n/ax;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/l;",
            ">;"
        }
    .end annotation
.end field

.field static final j:Lcom/google/android/d/l;

.field private static volatile m:Lcom/google/n/aw;

.field private static final serialVersionUID:J


# instance fields
.field a:I

.field b:J

.field c:J

.field d:Ljava/lang/Object;

.field e:Ljava/lang/Object;

.field f:Ljava/lang/Object;

.field g:Ljava/lang/Object;

.field h:Ljava/lang/Object;

.field i:Ljava/lang/Object;

.field private k:B

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 4074
    new-instance v0, Lcom/google/android/d/m;

    invoke-direct {v0}, Lcom/google/android/d/m;-><init>()V

    sput-object v0, Lcom/google/android/d/l;->PARSER:Lcom/google/n/ax;

    .line 4466
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/d/l;->m:Lcom/google/n/aw;

    .line 5189
    new-instance v0, Lcom/google/android/d/l;

    invoke-direct {v0}, Lcom/google/android/d/l;-><init>()V

    sput-object v0, Lcom/google/android/d/l;->j:Lcom/google/android/d/l;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, -0x1

    .line 3981
    invoke-direct {p0}, Lcom/google/n/x;-><init>()V

    .line 4372
    iput-byte v0, p0, Lcom/google/android/d/l;->k:B

    .line 4420
    iput v0, p0, Lcom/google/android/d/l;->l:I

    .line 3982
    iput-wide v2, p0, Lcom/google/android/d/l;->b:J

    .line 3983
    iput-wide v2, p0, Lcom/google/android/d/l;->c:J

    .line 3984
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    .line 3985
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    .line 3986
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    .line 3987
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    .line 3988
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    .line 3989
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/l;->i:Ljava/lang/Object;

    .line 3990
    return-void
.end method

.method constructor <init>(Lcom/google/n/j;Lcom/google/n/o;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 3996
    invoke-direct {p0}, Lcom/google/android/d/l;-><init>()V

    .line 3997
    invoke-static {}, Lcom/google/n/bn;->b()Lcom/google/n/bo;

    move-result-object v3

    .line 4001
    const/4 v0, 0x0

    move v6, v0

    .line 4002
    :cond_0
    :goto_0
    if-nez v6, :cond_2

    .line 4003
    :try_start_0
    invoke-virtual {p1}, Lcom/google/n/j;->a()I

    move-result v5

    .line 4004
    sparse-switch v5, :sswitch_data_0

    .line 4009
    iget-object v0, p0, Lcom/google/android/d/l;->as:Lcom/google/n/q;

    sget-object v1, Lcom/google/android/d/l;->j:Lcom/google/android/d/l;

    move-object v2, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/d/l;->a(Lcom/google/n/q;Lcom/google/n/at;Lcom/google/n/j;Lcom/google/n/bo;Lcom/google/n/o;I)Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v7

    .line 4012
    goto :goto_0

    :sswitch_0
    move v6, v7

    .line 4007
    goto :goto_0

    .line 4017
    :sswitch_1
    iget v0, p0, Lcom/google/android/d/l;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/d/l;->a:I

    .line 4018
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/d/l;->b:J
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 4064
    :catch_0
    move-exception v0

    .line 4065
    :try_start_1
    iput-object p0, v0, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 4070
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/l;->au:Lcom/google/n/bn;

    .line 4071
    iget-object v1, p0, Lcom/google/android/d/l;->as:Lcom/google/n/q;

    iget-boolean v2, v1, Lcom/google/n/q;->b:Z

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v2}, Lcom/google/n/be;->a()V

    iput-boolean v7, v1, Lcom/google/n/q;->b:Z

    :cond_1
    throw v0

    .line 4022
    :sswitch_2
    :try_start_2
    iget v0, p0, Lcom/google/android/d/l;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/d/l;->a:I

    .line 4023
    invoke-virtual {p1}, Lcom/google/n/j;->m()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/d/l;->c:J
    :try_end_2
    .catch Lcom/google/n/ak; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 4066
    :catch_1
    move-exception v0

    .line 4067
    :try_start_3
    new-instance v1, Lcom/google/n/ak;

    .line 4068
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/n/ak;-><init>(Ljava/lang/String;)V

    iput-object p0, v1, Lcom/google/n/ak;->a:Lcom/google/n/at;

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 4027
    :sswitch_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4028
    iget v1, p0, Lcom/google/android/d/l;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/d/l;->a:I

    .line 4029
    iput-object v0, p0, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    goto :goto_0

    .line 4033
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4034
    iget v1, p0, Lcom/google/android/d/l;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p0, Lcom/google/android/d/l;->a:I

    .line 4035
    iput-object v0, p0, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    goto :goto_0

    .line 4039
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4040
    iget v1, p0, Lcom/google/android/d/l;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p0, Lcom/google/android/d/l;->a:I

    .line 4041
    iput-object v0, p0, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    goto :goto_0

    .line 4045
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4046
    iget v1, p0, Lcom/google/android/d/l;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p0, Lcom/google/android/d/l;->a:I

    .line 4047
    iput-object v0, p0, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4051
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4052
    iget v1, p0, Lcom/google/android/d/l;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, p0, Lcom/google/android/d/l;->a:I

    .line 4053
    iput-object v0, p0, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    goto/16 :goto_0

    .line 4057
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/n/j;->f()Ljava/lang/String;

    move-result-object v0

    .line 4058
    iget v1, p0, Lcom/google/android/d/l;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p0, Lcom/google/android/d/l;->a:I

    .line 4059
    iput-object v0, p0, Lcom/google/android/d/l;->i:Ljava/lang/Object;
    :try_end_4
    .catch Lcom/google/n/ak; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0

    .line 4070
    :cond_2
    invoke-virtual {v3}, Lcom/google/n/bo;->a()Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->au:Lcom/google/n/bn;

    .line 4071
    iget-object v0, p0, Lcom/google/android/d/l;->as:Lcom/google/n/q;

    iget-boolean v1, v0, Lcom/google/n/q;->b:Z

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/google/n/q;->a:Lcom/google/n/be;

    invoke-virtual {v1}, Lcom/google/n/be;->a()V

    iput-boolean v7, v0, Lcom/google/n/q;->b:Z

    .line 4072
    :cond_3
    return-void

    .line 4004
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method constructor <init>(Lcom/google/n/w;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/n/w",
            "<",
            "Lcom/google/android/d/l;",
            "*>;)V"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 3979
    invoke-direct {p0, p1}, Lcom/google/n/x;-><init>(Lcom/google/n/w;)V

    .line 4372
    iput-byte v0, p0, Lcom/google/android/d/l;->k:B

    .line 4420
    iput v0, p0, Lcom/google/android/d/l;->l:I

    .line 3980
    return-void
.end method

.method public static d()Lcom/google/android/d/l;
    .locals 1

    .prologue
    .line 5192
    sget-object v0, Lcom/google/android/d/l;->j:Lcom/google/android/d/l;

    return-object v0
.end method

.method public static newBuilder()Lcom/google/android/d/n;
    .locals 1

    .prologue
    .line 4528
    new-instance v0, Lcom/google/android/d/n;

    invoke-direct {v0}, Lcom/google/android/d/n;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/n/ax;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/n/ax",
            "<",
            "Lcom/google/android/d/l;",
            ">;"
        }
    .end annotation

    .prologue
    .line 4086
    sget-object v0, Lcom/google/android/d/l;->PARSER:Lcom/google/n/ax;

    return-object v0
.end method

.method public final a(Lcom/google/n/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 4388
    invoke-virtual {p0}, Lcom/google/android/d/l;->c()I

    .line 4391
    new-instance v1, Lcom/google/n/y;

    invoke-direct {v1, p0, v5}, Lcom/google/n/y;-><init>(Lcom/google/n/x;Z)V

    .line 4392
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v6, :cond_0

    .line 4393
    iget-wide v2, p0, Lcom/google/android/d/l;->b:J

    invoke-static {v6, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    .line 4395
    :cond_0
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x2

    if-ne v0, v4, :cond_1

    .line 4396
    iget-wide v2, p0, Lcom/google/android/d/l;->c:J

    invoke-static {v4, v5}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v2, v3}, Lcom/google/n/l;->a(J)V

    .line 4398
    :cond_1
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v7, :cond_2

    .line 4399
    const/4 v2, 0x3

    iget-object v0, p0, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    :goto_0
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4401
    :cond_2
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v8, :cond_3

    .line 4402
    iget-object v0, p0, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    :goto_1
    invoke-static {v7, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4404
    :cond_3
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v2, 0x10

    if-ne v0, v2, :cond_4

    .line 4405
    const/4 v2, 0x5

    iget-object v0, p0, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    :goto_2
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4407
    :cond_4
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v2, 0x20

    if-ne v0, v2, :cond_5

    .line 4408
    const/4 v2, 0x6

    iget-object v0, p0, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    :goto_3
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4410
    :cond_5
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v2, 0x40

    if-ne v0, v2, :cond_6

    .line 4411
    const/4 v2, 0x7

    iget-object v0, p0, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    :goto_4
    invoke-static {v2, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4413
    :cond_6
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_7

    .line 4414
    iget-object v0, p0, Lcom/google/android/d/l;->i:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_d

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->i:Ljava/lang/Object;

    :goto_5
    invoke-static {v8, v4}, Lcom/google/n/bt;->a(II)I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v2

    invoke-virtual {p1, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4416
    :cond_7
    const/high16 v0, 0x20000000

    invoke-virtual {v1, v0, p1}, Lcom/google/n/y;->a(ILcom/google/n/l;)V

    .line 4417
    iget-object v0, p0, Lcom/google/android/d/l;->au:Lcom/google/n/bn;

    iget-object v0, v0, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {p1, v0}, Lcom/google/n/l;->a(Lcom/google/n/f;)V

    .line 4418
    return-void

    .line 4399
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_0

    .line 4402
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_1

    .line 4405
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_2

    .line 4408
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_3

    .line 4411
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_4

    .line 4414
    :cond_d
    check-cast v0, Lcom/google/n/f;

    goto :goto_5
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 4374
    iget-byte v2, p0, Lcom/google/android/d/l;->k:B

    .line 4375
    if-ne v2, v0, :cond_0

    .line 4383
    :goto_0
    return v0

    .line 4376
    :cond_0
    if-nez v2, :cond_1

    move v0, v1

    goto :goto_0

    .line 4378
    :cond_1
    iget-object v2, p0, Lcom/google/n/x;->as:Lcom/google/n/q;

    invoke-virtual {v2}, Lcom/google/n/q;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 4379
    iput-byte v1, p0, Lcom/google/android/d/l;->k:B

    move v0, v1

    .line 4380
    goto :goto_0

    .line 4382
    :cond_2
    iput-byte v0, p0, Lcom/google/android/d/l;->k:B

    goto :goto_0
.end method

.method public final c()I
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 4422
    iget v0, p0, Lcom/google/android/d/l;->l:I

    .line 4423
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 4461
    :goto_0
    return v0

    .line 4426
    :cond_0
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v4, :cond_e

    .line 4427
    iget-wide v2, p0, Lcom/google/android/d/l;->b:J

    .line 4428
    invoke-static {v4, v1}, Lcom/google/n/bt;->a(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/n/l;->c(I)I

    move-result v0

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x0

    .line 4430
    :goto_1
    iget v2, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_d

    .line 4431
    iget-wide v2, p0, Lcom/google/android/d/l;->c:J

    .line 4432
    invoke-static {v5, v1}, Lcom/google/n/bt;->a(II)I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-static {v2, v3}, Lcom/google/n/l;->b(J)I

    move-result v2

    add-int/2addr v2, v4

    add-int/2addr v0, v2

    move v2, v0

    .line 4434
    :goto_2
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v6, :cond_1

    .line 4435
    const/4 v3, 0x3

    .line 4436
    iget-object v0, p0, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_7

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->d:Ljava/lang/Object;

    :goto_3
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 4438
    :cond_1
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x8

    if-ne v0, v7, :cond_2

    .line 4440
    iget-object v0, p0, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_8

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->e:Ljava/lang/Object;

    :goto_4
    invoke-static {v6, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 4442
    :cond_2
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_3

    .line 4443
    const/4 v3, 0x5

    .line 4444
    iget-object v0, p0, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->f:Ljava/lang/Object;

    :goto_5
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 4446
    :cond_3
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_4

    .line 4447
    const/4 v3, 0x6

    .line 4448
    iget-object v0, p0, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_a

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->g:Ljava/lang/Object;

    :goto_6
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 4450
    :cond_4
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v3, 0x40

    if-ne v0, v3, :cond_5

    .line 4451
    const/4 v3, 0x7

    .line 4452
    iget-object v0, p0, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_b

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->h:Ljava/lang/Object;

    :goto_7
    invoke-static {v3, v1}, Lcom/google/n/bt;->a(II)I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v4

    invoke-static {v4}, Lcom/google/n/l;->c(I)I

    move-result v4

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v0, v3

    add-int/2addr v2, v0

    .line 4454
    :cond_5
    iget v0, p0, Lcom/google/android/d/l;->a:I

    and-int/lit16 v0, v0, 0x80

    const/16 v3, 0x80

    if-ne v0, v3, :cond_6

    .line 4456
    iget-object v0, p0, Lcom/google/android/d/l;->i:Ljava/lang/Object;

    instance-of v3, v0, Ljava/lang/String;

    if-eqz v3, :cond_c

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/n/f;->a(Ljava/lang/String;)Lcom/google/n/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/l;->i:Ljava/lang/Object;

    :goto_8
    invoke-static {v7, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-static {v1}, Lcom/google/n/l;->c(I)I

    move-result v1

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/n/l;->c(I)I

    move-result v3

    invoke-virtual {v0}, Lcom/google/n/f;->b()I

    move-result v0

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    .line 4458
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/d/l;->g()I

    move-result v0

    add-int/2addr v0, v2

    .line 4459
    iget-object v1, p0, Lcom/google/android/d/l;->au:Lcom/google/n/bn;

    iget-object v1, v1, Lcom/google/n/bn;->a:Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->b()I

    move-result v1

    add-int/2addr v0, v1

    .line 4460
    iput v0, p0, Lcom/google/android/d/l;->l:I

    goto/16 :goto_0

    .line 4436
    :cond_7
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_3

    .line 4440
    :cond_8
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_4

    .line 4444
    :cond_9
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_5

    .line 4448
    :cond_a
    check-cast v0, Lcom/google/n/f;

    goto/16 :goto_6

    .line 4452
    :cond_b
    check-cast v0, Lcom/google/n/f;

    goto :goto_7

    .line 4456
    :cond_c
    check-cast v0, Lcom/google/n/f;

    goto :goto_8

    :cond_d
    move v2, v0

    goto/16 :goto_2

    :cond_e
    move v0, v1

    goto/16 :goto_1
.end method

.method public final synthetic e()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3972
    invoke-static {}, Lcom/google/android/d/l;->newBuilder()Lcom/google/android/d/n;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/d/n;->a(Lcom/google/android/d/l;)Lcom/google/android/d/n;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic f()Lcom/google/n/au;
    .locals 1

    .prologue
    .line 3972
    invoke-static {}, Lcom/google/android/d/l;->newBuilder()Lcom/google/android/d/n;

    move-result-object v0

    return-object v0
.end method
