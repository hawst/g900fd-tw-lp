.class public final Lcom/google/android/d/v;
.super Lcom/google/n/v;
.source "PG"

# interfaces
.implements Lcom/google/android/d/w;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/n/v",
        "<",
        "Lcom/google/android/d/t;",
        "Lcom/google/android/d/v;",
        ">;",
        "Lcom/google/android/d/w;"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private c:Ljava/lang/Object;

.field private d:Z

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/n/ao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 2894
    sget-object v0, Lcom/google/android/d/t;->f:Lcom/google/android/d/t;

    invoke-direct {p0, v0}, Lcom/google/n/v;-><init>(Lcom/google/n/t;)V

    .line 2979
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/v;->b:Ljava/lang/Object;

    .line 3055
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/d/v;->c:Ljava/lang/Object;

    .line 3131
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/d/v;->d:Z

    .line 3164
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    .line 2895
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/d/t;)Lcom/google/android/d/v;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2939
    invoke-static {}, Lcom/google/android/d/t;->d()Lcom/google/android/d/t;

    move-result-object v2

    if-ne p1, v2, :cond_0

    .line 2964
    :goto_0
    return-object p0

    .line 2940
    :cond_0
    iget v2, p1, Lcom/google/android/d/t;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_1
    if-eqz v2, :cond_1

    .line 2941
    iget v2, p0, Lcom/google/android/d/v;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/d/v;->a:I

    .line 2942
    iget-object v2, p1, Lcom/google/android/d/t;->b:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/v;->b:Ljava/lang/Object;

    .line 2945
    :cond_1
    iget v2, p1, Lcom/google/android/d/t;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_6

    move v2, v0

    :goto_2
    if-eqz v2, :cond_2

    .line 2946
    iget v2, p0, Lcom/google/android/d/v;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/google/android/d/v;->a:I

    .line 2947
    iget-object v2, p1, Lcom/google/android/d/t;->c:Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/d/v;->c:Ljava/lang/Object;

    .line 2950
    :cond_2
    iget v2, p1, Lcom/google/android/d/t;->a:I

    and-int/lit8 v2, v2, 0x4

    const/4 v3, 0x4

    if-ne v2, v3, :cond_7

    :goto_3
    if-eqz v0, :cond_3

    .line 2951
    iget-boolean v0, p1, Lcom/google/android/d/t;->d:Z

    iget v1, p0, Lcom/google/android/d/v;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/d/v;->a:I

    iput-boolean v0, p0, Lcom/google/android/d/v;->d:Z

    .line 2953
    :cond_3
    iget-object v0, p1, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2954
    iget-object v0, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2955
    iget-object v0, p1, Lcom/google/android/d/t;->e:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    .line 2956
    iget v0, p0, Lcom/google/android/d/v;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/google/android/d/v;->a:I

    .line 2963
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/android/d/t;->au:Lcom/google/n/bn;

    iget-object v1, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    invoke-static {v1, v0}, Lcom/google/n/bn;->a(Lcom/google/n/bn;Lcom/google/n/bn;)Lcom/google/n/bn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/n/v;->I:Lcom/google/n/bn;

    goto :goto_0

    :cond_5
    move v2, v1

    .line 2940
    goto :goto_1

    :cond_6
    move v2, v1

    .line 2945
    goto :goto_2

    :cond_7
    move v0, v1

    .line 2950
    goto :goto_3

    .line 2958
    :cond_8
    iget v0, p0, Lcom/google/android/d/v;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-eq v0, v1, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    iget v0, p0, Lcom/google/android/d/v;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/d/v;->a:I

    .line 2959
    :cond_9
    iget-object v0, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/d/t;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_4
.end method

.method public final synthetic a()Lcom/google/n/t;
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2886
    new-instance v2, Lcom/google/android/d/t;

    invoke-direct {v2, p0}, Lcom/google/android/d/t;-><init>(Lcom/google/n/v;)V

    iget v3, p0, Lcom/google/android/d/v;->a:I

    const/4 v1, 0x0

    and-int/lit8 v4, v3, 0x1

    if-ne v4, v0, :cond_3

    :goto_0
    iget-object v1, p0, Lcom/google/android/d/v;->b:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/t;->b:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_0

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v1, p0, Lcom/google/android/d/v;->c:Ljava/lang/Object;

    iput-object v1, v2, Lcom/google/android/d/t;->c:Ljava/lang/Object;

    and-int/lit8 v1, v3, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_1

    or-int/lit8 v0, v0, 0x4

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/d/v;->d:Z

    iput-boolean v1, v2, Lcom/google/android/d/t;->d:Z

    iget v1, p0, Lcom/google/android/d/v;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    iget v1, p0, Lcom/google/android/d/v;->a:I

    and-int/lit8 v1, v1, -0x9

    iput v1, p0, Lcom/google/android/d/v;->a:I

    :cond_2
    iget-object v1, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    iput-object v1, v2, Lcom/google/android/d/t;->e:Ljava/util/List;

    iput v0, v2, Lcom/google/android/d/t;->a:I

    return-object v2

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/n/t;)Lcom/google/n/v;
    .locals 1

    .prologue
    .line 2886
    check-cast p1, Lcom/google/android/d/t;

    invoke-virtual {p0, p1}, Lcom/google/android/d/v;->a(Lcom/google/android/d/t;)Lcom/google/android/d/v;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2968
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2969
    iget-object v0, p0, Lcom/google/android/d/v;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/android/d/o;->d()Lcom/google/android/d/o;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/o;

    invoke-virtual {v0}, Lcom/google/android/d/o;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2974
    :goto_1
    return v2

    .line 2968
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2974
    :cond_1
    const/4 v2, 0x1

    goto :goto_1
.end method
