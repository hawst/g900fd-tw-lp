.class public final enum Lcom/google/android/d/h;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/n/ag;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/d/h;",
        ">;",
        "Lcom/google/n/ag;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/d/h;

.field public static final enum b:Lcom/google/android/d/h;

.field public static final enum c:Lcom/google/android/d/h;

.field public static final enum d:Lcom/google/android/d/h;

.field public static final enum e:Lcom/google/android/d/h;

.field private static final synthetic g:[Lcom/google/android/d/h;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 186
    new-instance v0, Lcom/google/android/d/h;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    .line 190
    new-instance v0, Lcom/google/android/d/h;

    const-string v1, "LISTENING"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/d/h;->b:Lcom/google/android/d/h;

    .line 194
    new-instance v0, Lcom/google/android/d/h;

    const-string v1, "RECORDING"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/d/h;->c:Lcom/google/android/d/h;

    .line 198
    new-instance v0, Lcom/google/android/d/h;

    const-string v1, "PROCESSING"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/d/h;->d:Lcom/google/android/d/h;

    .line 202
    new-instance v0, Lcom/google/android/d/h;

    const-string v1, "PLAYING_TTS"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/android/d/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/d/h;->e:Lcom/google/android/d/h;

    .line 181
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/d/h;

    sget-object v1, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/d/h;->b:Lcom/google/android/d/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/d/h;->c:Lcom/google/android/d/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/d/h;->d:Lcom/google/android/d/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/d/h;->e:Lcom/google/android/d/h;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/d/h;->g:[Lcom/google/android/d/h;

    .line 247
    new-instance v0, Lcom/google/android/d/i;

    invoke-direct {v0}, Lcom/google/android/d/i;-><init>()V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 256
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 257
    iput p3, p0, Lcom/google/android/d/h;->f:I

    .line 258
    return-void
.end method

.method public static a(I)Lcom/google/android/d/h;
    .locals 1

    .prologue
    .line 232
    packed-switch p0, :pswitch_data_0

    .line 238
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 233
    :pswitch_0
    sget-object v0, Lcom/google/android/d/h;->a:Lcom/google/android/d/h;

    goto :goto_0

    .line 234
    :pswitch_1
    sget-object v0, Lcom/google/android/d/h;->b:Lcom/google/android/d/h;

    goto :goto_0

    .line 235
    :pswitch_2
    sget-object v0, Lcom/google/android/d/h;->c:Lcom/google/android/d/h;

    goto :goto_0

    .line 236
    :pswitch_3
    sget-object v0, Lcom/google/android/d/h;->d:Lcom/google/android/d/h;

    goto :goto_0

    .line 237
    :pswitch_4
    sget-object v0, Lcom/google/android/d/h;->e:Lcom/google/android/d/h;

    goto :goto_0

    .line 232
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/d/h;
    .locals 1

    .prologue
    .line 181
    const-class v0, Lcom/google/android/d/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/d/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/d/h;
    .locals 1

    .prologue
    .line 181
    sget-object v0, Lcom/google/android/d/h;->g:[Lcom/google/android/d/h;

    invoke-virtual {v0}, [Lcom/google/android/d/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/d/h;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/google/android/d/h;->f:I

    return v0
.end method
