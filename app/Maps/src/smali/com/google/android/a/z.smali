.class Lcom/google/android/a/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/a/w;


# instance fields
.field private a:Lcom/google/n/l;

.field private b:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/z;->b:Ljava/io/ByteArrayOutputStream;

    iget-object v0, p0, Lcom/google/android/a/z;->b:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x1000

    new-instance v2, Lcom/google/n/l;

    new-array v1, v1, [B

    invoke-direct {v2, v0, v1}, Lcom/google/n/l;-><init>(Ljava/io/OutputStream;[B)V

    iput-object v2, p0, Lcom/google/android/a/z;->a:Lcom/google/n/l;

    .line 23
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 27
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/a/z;->b:Ljava/io/ByteArrayOutputStream;

    .line 28
    iget-object v0, p0, Lcom/google/android/a/z;->b:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x1000

    new-instance v2, Lcom/google/n/l;

    new-array v1, v1, [B

    invoke-direct {v2, v0, v1}, Lcom/google/n/l;-><init>(Ljava/io/OutputStream;[B)V

    iput-object v2, p0, Lcom/google/android/a/z;->a:Lcom/google/n/l;

    .line 29
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/a/z;->a:Lcom/google/n/l;

    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0, p2, p3}, Lcom/google/n/l;->a(J)V

    .line 34
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/a/z;->a:Lcom/google/n/l;

    const/4 v1, 0x2

    invoke-static {p1, v1}, Lcom/google/n/bt;->a(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/n/l;->b(I)V

    const-string v1, "UTF-8"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    array-length v2, v1

    invoke-virtual {v0, v2}, Lcom/google/n/l;->b(I)V

    invoke-virtual {v0, v1}, Lcom/google/n/l;->a([B)V

    .line 39
    return-void
.end method

.method public final b()[B
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/a/z;->a:Lcom/google/n/l;

    iget-object v1, v0, Lcom/google/n/l;->a:Ljava/io/OutputStream;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/n/l;->a()V

    .line 44
    :cond_0
    iget-object v0, p0, Lcom/google/android/a/z;->b:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
