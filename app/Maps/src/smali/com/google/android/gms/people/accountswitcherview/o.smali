.class Lcom/google/android/gms/people/accountswitcherview/o;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/people/accountswitcherview/n;

.field final b:Landroid/os/ParcelFileDescriptor;

.field final c:Landroid/graphics/Bitmap;

.field final d:I

.field final synthetic e:Lcom/google/android/gms/people/accountswitcherview/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/accountswitcherview/m;Lcom/google/android/gms/people/accountswitcherview/n;Landroid/os/ParcelFileDescriptor;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Lcom/google/android/gms/people/accountswitcherview/m;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 204
    iput-object p2, p0, Lcom/google/android/gms/people/accountswitcherview/o;->a:Lcom/google/android/gms/people/accountswitcherview/n;

    .line 205
    iput-object p3, p0, Lcom/google/android/gms/people/accountswitcherview/o;->b:Landroid/os/ParcelFileDescriptor;

    .line 206
    iput-object p4, p0, Lcom/google/android/gms/people/accountswitcherview/o;->c:Landroid/graphics/Bitmap;

    .line 207
    iput p5, p0, Lcom/google/android/gms/people/accountswitcherview/o;->d:I

    .line 208
    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 212
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->b:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/people/p;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 215
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Lcom/google/android/gms/people/accountswitcherview/m;

    iget-boolean v1, v1, Lcom/google/android/gms/people/accountswitcherview/m;->e:Z

    if-eqz v1, :cond_2

    .line 217
    if-nez v0, :cond_0

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->c:Landroid/graphics/Bitmap;

    .line 220
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 228
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Lcom/google/android/gms/people/accountswitcherview/m;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/m;->d:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/o;->a:Lcom/google/android/gms/people/accountswitcherview/n;

    iget-object v2, v2, Lcom/google/android/gms/people/accountswitcherview/n;->f:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_1

    .line 232
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 238
    :cond_1
    :goto_1
    return-object v1

    .line 222
    :cond_2
    if-nez v0, :cond_3

    .line 223
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->c:Landroid/graphics/Bitmap;

    move-object v1, v0

    goto :goto_0

    .line 225
    :cond_3
    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->d:I

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Lcom/google/android/gms/people/accountswitcherview/m;

    iget v2, v2, Lcom/google/android/gms/people/accountswitcherview/m;->f:F

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Lcom/google/android/gms/people/accountswitcherview/m;

    iget v3, v3, Lcom/google/android/gms/people/accountswitcherview/m;->g:F

    div-float/2addr v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/people/accountswitcherview/m;->a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 233
    :catch_0
    move-exception v0

    .line 234
    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_1

    .line 230
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->b:Landroid/os/ParcelFileDescriptor;

    if-eqz v1, :cond_4

    .line 232
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->b:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 235
    :cond_4
    :goto_2
    throw v0

    .line 233
    :catch_1
    move-exception v1

    .line 234
    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    goto :goto_2
.end method


# virtual methods
.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/o;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 197
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->a:Lcom/google/android/gms/people/accountswitcherview/n;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->a:Lcom/google/android/gms/people/accountswitcherview/n;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/o;->e:Lcom/google/android/gms/people/accountswitcherview/m;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/o;->a:Lcom/google/android/gms/people/accountswitcherview/n;

    iget-object v1, v1, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    if-eqz p1, :cond_2

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v2, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    const/4 v4, 0x0

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    aput-object v5, v3, v4

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, v0, Lcom/google/android/gms/people/accountswitcherview/m;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v4, v5, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v4, v3, v6

    invoke-direct {v2, v3}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    sget v3, Lcom/google/android/gms/people/accountswitcherview/m;->a:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/m;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v3, 0x10e0000

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    sput v0, Lcom/google/android/gms/people/accountswitcherview/m;->a:I

    :cond_0
    invoke-virtual {v2, v6}, Landroid/graphics/drawable/TransitionDrawable;->setCrossFadeEnabled(Z)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    sget v0, Lcom/google/android/gms/people/accountswitcherview/m;->a:I

    invoke-virtual {v2, v0}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
