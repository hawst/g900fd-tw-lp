.class public Lcom/google/android/gms/car/support/CarRestrictedEditText;
.super Landroid/widget/EditText;

# interfaces
.implements Lcom/google/android/gms/car/a/a;


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Lcom/google/android/gms/car/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v2, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->a:I

    iput v2, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->b:I

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->getInputType()I

    move-result v0

    const/high16 v1, 0x80000

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setInputType(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setTextIsSelectable(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setSelection(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->c:Z

    return-void
.end method


# virtual methods
.method protected onSelectionChanged(II)V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->c:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->d:Lcom/google/android/gms/car/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->d:Lcom/google/android/gms/car/a/b;

    iget v1, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->b:I

    iget v2, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->a:I

    invoke-interface {v0, v1, v2, p1, p2}, Lcom/google/android/gms/car/a/b;->a(IIII)V

    :cond_0
    iput p1, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->b:I

    iput p2, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->a:I

    return-void
.end method

.method public final setCarEditableListener(Lcom/google/android/gms/car/a/b;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->d:Lcom/google/android/gms/car/a/b;

    return-void
.end method

.method public final setInputEnabled(Z)V
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarRestrictedEditText;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
