.class Lcom/google/android/gms/car/support/x;
.super Landroid/telephony/PhoneStateListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/w;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/w;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCallStateChanged(ILjava/lang/String;)V
    .locals 3

    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "onCallStateChanged state="

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    iget-object v2, v0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    invoke-static {v0}, Lcom/google/android/gms/car/support/w;->a(Lcom/google/android/gms/car/support/w;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/w;->a(Z)V

    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onDataActivity(I)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onDataActivity: direction="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    invoke-static {v0}, Lcom/google/android/gms/car/support/w;->a(Lcom/google/android/gms/car/support/w;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/w;->a(Z)V

    return-void
.end method

.method public onDataConnectionStateChanged(II)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onDataConnectionStateChanged: state="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    invoke-static {v0}, Lcom/google/android/gms/car/support/w;->a(Lcom/google/android/gms/car/support/w;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/w;->a(Z)V

    return-void
.end method

.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onServiceStateChanged voiceState="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    iput-object p1, v0, Lcom/google/android/gms/car/support/w;->c:Landroid/telephony/ServiceState;

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    invoke-static {v0}, Lcom/google/android/gms/car/support/w;->a(Lcom/google/android/gms/car/support/w;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/w;->a(Z)V

    return-void
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "onSignalStrengthsChanged signalStrength="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    iput-object p1, v0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    invoke-static {v0}, Lcom/google/android/gms/car/support/w;->a(Lcom/google/android/gms/car/support/w;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/x;->a:Lcom/google/android/gms/car/support/w;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/w;->a(Z)V

    return-void
.end method
