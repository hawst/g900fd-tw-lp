.class public Lcom/google/android/gms/location/reporting/c;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/b;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/internal/pa;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/pa;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3, v1}, Lcom/google/android/gms/internal/pa;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;Lcom/google/android/gms/location/places/PlaceReport;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/internal/pa;->a(Landroid/accounts/Account;Lcom/google/android/gms/location/places/PlaceReport;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/pa;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/pa;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/pa;->a()V

    return-void
.end method

.method public final b(Landroid/accounts/Account;)I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/pa;->b(Landroid/accounts/Account;)I

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/pa;->l()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Lcom/google/android/gms/internal/pa;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/pa;->b()V

    return-void
.end method
