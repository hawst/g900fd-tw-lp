.class public Lcom/google/android/gms/car/support/z;
.super Landroid/graphics/drawable/Drawable;


# instance fields
.field a:F

.field private final b:Landroid/graphics/Paint;

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F

.field private i:F

.field private final j:Landroid/graphics/RectF;

.field private final k:Landroid/graphics/RectF;

.field private final l:Landroid/graphics/RectF;

.field private final m:Z

.field private final n:I

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/google/android/gms/car/support/aa;

    const-class v1, Ljava/lang/Float;

    const-string v2, "progress"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/aa;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/high16 v0, 0x42000000    # 32.0f

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/support/z;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;I)V
    .locals 4

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/car/support/z;->j:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/car/support/z;->k:Landroid/graphics/RectF;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/car/support/z;->l:Landroid/graphics/RectF;

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/z;->o:Z

    iget-object v2, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v2

    if-ne v2, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/z;->m:Z

    iput p2, p0, Lcom/google/android/gms/car/support/z;->n:I

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 13

    const/high16 v3, 0x43340000    # 180.0f

    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/z;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    iget v0, p0, Lcom/google/android/gms/car/support/z;->e:F

    iget v2, p0, Lcom/google/android/gms/car/support/z;->f:F

    iget v5, p0, Lcom/google/android/gms/car/support/z;->a:F

    sub-float/2addr v2, v0

    mul-float/2addr v2, v5

    add-float v5, v0, v2

    const/high16 v0, 0x42340000    # 45.0f

    iget v2, p0, Lcom/google/android/gms/car/support/z;->a:F

    sub-float/2addr v0, v1

    mul-float/2addr v0, v2

    add-float v6, v1, v0

    iget v0, p0, Lcom/google/android/gms/car/support/z;->g:F

    iget v2, p0, Lcom/google/android/gms/car/support/z;->h:F

    iget v7, p0, Lcom/google/android/gms/car/support/z;->a:F

    sub-float/2addr v2, v0

    mul-float/2addr v2, v7

    add-float v7, v0, v2

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/z;->m:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/car/support/z;->m:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_1
    iget v8, p0, Lcom/google/android/gms/car/support/z;->a:F

    sub-float/2addr v2, v0

    mul-float/2addr v2, v8

    add-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/car/support/z;->i:F

    neg-float v2, v2

    iget v8, p0, Lcom/google/android/gms/car/support/z;->a:F

    sub-float/2addr v2, v1

    mul-float/2addr v2, v8

    add-float/2addr v2, v1

    iget v8, p0, Lcom/google/android/gms/car/support/z;->d:F

    iget v9, p0, Lcom/google/android/gms/car/support/z;->a:F

    sub-float/2addr v1, v8

    mul-float/2addr v1, v9

    add-float/2addr v1, v8

    iget-object v8, p0, Lcom/google/android/gms/car/support/z;->k:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v9

    iget v10, p0, Lcom/google/android/gms/car/support/z;->c:F

    mul-float/2addr v10, v12

    sub-float/2addr v9, v10

    add-float/2addr v5, v7

    invoke-virtual {v4}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v10

    iget v11, p0, Lcom/google/android/gms/car/support/z;->c:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    invoke-virtual {v8, v7, v9, v5, v10}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v5, p0, Lcom/google/android/gms/car/support/z;->j:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/google/android/gms/car/support/z;->k:Landroid/graphics/RectF;

    invoke-virtual {v5, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v5, p0, Lcom/google/android/gms/car/support/z;->j:Landroid/graphics/RectF;

    neg-float v7, v1

    invoke-virtual {v5, v2, v7}, Landroid/graphics/RectF;->offset(FF)V

    iget-object v5, p0, Lcom/google/android/gms/car/support/z;->l:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/google/android/gms/car/support/z;->k:Landroid/graphics/RectF;

    invoke-virtual {v5, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v5, p0, Lcom/google/android/gms/car/support/z;->l:Landroid/graphics/RectF;

    invoke-virtual {v5, v2, v1}, Landroid/graphics/RectF;->offset(FF)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/z;->o:Z

    if-eqz v1, :cond_3

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/z;->k:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    neg-float v0, v6

    iget-object v1, p0, Lcom/google/android/gms/car/support/z;->j:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/google/android/gms/car/support/z;->j:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v12

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/gms/car/support/z;->j:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/z;->j:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/google/android/gms/car/support/z;->l:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/google/android/gms/car/support/z;->l:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    mul-float/2addr v1, v12

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/car/support/z;->l:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-virtual {p1, v6, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/z;->l:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :cond_1
    const/high16 v0, -0x3ccc0000    # -180.0f

    goto/16 :goto_0

    :cond_2
    move v2, v1

    goto/16 :goto_1

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/z;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v3, v0, v1}, Landroid/graphics/Canvas;->rotate(FFF)V

    goto :goto_2
.end method

.method public getIntrinsicHeight()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/support/z;->n:I

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/car/support/z;->n:I

    return v0
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method public isAutoMirrored()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 5

    const/high16 v4, 0x40c00000    # 6.0f

    const/high16 v3, 0x42900000    # 72.0f

    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->onBoundsChange(Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v1, v0

    mul-float/2addr v1, v4

    div-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/car/support/z;->c:F

    const/high16 v1, 0x41900000    # 18.0f

    int-to-float v2, v0

    mul-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/car/support/z;->d:F

    const/high16 v1, 0x427c0000    # 63.0f

    int-to-float v2, v0

    mul-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/car/support/z;->e:F

    const/high16 v1, 0x42280000    # 42.0f

    int-to-float v2, v0

    mul-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/car/support/z;->f:F

    int-to-float v1, v0

    mul-float/2addr v1, v4

    div-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/car/support/z;->g:F

    const/high16 v1, 0x41c00000    # 24.0f

    int-to-float v2, v0

    mul-float/2addr v1, v2

    div-float/2addr v1, v3

    iput v1, p0, Lcom/google/android/gms/car/support/z;->h:F

    int-to-float v0, v0

    mul-float/2addr v0, v4

    div-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/gms/car/support/z;->i:F

    return-void
.end method

.method public setAlpha(I)V
    .locals 0

    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/z;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    return-void
.end method
