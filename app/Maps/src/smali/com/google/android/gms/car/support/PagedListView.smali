.class public Lcom/google/android/gms/car/support/PagedListView;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/gms/car/support/PagedRecyclerView;

.field final c:Lcom/google/android/gms/car/support/an;

.field final d:Landroid/widget/ImageView;

.field final e:Landroid/widget/ImageView;

.field final f:Landroid/os/Handler;

.field g:I

.field h:Z

.field i:I

.field final j:Ljava/lang/Runnable;

.field private final k:Landroid/view/View;

.field private final l:Lcom/google/android/gms/car/support/aw;

.field private final m:Landroid/view/animation/Interpolator;

.field private n:I

.field private o:I

.field private p:Landroid/support/v7/widget/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v7/widget/bk",
            "<+",
            "Landroid/support/v7/widget/ce;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Landroid/support/v7/widget/bu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/support/PagedListView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/gms/car/support/PagedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/car/support/PagedListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 6

    const v5, 0x7f02008d

    const/4 v4, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->f:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/car/support/aw;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/aw;-><init>(Lcom/google/android/gms/car/support/PagedListView;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->l:Lcom/google/android/gms/car/support/aw;

    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->m:Landroid/view/animation/Interpolator;

    iput v2, p0, Lcom/google/android/gms/car/support/PagedListView;->n:I

    iput v2, p0, Lcom/google/android/gms/car/support/PagedListView;->o:I

    iput v3, p0, Lcom/google/android/gms/car/support/PagedListView;->g:I

    iput-boolean v3, p0, Lcom/google/android/gms/car/support/PagedListView;->h:Z

    iput v3, p0, Lcom/google/android/gms/car/support/PagedListView;->i:I

    new-instance v0, Lcom/google/android/gms/car/support/ar;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/ar;-><init>(Lcom/google/android/gms/car/support/PagedListView;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->q:Landroid/support/v7/widget/bu;

    new-instance v0, Lcom/google/android/gms/car/support/au;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/au;-><init>(Lcom/google/android/gms/car/support/PagedListView;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->j:Ljava/lang/Runnable;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040031

    invoke-virtual {v0, v1, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    const v0, 0x7f10011c

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/PagedListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/PagedRecyclerView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    const v0, 0x7f100119

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/PagedListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->k:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/support/ay;->a(Landroid/content/ContentResolver;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/PagedListView;->n:I

    new-instance v0, Lcom/google/android/gms/car/support/an;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/support/an;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    new-instance v1, Lcom/google/android/gms/car/support/aq;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/aq;-><init>(Lcom/google/android/gms/car/support/PagedListView;)V

    iput-object v1, v0, Lcom/google/android/gms/car/support/an;->c:Lcom/google/android/gms/car/support/ao;

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedRecyclerView;->setLayoutManager(Landroid/support/v7/widget/bs;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->l:Lcom/google/android/gms/car/support/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedRecyclerView;->a(Landroid/support/v7/widget/bq;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->q:Landroid/support/v7/widget/bu;

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/support/v7/widget/bu;

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/bw;

    iget-object v1, v0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    if-nez v1, :cond_0

    new-instance v1, Landroid/support/v7/widget/bv;

    invoke-direct {v1}, Landroid/support/v7/widget/bv;-><init>()V

    iput-object v1, v0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    :cond_0
    iget-object v0, v0, Landroid/support/v7/widget/bw;->e:Landroid/support/v7/widget/bv;

    const/16 v1, 0xc

    invoke-virtual {v0, v3, v1}, Landroid/support/v7/widget/bv;->a(II)V

    const v0, 0x7f10011a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/PagedListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    const-string v1, "ic_up"

    invoke-static {p1, v1}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    const v0, 0x7f10011b

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/PagedListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    const-string v1, "ic_down"

    invoke-static {p1, v1}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    sget-object v0, Lcom/google/android/gms/car/support/bb;->a:[I

    invoke-virtual {p1, p2, v0, p3, p4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iput-boolean v1, v2, Lcom/google/android/gms/car/support/PagedRecyclerView;->H:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->l:Lcom/google/android/gms/car/support/aw;

    iget-object v1, v0, Lcom/google/android/gms/car/support/aw;->a:Landroid/graphics/Paint;

    iget-object v0, v0, Lcom/google/android/gms/car/support/aw;->b:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a002c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/car/support/PagedListView;->a(Z)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10011a

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getHeight()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->a(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f10011b

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getHeight()I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->a(II)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;ZZ)V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    if-eqz p3, :cond_1

    const/16 v0, 0xc8

    :goto_0
    if-eqz p2, :cond_3

    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->m:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/car/support/as;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/support/as;-><init>(Lcom/google/android/gms/car/support/PagedListView;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withStartAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v3

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->m:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/car/support/at;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/support/at;-><init>(Lcom/google/android/gms/car/support/PagedListView;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    goto :goto_1
.end method

.method private c()V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/support/an;->e(I)Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_3

    :goto_1
    if-nez v0, :cond_4

    move v0, v1

    :goto_2
    iput v0, p0, Lcom/google/android/gms/car/support/PagedListView;->o:I

    iget v0, p0, Lcom/google/android/gms/car/support/PagedListView;->n:I

    if-ltz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/car/support/PagedListView;->o:I

    iget v0, p0, Lcom/google/android/gms/car/support/PagedListView;->n:I

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->a()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    iget-object v2, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    invoke-virtual {v2}, Landroid/support/v7/widget/bk;->a()I

    move-result v2

    if-ge v2, v0, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    iget-object v1, v1, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    invoke-virtual {v1, v2, v0}, Landroid/support/v7/widget/bl;->a(II)V

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedListView;->getHeight()I

    move-result v2

    div-int v0, v2, v0

    goto :goto_2

    :cond_5
    if-le v2, v0, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    iget-object v2, v2, Landroid/support/v7/widget/bk;->a:Landroid/support/v7/widget/bl;

    invoke-virtual {v2, v0, v1}, Landroid/support/v7/widget/bl;->b(II)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/car/support/PagedRecyclerView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    return-object v0
.end method

.method a(II)V
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    if-ne p2, v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/an;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/an;->f()I

    move-result v2

    const/4 v1, 0x0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_1
    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/an;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v3

    if-gt v3, p1, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v1, v0

    if-nez v1, :cond_3

    sget-object v0, Lcom/google/android/gms/car/support/PagedListView;->a:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-static {v1}, Lcom/google/android/gms/car/support/an;->b(Landroid/view/View;)I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    if-ne v1, p1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v3, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v2, v1, v0}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/RecyclerView;I)V

    goto :goto_0

    :cond_5
    if-nez p2, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/an;->j()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/support/PagedRecyclerView;->scrollBy(II)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    new-instance v1, Lcom/google/android/gms/car/support/av;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/support/av;-><init>(Lcom/google/android/gms/car/support/PagedListView;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedRecyclerView;->postOnAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/google/android/gms/car/support/PagedListView;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Invalid scroll direction ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method a(Z)V
    .locals 7

    const/4 v6, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/an;->j()Z

    move-result v3

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/an;->k()Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    if-nez v3, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v5, v0, p1}, Lcom/google/android/gms/car/support/PagedListView;->a(Landroid/view/View;ZZ)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    if-nez v4, :cond_2

    :goto_1
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/car/support/PagedListView;->a(Landroid/view/View;ZZ)V

    if-eqz v3, :cond_3

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->k:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedListView;->invalidate()V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    const/16 v0, 0x82

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    iget v0, v0, Lcom/google/android/gms/car/support/an;->b:I

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/an;->a(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/FrameLayout;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->l:Lcom/google/android/gms/car/support/aw;

    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    if-eqz v2, :cond_0

    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    const-string v3, "Cannot remove item decoration during a scroll  or layout"

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/bs;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, v1, Landroid/support/v7/widget/RecyclerView;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v1}, Landroid/support/v4/view/at;->a(Landroid/view/View;)I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    :cond_1
    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->h()V

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/an;->f()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/support/PagedListView;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 1

    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    invoke-virtual {v0}, Landroid/support/v7/widget/bk;->a()I

    move-result v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/car/support/PagedListView;->c()V

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/PagedListView;->a(Z)V

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/google/android/gms/car/support/PagedListView;->a(Landroid/view/View;)V

    const/4 v0, 0x1

    return v0
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    return-void
.end method

.method public final setAdapter(Landroid/support/v7/widget/bk;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/bk",
            "<+",
            "Landroid/support/v7/widget/ce;",
            ">;)V"
        }
    .end annotation

    instance-of v0, p1, Lcom/google/android/gms/car/support/ax;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ERROR: adapter ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] MUST implement ItemCap"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/support/PagedListView;->p:Landroid/support/v7/widget/bk;

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/PagedRecyclerView;->setAdapter(Landroid/support/v7/widget/bk;)V

    invoke-direct {p0}, Lcom/google/android/gms/car/support/PagedListView;->c()V

    return-void
.end method

.method public final setDarkMode()V
    .locals 4

    const v3, 0x7f02008e

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a003b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->l:Lcom/google/android/gms/car/support/aw;

    iget-object v1, v0, Lcom/google/android/gms/car/support/aw;->a:Landroid/graphics/Paint;

    iget-object v0, v0, Lcom/google/android/gms/car/support/aw;->b:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a002d

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public final setLightMode()V
    .locals 4

    const v3, 0x7f02008f

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a003c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/PagedListView;->l:Lcom/google/android/gms/car/support/aw;

    iget-object v1, v0, Lcom/google/android/gms/car/support/aw;->a:Landroid/graphics/Paint;

    iget-object v0, v0, Lcom/google/android/gms/car/support/aw;->b:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a002e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public final setMaxPages(I)V
    .locals 0

    iput p1, p0, Lcom/google/android/gms/car/support/PagedListView;->n:I

    invoke-direct {p0}, Lcom/google/android/gms/car/support/PagedListView;->c()V

    return-void
.end method
