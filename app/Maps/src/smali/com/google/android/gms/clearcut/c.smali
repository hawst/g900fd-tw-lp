.class public Lcom/google/android/gms/clearcut/c;
.super Ljava/lang/Object;


# instance fields
.field final synthetic a:Lcom/google/android/gms/clearcut/a;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:Lcom/google/android/gms/clearcut/d;

.field private final g:Lcom/google/r/a/a/a/a/d;

.field private h:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/clearcut/a;[B)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;[BLcom/google/android/gms/clearcut/d;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/clearcut/a;[BLcom/google/android/gms/clearcut/d;)V
    .locals 4

    iput-object p1, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/android/gms/clearcut/a;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/clearcut/c;->b:I

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->b(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->c(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->d(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->e:Ljava/lang/String;

    new-instance v0, Lcom/google/r/a/a/a/a/d;

    invoke-direct {v0}, Lcom/google/r/a/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/r/a/a/a/a/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->c(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->d:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->d(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/c;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/r/a/a/a/a/d;

    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->e(Lcom/google/android/gms/clearcut/a;)Lcom/google/android/gms/common/b/c;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/b/c;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/r/a/a/a/a/d;->a:J

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/r/a/a/a/a/d;

    iput-object p2, v0, Lcom/google/r/a/a/a/a/d;->h:[B

    :cond_0
    iput-object p3, p0, Lcom/google/android/gms/clearcut/c;->f:Lcom/google/android/gms/clearcut/d;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/clearcut/c;->d:Ljava/lang/String;

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/common/api/s;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/o;",
            ")",
            "Lcom/google/android/gms/common/api/s",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    const/4 v7, 0x1

    iget-boolean v0, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "do not reuse LogEventBuilder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-boolean v7, p0, Lcom/google/android/gms/clearcut/c;->h:Z

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-static {v0}, Lcom/google/android/gms/clearcut/a;->h(Lcom/google/android/gms/clearcut/a;)Lcom/google/android/gms/clearcut/e;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/clearcut/LogEventParcelable;

    new-instance v0, Lcom/google/android/gms/internal/sa;

    iget-object v1, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-static {v1}, Lcom/google/android/gms/clearcut/a;->f(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/clearcut/c;->a:Lcom/google/android/gms/clearcut/a;

    invoke-static {v2}, Lcom/google/android/gms/clearcut/a;->g(Lcom/google/android/gms/clearcut/a;)I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/clearcut/c;->b:I

    iget-object v4, p0, Lcom/google/android/gms/clearcut/c;->c:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/clearcut/c;->d:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/clearcut/c;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/internal/sa;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/r/a/a/a/a/d;

    iget-object v2, p0, Lcom/google/android/gms/clearcut/c;->f:Lcom/google/android/gms/clearcut/d;

    invoke-direct {v9, v0, v1, v2}, Lcom/google/android/gms/clearcut/LogEventParcelable;-><init>(Lcom/google/android/gms/internal/sa;Lcom/google/r/a/a/a/a/d;Lcom/google/android/gms/clearcut/d;)V

    invoke-interface {v8, p1, v9}, Lcom/google/android/gms/clearcut/e;->a(Lcom/google/android/gms/common/api/o;Lcom/google/android/gms/clearcut/LogEventParcelable;)Lcom/google/android/gms/common/api/s;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/clearcut/c;->g:Lcom/google/r/a/a/a/a/d;

    iput-object p1, v0, Lcom/google/r/a/a/a/a/d;->b:Ljava/lang/String;

    return-object p0
.end method
