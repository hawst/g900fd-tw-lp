.class Lcom/google/android/gms/car/support/u;
.super Landroid/support/v4/widget/bn;


# instance fields
.field a:Landroid/support/v4/widget/bk;

.field final synthetic b:Lcom/google/android/gms/car/support/CarDrawerLayout;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/CarDrawerLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/bn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)I
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {p2, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-boolean v2, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    :cond_0
    :goto_0
    iget-object v0, v1, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget v0, v0, Landroid/support/v4/widget/bk;->a:I

    iget v2, v1, Lcom/google/android/gms/car/support/CarDrawerLayout;->c:I

    if-eq v0, v2, :cond_1

    iget-object v0, v1, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget v0, v0, Landroid/support/v4/widget/bk;->a:I

    iput v0, v1, Lcom/google/android/gms/car/support/CarDrawerLayout;->c:I

    :cond_1
    return-void

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v3

    if-nez v0, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-boolean v2, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    goto :goto_0
.end method

.method public final a(II)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v1

    and-int/lit8 v2, p1, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    const/4 v3, 0x5

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->a:Landroid/support/v4/widget/bk;

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;I)V

    :cond_1
    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    const/4 v3, 0x3

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;F)V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    const/4 v5, 0x3

    invoke-virtual {v4, v3, v5}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v3

    if-eqz v3, :cond_1

    cmpl-float v0, p2, v6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->a:Landroid/support/v4/widget/bk;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/widget/bk;->a(II)Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->invalidate()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    cmpg-float v1, p2, v6

    if-gez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v2, v0, v1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final a(Landroid/view/View;II)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    iget-object v3, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v3}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_1

    int-to-float v1, p2

    int-to-float v0, v0

    div-float v0, v1, v0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    iget-object v3, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v3}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iput v1, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->d()V

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->e()V

    invoke-virtual {v2, p3}, Landroid/view/View;->offsetLeftAndRight(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->f()V

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->invalidate()V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, p2

    sub-int/2addr v1, v3

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    move v1, v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    if-ne p1, v1, :cond_2

    move v1, v2

    :goto_0
    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/support/u;->a:Landroid/support/v4/widget/bk;

    invoke-virtual {v1, v3}, Landroid/support/v4/widget/bk;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    iget-boolean v4, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v4, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    :cond_1
    :goto_2
    return v1

    :cond_2
    move v1, v3

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_1

    iget-boolean v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/u;->b:Lcom/google/android/gms/car/support/CarDrawerLayout;

    iput-boolean v3, v0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    goto :goto_2
.end method

.method public final c(Landroid/view/View;)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final d(Landroid/view/View;)I
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method
