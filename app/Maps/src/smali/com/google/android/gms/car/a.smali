.class public final Lcom/google/android/gms/car/a;
.super Ljava/lang/Object;


# static fields
.field static final a:Lcom/google/android/gms/common/api/h;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/h",
            "<",
            "Lcom/google/android/gms/car/eq;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Lcom/google/android/gms/common/api/a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/a",
            "<",
            "Lcom/google/android/gms/car/g;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:Lcom/google/android/gms/car/d;

.field public static final d:Lcom/google/android/gms/car/f;

.field private static final e:Lcom/google/android/gms/common/api/g;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/g",
            "<",
            "Lcom/google/android/gms/car/eq;",
            "Lcom/google/android/gms/car/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/h;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    new-instance v0, Lcom/google/android/gms/car/b;

    invoke-direct {v0}, Lcom/google/android/gms/car/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/a;->e:Lcom/google/android/gms/common/api/g;

    new-instance v0, Lcom/google/android/gms/common/api/a;

    sget-object v1, Lcom/google/android/gms/car/a;->e:Lcom/google/android/gms/common/api/g;

    sget-object v2, Lcom/google/android/gms/car/a;->a:Lcom/google/android/gms/common/api/h;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/a;-><init>(Lcom/google/android/gms/common/api/g;Lcom/google/android/gms/common/api/h;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/car/a;->b:Lcom/google/android/gms/common/api/a;

    new-instance v0, Lcom/google/android/gms/car/j;

    invoke-direct {v0}, Lcom/google/android/gms/car/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/a;->c:Lcom/google/android/gms/car/d;

    new-instance v0, Lcom/google/android/gms/car/k;

    invoke-direct {v0}, Lcom/google/android/gms/car/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/a;->d:Lcom/google/android/gms/car/f;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/car/e;)Lcom/google/android/gms/common/api/o;
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/car/a;->b:Lcom/google/android/gms/common/api/a;

    new-instance v2, Lcom/google/android/gms/car/i;

    invoke-direct {v2, p3}, Lcom/google/android/gms/car/i;-><init>(Lcom/google/android/gms/car/e;)V

    new-instance v3, Lcom/google/android/gms/car/g;

    invoke-direct {v3, v2}, Lcom/google/android/gms/car/g;-><init>(Lcom/google/android/gms/car/i;)V

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, v0, Lcom/google/android/gms/common/api/p;->b:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/car/e;)Lcom/google/android/gms/common/api/o;
    .locals 1

    new-instance v0, Lcom/google/android/gms/car/c;

    invoke-direct {v0}, Lcom/google/android/gms/car/c;-><init>()V

    invoke-static {p0, v0, p1, p2}, Lcom/google/android/gms/car/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/q;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/car/e;)Lcom/google/android/gms/common/api/o;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/o;)V
    .locals 2

    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "GoogleApiClient is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method
