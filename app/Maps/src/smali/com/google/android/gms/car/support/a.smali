.class Lcom/google/android/gms/car/support/a;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/CarAppLayout;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/CarAppLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/a;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.car.category.CATEGORY_PROJECTION"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.google.android.googlequicksearchbox"

    const-string v3, "com.google.android.projection.SearchProjectionService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/car/support/a;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v2, v1, Lcom/google/android/gms/car/support/CarAppLayout;->f:Landroid/os/Handler;

    if-nez v2, :cond_0

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "NotificationHandlerThread"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, v1, Lcom/google/android/gms/car/support/CarAppLayout;->e:Landroid/os/HandlerThread;

    iget-object v2, v1, Lcom/google/android/gms/car/support/CarAppLayout;->e:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Landroid/os/Handler;

    iget-object v3, v1, Lcom/google/android/gms/car/support/CarAppLayout;->e:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/gms/car/support/CarAppLayout;->m:Landroid/os/Handler$Callback;

    invoke-direct {v2, v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v2, v1, Lcom/google/android/gms/car/support/CarAppLayout;->f:Landroid/os/Handler;

    iget-object v2, v1, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/car/support/n;

    invoke-direct {v3, v1}, Lcom/google/android/gms/car/support/n;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    new-instance v4, Lcom/google/android/gms/car/support/o;

    invoke-direct {v4, v1}, Lcom/google/android/gms/car/support/o;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/car/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/car/e;)Lcom/google/android/gms/common/api/o;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/car/support/CarAppLayout;->g:Lcom/google/android/gms/common/api/o;

    iget-object v2, v1, Lcom/google/android/gms/car/support/CarAppLayout;->g:Lcom/google/android/gms/common/api/o;

    invoke-interface {v2}, Lcom/google/android/gms/common/api/o;->b()V

    :cond_0
    iget-object v1, v1, Lcom/google/android/gms/car/support/CarAppLayout;->f:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    return-void
.end method
