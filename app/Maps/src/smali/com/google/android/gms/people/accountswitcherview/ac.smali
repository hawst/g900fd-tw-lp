.class Lcom/google/android/gms/people/accountswitcherview/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/people/accountswitcherview/af;


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;)V
    .locals 0

    .prologue
    .line 1073
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/ac;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Lcom/google/android/gms/people/accountswitcherview/ae;
    .locals 3

    .prologue
    .line 1076
    new-instance v1, Lcom/google/android/gms/people/accountswitcherview/ae;

    invoke-direct {v1}, Lcom/google/android/gms/people/accountswitcherview/ae;-><init>()V

    .line 1077
    iput-object p1, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->b:Landroid/view/View;

    .line 1078
    const v0, 0x7f10023a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->c:Landroid/view/View;

    .line 1079
    const v0, 0x7f1000a8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->e:Landroid/view/View;

    .line 1080
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->e:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->k:Landroid/widget/ImageView;

    .line 1081
    const v0, 0x7f10023b

    .line 1082
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->f:Landroid/widget/TextView;

    .line 1083
    const v0, 0x7f1000a9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->g:Landroid/widget/TextView;

    .line 1084
    const v0, 0x7f100238

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->j:Landroid/widget/ImageView;

    .line 1085
    const v0, 0x7f10023f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->d:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    .line 1086
    const v0, 0x7f100239

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->a:Landroid/view/View;

    .line 1087
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/ac;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    const v2, 0x7f100236

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->findViewById(I)Landroid/view/View;

    .line 1088
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/ac;->a:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-boolean v0, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->j:Z

    if-eqz v0, :cond_0

    .line 1089
    const v0, 0x7f100009

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->h:Landroid/view/View;

    .line 1090
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->h:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->l:Landroid/widget/ImageView;

    .line 1091
    const v0, 0x7f1000b6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->i:Landroid/view/View;

    .line 1092
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->i:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->m:Landroid/widget/ImageView;

    .line 1093
    const v0, 0x7f1000b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->q:Landroid/view/View;

    .line 1094
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->q:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->u:Landroid/widget/ImageView;

    .line 1095
    const v0, 0x7f100237

    .line 1096
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->r:Landroid/widget/ImageView;

    .line 1097
    const v0, 0x7f10023c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->n:Landroid/view/View;

    .line 1098
    const v0, 0x7f10023d

    .line 1099
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->o:Landroid/widget/TextView;

    .line 1100
    const v0, 0x7f10023e

    .line 1101
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->p:Landroid/widget/TextView;

    .line 1102
    const v0, 0x7f1000b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->s:Landroid/view/View;

    .line 1103
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->s:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->v:Landroid/widget/ImageView;

    .line 1104
    const v0, 0x7f1000b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->t:Landroid/view/View;

    .line 1105
    iget-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->t:Landroid/view/View;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/people/accountswitcherview/ae;->w:Landroid/widget/ImageView;

    .line 1108
    :cond_0
    return-object v1
.end method
