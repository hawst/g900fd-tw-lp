.class Lcom/google/android/gms/car/support/m;
.super Landroid/content/BroadcastReceiver;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/CarAppLayout;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/CarAppLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/m;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/m;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->e:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/m;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->isInterrupted()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/m;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->e:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->interrupt()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/m;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarAppLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/m;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, v1, Lcom/google/android/gms/car/support/CarAppLayout;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
