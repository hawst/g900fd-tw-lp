.class public Lcom/google/android/gms/people/accountswitcherview/j;
.super Lcom/google/android/gms/people/accountswitcherview/m;
.source "PG"


# static fields
.field private static h:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/accountswitcherview/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;Z)V

    .line 30
    return-void
.end method

.method protected static a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/gms/people/accountswitcherview/j;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020033

    .line 73
    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/accountswitcherview/j;->h:Landroid/graphics/Bitmap;

    .line 76
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/accountswitcherview/j;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/k;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/accountswitcherview/k;-><init>(Lcom/google/android/gms/people/accountswitcherview/j;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 68
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/m;->b:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/people/accountswitcherview/j;->h:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020033

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/people/accountswitcherview/j;->h:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v1, Lcom/google/android/gms/people/accountswitcherview/j;->h:Landroid/graphics/Bitmap;

    .line 67
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/j;->a(Lcom/google/android/gms/people/accountswitcherview/n;)V

    .line 69
    return-void
.end method
