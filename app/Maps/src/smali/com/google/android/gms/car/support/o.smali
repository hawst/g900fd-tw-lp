.class Lcom/google/android/gms/car/support/o;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/car/e;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/CarAppLayout;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/CarAppLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/o;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/o;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->h:Ljava/util/concurrent/BlockingQueue;

    iget-object v1, p0, Lcom/google/android/gms/car/support/o;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, v1, Lcom/google/android/gms/car/support/CarAppLayout;->g:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/o;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v1, v0, Lcom/google/android/gms/car/support/CarAppLayout;->h:Ljava/util/concurrent/BlockingQueue;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/o;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->h:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/o;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->h:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->clear()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
