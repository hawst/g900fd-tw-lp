.class public Lcom/google/android/gms/car/support/an;
.super Landroid/support/v7/widget/ai;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:I

.field public c:Lcom/google/android/gms/car/support/ao;

.field private final d:Landroid/content/Context;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/support/an;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Landroid/support/v7/widget/ai;-><init>(IZ)V

    iput v0, p0, Lcom/google/android/gms/car/support/an;->b:I

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/an;->e:Z

    iput-object p1, p0, Lcom/google/android/gms/car/support/an;->d:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method protected final a(Landroid/support/v7/widget/cc;)I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    :goto_1
    sub-int/2addr v0, v2

    iget-object v2, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v2, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v1

    :cond_0
    sub-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v7/widget/ai;->a(Landroid/support/v7/widget/RecyclerView;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/an;->c:Lcom/google/android/gms/car/support/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/an;->c:Lcom/google/android/gms/car/support/ao;

    invoke-interface {v0}, Lcom/google/android/gms/car/support/ao;->a()V

    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 4

    new-instance v1, Lcom/google/android/gms/car/support/ap;

    iget-object v2, p0, Lcom/google/android/gms/car/support/an;->d:Landroid/content/Context;

    iget v0, p1, Landroid/support/v7/widget/RecyclerView;->y:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/car/support/ap;-><init>(Lcom/google/android/gms/car/support/an;Landroid/content/Context;Z)V

    iput p2, v1, Landroid/support/v7/widget/ca;->g:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/support/an;->a(Landroid/support/v7/widget/ca;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/an;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/ai;->b(ILandroid/support/v7/widget/bw;Landroid/support/v7/widget/cc;)I

    move-result v0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 0

    invoke-super {p0, p1}, Landroid/support/v7/widget/ai;->c(I)V

    iput p1, p0, Lcom/google/android/gms/car/support/an;->b:I

    return-void
.end method

.method public final e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/an;->e:Z

    return v0
.end method

.method public final j()Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/an;->f()I

    move-result v0

    if-gt v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/support/an;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/support/an;->b(Landroid/view/View;)I

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v0

    :goto_1
    if-ne v3, v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final k()Z
    .locals 6

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/an;->f()I

    move-result v0

    if-gt v0, v3, :cond_1

    move v2, v3

    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/an;->f()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/an;->e(I)Landroid/view/View;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/car/support/an;->b(Landroid/view/View;)I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->g:Landroid/support/v7/widget/bk;

    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/support/v7/widget/bk;->a()I

    move-result v1

    :goto_2
    add-int/lit8 v1, v1, -0x1

    if-ne v5, v1, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;->d:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v5

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v4, v1, v0

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v0

    :goto_3
    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    if-eqz v1, :cond_5

    iget-object v1, p0, Landroid/support/v7/widget/bs;->n:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v1

    :goto_4
    sub-int/2addr v0, v1

    if-gt v4, v0, :cond_0

    move v2, v3

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4
.end method
