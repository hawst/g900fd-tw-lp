.class public abstract Lcom/google/android/gms/car/m;
.super Landroid/app/Service;


# static fields
.field private static volatile f:Z


# instance fields
.field a:Lcom/google/android/gms/car/dr;

.field public b:Lcom/google/android/gms/car/em;

.field c:Lcom/google/android/gms/common/api/o;

.field d:I

.field e:Landroid/view/View;

.field private g:Lcom/google/android/gms/car/t;

.field private final h:Ljava/util/concurrent/Semaphore;

.field private final i:Lcom/google/android/gms/car/ae;

.field private final j:Ljava/lang/Runnable;

.field private final k:Landroid/os/IBinder$DeathRecipient;

.field private l:Lcom/google/android/gms/car/l;

.field private m:Lcom/google/android/gms/car/af;

.field private n:Landroid/content/Intent;

.field private o:Landroid/os/Bundle;

.field private p:Z

.field private volatile q:Z

.field private r:Lcom/google/android/gms/car/DrawingSpec;

.field private s:Ljava/lang/String;

.field private t:Lcom/google/android/gms/car/ek;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/car/m;->f:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->h:Ljava/util/concurrent/Semaphore;

    new-instance v0, Lcom/google/android/gms/car/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/n;-><init>(Lcom/google/android/gms/car/m;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->j:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/gms/car/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/o;-><init>(Lcom/google/android/gms/car/m;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->k:Landroid/os/IBinder$DeathRecipient;

    iput-boolean v1, p0, Lcom/google/android/gms/car/m;->q:Z

    new-instance v0, Lcom/google/android/gms/car/ae;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ae;-><init>(Lcom/google/android/gms/car/m;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->i:Lcom/google/android/gms/car/ae;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;Lcom/google/android/gms/car/dr;)Lcom/google/android/gms/car/dr;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    return-object p1
.end method

.method private static a(Ljava/lang/ClassLoader;Ljava/lang/String;)Lcom/google/android/gms/car/l;
    .locals 4

    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/l;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception while instantiating class "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;Lcom/google/android/gms/car/t;)Lcom/google/android/gms/car/t;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/m;->g:Lcom/google/android/gms/car/t;

    return-object p1
.end method

.method private a(I)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget v1, v0, Lcom/google/android/gms/car/l;->i:I

    const-string v0, "CAR.PROJECTION"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".moveActivityToState() from: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/car/l;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " to: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/car/l;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    if-ge v1, p1, :cond_9

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/car/m;->h()V

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget-object v1, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/l;->c(Landroid/os/Bundle;)V

    :pswitch_1
    if-le p1, v3, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget-object v1, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    iget-object v2, v0, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    if-eqz v2, :cond_3

    const-string v2, "android:viewHierarchyState"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    invoke-virtual {v0, v1}, Landroid/view/Window;->restoreHierarchyState(Landroid/os/Bundle;)V

    :cond_3
    :pswitch_2
    if-le p1, v3, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/car/m;->h()V

    iget-object v0, p0, Lcom/google/android/gms/car/m;->e:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/m;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v1, v0, Landroid/view/ViewGroup;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/gms/car/m;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/m;->e:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/gms/car/m;->e:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/em;->setContentView(Landroid/view/View;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v0}, Lcom/google/android/gms/car/l;->m()V

    :cond_6
    :pswitch_3
    if-le p1, v3, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->show()V

    :cond_7
    const/4 v0, 0x4

    if-le p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v0}, Lcom/google/android/gms/car/l;->n()V

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_9
    if-le v1, p1, :cond_0

    packed-switch v1, :pswitch_data_1

    goto/16 :goto_0

    :cond_a
    :goto_2
    :pswitch_4
    if-ge p1, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v0}, Lcom/google/android/gms/car/l;->q()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-direct {p0}, Lcom/google/android/gms/car/m;->g()V

    goto/16 :goto_0

    :pswitch_5
    const/4 v0, 0x5

    if-ge p1, v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v0}, Lcom/google/android/gms/car/l;->o()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/l;->b(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    iget-object v1, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/dr;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_b
    :goto_3
    :pswitch_6
    if-ge p1, v4, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v0}, Lcom/google/android/gms/car/l;->p()V

    goto :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/gms/car/m;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;ILcom/google/android/gms/car/DrawingSpec;Landroid/content/res/Configuration;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v1, p0, Lcom/google/android/gms/car/m;->d:I

    and-int/2addr v1, p1

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v1, p3, Landroid/content/res/Configuration;->uiMode:I

    iput v1, v0, Landroid/content/res/Configuration;->uiMode:I

    iget-object v1, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/l;->a(Landroid/content/res/Configuration;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget v1, v1, Lcom/google/android/gms/car/l;->i:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/m;->a(I)V

    iget-object v2, p0, Lcom/google/android/gms/car/m;->n:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v2

    if-nez p2, :cond_5

    const-string v3, "CAR.PROJECTION"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v0, 0x1

    :cond_3
    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Drawing spec is null, destroying virtual display for "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " old state is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/car/m;->g()V

    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->f()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_0

    :cond_5
    iget v2, p2, Lcom/google/android/gms/car/DrawingSpec;->b:I

    iget v3, p2, Lcom/google/android/gms/car/DrawingSpec;->c:I

    iget v4, p2, Lcom/google/android/gms/car/DrawingSpec;->d:I

    iget-object v5, p2, Lcom/google/android/gms/car/DrawingSpec;->e:Landroid/view/Surface;

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/google/android/gms/car/m;->a(IIILandroid/view/Surface;)Z

    move-result v2

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    iput-boolean v0, v2, Lcom/google/android/gms/car/em;->a:Z

    invoke-virtual {v2}, Lcom/google/android/gms/car/em;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->a()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/m;->a(Ljava/lang/ClassLoader;Ljava/lang/String;)Lcom/google/android/gms/car/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget-object v0, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_7
    invoke-direct {p0, v1}, Lcom/google/android/gms/car/m;->a(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;Landroid/content/Intent;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/l;->a(Landroid/content/Intent;)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;Landroid/view/KeyEvent;)V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {v0}, Lcom/google/android/gms/car/l;->f()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    and-int/lit16 v0, v0, 0x80

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/car/l;->a(ILandroid/view/KeyEvent;)Z

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/gms/car/em;->a:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/car/em;->a(Landroid/view/Window;ZZ)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/car/em;->a(Landroid/view/Window;Landroid/view/InputEvent;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    invoke-static {}, Lcom/google/android/gms/car/l;->s()Z

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    invoke-static {}, Lcom/google/android/gms/car/l;->r()Z

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;Landroid/view/MotionEvent;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v4}, Lcom/google/android/gms/car/em;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, v4, Lcom/google/android/gms/car/em;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    const/16 v3, 0x1002

    if-ne v0, v3, :cond_2

    invoke-virtual {v4}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, v1, v1}, Lcom/google/android/gms/car/em;->a(Landroid/view/Window;ZZ)V

    invoke-virtual {v4}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/car/em;->a(Landroid/view/Window;Landroid/view/InputEvent;)V

    :cond_0
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->h()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/em;->a(Landroid/view/Window;ZZ)V

    invoke-virtual {v4, p1}, Lcom/google/android/gms/car/em;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    const-string v0, "CAR.PROJECTION"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "injectTouchEvent handled="

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " event = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    if-nez v3, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    const/16 v3, 0x8

    if-ne v0, v3, :cond_0

    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    float-to-int v5, v0

    if-lez v5, :cond_6

    const/4 v0, 0x2

    :goto_3
    invoke-virtual {v4}, Lcom/google/android/gms/car/em;->getCurrentFocus()Landroid/view/View;

    move-result-object v6

    const-string v3, "CAR.PROJECTION"

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_7

    move v3, v1

    :goto_4
    if-eqz v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "movefocus current = "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v4, v0}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;I)V

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v6, :cond_9

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    :goto_5
    add-int/2addr v0, v5

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocusFromTouch()Z

    const-string v3, "CAR.PROJECTION"

    invoke-static {v3, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8

    :goto_6
    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "movefocus next = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "touchmode"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_5
    move v0, v2

    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto :goto_3

    :cond_7
    move v3, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_6

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto/16 :goto_1

    :cond_9
    move v0, v2

    goto :goto_5
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;Lcom/google/android/gms/car/DrawingSpec;Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object p2, p0, Lcom/google/android/gms/car/m;->n:Landroid/content/Intent;

    iput-object p1, p0, Lcom/google/android/gms/car/m;->r:Lcom/google/android/gms/car/DrawingSpec;

    iget-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/car/eo;->a()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    iget v0, p1, Lcom/google/android/gms/car/DrawingSpec;->b:I

    iget v1, p1, Lcom/google/android/gms/car/DrawingSpec;->c:I

    iget v2, p1, Lcom/google/android/gms/car/DrawingSpec;->d:I

    iget-object v3, p1, Lcom/google/android/gms/car/DrawingSpec;->e:Landroid/view/Surface;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/car/m;->a(IIILandroid/view/Surface;)Z

    :goto_0
    iput-object p3, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/m;->a(Ljava/lang/ClassLoader;Ljava/lang/String;)Lcom/google/android/gms/car/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget-object v1, p0, Lcom/google/android/gms/car/m;->n:Landroid/content/Intent;

    iput-object v1, v0, Lcom/google/android/gms/car/l;->l:Landroid/content/Intent;

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/m;->a(I)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    iget-object v1, p0, Lcom/google/android/gms/car/m;->r:Lcom/google/android/gms/car/DrawingSpec;

    iget-object v1, v1, Lcom/google/android/gms/car/DrawingSpec;->e:Landroid/view/Surface;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/af;->a(Landroid/view/Surface;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_1
.end method

.method private a(IIILandroid/view/Surface;)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v0, 0x0

    const-string v1, "CAR.PROJECTION"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v7

    :goto_0
    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".createVirtualDisplay()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "display"

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/m;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iget-object v2, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    invoke-virtual {v2}, Lcom/google/android/gms/car/af;->a()Landroid/view/Surface;

    move-result-object v2

    if-ne v2, p4, :cond_2

    :goto_1
    return v0

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/m;->g()V

    new-instance v0, Lcom/google/android/gms/car/af;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->a()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move v3, p1

    move v4, p2

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/af;-><init>(Landroid/hardware/display/DisplayManager;Ljava/lang/String;IIILandroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    move v0, v7

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/car/m;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/car/m;->q:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/car/m;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->j:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(Z)Z
    .locals 0

    sput-boolean p0, Lcom/google/android/gms/car/m;->f:Z

    return p0
.end method

.method static synthetic c(Lcom/google/android/gms/car/m;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/m;)Ljava/util/concurrent/Semaphore;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->h:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/m;)Lcom/google/android/gms/car/dr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/m;)Lcom/google/android/gms/car/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    return-object v0
.end method

.method static synthetic f()Z
    .locals 1

    sget-boolean v0, Lcom/google/android/gms/car/m;->f:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/m;)Lcom/google/android/gms/common/api/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    return-object v0
.end method

.method private g()V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x0

    const-string v0, "CAR.PROJECTION"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".destroyPresentationAndVirtualDisplay()"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    iput-boolean v1, v0, Lcom/google/android/gms/car/em;->a:Z

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->dismiss()V

    iput-object v3, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    invoke-virtual {v0}, Lcom/google/android/gms/car/af;->b()V

    iput-object v3, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    :cond_2
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private h()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "attachPresentation virtual display is null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getDisplay()Landroid/view/Display;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    iget-object v2, v2, Lcom/google/android/gms/car/af;->a:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v2}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v2

    if-eq v0, v2, :cond_4

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    iput-boolean v1, v0, Lcom/google/android/gms/car/em;->a:Z

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->dismiss()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    :cond_2
    new-instance v0, Lcom/google/android/gms/car/em;

    iget-object v2, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    iget-object v2, v2, Lcom/google/android/gms/car/af;->a:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v2}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/car/em;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/car/aw;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/aw;-><init>(Lcom/google/android/gms/car/m;)V

    invoke-virtual {v0, v2}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    iget-object v2, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v5

    iput-object v3, v2, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    const-string v0, "CAR.PROJECTION"

    const/4 v6, 0x3

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "Context DPI: "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->densityDpi:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_3
    iput-object p0, v2, Lcom/google/android/gms/car/l;->k:Lcom/google/android/gms/car/m;

    iput-object v4, v2, Lcom/google/android/gms/car/l;->m:Landroid/view/LayoutInflater;

    iput-object v5, v2, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    iget-object v0, v2, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    iput v1, v2, Lcom/google/android/gms/car/l;->i:I

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/android/gms/car/m;)Z
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/car/m;->i()Z

    move-result v0

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/car/m;)Landroid/os/IBinder$DeathRecipient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->k:Landroid/os/IBinder$DeathRecipient;

    return-object v0
.end method

.method private i()Z
    .locals 4

    iget-boolean v0, p0, Lcom/google/android/gms/car/m;->q:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/car/m;->q:Z

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->h:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x7d0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/m;->q:Z

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic j(Lcom/google/android/gms/car/m;)V
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    invoke-static {v2, v5}, Lcom/google/android/gms/common/g;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_2

    const-string v0, "android.permission.CAPTURE_VIDEO_OUTPUT"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/m;->checkCallingPermission(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "projection client manager does not have permssion:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " pid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " uid:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic k(Lcom/google/android/gms/car/m;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/m;->a(I)V

    invoke-static {}, Lcom/google/android/gms/car/eo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/af;->a(Landroid/view/Surface;)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_0
.end method

.method static synthetic l(Lcom/google/android/gms/car/m;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/m;->a(I)V

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_0
.end method

.method static synthetic m(Lcom/google/android/gms/car/m;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget v0, v0, Lcom/google/android/gms/car/l;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iget v0, v0, Lcom/google/android/gms/car/l;->i:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/m;->a(I)V

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->g()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_0
.end method

.method static synthetic n(Lcom/google/android/gms/car/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/m;->g()V

    return-void
.end method

.method static synthetic o(Lcom/google/android/gms/car/m;)Lcom/google/android/gms/car/ek;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->t:Lcom/google/android/gms/car/ek;

    return-object v0
.end method


# virtual methods
.method public abstract a()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/gms/car/l;",
            ">;"
        }
    .end annotation
.end method

.method public final a(Z)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/dr;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->e()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    const-string v0, "CAR.PROJECTION"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".finish()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/gms/car/r;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/r;-><init>(Lcom/google/android/gms/car/m;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->postAtFrontOfQueue(Ljava/lang/Runnable;)Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/common/api/o;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/car/a/c;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/m;->t:Lcom/google/android/gms/car/ek;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/ek;

    new-instance v1, Lcom/google/android/gms/car/s;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/s;-><init>(Lcom/google/android/gms/car/m;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ek;-><init>(Lcom/google/android/gms/car/el;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->t:Lcom/google/android/gms/car/ek;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->t:Lcom/google/android/gms/car/ek;

    return-object v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/gms/car/l;->v()V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "activity state:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v0, Lcom/google/android/gms/car/l;->i:I

    invoke-static {v2}, Lcom/google/android/gms/car/l;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarProjectionActivity not initialized with attach()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "configuration:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    if-eqz v0, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "surface:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/af;->a()Landroid/view/Surface;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "display:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/car/af;->a:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    if-eqz v0, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "isShowing:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->isShowing()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    if-eqz v0, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " layout param:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    :cond_3
    return-void
.end method

.method e()V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/car/m;->p:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/m;->p:Z

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/m;->a(I)V

    invoke-direct {p0}, Lcom/google/android/gms/car/m;->g()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->stopSelf()V

    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    invoke-interface {v0}, Lcom/google/android/gms/car/dr;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/m;->k:Landroid/os/IBinder$DeathRecipient;

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    const-string v0, "CAR.PROJECTION"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onBind()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v0, Lcom/google/android/gms/car/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/t;-><init>(Lcom/google/android/gms/car/m;)V

    iput-object v0, p0, Lcom/google/android/gms/car/m;->g:Lcom/google/android/gms/car/t;

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->g:Lcom/google/android/gms/car/t;

    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->a()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    const-string v0, "CAR.PROJECTION"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onCreate()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/m;->i:Lcom/google/android/gms/car/ae;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->i:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    :cond_1
    new-instance v0, Lcom/google/android/gms/car/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/p;-><init>(Lcom/google/android/gms/car/m;)V

    new-instance v1, Lcom/google/android/gms/car/q;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/q;-><init>(Lcom/google/android/gms/car/m;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/car/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/car/e;)Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "CAR.PROJECTION"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".onDestroy()"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/m;->t:Lcom/google/android/gms/car/ek;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->t:Lcom/google/android/gms/car/ek;

    iput-object v3, v0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/el;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    if-eqz v0, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/m;->a(I)V

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/m;->g()V

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    :cond_3
    iput-object v3, p0, Lcom/google/android/gms/car/m;->l:Lcom/google/android/gms/car/l;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->a:Lcom/google/android/gms/car/dr;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->m:Lcom/google/android/gms/car/af;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->n:Landroid/content/Intent;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->o:Landroid/os/Bundle;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->e:Landroid/view/View;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->r:Lcom/google/android/gms/car/DrawingSpec;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    iput-object v3, p0, Lcom/google/android/gms/car/m;->t:Lcom/google/android/gms/car/ek;

    return-void

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 3

    const/4 v1, 0x0

    const-string v0, "CAR.PROJECTION"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/m;->s:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".onUnbind()"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/m;->g()V

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/m;->g:Lcom/google/android/gms/car/t;

    invoke-virtual {p0}, Lcom/google/android/gms/car/m;->stopSelf()V

    return v1

    :cond_2
    move v0, v1

    goto :goto_0
.end method
