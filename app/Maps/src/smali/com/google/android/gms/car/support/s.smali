.class Lcom/google/android/gms/car/support/s;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/View$OnGenericMotionListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/CarDrawerLayout;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/CarDrawerLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/s;->a:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotion(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/s;->a:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/s;->a:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/s;->a:Lcom/google/android/gms/car/support/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->onGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
