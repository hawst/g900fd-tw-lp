.class Lcom/google/android/gms/car/support/ar;
.super Landroid/support/v7/widget/bu;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/PagedListView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/PagedListView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    invoke-direct {p0}, Landroid/support/v7/widget/bu;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/car/support/PagedListView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v0, v0, Lcom/google/android/gms/car/support/PagedListView;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v1, v1, Lcom/google/android/gms/car/support/PagedListView;->j:Ljava/lang/Runnable;

    const-wide/16 v2, 0x190

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    if-ne p1, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iput-boolean v4, v0, Lcom/google/android/gms/car/support/PagedListView;->h:Z

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget v0, v0, Lcom/google/android/gms/car/support/PagedListView;->i:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v1, v1, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget v2, v2, Lcom/google/android/gms/car/support/PagedListView;->i:I

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget v2, v2, Lcom/google/android/gms/car/support/PagedListView;->g:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/support/PagedListView;->a(II)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iput-boolean v5, v0, Lcom/google/android/gms/car/support/PagedListView;->h:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iput v5, v0, Lcom/google/android/gms/car/support/PagedListView;->i:I

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->a(Z)V

    if-gtz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/gms/car/support/PagedListView;->g:I

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget-boolean v0, v0, Lcom/google/android/gms/car/support/PagedListView;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iget v1, v0, Lcom/google/android/gms/car/support/PagedListView;->i:I

    add-int/2addr v1, p3

    iput v1, v0, Lcom/google/android/gms/car/support/PagedListView;->i:I

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/ar;->a:Lcom/google/android/gms/car/support/PagedListView;

    iput v1, v0, Lcom/google/android/gms/car/support/PagedListView;->g:I

    goto :goto_0
.end method
