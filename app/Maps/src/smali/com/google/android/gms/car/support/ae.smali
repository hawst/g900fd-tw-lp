.class public Lcom/google/android/gms/car/support/ae;
.super Lcom/google/android/gms/car/l;


# instance fields
.field final p:Landroid/os/Handler;

.field final q:Lcom/google/android/gms/car/support/g;

.field final r:Lcom/google/android/gms/car/support/bk;

.field s:Z

.field t:Z

.field u:Z

.field v:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/car/l;-><init>()V

    new-instance v0, Lcom/google/android/gms/car/support/af;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/af;-><init>(Lcom/google/android/gms/car/support/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/car/support/g;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    new-instance v0, Lcom/google/android/gms/car/support/ag;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/ag;-><init>(Lcom/google/android/gms/car/support/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/ae;->r:Lcom/google/android/gms/car/support/bk;

    return-void
.end method

.method public static w()V
    .locals 0

    return-void
.end method

.method public static x()V
    .locals 0

    return-void
.end method

.method static y()V
    .locals 0

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/gms/car/l;->a()V

    iput-boolean v2, p0, Lcom/google/android/gms/car/support/ae;->u:Z

    iput-boolean v2, p0, Lcom/google/android/gms/car/support/ae;->v:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/ae;->s:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/ae;->s:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->noteStateNotSaved()V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/gms/car/l;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->noteStateNotSaved()V

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/gms/car/l;->a(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/g;->a(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iget-object v1, p0, Lcom/google/android/gms/car/support/ae;->r:Lcom/google/android/gms/car/support/bk;

    invoke-virtual {v0, p0, v1, v3}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ae;Lcom/google/android/gms/car/support/bk;Lcom/google/android/gms/car/support/ab;)V

    iget-object v0, p0, Lcom/google/android/gms/car/l;->m:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/l;->m:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p0}, Landroid/view/LayoutInflater;->setFactory(Landroid/view/LayoutInflater$Factory;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/car/l;->a(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    if-eqz v3, :cond_1

    :cond_1
    invoke-virtual {v1, v0, v3}, Lcom/google/android/gms/car/support/g;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public final a(ILandroid/view/KeyEvent;)Z
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ae;->f()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/car/l;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/gms/car/l;->b()V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/ae;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/gms/car/l;->b(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->b()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "android:support:fragments"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method final b(Z)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/ae;->v:Z

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/ae;->v:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/gms/car/l;->c()V

    iput-boolean v2, p0, Lcom/google/android/gms/car/support/ae;->t:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public d()V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/gms/car/l;->d()V

    iput-boolean v1, p0, Lcom/google/android/gms/car/support/ae;->u:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-boolean v1, v0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    return-void
.end method

.method public e()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/gms/car/l;->e()V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/ae;->v:Z

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/google/android/gms/car/support/ae;->v:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-boolean v3, v0, Lcom/google/android/gms/car/support/g;->p:Z

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    invoke-virtual {v0, v2, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    iput-object v4, v0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iput-object v4, v0, Lcom/google/android/gms/car/support/g;->l:Lcom/google/android/gms/car/support/bk;

    iput-object v4, v0, Lcom/google/android/gms/car/support/g;->m:Lcom/google/android/gms/car/support/ab;

    return-void
.end method

.method public f()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iget-boolean v1, v0, Lcom/google/android/gms/car/support/g;->o:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can not perform this action after onSaveInstanceState"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    iget-object v1, v0, Lcom/google/android/gms/car/support/g;->k:Lcom/google/android/gms/car/support/ae;

    iget-object v1, v1, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/support/g;->a(Ljava/lang/String;II)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarProjectionActivity not initialized with attach()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/l;->k:Lcom/google/android/gms/car/m;

    invoke-virtual {v0}, Lcom/google/android/gms/car/m;->b()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/l;->o:Z

    :cond_2
    return-void
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 11

    const/4 v10, 0x2

    const/4 v5, -0x1

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-string v0, "fragment"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/car/l;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "class"

    invoke-interface {p3, v1, v0}, Landroid/util/AttributeSet;->getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/google/android/gms/car/support/ah;->a:[I

    invoke-virtual {p2, p3, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v4

    if-nez v0, :cond_12

    invoke-virtual {v4, v3}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    :goto_1
    invoke-virtual {v4, v2, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v7

    invoke-virtual {v4, v10}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ae;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/car/support/ab;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/car/l;->onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    if-eq v7, v5, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/car/support/g;->a(I)Lcom/google/android/gms/car/support/ab;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_11

    if-eqz v8, :cond_11

    iget-object v5, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iget-object v0, v5, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    if-eqz v8, :cond_7

    iget-object v0, v5, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_3
    if-ltz v4, :cond_7

    iget-object v0, v5, Lcom/google/android/gms/car/support/g;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_6

    iget-object v9, v0, Lcom/google/android/gms/car/support/ab;->y:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    :cond_2
    :goto_4
    move-object v1, v0

    :goto_5
    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/support/g;->a(I)Lcom/google/android/gms/car/support/ab;

    move-result-object v1

    :cond_3
    const-string v0, "CAR.PROJECTION"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v2

    :goto_6
    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "onCreateView: id=0x"

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " fname="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " existing="

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_4
    if-nez v1, :cond_c

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/ae;->h()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6}, Lcom/google/android/gms/car/support/ab;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/car/support/ab;

    move-result-object v1

    iput-boolean v2, v1, Lcom/google/android/gms/car/support/ab;->o:Z

    if-eqz v7, :cond_b

    move v0, v7

    :goto_7
    iput v0, v1, Lcom/google/android/gms/car/support/ab;->w:I

    iput v3, v1, Lcom/google/android/gms/car/support/ab;->x:I

    iput-object v8, v1, Lcom/google/android/gms/car/support/ab;->y:Ljava/lang/String;

    iput-boolean v2, v1, Lcom/google/android/gms/car/support/ab;->p:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-object v0, v1, Lcom/google/android/gms/car/support/ab;->s:Lcom/google/android/gms/car/support/g;

    iget-object v0, v1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iput-boolean v2, v1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;Z)V

    :goto_8
    iget-object v0, v1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not create a view."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move-object v0, v1

    goto/16 :goto_2

    :cond_6
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto/16 :goto_3

    :cond_7
    iget-object v0, v5, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_9

    if-eqz v8, :cond_9

    iget-object v0, v5, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v4, v0

    :goto_9
    if-ltz v4, :cond_9

    iget-object v0, v5, Lcom/google/android/gms/car/support/g;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/ab;

    if-eqz v0, :cond_8

    iget-object v9, v0, Lcom/google/android/gms/car/support/ab;->y:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    :cond_8
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_9

    :cond_9
    move-object v0, v1

    goto/16 :goto_4

    :cond_a
    move v0, v3

    goto/16 :goto_6

    :cond_b
    move v0, v3

    goto :goto_7

    :cond_c
    iget-boolean v0, v1, Lcom/google/android/gms/car/support/ab;->p:Z

    if-eqz v0, :cond_d

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Landroid/util/AttributeSet;->getPositionDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Duplicate id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", or parent id 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with another fragment for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    iput-boolean v2, v1, Lcom/google/android/gms/car/support/ab;->p:Z

    iget-object v0, v1, Lcom/google/android/gms/car/support/ab;->d:Landroid/os/Bundle;

    iput-boolean v2, v1, Lcom/google/android/gms/car/support/ab;->E:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iget v2, v0, Lcom/google/android/gms/car/support/g;->j:I

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/support/g;->a(Lcom/google/android/gms/car/support/ab;IIIZ)V

    goto/16 :goto_8

    :cond_e
    if-eqz v7, :cond_f

    iget-object v0, v1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    :cond_f
    iget-object v0, v1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_10

    iget-object v0, v1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_10
    iget-object v0, v1, Lcom/google/android/gms/car/support/ab;->H:Landroid/view/View;

    goto/16 :goto_0

    :cond_11
    move-object v1, v0

    goto/16 :goto_5

    :cond_12
    move-object v6, v0

    goto/16 :goto_1
.end method

.method protected final t()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/google/android/gms/car/l;->t()V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->p:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    iput-boolean v2, v0, Lcom/google/android/gms/car/support/g;->o:Z

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/google/android/gms/car/support/g;->a(IIIZ)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/ae;->q:Lcom/google/android/gms/car/support/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/g;->a()Z

    return-void
.end method
