.class Lcom/google/android/gms/car/support/av;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/gms/car/support/PagedListView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/PagedListView;I)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/av;->b:Lcom/google/android/gms/car/support/PagedListView;

    iput p2, p0, Lcom/google/android/gms/car/support/av;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/google/android/gms/car/support/av;->b:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v0, v0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/an;->f()I

    move-result v2

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/av;->b:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v0, v0, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/an;->e(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    iget v4, p0, Lcom/google/android/gms/car/support/av;->a:I

    neg-int v4, v4

    if-gt v3, v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/car/support/PagedListView;->a:Ljava/lang/String;

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/support/av;->b:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v1, v1, Lcom/google/android/gms/car/support/PagedListView;->c:Lcom/google/android/gms/car/support/an;

    invoke-static {v0}, Lcom/google/android/gms/car/support/an;->b(Landroid/view/View;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/av;->b:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v1, v1, Lcom/google/android/gms/car/support/PagedListView;->b:Lcom/google/android/gms/car/support/PagedRecyclerView;

    iget-object v2, v1, Landroid/support/v7/widget/RecyclerView;->h:Landroid/support/v7/widget/bs;

    iget-object v3, v1, Landroid/support/v7/widget/RecyclerView;->A:Landroid/support/v7/widget/cc;

    invoke-virtual {v2, v1, v0}, Landroid/support/v7/widget/bs;->a(Landroid/support/v7/widget/RecyclerView;I)V

    goto :goto_1
.end method
