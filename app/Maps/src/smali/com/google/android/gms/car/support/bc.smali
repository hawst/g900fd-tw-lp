.class public Lcom/google/android/gms/car/support/bc;
.super Ljava/lang/Object;


# instance fields
.field final a:Landroid/animation/TimeInterpolator;

.field final b:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/car/support/bc;->a:Landroid/animation/TimeInterpolator;

    iput-object p2, p0, Lcom/google/android/gms/car/support/bc;->b:Landroid/animation/TimeInterpolator;

    return-void
.end method


# virtual methods
.method public final a(F)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/bc;->a:Landroid/animation/TimeInterpolator;

    invoke-interface {v0, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method

.method public final b(F)F
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/bc;->b:Landroid/animation/TimeInterpolator;

    invoke-interface {v0, p1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    return v0
.end method
