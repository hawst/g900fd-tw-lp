.class public Lcom/google/android/gms/car/support/CarDrawerLayout;
.super Landroid/view/ViewGroup;


# static fields
.field static final a:[I


# instance fields
.field final b:Landroid/support/v4/widget/bk;

.field c:I

.field d:Z

.field private e:I

.field private final f:Landroid/graphics/Paint;

.field private final g:Landroid/graphics/Paint;

.field private final h:Lcom/google/android/gms/car/support/u;

.field private final i:Ljava/lang/Runnable;

.field private final j:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/car/support/v;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Lcom/google/android/gms/car/support/bc;

.field private final l:Lcom/google/android/gms/car/support/bc;

.field private final m:Lcom/google/android/gms/car/support/bc;

.field private final n:Lcom/google/android/gms/car/support/bc;

.field private final o:Landroid/os/Handler;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:Z

.field private w:Landroid/graphics/drawable/Drawable;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/car/support/CarDrawerLayout;->a:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    const v9, 0x3e8f5c29    # 0.28f

    const/4 v8, 0x0

    const/4 v3, 0x0

    const/high16 v7, 0x3e800000    # 0.25f

    const v6, 0x3e0f5c29    # 0.14f

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const v0, -0xd9d9da

    iput v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->e:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->f:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->g:Landroid/graphics/Paint;

    new-instance v0, Lcom/google/android/gms/car/support/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/r;-><init>(Lcom/google/android/gms/car/support/CarDrawerLayout;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->i:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->o:Landroid/os/Handler;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->q:Z

    new-instance v0, Lcom/google/android/gms/car/support/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/s;-><init>(Lcom/google/android/gms/car/support/CarDrawerLayout;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->j:Ljava/util/Set;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x43c80000    # 400.0f

    mul-float/2addr v0, v1

    new-instance v1, Lcom/google/android/gms/car/support/u;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/u;-><init>(Lcom/google/android/gms/car/support/CarDrawerLayout;)V

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->h:Lcom/google/android/gms/car/support/u;

    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->h:Lcom/google/android/gms/car/support/u;

    invoke-static {p0, v1, v2}, Landroid/support/v4/widget/bk;->a(Landroid/view/ViewGroup;FLandroid/support/v4/widget/bn;)Landroid/support/v4/widget/bk;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iput v0, v1, Landroid/support/v4/widget/bk;->h:F

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->h:Lcom/google/android/gms/car/support/u;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iput-object v1, v0, Lcom/google/android/gms/car/support/u;->a:Landroid/support/v4/widget/bk;

    invoke-static {p0, v3}, Landroid/support/v4/view/bl;->a(Landroid/view/ViewGroup;Z)V

    const-string v0, "drawer_shadow"

    invoke-static {p1, v0}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->invalidate()V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x1010434

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0021

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->e:I

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->invalidate()V

    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->a:[F

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-direct {v1, v2, v7, v7, v3}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->a:[F

    const v4, 0x3edc28f6    # 0.43f

    const v5, 0x3edc28f6    # 0.43f

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->k:Lcom/google/android/gms/car/support/bc;

    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->c:[F

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-direct {v1, v2, v8, v7, v3}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->b:[F

    const v4, 0x3e947ae1    # 0.29f

    const v5, 0x3f11eb85    # 0.57f

    invoke-direct {v2, v3, v4, v6, v5}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->l:Lcom/google/android/gms/car/support/bc;

    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->a:[F

    const/high16 v3, 0x3f200000    # 0.625f

    const/high16 v4, 0x3e000000    # 0.125f

    invoke-direct {v1, v2, v3, v7, v4}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->c:[F

    const v4, 0x3f147ae1    # 0.58f

    invoke-direct {v2, v3, v4, v6, v9}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->m:Lcom/google/android/gms/car/support/bc;

    new-instance v0, Lcom/google/android/gms/car/support/bc;

    new-instance v1, Lcom/google/android/gms/car/support/az;

    sget-object v2, Lcom/google/android/gms/car/support/az;->b:[F

    const/high16 v3, 0x3f200000    # 0.625f

    const/high16 v4, 0x3ec00000    # 0.375f

    invoke-direct {v1, v2, v3, v4, v8}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    new-instance v2, Lcom/google/android/gms/car/support/az;

    sget-object v3, Lcom/google/android/gms/car/support/az;->c:[F

    const v4, 0x3f147ae1    # 0.58f

    invoke-direct {v2, v3, v4, v6, v9}, Lcom/google/android/gms/car/support/az;-><init>([FFFF)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/support/bc;-><init>(Landroid/animation/TimeInterpolator;Landroid/animation/TimeInterpolator;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->n:Lcom/google/android/gms/car/support/bc;

    return-void
.end method

.method private a(II)V
    .locals 2

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v0

    invoke-static {p2, v0}, Landroid/support/v4/view/p;->a(II)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    iput p1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->t:I

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v0}, Landroid/support/v4/widget/bk;->c()V

    :cond_1
    packed-switch p1, :pswitch_data_0

    :goto_1
    return-void

    :cond_2
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    iput p1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->u:I

    goto :goto_0

    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->g()V

    goto :goto_1

    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->h()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private b(Landroid/view/View;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v2

    if-ne p1, v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a drawer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private g()V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-boolean v5, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->r:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;II)Z

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const/high16 v2, 0x60000

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->invalidate()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;II)Z

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    goto :goto_0
.end method

.method private h()V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    cmpl-float v0, v0, v4

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_1

    move v1, v2

    :goto_1
    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "View "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a sliding drawer"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    iput-boolean v2, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    iget-boolean v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->r:Z

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    invoke-virtual {v2, v1, v3, v4}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;II)Z

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const/high16 v2, 0x60000

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    const/high16 v0, 0x40000

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setDescendantFocusability(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_3

    const/16 v0, 0x82

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->invalidate()V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iput v4, v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iput-boolean v3, v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    goto :goto_2
.end method

.method private i()Landroid/view/View;
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildCount()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_2
    return-object v0

    :cond_0
    move v1, v2

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method a()F
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    return v0
.end method

.method public final a(Landroid/view/View;)I
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->a:I

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/p;->a(II)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->t:I

    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->u:I

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Landroid/view/View;I)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->a:I

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/p;->a(II)I

    move-result v0

    and-int/2addr v0, p2

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    goto :goto_0
.end method

.method b()Landroid/view/View;
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildCount()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->a:I

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v4

    invoke-static {v0, v4}, Landroid/support/v4/view/p;->a(II)I

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    move-object v0, v1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No drawer view found."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method c()Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_1
    if-ltz v2, :cond_3

    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v1

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_2
    if-nez v1, :cond_2

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No content view found."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public computeScroll()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/bk;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Landroid/support/v4/view/at;->d(Landroid/view/View;)V

    :cond_0
    return-void
.end method

.method d()V
    .locals 5

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->m:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->b:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/View;->setAlpha(F)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->m:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->a:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-boolean v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x16

    if-eq v3, v0, :cond_0

    const/4 v0, 0x2

    if-ne v3, v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->h()V

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    :cond_2
    const/16 v0, 0x15

    if-eq v3, v0, :cond_3

    if-ne v3, v1, :cond_6

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b(Landroid/view/View;)Z

    move-result v0

    :goto_1
    if-nez v0, :cond_6

    if-ne v2, v1, :cond_4

    invoke-direct {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->g()V

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 11

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v0

    if-ne p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    iget v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->e:I

    const/high16 v4, -0x1000000

    and-int/2addr v1, v4

    ushr-int/lit8 v7, v1, 0x18

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v8

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildCount()I

    move-result v6

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_7

    invoke-virtual {p0, v4}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    if-eq v9, p2, :cond_6

    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {v9}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    const/4 v10, -0x1

    if-ne v1, v10, :cond_2

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v1

    if-ne v9, v1, :cond_4

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_6

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v1

    if-lt v1, v5, :cond_6

    const/4 v1, 0x3

    invoke-virtual {p0, v9, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v1

    if-le v1, v2, :cond_d

    :goto_4
    move v2, v1

    move v1, v3

    :cond_0
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3

    :cond_5
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v1

    if-lt v1, v3, :cond_0

    :cond_6
    move v1, v3

    goto :goto_5

    :cond_7
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getHeight()I

    move-result v4

    invoke-virtual {p1, v2, v1, v3, v4}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    :cond_8
    move v6, v2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v9

    invoke-virtual {p1, v8}, Landroid/graphics/Canvas;->restoreToCount(I)V

    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    if-eqz v0, :cond_b

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->n:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->b:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    :goto_6
    int-to-float v1, v7

    mul-float/2addr v0, v1

    const v1, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    if-lez v0, :cond_9

    shl-int/lit8 v0, v0, 0x18

    iget v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->e:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v6

    const/4 v2, 0x0

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v6, -0x1

    int-to-float v1, v0

    const/4 v2, 0x0

    int-to-float v3, v6

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v1

    const/4 v2, 0x0

    int-to-float v3, v1

    iget-object v4, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget v4, v4, Landroid/support/v4/widget/bk;->i:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    add-int/2addr v0, v1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v3, v1, v4, v0, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_a
    :goto_7
    return v9

    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->n:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->a:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    goto/16 :goto_6

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getWidth()I

    move-result v2

    sub-int/2addr v2, v1

    const/4 v3, 0x0

    int-to-float v2, v2

    iget-object v4, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget v4, v4, Landroid/support/v4/widget/bk;->i:I

    int-to-float v4, v4

    div-float/2addr v2, v4

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getWidth()I

    move-result v4

    sub-int/2addr v4, v1

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getWidth()I

    move-result v6

    sub-int v1, v6, v1

    add-int/2addr v0, v1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v1

    invoke-virtual {v3, v4, v5, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getClipBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto/16 :goto_7

    :cond_d
    move v1, v2

    goto/16 :goto_4
.end method

.method e()V
    .locals 5

    const/high16 v2, 0x3f800000    # 1.0f

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->l:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->b:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    sub-float v0, v2, v0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/View;->setAlpha(F)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->l:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->a:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    sub-float v0, v2, v0

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method f()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->s:Z

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->d:Z

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->k:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->b:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/v;

    iget v2, v0, Lcom/google/android/gms/car/support/v;->a:I

    iget v0, v0, Lcom/google/android/gms/car/support/v;->b:I

    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    invoke-static {v2}, Landroid/graphics/Color;->red(I)I

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    invoke-static {v2}, Landroid/graphics/Color;->green(I)I

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    invoke-static {v2}, Landroid/graphics/Color;->blue(I)I

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->k:Lcom/google/android/gms/car/support/bc;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    iget-object v1, v1, Lcom/google/android/gms/car/support/bc;->a:Landroid/animation/TimeInterpolator;

    invoke-interface {v1, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    goto :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    instance-of v0, p1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    check-cast p1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;-><init>(Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;)V

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->q:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->r:Z

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->requestLayout()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->r:Z

    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->s:Z

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a003d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->j:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/support/v4/view/ac;->a(Landroid/view/MotionEvent;)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/bk;->a(Landroid/view/MotionEvent;)Z

    move-result v3

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    move v0, v2

    :goto_1
    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    const/4 v6, 0x0

    cmpl-float v0, v0, v6

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    float-to-int v4, v4

    float-to-int v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/widget/bk;->b(II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v4

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v2, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->v:Z

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :pswitch_2
    iput-boolean v2, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->v:Z

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->i()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    move v1, v0

    :goto_0
    if-eqz v1, :cond_1

    invoke-static {p2}, Landroid/support/v4/view/t;->b(Landroid/view/KeyEvent;)V

    :goto_1
    return v0

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->i()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;)I

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->h()V

    :cond_0
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->p:Z

    sub-int v3, p4, p2

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v2, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v2, v6

    float-to-int v2, v2

    const/4 v6, 0x5

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v6

    if-eqz v6, :cond_0

    neg-int v2, v2

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->getMarginStart()I

    move-result v6

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getWidth()I

    move-result v7

    add-int/2addr v6, v7

    add-int/2addr v6, v2

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int v7, v6, v7

    iget v8, v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    iget v1, v1, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v1, v9

    invoke-virtual {v4, v7, v8, v6, v1}, Landroid/view/View;->layout(IIII)V

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v4

    sub-int v2, v4, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v4, 0x3

    invoke-virtual {p0, v5, v4}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->getMarginStart()I

    move-result v4

    sub-int/2addr v4, v2

    iget v6, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v7

    sub-int/2addr v3, v7

    sub-int v2, v3, v2

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v1

    invoke-virtual {v5, v4, v6, v2, v0}, Landroid/view/View;->layout(IIII)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->d()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->e()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->f()V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->q:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->o:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->q:Z

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->p:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->r:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-boolean v1, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    const/16 v2, 0x82

    invoke-virtual {v0, v2}, Landroid/view/View;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_6

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_6

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    :cond_2
    :goto_3
    return-void

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v4

    add-int/2addr v4, v2

    iget v6, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->getMarginStart()I

    move-result v7

    sub-int/2addr v3, v7

    add-int/2addr v2, v3

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    add-int/2addr v0, v1

    invoke-virtual {v5, v4, v6, v2, v0}, Landroid/view/View;->layout(IIII)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    const/16 v2, 0x82

    invoke-virtual {v0, v2}, Landroid/view/View;->getFocusables(I)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_2

    :cond_6
    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->x:Landroid/view/View;

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setFocusable(Z)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->y:Landroid/view/View;

    goto :goto_3
.end method

.method protected onMeasure(II)V
    .locals 8

    const/16 v1, 0x12c

    const/high16 v7, -0x80000000

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    if-ne v3, v6, :cond_0

    if-eq v4, v6, :cond_3

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->isInEditMode()Z

    move-result v5

    if-eqz v5, :cond_2

    if-eq v3, v7, :cond_1

    if-nez v3, :cond_1

    move v2, v1

    :cond_1
    if-eq v4, v7, :cond_3

    if-nez v4, :cond_3

    :goto_0
    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->setMeasuredDimension(II)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->c()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v4, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->leftMargin:I

    sub-int/2addr v2, v4

    iget v4, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v2, v4

    invoke-static {v2, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v4, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    sub-int/2addr v1, v4

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->bottomMargin:I

    sub-int v0, v1, v0

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-virtual {v3, v2, v0}, Landroid/view/View;->measure(II)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v2, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->leftMargin:I

    iget v3, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->rightMargin:I

    add-int/2addr v2, v3

    iget v3, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->width:I

    invoke-static {p1, v2, v3}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildMeasureSpec(III)I

    move-result v2

    iget v3, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->topMargin:I

    iget v4, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->bottomMargin:I

    add-int/2addr v3, v4

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->height:I

    invoke-static {p2, v3, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildMeasureSpec(III)I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/view/View;->measure(II)V

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "DrawerLayout must be measured with MeasureSpec.EXACTLY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v1, v0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    check-cast p1, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;

    invoke-virtual {p1}, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    iget v0, p1, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;->a:I

    iget v0, p1, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;->b:I

    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(II)V

    iget v0, p1, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;->c:I

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(II)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;

    invoke-direct {v3, v0}, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;-><init>(Landroid/os/Parcelable;)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildCount()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/support/CarDrawerLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    if-ne v5, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget-boolean v5, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v5, :cond_2

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->a:I

    iput v0, v3, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;->a:I

    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->t:I

    iput v0, v3, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;->b:I

    iget v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->u:I

    iput v0, v3, Lcom/google/android/gms/car/support/CarDrawerLayout$SavedState;->c:I

    return-object v3

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/bk;->b(Landroid/view/MotionEvent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->a:I

    invoke-static {p0}, Landroid/support/v4/view/at;->h(Landroid/view/View;)I

    move-result v1

    invoke-static {v0, v1}, Landroid/support/v4/view/p;->a(II)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/gms/car/support/CarDrawerLayout$LayoutParams;->b:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_3

    move v0, v2

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v4, v1}, Landroid/support/v4/widget/bk;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget-object v1, v1, Landroid/support/v4/widget/bk;->k:Landroid/view/View;

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v3, v2

    :cond_1
    return v3

    :cond_2
    const/4 v0, 0x2

    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public requestDisallowInterceptTouchEvent(Z)V
    .locals 2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarDrawerLayout;->b()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/support/CarDrawerLayout;->a(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    :cond_1
    return-void
.end method

.method public requestLayout()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/CarDrawerLayout;->p:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    :cond_0
    return-void
.end method
