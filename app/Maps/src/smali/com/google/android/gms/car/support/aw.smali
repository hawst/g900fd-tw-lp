.class Lcom/google/android/gms/car/support/aw;
.super Landroid/support/v7/widget/bq;


# instance fields
.field final a:Landroid/graphics/Paint;

.field final synthetic b:Lcom/google/android/gms/car/support/PagedListView;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/support/PagedListView;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/gms/car/support/aw;->b:Lcom/google/android/gms/car/support/PagedListView;

    invoke-direct {p0}, Landroid/support/v7/widget/bq;-><init>()V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/aw;->a:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/google/android/gms/car/support/aw;->a:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/gms/car/support/aw;->b:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v1}, Lcom/google/android/gms/car/support/PagedListView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a002c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method private a(Landroid/view/View;)Landroid/widget/TextView;
    .locals 4

    const/4 v0, 0x0

    if-nez p1, :cond_0

    move-object p1, v0

    :goto_0
    return-object p1

    :cond_0
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    check-cast p1, Landroid/widget/TextView;

    goto :goto_0

    :cond_1
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_3

    check-cast p1, Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/support/aw;->a(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object p1, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move-object p1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;)V
    .locals 10

    const/4 v7, 0x0

    invoke-virtual {p2, v7}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    move v6, v7

    :goto_0
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v1

    sub-int v8, v0, v1

    int-to-float v1, v6

    const/4 v2, 0x0

    int-to-float v3, v8

    const/high16 v4, 0x3f800000    # 1.0f

    iget-object v5, p0, Lcom/google/android/gms/car/support/aw;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v9

    :goto_1
    if-ge v7, v9, :cond_4

    invoke-virtual {p2, v7}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v0, v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;->bottomMargin:I

    sub-int v0, v1, v0

    add-int/lit8 v2, v0, -0x1

    if-lez v2, :cond_0

    int-to-float v1, v6

    int-to-float v2, v2

    int-to-float v3, v8

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/gms/car/support/aw;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/car/support/aw;->a(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    :cond_2
    move-object v3, v0

    move v0, v7

    :goto_2
    if-eqz v3, :cond_3

    if-eq v3, v1, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v3, v0

    move v0, v2

    goto :goto_2

    :cond_3
    move v6, v0

    goto :goto_0

    :cond_4
    return-void
.end method
