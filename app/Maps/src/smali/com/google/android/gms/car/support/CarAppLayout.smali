.class public Lcom/google/android/gms/car/support/CarAppLayout;
.super Landroid/widget/FrameLayout;

# interfaces
.implements Lcom/google/android/gms/car/support/y;


# static fields
.field static final a:Ljava/lang/String;

.field private static final n:Landroid/content/Intent;


# instance fields
.field private final A:Landroid/widget/TextView;

.field private final B:Lcom/google/android/gms/car/support/z;

.field private final C:Landroid/graphics/drawable/Drawable;

.field private final D:Landroid/graphics/drawable/Drawable;

.field private final E:Landroid/graphics/drawable/Drawable;

.field private final F:Landroid/graphics/drawable/Drawable;

.field private final G:Landroid/os/Handler;

.field private final H:Ljava/lang/Runnable;

.field final b:Landroid/content/BroadcastReceiver;

.field final c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

.field final d:Landroid/content/Context;

.field e:Landroid/os/HandlerThread;

.field f:Landroid/os/Handler;

.field g:Lcom/google/android/gms/common/api/o;

.field final h:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lcom/google/android/gms/common/api/o;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/android/gms/car/support/p;

.field j:Lcom/google/android/gms/car/support/q;

.field k:Lcom/google/android/gms/car/a/c;

.field l:Lcom/google/android/gms/car/support/w;

.field final m:Landroid/os/Handler$Callback;

.field private final o:Landroid/widget/ImageView;

.field private final p:Landroid/view/ViewGroup;

.field private final q:Landroid/view/View;

.field private final r:Landroid/view/View;

.field private final s:Landroid/view/View;

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/widget/ImageView;

.field private final x:Landroid/widget/ImageView;

.field private final y:Landroid/widget/ImageView;

.field private final z:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.projected.BVRA"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->n:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/support/CarAppLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/car/support/CarAppLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->h:Ljava/util/concurrent/BlockingQueue;

    new-instance v0, Lcom/google/android/gms/car/support/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/b;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->m:Landroid/os/Handler$Callback;

    new-instance v0, Lcom/google/android/gms/car/support/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/d;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->H:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040028

    invoke-virtual {v0, v1, p0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v1, "ic_airplane_mode"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->F:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v1, "wifi_signal"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->C:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v1, "cell_signal"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->D:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v1, "battery"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->E:Landroid/graphics/drawable/Drawable;

    new-instance v0, Lcom/google/android/gms/car/support/w;

    invoke-direct {v0, p1, p0}, Lcom/google/android/gms/car/support/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/support/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->l:Lcom/google/android/gms/car/support/w;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->G:Landroid/os/Handler;

    const v0, 0x7f100106

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->u:Landroid/view/View;

    const v0, 0x7f100109

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->v:Landroid/widget/TextView;

    const v0, 0x7f100110

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->o:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->o:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v2, "ic_mic_none"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->o:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/gms/car/support/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/a;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f100103

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->p:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/animation/LayoutTransition;->enableTransitionType(I)V

    const v0, 0x10c000f

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    const v1, 0x10c000e

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v1

    const v2, 0x10c000d

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/support/CarAppLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v3

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v0}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v5, v2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v6, v2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutTransition()Landroid/animation/LayoutTransition;

    move-result-object v0

    invoke-virtual {v0, v7, v2}, Landroid/animation/LayoutTransition;->setInterpolator(ILandroid/animation/TimeInterpolator;)V

    const v0, 0x7f10010a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->q:Landroid/view/View;

    const v0, 0x7f10010b

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->r:Landroid/view/View;

    const v0, 0x7f10010c

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->s:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->s:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/car/support/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/e;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10010d

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->t:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->t:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v2, "ic_google"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f10010e

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/CarRestrictedEditText;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    new-instance v1, Lcom/google/android/gms/car/support/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/f;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    new-instance v1, Lcom/google/android/gms/car/support/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/i;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    new-instance v1, Lcom/google/android/gms/car/support/j;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/j;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    new-instance v1, Lcom/google/android/gms/car/support/l;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/support/l;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f100108

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->x:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->x:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f100107

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const-string v2, "cell_signal"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/support/al;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f10010f

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    new-instance v0, Lcom/google/android/gms/car/support/z;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/support/z;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->B:Lcom/google/android/gms/car/support/z;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->B:Lcom/google/android/gms/car/support/z;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const v0, 0x7f100104

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->z:Landroid/view/View;

    const v0, 0x7f100105

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->A:Landroid/widget/TextView;

    invoke-direct {p0, v5}, Lcom/google/android/gms/car/support/CarAppLayout;->a(I)V

    new-instance v0, Lcom/google/android/gms/car/support/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/m;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->b:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method private a(I)V
    .locals 8

    const v7, 0x7f0b0003

    const/4 v6, 0x0

    const/16 v2, 0x8

    const/4 v5, 0x2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    if-ne p1, v5, :cond_0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/car/support/CarAppLayout;->q:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/google/android/gms/car/support/CarAppLayout;->s:Landroid/view/View;

    if-eq p1, v5, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v3, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    if-ne p1, v5, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setVisibility(I)V

    if-ne p1, v5, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->u:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    return-void

    :cond_0
    const/4 v3, 0x1

    if-ne p1, v3, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0004

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginStart(I)V

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    neg-int v3, v3

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMarginEnd(I)V

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->z:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->u:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3
.end method

.method private static a(Landroid/view/View;I)V
    .locals 2

    instance-of v0, p0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v0, p0, Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    check-cast p0, Landroid/widget/ImageView;

    new-instance v0, Landroid/graphics/PorterDuffColorFilter;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v0, p1, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p0, v0}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->B:Lcom/google/android/gms/car/support/z;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->A:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method public dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "dispatchGenericMotionEvent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    sget-object v3, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    invoke-static {v3, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "v="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " handled="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    :cond_1
    if-nez v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_1
    if-nez v0, :cond_3

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchGenericMotionEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :cond_3
    return v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    sget-object v2, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "dispatchKeyEvent handled="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/car/support/CarAppLayout;->n:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->callOnClick()Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x15 -> :sswitch_1
        0x16 -> :sswitch_0
    .end sparse-switch
.end method

.method public final e()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->A:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method f()V
    .locals 5

    const/4 v4, -0x1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    const/4 v1, 0x0

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "level"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "scale"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    sget-object v1, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting battery level to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->E:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->G:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->H:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3a98

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->l:Lcom/google/android/gms/car/support/w;

    iget-object v1, v0, Lcom/google/android/gms/car/support/w;->b:Landroid/telephony/TelephonyManager;

    iget-object v2, v0, Lcom/google/android/gms/car/support/w;->f:Landroid/telephony/PhoneStateListener;

    const/16 v3, 0x1e1

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, v0, Lcom/google/android/gms/car/support/w;->e:Landroid/content/Context;

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/w;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->f()V

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.car.DISCONNECTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/support/CarAppLayout;->b:Landroid/content/BroadcastReceiver;

    const-string v3, "com.google.android.gms.permission.CAR"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->l:Lcom/google/android/gms/car/support/w;

    iget-object v1, v0, Lcom/google/android/gms/car/support/w;->b:Landroid/telephony/TelephonyManager;

    iget-object v2, v0, Lcom/google/android/gms/car/support/w;->f:Landroid/telephony/PhoneStateListener;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    iget-object v1, v0, Lcom/google/android/gms/car/support/w;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->G:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/CarAppLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    const v0, 0x7f100102

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    return-void
.end method

.method public final setAirplaneMode()V
    .locals 2

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->F:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final setCellSignalLevel(I)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setCellSignalLevel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->D:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageLevel(I)V

    return-void
.end method

.method public final setMenuClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setMenuDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final setMenuProgress(F)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->B:Lcom/google/android/gms/car/support/z;

    iput p1, v0, Lcom/google/android/gms/car/support/z;->a:F

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/z;->invalidateSelf()V

    return-void
.end method

.method public final setMicButtonColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->o:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->a(Landroid/view/View;I)V

    return-void
.end method

.method public final setSearchBoxColors(IIII)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->r:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->t:Landroid/widget/ImageView;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, p2, v1}, Landroid/widget/ImageView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setTextColor(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    invoke-virtual {v0, p4}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setHintTextColor(I)V

    return-void
.end method

.method public final setSearchBoxModeLarge(Lcom/google/android/gms/car/a/c;Lcom/google/android/gms/car/support/q;Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->i:Lcom/google/android/gms/car/support/p;

    iput-object p2, p0, Lcom/google/android/gms/car/support/CarAppLayout;->j:Lcom/google/android/gms/car/support/q;

    iput-object p1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->k:Lcom/google/android/gms/car/a/c;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setHint(Ljava/lang/CharSequence;)V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->requestFocus()Z

    return-void
.end method

.method public final setSearchBoxModeNone()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->i:Lcom/google/android/gms/car/support/p;

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->j:Lcom/google/android/gms/car/support/q;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->k:Lcom/google/android/gms/car/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->k:Lcom/google/android/gms/car/a/c;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/c;->a()V

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->k:Lcom/google/android/gms/car/a/c;

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setSearchBoxModeSmall(Lcom/google/android/gms/car/support/p;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->i:Lcom/google/android/gms/car/support/p;

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->j:Lcom/google/android/gms/car/support/q;

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->k:Lcom/google/android/gms/car/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->k:Lcom/google/android/gms/car/a/c;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/c;->a()V

    iput-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->k:Lcom/google/android/gms/car/a/c;

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setHint(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->c:Lcom/google/android/gms/car/support/CarRestrictedEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/CarRestrictedEditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setStatusViewColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->v:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->x:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->y:Landroid/widget/ImageView;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->a(Landroid/view/View;I)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->A:Landroid/widget/TextView;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/support/CarAppLayout;->a(Landroid/view/View;I)V

    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->A:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final setTitleClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->A:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final setWifiSignalLevel(I)V
    .locals 2

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setWifiSignalLevel: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/car/support/CarAppLayout;->C:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/CarAppLayout;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageLevel(I)V

    return-void
.end method
