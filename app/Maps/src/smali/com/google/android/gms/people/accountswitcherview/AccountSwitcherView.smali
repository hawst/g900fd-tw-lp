.class public Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/google/android/gms/people/accountswitcherview/ab;
.implements Lcom/google/android/gms/people/accountswitcherview/ad;


# instance fields
.field public a:Lcom/google/android/gms/people/accountswitcherview/b;

.field public b:Lcom/google/android/gms/people/accountswitcherview/d;

.field public c:Lcom/google/android/gms/people/accountswitcherview/c;

.field public d:Lcom/google/android/gms/people/model/a;

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/people/model/a;",
            ">;"
        }
    .end annotation
.end field

.field public f:Landroid/widget/ListView;

.field public g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

.field public h:Lcom/google/android/gms/people/accountswitcherview/p;

.field public i:Lcom/google/android/gms/common/api/o;

.field public j:Lcom/google/android/gms/people/accountswitcherview/g;

.field public k:I

.field public l:Lcom/google/android/gms/people/accountswitcherview/e;

.field public m:Z

.field public n:Z

.field private o:Landroid/widget/FrameLayout;

.field private p:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

.field private q:Z

.field private r:Landroid/view/ViewGroup;

.field private s:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

.field private t:I

.field private u:I

.field private v:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 75
    iput-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->m:Z

    .line 76
    iput-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->n:Z

    .line 89
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 90
    new-array v2, v0, [I

    const v3, 0x7f010103

    aput v3, v2, v1

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 92
    sget-object v3, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v4, "L"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-lt v3, v4, :cond_1

    :cond_0
    :goto_0
    invoke-virtual {v2, v1, v0}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->v:Z

    .line 93
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 94
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f04001a

    invoke-virtual {v0, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    const v0, 0x7f10024a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f10023f

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->s:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->s:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f1000af

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-boolean v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->v:Z

    iput-boolean v2, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->k:Z

    iget-boolean v2, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->k:Z

    iput-boolean v2, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->j:Z

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iput-object p0, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a:Lcom/google/android/gms/people/accountswitcherview/ad;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iput-object p0, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->h:Lcom/google/android/gms/people/accountswitcherview/ab;

    const v0, 0x7f1000aa

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->f:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->f:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    const v0, 0x7f1000b0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->p:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->k:I

    const v0, 0x7f1000ae

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setNavigationMode(I)V

    .line 95
    return-void

    :cond_1
    move v0, v1

    .line 92
    goto :goto_0
.end method

.method private a(ZLandroid/view/animation/Interpolator;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 601
    if-eqz p1, :cond_0

    move v0, v1

    move v3, v2

    .line 608
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->p:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    const-string v5, "animatedHeightFraction"

    const/4 v6, 0x2

    new-array v6, v6, [F

    int-to-float v3, v3

    aput v3, v6, v2

    int-to-float v0, v0

    aput v0, v6, v1

    invoke-static {v4, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 610
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 611
    invoke-virtual {v0, p2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 612
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 613
    return-void

    :cond_0
    move v0, v2

    move v3, v1

    .line 606
    goto :goto_0
.end method

.method private b(Z)V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    const v5, 0x3f4ccccd    # 0.8f

    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 569
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget v0, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b:I

    .line 570
    packed-switch v0, :pswitch_data_0

    .line 597
    :goto_0
    return-void

    .line 572
    :pswitch_0
    if-eqz p1, :cond_0

    .line 573
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 574
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 575
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    .line 576
    new-instance v0, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v0, v5}, Landroid/view/animation/AccelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v4, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(ZLandroid/view/animation/Interpolator;)V

    .line 580
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 581
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->p:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    .line 578
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1

    .line 585
    :pswitch_1
    if-eqz p1, :cond_1

    .line 586
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 587
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 588
    const-wide/16 v2, 0x85

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 589
    const/4 v0, 0x1

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1, v5}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a(ZLandroid/view/animation/Interpolator;)V

    .line 593
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v6}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 594
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->p:Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/accountswitcherview/ShrinkingItem;->setVisibility(I)V

    goto :goto_0

    .line 591
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_2

    .line 570
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b()Z
    .locals 2

    .prologue
    .line 727
    sget-object v0, Landroid/os/Build$VERSION;->CODENAME:Ljava/lang/String;

    const-string v1, "L"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 561
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b(Z)V

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->l:Lcom/google/android/gms/people/accountswitcherview/e;

    if-eqz v0, :cond_0

    .line 563
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->l:Lcom/google/android/gms/people/accountswitcherview/e;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 564
    iget v1, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b:I

    .line 563
    invoke-interface {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/e;->b(I)V

    .line 566
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/model/a;)V
    .locals 2

    .prologue
    .line 649
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setSelectedAccount(Lcom/google/android/gms/people/model/a;Z)V

    .line 650
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/b;

    if-eqz v0, :cond_0

    .line 651
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/b;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Lcom/google/android/gms/people/model/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Lcom/google/android/gms/people/model/a;)V

    .line 653
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 122
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    if-eq v0, p1, :cond_0

    .line 123
    iput-boolean p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->offsetTopAndBottom(I)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->offsetTopAndBottom(I)V

    .line 126
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    if-nez v0, :cond_1

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setVisibility(I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 133
    :goto_0
    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    .line 134
    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    .line 136
    :cond_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setVisibility(I)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public getNestedScrollAxes()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x2

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 666
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    if-eq p1, v0, :cond_1

    .line 667
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->s:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    if-ne p1, v0, :cond_1

    .line 671
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 672
    iget v0, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b:I

    if-ne v0, v2, :cond_2

    move v0, v1

    .line 671
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setNavigationMode(I)V

    .line 675
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->s:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 676
    iget v0, v0, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b:I

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    iput-boolean v1, v3, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->a:Z

    iget-boolean v0, v3, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, v3, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v3}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->refreshDrawableState()V

    .line 678
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-direct {p0, v2}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->l:Lcom/google/android/gms/people/accountswitcherview/e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->l:Lcom/google/android/gms/people/accountswitcherview/e;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget v1, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/e;->b(I)V

    .line 680
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 672
    goto :goto_0

    .line 676
    :cond_3
    iget-object v0, v3, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/p;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_1

    .line 618
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/p;->a(I)Lcom/google/android/gms/people/model/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->setSelectedAccount(Lcom/google/android/gms/people/model/a;Z)V

    .line 620
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/b;

    if-eqz v0, :cond_0

    .line 621
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->a:Lcom/google/android/gms/people/accountswitcherview/b;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Lcom/google/android/gms/people/model/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/b;->a(Lcom/google/android/gms/people/model/a;)V

    .line 632
    :cond_0
    :goto_0
    return-void

    .line 623
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/p;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 624
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:Lcom/google/android/gms/people/accountswitcherview/c;

    if-eqz v0, :cond_0

    .line 625
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->c:Lcom/google/android/gms/people/accountswitcherview/c;

    invoke-interface {v0}, Lcom/google/android/gms/people/accountswitcherview/c;->b()V

    goto :goto_0

    .line 627
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/people/accountswitcherview/p;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 628
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Lcom/google/android/gms/people/accountswitcherview/d;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b:Lcom/google/android/gms/people/accountswitcherview/d;

    invoke-interface {v0}, Lcom/google/android/gms/people/accountswitcherview/d;->a()V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 290
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 295
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    .line 296
    :goto_0
    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 297
    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 299
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    .line 302
    :cond_1
    return-void

    .line 295
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    .line 270
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 271
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 272
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 273
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 274
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v0

    .line 276
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    .line 277
    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v4

    .line 276
    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    .line 282
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->getHeight()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 281
    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->measure(II)V

    .line 286
    :cond_0
    return-void

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 275
    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->getMeasuredHeight()I

    move-result v0

    goto :goto_1

    .line 271
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public onNestedFling(Landroid/view/View;FFZ)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 309
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    .line 310
    :goto_0
    if-nez p4, :cond_1

    cmpg-float v1, p3, v2

    if-gez v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-gez v1, :cond_1

    .line 312
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    .line 313
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    .line 314
    const/4 v0, 0x1

    .line 326
    :goto_1
    return v0

    .line 309
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 315
    :cond_1
    if-eqz p4, :cond_3

    cmpl-float v1, p3, v2

    if-lez v1, :cond_3

    .line 316
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_2

    .line 320
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    neg-int v1, v1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    .line 322
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    neg-int v2, v2

    if-le v1, v2, :cond_3

    .line 323
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    .line 326
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onNestedPreScroll(Landroid/view/View;II[I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 339
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    .line 340
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget v1, v1, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b:I

    if-ne v1, v5, :cond_2

    .line 371
    :cond_0
    :goto_1
    return-void

    .line 339
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 345
    :cond_2
    if-lez p3, :cond_6

    .line 347
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-lez v1, :cond_6

    .line 349
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    if-le v1, p3, :cond_3

    .line 350
    neg-int v1, p3

    .line 356
    :goto_2
    if-eqz v1, :cond_0

    .line 358
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_4

    .line 359
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    neg-int v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    .line 363
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    add-int/2addr v3, v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    neg-int v4, v4

    if-ge v3, v4, :cond_5

    .line 364
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    .line 368
    :goto_4
    aput v2, p4, v2

    .line 369
    aput v1, p4, v5

    goto :goto_1

    .line 352
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    neg-int v1, v1

    goto :goto_2

    .line 361
    :cond_4
    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    goto :goto_3

    .line 366
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public onNestedScroll(Landroid/view/View;IIII)V
    .locals 3

    .prologue
    .line 392
    const/4 v0, 0x0

    .line 393
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->q:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->r:Landroid/view/ViewGroup;

    .line 394
    :goto_0
    if-gez p5, :cond_5

    .line 395
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    if-gez v2, :cond_5

    .line 396
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    if-gt p5, v0, :cond_0

    .line 397
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result p5

    .line 403
    :cond_0
    :goto_1
    if-eqz p5, :cond_1

    .line 405
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    if-lez v0, :cond_3

    .line 406
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    .line 410
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    sub-int/2addr v0, p5

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-le v0, v2, :cond_4

    .line 411
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getTop()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    .line 416
    :cond_1
    :goto_3
    return-void

    .line 393
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    goto :goto_0

    .line 408
    :cond_3
    neg-int v0, p5

    invoke-virtual {v1, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->t:I

    goto :goto_2

    .line 413
    :cond_4
    neg-int v0, p5

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->o:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->u:I

    goto :goto_3

    :cond_5
    move p5, v0

    goto :goto_1
.end method

.method public onStartNestedScroll(Landroid/view/View;Landroid/view/View;I)Z
    .locals 1

    .prologue
    .line 334
    const/4 v0, 0x0

    return v0
.end method

.method public final setNavigationMode(I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 544
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->setNavigationMode(I)V

    .line 545
    invoke-direct {p0, v1}, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->b(Z)V

    .line 546
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->s:Lcom/google/android/gms/people/accountswitcherview/ExpanderView;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    .line 547
    iget v3, v3, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->b:I

    if-ne v3, v0, :cond_0

    :goto_0
    iput-boolean v0, v2, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->a:Z

    iget-boolean v0, v2, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->c:Ljava/lang/String;

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v2}, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->refreshDrawableState()V

    .line 549
    return-void

    :cond_0
    move v0, v1

    .line 547
    goto :goto_0

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/people/accountswitcherview/ExpanderView;->b:Ljava/lang/String;

    goto :goto_1
.end method

.method public setSelectedAccount(Lcom/google/android/gms/people/model/a;Z)V
    .locals 3

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Lcom/google/android/gms/people/model/a;

    .line 460
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Lcom/google/android/gms/people/model/a;

    .line 461
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 462
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Lcom/google/android/gms/people/model/a;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/people/accountswitcherview/p;->a(Ljava/util/List;Lcom/google/android/gms/people/model/a;Lcom/google/android/gms/people/model/a;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Ljava/util/List;

    .line 463
    if-nez p2, :cond_0

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->d:Lcom/google/android/gms/people/model/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/model/a;)V

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->h:Lcom/google/android/gms/people/accountswitcherview/p;

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/p;->a(Ljava/util/List;)V

    .line 471
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/AccountSwitcherView;->g:Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/SelectedAccountNavigationView;->a(Lcom/google/android/gms/people/model/a;)V

    goto :goto_0
.end method
