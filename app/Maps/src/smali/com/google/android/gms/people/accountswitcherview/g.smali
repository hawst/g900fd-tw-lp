.class public Lcom/google/android/gms/people/accountswitcherview/g;
.super Lcom/google/android/gms/people/accountswitcherview/m;
.source "PG"


# static fields
.field private static h:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/accountswitcherview/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;Z)V

    .line 62
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 83
    sget-object v0, Lcom/google/android/gms/people/accountswitcherview/g;->h:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02003d

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/accountswitcherview/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/accountswitcherview/g;->h:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v0, Lcom/google/android/gms/people/accountswitcherview/g;->h:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/people/accountswitcherview/g;Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/accountswitcherview/n;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 25
    invoke-super/range {p0 .. p5}, Lcom/google/android/gms/people/accountswitcherview/m;->a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/accountswitcherview/n;Landroid/graphics/Bitmap;I)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/accountswitcherview/h;-><init>(Lcom/google/android/gms/people/accountswitcherview/g;Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    .line 72
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/m;->b:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/people/accountswitcherview/g;->h:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003d

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/accountswitcherview/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/people/accountswitcherview/g;->h:Landroid/graphics/Bitmap;

    :cond_0
    sget-object v1, Lcom/google/android/gms/people/accountswitcherview/g;->h:Landroid/graphics/Bitmap;

    .line 71
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/g;->a(Lcom/google/android/gms/people/accountswitcherview/n;)V

    .line 73
    return-void
.end method
