.class Lcom/google/android/gms/car/p;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/api/r;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/m;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/p;->a:Lcom/google/android/gms/car/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/p;->a:Lcom/google/android/gms/car/m;

    invoke-static {v1}, Lcom/google/android/gms/car/m;->c(Lcom/google/android/gms/car/m;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".onConnectionFailed() with error "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v2, 0x10000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/car/p;->a:Lcom/google/android/gms/car/m;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/m;->startActivity(Landroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/p;->a:Lcom/google/android/gms/car/m;

    invoke-virtual {v0}, Lcom/google/android/gms/car/m;->b()V

    return-void

    :cond_1
    iget v0, p1, Lcom/google/android/gms/common/a;->c:I

    invoke-static {v0}, Lcom/google/android/gms/common/g;->a(I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method
