.class public Lcom/google/android/gms/car/em;
.super Landroid/app/Presentation;


# instance fields
.field a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/car/em;-><init>(Landroid/content/Context;Landroid/view/Display;I)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/view/Display;I)V
    .locals 2

    if-nez p3, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/car/em;->a(Landroid/content/Context;)I

    move-result p3

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/em;->a:Z

    invoke-virtual {p0}, Lcom/google/android/gms/car/em;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x7ee

    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const/high16 v1, 0x1000000

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p0, Landroid/app/Service;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v2, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v2, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/16 v3, 0x80

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getServiceInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ServiceInfo;

    move-result-object v1

    iget-object v3, v1, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->theme:I

    if-eqz v3, :cond_1

    const-string v4, "android.app.theme"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Could not get theme for component "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; use the default theme"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method static a(Landroid/view/Window;Landroid/view/InputEvent;)V
    .locals 0

    invoke-virtual {p0, p1}, Landroid/view/Window;->injectInputEvent(Landroid/view/InputEvent;)V

    return-void
.end method

.method static a(Landroid/view/Window;ZZ)V
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1, p2}, Landroid/view/Window;->setLocalFocus(ZZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/em;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/car/em;->a:Z

    return p1
.end method


# virtual methods
.method public cancel()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/car/em;->a:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/app/Presentation;->cancel()V

    :cond_0
    return-void
.end method

.method public dismiss()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/car/em;->a:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/app/Presentation;->dismiss()V

    :cond_0
    return-void
.end method

.method public show()V
    .locals 2

    invoke-super {p0}, Landroid/app/Presentation;->show()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/google/android/gms/car/en;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/en;-><init>(Lcom/google/android/gms/car/em;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
