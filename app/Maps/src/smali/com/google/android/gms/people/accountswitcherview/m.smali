.class public Lcom/google/android/gms/people/accountswitcherview/m;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static a:I


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/google/android/gms/common/api/o;

.field final d:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field e:Z

.field f:F

.field g:F

.field private h:Lcom/google/android/gms/people/accountswitcherview/n;

.field private final i:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/gms/people/accountswitcherview/n;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/people/accountswitcherview/m;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/o;Z)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->d:Ljava/util/concurrent/ConcurrentHashMap;

    .line 65
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->i:Ljava/util/LinkedList;

    .line 75
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/m;->b:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/google/android/gms/people/accountswitcherview/m;->c:Lcom/google/android/gms/common/api/o;

    .line 77
    iput-boolean p3, p0, Lcom/google/android/gms/people/accountswitcherview/m;->e:Z

    .line 78
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 79
    const v1, 0x7f0e0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/m;->g:F

    .line 80
    const v1, 0x7f0e0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->f:F

    .line 81
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;IF)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v7, 0x3f000000    # 0.5f

    const/4 v2, 0x0

    .line 262
    int-to-float v0, p1

    mul-float/2addr v0, p2

    float-to-int v0, v0

    cmpg-float v1, v7, v2

    if-ltz v1, :cond_0

    cmpl-float v1, v7, v3

    if-gtz v1, :cond_0

    cmpg-float v1, v7, v2

    if-ltz v1, :cond_0

    cmpl-float v1, v7, v3

    if-lez v1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "horizontalCenterPercent and verticalCenterPercent must be between 0.0f and 1.0f, inclusive."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ne p1, v1, :cond_2

    if-ne v0, v2, :cond_2

    :goto_0
    return-object p0

    :cond_2
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    int-to-float v3, p1

    int-to-float v4, v1

    div-float/2addr v3, v4

    int-to-float v4, v0

    int-to-float v6, v2

    div-float/2addr v4, v6

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    invoke-virtual {v5, v4, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    int-to-float v3, p1

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v0, v0

    div-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-float v0, v1

    mul-float/2addr v0, v7

    div-int/lit8 v6, v3, 0x2

    int-to-float v6, v6

    sub-float/2addr v0, v6

    float-to-int v0, v0

    int-to-float v6, v2

    mul-float/2addr v6, v7

    div-int/lit8 v7, v4, 0x2

    int-to-float v7, v7

    sub-float/2addr v6, v7

    float-to-int v6, v6

    sub-int/2addr v1, v3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v1

    sub-int v0, v2, v4

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, v8}, Ljava/lang/Math;->max(II)I

    move-result v2

    const/4 v6, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;

    if-nez v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/n;

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;

    invoke-virtual {v0}, Lcom/google/android/gms/people/accountswitcherview/n;->a()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 130
    const/4 v0, 0x0

    move v1, v0

    .line 132
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->i:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/n;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->i:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 136
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    if-ne v0, p1, :cond_2

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/people/accountswitcherview/n;->d:Z

    .line 143
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/accountswitcherview/n;Landroid/graphics/Bitmap;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 159
    .line 161
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, p3, :cond_1

    .line 162
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/m;->a()V

    .line 188
    if-eqz p2, :cond_0

    .line 190
    :try_start_1
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 195
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->h:Lcom/google/android/gms/people/accountswitcherview/n;

    .line 167
    iget-object v0, p3, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    .line 171
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p3, :cond_2

    iget-boolean v0, p3, Lcom/google/android/gms/people/accountswitcherview/n;->d:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v0, :cond_3

    .line 185
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/m;->a()V

    .line 188
    if-eqz p2, :cond_0

    .line 190
    :try_start_3
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 176
    :cond_3
    :try_start_4
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez p2, :cond_5

    .line 177
    :cond_4
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Avatar loaded: status="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "  pfd="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 180
    :cond_5
    if-eqz p2, :cond_6

    .line 181
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/o;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p4

    move v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/accountswitcherview/o;-><init>(Lcom/google/android/gms/people/accountswitcherview/m;Lcom/google/android/gms/people/accountswitcherview/n;Landroid/os/ParcelFileDescriptor;Landroid/graphics/Bitmap;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/accountswitcherview/o;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object p2, v6

    .line 185
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/m;->a()V

    .line 188
    if-eqz p2, :cond_0

    .line 190
    :try_start_5
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_0

    .line 192
    :catch_1
    move-exception v0

    goto :goto_0

    .line 185
    :catchall_0
    move-exception v0

    .line 186
    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/m;->a()V

    .line 188
    if-eqz p2, :cond_7

    .line 190
    :try_start_6
    invoke-virtual {p2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    .line 192
    :cond_7
    :goto_1
    throw v0

    :catch_2
    move-exception v0

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/people/accountswitcherview/n;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->d:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p1, Lcom/google/android/gms/people/accountswitcherview/n;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    iget-object v1, p1, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->d:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p1, Lcom/google/android/gms/people/accountswitcherview/n;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 101
    iget-object v0, p1, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/m;->a(Landroid/widget/ImageView;)V

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/people/accountswitcherview/n;->e:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/accountswitcherview/m;->a(Landroid/widget/ImageView;)V

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/m;->c:Lcom/google/android/gms/common/api/o;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/m;->i:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/gms/people/accountswitcherview/m;->a()V

    goto :goto_0
.end method
