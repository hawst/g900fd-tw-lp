.class public Lcom/google/android/gms/car/support/PagedRecyclerView;
.super Landroid/support/v7/widget/RecyclerView;


# instance fields
.field H:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/support/PagedRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/car/support/PagedRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/PagedRecyclerView;->H:Z

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->k:Z

    return-void
.end method

.method private a(Landroid/view/View;F)V
    .locals 2

    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    check-cast p1, Landroid/view/ViewGroup;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, p2}, Lcom/google/android/gms/car/support/PagedRecyclerView;->a(Landroid/view/View;F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/View;->setAlpha(F)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3

    const/high16 v1, 0x3f800000    # 1.0f

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/PagedRecyclerView;->H:Z

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getBottom()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getBottom()I

    move-result v2

    if-le v0, v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getBottom()I

    move-result v0

    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    :goto_0
    sub-float v2, v1, v0

    sub-float v0, v1, v0

    mul-float/2addr v0, v2

    sub-float v0, v1, v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->a(Landroid/view/View;F)V

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v7/widget/RecyclerView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getTop()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getTop()I

    move-result v2

    if-le v0, v2, :cond_2

    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/PagedRecyclerView;->getTop()I

    move-result v2

    sub-int/2addr v0, v2

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method
