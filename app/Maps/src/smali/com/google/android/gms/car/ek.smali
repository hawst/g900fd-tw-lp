.class Lcom/google/android/gms/car/ek;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/car/a/c;


# instance fields
.field a:Lcom/google/android/gms/car/el;

.field b:Lcom/google/android/gms/car/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/el;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/el;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/el;

    if-eqz v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    if-nez v0, :cond_2

    :goto_2
    return-void

    :cond_0
    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/el;

    invoke-interface {v0}, Lcom/google/android/gms/car/el;->a()V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/car/a/a;)V
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/el;

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    if-nez v2, :cond_3

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    :cond_3
    if-eqz p1, :cond_0

    new-instance v2, Landroid/view/inputmethod/EditorInfo;

    invoke-direct {v2}, Landroid/view/inputmethod/EditorInfo;-><init>()V

    invoke-interface {p1, v2}, Lcom/google/android/gms/car/a/a;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v3

    const-string v4, "CAR.INPUT"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_5

    :goto_3
    if-eqz v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "startInput/editorInfo.imeOptions="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, v2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_4
    if-nez v3, :cond_6

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Null input connection received for view of type: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/el;

    new-instance v1, Lcom/google/android/gms/car/a/j;

    invoke-direct {v1, v3, p1}, Lcom/google/android/gms/car/a/j;-><init>(Landroid/view/inputmethod/InputConnection;Lcom/google/android/gms/car/a/a;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/car/el;->a(Lcom/google/android/gms/car/a/j;Landroid/view/inputmethod/EditorInfo;)V

    goto :goto_2
.end method
