.class Lcom/google/android/gms/car/q;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/car/e;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/m;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    const/4 v1, 0x1

    const-string v0, "CAR.PROJECTION"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-static {v2}, Lcom/google/android/gms/car/m;->c(Lcom/google/android/gms/car/m;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".onConnected()"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0, v1}, Lcom/google/android/gms/car/m;->a(Lcom/google/android/gms/car/m;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->d(Lcom/google/android/gms/car/m;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    const/4 v1, 0x0

    const-string v0, "CAR.PROJECTION"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-static {v2}, Lcom/google/android/gms/car/m;->c(Lcom/google/android/gms/car/m;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ".onDisconnected()"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->d(Lcom/google/android/gms/car/m;)Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    iget-object v0, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0, v1}, Lcom/google/android/gms/car/m;->a(Lcom/google/android/gms/car/m;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/car/q;->a:Lcom/google/android/gms/car/m;

    invoke-virtual {v0}, Lcom/google/android/gms/car/m;->b()V

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
