.class public final Lcom/google/android/gms/common/api/p;
.super Ljava/lang/Object;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/q;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/gms/common/api/r;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/gms/common/api/a",
            "<*>;",
            "Lcom/google/android/gms/common/api/b;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Landroid/os/Looper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->f:Ljava/util/Map;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/p;->g:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->b:Ljava/util/Set;

    iput-object p1, p0, Lcom/google/android/gms/common/api/p;->e:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->h:Landroid/os/Looper;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/p;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/o;
    .locals 15

    const/4 v12, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/common/api/p;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v12

    :goto_0
    const-string v2, "must call addApi() to add at least one API"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/common/api/p;->g:I

    if-ltz v0, :cond_8

    invoke-static {v1}, Lcom/google/android/gms/common/api/ai;->a(Landroid/support/v4/app/m;)Lcom/google/android/gms/common/api/ai;

    move-result-object v14

    iget v0, p0, Lcom/google/android/gms/common/api/p;->g:I

    iget-object v2, v14, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    if-eqz v2, :cond_3

    invoke-virtual {v14, v0}, Lcom/google/android/gms/common/api/ai;->b(I)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/common/api/aj;->h:Lcom/google/android/gms/common/api/o;

    :goto_1
    if-nez v0, :cond_2

    new-instance v13, Lcom/google/android/gms/common/api/x;

    iget-object v0, p0, Lcom/google/android/gms/common/api/p;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    iget-object v6, p0, Lcom/google/android/gms/common/api/p;->h:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v2, p0, Lcom/google/android/gms/common/api/p;->c:Ljava/util/Set;

    iget-object v5, p0, Lcom/google/android/gms/common/api/p;->d:Ljava/lang/String;

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;)V

    iget-object v8, p0, Lcom/google/android/gms/common/api/p;->f:Ljava/util/Map;

    iget-object v9, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/Set;

    iget-object v10, p0, Lcom/google/android/gms/common/api/p;->b:Ljava/util/Set;

    iget v11, p0, Lcom/google/android/gms/common/api/p;->g:I

    move-object v4, v13

    move-object v5, v7

    move-object v7, v0

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/common/api/x;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    move-object v0, v13

    :cond_2
    iget v2, p0, Lcom/google/android/gms/common/api/p;->g:I

    const-string v4, "GoogleApiClient instance cannot be null"

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    iget-object v4, v14, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v4

    if-gez v4, :cond_5

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Already managing a GoogleApiClient with id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v12, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    move v12, v3

    goto :goto_2

    :cond_6
    new-instance v3, Lcom/google/android/gms/common/api/ak;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/common/api/ak;-><init>(Lcom/google/android/gms/common/api/o;Lcom/google/android/gms/common/api/r;)V

    iget-object v4, v14, Lcom/google/android/gms/common/api/ai;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v2, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v3, v14, Landroid/support/v4/app/j;->D:Landroid/support/v4/app/m;

    if-eqz v3, :cond_7

    invoke-virtual {v14}, Lcom/google/android/gms/common/api/ai;->g()Landroid/support/v4/app/am;

    move-result-object v3

    invoke-virtual {v3, v2, v1, v14}, Landroid/support/v4/app/am;->a(ILandroid/os/Bundle;Landroid/support/v4/app/an;)Landroid/support/v4/a/a;

    :cond_7
    :goto_3
    return-object v0

    :cond_8
    new-instance v9, Lcom/google/android/gms/common/api/x;

    iget-object v10, p0, Lcom/google/android/gms/common/api/p;->e:Landroid/content/Context;

    iget-object v11, p0, Lcom/google/android/gms/common/api/p;->h:Landroid/os/Looper;

    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v2, p0, Lcom/google/android/gms/common/api/p;->c:Ljava/util/Set;

    iget-object v5, p0, Lcom/google/android/gms/common/api/p;->d:Ljava/lang/String;

    move-object v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/common/api/p;->f:Ljava/util/Map;

    iget-object v6, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/Set;

    iget-object v7, p0, Lcom/google/android/gms/common/api/p;->b:Ljava/util/Set;

    const/4 v8, -0x1

    move-object v1, v9

    move-object v2, v10

    move-object v3, v11

    move-object v4, v0

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/common/api/x;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    move-object v0, v9

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/a",
            "<+",
            "Lcom/google/android/gms/common/api/d;",
            ">;)",
            "Lcom/google/android/gms/common/api/p;"
        }
    .end annotation

    iget-object v0, p0, Lcom/google/android/gms/common/api/p;->f:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/android/gms/common/api/a;->c:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/common/api/p;->c:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Scope;

    iget-object v0, v0, Lcom/google/android/gms/common/api/Scope;->b:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/a;Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/p;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O::",
            "Lcom/google/android/gms/common/api/c;",
            ">(",
            "Lcom/google/android/gms/common/api/a",
            "<TO;>;TO;)",
            "Lcom/google/android/gms/common/api/p;"
        }
    .end annotation

    const-string v0, "Null options are not permitted for this Api"

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/p;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/android/gms/common/api/a;->c:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/common/api/p;->c:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Scope;

    iget-object v0, v0, Lcom/google/android/gms/common/api/Scope;->b:Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/gms/common/api/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/p;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/p;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/common/api/p;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method
