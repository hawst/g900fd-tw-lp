.class public Lcom/google/android/gms/car/l;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/view/LayoutInflater$Factory;


# static fields
.field public static h:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field i:I

.field public j:Landroid/content/Context;

.field public k:Lcom/google/android/gms/car/m;

.field l:Landroid/content/Intent;

.field public m:Landroid/view/LayoutInflater;

.field public n:Landroid/view/Window;

.field public o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x6

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "STATE_INITIALIZING"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "STATE_CREATED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "STATE_STOPPED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string v2, "STATE_STARTED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string v2, "STATE_PAUSED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string v2, "STATE_RESUMED"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    const-string v1, "STATE_FINISHED"

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/l;->i:I

    return-void
.end method

.method static a(I)Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "Unknown"

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/car/l;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public static r()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static s()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public static u()V
    .locals 0

    return-void
.end method

.method public static v()V
    .locals 0

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/content/res/Configuration;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/l;->k:Lcom/google/android/gms/car/m;

    iput-object p1, v0, Lcom/google/android/gms/car/m;->e:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/gms/car/m;->b:Lcom/google/android/gms/car/em;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/em;->setContentView(Landroid/view/View;)V

    return-void
.end method

.method public a(ILandroid/view/KeyEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public final b(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/l;->k:Lcom/google/android/gms/car/m;

    iput p1, v0, Lcom/google/android/gms/car/m;->d:I

    return-void
.end method

.method public final b(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/l;->l:Landroid/content/Intent;

    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    const-string v0, "android:viewHierarchyState"

    iget-object v1, p0, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->saveHierarchyState()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method final c(Landroid/os/Bundle;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/l;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/l;->i:I

    return-void
.end method

.method public d()V
    .locals 0

    return-void
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/l;->k:Lcom/google/android/gms/car/m;

    invoke-virtual {v0}, Lcom/google/android/gms/car/m;->b()V

    return-void
.end method

.method public final h()Landroid/content/Context;
    .locals 2

    const-string v0, "CAR.PROJECTION"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Context DPI: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->densityDpi:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/l;->l:Landroid/content/Intent;

    return-object v0
.end method

.method public final j()Landroid/view/LayoutInflater;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/l;->m:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public final k()Landroid/content/res/Resources;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarProjectionActivity not initialized with attach()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/google/android/gms/car/m;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarProjectionActivity not initialized with attach()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/l;->k:Lcom/google/android/gms/car/m;

    return-object v0
.end method

.method final m()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/car/l;->a()V

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/car/l;->i:I

    return-void
.end method

.method final n()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/car/l;->b()V

    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/gms/car/l;->i:I

    invoke-virtual {p0}, Lcom/google/android/gms/car/l;->t()V

    return-void
.end method

.method final o()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/car/l;->c()V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/car/l;->i:I

    return-void
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method final p()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/car/l;->d()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/l;->i:I

    return-void
.end method

.method final q()V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/car/l;->e()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/l;->i:I

    iput-object v1, p0, Lcom/google/android/gms/car/l;->j:Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/gms/car/l;->k:Lcom/google/android/gms/car/m;

    iput-object v1, p0, Lcom/google/android/gms/car/l;->l:Landroid/content/Intent;

    iput-object v1, p0, Lcom/google/android/gms/car/l;->m:Landroid/view/LayoutInflater;

    iput-object v1, p0, Lcom/google/android/gms/car/l;->n:Landroid/view/Window;

    return-void
.end method

.method public t()V
    .locals 0

    return-void
.end method
