.class Lcom/google/android/gms/car/support/ap;
.super Landroid/support/v7/widget/ap;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/an;

.field private final m:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/support/an;Landroid/content/Context;Z)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/car/support/ap;->a:Lcom/google/android/gms/car/support/an;

    invoke-direct {p0, p2}, Landroid/support/v7/widget/ap;-><init>(Landroid/content/Context;)V

    if-eqz p3, :cond_0

    const v0, 0x10c0005

    :goto_0
    invoke-static {p2, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/ap;->m:Landroid/view/animation/Interpolator;

    return-void

    :cond_0
    const v0, 0x10c000d

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/car/support/ap;->a:Lcom/google/android/gms/car/support/an;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/an;->b(I)Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/view/View;Landroid/support/v7/widget/cb;)V
    .locals 4

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/car/support/ap;->a(Landroid/view/View;I)I

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/an;->a:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    neg-int v0, v0

    const/16 v2, 0x1f4

    iget-object v3, p0, Lcom/google/android/gms/car/support/ap;->m:Landroid/view/animation/Interpolator;

    iput v1, p2, Landroid/support/v7/widget/cb;->a:I

    iput v0, p2, Landroid/support/v7/widget/cb;->b:I

    iput v2, p2, Landroid/support/v7/widget/cb;->c:I

    iput-object v3, p2, Landroid/support/v7/widget/cb;->d:Landroid/view/animation/Interpolator;

    const/4 v0, 0x1

    iput-boolean v0, p2, Landroid/support/v7/widget/cb;->e:Z

    goto :goto_0
.end method
