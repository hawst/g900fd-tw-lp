.class Lcom/google/android/gms/car/ae;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/m;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 7

    const/16 v6, 0xa

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/car/m;->f()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->n(Lcom/google/android/gms/car/m;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/car/m;->b(Z)Z

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "FATAL EXCEPTION: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    const-string v1, "activity"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/m;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v3

    const/4 v1, 0x0

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    iget v5, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, v3, :cond_4

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_1
    if-eqz v1, :cond_2

    const-string v0, "Process: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v0, "PID: "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->e(Lcom/google/android/gms/car/m;)Lcom/google/android/gms/car/dr;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->e(Lcom/google/android/gms/car/m;)Lcom/google/android/gms/car/dr;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/car/ExceptionParcel;

    invoke-direct {v1, p2}, Lcom/google/android/gms/car/ExceptionParcel;-><init>(Ljava/lang/Throwable;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/ExceptionParcel;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->g(Lcom/google/android/gms/car/m;)Lcom/google/android/gms/common/api/o;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->n(Lcom/google/android/gms/car/m;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-static {v0}, Lcom/google/android/gms/car/m;->n(Lcom/google/android/gms/car/m;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->a:Lcom/google/android/gms/car/m;

    invoke-static {v1}, Lcom/google/android/gms/car/m;->n(Lcom/google/android/gms/car/m;)V

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    invoke-static {v6}, Ljava/lang/System;->exit(I)V

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method
