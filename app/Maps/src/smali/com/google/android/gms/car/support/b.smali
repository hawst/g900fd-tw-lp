.class Lcom/google/android/gms/car/support/b;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/support/CarAppLayout;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/CarAppLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/car/support/b;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 6

    const/4 v5, 0x5

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->h:Ljava/util/concurrent/BlockingQueue;

    const-wide/16 v2, 0xa

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v4}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/o;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_3

    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/b;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    iget-object v0, v0, Lcom/google/android/gms/car/support/CarAppLayout;->h:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    const/4 v0, 0x1

    return v0

    :cond_1
    invoke-interface {v1}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/content/Intent;

    :try_start_2
    sget-object v2, Lcom/google/android/gms/car/a;->c:Lcom/google/android/gms/car/d;

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/car/d;->a(Lcom/google/android/gms/common/api/o;Landroid/content/Intent;)V
    :try_end_2
    .catch Lcom/google/android/gms/car/ao; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_0
    move-exception v0

    :goto_3
    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/gms/car/support/CarAppLayout;->a:Ljava/lang/String;

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_3

    :catch_3
    move-exception v0

    goto :goto_0
.end method
