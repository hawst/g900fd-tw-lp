.class public Lcom/google/android/gms/car/support/w;
.super Landroid/content/BroadcastReceiver;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Landroid/telephony/TelephonyManager;

.field c:Landroid/telephony/ServiceState;

.field d:Landroid/telephony/SignalStrength;

.field e:Landroid/content/Context;

.field f:Landroid/telephony/PhoneStateListener;

.field private g:I

.field private h:I

.field private final i:Landroid/net/wifi/WifiManager;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:I

.field private n:Z

.field private o:Lcom/google/android/gms/car/support/y;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/gms/car/support/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/car/support/y;)V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput v2, p0, Lcom/google/android/gms/car/support/w;->g:I

    iput v0, p0, Lcom/google/android/gms/car/support/w;->h:I

    iput v2, p0, Lcom/google/android/gms/car/support/w;->l:I

    iput v0, p0, Lcom/google/android/gms/car/support/w;->m:I

    iput-boolean v2, p0, Lcom/google/android/gms/car/support/w;->n:Z

    new-instance v0, Lcom/google/android/gms/car/support/x;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/support/x;-><init>(Lcom/google/android/gms/car/support/w;)V

    iput-object v0, p0, Lcom/google/android/gms/car/support/w;->f:Landroid/telephony/PhoneStateListener;

    iput-object p1, p0, Lcom/google/android/gms/car/support/w;->e:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/gms/car/support/w;->o:Lcom/google/android/gms/car/support/y;

    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/gms/car/support/w;->b:Landroid/telephony/TelephonyManager;

    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/gms/car/support/w;->i:Landroid/net/wifi/WifiManager;

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_on"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/w;->n:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/support/w;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/car/support/w;->b()V

    return-void
.end method

.method private b()V
    .locals 12

    const/4 v0, 0x0

    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x3

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/gms/car/support/w;->b:Landroid/telephony/TelephonyManager;

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v1

    if-nez v1, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/support/w;->g:I

    :cond_0
    :goto_0
    iput v0, p0, Lcom/google/android/gms/car/support/w;->g:I

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-object v1, v0

    move-object v2, v0

    move-object v3, v0

    move-object v4, v0

    :cond_2
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v10, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    invoke-static {v10, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_3

    sget-object v10, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "CellInfo: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    instance-of v10, v0, Landroid/telephony/CellInfoCdma;

    if-eqz v10, :cond_4

    check-cast v0, Landroid/telephony/CellInfoCdma;

    move-object v4, v0

    goto :goto_1

    :cond_4
    instance-of v10, v0, Landroid/telephony/CellInfoGsm;

    if-eqz v10, :cond_5

    check-cast v0, Landroid/telephony/CellInfoGsm;

    move-object v3, v0

    goto :goto_1

    :cond_5
    instance-of v10, v0, Landroid/telephony/CellInfoLte;

    if-eqz v10, :cond_6

    check-cast v0, Landroid/telephony/CellInfoLte;

    move-object v2, v0

    goto :goto_1

    :cond_6
    instance-of v10, v0, Landroid/telephony/CellInfoWcdma;

    if-eqz v10, :cond_7

    check-cast v0, Landroid/telephony/CellInfoWcdma;

    move-object v1, v0

    goto :goto_1

    :cond_7
    sget-object v10, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    invoke-static {v10, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v10

    if-eqz v10, :cond_2

    sget-object v10, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Registered cellInfo is unrecognized type "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-nez v0, :cond_d

    move v0, v8

    :goto_2
    if-eqz v0, :cond_e

    if-eqz v4, :cond_e

    invoke-virtual {v4}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthCdma;->getLevel()I

    move-result v0

    :cond_9
    :goto_3
    if-nez v0, :cond_a

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthWcdma;->getLevel()I

    move-result v0

    :cond_a
    if-nez v0, :cond_c

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v8

    :goto_4
    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    if-eqz v0, :cond_20

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-nez v0, :cond_20

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v1}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    move-result v1

    const/16 v2, -0x4b

    if-lt v0, v2, :cond_10

    const/4 v0, 0x4

    :goto_5
    const/16 v2, -0x5a

    if-lt v1, v2, :cond_14

    const/4 v1, 0x4

    :goto_6
    if-ge v0, v1, :cond_18

    :goto_7
    sget-object v1, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_c

    sget-object v1, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCdmaLevel="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_c
    :goto_8
    sget-object v1, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getCellSignalLevel = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_d
    move v0, v5

    goto/16 :goto_2

    :cond_e
    if-eqz v2, :cond_21

    invoke-virtual {v2}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthLte;->getLevel()I

    move-result v0

    :goto_9
    if-nez v0, :cond_9

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/CellSignalStrengthGsm;->getLevel()I

    move-result v0

    goto/16 :goto_3

    :cond_f
    move v0, v5

    goto :goto_4

    :cond_10
    const/16 v2, -0x55

    if-lt v0, v2, :cond_11

    move v0, v6

    goto :goto_5

    :cond_11
    const/16 v2, -0x5f

    if-lt v0, v2, :cond_12

    move v0, v7

    goto :goto_5

    :cond_12
    const/16 v2, -0x64

    if-lt v0, v2, :cond_13

    move v0, v8

    goto :goto_5

    :cond_13
    move v0, v5

    goto :goto_5

    :cond_14
    const/16 v2, -0x6e

    if-lt v1, v2, :cond_15

    move v1, v6

    goto :goto_6

    :cond_15
    const/16 v2, -0x82

    if-lt v1, v2, :cond_16

    move v1, v7

    goto :goto_6

    :cond_16
    const/16 v2, -0x96

    if-lt v1, v2, :cond_17

    move v1, v8

    goto :goto_6

    :cond_17
    move v1, v5

    goto :goto_6

    :cond_18
    move v0, v1

    goto :goto_7

    :cond_19
    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->d:Landroid/telephony/SignalStrength;

    invoke-virtual {v0}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v0

    if-le v0, v7, :cond_1a

    const/16 v1, 0x63

    if-ne v0, v1, :cond_1c

    :cond_1a
    :goto_a
    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1b

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "getGsmLevel="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1b
    move v0, v5

    goto/16 :goto_8

    :cond_1c
    const/16 v1, 0xc

    if-lt v0, v1, :cond_1d

    const/4 v5, 0x4

    goto :goto_a

    :cond_1d
    const/16 v1, 0x8

    if-lt v0, v1, :cond_1e

    move v5, v6

    goto :goto_a

    :cond_1e
    const/4 v1, 0x5

    if-lt v0, v1, :cond_1f

    move v5, v7

    goto :goto_a

    :cond_1f
    move v5, v8

    goto :goto_a

    :cond_20
    move v0, v5

    goto/16 :goto_7

    :cond_21
    move v0, v5

    goto :goto_9
.end method


# virtual methods
.method final a()V
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/car/support/w;->b()V

    iget-object v2, p0, Lcom/google/android/gms/car/support/w;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "airplane_mode_on"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/w;->n:Z

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/w;->l:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/support/w;->a(Z)V

    return-void
.end method

.method a(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->o:Lcom/google/android/gms/car/support/y;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/w;->j:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/w;->k:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/car/support/w;->m:I

    iget v1, p0, Lcom/google/android/gms/car/support/w;->l:I

    if-ne v0, v1, :cond_2

    if-eqz p1, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->o:Lcom/google/android/gms/car/support/y;

    iget v1, p0, Lcom/google/android/gms/car/support/w;->l:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/support/y;->setWifiSignalLevel(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/w;->l:I

    iput v0, p0, Lcom/google/android/gms/car/support/w;->m:I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->c:Landroid/telephony/ServiceState;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->c:Landroid/telephony/ServiceState;

    invoke-virtual {v0}, Landroid/telephony/ServiceState;->getState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    iget v0, p0, Lcom/google/android/gms/car/support/w;->h:I

    if-nez v0, :cond_4

    if-eqz p1, :cond_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->o:Lcom/google/android/gms/car/support/y;

    invoke-interface {v0, v2}, Lcom/google/android/gms/car/support/y;->setCellSignalLevel(I)V

    iput v2, p0, Lcom/google/android/gms/car/support/w;->h:I

    goto :goto_0

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/w;->n:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->o:Lcom/google/android/gms/car/support/y;

    invoke-interface {v0}, Lcom/google/android/gms/car/support/y;->setAirplaneMode()V

    goto :goto_0

    :cond_6
    iget v0, p0, Lcom/google/android/gms/car/support/w;->h:I

    iget v1, p0, Lcom/google/android/gms/car/support/w;->g:I

    if-ne v0, v1, :cond_7

    if-eqz p1, :cond_0

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->o:Lcom/google/android/gms/car/support/y;

    iget v1, p0, Lcom/google/android/gms/car/support/w;->g:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/support/y;->setCellSignalLevel(I)V

    iget v0, p0, Lcom/google/android/gms/car/support/w;->g:I

    iput v0, p0, Lcom/google/android/gms/car/support/w;->h:I

    goto :goto_0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/support/w;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "onReceive intent:"

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v3, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v0, "wifi_state"

    const/4 v3, 0x4

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v4, :cond_4

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/support/w;->j:Z

    :cond_2
    :goto_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/support/w;->a(Z)V

    :cond_3
    return-void

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    const-string v3, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    iget-boolean v3, p0, Lcom/google/android/gms/car/support/w;->k:Z

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_7

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/w;->k:Z

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/w;->k:Z

    if-eqz v0, :cond_2

    if-nez v3, :cond_2

    const-string v0, "wifiInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiInfo;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->i:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    :cond_6
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getRssi()I

    move-result v0

    invoke-static {v0, v5}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/w;->l:I

    goto :goto_1

    :cond_7
    move v1, v2

    goto :goto_2

    :cond_8
    const-string v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "newRssi"

    const/16 v1, -0xc8

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0, v5}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/support/w;->l:I

    goto :goto_1

    :cond_9
    const-string v3, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-direct {p0}, Lcom/google/android/gms/car/support/w;->b()V

    goto :goto_1

    :cond_a
    const-string v3, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/support/w;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "airplane_mode_on"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v1, :cond_b

    :goto_3
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/w;->n:Z

    goto :goto_1

    :cond_b
    move v1, v2

    goto :goto_3
.end method
