.class public Lcom/google/android/gms/people/accountswitcherview/p;
.super Landroid/widget/BaseAdapter;
.source "PG"


# static fields
.field private static final f:I


# instance fields
.field public a:Lcom/google/android/gms/people/accountswitcherview/g;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Lcom/google/android/gms/people/accountswitcherview/a;

.field private g:Lcom/google/android/gms/people/accountswitcherview/t;

.field private h:Lcom/google/android/gms/people/accountswitcherview/r;

.field private i:I

.field private j:Landroid/view/LayoutInflater;

.field private k:I

.field private l:Landroid/content/Context;

.field private m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/people/model/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const v0, 0x7f040019

    sput v0, Lcom/google/android/gms/people/accountswitcherview/p;->f:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/google/android/gms/people/accountswitcherview/t;Lcom/google/android/gms/people/accountswitcherview/r;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 74
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 75
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    .line 76
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    .line 77
    iput-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->c:Z

    .line 78
    iput-object p1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->l:Landroid/content/Context;

    .line 79
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    :goto_0
    iput p2, p0, Lcom/google/android/gms/people/accountswitcherview/p;->i:I

    .line 80
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->j:Landroid/view/LayoutInflater;

    .line 81
    if-eqz p3, :cond_1

    :goto_1
    iput-object p3, p0, Lcom/google/android/gms/people/accountswitcherview/p;->g:Lcom/google/android/gms/people/accountswitcherview/t;

    .line 82
    iput-object p4, p0, Lcom/google/android/gms/people/accountswitcherview/p;->h:Lcom/google/android/gms/people/accountswitcherview/r;

    .line 83
    new-array v0, v1, [I

    const v1, 0x7f0100ff

    aput v1, v0, v3

    .line 87
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 88
    iget v1, v1, Landroid/util/TypedValue;->data:I

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0091

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 89
    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->k:I

    .line 91
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 92
    new-instance v0, Lcom/google/android/gms/people/accountswitcherview/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->e:Lcom/google/android/gms/people/accountswitcherview/a;

    .line 93
    return-void

    .line 79
    :cond_0
    sget p2, Lcom/google/android/gms/people/accountswitcherview/p;->f:I

    goto :goto_0

    .line 81
    :cond_1
    new-instance p3, Lcom/google/android/gms/people/accountswitcherview/q;

    invoke-direct {p3, p0}, Lcom/google/android/gms/people/accountswitcherview/q;-><init>(Lcom/google/android/gms/people/accountswitcherview/p;)V

    goto :goto_1
.end method

.method public static a(Ljava/util/List;Lcom/google/android/gms/people/model/a;Lcom/google/android/gms/people/model/a;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/people/model/a;",
            ">;",
            "Lcom/google/android/gms/people/model/a;",
            "Lcom/google/android/gms/people/model/a;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/people/model/a;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 338
    .line 340
    if-eqz p2, :cond_2

    invoke-interface {p2}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v1

    move-object v5, v1

    .line 342
    :goto_0
    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 344
    :goto_1
    const/4 v0, 0x0

    move v3, v2

    move v4, v2

    move v2, v0

    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 345
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/a;

    .line 346
    if-gez v4, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    move v4, v2

    .line 349
    :cond_0
    if-gez v3, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v2

    .line 344
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    move-object v5, v0

    .line 340
    goto :goto_0

    :cond_3
    move-object v1, v0

    .line 342
    goto :goto_1

    .line 353
    :cond_4
    if-ltz v4, :cond_5

    .line 354
    invoke-interface {p0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 356
    :cond_5
    if-gez v3, :cond_6

    if-eqz v1, :cond_6

    .line 357
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 358
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 360
    :cond_6
    return-object p0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/people/model/a;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 292
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/p;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne p1, v1, :cond_1

    .line 295
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/a;

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/people/model/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 261
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->d:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/a;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/p;->notifyDataSetChanged()V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/p;->notifyDataSetChanged()V

    .line 262
    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->e:Lcom/google/android/gms/people/accountswitcherview/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/accountswitcherview/a;->a(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    goto :goto_1
.end method

.method public getCount()I
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 286
    iget-boolean v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lcom/google/android/gms/people/accountswitcherview/p;->c:Z

    if-eqz v3, :cond_2

    :goto_1
    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    .line 287
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    :cond_0
    add-int/2addr v0, v2

    return v0

    :cond_1
    move v0, v2

    .line 286
    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/p;->a(I)Lcom/google/android/gms/people/model/a;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 3

    .prologue
    const-wide/16 v0, -0x1

    .line 300
    iget-boolean v2, p0, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/p;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-wide v0

    .line 302
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/gms/people/accountswitcherview/p;->c:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/p;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    if-ne p1, v2, :cond_2

    .line 303
    const-wide/16 v0, -0x2

    goto :goto_0

    .line 305
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/a;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/people/accountswitcherview/p;->getCount()I

    move-result v0

    .line 146
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    if-eqz v1, :cond_0

    add-int/lit8 v1, v0, -0x1

    if-ne p1, v1, :cond_0

    .line 147
    const/4 v0, 0x2

    .line 153
    :goto_0
    return v0

    .line 149
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->c:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    if-eqz v1, :cond_1

    add-int/lit8 v1, v0, -0x2

    if-eq p1, v1, :cond_2

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->b:Z

    if-nez v1, :cond_3

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_3

    .line 151
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 153
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 115
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/p;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 117
    if-nez p2, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->j:Landroid/view/LayoutInflater;

    const v1, 0x7f040089

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 135
    :cond_0
    :goto_0
    return-object p2

    .line 120
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/p;->getItemViewType(I)I

    move-result v0

    if-ne v0, v7, :cond_2

    .line 122
    if-nez p2, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->j:Landroid/view/LayoutInflater;

    const v1, 0x7f04001b

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_0

    .line 126
    :cond_2
    if-nez p2, :cond_3

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->j:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/gms/people/accountswitcherview/p;->i:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 129
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/accountswitcherview/p;->a(I)Lcom/google/android/gms/people/model/a;

    move-result-object v1

    .line 130
    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/p;->a:Lcom/google/android/gms/people/accountswitcherview/g;

    iget-object v0, p0, Lcom/google/android/gms/people/accountswitcherview/p;->g:Lcom/google/android/gms/people/accountswitcherview/t;

    iget-object v3, p0, Lcom/google/android/gms/people/accountswitcherview/p;->h:Lcom/google/android/gms/people/accountswitcherview/r;

    .line 132
    iget v3, p0, Lcom/google/android/gms/people/accountswitcherview/p;->k:I

    .line 130
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_5

    invoke-interface {v0, p2}, Lcom/google/android/gms/people/accountswitcherview/t;->a(Landroid/view/View;)Lcom/google/android/gms/people/accountswitcherview/s;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_1
    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/s;->b:Landroid/widget/ImageView;

    if-eqz v4, :cond_4

    if-eqz v2, :cond_4

    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/s;->b:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v1}, Lcom/google/android/gms/people/model/a;->i()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/s;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/accountswitcherview/g;->a(Landroid/widget/ImageView;)V

    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/s;->b:Landroid/widget/ImageView;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, Lcom/google/android/gms/people/model/a;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/android/gms/people/accountswitcherview/g;->a(Landroid/widget/ImageView;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_4
    :goto_2
    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/s;->a:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/s;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/s;->a:Landroid/widget/TextView;

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/s;->a:Landroid/widget/TextView;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v0, Lcom/google/android/gms/people/accountswitcherview/s;->a:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/people/accountswitcherview/p;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0089

    new-array v4, v7, [Ljava/lang/Object;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/accountswitcherview/s;

    goto :goto_1

    :cond_6
    iget-object v4, v0, Lcom/google/android/gms/people/accountswitcherview/s;->b:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/people/accountswitcherview/g;->a(Landroid/widget/ImageView;)V

    iget-object v2, v0, Lcom/google/android/gms/people/accountswitcherview/s;->b:Landroid/widget/ImageView;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v1}, Lcom/google/android/gms/people/model/a;->b()Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/a;->f()Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/accountswitcherview/g;->a(Landroid/content/Context;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x3

    return v0
.end method
