.class public Lcom/google/android/apps/gmm/v/bd;
.super Lcom/google/android/apps/gmm/v/ai;
.source "PG"


# instance fields
.field public a:F

.field private b:F

.field private c:F

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->l:Lcom/google/android/apps/gmm/v/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 33
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/bd;->d:Z

    .line 38
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/bd;->e:Z

    .line 45
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/v/bd;->a:F

    .line 57
    return-void
.end method

.method public constructor <init>(F)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/bd;-><init>()V

    .line 75
    iput p1, p0, Lcom/google/android/apps/gmm/v/bd;->a:F

    .line 76
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/bd;-><init>()V

    .line 66
    iput p1, p0, Lcom/google/android/apps/gmm/v/bd;->b:F

    iput p2, p0, Lcom/google/android/apps/gmm/v/bd;->c:F

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/bd;->d:Z

    .line 67
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 2

    .prologue
    .line 89
    check-cast p2, Lcom/google/android/apps/gmm/v/bd;

    .line 91
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/bd;->d:Z

    if-eqz v0, :cond_2

    .line 92
    if-eqz p2, :cond_0

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/bd;->d:Z

    if-nez v0, :cond_1

    .line 93
    :cond_0
    const v0, 0x8037

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 95
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/v/bd;->b:F

    iget v1, p0, Lcom/google/android/apps/gmm/v/bd;->c:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPolygonOffset(FF)V

    .line 98
    :cond_2
    if-eqz p2, :cond_3

    iget v0, p2, Lcom/google/android/apps/gmm/v/bd;->a:F

    iget v1, p0, Lcom/google/android/apps/gmm/v/bd;->a:F

    sub-float/2addr v0, v1

    .line 104
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x38d1b717    # 1.0E-4f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    .line 105
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/v/bd;->a:F

    invoke-static {v0}, Landroid/opengl/GLES20;->glLineWidth(F)V

    .line 107
    :cond_4
    return-void
.end method

.method final b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 1

    .prologue
    .line 111
    check-cast p2, Lcom/google/android/apps/gmm/v/bd;

    .line 113
    if-eqz p2, :cond_0

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/bd;->d:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/bd;->d:Z

    if-eqz v0, :cond_1

    .line 114
    :cond_0
    const v0, 0x8037

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 117
    :cond_1
    if-nez p2, :cond_2

    .line 125
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Landroid/opengl/GLES20;->glLineWidth(F)V

    .line 127
    :cond_2
    return-void
.end method
