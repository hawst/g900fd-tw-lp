.class public Lcom/google/android/apps/gmm/startpage/f/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/e/f;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public final b:Lcom/google/android/apps/gmm/startpage/d/d;

.field public c:Ljava/lang/Runnable;

.field public d:Lcom/google/android/apps/gmm/startpage/e/h;

.field public e:Lcom/google/android/apps/gmm/util/b/h;

.field public f:Z

.field public g:Lcom/google/android/apps/gmm/cardui/g/c;

.field public h:Lcom/google/android/apps/gmm/shared/net/k;

.field public i:Lcom/google/android/apps/gmm/startpage/e/g;

.field public j:Lcom/google/android/apps/gmm/startpage/e/l;

.field private final k:Lcom/google/android/apps/gmm/cardui/g/b;

.field private final l:Lcom/google/android/apps/gmm/r/a/a;

.field private m:Lcom/google/android/apps/gmm/login/d/c;

.field private n:Lcom/google/android/libraries/curvular/c/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/i;->c:Ljava/lang/Runnable;

    .line 56
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->f:Z

    .line 60
    iput-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/i;->h:Lcom/google/android/apps/gmm/shared/net/k;

    .line 62
    iput-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/i;->i:Lcom/google/android/apps/gmm/startpage/e/g;

    .line 64
    iput-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/i;->j:Lcom/google/android/apps/gmm/startpage/e/l;

    .line 70
    new-instance v1, Lcom/google/android/apps/gmm/startpage/f/j;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/startpage/f/j;-><init>(Lcom/google/android/apps/gmm/startpage/f/i;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/i;->n:Lcom/google/android/libraries/curvular/c/h;

    .line 81
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 82
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/f/i;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 83
    new-instance v1, Lcom/google/android/apps/gmm/startpage/f/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/startpage/f/m;-><init>(Lcom/google/android/apps/gmm/startpage/f/i;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/i;->k:Lcom/google/android/apps/gmm/cardui/g/b;

    .line 86
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/startpage/d/d;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 87
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v1

    sget-object v2, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    if-eq v1, v2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v1

    sget-object v2, Lcom/google/o/h/a/dq;->j:Lcom/google/o/h/a/dq;

    if-ne v1, v2, :cond_2

    .line 88
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v1

    sget-object v2, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    if-ne v1, v2, :cond_1

    const/4 v0, 0x1

    .line 89
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/startpage/g;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/startpage/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 94
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/g;->a(Z)Lcom/google/android/apps/gmm/r/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    .line 100
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/startpage/f/k;-><init>(Lcom/google/android/apps/gmm/startpage/f/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->m:Lcom/google/android/apps/gmm/login/d/c;

    .line 116
    return-void

    .line 97
    :cond_2
    iput-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 177
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/startpage/e/c;)V
    .locals 2

    .prologue
    .line 279
    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/i;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/startpage/f/p;-><init>(Lcom/google/android/apps/gmm/startpage/d/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->d:Lcom/google/android/apps/gmm/startpage/e/h;

    .line 280
    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 286
    invoke-static {p1}, Lcom/google/android/apps/gmm/startpage/u;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 287
    if-eqz v0, :cond_0

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/i;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Ljava/lang/String;)V

    .line 290
    :cond_0
    return-void
.end method

.method public final b()Lcom/google/android/apps/gmm/cardui/g/b;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->k:Lcom/google/android/apps/gmm/cardui/g/b;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/startpage/e/h;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->d:Lcom/google/android/apps/gmm/startpage/e/h;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/startpage/e/g;
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/f/i;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 186
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->i:Lcom/google/android/apps/gmm/startpage/e/g;

    goto :goto_0
.end method

.method public final e()Lcom/google/android/apps/gmm/startpage/e/l;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->j:Lcom/google/android/apps/gmm/startpage/e/l;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/startpage/e/j;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/r/a/a;->a()Lcom/google/android/apps/gmm/startpage/e/j;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/t;
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 202
    :goto_0
    return-object v0

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    .line 202
    invoke-interface {v0}, Lcom/google/android/apps/gmm/r/a/a;->b()Lcom/google/android/apps/gmm/base/l/a/t;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/apps/gmm/startpage/e/k;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 208
    :goto_0
    return-object v0

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->l:Lcom/google/android/apps/gmm/r/a/a;

    .line 208
    invoke-interface {v0}, Lcom/google/android/apps/gmm/r/a/a;->c()Lcom/google/android/apps/gmm/startpage/e/k;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()Lcom/google/android/apps/gmm/login/d/c;
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->m:Lcom/google/android/apps/gmm/login/d/c;

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/c/h;
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/i;->n:Lcom/google/android/libraries/curvular/c/h;

    return-object v0
.end method
