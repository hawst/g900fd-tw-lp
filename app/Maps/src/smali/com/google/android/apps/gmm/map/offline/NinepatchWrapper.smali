.class public Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field a:J


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 170
    invoke-static {}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->nativeInitClass()Z

    .line 173
    return-void
.end method

.method private constructor <init>(J)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    .line 38
    return-void
.end method

.method public static a(Landroid/graphics/Bitmap;I)Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 50
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 54
    :cond_0
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 55
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    .line 56
    if-eqz v3, :cond_1

    if-nez v7, :cond_2

    :cond_1
    move-object v0, v8

    .line 76
    :goto_0
    return-object v0

    .line 59
    :cond_2
    mul-int v0, v3, v7

    new-array v1, v0, [I

    move-object v0, p0

    move v4, v2

    move v5, v2

    move v6, v3

    .line 60
    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    .line 65
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v0

    .line 66
    if-nez v0, :cond_3

    .line 67
    new-array v0, v2, [B

    .line 71
    :cond_3
    invoke-static {v1, v3, v7, v0, p1}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->nativeCreateNinepatch([III[BI)J

    move-result-wide v2

    .line 73
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-nez v0, :cond_4

    move-object v0, v8

    .line 74
    goto :goto_0

    .line 76
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;-><init>(J)V

    goto :goto_0
.end method

.method static native nativeCalculateContentRegion(JII)[I
.end method

.method static native nativeCalculateStretchedDimensions(JII)[I
.end method

.method private static native nativeCreateNinepatch([III[BI)J
.end method

.method private static native nativeDestroyNinepatch(J)V
.end method

.method private static native nativeInitClass()Z
.end method

.method static native nativeRenderNinepatch(JII)[I
.end method


# virtual methods
.method public close()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 158
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 159
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->nativeDestroyNinepatch(J)V

    .line 161
    :cond_0
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    .line 162
    return-void
.end method

.method protected finalize()V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->close()V

    .line 154
    return-void
.end method
