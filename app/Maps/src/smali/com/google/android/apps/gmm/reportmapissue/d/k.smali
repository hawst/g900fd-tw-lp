.class Lcom/google/android/apps/gmm/reportmapissue/d/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/reportmapissue/c/g;


# instance fields
.field final a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

.field private final b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/reportmapissue/a/g;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    .line 18
    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->b:Ljava/lang/Runnable;

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->a:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 28
    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 45
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 43
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->a:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/g;->b:Ljava/lang/String;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/k;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 72
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method
