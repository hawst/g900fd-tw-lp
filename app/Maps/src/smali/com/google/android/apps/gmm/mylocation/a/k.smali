.class public Lcom/google/android/apps/gmm/mylocation/a/k;
.super Lcom/google/android/apps/gmm/mylocation/a/j;
.source "PG"


# instance fields
.field private c:J

.field private d:Z

.field private e:J

.field private f:Lcom/google/android/apps/gmm/map/b/a/ab;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private g:Lcom/google/android/apps/gmm/map/b/a/a;

.field private h:Lcom/google/android/apps/gmm/map/b/a/a;

.field private i:Lcom/google/android/apps/gmm/map/b/a/a;

.field private j:Lcom/google/android/apps/gmm/map/b/a/a;

.field private k:Z

.field private final l:Lcom/google/android/apps/gmm/mylocation/g/a;

.field private m:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/a/j;-><init>()V

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/a;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/a;-><init>(D)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->l:Lcom/google/android/apps/gmm/mylocation/g/a;

    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    add-int/lit8 v5, v0, -0x1

    .line 280
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 281
    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 282
    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 283
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 286
    const/4 v0, 0x0

    move v3, v0

    move v4, v1

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_0

    .line 287
    invoke-virtual {p0, v3, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 288
    add-int/lit8 v1, v3, 0x1

    invoke-virtual {p0, v1, v7}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 289
    invoke-static {v6, v7, p1, v8}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v1

    .line 290
    cmpg-float v9, v1, v2

    if-gez v9, :cond_1

    .line 293
    invoke-static {v6, v7, p1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->b(I)F

    move-result v2

    mul-float/2addr v0, v2

    add-float/2addr v0, v4

    .line 295
    :goto_1
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->b(I)F

    move-result v2

    add-float/2addr v4, v2

    .line 286
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    .line 297
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v1

    div-float/2addr v0, v1

    return v0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private a(DLcom/google/android/apps/gmm/mylocation/g/a;)V
    .locals 15

    .prologue
    .line 200
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-nez v2, :cond_0

    .line 202
    new-instance v12, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->h:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide/from16 v2, p1

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v2

    double-to-int v13, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->i:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide/from16 v2, p1

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-direct {v12, v13, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 203
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->j:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide/from16 v2, p1

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v2

    double-to-float v2, v2

    .line 204
    move-object/from16 v0, p3

    iput-object v12, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 205
    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    .line 214
    :goto_0
    return-void

    .line 208
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v4, v2, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide/from16 v2, p1

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v2

    .line 209
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 210
    iget-object v5, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    double-to-float v2, v2

    invoke-virtual {v5, v2, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FLcom/google/android/apps/gmm/map/b/a/y;)I

    move-result v2

    .line 211
    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 212
    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->c(I)F

    move-result v2

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 0

    .prologue
    .line 303
    return-void
.end method

.method public final declared-synchronized b(J)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 79
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->m:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 91
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 84
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->d:Z

    if-eqz v0, :cond_2

    .line 85
    iput-wide p1, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->c:J

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->d:Z

    .line 89
    :cond_2
    iget-wide v0, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->c:J

    sub-long v0, p1, v0

    long-to-double v0, v0

    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double/2addr v0, v2

    .line 90
    iget-object v2, p0, Lcom/google/android/apps/gmm/mylocation/a/k;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/a/k;->a(DLcom/google/android/apps/gmm/mylocation/g/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 91
    const/4 v0, 0x1

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/r/b/a;)Z
    .locals 22

    .prologue
    .line 97
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->e:J

    sub-long/2addr v2, v4

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double v4, v2, v4

    .line 101
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->d:Z

    .line 102
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->e:J

    .line 105
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v2, v3, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->h:Lcom/google/android/apps/gmm/map/b/a/a;

    if-nez v3, :cond_5

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/a;

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v6, v6

    invoke-direct {v3, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/a;-><init>(D)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->h:Lcom/google/android/apps/gmm/map/b/a/a;

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v2

    invoke-direct {v3, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/a;-><init>(D)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->i:Lcom/google/android/apps/gmm/map/b/a/a;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/a;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v3

    float-to-double v6, v3

    invoke-direct {v2, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/a;-><init>(D)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->j:Lcom/google/android/apps/gmm/map/b/a/a;

    .line 107
    :goto_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_8

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_9

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    const/4 v3, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    .line 115
    :goto_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_a

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->l:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v18, v2

    .line 117
    :goto_3
    if-eqz v18, :cond_2

    .line 120
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_b

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    :goto_4
    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v2, v3, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 121
    :cond_0
    move-object/from16 v0, v18

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/mylocation/a/k;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v20, v0

    .line 123
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v3

    float-to-double v6, v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v2

    mul-double/2addr v2, v6

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v6

    float-to-double v6, v6

    div-double v6, v2, v6

    .line 126
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_c

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    :goto_5
    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-nez v2, :cond_d

    .line 131
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    add-double v8, v20, v6

    move-wide/from16 v4, v20

    move-wide v10, v6

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V

    .line 149
    :cond_2
    :goto_6
    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 153
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->m:Z

    if-nez v2, :cond_3

    .line 154
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/mylocation/a/k;->a(DLcom/google/android/apps/gmm/mylocation/g/a;)V

    .line 156
    :cond_3
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->m:Z

    .line 158
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->k:Z

    .line 159
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->k:Z

    if-nez v2, :cond_4

    .line 163
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->h:Lcom/google/android/apps/gmm/map/b/a/a;

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v14

    const-wide/16 v16, 0x0

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v8

    const-wide/16 v10, 0x0

    move-object v3, v12

    move-wide v4, v14

    move-wide/from16 v6, v16

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V

    .line 164
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->i:Lcom/google/android/apps/gmm/map/b/a/a;

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v14

    const-wide/16 v16, 0x0

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v8

    const-wide/16 v10, 0x0

    move-object v3, v12

    move-wide v4, v14

    move-wide/from16 v6, v16

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V

    .line 165
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->j:Lcom/google/android/apps/gmm/map/b/a/a;

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v14

    const-wide/16 v16, 0x0

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v8

    const-wide/16 v10, 0x0

    move-object v3, v12

    move-wide v4, v14

    move-wide/from16 v6, v16

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V

    .line 166
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v14

    const-wide/16 v16, 0x0

    const-wide/16 v2, 0x0

    iget-wide v4, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v2 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v8

    const-wide/16 v10, 0x0

    move-object v3, v12

    move-wide v4, v14

    move-wide/from16 v6, v16

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V

    .line 168
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v2

    .line 105
    :cond_5
    :try_start_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v6

    float-to-double v6, v6

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v6, v6

    invoke-static {v3, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->a(FF)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->h:Lcom/google/android/apps/gmm/map/b/a/a;

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v6, v6

    iget v8, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v8, v8

    add-double/2addr v6, v8

    iget v8, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-double v8, v8

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDD)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->i:Lcom/google/android/apps/gmm/map/b/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v2

    iget v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v2

    add-double/2addr v6, v8

    iget v2, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v2

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDD)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->j:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v12, v2, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v4 .. v13}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v6

    float-to-double v0, v6

    move-wide/from16 v16, v0

    :goto_7
    const-wide v6, 0x4066800000000000L    # 180.0

    sub-double v6, v16, v6

    cmpg-double v6, v2, v6

    if-gez v6, :cond_6

    const-wide v6, 0x4076800000000000L    # 360.0

    add-double/2addr v2, v6

    goto :goto_7

    :cond_6
    :goto_8
    const-wide v6, 0x4066800000000000L    # 180.0

    add-double v6, v6, v16

    cmpl-double v6, v2, v6

    if-lez v6, :cond_7

    const-wide v6, 0x4076800000000000L    # 360.0

    sub-double/2addr v2, v6

    goto :goto_8

    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->j:Lcom/google/android/apps/gmm/map/b/a/a;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->j:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v6, v12, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v8, v12, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v10, v12, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v12, v12, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    invoke-static/range {v4 .. v13}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v10

    const-wide/16 v14, 0x0

    move-object/from16 v7, v18

    move-wide v8, v2

    move-wide/from16 v12, v16

    invoke-virtual/range {v7 .. v15}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 97
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 107
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 112
    :cond_9
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->b:Lcom/google/android/apps/gmm/mylocation/g/a;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    goto/16 :goto_2

    .line 115
    :cond_a
    const/4 v2, 0x0

    move-object/from16 v18, v2

    goto/16 :goto_3

    .line 120
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_4

    .line 126
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_5

    .line 133
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v18

    if-eq v0, v2, :cond_e

    .line 137
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v12, v2, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v14, v2, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v0, v2, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide/from16 v16, v0

    move-wide v8, v4

    invoke-static/range {v8 .. v17}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDDDD)D

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    iget-wide v10, v8, Lcom/google/android/apps/gmm/map/b/a/a;->a:D

    iget-wide v12, v8, Lcom/google/android/apps/gmm/map/b/a/a;->b:D

    iget-wide v14, v8, Lcom/google/android/apps/gmm/map/b/a/a;->c:D

    iget-wide v0, v8, Lcom/google/android/apps/gmm/map/b/a/a;->d:D

    move-wide/from16 v16, v0

    move-wide v8, v4

    invoke-static/range {v8 .. v17}, Lcom/google/android/apps/gmm/map/b/a/a;->b(DDDDD)D

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v8

    float-to-double v8, v8

    mul-double/2addr v4, v8

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    double-to-float v2, v2

    invoke-virtual {v9, v2, v8}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(FLcom/google/android/apps/gmm/map/b/a/y;)I

    move-object/from16 v0, v18

    invoke-static {v0, v8}, Lcom/google/android/apps/gmm/mylocation/a/k;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v10, v2

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v2

    float-to-double v2, v2

    div-double v12, v4, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    add-double v14, v20, v6

    move-wide/from16 v16, v6

    invoke-virtual/range {v9 .. v17}, Lcom/google/android/apps/gmm/map/b/a/a;->c(DDDD)V

    goto/16 :goto_6

    .line 143
    :cond_e
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/mylocation/a/k;->g:Lcom/google/android/apps/gmm/map/b/a/a;

    add-double v12, v20, v6

    move-wide v10, v4

    move-wide v14, v6

    invoke-virtual/range {v9 .. v15}, Lcom/google/android/apps/gmm/map/b/a/a;->a(DDD)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_6
.end method
