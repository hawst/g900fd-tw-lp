.class public Lcom/google/android/apps/gmm/map/t;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public b:Lcom/google/android/apps/gmm/map/c/a;

.field public final c:Lcom/google/android/apps/gmm/map/o;

.field public d:Lcom/google/android/apps/gmm/map/ah;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public e:Z

.field public final f:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/b/b;",
            ">;"
        }
    .end annotation
.end field

.field h:Z

.field public i:Landroid/graphics/Point;

.field public j:Z

.field public k:Lcom/google/android/apps/gmm/map/b/a/t;

.field public l:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/t;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/map/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/y;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/t;-><init>(Lcom/google/android/apps/gmm/map/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;)V

    .line 117
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t;->g:Ljava/util/Map;

    .line 110
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    .line 111
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/t;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    .line 112
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Lcom/google/android/apps/gmm/map/o/ar;Landroid/widget/TextView;Lcom/google/android/apps/gmm/map/s;Lcom/google/android/apps/gmm/map/b/a/ai;ZLcom/google/android/apps/gmm/map/x;)Landroid/view/View;
    .locals 6

    .prologue
    .line 181
    if-nez p7, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v1

    .line 183
    if-nez v1, :cond_2

    .line 184
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    move-object v1, p1

    move-object v3, p4

    move-object v4, p5

    move v5, p6

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/o;->a(Landroid/view/LayoutInflater;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/s;Lcom/google/android/apps/gmm/map/b/a/ai;Z)V

    .line 191
    new-instance v0, Lcom/google/android/apps/gmm/map/ah;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    .line 192
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->v()Lcom/google/android/apps/gmm/map/b/c;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v1, v2, p7}, Lcom/google/android/apps/gmm/map/ah;-><init>(Lcom/google/android/apps/gmm/map/b/c;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/x;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/o/ar;)V

    .line 196
    if-eqz p3, :cond_1

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, p3}, Lcom/google/android/apps/gmm/map/o;->a(Landroid/widget/TextView;)V

    .line 212
    :cond_1
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/t;->e:Z

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 206
    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 207
    if-eqz v0, :cond_1

    .line 208
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v0, :cond_0

    .line 556
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    .line 557
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v4

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    .line 556
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Lcom/google/android/apps/gmm/map/f/o;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v0

    .line 559
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lcom/google/maps/a/a;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 707
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 711
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    .line 712
    sget-object v1, Lcom/google/android/apps/gmm/map/t;->a:Ljava/lang/String;

    const-string v3, "GoogleMap must be created before accessing camera"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v3, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 715
    :goto_1
    return-object v0

    :cond_0
    move v1, v2

    .line 711
    goto :goto_0

    .line 715
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    if-nez v1, :cond_2

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    iget v2, v1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    iget v1, v1, Lcom/google/android/apps/gmm/v/n;->K:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->x:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    invoke-static {v0, v2, v1, v3, v4}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;FFII)Lcom/google/maps/a/a;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 14

    .prologue
    .line 622
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 623
    :goto_0
    if-nez v0, :cond_1

    .line 624
    const-string v0, ""

    .line 634
    :goto_1
    return-object v0

    .line 622
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0

    .line 627
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 628
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->y()Lcom/google/android/apps/gmm/map/ak;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/ak;->a()Lcom/google/android/apps/gmm/map/b/a/ba;

    move-result-object v1

    .line 632
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 634
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v12, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    sub-double/2addr v6, v8

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v12, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->max(DD)D

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(DD)D

    move-result-wide v8

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v10, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v12, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(DD)D

    move-result-wide v10

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v12, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/ba;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v12, v13, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    sub-double v0, v8, v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x6a

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ll="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "spn="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V
    .locals 3
    .param p2    # Lcom/google/android/apps/gmm/map/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 342
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t;->j:Z

    if-nez v0, :cond_1

    .line 343
    if-eqz p2, :cond_0

    .line 344
    sget-object v0, Lcom/google/android/apps/gmm/map/t;->a:Ljava/lang/String;

    .line 345
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/p;->c()V

    .line 354
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    if-eqz p3, :cond_2

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/s/c;

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/s/c;-><init>(Lcom/google/android/apps/gmm/map/s/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 352
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/t;->a:Ljava/lang/String;

    .line 353
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/t;->h:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/map/t;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "animating camera: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/map/t;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "requesting post layout animating camera: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Lcom/google/android/apps/gmm/map/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/apps/gmm/map/v;-><init>(Lcom/google/android/apps/gmm/map/t;Landroid/view/View;Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)V
    .locals 8

    .prologue
    const/16 v7, 0x1c

    const/4 v1, 0x0

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->k:Lcom/google/android/apps/gmm/map/b/a/t;

    if-ne v0, p2, :cond_1

    .line 296
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    if-eqz p2, :cond_3

    .line 287
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/t;->a:Lcom/google/android/apps/gmm/map/b/a/ag;

    if-nez v0, :cond_2

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x8

    const/16 v3, 0x1a

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ag;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/ag;-><init>(Lcom/google/e/a/a/a/b;)V

    iput-object v2, p2, Lcom/google/android/apps/gmm/map/b/a/t;->a:Lcom/google/android/apps/gmm/map/b/a/ag;

    :cond_2
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/t;->a:Lcom/google/android/apps/gmm/map/b/a/ag;

    .line 288
    :goto_1
    if-eqz v0, :cond_4

    .line 289
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ag;->a:Lcom/google/e/a/a/a/b;

    move-object v2, v0

    .line 290
    :goto_2
    if-eqz p2, :cond_7

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/t;->b:Ljava/lang/String;

    if-nez v0, :cond_6

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v4, :cond_6

    aget-object v5, v3, v1

    const/4 v0, 0x1

    invoke-virtual {v5, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v6, "atk"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    invoke-virtual {v5, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/t;->b:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/t;->b:Ljava/lang/String;

    .line 291
    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1, p1, v2, v0}, Lcom/google/android/apps/gmm/map/o;->a(Ljava/lang/String;Lcom/google/e/a/a/a/b;Ljava/lang/String;)V

    .line 293
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/t;->k:Lcom/google/android/apps/gmm/map/b/a/t;

    .line 294
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t;->l:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 287
    goto :goto_1

    :cond_4
    move-object v2, v1

    .line 289
    goto :goto_2

    .line 290
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_6
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/b/a/t;->b:Ljava/lang/String;

    goto :goto_4

    :cond_7
    move-object v0, v1

    goto :goto_4
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V
    .locals 2

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 508
    if-eqz v0, :cond_0

    .line 509
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/o;->c(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->g:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/map/o;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 514
    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 649
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 650
    const-string v0, ""

    .line 676
    :goto_1
    return-object v0

    .line 649
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0

    .line 653
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "http://maps.google.com/"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 654
    const-string v0, "maps?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 659
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_4

    .line 661
    const-string v0, "s"

    .line 662
    const-string v2, "q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 664
    :try_start_0
    const-string v2, "UTF-8"

    invoke-static {p1, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 669
    :goto_3
    const-string v2, "&"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 675
    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 676
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 659
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 667
    :catch_0
    move-exception v2

    sget-object v2, Lcom/google/android/apps/gmm/map/t;->a:Ljava/lang/String;

    goto :goto_3

    .line 672
    :cond_4
    const-string v0, ""

    goto :goto_4
.end method
