.class public Lcom/google/android/apps/gmm/aa/a/b;
.super Lcom/google/android/apps/gmm/aa/a/e;
.source "PG"


# instance fields
.field private final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private c:Lcom/google/android/apps/gmm/navigation/g/b/f;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/aa/a/d;Lcom/google/android/apps/gmm/navigation/a/d;)V
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-direct {p0, p2, p3, v0}, Lcom/google/android/apps/gmm/aa/a/e;-><init>(Lcom/google/android/apps/gmm/aa/a/d;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 66
    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->i:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v3, :cond_0

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_4

    .line 69
    :cond_2
    :goto_1
    return v0

    .line 66
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 69
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/navigation/b/f;->X_()Z

    move-result v1

    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/apps/gmm/l;->dM:I

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 40
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 41
    return-void

    .line 40
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 45
    if-eqz p1, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->f:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 46
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v2, :cond_0

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v0

    .line 48
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/o/a/c;->d(Z)Z

    .line 50
    :cond_2
    return-void

    .line 45
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->g:Lcom/google/android/apps/gmm/aa/a/a/a;

    goto :goto_0

    .line 46
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(Z)I
    .locals 3

    .prologue
    .line 54
    if-eqz p1, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->d:Lcom/google/android/apps/gmm/aa/a/a/a;

    .line 55
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v2, :cond_0

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_4

    .line 56
    const/4 v0, -0x1

    .line 60
    :goto_2
    return v0

    .line 54
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->e:Lcom/google/android/apps/gmm/aa/a/a/a;

    goto :goto_0

    .line 55
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 58
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v0

    .line 59
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    .line 60
    if-eqz p1, :cond_5

    sget v0, Lcom/google/android/apps/gmm/l;->dN:I

    goto :goto_2

    :cond_5
    sget v0, Lcom/google/android/apps/gmm/l;->dD:I

    goto :goto_2
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->r:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v2, :cond_0

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->onBackPressed()V

    .line 78
    :cond_2
    return-void

    .line 75
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->t:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v2, :cond_0

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/f;->d()Z

    .line 85
    :cond_2
    return-void

    .line 82
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 89
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->s:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v3, :cond_0

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    move v0, v1

    .line 111
    :goto_1
    return v0

    .line 89
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 93
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->b()Z

    move-result v0

    .line 96
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_5

    .line 97
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/navigation/b/f;->c()Z

    .line 98
    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/gmm/l;->dL:I

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_1

    .line 102
    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v2, :cond_7

    .line 106
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/mylocation/b/i;->c()V

    .line 108
    if-eqz v0, :cond_6

    sget v0, Lcom/google/android/apps/gmm/l;->dL:I

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v1

    .line 111
    goto :goto_1
.end method

.method public final e()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 116
    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->z:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v3, :cond_0

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_4

    .line 127
    :cond_2
    :goto_1
    return v0

    .line 116
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 119
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v1, :cond_2

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 121
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v2, :cond_2

    .line 122
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v2

    .line 123
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 122
    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/navigation/b/f;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, -0x21524111

    goto :goto_1
.end method

.method public final f()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 132
    sget-object v1, Lcom/google/android/apps/gmm/aa/a/a/a;->A:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v3, :cond_0

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v1, v1, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v1, :cond_3

    :cond_1
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_4

    .line 143
    :cond_2
    :goto_1
    return v0

    .line 132
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 135
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v1, :cond_2

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 137
    array-length v2, v1

    if-lez v2, :cond_2

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v2

    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/navigation/b/f;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)Z

    move-result v1

    if-eqz v1, :cond_2

    const v0, -0x21524111

    goto :goto_1
.end method

.method public final g()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 148
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->u:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v3, :cond_0

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    const/4 v2, 0x0

    .line 150
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/feedback/a/e;->a(ZLcom/google/android/apps/gmm/feedback/a/d;)V

    .line 152
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 148
    goto :goto_0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 156
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->C:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-nez v2, :cond_0

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/aa/a/a/a;->G:Z

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/f;->c()Z

    .line 159
    :cond_2
    return-void

    .line 156
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
