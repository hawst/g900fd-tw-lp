.class Lcom/google/android/apps/gmm/map/internal/d/az;
.super Lcom/google/android/apps/gmm/map/internal/d/aw;
.source "PG"


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 0

    .prologue
    .line 325
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/aw;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 326
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 348
    const/16 v0, 0x800

    return v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/ac;Landroid/content/res/Resources;Ljava/util/Locale;Ljava/io/File;ZLcom/google/android/apps/gmm/map/internal/d/r;Lcom/google/android/apps/gmm/map/internal/d/ag;)Lcom/google/android/apps/gmm/map/internal/d/as;
    .locals 2

    .prologue
    .line 338
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/al;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/az;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v0, p1, v1, p3, p4}, Lcom/google/android/apps/gmm/map/internal/d/al;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/b/a/ai;Ljava/util/Locale;Ljava/io/File;)V

    return-object v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/d/ac;)Lcom/google/android/apps/gmm/map/internal/d/b/as;
    .locals 5

    .prologue
    .line 353
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/az;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 354
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/internal/d/ac;->C()I

    move-result v3

    shr-int/lit8 v3, v3, 0x3

    const/16 v4, 0x80

    mul-int/lit8 v3, v3, 0x12

    invoke-static {v4, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/16 v4, 0x24

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/aq;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;I)V

    return-object v0
.end method

.method final b()Lcom/google/android/apps/gmm/map/internal/d/b/ap;
    .locals 1

    .prologue
    .line 343
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/ba;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/ba;-><init>()V

    return-object v0
.end method

.method public final c()B
    .locals 1

    .prologue
    .line 359
    const/16 v0, 0xa

    return v0
.end method
