.class public Lcom/google/android/apps/gmm/search/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/didyoumean/a/a;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/search/f;

.field private final b:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/search/f;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/r;->a:Lcom/google/android/apps/gmm/search/f;

    .line 28
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/f;->c:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/r;->b:Lcom/google/b/c/cv;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->b:Lcom/google/b/c/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->b:Lcom/google/b/c/cv;

    .line 34
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->b:Lcom/google/b/c/cv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 37
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->a:Lcom/google/android/apps/gmm/search/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/f;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 7
    .param p1    # Landroid/content/Context;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 53
    if-eqz p1, :cond_0

    .line 54
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v4

    .line 55
    if-eqz v4, :cond_0

    iget-object v0, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->a:Lcom/google/android/apps/gmm/search/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/f;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v0, :cond_2

    .line 57
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->a:Lcom/google/android/apps/gmm/search/f;

    .line 58
    iget-object v0, v0, Lcom/google/android/apps/gmm/search/f;->a:Ljava/lang/String;

    iget-object v5, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/i;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->a:Lcom/google/android/apps/gmm/search/f;

    .line 59
    iget-object v0, v0, Lcom/google/android/apps/gmm/search/f;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v5, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_0
    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    .line 60
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    const/4 v0, 0x1

    .line 61
    iget-object v5, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v0, v5, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    .line 62
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 66
    iget-object v0, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v0

    .line 68
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/r;->a()Ljava/lang/String;

    move-result-object v5

    move-object v4, v3

    .line 67
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Ljava/lang/String;)V

    .line 80
    :cond_0
    :goto_1
    return-void

    .line 59
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_2
    sget-object v0, Lcom/google/b/f/t;->gp:Lcom/google/b/f/t;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 72
    iget-object v1, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v2, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/aq;

    new-instance v2, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/gmm/search/r;->a:Lcom/google/android/apps/gmm/search/f;

    .line 74
    iget-object v4, v4, Lcom/google/android/apps/gmm/search/f;->a:Ljava/lang/String;

    if-eqz v4, :cond_3

    const-string v5, "\\s+"

    const-string v6, " "

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/apps/gmm/search/aj;->a:Ljava/lang/String;

    .line 75
    :cond_3
    iput-object v0, v2, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    .line 72
    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->b:Lcom/google/b/c/cv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->b:Lcom/google/b/c/cv;

    .line 44
    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/r;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 47
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
