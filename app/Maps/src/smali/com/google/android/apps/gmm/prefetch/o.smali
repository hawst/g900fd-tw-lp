.class Lcom/google/android/apps/gmm/prefetch/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/prefetch/n;

.field private b:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/prefetch/n;)V
    .locals 1

    .prologue
    .line 136
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 139
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->b:I

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    .line 143
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    monitor-enter v1

    .line 144
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/n;->a:Ljava/lang/String;

    const-string v2, "onServiceConnected"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 147
    instance-of v0, p2, Lcom/google/android/apps/gmm/prefetch/e;

    if-nez v0, :cond_1

    .line 170
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/n;->a:Ljava/lang/String;

    .line 171
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x38

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not an instance of BasePrefetcherService.LocalBinder"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 170
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->b:I

    if-lt v0, v4, :cond_0

    .line 174
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/n;->a:Ljava/lang/String;

    const-string v2, "%s is not an instance of BasePrefetcherService.LocalBinder after %d tries."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 176
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/apps/gmm/prefetch/o;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 173
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 203
    :goto_0
    return-void

    .line 180
    :cond_0
    const/4 v0, 0x5

    :try_start_1
    iget v2, p0, Lcom/google/android/apps/gmm/prefetch/o;->b:I

    shr-int/2addr v0, v2

    .line 181
    sget-object v2, Lcom/google/android/apps/gmm/prefetch/n;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Sleeping for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "s before reconnecting."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 182
    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 186
    :goto_1
    :try_start_2
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->b:I

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/n;->b:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetch/n;->a()V

    .line 189
    monitor-exit v1

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 192
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    check-cast p2, Lcom/google/android/apps/gmm/prefetch/e;

    iget-object v2, p2, Lcom/google/android/apps/gmm/prefetch/e;->a:Lcom/google/android/apps/gmm/prefetch/b;

    iput-object v2, v0, Lcom/google/android/apps/gmm/prefetch/n;->c:Lcom/google/android/apps/gmm/prefetch/b;

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/n;->d:Lcom/google/android/apps/gmm/prefetch/a/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetch/n;->e:Ljava/util/Queue;

    if-eqz v0, :cond_2

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/prefetch/n;->g:Z

    if-eqz v0, :cond_3

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/prefetch/n;->e:Ljava/util/Queue;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v3, v3, Lcom/google/android/apps/gmm/prefetch/n;->f:Ljava/util/Queue;

    iget-object v4, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    .line 196
    iget-object v4, v4, Lcom/google/android/apps/gmm/prefetch/n;->d:Lcom/google/android/apps/gmm/prefetch/a/c;

    .line 195
    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/prefetch/n;->a(Ljava/util/Queue;Ljava/util/Queue;Lcom/google/android/apps/gmm/prefetch/a/c;)V

    .line 200
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/prefetch/n;->e:Ljava/util/Queue;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/prefetch/n;->d:Lcom/google/android/apps/gmm/prefetch/a/c;

    .line 203
    :cond_2
    monitor-exit v1

    goto :goto_0

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/prefetch/n;->e:Ljava/util/Queue;

    iget-object v3, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    iget-object v3, v3, Lcom/google/android/apps/gmm/prefetch/n;->d:Lcom/google/android/apps/gmm/prefetch/a/c;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/prefetch/n;->a(Ljava/util/Queue;Lcom/google/android/apps/gmm/prefetch/a/c;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 212
    sget-object v0, Lcom/google/android/apps/gmm/prefetch/n;->a:Ljava/lang/String;

    const-string v1, "onServiceDisconnected"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/o;->a:Lcom/google/android/apps/gmm/prefetch/n;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/prefetch/n;->c:Lcom/google/android/apps/gmm/prefetch/b;

    .line 214
    return-void
.end method
