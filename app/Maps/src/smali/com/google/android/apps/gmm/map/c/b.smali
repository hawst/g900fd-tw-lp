.class public Lcom/google/android/apps/gmm/map/c/b;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/map/c/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/map/c/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/c/b;->a:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/util/WeakHashMap;

    invoke-direct {v0}, Ljava/util/WeakHashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/c/b;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    sget-object v0, Lcom/google/android/apps/gmm/map/c/b;->b:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/c/a;

    .line 44
    if-nez v0, :cond_0

    .line 45
    sget-object v0, Lcom/google/android/apps/gmm/map/c/b;->b:Ljava/util/Map;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/c/a;

    .line 49
    :cond_0
    if-nez v0, :cond_2

    instance-of v1, p0, Lcom/google/android/apps/gmm/map/c/a;

    if-eqz v1, :cond_2

    .line 50
    instance-of v0, p0, Landroid/app/Application;

    if-eqz v0, :cond_1

    .line 51
    sget-object v0, Lcom/google/android/apps/gmm/map/c/b;->a:Ljava/lang/String;

    const-string v1, "Use MapEnvironmentUtil.register() instead of implementing MapEnvironment in Application."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    :goto_0
    check-cast p0, Lcom/google/android/apps/gmm/map/c/a;

    .line 59
    :goto_1
    return-object p0

    .line 54
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/c/b;->a:Ljava/lang/String;

    const-string v1, "Use MapEnvironmentUtil.register() to bind MapEnvironment to a Context."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    move-object p0, v0

    .line 59
    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/apps/gmm/map/c/b;->b:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    return-void
.end method
