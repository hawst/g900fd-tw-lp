.class public Lcom/google/android/apps/gmm/map/internal/c/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field public final a:I

.field public final b:J

.field public final c:Lcom/google/android/apps/gmm/map/b/a/j;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public final e:[Lcom/google/android/apps/gmm/map/internal/c/z;

.field public final f:Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final g:Ljava/lang/String;

.field public final h:F

.field public final i:Z

.field public final j:[I

.field final k:Lcom/google/android/apps/gmm/map/indoor/d/g;

.field private final l:I

.field private final m:I

.field private final n:Ljava/lang/String;

.field private final o:I


# direct methods
.method public constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFI[ILcom/google/android/apps/gmm/map/indoor/d/g;)V
    .locals 19

    .prologue
    .line 92
    const/16 v17, 0x0

    move-object/from16 v1, p0

    move-wide/from16 v2, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    move-object/from16 v11, p10

    move/from16 v12, p11

    move/from16 v13, p12

    move/from16 v14, p13

    move-object/from16 v15, p14

    move-object/from16 v16, p15

    invoke-direct/range {v1 .. v17}, Lcom/google/android/apps/gmm/map/internal/c/ad;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFI[ILcom/google/android/apps/gmm/map/indoor/d/g;Z)V

    .line 108
    return-void
.end method

.method public constructor <init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFI[ILcom/google/android/apps/gmm/map/indoor/d/g;Z)V
    .locals 3

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->b:J

    .line 131
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->l:I

    .line 132
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->m:I

    .line 133
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 134
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 135
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 136
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->n:Ljava/lang/String;

    .line 137
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 138
    iput-object p10, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->g:Ljava/lang/String;

    .line 140
    iput p11, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->o:I

    .line 141
    iput p12, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->h:F

    .line 142
    move/from16 v0, p13

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->a:I

    .line 143
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->j:[I

    .line 144
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->k:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 145
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->i:Z

    .line 146
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Lcom/google/android/apps/gmm/map/internal/c/ay;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 163
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/map/internal/c/ad;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;ZLjava/util/Collection;)V

    .line 164
    return-void
.end method

.method protected static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;ZLjava/util/Collection;)V
    .locals 42
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/DataInput;",
            "Lcom/google/android/apps/gmm/map/internal/c/bs;",
            "Lcom/google/android/apps/gmm/map/internal/c/ay;",
            "Z",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 184
    const/4 v4, 0x1

    .line 188
    if-nez p3, :cond_0

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    const/16 v6, 0xc

    if-lt v5, v6, :cond_0

    .line 189
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    .line 191
    :cond_0
    new-array v0, v4, [Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v40, v0

    .line 192
    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_1

    .line 193
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    move-object/from16 v0, p0

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v6

    aput-object v6, v40, v5

    .line 192
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 197
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v6

    new-instance v41, Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-object/from16 v0, v41

    invoke-direct {v0, v6, v5}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 200
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 201
    new-array v12, v6, [Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 202
    const/4 v5, 0x0

    :goto_1
    if-ge v5, v6, :cond_2

    .line 203
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v41

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/z;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v7

    aput-object v7, v12, v5

    .line 202
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 207
    :cond_2
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v16

    .line 210
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40800000    # 4.0f

    div-float v17, v5, v6

    .line 213
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v18

    .line 217
    const/4 v10, 0x0

    .line 218
    and-int/lit8 v5, v18, 0x1

    if-eqz v5, :cond_4

    const/4 v5, 0x1

    :goto_2
    if-eqz v5, :cond_5

    .line 219
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v10

    .line 225
    :cond_3
    :goto_3
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 226
    new-array v0, v6, [I

    move-object/from16 v20, v0

    .line 227
    const/4 v5, 0x0

    :goto_4
    if-ge v5, v6, :cond_7

    .line 228
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v7

    aput v7, v20, v5

    .line 227
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 218
    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    .line 220
    :cond_5
    and-int/lit8 v5, v18, 0x2

    if-eqz v5, :cond_6

    const/4 v5, 0x1

    :goto_5
    if-eqz v5, :cond_3

    .line 221
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->e()Lcom/google/android/apps/gmm/map/b/a/k;

    move-result-object v10

    goto :goto_3

    .line 220
    :cond_6
    const/4 v5, 0x0

    goto :goto_5

    .line 231
    :cond_7
    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->b:Lcom/google/android/apps/gmm/map/internal/c/n;

    .line 232
    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v6, :cond_8

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_8
    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/n;->e:Lcom/google/android/apps/gmm/map/indoor/d/g;

    move-object/from16 v21, v0

    .line 234
    const/4 v5, 0x0

    move/from16 v39, v5

    :goto_6
    move/from16 v0, v39

    if-ge v0, v4, :cond_12

    .line 235
    if-eqz p3, :cond_e

    .line 236
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/ae;

    .line 237
    move-object/from16 v0, p2

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v6, v6

    .line 238
    move-object/from16 v0, p2

    iget v8, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 239
    move-object/from16 v0, p2

    iget v9, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    aget-object v11, v40, v39

    .line 243
    move-object/from16 v0, v41

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v13, :cond_9

    const/4 v13, 0x0

    .line 244
    :goto_7
    move-object/from16 v0, v41

    iget v14, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    .line 245
    move-object/from16 v0, v41

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v15, :cond_b

    const/4 v15, 0x0

    .line 249
    :goto_8
    and-int/lit8 v19, v18, 0x4

    if-eqz v19, :cond_c

    const/16 v19, 0x1

    :goto_9
    if-eqz v19, :cond_d

    const/high16 v19, 0x3f000000    # 0.5f

    :goto_a
    const/16 v22, 0x0

    invoke-direct/range {v5 .. v22}, Lcom/google/android/apps/gmm/map/internal/c/ae;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFIF[ILcom/google/android/apps/gmm/map/indoor/d/g;Z)V

    .line 236
    move-object/from16 v0, p4

    invoke-interface {v0, v5}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 234
    :goto_b
    add-int/lit8 v5, v39, 0x1

    move/from16 v39, v5

    goto :goto_6

    .line 243
    :cond_9
    move-object/from16 v0, v41

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v14, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v14, v14

    if-nez v14, :cond_a

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v13

    goto :goto_7

    :cond_a
    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v14, 0x0

    aget-object v13, v13, v14

    goto :goto_7

    .line 245
    :cond_b
    move-object/from16 v0, v41

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v15, v15, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto :goto_8

    .line 249
    :cond_c
    const/16 v19, 0x0

    goto :goto_9

    :cond_d
    const/16 v19, 0x0

    goto :goto_a

    .line 254
    :cond_e
    new-instance v23, Lcom/google/android/apps/gmm/map/internal/c/ad;

    .line 255
    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v0, v5

    move-wide/from16 v24, v0

    .line 256
    move-object/from16 v0, p2

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    move/from16 v26, v0

    .line 257
    move-object/from16 v0, p2

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    move/from16 v27, v0

    aget-object v29, v40, v39

    .line 261
    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v5, :cond_f

    const/16 v31, 0x0

    .line 262
    :goto_c
    move-object/from16 v0, v41

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    move/from16 v32, v0

    .line 263
    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v5, :cond_11

    const/16 v33, 0x0

    :goto_d
    move-object/from16 v28, v10

    move-object/from16 v30, v12

    move/from16 v34, v16

    move/from16 v35, v17

    move/from16 v36, v18

    move-object/from16 v37, v20

    move-object/from16 v38, v21

    invoke-direct/range {v23 .. v38}, Lcom/google/android/apps/gmm/map/internal/c/ad;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFI[ILcom/google/android/apps/gmm/map/indoor/d/g;)V

    .line 254
    move-object/from16 v0, p4

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 261
    :cond_f
    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v6, v6

    if-nez v6, :cond_10

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v31

    goto :goto_c

    :cond_10
    iget-object v5, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v6, 0x0

    aget-object v31, v5, v6

    goto :goto_c

    .line 263
    :cond_11
    move-object/from16 v0, v41

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v0, v5, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    move-object/from16 v33, v0

    goto :goto_d

    .line 271
    :cond_12
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/ba;Lcom/google/android/apps/gmm/map/internal/c/bs;)[Lcom/google/android/apps/gmm/map/internal/c/ad;
    .locals 21

    .prologue
    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ba;->a()Lcom/google/maps/b/a/cu;

    move-result-object v2

    .line 276
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/maps/b/a/ba;->a:Lcom/google/maps/b/a/cz;

    iget v3, v3, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/maps/b/a/ba;->b:Lcom/google/maps/b/a/cy;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 275
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Lcom/google/maps/b/a/cu;ILcom/google/maps/b/a/cy;Lcom/google/android/apps/gmm/map/b/a/aw;)[Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v19

    .line 277
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/maps/b/a/ba;->f:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v2

    .line 278
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v3, v3

    if-nez v3, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v11

    .line 280
    :goto_0
    move-object/from16 v0, v19

    array-length v2, v0

    new-array v0, v2, [Lcom/google/android/apps/gmm/map/internal/c/ad;

    move-object/from16 v20, v0

    .line 281
    const/4 v2, 0x0

    :goto_1
    move-object/from16 v0, v19

    array-length v3, v0

    if-ge v2, v3, :cond_1

    .line 282
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/ad;

    const-wide/16 v4, 0x0

    .line 284
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/maps/b/a/ba;->g:Lcom/google/maps/b/a/cz;

    iget v6, v6, Lcom/google/maps/b/a/cz;->b:I

    .line 285
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/maps/b/a/ba;->h:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    const/4 v8, 0x0

    aget-object v9, v19, v2

    const/4 v10, 0x0

    new-array v10, v10, [Lcom/google/android/apps/gmm/map/internal/c/z;

    const/4 v12, 0x0

    const-string v13, ""

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    move/from16 v0, v17

    new-array v0, v0, [I

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-direct/range {v3 .. v18}, Lcom/google/android/apps/gmm/map/internal/c/ad;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Lcom/google/android/apps/gmm/map/internal/c/be;ILjava/lang/String;IFI[ILcom/google/android/apps/gmm/map/indoor/d/g;)V

    aput-object v3, v20, v2

    .line 281
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 278
    :cond_0
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v3, 0x0

    aget-object v11, v2, v3

    goto :goto_0

    .line 298
    :cond_1
    return-object v20
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 304
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->b:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 314
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->l:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 319
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->m:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 434
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 366
    const/16 v0, 0x8

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->o:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->j:[I

    return-object v0
.end method

.method public final k()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    shl-int/lit8 v0, v0, 0x2

    add-int/lit16 v4, v0, 0xa0

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    if-eqz v0, :cond_0

    .line 413
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v6, v5

    move v2, v1

    move v0, v1

    :goto_0
    if-ge v2, v6, :cond_1

    aget-object v3, v5, v2

    .line 414
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/z;->a()I

    move-result v3

    add-int/2addr v3, v0

    .line 413
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    .line 417
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 418
    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    add-int/lit8 v3, v2, 0x40

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->g:Ljava/lang/String;

    .line 419
    if-nez v2, :cond_3

    move v2, v1

    :goto_2
    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 420
    if-nez v3, :cond_4

    :goto_3
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    add-int/2addr v0, v4

    .line 422
    return v0

    .line 418
    :cond_2
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->d()I

    move-result v2

    goto :goto_1

    .line 419
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x4

    shl-int/lit8 v2, v2, 0x2

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x28

    goto :goto_2

    .line 420
    :cond_4
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 439
    new-instance v3, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "zGrade"

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->l:I

    .line 440
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "zWithinGrade"

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->m:I

    .line 441
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "labels[0]"

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v1, v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->e:[Lcom/google/android/apps/gmm/map/internal/c/z;

    const/4 v4, 0x0

    aget-object v1, v1, v4

    .line 442
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/z;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move-object v1, v2

    goto :goto_0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "name"

    .line 443
    new-instance v1, Lcom/google/b/a/al;

    invoke-direct {v1}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v1, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "style"

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 444
    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "rank"

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->o:I

    .line 445
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "shift"

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ad;->h:F

    .line 446
    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 447
    invoke-virtual {v3}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
