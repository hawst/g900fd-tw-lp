.class public Lcom/google/android/apps/gmm/n/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/p/e",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 46
    invoke-static {}, Lcom/google/b/c/hj;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/n/a;->a:Ljava/util/Map;

    .line 48
    invoke-static {}, Lcom/google/b/c/hj;->b()Ljava/util/LinkedHashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/n/a;->b:Ljava/util/Map;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/n/a;->c:Landroid/content/SharedPreferences;

    .line 121
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 163
    iget-object v1, p0, Lcom/google/android/apps/gmm/n/a;->c:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 164
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/n/a;->c:Landroid/content/SharedPreferences;

    .line 166
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/n/a;->c:Landroid/content/SharedPreferences;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/j/c;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/n/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/p/e;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/map/p/e;->a(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/SharedPreferences;)V

    goto :goto_0

    .line 167
    :cond_1
    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 0

    .prologue
    .line 138
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 139
    invoke-direct {p0}, Lcom/google/android/apps/gmm/n/a;->c()V

    .line 140
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 81
    invoke-direct {p0}, Lcom/google/android/apps/gmm/n/a;->c()V

    .line 119
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/n/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 133
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->h()V

    .line 134
    return-void
.end method
