.class public final enum Lcom/google/android/apps/gmm/directions/f/b/m;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/f/b/m;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum c:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum d:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum e:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum f:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum g:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum h:Lcom/google/android/apps/gmm/directions/f/b/m;

.field public static final enum i:Lcom/google/android/apps/gmm/directions/f/b/m;

.field private static final synthetic n:[Lcom/google/android/apps/gmm/directions/f/b/m;


# instance fields
.field public final j:I

.field public final k:Lcom/google/android/apps/gmm/directions/f/b/n;

.field public final l:[Lcom/google/android/apps/gmm/directions/f/b/q;

.field public final m:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "AVOID_HIGHWAYS"

    const/4 v2, 0x0

    sget v3, Lcom/google/android/apps/gmm/l;->fv:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "AVOID_TOLLS"

    const/4 v2, 0x1

    sget v3, Lcom/google/android/apps/gmm/l;->fw:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->b:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "PREFER_BUS"

    const/4 v2, 0x2

    sget v3, Lcom/google/android/apps/gmm/l;->nW:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->c:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "PREFER_SUBWAY"

    const/4 v2, 0x3

    sget v3, Lcom/google/android/apps/gmm/l;->nX:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/4 v6, 0x3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->d:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "PREFER_TRAIN"

    const/4 v2, 0x4

    sget v3, Lcom/google/android/apps/gmm/l;->nY:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/4 v6, 0x4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->e:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "PREFER_TRAM"

    const/4 v2, 0x5

    sget v3, Lcom/google/android/apps/gmm/l;->fG:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/4 v6, 0x5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->f:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "PREFER_ANY_RAIL"

    const/4 v2, 0x6

    sget v3, Lcom/google/android/apps/gmm/l;->fF:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/4 v6, 0x6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->g:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "SORT_OPTION"

    const/4 v2, 0x7

    sget v3, Lcom/google/android/apps/gmm/l;->fK:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->b:Lcom/google/android/apps/gmm/directions/f/b/n;

    .line 56
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/b/p;->values()[Lcom/google/android/apps/gmm/directions/f/b/p;

    move-result-object v5

    const/4 v6, 0x7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->h:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    const-string v1, "AVOID_FERRIES"

    const/16 v2, 0x8

    sget v3, Lcom/google/android/apps/gmm/l;->fu:I

    sget-object v4, Lcom/google/android/apps/gmm/directions/f/b/n;->a:Lcom/google/android/apps/gmm/directions/f/b/n;

    const/4 v5, 0x0

    const/16 v6, 0x8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/f/b/m;-><init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->i:Lcom/google/android/apps/gmm/directions/f/b/m;

    .line 20
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/f/b/m;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->b:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->c:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->d:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->e:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->f:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->g:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->h:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/directions/f/b/m;->i:Lcom/google/android/apps/gmm/directions/f/b/m;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->n:[Lcom/google/android/apps/gmm/directions/f/b/m;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IILcom/google/android/apps/gmm/directions/f/b/n;[Lcom/google/android/apps/gmm/directions/f/b/q;I)V
    .locals 0
    .param p5    # [Lcom/google/android/apps/gmm/directions/f/b/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/apps/gmm/directions/f/b/n;",
            "[",
            "Lcom/google/android/apps/gmm/directions/f/b/q;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput p3, p0, Lcom/google/android/apps/gmm/directions/f/b/m;->j:I

    .line 73
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/f/b/m;->k:Lcom/google/android/apps/gmm/directions/f/b/n;

    .line 74
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/f/b/m;->l:[Lcom/google/android/apps/gmm/directions/f/b/q;

    .line 75
    iput p6, p0, Lcom/google/android/apps/gmm/directions/f/b/m;->m:I

    .line 76
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/directions/f/b/m;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 97
    invoke-static {}, Lcom/google/android/apps/gmm/directions/f/b/m;->values()[Lcom/google/android/apps/gmm/directions/f/b/m;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 98
    iget v4, v0, Lcom/google/android/apps/gmm/directions/f/b/m;->m:I

    if-ne v4, p0, :cond_0

    .line 102
    :goto_1
    return-object v0

    .line 97
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 102
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/f/b/m;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/f/b/m;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/b/m;->n:[Lcom/google/android/apps/gmm/directions/f/b/m;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/f/b/m;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/f/b/m;

    return-object v0
.end method
