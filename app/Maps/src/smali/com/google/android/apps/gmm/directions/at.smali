.class Lcom/google/android/apps/gmm/directions/at;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V
    .locals 0

    .prologue
    .line 947
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/at;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 958
    .line 959
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->v:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/se;->d()Lcom/google/o/h/a/se;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/se;

    .line 960
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/at;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/av;->s()Lcom/google/android/apps/gmm/suggest/e/c;

    move-result-object v6

    .line 962
    iget-object v1, v0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/b/a/v;

    iget-object v1, v1, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/p;->d()Lcom/google/o/b/a/p;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/b/a/p;

    iget v1, v1, Lcom/google/o/b/a/p;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v2, 0x100

    if-ne v1, v2, :cond_0

    move v1, v4

    :goto_0
    if-eqz v1, :cond_3

    .line 964
    iget-object v1, v0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    invoke-virtual {v1}, Lcom/google/maps/g/a/je;->h()Ljava/lang/String;

    move-result-object v7

    .line 965
    iget-object v1, v0, Lcom/google/o/h/a/se;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/v;->d()Lcom/google/o/b/a/v;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/b/a/v;

    iget-object v1, v1, Lcom/google/o/b/a/v;->t:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/p;->d()Lcom/google/o/b/a/p;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/b/a/p;

    iget-object v2, v1, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_1

    move-object v1, v2

    check-cast v1, Ljava/lang/String;

    .line 964
    :goto_1
    invoke-static {v7, v1}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    move-object v2, v1

    .line 971
    :goto_2
    iget v1, v0, Lcom/google/o/h/a/se;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_4

    move v1, v4

    :goto_3
    if-eqz v1, :cond_9

    new-instance v4, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    .line 973
    iget-object v1, v0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_5

    check-cast v1, Ljava/lang/String;

    :goto_4
    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/z/b/f;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v1

    .line 974
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->g()Lcom/google/android/apps/gmm/util/b/b;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/util/b/b;->b:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v4, v1, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez v3, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move v1, v5

    .line 962
    goto :goto_0

    .line 965
    :cond_1
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    iput-object v3, v1, Lcom/google/o/b/a/p;->j:Ljava/lang/Object;

    :cond_2
    move-object v1, v3

    goto :goto_1

    .line 967
    :cond_3
    iget-object v1, v0, Lcom/google/o/h/a/se;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/je;->i()Lcom/google/maps/g/a/je;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/je;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/at;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/r/a/ap;->a(Lcom/google/maps/g/a/je;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    move-object v2, v1

    goto :goto_2

    :cond_4
    move v1, v5

    .line 971
    goto :goto_3

    .line 973
    :cond_5
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    iput-object v3, v0, Lcom/google/o/h/a/se;->g:Ljava/lang/Object;

    :cond_6
    move-object v1, v3

    goto :goto_4

    .line 974
    :cond_7
    iget v7, v4, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v4, Lcom/google/maps/g/ia;->a:I

    iput-object v3, v4, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 975
    :cond_8
    iget-object v1, v1, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v1}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hy;

    .line 977
    :goto_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/at;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v3

    .line 980
    :try_start_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/at;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    iget-object v0, v0, Lcom/google/o/h/a/se;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/r/b/a/afz;)V

    .line 981
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/at;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0, v2, v6, v1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/maps/g/hy;)V

    .line 982
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 983
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/at;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v5}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 984
    return-void

    .line 975
    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 982
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 954
    sget-object v0, Lcom/google/o/h/a/g;->x:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 955
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    const/high16 v1, 0x100000

    .line 950
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
