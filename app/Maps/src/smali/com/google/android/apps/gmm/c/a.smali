.class public Lcom/google/android/apps/gmm/c/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/c/a/a;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/android/apps/gmm/c/a/b;",
            "Lcom/google/android/apps/gmm/c/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/c/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lcom/google/android/apps/gmm/c/a/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 31
    const-class v0, Lcom/google/android/apps/gmm/c/a/b;

    .line 32
    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/c/a;->a:Ljava/util/EnumMap;

    .line 31
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    if-nez v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/c/a;->e()Lcom/google/android/apps/gmm/c/a/b;

    move-result-object v0

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/c/a/c;->c()Z

    move-result v1

    .line 96
    if-eqz v0, :cond_3

    .line 98
    if-nez v1, :cond_2

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/c/a;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/c/a;->a:Ljava/util/EnumMap;

    invoke-virtual {v2, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/c/a;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/c/a/c;->a(Landroid/view/View;)V

    goto :goto_0

    .line 104
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/c/a;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/c/a;->a:Ljava/util/EnumMap;

    invoke-virtual {v2, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 108
    :cond_3
    if-eqz v1, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/c/a/c;->b()V

    goto :goto_0
.end method

.method private e()Lcom/google/android/apps/gmm/c/a/b;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/apps/gmm/c/a/b;->values()[Lcom/google/android/apps/gmm/c/a/b;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 121
    iget-object v4, p0, Lcom/google/android/apps/gmm/c/a;->a:Ljava/util/EnumMap;

    invoke-virtual {v4, v0}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 125
    :goto_1
    return-object v0

    .line 120
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final Y_()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 47
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/activities/ak;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/apps/gmm/c/a;->d()V

    .line 58
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 3

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/c/a;->b:Lcom/google/android/libraries/curvular/ae;

    .line 41
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/c/a/b;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    invoke-direct {p0}, Lcom/google/android/apps/gmm/c/a;->d()V

    .line 73
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/c/a/b;Lcom/google/android/apps/gmm/c/a/d;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/gmm/c/a;->d()V

    .line 64
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/c/a/c;)V
    .locals 1

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/c/a/c;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-direct {p0}, Lcom/google/android/apps/gmm/c/a;->e()Lcom/google/android/apps/gmm/c/a/b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/c/a/c;->d()V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/c/a;->d()V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/c/a;->c:Lcom/google/android/apps/gmm/c/a/c;

    .line 155
    return-void
.end method
