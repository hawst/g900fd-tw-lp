.class Lcom/google/android/apps/gmm/util/b/ab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/util/b/aa;


# instance fields
.field private final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/libraries/curvular/ce;

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<*>;>;",
            "Lcom/google/android/libraries/curvular/ce;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput-object p1, p0, Lcom/google/android/apps/gmm/util/b/ab;->a:Ljava/lang/Class;

    .line 156
    iput-object p2, p0, Lcom/google/android/apps/gmm/util/b/ab;->b:Lcom/google/android/libraries/curvular/ce;

    .line 157
    iput p3, p0, Lcom/google/android/apps/gmm/util/b/ab;->c:I

    .line 158
    iput p4, p0, Lcom/google/android/apps/gmm/util/b/ab;->d:I

    .line 159
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<*>;>;"
        }
    .end annotation

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ab;->a:Ljava/lang/Class;

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/ce;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/b/ab;->b:Lcom/google/android/libraries/curvular/ce;

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/google/android/apps/gmm/util/b/ab;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Lcom/google/android/apps/gmm/util/b/ab;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method
