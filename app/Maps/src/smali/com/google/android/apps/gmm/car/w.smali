.class public Lcom/google/android/apps/gmm/car/w;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/car/y;

.field final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            "Lcom/google/android/apps/gmm/car/bk;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            "Lcom/google/android/apps/gmm/car/bk;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/car/bm;",
            "Lcom/google/android/apps/gmm/directions/f/c;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/base/a;

.field private g:I

.field private final h:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/y;)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/w;->c:Ljava/util/HashMap;

    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/w;->d:Ljava/util/HashMap;

    .line 70
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/w;->e:Ljava/util/HashMap;

    .line 244
    new-instance v0, Lcom/google/android/apps/gmm/car/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/x;-><init>(Lcom/google/android/apps/gmm/car/w;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/w;->h:Ljava/lang/Object;

    .line 76
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/base/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/w;->f:Lcom/google/android/apps/gmm/base/a;

    .line 77
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/y;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/w;->b:Lcom/google/android/apps/gmm/car/y;

    .line 78
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/directions/a/a;Lcom/google/android/apps/gmm/car/e/m;Lcom/google/android/apps/gmm/car/bm;ILcom/google/android/apps/gmm/car/e/t;)V
    .locals 3
    .param p4    # Lcom/google/android/apps/gmm/car/e/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 224
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "navigate to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/car/bn;->b:Lcom/google/android/apps/gmm/car/bn;

    if-eq v0, v1, :cond_0

    .line 226
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "navigate() called when navigation is not possible."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 230
    invoke-interface {p0}, Lcom/google/android/apps/gmm/directions/a/a;->c()V

    .line 231
    invoke-virtual {p1, p2, p3, p4}, Lcom/google/android/apps/gmm/car/e/m;->a(Lcom/google/android/apps/gmm/car/bm;ILcom/google/android/apps/gmm/car/e/t;)V

    .line 232
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "start() startCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 86
    iget v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    if-nez v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/w;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/w;->h:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 89
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    .line 90
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V
    .locals 12

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 123
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 124
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "prefetch to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/w;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v11

    .line 127
    if-nez v11, :cond_0

    .line 129
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 163
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_1

    move v0, v9

    :goto_1
    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 136
    new-array v8, v9, [F

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget-wide v4, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    aget v0, v8, v10

    .line 138
    float-to-double v0, v0

    const-wide v2, 0x4122ebc000000000L    # 620000.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 139
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v0, v10

    .line 135
    goto :goto_1

    .line 144
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/car/bk;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/car/bk;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/w;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    move-result v1

    const/4 v2, 0x3

    if-lt v1, v2, :cond_3

    .line 148
    sget-object v1, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/w;->d:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 153
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/w;->c:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 156
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    goto :goto_0

    .line 160
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/w;->b:Lcom/google/android/apps/gmm/car/y;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v11, v9, v1}, Lcom/google/android/apps/gmm/car/y;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/map/r/b/a;ZLcom/google/android/apps/gmm/directions/f/c;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;Lcom/google/android/apps/gmm/directions/f/c;)V
    .locals 3
    .param p3    # Lcom/google/android/apps/gmm/directions/f/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 185
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 186
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "fetch to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 188
    new-instance v0, Lcom/google/android/apps/gmm/car/bk;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/car/bk;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/w;->c:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 192
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 207
    :goto_0
    return-void

    .line 195
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/w;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    .line 199
    if-nez v0, :cond_1

    .line 201
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/w;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 206
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/w;->b:Lcom/google/android/apps/gmm/car/y;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v0, v2, p3}, Lcom/google/android/apps/gmm/car/y;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/map/r/b/a;ZLcom/google/android/apps/gmm/directions/f/c;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    .line 99
    sget-object v0, Lcom/google/android/apps/gmm/car/w;->a:Ljava/lang/String;

    iget v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "stop() startCount="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 100
    iget v0, p0, Lcom/google/android/apps/gmm/car/w;->g:I

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/w;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/w;->h:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 103
    :cond_0
    return-void
.end method
