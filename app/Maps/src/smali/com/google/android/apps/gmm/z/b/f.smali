.class public Lcom/google/android/apps/gmm/z/b/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/maps/g/ia;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    .line 25
    return-void
.end method

.method public constructor <init>(Lcom/google/maps/g/hy;)V
    .locals 1
    .param p1    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    if-nez p1, :cond_0

    .line 29
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    .line 33
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-static {p1}, Lcom/google/maps/g/hy;->a(Lcom/google/maps/g/hy;)Lcom/google/maps/g/ia;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/maps/g/hy;
    .locals 3

    .prologue
    .line 102
    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    array-length v0, v0

    if-gtz v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/b/f/bf;)Lcom/google/android/apps/gmm/z/b/f;
    .locals 3

    .prologue
    .line 79
    if-eqz p1, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/maps/g/ia;->g:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p1, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, v0, Lcom/google/maps/g/ia;->a:I

    .line 82
    :cond_1
    return-object p0
.end method

.method public final a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;
    .locals 4
    .param p1    # Lcom/google/b/f/cq;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 86
    if-eqz p1, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-static {}, Lcom/google/b/f/b;->newBuilder()Lcom/google/b/f/d;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/b/f/cq;->a()I

    move-result v2

    iget v3, v1, Lcom/google/b/f/d;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v1, Lcom/google/b/f/d;->a:I

    iput v2, v1, Lcom/google/b/f/d;->c:I

    iget-object v2, v0, Lcom/google/maps/g/ia;->e:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/b/f/d;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Lcom/google/maps/g/ia;->a:I

    .line 89
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 40
    if-eqz p1, :cond_1

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/g/ia;->a:I

    iput-object p1, v0, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 43
    :cond_1
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 47
    if-eqz p1, :cond_1

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/g/ia;->a:I

    iput-object p1, v0, Lcom/google/maps/g/ia;->d:Ljava/lang/Object;

    .line 50
    :cond_1
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 54
    if-eqz p1, :cond_1

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/g/ia;->a:I

    iput-object p1, v0, Lcom/google/maps/g/ia;->b:Ljava/lang/Object;

    .line 57
    :cond_1
    return-object p0
.end method
