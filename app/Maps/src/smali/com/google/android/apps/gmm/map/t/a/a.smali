.class public Lcom/google/android/apps/gmm/map/t/a/a;
.super Lcom/google/android/apps/gmm/v/c;
.source "PG"


# instance fields
.field public a:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/b/i;",
            ">;"
        }
    .end annotation
.end field

.field private b:F

.field private c:F


# direct methods
.method public constructor <init>(FF)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/c;-><init>()V

    .line 20
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/a/a;->a:Ljava/util/Collection;

    .line 29
    iput p1, p0, Lcom/google/android/apps/gmm/map/t/a/a;->c:F

    .line 30
    iput p2, p0, Lcom/google/android/apps/gmm/map/t/a/a;->b:F

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v0

    .line 52
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/v/c;->getTransformation(JLandroid/view/animation/Transformation;)Z

    iget v0, p0, Lcom/google/android/apps/gmm/v/c;->e:F

    .line 53
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    .line 55
    iget v2, p0, Lcom/google/android/apps/gmm/map/t/a/a;->c:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/a/a;->b:F

    mul-float/2addr v0, v2

    add-float/2addr v1, v0

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/a/a;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/i;

    .line 58
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/b/i;->a(F)V

    goto :goto_0

    .line 60
    :cond_0
    return-void
.end method
