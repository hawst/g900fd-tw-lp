.class Lcom/google/android/apps/gmm/map/internal/d/v;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/j;

.field final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/an;)V
    .locals 1

    .prologue
    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 429
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 430
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 431
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->b:Ljava/lang/String;

    .line 439
    :goto_0
    return-void

    .line 434
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->b:Ljava/lang/String;

    goto :goto_0

    .line 437
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->b:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 443
    if-ne p0, p1, :cond_1

    .line 453
    :cond_0
    :goto_0
    return v0

    .line 447
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/d/v;

    if-nez v2, :cond_2

    move v0, v1

    .line 448
    goto :goto_0

    .line 451
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/d/v;

    .line 452
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/v;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    move v2, v0

    :goto_1
    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/v;->b:Ljava/lang/String;

    .line 453
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v0

    :goto_2
    if-nez v2, :cond_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v1

    .line 452
    goto :goto_1

    :cond_7
    move v2, v1

    .line 453
    goto :goto_2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 458
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/v;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
