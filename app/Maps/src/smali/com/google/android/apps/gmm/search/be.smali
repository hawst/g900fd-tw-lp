.class public final Lcom/google/android/apps/gmm/search/be;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static a(Lcom/google/android/apps/gmm/map/b/a/q;Landroid/graphics/Rect;Landroid/graphics/Point;FLcom/google/android/apps/gmm/map/f/o;I)F
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 137
    .line 138
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    .line 137
    invoke-virtual {p4, v0}, Lcom/google/android/apps/gmm/map/f/o;->c(Lcom/google/android/apps/gmm/map/b/a/y;)[I

    move-result-object v0

    .line 139
    if-nez v0, :cond_0

    .line 140
    iget-object v0, p4, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 173
    :goto_0
    return v0

    .line 142
    :cond_0
    new-instance v1, Landroid/graphics/Point;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-direct {v1, v2, v0}, Landroid/graphics/Point;-><init>(II)V

    .line 158
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 159
    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, p2, Landroid/graphics/Point;->x:I

    sub-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    add-int/2addr v2, p5

    int-to-float v2, v2

    .line 160
    mul-float/2addr v2, p3

    div-float v0, v2, v0

    .line 164
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    .line 165
    iget v1, v1, Landroid/graphics/Point;->y:I

    iget v3, p2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    add-int/2addr v1, p5

    int-to-float v1, v1

    .line 166
    mul-float/2addr v1, p3

    div-float/2addr v1, v2

    .line 169
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 170
    cmpl-float v1, v0, p3

    if-lez v1, :cond_1

    .line 171
    invoke-virtual {p4, v0}, Lcom/google/android/apps/gmm/map/f/o;->b(F)F

    move-result v0

    goto :goto_0

    .line 173
    :cond_1
    iget-object v0, p4, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    goto :goto_0
.end method
