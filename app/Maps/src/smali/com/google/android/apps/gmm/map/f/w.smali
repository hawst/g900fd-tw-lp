.class public Lcom/google/android/apps/gmm/map/f/w;
.super Landroid/animation/ValueAnimator;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/f/t;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/f/t;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/w;->a:Lcom/google/android/apps/gmm/map/f/t;

    invoke-direct {p0}, Landroid/animation/ValueAnimator;-><init>()V

    return-void
.end method


# virtual methods
.method final a(J)V
    .locals 5

    .prologue
    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/w;->getStartDelay()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/w;->getDuration()J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 153
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 154
    invoke-super {p0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 162
    :goto_0
    return-void

    .line 158
    :cond_0
    long-to-double v2, p1

    long-to-double v0, v0

    div-double v0, v2, v0

    .line 159
    double-to-long v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/w;->getStartDelay()J

    move-result-wide v2

    mul-long/2addr v0, v2

    .line 160
    invoke-super {p0, v0, v1}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 161
    sub-long v0, p1, v0

    invoke-super {p0, v0, v1}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    goto :goto_0
.end method

.method public synthetic setDuration(J)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/w;->a:Lcom/google/android/apps/gmm/map/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/t;)V

    return-object p0
.end method

.method public synthetic setDuration(J)Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 117
    invoke-super {p0, p1, p2}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/w;->a:Lcom/google/android/apps/gmm/map/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/t;)V

    return-object p0
.end method

.method public setStartDelay(J)V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0, p1, p2}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/w;->a:Lcom/google/android/apps/gmm/map/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/t;)V

    .line 132
    return-void
.end method
