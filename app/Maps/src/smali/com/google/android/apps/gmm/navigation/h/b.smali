.class public Lcom/google/android/apps/gmm/navigation/h/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bq;


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/internal/c/bq;

.field private static final i:Lcom/google/android/apps/gmm/map/internal/c/bq;


# instance fields
.field b:Lcom/google/android/apps/gmm/map/r/a/w;

.field c:Lcom/google/android/apps/gmm/map/b/a/ab;

.field d:I

.field e:I

.field f:I

.field g:Lcom/google/android/apps/gmm/map/internal/c/bq;

.field final h:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final j:I

.field private final k:Z

.field private final l:I

.field private m:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private n:I

.field private o:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/h/b;->i:Lcom/google/android/apps/gmm/map/internal/c/bq;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/navigation/h/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/h/c;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/h/b;->a:Lcom/google/android/apps/gmm/map/internal/c/bq;

    return-void
.end method

.method public constructor <init>(IIZLcom/google/android/apps/gmm/shared/net/a/b;)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const/16 v1, 0x12c

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->h:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 83
    iput p1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->l:I

    .line 84
    const/4 v0, 0x1

    invoke-static {p2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->j:I

    .line 85
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/navigation/h/b;->k:Z

    .line 86
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->m:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 87
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 177
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    if-eqz v0, :cond_8

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/bq;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    .line 179
    if-nez v0, :cond_7

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v0, :cond_1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->d:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->d:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->d:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->l:I

    iget v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->j:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v6

    int-to-double v8, v5

    mul-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v5, v6

    shl-int/lit8 v5, v5, 0x1

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    div-int/lit8 v8, v5, 0x2

    sub-int/2addr v7, v8

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    div-int/lit8 v9, v5, 0x2

    add-int/2addr v8, v9

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v7, v8, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ag;

    invoke-direct {v0, v6, v7, v4, v5}, Lcom/google/android/apps/gmm/map/internal/c/ag;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->n:I

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->k:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x3

    if-lt v4, v5, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/ag;

    iget v6, p0, Lcom/google/android/apps/gmm/navigation/h/b;->l:I

    iget v7, p0, Lcom/google/android/apps/gmm/navigation/h/b;->j:I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v8

    int-to-double v10, v7

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v7, v8

    shl-int/lit8 v7, v7, 0x1

    invoke-direct {v5, v0, v4, v6, v7}, Lcom/google/android/apps/gmm/map/internal/c/ag;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;II)V

    iput-object v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->o:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->o:I

    goto/16 :goto_0

    :cond_3
    iget v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->f:I

    if-ne v0, v4, :cond_4

    move v0, v3

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    add-int/lit8 v5, v0, 0x1

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->m:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v5, v5, Lcom/google/r/b/a/ou;->B:I

    mul-int/lit16 v5, v5, 0x3e8

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v6

    int-to-double v4, v5

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v4, v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    add-int/lit8 v0, v0, 0x1

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aget-wide v6, v5, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    iget v5, p0, Lcom/google/android/apps/gmm/navigation/h/b;->f:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aget-wide v8, v0, v5

    sub-double/2addr v6, v8

    int-to-double v4, v4

    cmpg-double v0, v6, v4

    if-gtz v0, :cond_5

    move v0, v3

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1

    :cond_6
    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    goto/16 :goto_0

    .line 181
    :cond_7
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/h/b;->h:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    .line 182
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->h:Lcom/google/android/apps/gmm/map/util/a/e;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 187
    :goto_2
    return-object v0

    :cond_8
    move-object v0, v1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;I)V
    .locals 2

    .prologue
    .line 129
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    .line 130
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->d:I

    if-gt v1, v0, :cond_1

    .line 132
    sget-object v1, Lcom/google/android/apps/gmm/navigation/h/b;->a:Lcom/google/android/apps/gmm/map/internal/c/bq;

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    .line 133
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->d:I

    .line 134
    iput p2, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    .line 145
    :cond_0
    :goto_0
    iput p2, p0, Lcom/google/android/apps/gmm/navigation/h/b;->f:I

    .line 146
    return-void

    .line 135
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    if-gt v0, p2, :cond_2

    .line 137
    sget-object v0, Lcom/google/android/apps/gmm/navigation/h/b;->a:Lcom/google/android/apps/gmm/map/internal/c/bq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    .line 138
    iput p2, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    goto :goto_0

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->f:I

    if-ge v0, p2, :cond_0

    .line 143
    sget-object v0, Lcom/google/android/apps/gmm/navigation/h/b;->a:Lcom/google/android/apps/gmm/map/internal/c/bq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/w;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 107
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->b:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 108
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->c:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 109
    iput v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->d:I

    .line 110
    iput v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->e:I

    .line 111
    iput v1, p0, Lcom/google/android/apps/gmm/navigation/h/b;->f:I

    .line 112
    sget-object v0, Lcom/google/android/apps/gmm/navigation/h/b;->a:Lcom/google/android/apps/gmm/map/internal/c/bq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/h/b;->g:Lcom/google/android/apps/gmm/map/internal/c/bq;

    .line 113
    return-void
.end method
