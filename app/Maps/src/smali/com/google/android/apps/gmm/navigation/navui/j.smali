.class Lcom/google/android/apps/gmm/navigation/navui/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic b:Lcom/google/android/apps/gmm/base/g/c;

.field final synthetic c:Lcom/google/android/apps/gmm/x/a;

.field final synthetic d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field final synthetic e:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 342
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->e:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->b:Lcom/google/android/apps/gmm/base/g/c;

    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->c:Lcom/google/android/apps/gmm/x/a;

    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 345
    sget-object v0, Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;->a:Ljava/lang/String;

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->b()V

    .line 352
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->b:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->c:Lcom/google/android/apps/gmm/x/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->b:Lcom/google/android/apps/gmm/base/g/c;

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v3, v2}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    move-result-object v0

    .line 356
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 358
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v3, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 359
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 364
    :goto_0
    return-void

    .line 361
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->e:Lcom/google/android/apps/gmm/navigation/navui/NavigationFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->l()Lcom/google/android/apps/gmm/droppedpin/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->b:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/j;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 362
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/droppedpin/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto :goto_0
.end method
