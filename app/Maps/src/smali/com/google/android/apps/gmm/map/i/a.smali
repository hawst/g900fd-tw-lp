.class public Lcom/google/android/apps/gmm/map/i/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/i/a/a;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

.field private final c:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/r/b/a/acy;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/map/i/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/i/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/c/a/g;)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-static {}, Lcom/google/b/c/hj;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/a;->c:Ljava/util/concurrent/ConcurrentMap;

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/i/a;->b:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/r/b/a/acy;FLandroid/content/res/Resources;Lcom/google/android/apps/gmm/map/i/a/c;)Landroid/graphics/drawable/Drawable;
    .locals 9
    .param p5    # Lcom/google/android/apps/gmm/map/i/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/a;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1, p2}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/lang/String;

    .line 77
    if-nez v7, :cond_1

    .line 78
    sget-object v0, Lcom/google/android/apps/gmm/map/i/a;->a:Ljava/lang/String;

    const-string v0, "Could not find the icon (%s, %s).\nYou must call requestIcons() in advance to register an icon URL for (%s, %s)."

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    const/4 v2, 0x3

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 99
    :cond_0
    :goto_0
    return-object v8

    .line 85
    :cond_1
    if-eqz p5, :cond_2

    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/map/i/b;

    move-object v1, p0

    move-object v2, p5

    move-object v3, p1

    move-object v4, p2

    move-object v5, p4

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/i/b;-><init>(Lcom/google/android/apps/gmm/map/i/a;Lcom/google/android/apps/gmm/map/i/a/c;Ljava/lang/String;Lcom/google/r/b/a/acy;Landroid/content/res/Resources;F)V

    .line 94
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/a;->b:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    sget-object v2, Lcom/google/android/apps/gmm/map/i/a;->a:Ljava/lang/String;

    invoke-interface {v1, v7, v2, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 95
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 96
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v1, p4, p3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->a(Landroid/content/res/Resources;F)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    if-nez v8, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    goto :goto_0

    :cond_2
    move-object v0, v8

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/google/r/b/a/acy;)Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/a;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1, p2}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 167
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ada;

    .line 168
    iget v1, v0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    iget v1, v0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_2

    move v1, v2

    :goto_2
    if-eqz v1, :cond_0

    iget v1, v0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v8, :cond_3

    move v1, v2

    :goto_3
    if-nez v1, :cond_7

    .line 169
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/map/i/a;->a:Ljava/lang/String;

    const-string v4, "TactileIconUrlMapEntry must have ID, Icon Style and URL. (hasId(), hasIconStyle(), hasUrl()) = (%b, %b, %b)"

    const/4 v1, 0x3

    new-array v6, v1, [Ljava/lang/Object;

    .line 171
    iget v1, v0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_4

    move v1, v2

    :goto_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v6, v3

    iget v1, v0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v7, :cond_5

    move v1, v2

    :goto_5
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v6, v2

    iget v0, v0, Lcom/google/r/b/a/ada;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v8, :cond_6

    move v0, v2

    :goto_6
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v6, v7

    .line 169
    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_0

    :cond_1
    move v1, v3

    .line 168
    goto :goto_1

    :cond_2
    move v1, v3

    goto :goto_2

    :cond_3
    move v1, v3

    goto :goto_3

    :cond_4
    move v1, v3

    .line 171
    goto :goto_4

    :cond_5
    move v1, v3

    goto :goto_5

    :cond_6
    move v0, v3

    goto :goto_6

    .line 175
    :cond_7
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/i/a;->c:Ljava/util/concurrent/ConcurrentMap;

    iget-object v1, v0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_9

    check-cast v1, Ljava/lang/String;

    :goto_7
    iget v4, v0, Lcom/google/r/b/a/ada;->c:I

    invoke-static {v4}, Lcom/google/r/b/a/acy;->a(I)Lcom/google/r/b/a/acy;

    move-result-object v4

    if-nez v4, :cond_8

    sget-object v4, Lcom/google/r/b/a/acy;->a:Lcom/google/r/b/a/acy;

    :cond_8
    invoke-static {v1, v4}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/r/b/a/ada;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v1, v0}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_9
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_a

    iput-object v4, v0, Lcom/google/r/b/a/ada;->b:Ljava/lang/Object;

    :cond_a
    move-object v1, v4

    goto :goto_7

    .line 177
    :cond_b
    return-void
.end method

.method public final a(Ljava/util/Collection;Lcom/google/android/apps/gmm/map/i/a/b;)V
    .locals 5
    .param p2    # Lcom/google/android/apps/gmm/map/i/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/r/b/a/ada;",
            ">;",
            "Lcom/google/android/apps/gmm/map/i/a/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 124
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 126
    :cond_0
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    .line 128
    if-nez v0, :cond_2

    .line 129
    if-eqz p2, :cond_1

    .line 130
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/i/a/b;->a()V

    .line 163
    :cond_1
    return-void

    .line 135
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/i/a;->a(Ljava/util/Collection;)V

    .line 137
    new-instance v1, Lcom/google/android/apps/gmm/map/i/c;

    invoke-direct {v1, p0, v0, p2}, Lcom/google/android/apps/gmm/map/i/c;-><init>(Lcom/google/android/apps/gmm/map/i/a;ILcom/google/android/apps/gmm/map/i/a/b;)V

    .line 156
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ada;

    .line 157
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/i/a;->b:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    .line 158
    invoke-virtual {v0}, Lcom/google/r/b/a/ada;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/map/i/a;->a:Ljava/lang/String;

    .line 157
    invoke-interface {v3, v0, v4, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 160
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/f;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Lcom/google/r/b/a/acy;)Landroid/graphics/Picture;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/a;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1, p2}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 106
    if-nez v0, :cond_0

    .line 107
    sget-object v0, Lcom/google/android/apps/gmm/map/i/a;->a:Ljava/lang/String;

    const-string v0, "Could not find the icon (%s, %s).\nYou must call requestIcons() in advance to register an icon URL for (%s, %s)."

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-object v0, v1

    .line 117
    :goto_0
    return-object v0

    .line 113
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/a;->b:Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    sget-object v3, Lcom/google/android/apps/gmm/map/i/a;->a:Ljava/lang/String;

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v2

    .line 114
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 115
    iget v0, v2, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    const/4 v3, 0x6

    if-eq v0, v3, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/a/a/b;

    if-nez v0, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    :cond_2
    iget-object v0, v0, Lcom/a/a/b;->a:Landroid/graphics/Picture;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 117
    goto :goto_0
.end method
