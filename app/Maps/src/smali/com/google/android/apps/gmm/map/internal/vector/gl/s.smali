.class final enum Lcom/google/android/apps/gmm/map/internal/vector/gl/s;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/vector/gl/s;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/map/internal/vector/gl/s;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 21
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const-string v1, "NO_MIPMAP"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const-string v1, "MANUAL_MIPMAP"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    .line 23
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const-string v1, "AUTO_MIPMAP"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->d:[Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/vector/gl/s;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/vector/gl/s;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->d:[Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    return-object v0
.end method
