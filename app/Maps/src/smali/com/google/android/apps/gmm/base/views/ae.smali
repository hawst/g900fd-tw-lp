.class public Lcom/google/android/apps/gmm/base/views/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/aq;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

.field public final b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

.field final synthetic c:Lcom/google/android/apps/gmm/base/views/ac;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/views/ac;Lcom/google/android/apps/gmm/base/views/MultiColumnListView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/ae;->c:Lcom/google/android/apps/gmm/base/views/ac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/ae;->a:Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 56
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/views/ae;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 78
    if-eqz p2, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ae;->c:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ac;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ae;->c:Lcom/google/android/apps/gmm/base/views/ac;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/base/views/ac;->a(Lcom/google/android/apps/gmm/base/views/ac;Landroid/view/View;)V

    .line 85
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ae;->c:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ac;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ae;->c:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ac;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 97
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ae;->c:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ac;->a:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ac;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 94
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 71
    if-nez p1, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ae;->c:Lcom/google/android/apps/gmm/base/views/ac;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ac;->a:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ac;->a:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 74
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 100
    return-void
.end method
