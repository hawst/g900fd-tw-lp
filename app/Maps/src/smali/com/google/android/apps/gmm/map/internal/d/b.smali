.class public abstract Lcom/google/android/apps/gmm/map/internal/d/b;
.super Lcom/google/android/apps/gmm/map/internal/d/i;
.source "PG"


# instance fields
.field public a:I

.field public b:[[B

.field public c:[[B

.field public d:[I

.field public e:[I

.field public f:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic g:Lcom/google/android/apps/gmm/map/internal/d/a;

.field private final o:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/a;Z)V
    .locals 5

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 527
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    .line 529
    sget-object v3, Lcom/google/r/b/a/el;->bd:Lcom/google/r/b/a/el;

    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    invoke-direct {p0, v3, v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/i;-><init>(Lcom/google/r/b/a/el;II)V

    .line 514
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 532
    if-eqz p2, :cond_0

    .line 533
    const/16 v1, 0x10

    .line 535
    :cond_0
    new-array v0, v1, [[B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->b:[[B

    .line 536
    new-array v0, v1, [[B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->c:[[B

    .line 537
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->e:[I

    .line 538
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->d:[I

    move v0, v2

    .line 539
    :goto_1
    if-ge v0, v1, :cond_2

    .line 540
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->e:[I

    aput v2, v3, v0

    .line 541
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->d:[I

    const/4 v4, -0x1

    aput v4, v3, v0

    .line 539
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 529
    goto :goto_0

    .line 543
    :cond_2
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/e/a/a/a/b;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 5

    .prologue
    .line 894
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    .line 895
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ab;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/gmm/map/internal/c/ab;-><init>(Lcom/google/e/a/a/a/b;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    .line 897
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    return-object v1
.end method

.method private c(Lcom/google/e/a/a/a/b;)V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x2

    const/16 v11, 0x15

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 727
    const/16 v0, 0x1a

    invoke-virtual {p1, v3, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 728
    invoke-virtual {v0, v12, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v4, v4

    const/4 v1, 0x3

    invoke-virtual {v0, v1, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v5, v6

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v1, v6

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/d/a;->d:I

    sub-int v6, v1, v6

    new-instance v7, Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/internal/c/cd;-><init>()V

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bv;->values()[Lcom/google/android/apps/gmm/map/internal/c/bv;

    move-result-object v8

    array-length v9, v8

    move v1, v2

    :goto_0
    if-ge v1, v9, :cond_1

    aget-object v10, v8, v1

    invoke-virtual {v10, v0}, Lcom/google/android/apps/gmm/map/internal/c/bv;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/bu;

    move-result-object v10

    if-eqz v10, :cond_0

    invoke-virtual {v7, v10}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v1, v8

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(I)Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v1

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v8, v8, v2

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v8, v8, v2

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    sget-object v9, Lcom/google/android/apps/gmm/map/internal/c/bv;->e:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    if-eqz v8, :cond_2

    invoke-interface {v8, v1}, Lcom/google/android/apps/gmm/map/internal/c/bu;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-virtual {v7, v8}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)V

    :cond_2
    new-instance v8, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v8, v6, v4, v5, v7}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    invoke-static {v1, v8}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v4

    .line 730
    invoke-virtual {v0, v3, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v1, v6

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(I)Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v1

    .line 729
    invoke-virtual {p0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/d/b;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/b/a/an;)Ljava/lang/Integer;

    move-result-object v5

    .line 731
    if-nez v5, :cond_3

    .line 732
    const-string v0, "DashServerMapTileStore"

    const-string v1, "Received wrong tile"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 758
    :goto_1
    return-void

    .line 734
    :cond_3
    const/16 v1, 0x19

    invoke-virtual {p1, v12, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 735
    if-eqz v1, :cond_4

    array-length v6, v1

    if-nez v6, :cond_5

    .line 745
    :cond_4
    const-string v1, "DashServerMapTileStore"

    iget-object v0, v4, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v4, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x38

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "No TILE_DATA tag in proto or COMPACT-0 tile with key: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 748
    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->b:[[B

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aput-object v1, v4, v6

    .line 749
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->d:[I

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v13}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_8

    move v1, v3

    :goto_2
    if-nez v1, :cond_6

    invoke-virtual {v0, v13}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_9

    :cond_6
    move v1, v3

    :goto_3
    if-eqz v1, :cond_a

    .line 750
    invoke-virtual {v0, v13, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v1, v8

    :goto_4
    aput v1, v4, v6

    .line 752
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->e:[I

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/16 v7, 0x21

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_b

    move v1, v3

    :goto_5
    if-nez v1, :cond_7

    invoke-virtual {v0, v7}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_c

    :cond_7
    move v1, v3

    :goto_6
    if-eqz v1, :cond_d

    const/16 v1, 0x21

    .line 753
    invoke-virtual {v0, v1, v11}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    :goto_7
    aput v0, v4, v6

    .line 755
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->c:[[B

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/internal/d/b;->a(I)[B

    move-result-object v2

    aput-object v2, v0, v1

    goto/16 :goto_1

    :cond_8
    move v1, v2

    .line 749
    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_3

    .line 750
    :cond_a
    const/4 v1, -0x1

    goto :goto_4

    :cond_b
    move v1, v2

    .line 752
    goto :goto_5

    :cond_c
    move v1, v2

    goto :goto_6

    :cond_d
    move v0, v2

    .line 753
    goto :goto_7
.end method


# virtual methods
.method protected a()I
    .locals 1

    .prologue
    .line 547
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->a:I

    return v0
.end method

.method protected final a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 797
    sget-object v0, Lcom/google/r/b/a/b/bg;->g:Lcom/google/e/a/a/a/d;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/d;Ljava/io/DataInput;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 798
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b;->b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/google/e/a/a/a/b;)V
    .locals 4

    .prologue
    const/16 v2, 0x15

    .line 719
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->a:I

    .line 720
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    .line 721
    if-eqz v0, :cond_0

    .line 722
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/a;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Received tile response code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 724
    :cond_0
    return-void
.end method

.method protected final a(Ljava/io/DataOutput;)V
    .locals 1

    .prologue
    .line 552
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 553
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Ljava/io/DataOutput;Lcom/google/e/a/a/a/b;)V

    .line 554
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/internal/d/p;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 712
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    if-nez v2, :cond_1

    .line 715
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method protected a(I)[B
    .locals 1

    .prologue
    .line 706
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/a;->a:[B

    return-object v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 923
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->e:[I

    aget v0, v0, p1

    return v0
.end method

.method b(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 8

    .prologue
    const/16 v7, 0x1a

    const/16 v6, 0x9

    const/4 v2, 0x0

    .line 802
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b;->a(Lcom/google/e/a/a/a/b;)V

    .line 804
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    .line 805
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    .line 806
    if-le v1, v0, :cond_0

    .line 807
    const-string v3, "DashServerMapTileStore"

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x30

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Received "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " tiles, expected "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v3, v1, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v0

    .line 811
    :cond_0
    :goto_0
    if-ge v2, v1, :cond_1

    .line 812
    invoke-virtual {p1, v6, v2, v7}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/b;->c(Lcom/google/e/a/a/a/b;)V

    .line 811
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 814
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/e/a/a/a/b;
    .locals 13

    .prologue
    .line 557
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/d/c;->a:Lcom/google/android/apps/gmm/map/internal/d/c;

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/c;->a:Lcom/google/android/apps/gmm/map/internal/d/c;

    if-eq v1, v4, :cond_0

    iget v4, v2, Lcom/google/android/apps/gmm/map/internal/d/c;->f:I

    iget v5, v1, Lcom/google/android/apps/gmm/map/internal/d/c;->f:I

    if-ge v4, v5, :cond_1

    :cond_0
    move-object v1, v2

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/a;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Lcom/google/e/a/a/a/b;

    move-result-object v5

    .line 558
    new-instance v6, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/bg;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v6, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 561
    const/4 v0, 0x1

    iget-object v1, v6, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0, v5}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 569
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v1, v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 571
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/a;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v2

    .line 572
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 573
    const/high16 v0, 0x40000000    # 2.0f

    iget v4, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0x1

    iget v4, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    add-int/2addr v4, v0

    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    add-int/2addr v0, v7

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v0, 0x0

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/a;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v4, v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 572
    :cond_3
    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v0

    .line 576
    iget v7, v1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 581
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->e:Ljava/util/TreeSet;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/d/a;->k()Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v0

    .line 586
    :cond_4
    if-ltz v7, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    array-length v1, v1

    if-lt v7, v1, :cond_7

    :cond_5
    const/4 v0, -0x1

    .line 587
    :goto_1
    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    .line 590
    const/16 v0, 0x15

    .line 593
    :cond_6
    sub-int/2addr v0, v7

    .line 596
    const/4 v1, 0x3

    .line 598
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 609
    iget v8, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->l:I

    .line 610
    const/4 v0, 0x0

    .line 612
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v2, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v2, :cond_8

    const/4 v0, 0x1

    .line 613
    :goto_2
    const/4 v3, 0x0

    .line 614
    const/4 v2, 0x1

    move v4, v0

    :goto_3
    if-ge v2, v8, :cond_b

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v9, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v9, :cond_9

    const/4 v0, 0x1

    :goto_4
    and-int/2addr v4, v0

    .line 616
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    if-eq v0, v7, :cond_a

    const/4 v0, 0x1

    :goto_5
    or-int/2addr v3, v0

    .line 614
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 586
    :cond_7
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    aget v0, v0, v7

    goto :goto_1

    .line 612
    :cond_8
    const/4 v0, 0x0

    goto :goto_2

    .line 615
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    .line 616
    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 618
    :cond_b
    if-eqz v3, :cond_17

    .line 619
    if-nez v4, :cond_e

    .line 620
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 621
    const/4 v0, 0x0

    :goto_6
    if-ge v0, v8, :cond_d

    .line 622
    if-lez v0, :cond_c

    .line 623
    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 626
    const-string v2, " z"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 627
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 621
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 629
    :cond_d
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 630
    const-string v0, "DashServerMapTileStore"

    const-string v2, "Batch request contains multiple zooms: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 632
    :cond_e
    const/4 v0, 0x0

    .line 636
    :goto_7
    const/4 v1, 0x1

    :goto_8
    if-gt v1, v0, :cond_f

    .line 637
    const/4 v2, 0x7

    add-int v3, v7, v1

    int-to-long v10, v3

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 636
    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    .line 640
    :cond_f
    const/4 v2, 0x0

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/a;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/a;->i:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 642
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/r;->d()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 644
    :goto_9
    const/4 v1, 0x0

    move v12, v1

    move v1, v2

    move v2, v12

    :goto_a
    if-ge v2, v8, :cond_15

    .line 645
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v3, v3, v2

    .line 646
    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 647
    new-instance v5, Lcom/google/e/a/a/a/b;

    sget-object v7, Lcom/google/r/b/a/b/bg;->l:Lcom/google/e/a/a/a/d;

    invoke-direct {v5, v7}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 648
    const/4 v7, 0x2

    iget v9, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v7, v9}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 649
    const/4 v7, 0x3

    iget v9, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v7, v9}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 650
    const/4 v7, 0x4

    iget v9, v4, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget v10, v10, Lcom/google/android/apps/gmm/map/internal/d/a;->d:I

    add-int/2addr v9, v10

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v7, v9}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 651
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/internal/d/a;->c()Z

    move-result v7

    if-eqz v7, :cond_10

    .line 657
    const/16 v7, 0xf

    sget-object v9, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/internal/c/ac;->m:Ljava/lang/String;

    iget-object v10, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v7, v9}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 659
    :cond_10
    sget-object v7, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v7, v9, :cond_11

    .line 660
    const/4 v1, 0x1

    .line 661
    if-eqz v0, :cond_11

    .line 662
    const/16 v7, 0x20

    const/4 v9, 0x0

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v7, v9}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 665
    :cond_11
    const/4 v7, 0x1

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/ai;->y:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v7, v9}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 666
    const/4 v7, 0x7

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/ai;->z:I

    int-to-long v10, v9

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v9

    iget-object v10, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v10, v7, v9}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 667
    iget-object v7, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v7, v5}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/e/a/a/a/b;)V

    .line 668
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/d/a;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/d/ac;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v4

    .line 669
    iget-boolean v4, v4, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    if-eqz v4, :cond_13

    .line 672
    const/16 v4, 0x8

    iget v7, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->i:I

    int-to-long v10, v7

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v9, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v4, v7}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 673
    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v7, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 674
    const/16 v4, 0x1f

    iget v7, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->j:I

    int-to-long v10, v7

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v7

    iget-object v9, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v4, v7}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 676
    :cond_12
    const/16 v4, 0x21

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/d/p;->k:I

    int-to-long v10, v3

    invoke-static {v10, v11}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v5, v4, v3}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 678
    :cond_13
    const/16 v3, 0x9

    invoke-virtual {v6, v3, v5}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 644
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_a

    .line 642
    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_9

    .line 680
    :cond_15
    if-eqz v1, :cond_16

    if-eqz v0, :cond_16

    .line 681
    const/16 v1, 0xa

    invoke-virtual {v6, v1, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 684
    :cond_16
    return-object v6

    :cond_17
    move v0, v1

    goto/16 :goto_7
.end method

.method protected final c(I)[B
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->b:[[B

    aget-object v0, v0, p1

    return-object v0
.end method

.method protected final d(I)[B
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b;->c:[[B

    aget-object v0, v0, p1

    return-object v0
.end method
