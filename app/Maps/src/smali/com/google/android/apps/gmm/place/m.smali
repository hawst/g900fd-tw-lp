.class public Lcom/google/android/apps/gmm/place/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;


# instance fields
.field public final a:Landroid/view/ViewGroup;

.field public final b:Landroid/view/View;

.field private final c:Lcom/google/android/apps/gmm/base/activities/c;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:I

.field private g:Lcom/google/android/apps/gmm/place/o;

.field private h:Z

.field private i:Z

.field private j:Landroid/view/ViewPropertyAnimator;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 4

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->i:Z

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    .line 69
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/n;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    .line 70
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/place/o;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/o;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/m;->g:Lcom/google/android/apps/gmm/place/o;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/m;->g:Lcom/google/android/apps/gmm/place/o;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/f;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/m;->d:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/c;->a:Lcom/google/android/libraries/curvular/bk;

    .line 79
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/m;->e:Landroid/view/View;

    .line 81
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/m;->f:I

    .line 82
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;F)I
    .locals 3

    .prologue
    .line 166
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 167
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v0

    aget v0, v1, v0

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 168
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v2, v1

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    .line 166
    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 170
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getTop()I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    return v0
.end method


# virtual methods
.method public a()V
    .locals 8

    .prologue
    const/4 v5, 0x4

    const/4 v1, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v6, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 222
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v6, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v6, v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    .line 223
    iget-object v7, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    if-eqz v0, :cond_1

    move v0, v3

    :goto_1
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    .line 224
    iget-object v7, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    invoke-virtual {v7, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 226
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v6, v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->i:Z

    .line 227
    iget-object v6, p0, Lcom/google/android/apps/gmm/place/m;->d:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->i:Z

    if-eqz v0, :cond_4

    move v0, v3

    :goto_4
    invoke-virtual {v6, v0}, Landroid/view/View;->setAlpha(F)V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->e:Landroid/view/View;

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/place/m;->i:Z

    if-eqz v6, :cond_5

    :goto_5
    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 230
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v4, :cond_6

    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq v0, v4, :cond_6

    :goto_6
    if-eqz v1, :cond_7

    :goto_7
    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 232
    return-void

    :cond_0
    move v0, v2

    .line 222
    goto :goto_0

    :cond_1
    move v0, v4

    .line 223
    goto :goto_1

    :cond_2
    move v0, v5

    .line 224
    goto :goto_2

    :cond_3
    move v0, v2

    .line 226
    goto :goto_3

    :cond_4
    move v0, v4

    .line 227
    goto :goto_4

    :cond_5
    move v3, v4

    .line 228
    goto :goto_5

    :cond_6
    move v1, v2

    .line 230
    goto :goto_6

    :cond_7
    move v2, v5

    goto :goto_7
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 7

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v3, :cond_4

    move v3, v0

    move v0, v1

    :goto_0
    iget-object v6, p0, Lcom/google/android/apps/gmm/place/m;->g:Lcom/google/android/apps/gmm/place/o;

    iput v3, v6, Lcom/google/android/apps/gmm/place/o;->a:I

    iput-boolean v0, v6, Lcom/google/android/apps/gmm/place/o;->b:Z

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/place/o;->invalidateSelf()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x12

    if-ge v0, v3, :cond_8

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_5

    move v0, v2

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    if-eqz v0, :cond_6

    move v0, v4

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setAlpha(F)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_0
    :goto_4
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    :cond_1
    :goto_5
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_d

    :cond_2
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->i:Z

    if-eq v2, v0, :cond_3

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/m;->i:Z

    if-eqz v2, :cond_e

    :goto_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/m;->j:Landroid/view/ViewPropertyAnimator;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->j:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 100
    :cond_3
    return-void

    .line 99
    :cond_4
    sget-object v3, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v3, :cond_10

    invoke-static {p1, p3}, Lcom/google/android/apps/gmm/place/m;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;F)I

    move-result v3

    sub-int v3, v0, v3

    invoke-static {v3, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v3, v0

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    :cond_6
    move v0, v5

    goto :goto_2

    :cond_7
    const/4 v0, 0x4

    goto :goto_3

    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_9

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_9

    move v0, v2

    :goto_8
    if-eqz v0, :cond_a

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget v3, p0, Lcom/google/android/apps/gmm/place/m;->f:I

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setTranslationY(F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/base/h/a;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    goto/16 :goto_4

    :cond_9
    move v0, v1

    goto :goto_8

    :cond_a
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_b

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-eq p2, v0, :cond_b

    move v0, v2

    :goto_9
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/base/h/a;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/place/n;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/place/n;-><init>(Lcom/google/android/apps/gmm/place/m;)V

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/m;->h:Z

    goto/16 :goto_4

    :cond_b
    move v0, v1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->e:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setAlpha(F)V

    goto/16 :goto_5

    :cond_d
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v0, :cond_f

    invoke-static {p1, p3}, Lcom/google/android/apps/gmm/place/m;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;F)I

    move-result v0

    if-lez v0, :cond_2

    move v2, v1

    goto/16 :goto_6

    :cond_e
    move v4, v5

    goto/16 :goto_7

    :cond_f
    move v2, v1

    goto/16 :goto_6

    :cond_10
    move v0, v1

    move v3, v1

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 0

    .prologue
    .line 95
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 106
    return-void
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/m;->a()V

    .line 248
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->c:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->j:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->j:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/m;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 261
    return-void
.end method
