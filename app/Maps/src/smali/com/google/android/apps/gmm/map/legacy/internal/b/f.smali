.class public Lcom/google/android/apps/gmm/map/legacy/internal/b/f;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
.source "PG"


# instance fields
.field private volatile A:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

.field private B:Lcom/google/android/apps/gmm/map/indoor/c/q;

.field private final C:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field public volatile a:Lcom/google/android/apps/gmm/map/b/a/ai;

.field private z:Lcom/google/android/apps/gmm/map/indoor/c/s;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/k;ILcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V
    .locals 21

    .prologue
    .line 136
    sget-object v6, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    new-instance v8, Lcom/google/android/apps/gmm/map/legacy/internal/b/g;

    .line 142
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v2

    move-object/from16 v0, p5

    move/from16 v1, p6

    invoke-direct {v8, v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/g;-><init>(Lcom/google/android/apps/gmm/map/internal/b/g;ILcom/google/android/apps/gmm/map/internal/c/cw;)V

    const/4 v15, 0x0

    const/16 v16, 0x1

    const/16 v17, 0x1

    const/16 v18, 0x1

    sget-object v19, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v7, p4

    move/from16 v9, p6

    move/from16 v10, p7

    move/from16 v11, p8

    move-object/from16 v12, p9

    move/from16 v13, p10

    move/from16 v14, p10

    move-object/from16 v20, p11

    .line 136
    invoke-direct/range {v2 .. v20}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/k;IIZZZZLcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 101
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/a/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->A:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    .line 104
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 121
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->C:Ljava/util/Collection;

    .line 156
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    .locals 2

    .prologue
    .line 213
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v0

    .line 214
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->d()Z

    move-result v1

    if-nez v1, :cond_1

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->A:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/a/e;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v0

    .line 217
    :cond_1
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, p1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 165
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/a/e;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->A:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v1, p1, :cond_0

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->a:Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/indoor/c/q;)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    if-nez v0, :cond_1

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    if-nez v0, :cond_2

    .line 179
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    .line 185
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    if-nez v1, :cond_3

    iput-object p1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->a:Z

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0

    .line 181
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v1

    .line 182
    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    .line 183
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185
    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v1

    :try_start_1
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->c:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-exit v1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/internal/b/e;)V
    .locals 3

    .prologue
    .line 197
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/c/s;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v2, v1, :cond_0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/indoor/c/s;->a:Z

    .line 199
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/b/e;)V

    .line 200
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/Collection;ILjava/util/Set;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;I",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->C:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->clear()V

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->C:Ljava/util/Collection;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/c/s;->a(Ljava/util/Collection;)J

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->C:Ljava/util/Collection;

    invoke-interface {p2, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    .line 229
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/Collection;ILjava/util/Set;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->B:Lcom/google/android/apps/gmm/map/indoor/c/q;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/indoor/c/q;->b:Lcom/google/android/apps/gmm/map/indoor/d/a;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/indoor/c/q;->a(Lcom/google/android/apps/gmm/map/indoor/d/a;)Lcom/google/android/apps/gmm/map/indoor/d/e;

    move-result-object v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->e()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/cd;->a:[Lcom/google/android/apps/gmm/map/internal/c/bu;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/bv;->ordinal()I

    move-result v4

    aget-object v0, v0, v4

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/w;

    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v4, :cond_0

    if-nez v2, :cond_1

    const/4 v0, 0x0

    :goto_1
    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Z)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/w;->a:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/b/a/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    .line 236
    :cond_2
    return-void
.end method

.method protected final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    if-nez v0, :cond_0

    .line 208
    :goto_0
    return-void

    .line 207
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;->z:Lcom/google/android/apps/gmm/map/indoor/c/s;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/indoor/c/s;->b(Ljava/util/Collection;)J

    goto :goto_0
.end method
