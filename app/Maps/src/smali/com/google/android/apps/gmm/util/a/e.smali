.class public Lcom/google/android/apps/gmm/util/a/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/util/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:Ljava/lang/Throwable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public c:Z

.field public final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/util/a/e;->c:Z

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/util/a/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/util/a/f;-><init>(Lcom/google/android/apps/gmm/util/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/util/a/e;->d:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 94
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_4

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    if-eqz v2, :cond_1

    :goto_1
    const-string v1, "Already unblocked"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v2, v1

    .line 94
    goto :goto_0

    :cond_1
    move v0, v1

    .line 95
    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/a;->e:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/a;->c:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/a/a;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/util/a/e;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/util/a/e;->b:Ljava/lang/Throwable;

    iget-object v0, p0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    iput-object v3, p0, Lcom/google/android/apps/gmm/util/a/e;->a:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/a/a;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/a;)V

    .line 97
    :cond_4
    return-void
.end method
