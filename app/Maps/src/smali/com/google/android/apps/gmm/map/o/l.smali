.class public Lcom/google/android/apps/gmm/map/o/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/k;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/internal/c/aa;

.field final b:F

.field final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/aa;F)V
    .locals 2

    .prologue
    .line 370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 371
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/l;->a:Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 372
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/l;->b:F

    .line 373
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/aa;->hashCode()I

    move-result v0

    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/l;->c:I

    .line 374
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 383
    if-ne p0, p1, :cond_1

    .line 390
    :cond_0
    :goto_0
    return v0

    .line 386
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/o/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 387
    goto :goto_0

    .line 389
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/o/l;

    .line 390
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/l;->a:Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/l;->a:Lcom/google/android/apps/gmm/map/internal/c/aa;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/l;->b:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/l;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 378
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/l;->c:I

    return v0
.end method
