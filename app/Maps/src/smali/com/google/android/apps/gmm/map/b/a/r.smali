.class public Lcom/google/android/apps/gmm/map/b/a/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final b:Lcom/google/android/apps/gmm/map/b/a/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/r;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/q;DD)V
    .locals 8

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v4, p2, v4

    sub-double/2addr v2, v4

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v6, p4, v6

    sub-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L    # 180.0

    add-double/2addr v4, v6

    const-wide v6, 0x4076800000000000L    # 360.0

    rem-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L    # 180.0

    sub-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v4, p2, v4

    add-double/2addr v2, v4

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double v6, p4, v6

    add-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L    # 180.0

    add-double/2addr v4, v6

    const-wide v6, 0x4076800000000000L    # 360.0

    rem-double/2addr v4, v6

    const-wide v6, 0x4066800000000000L    # 180.0

    sub-double/2addr v4, v6

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/r;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)V

    .line 152
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    const-string v0, "Null southwest"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :cond_0
    const-string v0, "Null northeast"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_1
    iget-wide v4, p2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "Southern latitude exceeds northern latitude (%s > %s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-wide v6, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 137
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v4, v2

    iget-wide v6, p2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 138
    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v4, v1

    .line 136
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 139
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 140
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 141
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;)D
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 351
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 352
    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 353
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    cmpg-double v1, v2, v4

    if-gez v1, :cond_3

    :cond_2
    :goto_0
    if-nez v0, :cond_5

    const/4 v0, 0x0

    .line 354
    :goto_1
    if-nez v0, :cond_8

    .line 355
    const-wide/16 v0, 0x0

    .line 357
    :goto_2
    return-wide v0

    .line 353
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    cmpl-double v1, v2, v4

    if-gtz v1, :cond_2

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/r;->a(D)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/r;->a(D)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/r;->a(D)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/r;->a(D)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v8

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/r;->a(D)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    :goto_3
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/r;->a(D)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    :goto_4
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/r;

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v5, v6, v7, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v0, v8, v9, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-direct {v4, v5, v0}, Lcom/google/android/apps/gmm/map/b/a/r;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)V

    move-object v0, v4

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    goto :goto_3

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    goto :goto_4

    .line 357
    :cond_8
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/r;->c()D

    move-result-wide v0

    goto/16 :goto_2
.end method

.method public static a(Lcom/google/d/a/a/ju;)Lcom/google/android/apps/gmm/map/b/a/r;
    .locals 6
    .param p0    # Lcom/google/d/a/a/ju;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 280
    if-nez p0, :cond_0

    move-object v0, v1

    .line 297
    :goto_0
    return-object v0

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/google/d/a/a/ju;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/d/a/a/hp;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v2

    .line 284
    iget-object v0, p0, Lcom/google/d/a/a/ju;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/hp;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/d/a/a/hp;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    .line 286
    if-eqz v2, :cond_1

    if-nez v3, :cond_2

    .line 287
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/r;->c:Ljava/lang/String;

    move-object v0, v1

    .line 288
    goto :goto_0

    .line 291
    :cond_2
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/r;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 292
    :catch_0
    move-exception v0

    .line 294
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/r;->c:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 297
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/r;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v1, v4, v5, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v2, v4, v5, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/r;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)V

    goto :goto_0
.end method

.method public static a()Lcom/google/android/apps/gmm/map/b/a/s;
    .locals 1

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/s;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(D)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 216
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpg-double v2, v2, v4

    if-gtz v2, :cond_2

    .line 217
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpg-double v2, v2, p1

    if-gtz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpg-double v2, p1, v2

    if-gtz v2, :cond_1

    .line 219
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 217
    goto :goto_0

    .line 219
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpg-double v2, v2, p1

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpg-double v2, p1, v2

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 12

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    .line 253
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    add-double/2addr v2, v4

    div-double/2addr v2, v10

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 256
    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/p;->c(DD)D

    move-result-wide v6

    div-double/2addr v6, v10

    add-double/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    return-object v0
.end method

.method public final c()D
    .locals 10

    .prologue
    const-wide v8, 0x4066800000000000L    # 180.0

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 314
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/p;->c(DD)D

    move-result-wide v0

    mul-double/2addr v0, v6

    div-double/2addr v0, v8

    .line 315
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v6

    div-double/2addr v2, v8

    .line 316
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v4, v6

    div-double/2addr v4, v8

    .line 317
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    sub-double/2addr v2, v4

    .line 315
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    .line 318
    mul-double/2addr v0, v2

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 230
    if-ne p0, p1, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 233
    goto :goto_0

    .line 235
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/r;

    .line 236
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 237
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 225
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 243
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "southwest"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 244
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "northeast"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 245
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 246
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
