.class public Lcom/google/android/apps/gmm/place/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Landroid/widget/PopupMenu$OnMenuItemClickListener;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Ljava/lang/CharSequence;

.field private final c:I

.field private d:Landroid/widget/PopupMenu;


# direct methods
.method private constructor <init>(Landroid/view/View;Ljava/lang/CharSequence;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/g;->a:Landroid/view/View;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/g;->b:Ljava/lang/CharSequence;

    .line 42
    iput p3, p0, Lcom/google/android/apps/gmm/place/g;->c:I

    .line 43
    return-void
.end method

.method public static a(Landroid/view/View;Ljava/lang/CharSequence;I)V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/place/g;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/place/g;-><init>(Landroid/view/View;Ljava/lang/CharSequence;I)V

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 37
    return-void
.end method


# virtual methods
.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g;->d:Landroid/widget/PopupMenu;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/PopupMenu;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/g;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/g;->a:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g;->d:Landroid/widget/PopupMenu;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g;->d:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/place/g;->c:I

    invoke-interface {v0, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g;->d:Landroid/widget/PopupMenu;

    invoke-virtual {v0, p0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g;->d:Landroid/widget/PopupMenu;

    invoke-virtual {v0}, Landroid/widget/PopupMenu;->show()V

    .line 49
    const/4 v0, 0x1

    return v0
.end method

.method public onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 63
    const-string v0, "clipboard"

    .line 64
    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 65
    sget v2, Lcom/google/android/apps/gmm/l;->cb:I

    .line 66
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/g;->b:Ljava/lang/CharSequence;

    .line 65
    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 67
    const/4 v0, 0x1

    return v0
.end method
