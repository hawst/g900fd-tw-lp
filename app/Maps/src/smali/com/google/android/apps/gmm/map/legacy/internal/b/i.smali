.class public Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/b;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/b/i;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final A:I

.field private final B:Lcom/google/android/apps/gmm/map/t/k;

.field private final C:I

.field private final D:I

.field private final E:Z

.field private final F:Z

.field private final G:Z

.field private final H:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashSet",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;>;"
        }
    .end annotation
.end field

.field private final I:[I

.field private final J:Lcom/google/android/apps/gmm/shared/c/a;

.field private K:Lcom/google/android/apps/gmm/map/internal/b/d;

.field private volatile L:Z

.field private final M:Lcom/google/android/apps/gmm/map/b/a/y;

.field private N:Lcom/google/android/apps/gmm/map/f/a/a;

.field private O:J

.field private P:J

.field private Q:Z

.field private final R:Z

.field private final S:Lcom/google/android/apps/gmm/map/internal/b/g;

.field private final T:Lcom/google/android/apps/gmm/map/internal/c/al;

.field private U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final V:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final W:Lcom/google/android/apps/gmm/map/e/a;

.field private X:J

.field private Y:Z

.field private Z:I

.field private aa:I

.field private ab:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final ac:Ljava/lang/StringBuilder;

.field private final ad:Ljava/util/Timer;

.field private ae:Ljava/util/TimerTask;

.field private final af:[F

.field public final b:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public final c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

.field public g:Lcom/google/android/apps/gmm/map/internal/b/e;

.field volatile i:Z

.field public volatile j:Lcom/google/android/apps/gmm/map/legacy/internal/b/k;

.field k:Ljava/lang/Runnable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public volatile l:Z

.field public m:Z

.field public n:Z

.field public o:D

.field public p:Z

.field volatile q:Z

.field public r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

.field public s:Lcom/google/android/apps/gmm/v/cp;

.field public t:Ljava/util/concurrent/atomic/AtomicInteger;

.field u:Lcom/google/android/apps/gmm/map/t/b;

.field public v:F

.field public w:Lcom/google/android/apps/gmm/map/c/a;

.field volatile x:Lcom/google/android/apps/gmm/map/o/p;

.field public volatile y:Z

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 91
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/k;IIZZZZLcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V
    .locals 6

    .prologue
    .line 795
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    .line 165
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    .line 172
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    .line 180
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->J:Lcom/google/android/apps/gmm/shared/c/a;

    .line 213
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->M:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 222
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->O:J

    .line 225
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->P:J

    .line 228
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->Q:Z

    .line 234
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->l:Z

    .line 248
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->m:Z

    .line 251
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->n:Z

    .line 256
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->o:D

    .line 268
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->p:Z

    .line 275
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->q:Z

    .line 292
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->U:Ljava/util/List;

    .line 300
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->V:Ljava/util/Set;

    .line 311
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->X:J

    .line 316
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->Y:Z

    .line 321
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->Z:I

    .line 326
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->aa:I

    .line 331
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->t:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 336
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ab:Ljava/util/List;

    .line 341
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ac:Ljava/lang/StringBuilder;

    .line 346
    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->f:Lcom/google/android/apps/gmm/map/t/b;

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    .line 351
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->v:F

    .line 370
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->y:Z

    .line 387
    const/4 v2, 0x3

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->af:[F

    .line 796
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 797
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    .line 798
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->S:Lcom/google/android/apps/gmm/map/internal/b/g;

    .line 799
    iput p7, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->z:I

    .line 800
    iput p8, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->A:I

    .line 801
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->B:Lcom/google/android/apps/gmm/map/t/k;

    .line 802
    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->C:I

    .line 803
    move/from16 v0, p12

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->D:I

    .line 804
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->G:Z

    .line 805
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->E:Z

    .line 806
    move/from16 v0, p15

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->F:Z

    .line 807
    move/from16 v0, p16

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->R:Z

    .line 808
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    .line 809
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/al;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/internal/c/al;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    .line 810
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->w:Lcom/google/android/apps/gmm/map/c/a;

    .line 811
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->f:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    .line 812
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->h:Lcom/google/android/apps/gmm/v/ad;

    .line 813
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    iput-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->e:Lcom/google/android/apps/gmm/map/internal/c/ce;

    .line 814
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p9}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->H:Ljava/util/ArrayList;

    .line 815
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p9, :cond_0

    .line 816
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->H:Ljava/util/ArrayList;

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 818
    :cond_0
    add-int/lit8 v2, p9, 0x1

    new-array v2, v2, [I

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->I:[I

    .line 820
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 822
    if-eqz p2, :cond_1

    .line 823
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-direct {v2, p0, p3}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    .line 824
    new-instance v2, Lcom/google/android/apps/gmm/v/cr;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/v/cr;-><init>(Lcom/google/android/apps/gmm/v/f;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->s:Lcom/google/android/apps/gmm/v/cp;

    .line 825
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v3, p2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    const/4 v5, 0x1

    invoke-direct {v4, v2, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 828
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->q()Lcom/google/android/apps/gmm/map/e/a;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->W:Lcom/google/android/apps/gmm/map/e/a;

    .line 831
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iput-object p0, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->p:Lcom/google/android/apps/gmm/map/legacy/a/b/i;

    .line 833
    invoke-virtual {p4}, Lcom/google/android/apps/gmm/map/b/a/ai;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/util/Timer;

    const-string v3, "Traffic auto-refresh timer"

    invoke-direct {v2, v3}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    :goto_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ad:Ljava/util/Timer;

    .line 834
    return-void

    .line 833
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Landroid/content/res/Resources;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
    .locals 19

    .prologue
    .line 638
    const/16 v1, 0x100

    .line 639
    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/a/a;->a(Landroid/content/res/Resources;I)I

    move-result v1

    .line 641
    shl-int/lit8 v13, v1, 0x1

    .line 642
    shl-int/lit8 v14, v13, 0x1

    .line 644
    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v5, v1, v14, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;-><init>(IIZZ)V

    .line 648
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;-><init>()V

    .line 649
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->q:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 652
    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;

    .line 656
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/c/a;->A_()Lcom/google/android/apps/gmm/map/internal/b/f;

    move-result-object v12

    const/4 v15, 0x4

    sget-object v16, Lcom/google/android/apps/gmm/map/t/l;->d:Lcom/google/android/apps/gmm/map/t/l;

    const/16 v17, 0x100

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object v11, v1

    move-object/from16 v18, v6

    invoke-direct/range {v7 .. v18}, Lcom/google/android/apps/gmm/map/legacy/internal/b/f;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/k;ILcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 663
    return-object v7
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/t/l;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
    .locals 26

    .prologue
    .line 596
    const/16 v1, 0x100

    .line 597
    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/a/a;->a(Landroid/content/res/Resources;I)I

    move-result v14

    .line 598
    shl-int/lit8 v15, v14, 0x1

    .line 599
    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    const/4 v1, 0x4

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v5, v1, v15, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;-><init>(IIZZ)V

    .line 609
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;-><init>()V

    .line 610
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 613
    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 618
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/c/a;->A_()Lcom/google/android/apps/gmm/map/internal/b/f;

    move-result-object v13

    const/16 v16, 0x0

    const/16 v18, 0x100

    const/16 v19, 0x100

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    sget-object v24, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p4

    move-object v12, v1

    move-object/from16 v17, p5

    move-object/from16 v25, v6

    invoke-direct/range {v7 .. v25}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/k;IIZZZZLcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    return-object v7
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
    .locals 7

    .prologue
    .line 436
    sget-object v6, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
    .locals 27

    .prologue
    .line 455
    const/16 v1, 0x100

    .line 456
    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/a/a;->a(Landroid/content/res/Resources;I)I

    move-result v15

    .line 457
    shl-int/lit8 v16, v15, 0x1

    .line 460
    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->d:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->l:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p3

    if-ne v0, v1, :cond_2

    :cond_0
    const/16 v23, 0x1

    .line 469
    :goto_0
    if-nez v23, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->r:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p3

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->s:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 473
    :cond_1
    if-eqz v23, :cond_3

    sget-object v1, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p5

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/google/android/apps/gmm/map/t/b;->d:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x1

    .line 475
    :goto_1
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    const/16 v2, 0x8

    const/4 v3, 0x0

    move/from16 v0, v16

    invoke-direct {v6, v2, v0, v1, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;-><init>(IIZZ)V

    .line 479
    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;-><init>()V

    .line 480
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    move-object/from16 v4, p3

    move-object/from16 v5, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 483
    new-instance v8, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    new-instance v14, Lcom/google/android/apps/gmm/map/internal/b/c;

    .line 488
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/c/a;->C_()Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-result-object v2

    invoke-direct {v14, v2}, Lcom/google/android/apps/gmm/map/internal/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/c/cw;)V

    const/16 v17, 0x8

    const/16 v19, 0x100

    const/16 v20, 0x100

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v24, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object/from16 v11, p2

    move-object/from16 v12, p3

    move-object v13, v1

    move-object/from16 v18, p6

    move-object/from16 v25, p5

    move-object/from16 v26, v7

    invoke-direct/range {v8 .. v26}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/k;IIZZZZLcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    return-object v8

    .line 460
    :cond_2
    const/16 v23, 0x0

    goto :goto_0

    .line 473
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Z)Lcom/google/android/apps/gmm/map/legacy/internal/b/m;
    .locals 23

    .prologue
    .line 554
    const/16 v1, 0x100

    .line 555
    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/a/a;->a(Landroid/content/res/Resources;I)I

    move-result v13

    .line 556
    shl-int/lit8 v14, v13, 0x1

    .line 557
    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v5, v1, v14, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;-><init>(IIZZ)V

    .line 566
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;-><init>()V

    .line 567
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    move-object/from16 v2, p0

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 570
    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/internal/b/m;

    .line 574
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/c/a;->A_()Lcom/google/android/apps/gmm/map/internal/b/f;

    move-result-object v12

    const/4 v15, 0x0

    sget-object v16, Lcom/google/android/apps/gmm/map/t/l;->a:Lcom/google/android/apps/gmm/map/t/l;

    const/16 v17, 0x100

    const/16 v18, 0x1

    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object v11, v1

    move/from16 v21, p5

    move-object/from16 v22, v6

    invoke-direct/range {v7 .. v22}, Lcom/google/android/apps/gmm/map/legacy/internal/b/m;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/l;IZZZZLcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    return-object v7
.end method

.method public static b(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
    .locals 26

    .prologue
    .line 510
    const/16 v1, 0x100

    .line 511
    move-object/from16 v0, p4

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/a/a;->b(Landroid/content/res/Resources;I)I

    move-result v14

    .line 512
    shl-int/lit8 v15, v14, 0x1

    .line 513
    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v5, v1, v15, v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;-><init>(IIZZ)V

    .line 524
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;-><init>()V

    .line 525
    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    move-object/from16 v4, p3

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/a;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    .line 527
    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 532
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/c/a;->A_()Lcom/google/android/apps/gmm/map/internal/b/f;

    move-result-object v13

    const/16 v16, 0x8

    sget-object v17, Lcom/google/android/apps/gmm/map/t/m;->a:Lcom/google/android/apps/gmm/map/t/m;

    const/16 v18, 0x100

    const/16 v19, 0x180

    const/16 v20, 0x1

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object v12, v1

    move-object/from16 v24, p5

    move-object/from16 v25, v6

    invoke-direct/range {v7 .. v25}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;-><init>(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/legacy/a/b/d;Lcom/google/android/apps/gmm/map/internal/b/g;IIILcom/google/android/apps/gmm/map/t/k;IIZZZZLcom/google/android/apps/gmm/map/t/b;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V

    return-object v7
.end method


# virtual methods
.method protected a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    .locals 2

    .prologue
    .line 979
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/a/e;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 716
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->q:Z

    .line 719
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a()Z

    .line 721
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ae:Ljava/util/TimerTask;

    if-eqz v0, :cond_1

    .line 722
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ae:Ljava/util/TimerTask;

    invoke-virtual {v0}, Ljava/util/TimerTask;->cancel()Z

    .line 723
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ae:Ljava/util/TimerTask;

    .line 726
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    if-eqz v0, :cond_2

    .line 727
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->e:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c()V

    .line 729
    :cond_2
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/Set;)V
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1113
    .line 1114
    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/apps/gmm/map/f/o;->e:I

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_3

    const/4 v6, 0x1

    move v15, v6

    .line 1116
    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->M:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1117
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->U:Ljava/util/List;

    move-object/from16 v0, p1

    invoke-interface {v6, v0, v7}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/List;)J

    move-result-wide v16

    .line 1119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->U:Ljava/util/List;

    move-object/from16 v18, v0

    .line 1120
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->H:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 1125
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->O:J

    cmp-long v6, v6, v16

    if-eqz v6, :cond_4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->U:Ljava/util/List;

    .line 1126
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_4

    const/4 v6, 0x1

    .line 1129
    :goto_1
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a()Z

    move-result v7

    .line 1131
    if-eqz v15, :cond_d

    .line 1132
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->D:Lcom/google/android/apps/gmm/map/f/a/i;

    move-object/from16 v20, v0

    .line 1133
    if-eqz v20, :cond_5

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/f/a/i;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v7, :cond_5

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/f/a/i;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->N:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1134
    invoke-virtual {v7, v8}, Lcom/google/android/apps/gmm/map/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/f/a/i;->b:J

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->J:Lcom/google/android/apps/gmm/shared/c/a;

    .line 1135
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/shared/c/a;->a()J

    move-result-wide v10

    sub-long/2addr v8, v10

    const-wide/16 v10, 0x5dc

    cmp-long v7, v8, v10

    if-ltz v7, :cond_5

    .line 1140
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->Q:Z

    .line 1141
    new-instance v6, Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, v20

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/f/a/i;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1142
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v8

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v9}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v9

    .line 1143
    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/apps/gmm/map/f/o;->i:F

    sget-object v11, Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;IIFLcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 1144
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1145
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    invoke-interface {v7, v6, v9}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/List;)J

    .line 1146
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->p:Z

    if-eqz v7, :cond_0

    .line 1147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    .line 1149
    iget-object v8, v6, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v8

    const/4 v10, 0x0

    const/4 v11, 0x0

    .line 1153
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v6

    iget-object v12, v6, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    const/16 v13, 0x8

    const/4 v14, 0x1

    move-object/from16 v6, v16

    .line 1147
    invoke-virtual/range {v6 .. v14}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/bc;IZ)V

    .line 1194
    :cond_0
    :goto_2
    if-eqz v20, :cond_c

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/f/a/i;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    :goto_3
    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->N:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 1223
    :cond_1
    :goto_4
    if-nez v15, :cond_2

    .line 1224
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->Q:Z

    .line 1227
    :cond_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v18

    move/from16 v3, v19

    move-object/from16 v4, p3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/Collection;ILjava/util/Set;)V

    .line 1234
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->I:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->U:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ne v6, v7, :cond_10

    const/4 v6, 0x1

    :goto_5
    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->l:Z

    .line 1237
    return-void

    .line 1114
    :cond_3
    const/4 v6, 0x0

    move v15, v6

    goto/16 :goto_0

    .line 1126
    :cond_4
    const/4 v6, 0x0

    goto/16 :goto_1

    .line 1157
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->Q:Z

    if-nez v7, :cond_b

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->y:Z

    if-eqz v7, :cond_b

    if-nez v6, :cond_6

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->P:J

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    .line 1158
    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/internal/c/al;->b()J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_b

    .line 1163
    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->af:[F

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    const/4 v8, 0x0

    iget v9, v7, Lcom/google/android/apps/gmm/map/f/j;->d:F

    aput v9, v6, v8

    const/4 v8, 0x1

    iget v9, v7, Lcom/google/android/apps/gmm/map/f/j;->e:F

    aput v9, v6, v8

    const/4 v8, 0x2

    iget v7, v7, Lcom/google/android/apps/gmm/map/f/j;->f:F

    aput v7, v6, v8

    .line 1164
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->af:[F

    .line 1165
    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v7

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v8

    .line 1164
    const/4 v9, 0x0

    aget v9, v6, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const v10, 0x33d6bf95    # 1.0E-7f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_8

    const/4 v9, 0x1

    aget v9, v6, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const v10, 0x33d6bf95    # 1.0E-7f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_8

    const/4 v9, 0x2

    aget v9, v6, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const v10, 0x33d6bf95    # 1.0E-7f

    cmpg-float v9, v9, v10

    if-gez v9, :cond_8

    const/4 v6, 0x0

    .line 1166
    :goto_6
    if-eqz v6, :cond_a

    .line 1167
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->V:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 1168
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->V:Ljava/util/Set;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Ljava/util/Set;)V

    .line 1169
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->p:Z

    if-eqz v6, :cond_7

    .line 1170
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    .line 1172
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->U:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->V:Ljava/util/Set;

    const/4 v11, 0x0

    .line 1176
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v12

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    const/4 v13, 0x4

    const/4 v14, 0x0

    .line 1170
    invoke-virtual/range {v6 .. v14}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/bc;IZ)V

    .line 1179
    :cond_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/c/al;->b()J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->P:J

    .line 1186
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->O:J

    goto/16 :goto_2

    .line 1164
    :cond_8
    const/4 v9, 0x0

    aget v9, v6, v9

    const/4 v10, 0x0

    aget v10, v6, v10

    mul-float/2addr v9, v10

    mul-int/2addr v7, v7

    int-to-float v7, v7

    div-float v7, v9, v7

    const/4 v9, 0x1

    aget v9, v6, v9

    const/4 v10, 0x1

    aget v10, v6, v10

    mul-float/2addr v9, v10

    mul-int/2addr v8, v8

    int-to-float v8, v8

    div-float v8, v9, v8

    add-float/2addr v7, v8

    float-to-double v8, v7

    const-wide/high16 v10, 0x4022000000000000L    # 9.0

    cmpg-double v7, v8, v10

    if-gez v7, :cond_9

    const/4 v7, 0x2

    aget v6, v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    float-to-double v6, v6

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    cmpg-double v6, v6, v8

    if-gez v6, :cond_9

    const/4 v6, 0x1

    goto :goto_6

    :cond_9
    const/4 v6, 0x0

    goto :goto_6

    .line 1189
    :cond_a
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v7, v6, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    monitor-enter v7

    :try_start_0
    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->b()V

    monitor-exit v7

    goto/16 :goto_2

    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 1191
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->Q:Z

    if-eqz v6, :cond_0

    .line 1192
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->j:Lcom/google/android/apps/gmm/map/legacy/a/b/a;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/legacy/a/b/a;->a()V

    goto/16 :goto_2

    .line 1194
    :cond_c
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 1195
    :cond_d
    if-nez v6, :cond_e

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->P:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    .line 1196
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/c/al;->b()J

    move-result-wide v10

    cmp-long v6, v8, v10

    if-nez v6, :cond_e

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->i:Z

    if-nez v6, :cond_e

    if-eqz v7, :cond_1

    .line 1199
    :cond_e
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->i:Z

    .line 1200
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->V:Ljava/util/Set;

    invoke-interface {v6}, Ljava/util/Set;->clear()V

    .line 1201
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->V:Ljava/util/Set;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Ljava/util/Set;)V

    .line 1202
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->p:Z

    if-eqz v6, :cond_f

    .line 1203
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    .line 1205
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->U:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->V:Ljava/util/Set;

    const/4 v11, 0x0

    .line 1209
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v12

    iget-object v12, v12, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    const/16 v13, 0x8

    const/4 v14, 0x0

    .line 1203
    invoke-virtual/range {v6 .. v14}, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->a(Lcom/google/android/apps/gmm/map/internal/b/e;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Ljava/util/Set;Ljava/util/Set;Lcom/google/android/apps/gmm/map/b/a/bc;IZ)V

    .line 1212
    :cond_f
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/c/al;->b()J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->P:J

    .line 1219
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->O:J

    goto/16 :goto_4

    .line 1234
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_5
.end method

.method protected a(Lcom/google/android/apps/gmm/map/internal/b/e;)V
    .locals 0

    .prologue
    .line 667
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    .line 668
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/bv;)V
    .locals 3

    .prologue
    .line 943
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/al;->a:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/map/internal/c/cd;->a(Lcom/google/android/apps/gmm/map/internal/c/bv;)V

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/c/al;->b:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 944
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 945
    monitor-exit p0

    return-void

    .line 943
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 0

    .prologue
    .line 838
    return-void
.end method

.method protected a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/Collection;ILjava/util/Set;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;I",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 992
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->I:[I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 996
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    monitor-enter v7

    .line 999
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V

    .line 1001
    const/4 v2, 0x0

    move v6, v2

    :goto_0
    move/from16 v0, p3

    if-gt v6, v0, :cond_16

    .line 1003
    move/from16 v0, p3

    if-ne v6, v0, :cond_6

    .line 1004
    const/4 v2, 0x0

    move-object v5, v2

    .line 1010
    :goto_1
    invoke-interface/range {p2 .. p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 1013
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    move-result-object v3

    .line 1014
    if-eqz v3, :cond_15

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->d()Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1015
    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1017
    move-object v0, v3

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;

    move-object v2, v0

    .line 1032
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->m:Z

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->e()Z

    move-result v4

    if-eqz v4, :cond_1

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->m:Z

    if-ne v4, v3, :cond_7

    .line 1033
    :cond_1
    :goto_3
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->n:Z

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->o:Z

    if-eq v4, v3, :cond_3

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->o:Z

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->b:Lcom/google/android/apps/gmm/map/t/am;

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->o:Z

    if-eqz v3, :cond_14

    iget v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->p:F

    const v9, 0x3e4ccccd    # 0.2f

    mul-float/2addr v3, v9

    :goto_4
    iget-boolean v9, v4, Lcom/google/android/apps/gmm/map/t/am;->p:Z

    if-eqz v9, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_2
    iput v3, v4, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 1034
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->w:Lcom/google/android/apps/gmm/map/c/a;

    .line 1035
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v3

    .line 1036
    iget-wide v10, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->o:D

    if-eqz v3, :cond_4

    :cond_4
    double-to-float v3, v10

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->c:Lcom/google/android/apps/gmm/map/t/e;

    iput v3, v4, Lcom/google/android/apps/gmm/map/t/e;->c:F

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->d:Lcom/google/android/apps/gmm/map/t/e;

    iput v3, v2, Lcom/google/android/apps/gmm/map/t/e;->c:F

    .line 1038
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->I:[I

    aget v3, v2, v6

    add-int/lit8 v3, v3, 0x1

    aput v3, v2, v6

    .line 1039
    invoke-interface/range {p4 .. p4}, Ljava/util/Set;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->z:I

    if-ne v2, v3, :cond_0

    .line 1055
    :cond_5
    invoke-interface/range {p4 .. p4}, Ljava/util/Set;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->z:I

    if-eq v2, v3, :cond_16

    if-eqz v5, :cond_16

    .line 1057
    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v2

    if-eqz v2, :cond_16

    .line 1001
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move-object/from16 p2, v5

    goto/16 :goto_0

    .line 1006
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->H:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    .line 1007
    invoke-interface {v2}, Ljava/util/Set;->clear()V

    move-object v5, v2

    goto/16 :goto_1

    .line 1032
    :cond_7
    iput-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->m:Z

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->m:Z

    if-eqz v3, :cond_e

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    if-nez v3, :cond_e

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    const/high16 v4, -0x70000000

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    new-instance v10, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v11, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    iget v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    invoke-direct {v10, v11, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    const/4 v3, 0x0

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/b/a/ai;->b()Z

    move-result v11

    if-nez v11, :cond_8

    const/high16 v4, -0x6f010000

    const/high16 v3, -0x3e200000    # -28.0f

    :cond_8
    new-instance v11, Lcom/google/android/apps/gmm/map/t/q;

    sget-object v12, Lcom/google/android/apps/gmm/map/t/l;->z:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v11, v12}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    iput-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v12, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, 0xe

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v14, "Tile Bounds ["

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iput-object v12, v11, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    const/4 v12, 0x5

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v9, v12}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/co;)V

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    iget-boolean v12, v11, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v12, :cond_9

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_9
    iget-object v12, v11, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v10, v12, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v10, 0x1

    iput-boolean v10, v11, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/high16 v12, 0x40000000    # 2.0f

    iget v11, v11, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int v11, v12, v11

    int-to-float v11, v11

    iget-boolean v12, v10, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v12, :cond_a

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_a
    iput v11, v10, Lcom/google/android/apps/gmm/map/t/q;->d:F

    const/4 v11, 0x0

    iput-boolean v11, v10, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    new-instance v11, Lcom/google/android/apps/gmm/v/x;

    invoke-direct {v11, v4}, Lcom/google/android/apps/gmm/v/x;-><init>(I)V

    invoke-virtual {v10, v11}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    new-instance v11, Lcom/google/android/apps/gmm/v/bd;

    const/high16 v12, 0x40000000    # 2.0f

    invoke-direct {v11, v12}, Lcom/google/android/apps/gmm/v/bd;-><init>(F)V

    invoke-virtual {v10, v11}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v11, v11, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v12, 0x302

    const/16 v13, 0x303

    invoke-virtual {v11, v12, v13}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v11

    invoke-virtual {v10, v11}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v10, Lcom/google/android/apps/gmm/map/t/q;

    sget-object v11, Lcom/google/android/apps/gmm/map/t/l;->z:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v10, v11}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    iput-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/lit8 v13, v13, 0x14

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "Tile Bounds Label ["

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "]"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    new-instance v10, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v11, v11, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    iget-object v12, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v12, v12, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    iget-object v13, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/high16 v14, 0x40000000    # 2.0f

    iget v13, v13, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int v13, v14, v13

    add-int/2addr v12, v13

    invoke-direct {v10, v11, v12}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-virtual {v10, v10}, Lcom/google/android/apps/gmm/map/b/a/y;->h(Lcom/google/android/apps/gmm/map/b/a/y;)V

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    iget-boolean v12, v11, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v12, :cond_b

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_b
    iget-object v12, v11, Lcom/google/android/apps/gmm/map/t/q;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v10, v10, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v10, v12, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    const/4 v10, 0x1

    iput-boolean v10, v11, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    const/4 v11, 0x6

    iget-object v12, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v9, v11}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v9

    invoke-virtual {v10, v9}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/co;)V

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    const/4 v10, 0x0

    const/4 v11, 0x0

    iget-boolean v12, v9, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v12, :cond_c

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_c
    iget-object v12, v9, Lcom/google/android/apps/gmm/map/t/q;->e:[F

    const/4 v13, 0x0

    aput v10, v12, v13

    iget-object v10, v9, Lcom/google/android/apps/gmm/map/t/q;->e:[F

    const/4 v12, 0x1

    aput v3, v10, v12

    iget-object v3, v9, Lcom/google/android/apps/gmm/map/t/q;->e:[F

    const/4 v10, 0x2

    aput v11, v3, v10

    const/4 v3, 0x1

    iput-boolean v3, v9, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    const/16 v3, 0x100

    const/16 v9, 0x1c

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_4444:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v9, v10}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v9}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v10, Ljava/lang/StringBuilder;

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->n:Z

    if-eqz v3, :cond_f

    iget v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->g:I

    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v12, 0xc

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "U"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_5
    invoke-direct {v10, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v11, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v3, v11, :cond_d

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->f:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/ai;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/ac;->m:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->e:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/high16 v10, 0x40a00000    # 5.0f

    const/high16 v11, 0x41b00000    # 22.0f

    sget-object v12, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->u:Landroid/graphics/Paint;

    invoke-virtual {v4, v3, v10, v11, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    new-instance v4, Lcom/google/android/apps/gmm/v/ci;

    new-instance v10, Lcom/google/android/apps/gmm/v/ar;

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v11, v11, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    const/4 v12, 0x0

    invoke-direct {v10, v9, v11, v12}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    const/4 v9, 0x0

    invoke-direct {v4, v10, v9}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    new-instance v4, Lcom/google/android/apps/gmm/v/cd;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/v/cd;-><init>()V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/4 v9, 0x1

    const/16 v10, 0x303

    invoke-virtual {v4, v9, v10}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/v/ai;)V

    :cond_e
    iget-boolean v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->m:Z

    if-eqz v3, :cond_10

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/t/p;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->a(Lcom/google/android/apps/gmm/map/t/p;)V

    goto/16 :goto_3

    .line 1062
    :catchall_0
    move-exception v2

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 1032
    :cond_f
    :try_start_1
    const-string v3, ""

    goto :goto_5

    :cond_10
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->k:Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v3, :cond_12

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v9, Lcom/google/android/apps/gmm/v/af;

    const/4 v10, 0x0

    invoke-direct {v9, v3, v10}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v4, v9}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    const/4 v4, 0x0

    iget-boolean v9, v3, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v9, :cond_11

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_11
    int-to-byte v4, v4

    iput-byte v4, v3, Lcom/google/android/apps/gmm/v/aa;->w:B

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_12
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->l:Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v3, :cond_1

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->q:Lcom/google/android/apps/gmm/v/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v9, Lcom/google/android/apps/gmm/v/af;

    const/4 v10, 0x0

    invoke-direct {v9, v3, v10}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v4, v9}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    const/4 v4, 0x0

    iget-boolean v9, v3, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v9, :cond_13

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_13
    int-to-byte v4, v4

    iput-byte v4, v3, Lcom/google/android/apps/gmm/v/aa;->w:B

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->s:Ljava/util/Set;

    invoke-interface {v4, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 1033
    :cond_14
    iget v3, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/au;->p:F

    goto/16 :goto_4

    .line 1045
    :cond_15
    if-eqz v5, :cond_0

    .line 1046
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->M:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v2

    .line 1047
    if-eqz v2, :cond_0

    .line 1048
    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 1062
    :cond_16
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V
    .locals 3

    .prologue
    .line 1069
    monitor-enter p0

    if-nez p1, :cond_1

    .line 1082
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 1074
    :cond_1
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->q:Z

    if-eqz v0, :cond_0

    .line 1078
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    if-eqz v0, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1069
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/am;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1481
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->R:Z

    if-nez v1, :cond_0

    .line 1495
    :goto_0
    return-void

    .line 1484
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    monitor-enter v2

    .line 1485
    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->L:Z

    if-eqz v1, :cond_1

    .line 1486
    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Adding labels: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1487
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->L:Z

    :cond_1
    move v1, v0

    .line 1489
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 1490
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    .line 1491
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    instance-of v3, v0, Lcom/google/android/apps/gmm/map/o/ak;

    if-eqz v3, :cond_2

    .line 1492
    check-cast v0, Lcom/google/android/apps/gmm/map/o/ak;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/o/ak;->a(Lcom/google/android/apps/gmm/map/o/am;)V

    .line 1489
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1495
    :cond_3
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 0

    .prologue
    .line 1500
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->x:Lcom/google/android/apps/gmm/map/o/p;

    .line 1501
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/af;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 673
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->q:Z

    .line 674
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/t/af;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    const-string v2, "GLState should not be null"

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->d:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V

    :cond_1
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->o:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    .line 675
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    if-nez v0, :cond_2

    .line 679
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->S:Lcom/google/android/apps/gmm/map/internal/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->D:I

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->G:Z

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    .line 680
    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/b/g;->a(Lcom/google/android/apps/gmm/map/b/a/ai;IZLcom/google/android/apps/gmm/map/internal/c/ce;)Lcom/google/android/apps/gmm/map/internal/b/e;

    move-result-object v0

    .line 679
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/b/e;)V

    .line 681
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->S:Lcom/google/android/apps/gmm/map/internal/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->G:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    .line 682
    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/b/g;->a(Lcom/google/android/apps/gmm/map/b/a/ai;ZLcom/google/android/apps/gmm/map/internal/c/ce;)Lcom/google/android/apps/gmm/map/internal/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->K:Lcom/google/android/apps/gmm/map/internal/b/d;

    .line 683
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->K:Lcom/google/android/apps/gmm/map/internal/b/d;

    if-nez v0, :cond_2

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/internal/b/d;

    if-eqz v0, :cond_5

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->K:Lcom/google/android/apps/gmm/map/internal/b/d;

    .line 695
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ad:Ljava/util/Timer;

    if-eqz v0, :cond_3

    .line 696
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/j;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ae:Ljava/util/TimerTask;

    .line 704
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ad:Ljava/util/Timer;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->ae:Ljava/util/TimerTask;

    const-wide/16 v2, 0x0

    sget-wide v4, Lcom/google/android/apps/gmm/map/b/a/ai;->a:J

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 708
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    if-eqz v0, :cond_4

    .line 709
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->e:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 711
    :cond_4
    return-void

    .line 690
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad out-of-bounds coord generator"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 3

    .prologue
    .line 1334
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->f:Lcom/google/android/apps/gmm/map/t/b;

    if-eq p1, v0, :cond_1

    .line 1351
    :cond_0
    :goto_0
    return-void

    .line 1347
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    if-eq v0, p1, :cond_0

    .line 1348
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    .line 1349
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 903
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 905
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/g;-><init>()V

    .line 906
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/internal/c/g;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/g;->a()Lcom/google/android/apps/gmm/map/internal/c/f;

    move-result-object v0

    .line 907
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z

    .line 911
    :goto_1
    return-void

    .line 903
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 909
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->d:Lcom/google/android/apps/gmm/map/internal/c/bv;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/c/bv;)V

    goto :goto_1
.end method

.method protected a(Ljava/util/Set;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 972
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 2

    .prologue
    .line 934
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/c/al;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z

    move-result v0

    .line 935
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 936
    monitor-exit p0

    return v0

    .line 934
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V
    .locals 3

    .prologue
    .line 1089
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1091
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->q:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    .line 1102
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 1095
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    if-eqz v0, :cond_3

    .line 1096
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1099
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->x:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v0, :cond_1

    .line 1100
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->x:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/o/p;->c(Lcom/google/android/apps/gmm/map/o/ak;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public final declared-synchronized d()Lcom/google/android/apps/gmm/map/internal/c/cd;
    .locals 1

    .prologue
    .line 948
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->T:Lcom/google/android/apps/gmm/map/internal/c/al;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/al;->a()Lcom/google/android/apps/gmm/map/internal/c/cd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1505
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "tileType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 1506
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "drawMode"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->u:Lcom/google/android/apps/gmm/map/t/b;

    .line 1507
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maxTilesPerView"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->z:I

    .line 1508
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maxTilesToFetch"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->A:I

    .line 1509
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "drawOrder"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->B:Lcom/google/android/apps/gmm/map/t/k;

    .line 1510
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "fetchMissingAncestorTiles"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->E:Z

    .line 1511
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "allowMultiZoom"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->G:Z

    .line 1512
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "prefetchAncestors"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->F:Z

    .line 1513
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "tileSize"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->C:I

    .line 1514
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "allowMultiZoom"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->G:Z

    .line 1515
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "isContributingLabels"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->R:Z

    .line 1516
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maxTileSize"

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->D:I

    .line 1517
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 1518
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
