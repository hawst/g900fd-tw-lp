.class public final enum Lcom/google/android/apps/gmm/directions/aw;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/aw;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/aw;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/aw;

.field private static final synthetic c:[Lcom/google/android/apps/gmm/directions/aw;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/directions/aw;

    const-string v1, "ODELAY_CARDS"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/aw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/aw;->a:Lcom/google/android/apps/gmm/directions/aw;

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/directions/aw;

    const-string v1, "TRIP_CARDS"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/directions/aw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/aw;->b:Lcom/google/android/apps/gmm/directions/aw;

    .line 77
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/aw;

    sget-object v1, Lcom/google/android/apps/gmm/directions/aw;->a:Lcom/google/android/apps/gmm/directions/aw;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/directions/aw;->b:Lcom/google/android/apps/gmm/directions/aw;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/directions/aw;->c:[Lcom/google/android/apps/gmm/directions/aw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 77
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/aw;
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/google/android/apps/gmm/directions/aw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/aw;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/aw;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/directions/aw;->c:[Lcom/google/android/apps/gmm/directions/aw;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/aw;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/aw;

    return-object v0
.end method
