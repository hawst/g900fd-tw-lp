.class public Lcom/google/android/apps/gmm/map/internal/d/b/aq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/as;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/d/b/ar;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/map/internal/c/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "SoftInMemoryTileCache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p3, v1, p1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/l;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v1, v4, v4, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->b:Lcom/google/android/apps/gmm/map/internal/c/l;

    .line 57
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V
    .locals 3

    .prologue
    .line 61
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/b/ar;

    invoke-direct {v2, p2}, Lcom/google/android/apps/gmm/map/internal/d/b/ar;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    invoke-virtual {v0, p1, v2}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 64
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 97
    monitor-exit v1

    .line 98
    const/4 v0, 0x1

    return v0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->b:Lcom/google/android/apps/gmm/map/internal/c/l;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->b:Lcom/google/android/apps/gmm/map/internal/c/l;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V

    .line 70
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 79
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v2

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/b/ar;

    .line 81
    if-eqz v0, :cond_3

    .line 82
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ar;->b:Lcom/google/android/apps/gmm/map/internal/c/bo;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ar;->b:Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 83
    :goto_0
    if-nez v0, :cond_0

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/aq;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    :cond_0
    monitor-exit v2

    .line 89
    :goto_1
    return-object v0

    .line 82
    :cond_1
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ar;->a:Ljava/lang/ref/SoftReference;

    if-nez v3, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/ar;->a:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bo;

    goto :goto_0

    .line 89
    :cond_3
    monitor-exit v2

    move-object v0, v1

    goto :goto_1

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
