.class public Lcom/google/android/apps/gmm/reportmapissue/n;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/b/a/q;

.field private final b:Lcom/google/e/a/a/a/b;

.field private final c:D

.field private final d:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field private final e:Lcom/google/maps/g/hy;

.field private final f:Lcom/google/o/b/a/v;

.field private final h:Z

.field private final i:Lcom/google/e/a/a/a/b;

.field private final j:Lcom/google/r/b/a/ado;

.field private final k:Lcom/google/android/apps/gmm/reportmapissue/a/h;

.field private l:Lcom/google/e/a/a/a/b;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/e/a/a/a/b;DLcom/google/android/apps/gmm/map/indoor/d/f;Lcom/google/maps/g/hy;Lcom/google/o/b/a/v;ZLcom/google/e/a/a/a/b;Lcom/google/r/b/a/ado;Lcom/google/android/apps/gmm/reportmapissue/a/h;)V
    .locals 3
    .param p5    # Lcom/google/android/apps/gmm/map/indoor/d/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/o/b/a/v;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p11    # Lcom/google/android/apps/gmm/reportmapissue/a/h;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 96
    sget-object v0, Lcom/google/r/b/a/el;->bY:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/bb;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 98
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 99
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/e/a/a/a/b;

    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->b:Lcom/google/e/a/a/a/b;

    .line 100
    iput-wide p3, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->c:D

    .line 101
    iput-object p5, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 102
    iput-object p6, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->e:Lcom/google/maps/g/hy;

    .line 103
    iput-object p7, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->f:Lcom/google/o/b/a/v;

    .line 104
    iput-boolean p8, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->h:Z

    .line 105
    iput-object p9, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->i:Lcom/google/e/a/a/a/b;

    .line 106
    iput-object p10, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->j:Lcom/google/r/b/a/ado;

    .line 107
    iput-object p11, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->k:Lcom/google/android/apps/gmm/reportmapissue/a/h;

    .line 108
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/indoor/d/f;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;Landroid/content/res/Resources;ZLcom/google/android/apps/gmm/reportmapissue/a/h;)Lcom/google/android/apps/gmm/reportmapissue/n;
    .locals 15
    .param p1    # Lcom/google/android/apps/gmm/map/indoor/d/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/apps/gmm/reportmapissue/a/h;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-virtual/range {p3 .. p3}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v5, 0x0

    .line 69
    :goto_0
    invoke-interface/range {p4 .. p4}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v2

    .line 70
    if-eqz v2, :cond_1

    .line 71
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v10

    .line 74
    :goto_1
    const/4 v2, 0x5

    const/high16 v3, 0x40c00000    # 6.0f

    .line 75
    invoke-virtual/range {p5 .. p5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 74
    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 77
    float-to-double v2, v2

    .line 78
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/f/o;->g()D

    move-result-wide v6

    div-double v6, v2, v6

    .line 81
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 80
    move-object/from16 v0, p5

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/search/bc;->a(Landroid/graphics/Point;Landroid/content/res/Resources;)Lcom/google/e/a/a/a/b;

    move-result-object v12

    .line 84
    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/gmm/base/i/f;->a(Landroid/content/res/Resources;)Lcom/google/r/b/a/ado;

    move-result-object v13

    .line 86
    new-instance v3, Lcom/google/android/apps/gmm/reportmapissue/n;

    move-object v4, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move/from16 v11, p6

    move-object/from16 v14, p7

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/gmm/reportmapissue/n;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/e/a/a/a/b;DLcom/google/android/apps/gmm/map/indoor/d/f;Lcom/google/maps/g/hy;Lcom/google/o/b/a/v;ZLcom/google/e/a/a/a/b;Lcom/google/r/b/a/ado;Lcom/google/android/apps/gmm/reportmapissue/a/h;)V

    return-object v3

    .line 67
    :cond_0
    sget-object v3, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v5

    goto :goto_0

    .line 71
    :cond_1
    const/4 v10, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected final S_()Lcom/google/b/a/ak;
    .locals 5

    .prologue
    .line 174
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->S_()Lcom/google/b/a/ak;

    move-result-object v1

    const-string v0, "location"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 175
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "radiusMeters"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->c:D

    .line 176
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "level"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 177
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "loggingParams"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->e:Lcom/google/maps/g/hy;

    .line 178
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "userLocationDescriptor"

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->f:Lcom/google/o/b/a/v;

    .line 179
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v1
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 150
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->l:Lcom/google/e/a/a/a/b;

    .line 151
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 112
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/bb;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 114
    const/16 v2, 0xd

    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/maps/a/a/a;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v8, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v7, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->b:Lcom/google/e/a/a/a/b;

    iget-object v2, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v7, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 117
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 118
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->i:Lcom/google/e/a/a/a/b;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v0, :cond_1

    .line 121
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    new-instance v3, Lcom/google/e/a/a/a/b;

    sget-object v4, Lcom/google/r/b/a/b/t;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v3, v4}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/l;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v6, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget v4, v2, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    const/high16 v5, -0x80000000

    if-eq v4, v5, :cond_0

    iget v2, v2, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    int-to-long v4, v2

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v4, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v8, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_0
    iget-object v2, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->e:Lcom/google/maps/g/hy;

    if-eqz v0, :cond_2

    .line 124
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->e:Lcom/google/maps/g/hy;

    sget-object v3, Lcom/google/maps/g/b/r;->a:Lcom/google/e/a/a/a/d;

    .line 125
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 124
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 127
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->f:Lcom/google/o/b/a/v;

    if-eqz v0, :cond_3

    .line 128
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->f:Lcom/google/o/b/a/v;

    sget-object v3, Lcom/google/o/b/a/a/a;->f:Lcom/google/e/a/a/a/d;

    .line 129
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 128
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 132
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->h:Z

    if-eqz v0, :cond_4

    .line 133
    const/16 v2, 0xc

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 137
    :cond_4
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->j:Lcom/google/r/b/a/ado;

    sget-object v3, Lcom/google/r/b/a/b/aw;->r:Lcom/google/e/a/a/a/d;

    .line 138
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 137
    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 141
    const/16 v2, 0xe

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v2, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 145
    return-object v1
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 157
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->k:Lcom/google/android/apps/gmm/reportmapissue/a/h;

    if-eqz v0, :cond_0

    .line 159
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->l:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->k:Lcom/google/android/apps/gmm/reportmapissue/a/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->l:Lcom/google/e/a/a/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/a/h;->a(Lcom/google/e/a/a/a/b;)V

    .line 169
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/n;->k:Lcom/google/android/apps/gmm/reportmapissue/a/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/reportmapissue/a/h;->af_()V

    goto :goto_0
.end method
