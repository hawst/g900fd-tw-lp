.class public Lcom/google/android/apps/gmm/map/t/af;
.super Lcom/google/android/apps/gmm/v/bi;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c;
.implements Lcom/google/android/apps/gmm/map/internal/vector/k;


# static fields
.field private static final v:J


# instance fields
.field public final a:Lcom/google/android/apps/gmm/v/ad;

.field public b:Lcom/google/android/apps/gmm/map/t/ak;

.field public final c:Lcom/google/android/apps/gmm/shared/net/ad;

.field public d:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

.field public e:Lcom/google/android/apps/gmm/map/a/b;

.field final f:Lcom/google/android/apps/gmm/v/f;

.field public g:Lcom/google/android/apps/gmm/map/internal/a;

.field private final p:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile q:Z

.field private final r:Lcom/google/android/apps/gmm/map/util/a/b;

.field private final s:Lcom/google/android/apps/gmm/map/internal/c/cw;

.field private final t:Landroid/content/res/Resources;

.field private u:J

.field private final w:Lcom/google/android/apps/gmm/map/t/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 106
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/t/af;->v:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/internal/c/cw;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/d;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 156
    invoke-direct {p0, p7, p8}, Lcom/google/android/apps/gmm/v/bi;-><init>(II)V

    .line 82
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 87
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/t/af;->q:Z

    .line 101
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/t/af;->u:J

    .line 114
    new-instance v0, Lcom/google/android/apps/gmm/map/t/ag;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/t/ag;-><init>(Lcom/google/android/apps/gmm/map/t/af;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->f:Lcom/google/android/apps/gmm/v/f;

    .line 157
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    .line 158
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 159
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/t/af;->r:Lcom/google/android/apps/gmm/map/util/a/b;

    .line 160
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/t/af;->s:Lcom/google/android/apps/gmm/map/internal/c/cw;

    .line 161
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/t/af;->t:Landroid/content/res/Resources;

    .line 162
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/bi;->o:Lcom/google/android/apps/gmm/v/be;

    .line 165
    :cond_0
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/t/af;->w:Lcom/google/android/apps/gmm/map/t/d;

    .line 166
    return-void
.end method

.method private static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 191
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const-string v1, "GmmRenderTarget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GL error set on entry to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", error="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 5

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 326
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    .line 327
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 326
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/t/af;->a(FFFF)V

    .line 328
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/v/aq;)V
    .locals 0

    .prologue
    .line 178
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bi;->a(Lcom/google/android/apps/gmm/v/aq;)V

    .line 179
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    check-cast v0, Lcom/google/android/apps/gmm/v/i;

    .line 400
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/i;->c()Z

    move-result v0

    .line 403
    sget-boolean v1, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/t/af;->q:Z

    if-eqz v1, :cond_0

    .line 404
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/af;->o:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/v/be;->k()V

    .line 405
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/af;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 407
    :cond_0
    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->e:Lcom/google/android/apps/gmm/map/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/a/b;->V_()V

    .line 413
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 304
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->o:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->g()V

    .line 307
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 311
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/t/af;->q:Z

    .line 312
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->o:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->h()V

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->w:Lcom/google/android/apps/gmm/map/t/d;

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->w:Lcom/google/android/apps/gmm/map/t/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/d;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/t/d;->e:J

    sub-long/2addr v2, v4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/d;->a:[J

    iget v4, v0, Lcom/google/android/apps/gmm/map/t/d;->d:I

    aput-wide v2, v1, v4

    iget v1, v0, Lcom/google/android/apps/gmm/map/t/d;->d:I

    add-int/lit8 v1, v1, 0x1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_2

    iput v6, v0, Lcom/google/android/apps/gmm/map/t/d;->d:I

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/d;->c:Z

    .line 319
    :cond_1
    :goto_0
    return-void

    .line 317
    :cond_2
    iput v1, v0, Lcom/google/android/apps/gmm/map/t/d;->d:I

    goto :goto_0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/c/a/a;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v0

    return-object v0
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 4

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->w:Lcom/google/android/apps/gmm/map/t/d;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->w:Lcom/google/android/apps/gmm/map/t/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/d;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/t/d;->e:J

    .line 234
    :cond_0
    const-string v0, "GL_onDrawFrame"

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->s:Lcom/google/android/apps/gmm/map/internal/c/cw;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/cw;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/af;->c:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->b(Lcom/google/android/apps/gmm/shared/net/ad;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->s:Lcom/google/android/apps/gmm/map/internal/c/cw;

    new-instance v1, Lcom/google/android/apps/gmm/map/t/ah;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/t/ah;-><init>(Lcom/google/android/apps/gmm/map/t/af;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/internal/c/cy;)V

    .line 286
    :goto_0
    return-void

    .line 269
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->b:Lcom/google/android/apps/gmm/map/t/ak;

    if-eqz v0, :cond_3

    .line 271
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_2

    .line 272
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/t/af;->q:Z

    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->p:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->o:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->i()V

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->b:Lcom/google/android/apps/gmm/map/t/ak;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/t/ak;->c()V

    .line 280
    sget-boolean v0, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v0, :cond_3

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->o:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/be;->j()V

    .line 285
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/ad;->b()V

    goto :goto_0
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 1

    .prologue
    .line 211
    if-lez p2, :cond_0

    if-gtz p3, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 214
    :cond_1
    const-string v0, "onSurfaceChanged"

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/t/af;->a(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p0, p2, p3}, Lcom/google/android/apps/gmm/map/t/af;->a(II)V

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    goto :goto_0
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 4

    .prologue
    .line 199
    const-string v0, "onSurfaceCreated"

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/t/af;->a(Ljava/lang/String;)V

    .line 200
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/t/af;->t:Landroid/content/res/Resources;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/t/af;->r:Lcom/google/android/apps/gmm/map/util/a/b;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;-><init>(Lcom/google/android/apps/gmm/v/ad;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->b:Lcom/google/android/apps/gmm/map/t/ak;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->b:Lcom/google/android/apps/gmm/map/t/ak;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/t/ak;->b()V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/af;->a:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/ad;->c()V

    .line 206
    return-void
.end method
