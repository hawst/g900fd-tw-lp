.class public Lcom/google/android/apps/gmm/aa/a/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:Lcom/google/android/apps/gmm/shared/b/a;

.field public final d:Lcom/google/android/apps/gmm/navigation/a/d;

.field public final e:Lcom/google/android/apps/gmm/z/a/b;

.field public final f:Ljava/lang/String;

.field public g:Z

.field public h:I

.field public i:I

.field private final j:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/gmm/aa/a/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/aa/a/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/a/d;Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/z/a/b;)V
    .locals 7

    .prologue
    .line 71
    sget v0, Lcom/google/android/apps/gmm/l;->oM:I

    invoke-virtual {p2, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->oP:I

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->oO:I

    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/l;->oK:I

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/l;->oH:I

    invoke-virtual {p2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->oQ:I

    invoke-virtual {p2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v6, Lcom/google/android/apps/gmm/l;->oN:I

    invoke-virtual {p2, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/aa/a/c;-><init>(Lcom/google/android/apps/gmm/navigation/a/d;Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/z/a/b;)V

    .line 73
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/navigation/a/d;Landroid/content/Context;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/z/a/b;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/navigation/a/d;",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/shared/b/a;",
            "Lcom/google/b/c/cv",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/apps/gmm/z/a/b;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/a/c;->b:Landroid/content/Context;

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/a/c;->d:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 81
    iput-object p3, p0, Lcom/google/android/apps/gmm/aa/a/c;->c:Lcom/google/android/apps/gmm/shared/b/a;

    .line 82
    iput-object p5, p0, Lcom/google/android/apps/gmm/aa/a/c;->e:Lcom/google/android/apps/gmm/z/a/b;

    .line 84
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aG:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v1, 0x2

    invoke-virtual {p3, v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/aa/a/c;->h:I

    .line 85
    sget v0, Lcom/google/android/apps/gmm/l;->oL:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    sget v2, Lcom/google/android/apps/gmm/l;->oI:I

    .line 86
    invoke-virtual {p2, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    .line 85
    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/a/c;->f:Ljava/lang/String;

    .line 87
    iput-object p4, p0, Lcom/google/android/apps/gmm/aa/a/c;->j:Lcom/google/b/c/cv;

    .line 88
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->aH:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {p3, v0, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/aa/a/c;->i:I

    .line 89
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 158
    iget v0, p0, Lcom/google/android/apps/gmm/aa/a/c;->i:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/c;->j:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 159
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/aa/a/c;->i:I

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/c;->j:Lcom/google/b/c/cv;

    iget v1, p0, Lcom/google/android/apps/gmm/aa/a/c;->i:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/aa/a/c;->i:I

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
