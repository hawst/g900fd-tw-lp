.class public final enum Lcom/google/android/apps/gmm/car/a/i;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/car/a/i;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/car/a/i;

.field public static final enum b:Lcom/google/android/apps/gmm/car/a/i;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/car/a/i;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/car/a/i;

    const-string v1, "HEADS_UP_GUIDANCE"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/car/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/car/a/i;->a:Lcom/google/android/apps/gmm/car/a/i;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/car/a/i;

    const-string v1, "STREAM_GUIDANCE"

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/apps/gmm/car/a/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/car/a/i;->b:Lcom/google/android/apps/gmm/car/a/i;

    .line 24
    new-array v0, v4, [Lcom/google/android/apps/gmm/car/a/i;

    sget-object v1, Lcom/google/android/apps/gmm/car/a/i;->a:Lcom/google/android/apps/gmm/car/a/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/car/a/i;->b:Lcom/google/android/apps/gmm/car/a/i;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/car/a/i;->d:[Lcom/google/android/apps/gmm/car/a/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 31
    iput p3, p0, Lcom/google/android/apps/gmm/car/a/i;->c:I

    .line 32
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/car/a/i;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/car/a/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/a/i;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/car/a/i;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/gmm/car/a/i;->d:[Lcom/google/android/apps/gmm/car/a/i;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/car/a/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/car/a/i;

    return-object v0
.end method
