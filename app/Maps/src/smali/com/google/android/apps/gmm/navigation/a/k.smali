.class public Lcom/google/android/apps/gmm/navigation/a/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/av;


# instance fields
.field final a:Landroid/content/Context;

.field private final b:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final c:Lcom/google/android/apps/gmm/navigation/a/s;

.field private final d:Lcom/google/android/apps/gmm/shared/b/a;

.field private e:Lcom/google/android/apps/gmm/navigation/a/n;

.field private f:Lcom/google/maps/g/a/al;

.field private final g:Lcom/google/android/apps/gmm/navigation/a/t;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/navigation/a/s;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/a/l;-><init>(Lcom/google/android/apps/gmm/navigation/a/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/k;->g:Lcom/google/android/apps/gmm/navigation/a/t;

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/k;->a:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/k;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 60
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/k;->c:Lcom/google/android/apps/gmm/navigation/a/s;

    .line 61
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/a/k;->d:Lcom/google/android/apps/gmm/shared/b/a;

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/k;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/m;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/a/m;-><init>(Lcom/google/android/apps/gmm/navigation/a/k;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 64
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 140
    move-object v0, p1

    :goto_0
    instance-of v1, v0, Lcom/google/android/apps/gmm/navigation/a/b/e;

    if-eqz v1, :cond_0

    .line 141
    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/b/e;->a:Lcom/google/android/apps/gmm/navigation/a/b/a;

    goto :goto_0

    .line 144
    :cond_0
    instance-of v1, v0, Lcom/google/android/apps/gmm/navigation/a/b/d;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 145
    check-cast v1, Lcom/google/android/apps/gmm/navigation/a/b/d;

    .line 148
    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/k;->f:Lcom/google/maps/g/a/al;

    iget-object v6, v1, Lcom/google/android/apps/gmm/navigation/a/b/d;->b:Lcom/google/maps/g/a/al;

    if-eq v5, v6, :cond_1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/b/d;->b:Lcom/google/maps/g/a/al;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/navigation/a/k;->a(Lcom/google/maps/g/a/al;)Z

    move-result v1

    if-nez v1, :cond_1

    move-object v0, v4

    .line 195
    :goto_1
    return-object v0

    .line 153
    :cond_1
    instance-of v1, v0, Lcom/google/android/apps/gmm/navigation/a/b/b;

    if-eqz v1, :cond_b

    .line 154
    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/b/b;

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/a/b/b;->a:[Lcom/google/android/apps/gmm/navigation/a/b/a;

    .line 155
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    move v1, v2

    .line 158
    :goto_2
    array-length v5, v6

    if-ge v0, v5, :cond_7

    .line 159
    aget-object v5, v6, v0

    invoke-direct {p0, v5}, Lcom/google/android/apps/gmm/navigation/a/k;->a(Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v5

    .line 160
    aget-object v8, v6, v0

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/navigation/a/b/a;->a()Z

    move-result v8

    if-eqz v8, :cond_4

    if-eqz v5, :cond_2

    instance-of v8, v5, Lcom/google/android/apps/gmm/navigation/a/bb;

    if-eqz v8, :cond_4

    .line 161
    :cond_2
    instance-of v5, v5, Lcom/google/android/apps/gmm/navigation/a/bb;

    if-eqz v5, :cond_3

    move v1, v3

    .line 158
    :cond_3
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 166
    :cond_4
    if-nez v5, :cond_5

    .line 167
    const-string v1, "CannedSpeechAlertGenerator"

    aget-object v0, v6, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x21

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "No voice instruction defined for:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v4

    .line 168
    goto :goto_1

    .line 169
    :cond_5
    instance-of v8, v5, Lcom/google/android/apps/gmm/navigation/a/bb;

    if-eqz v8, :cond_6

    move-object v0, v5

    .line 171
    goto :goto_1

    .line 173
    :cond_6
    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 176
    :cond_7
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_9

    .line 177
    if-eqz v1, :cond_8

    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bb;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/a/bb;-><init>()V

    goto :goto_1

    :cond_8
    move-object v0, v4

    goto :goto_1

    .line 178
    :cond_9
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v3, :cond_a

    .line 179
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/a;

    goto/16 :goto_1

    .line 181
    :cond_a
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/ab;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/a/ab;-><init>([Lcom/google/android/apps/gmm/navigation/a/a;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 184
    :cond_b
    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/a/k;->e:Lcom/google/android/apps/gmm/navigation/a/n;

    instance-of v1, v0, Lcom/google/android/apps/gmm/navigation/a/b/f;

    if-eqz v1, :cond_e

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/navigation/a/b/f;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/a/b/f;->a:I

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/a/n;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/a/n;->a:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_c

    invoke-static {}, Lcom/google/android/apps/gmm/navigation/a/q;->a()Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    .line 185
    :goto_4
    iget-boolean v5, v1, Lcom/google/android/apps/gmm/navigation/a/q;->a:Z

    if-eqz v5, :cond_13

    .line 189
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/bb;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/a/bb;-><init>()V

    goto/16 :goto_1

    .line 184
    :cond_c
    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/navigation/a/n;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/a/q;->a(Ljava/io/File;)Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    goto :goto_4

    :cond_d
    invoke-static {}, Lcom/google/android/apps/gmm/navigation/a/q;->b()Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    goto :goto_4

    :cond_e
    instance-of v1, v0, Lcom/google/android/apps/gmm/navigation/a/b/d;

    if-eqz v1, :cond_f

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/navigation/a/b/d;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/a/b/d;->a:I

    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/navigation/a/n;->a(I)Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    goto :goto_4

    :cond_f
    instance-of v1, v0, Lcom/google/android/apps/gmm/navigation/a/b/g;

    if-eqz v1, :cond_12

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/navigation/a/b/g;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/a/b/g;->a:I

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/a/n;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_11

    iget-object v6, v5, Lcom/google/android/apps/gmm/navigation/a/n;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_10

    invoke-static {}, Lcom/google/android/apps/gmm/navigation/a/q;->a()Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    goto :goto_4

    :cond_10
    invoke-virtual {v5, v1}, Lcom/google/android/apps/gmm/navigation/a/n;->a(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/navigation/a/q;->a(Ljava/io/File;)Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    goto :goto_4

    :cond_11
    invoke-static {}, Lcom/google/android/apps/gmm/navigation/a/q;->b()Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    goto :goto_4

    :cond_12
    invoke-static {}, Lcom/google/android/apps/gmm/navigation/a/q;->b()Lcom/google/android/apps/gmm/navigation/a/q;

    move-result-object v1

    goto :goto_4

    .line 190
    :cond_13
    iget-boolean v5, v1, Lcom/google/android/apps/gmm/navigation/a/q;->a:Z

    if-nez v5, :cond_14

    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/a/q;->b:Ljava/io/File;

    if-nez v5, :cond_14

    :goto_5
    if-nez v3, :cond_15

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/k;->d:Lcom/google/android/apps/gmm/shared/b/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/a/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/navigation/a/c;

    move-result-object v0

    .line 192
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/k;->a:Landroid/content/Context;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/q;->b:Ljava/io/File;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/k;->b:Lcom/google/android/apps/gmm/shared/c/a/j;

    invoke-static {v2, v1, v0, v3}, Lcom/google/android/apps/gmm/navigation/a/al;->a(Landroid/content/Context;Ljava/io/File;Lcom/google/android/apps/gmm/navigation/a/c;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/navigation/a/a;

    move-result-object v0

    goto/16 :goto_1

    :cond_14
    move v3, v2

    .line 190
    goto :goto_5

    .line 194
    :cond_15
    const-string v1, "CannedSpeechAlertGenerator"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x21

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Cannot find sound for a message: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object v0, v4

    .line 195
    goto/16 :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 127
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/k;->e:Lcom/google/android/apps/gmm/navigation/a/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 136
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 131
    :cond_1
    :try_start_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->b:Lcom/google/android/apps/gmm/navigation/a/b/a;

    .line 132
    if-eqz v1, :cond_0

    .line 136
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/navigation/a/k;->a(Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 204
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/aw;Lcom/google/android/apps/gmm/aa/d/s;)V
    .locals 0

    .prologue
    .line 109
    if-eqz p2, :cond_0

    .line 110
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/navigation/a/k;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;

    .line 111
    invoke-interface {p2}, Lcom/google/android/apps/gmm/navigation/a/aw;->e()V

    .line 113
    :cond_0
    return-void
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/navigation/a/n;)V
    .locals 1

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/k;->e:Lcom/google/android/apps/gmm/navigation/a/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    monitor-exit p0

    return-void

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Lcom/google/maps/g/a/al;)Z
    .locals 3

    .prologue
    .line 94
    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/k;->e:Lcom/google/android/apps/gmm/navigation/a/n;

    .line 100
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/k;->e:Lcom/google/android/apps/gmm/navigation/a/n;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/k;->c:Lcom/google/android/apps/gmm/navigation/a/s;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/k;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/gmm/aa/b/a;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/k;->g:Lcom/google/android/apps/gmm/navigation/a/t;

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/apps/gmm/navigation/a/s;->a(Ljava/util/Locale;Lcom/google/maps/g/a/al;Lcom/google/android/apps/gmm/navigation/a/t;)Lcom/google/android/apps/gmm/navigation/a/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/k;->e:Lcom/google/android/apps/gmm/navigation/a/n;

    .line 98
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/k;->f:Lcom/google/maps/g/a/al;

    goto :goto_0

    .line 100
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 201
    return-void
.end method
