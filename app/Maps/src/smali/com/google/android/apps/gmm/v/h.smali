.class public final enum Lcom/google/android/apps/gmm/v/h;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/v/h;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/v/h;

.field public static final enum b:Lcom/google/android/apps/gmm/v/h;

.field public static final enum c:Lcom/google/android/apps/gmm/v/h;

.field public static final enum d:Lcom/google/android/apps/gmm/v/h;

.field public static final enum e:Lcom/google/android/apps/gmm/v/h;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/v/h;


# instance fields
.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 15
    new-instance v0, Lcom/google/android/apps/gmm/v/h;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/gmm/v/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/h;->a:Lcom/google/android/apps/gmm/v/h;

    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/v/h;

    const-string v1, "BEFORE_CAMERA"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/apps/gmm/v/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/h;->b:Lcom/google/android/apps/gmm/v/h;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/v/h;

    const-string v1, "CAMERA"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/apps/gmm/v/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/h;->c:Lcom/google/android/apps/gmm/v/h;

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/v/h;

    const-string v1, "AFTER_CAMERA"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/android/apps/gmm/v/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/h;->d:Lcom/google/android/apps/gmm/v/h;

    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/v/h;

    const-string v1, "LAST"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/android/apps/gmm/v/h;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/v/h;->e:Lcom/google/android/apps/gmm/v/h;

    .line 14
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/h;

    sget-object v1, Lcom/google/android/apps/gmm/v/h;->a:Lcom/google/android/apps/gmm/v/h;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/v/h;->b:Lcom/google/android/apps/gmm/v/h;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/v/h;->c:Lcom/google/android/apps/gmm/v/h;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/v/h;->d:Lcom/google/android/apps/gmm/v/h;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/v/h;->e:Lcom/google/android/apps/gmm/v/h;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/apps/gmm/v/h;->g:[Lcom/google/android/apps/gmm/v/h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput p3, p0, Lcom/google/android/apps/gmm/v/h;->f:I

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/gmm/v/h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 14
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->g:[Lcom/google/android/apps/gmm/v/h;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/v/h;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method
