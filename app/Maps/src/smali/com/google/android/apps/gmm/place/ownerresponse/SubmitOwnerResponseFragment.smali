.class public Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"


# instance fields
.field c:Lcom/google/android/apps/gmm/place/ownerresponse/b;

.field d:Lcom/google/android/apps/gmm/place/ownerresponse/j;

.field private e:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/place/ownerresponse/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 38
    return-void
.end method

.method public static a(Lcom/google/maps/g/pc;Lcom/google/r/b/a/acq;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;-><init>()V

    .line 59
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 60
    const-string v2, "ReviewData"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 61
    const-string v2, "Establishment"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 62
    const-string v2, "OwnerResponseText"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 72
    const-string v1, "ReviewData"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Lcom/google/maps/g/pc;

    .line 73
    const-string v1, "Establishment"

    .line 74
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Lcom/google/r/b/a/acq;

    .line 75
    const-string v1, "OwnerResponseText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/place/ownerresponse/b;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->d:Lcom/google/android/apps/gmm/place/ownerresponse/j;

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/ownerresponse/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;Lcom/google/android/apps/gmm/place/ownerresponse/j;Lcom/google/maps/g/pc;Lcom/google/r/b/a/acq;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->c:Lcom/google/android/apps/gmm/place/ownerresponse/b;

    .line 78
    return-void

    .line 77
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 98
    new-instance v1, Lcom/google/android/apps/gmm/place/ownerresponse/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/ownerresponse/h;-><init>(Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 111
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/c/n;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->e:Lcom/google/android/libraries/curvular/ae;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->e:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->c:Lcom/google/android/apps/gmm/place/ownerresponse/b;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->e:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 88
    return-object v0
.end method
