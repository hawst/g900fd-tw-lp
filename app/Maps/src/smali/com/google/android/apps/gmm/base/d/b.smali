.class public Lcom/google/android/apps/gmm/base/d/b;
.super Landroid/graphics/drawable/GradientDrawable;
.source "PG"

# interfaces
.implements Landroid/view/View$OnAttachStateChangeListener;
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;


# static fields
.field private static final g:Lcom/google/b/c/dc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dc",
            "<",
            "Lcom/google/r/b/a/se;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

.field private b:F

.field private c:Z

.field private final d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    .line 47
    sget-object v0, Lcom/google/r/b/a/se;->b:Lcom/google/r/b/a/se;

    sget v1, Lcom/google/android/apps/gmm/f;->bo:I

    .line 48
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/r/b/a/se;->c:Lcom/google/r/b/a/se;

    sget v3, Lcom/google/android/apps/gmm/f;->bp:I

    .line 49
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/r/b/a/se;->d:Lcom/google/r/b/a/se;

    sget v5, Lcom/google/android/apps/gmm/f;->bq:I

    .line 50
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/r/b/a/se;->e:Lcom/google/r/b/a/se;

    sget v7, Lcom/google/android/apps/gmm/f;->br:I

    .line 51
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 47
    invoke-static/range {v0 .. v7}, Lcom/google/b/c/dc;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dc;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/d/b;->g:Lcom/google/b/c/dc;

    return-void
.end method

.method private constructor <init>(Landroid/view/View;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;ILcom/google/r/b/a/se;)V
    .locals 6

    .prologue
    const/high16 v4, -0x1000000

    const v5, 0xffffff

    .line 64
    sget-object v0, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v1, 0x3

    new-array v1, v1, [I

    const/4 v2, 0x0

    and-int v3, p3, v5

    or-int/2addr v3, v4

    aput v3, v1, v2

    const/4 v2, 0x1

    and-int v3, p3, v5

    or-int/2addr v3, v4

    aput v3, v1, v2

    const/4 v2, 0x2

    const/high16 v3, -0x34000000    # -3.3554432E7f

    and-int v4, p3, v5

    or-int/2addr v3, v4

    aput v3, v1, v2

    invoke-direct {p0, v0, v1}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 39
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/d/b;->b:F

    .line 42
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->d:Landroid/graphics/Paint;

    .line 70
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/d/b;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->d:Landroid/graphics/Paint;

    and-int v1, p3, v5

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 74
    if-eqz p1, :cond_1

    .line 75
    invoke-virtual {p1, p0}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 77
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/d/b;->g:Lcom/google/b/c/dc;

    invoke-virtual {v0, p4}, Lcom/google/b/c/dc;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 78
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-object v0, Lcom/google/android/apps/gmm/base/d/b;->g:Lcom/google/b/c/dc;

    invoke-virtual {v0, p4}, Lcom/google/b/c/dc;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->e:Landroid/graphics/drawable/Drawable;

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->br:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/d/b;->f:I

    .line 83
    :cond_1
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/r/b/a/se;)V
    .locals 1

    .prologue
    .line 59
    const v0, 0xffffff

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/apps/gmm/base/d/b;-><init>(Landroid/view/View;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;ILcom/google/r/b/a/se;)V

    .line 60
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 108
    sget-object v0, Lcom/google/android/apps/gmm/base/d/c;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 118
    const/4 p2, 0x0

    .line 119
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/d/b;->c:Z

    .line 123
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/base/d/b;->b:F

    cmpl-float v0, v0, p2

    if-eqz v0, :cond_0

    iput p2, p0, Lcom/google/android/apps/gmm/base/d/b;->b:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/b;->invalidateSelf()V

    .line 124
    :cond_0
    return-void

    .line 110
    :pswitch_0
    const/high16 p2, 0x3f800000    # 1.0f

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/d/b;->c:Z

    goto :goto_0

    .line 115
    :pswitch_1
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/d/b;->c:Z

    goto :goto_0

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p2, p3}, Lcom/google/android/apps/gmm/base/d/b;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 95
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    invoke-direct {p0, p3, v0}, Lcom/google/android/apps/gmm/base/d/b;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 90
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/apps/gmm/base/d/b;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 104
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 136
    iget v0, p0, Lcom/google/android/apps/gmm/base/d/b;->b:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    .line 137
    invoke-super {p0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 147
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/d/b;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/d/b;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/b;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/b;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/google/android/apps/gmm/base/d/b;->f:I

    add-int/2addr v3, v4

    .line 152
    iget-object v4, p0, Lcom/google/android/apps/gmm/base/d/b;->e:Landroid/graphics/drawable/Drawable;

    add-int/2addr v0, v2

    add-int/2addr v1, v3

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 153
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 155
    :cond_1
    return-void

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/gmm/base/d/b;->b:F

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/d/b;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/d/b;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 142
    iget v0, p0, Lcom/google/android/apps/gmm/base/d/b;->b:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 143
    invoke-super {p0, p1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 162
    :cond_0
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/d/b;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->n:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 169
    :cond_0
    return-void
.end method
