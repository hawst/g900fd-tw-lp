.class public Lcom/google/android/apps/gmm/directions/i/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/h;


# instance fields
.field private final a:Ljava/lang/CharSequence;

.field private final b:Ljava/lang/CharSequence;

.field private final c:Lcom/google/android/libraries/curvular/aw;

.field private final d:Lcom/google/android/libraries/curvular/aw;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/lang/Boolean;

.field private final g:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;Ljava/util/List;ZLjava/lang/Runnable;)V
    .locals 1
    .param p3    # Ljava/lang/CharSequence;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Runnable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/a/ag;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Lcom/google/android/libraries/curvular/aw;",
            "Lcom/google/android/libraries/curvular/aw;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;Z",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/i/n;->a:Ljava/lang/CharSequence;

    .line 48
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/i/n;->b:Ljava/lang/CharSequence;

    .line 49
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/i/n;->c:Lcom/google/android/libraries/curvular/aw;

    .line 50
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/i/n;->d:Lcom/google/android/libraries/curvular/aw;

    .line 51
    iput-object p6, p0, Lcom/google/android/apps/gmm/directions/i/n;->e:Ljava/util/List;

    .line 52
    invoke-static {p7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->f:Ljava/lang/Boolean;

    .line 53
    iput-object p8, p0, Lcom/google/android/apps/gmm/directions/i/n;->g:Ljava/lang/Runnable;

    .line 54
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/maps/g/a/al;Lcom/google/android/apps/gmm/map/i/a/a;ZLjava/lang/Runnable;)Lcom/google/android/apps/gmm/directions/i/n;
    .locals 9
    .param p3    # Lcom/google/android/apps/gmm/map/i/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Runnable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 71
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->A:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 73
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->A:Ljava/lang/String;

    move-object v7, v5

    .line 79
    :goto_0
    const/4 v0, 0x0

    new-instance v4, Lcom/google/android/apps/gmm/directions/i/o;

    invoke-direct {v4, p0, p3, v0}, Lcom/google/android/apps/gmm/directions/i/o;-><init>(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/map/i/a/a;Z)V

    .line 80
    new-instance v5, Lcom/google/android/apps/gmm/directions/i/o;

    invoke-direct {v5, p0, p3, v3}, Lcom/google/android/apps/gmm/directions/i/o;-><init>(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/map/i/a/a;Z)V

    .line 82
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 84
    new-instance v2, Lcom/google/android/apps/gmm/directions/i/af;

    invoke-direct {v2, v0, p4}, Lcom/google/android/apps/gmm/directions/i/af;-><init>(Lcom/google/maps/g/a/da;Z)V

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 74
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    if-lez v0, :cond_2

    .line 75
    iget v1, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    move-object v0, p1

    move-object v2, p2

    move v4, v3

    move-object v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v7, v5

    goto :goto_0

    .line 87
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/n;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    move-object v1, p0

    move-object v3, v7

    move v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/directions/i/n;-><init>(Lcom/google/android/apps/gmm/map/r/a/ag;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;Ljava/util/List;ZLjava/lang/Runnable;)V

    return-object v0

    :cond_2
    move-object v7, v5

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 182
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->f:Ljava/lang/Boolean;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/h/k;

    .line 184
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/directions/h/k;->a(Z)V

    goto :goto_0

    .line 186
    :cond_0
    return-void
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->g:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 198
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->d:Lcom/google/android/libraries/curvular/aw;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->c:Lcom/google/android/libraries/curvular/aw;

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/k;",
            ">;"
        }
    .end annotation

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/n;->e:Ljava/util/List;

    return-object v0
.end method
