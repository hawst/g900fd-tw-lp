.class public final enum Lcom/google/android/apps/gmm/place/cm;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/place/cm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/place/cm;

.field public static final enum b:Lcom/google/android/apps/gmm/place/cm;

.field public static final enum c:Lcom/google/android/apps/gmm/place/cm;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/place/cm;


# instance fields
.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/place/cm;

    const-string v1, "LINES"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/place/cm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/place/cm;->a:Lcom/google/android/apps/gmm/place/cm;

    .line 36
    new-instance v0, Lcom/google/android/apps/gmm/place/cm;

    const-string v1, "NEXT_DEPARTURES"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/cm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/place/cm;->b:Lcom/google/android/apps/gmm/place/cm;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/place/cm;

    const-string v1, "FULL_SCHEDULE"

    invoke-direct {v0, v1, v3, v5}, Lcom/google/android/apps/gmm/place/cm;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/place/cm;->c:Lcom/google/android/apps/gmm/place/cm;

    .line 26
    new-array v0, v5, [Lcom/google/android/apps/gmm/place/cm;

    sget-object v1, Lcom/google/android/apps/gmm/place/cm;->a:Lcom/google/android/apps/gmm/place/cm;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/place/cm;->b:Lcom/google/android/apps/gmm/place/cm;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/place/cm;->c:Lcom/google/android/apps/gmm/place/cm;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/place/cm;->e:[Lcom/google/android/apps/gmm/place/cm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    iput p3, p0, Lcom/google/android/apps/gmm/place/cm;->d:I

    .line 49
    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/place/cm;
    .locals 5

    .prologue
    .line 62
    invoke-static {}, Lcom/google/android/apps/gmm/place/cm;->values()[Lcom/google/android/apps/gmm/place/cm;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 63
    iget v4, v0, Lcom/google/android/apps/gmm/place/cm;->d:I

    if-ne v4, p0, :cond_0

    .line 67
    :goto_1
    return-object v0

    .line 62
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/place/cm;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/place/cm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/cm;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/place/cm;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/apps/gmm/place/cm;->e:[Lcom/google/android/apps/gmm/place/cm;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/place/cm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/place/cm;

    return-object v0
.end method
