.class public Lcom/google/android/apps/gmm/suggest/e/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Z

.field final b:I

.field final c:I

.field private final d:I

.field private final e:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput p1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    .line 210
    iput p2, p0, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/e/f;->a:Z

    .line 212
    iput v1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    .line 213
    iput v1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    .line 214
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 1

    .prologue
    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 217
    iput p1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    .line 218
    iput p2, p0, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/suggest/e/f;->a:Z

    .line 220
    iput p3, p0, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    .line 221
    iput p4, p0, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    .line 222
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;I)Lcom/google/b/c/cv;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/e/a/a/a/b;",
            "I)",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/f;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v12, 0x9

    const/4 v11, 0x2

    const/16 v10, 0x15

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 271
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v5

    move v2, v3

    .line 272
    :goto_0
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, p1}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-ge v2, v0, :cond_12

    .line 273
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v2, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 274
    :goto_1
    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {v5, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 272
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 273
    :cond_1
    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_5

    move v1, v4

    :goto_2
    if-nez v1, :cond_2

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_6

    :cond_2
    move v1, v4

    :goto_3
    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v11}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_7

    move v1, v4

    :goto_4
    if-nez v1, :cond_3

    invoke-virtual {v0, v11}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_8

    :cond_3
    move v1, v4

    :goto_5
    if-nez v1, :cond_9

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    move v1, v3

    goto :goto_2

    :cond_6
    move v1, v3

    goto :goto_3

    :cond_7
    move v1, v3

    goto :goto_4

    :cond_8
    move v1, v3

    goto :goto_5

    :cond_9
    invoke-virtual {v0, v4, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v0, v11, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v7, v8

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v12}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_d

    move v1, v4

    :goto_6
    if-nez v1, :cond_a

    invoke-virtual {v0, v12}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_e

    :cond_a
    move v1, v4

    :goto_7
    if-eqz v1, :cond_c

    const/16 v8, 0xa

    iget-object v1, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_f

    move v1, v4

    :goto_8
    if-nez v1, :cond_b

    invoke-virtual {v0, v8}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_10

    :cond_b
    move v1, v4

    :goto_9
    if-nez v1, :cond_11

    :cond_c
    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/f;

    invoke-direct {v0, v6, v7}, Lcom/google/android/apps/gmm/suggest/e/f;-><init>(II)V

    goto/16 :goto_1

    :cond_d
    move v1, v3

    goto :goto_6

    :cond_e
    move v1, v3

    goto :goto_7

    :cond_f
    move v1, v3

    goto :goto_8

    :cond_10
    move v1, v3

    goto :goto_9

    :cond_11
    invoke-virtual {v0, v12, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v1, v8

    const/16 v8, 0xa

    invoke-virtual {v0, v8, v10}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v8, v8

    new-instance v0, Lcom/google/android/apps/gmm/suggest/e/f;

    invoke-direct {v0, v6, v7, v1, v8}, Lcom/google/android/apps/gmm/suggest/e/f;-><init>(IIII)V

    goto/16 :goto_1

    .line 278
    :cond_12
    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v2

    .line 279
    invoke-virtual {v2}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v5

    move v1, v3

    :goto_a
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/f;

    iget v6, v0, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    if-gt v1, v6, :cond_13

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    iget v6, v0, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    if-lt v1, v6, :cond_14

    :cond_13
    :goto_b
    if-eqz v3, :cond_17

    move-object v0, v2

    .line 282
    :goto_c
    return-object v0

    .line 279
    :cond_14
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/suggest/e/f;->a:Z

    if-eqz v1, :cond_15

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    iget v6, v0, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    if-gt v1, v6, :cond_13

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    iget v6, v0, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    if-lt v1, v6, :cond_13

    iget v1, v0, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    iget v6, v0, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    if-ge v1, v6, :cond_13

    :cond_15
    iget v0, v0, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    move v1, v0

    goto :goto_a

    :cond_16
    move v3, v4

    goto :goto_b

    .line 282
    :cond_17
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_c
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 226
    instance-of v1, p1, Lcom/google/android/apps/gmm/suggest/e/f;

    if-nez v1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return v0

    .line 229
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/suggest/e/f;

    .line 230
    iget v1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    iget v2, p1, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    iget v2, p1, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->a:Z

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/suggest/e/f;->a:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    iget v2, p1, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 236
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/e/f;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/e/f;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/suggest/e/f;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
