.class public abstract Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.super Landroid/app/DialogFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/a/a;
.implements Lcom/google/android/apps/gmm/base/fragments/a/c;
.implements Lcom/google/android/apps/gmm/z/b/o;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public a:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:Lcom/google/android/apps/gmm/base/fragments/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 67
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 438
    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 440
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    .line 439
    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/app/DialogFragment;

    .line 441
    if-eqz v0, :cond_0

    .line 442
    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 444
    :cond_0
    return-void
.end method


# virtual methods
.method public F_()Z
    .locals 1

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final K_()V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a:Landroid/view/View;

    .line 362
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->isResumed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 366
    :cond_0
    :goto_1
    return-void

    .line 361
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getView()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 365
    :cond_2
    invoke-static {v0}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method public a(Z)Lcom/google/android/apps/gmm/base/fragments/g;
    .locals 1

    .prologue
    .line 156
    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->b:Lcom/google/android/apps/gmm/base/fragments/g;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->a:Lcom/google/android/apps/gmm/base/fragments/g;

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 397
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->F_()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Z)Lcom/google/android/apps/gmm/base/fragments/g;

    move-result-object v1

    .line 399
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->a:Lcom/google/android/apps/gmm/base/fragments/g;

    if-ne v1, v0, :cond_1

    .line 400
    new-instance v0, Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;-><init>(Landroid/content/Context;)V

    .line 401
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setFragment(Landroid/app/Fragment;)V

    .line 402
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 403
    sget v1, Lcom/google/android/apps/gmm/g;->ey:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/base/fragments/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/fragments/f;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 413
    invoke-virtual {p1, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 427
    :cond_0
    :goto_0
    return-void

    .line 415
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 416
    sget v0, Lcom/google/android/apps/gmm/h;->K:I

    .line 417
    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 418
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 419
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 420
    invoke-virtual {p1, v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 422
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->c:Lcom/google/android/apps/gmm/base/fragments/g;

    if-ne v1, v0, :cond_0

    .line 423
    sget v0, Lcom/google/android/apps/gmm/h;->J:I

    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 424
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V
    .locals 3
    .param p2    # Lcom/google/android/apps/gmm/base/fragments/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 161
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 162
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 163
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 164
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    .line 165
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 166
    return-void
.end method

.method public final a_(I)V
    .locals 1

    .prologue
    .line 350
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->d:Ljava/lang/Integer;

    .line 351
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->F_()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x10300f0

    :goto_0
    return v0

    :cond_0
    const v0, 0x1030076

    goto :goto_0
.end method

.method c()V
    .locals 3

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 221
    :goto_0
    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 223
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    .line 222
    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 225
    :cond_0
    return-void

    .line 220
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public dismiss()V
    .locals 3

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 212
    :cond_0
    return-void

    .line 211
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public dismissAllowingStateLoss()V
    .locals 3

    .prologue
    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 217
    :cond_0
    return-void

    .line 216
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Landroid/app/Fragment;
    .locals 0

    .prologue
    .line 339
    return-object p0
.end method

.method public final e_()Lcom/google/android/apps/gmm/base/fragments/a/a;
    .locals 1

    .prologue
    .line 325
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    return-object v0
.end method

.method public f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 355
    sget-object v0, Lcom/google/b/f/t;->cF:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final g()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->d:Ljava/lang/Integer;

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 251
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->c()V

    .line 253
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 254
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->a()V

    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->setStyle(II)V

    if-eqz p1, :cond_1

    const-string v0, "ue3ActivationId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "ue3ActivationId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->d:Ljava/lang/Integer;

    :cond_0
    const-string v0, "fragmentResultListener"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "fragmentResultListener"

    invoke-virtual {v0, p1, v1}, Landroid/app/FragmentManager;->getFragment(Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    .line 91
    :cond_1
    return-void

    .line 90
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 130
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 133
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 135
    const v2, 0x1030002

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 137
    return-object v0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 431
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    .line 432
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 245
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->F_()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    sget-object v1, Lcom/google/android/apps/gmm/base/layout/w;->c:Lcom/google/android/apps/gmm/base/layout/w;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setMapRenderingMode(Lcom/google/android/apps/gmm/base/layout/w;)V

    .line 246
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 247
    return-void

    .line 245
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 229
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getView()Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/base/fragments/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/fragments/e;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;Lcom/google/b/a/ar;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->F_()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    sget-object v2, Lcom/google/android/apps/gmm/base/layout/w;->a:Lcom/google/android/apps/gmm/base/layout/w;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->setMapRenderingMode(Lcom/google/android/apps/gmm/base/layout/w;)V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 231
    return-void

    .line 230
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 271
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 273
    const-string v0, "ue3ActivationId"

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v0, :cond_1

    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "fragmentResultListener"

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {v1, p1, v2, v0}, Landroid/app/FragmentManager;->putFragment(Landroid/os/Bundle;Ljava/lang/String;Landroid/app/Fragment;)V

    .line 280
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/z/a/b;->a()V

    .line 266
    invoke-super {p0}, Landroid/app/DialogFragment;->onStop()V

    .line 267
    return-void

    .line 265
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "You should use #show(GmmActivity), instead."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206
    const/4 v0, 0x0

    return v0
.end method

.method public show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 191
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->c:Ljava/lang/String;

    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "You should use #show(GmmActivity), instead."

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 193
    return-void
.end method
