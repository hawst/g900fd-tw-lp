.class public Lcom/google/android/apps/gmm/v/bt;
.super Lcom/google/android/apps/gmm/v/ai;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/16 v1, 0x1e00

    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->k:Lcom/google/android/apps/gmm/v/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 53
    const/16 v0, 0x207

    iput v0, p0, Lcom/google/android/apps/gmm/v/bt;->a:I

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/v/bt;->b:I

    .line 55
    iput v2, p0, Lcom/google/android/apps/gmm/v/bt;->c:I

    .line 56
    iput v2, p0, Lcom/google/android/apps/gmm/v/bt;->d:I

    .line 58
    iput v1, p0, Lcom/google/android/apps/gmm/v/bt;->e:I

    .line 59
    iput v1, p0, Lcom/google/android/apps/gmm/v/bt;->f:I

    .line 60
    iput v1, p0, Lcom/google/android/apps/gmm/v/bt;->g:I

    .line 64
    return-void
.end method

.method public constructor <init>(IIIIIII)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/16 v1, 0x1e00

    .line 68
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->k:Lcom/google/android/apps/gmm/v/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 53
    const/16 v0, 0x207

    iput v0, p0, Lcom/google/android/apps/gmm/v/bt;->a:I

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/v/bt;->b:I

    .line 55
    iput v2, p0, Lcom/google/android/apps/gmm/v/bt;->c:I

    .line 56
    iput v2, p0, Lcom/google/android/apps/gmm/v/bt;->d:I

    .line 58
    iput v1, p0, Lcom/google/android/apps/gmm/v/bt;->e:I

    .line 59
    iput v1, p0, Lcom/google/android/apps/gmm/v/bt;->f:I

    .line 60
    iput v1, p0, Lcom/google/android/apps/gmm/v/bt;->g:I

    .line 69
    iput p1, p0, Lcom/google/android/apps/gmm/v/bt;->a:I

    iput p2, p0, Lcom/google/android/apps/gmm/v/bt;->b:I

    iput p3, p0, Lcom/google/android/apps/gmm/v/bt;->c:I

    iput p4, p0, Lcom/google/android/apps/gmm/v/bt;->d:I

    .line 70
    iput p5, p0, Lcom/google/android/apps/gmm/v/bt;->e:I

    iput p6, p0, Lcom/google/android/apps/gmm/v/bt;->f:I

    iput p7, p0, Lcom/google/android/apps/gmm/v/bt;->g:I

    .line 71
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 3

    .prologue
    .line 88
    if-nez p2, :cond_0

    .line 89
    const/16 v0, 0xb90

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 94
    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/v/bt;

    .line 95
    if-eqz p2, :cond_7

    iget v0, p2, Lcom/google/android/apps/gmm/v/bt;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->a:I

    if-ne v0, v1, :cond_1

    iget v0, p2, Lcom/google/android/apps/gmm/v/bt;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->b:I

    if-ne v0, v1, :cond_1

    iget v0, p2, Lcom/google/android/apps/gmm/v/bt;->c:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->c:I

    if-eq v0, v1, :cond_7

    .line 99
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/v/bt;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/bt;->c:I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glStencilFunc(III)V

    .line 104
    :cond_2
    :goto_0
    if-eqz p2, :cond_8

    iget v0, p2, Lcom/google/android/apps/gmm/v/bt;->e:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->e:I

    if-ne v0, v1, :cond_3

    iget v0, p2, Lcom/google/android/apps/gmm/v/bt;->f:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->f:I

    if-ne v0, v1, :cond_3

    iget v0, p2, Lcom/google/android/apps/gmm/v/bt;->g:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->g:I

    if-eq v0, v1, :cond_8

    .line 108
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/v/bt;->e:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->f:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/bt;->g:I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glStencilOp(III)V

    .line 113
    :cond_4
    :goto_1
    if-eqz p2, :cond_5

    iget v0, p2, Lcom/google/android/apps/gmm/v/bt;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->d:I

    if-eq v0, v1, :cond_6

    .line 114
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/v/bt;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glStencilMask(I)V

    .line 116
    :cond_6
    return-void

    .line 100
    :cond_7
    if-nez p2, :cond_2

    .line 101
    iget v0, p0, Lcom/google/android/apps/gmm/v/bt;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/bt;->c:I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glStencilFunc(III)V

    goto :goto_0

    .line 109
    :cond_8
    if-nez p2, :cond_4

    .line 110
    iget v0, p0, Lcom/google/android/apps/gmm/v/bt;->e:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/bt;->f:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/bt;->g:I

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glStencilOp(III)V

    goto :goto_1
.end method

.method final b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 1

    .prologue
    .line 120
    check-cast p2, Lcom/google/android/apps/gmm/v/bt;

    .line 121
    if-nez p2, :cond_0

    .line 122
    const/16 v0, 0xb90

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 124
    :cond_0
    return-void
.end method
