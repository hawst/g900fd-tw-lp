.class Lcom/google/android/apps/gmm/map/internal/d/b/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/k;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x1

    return v0
.end method

.method public final a([B)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 10

    .prologue
    const-wide v8, 0xffffffffL

    const/16 v6, 0x20

    .line 204
    const/4 v0, 0x0

    .line 205
    array-length v1, p1

    const/16 v2, 0x18

    if-lt v1, v2, :cond_0

    .line 206
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;-><init>()V

    .line 207
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 208
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->d:I

    .line 209
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0xc

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->c:J

    .line 210
    const/16 v1, 0x10

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x14

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->b:J

    .line 212
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    .line 214
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/b/n;[B)V
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 194
    const/4 v0, 0x0

    const/16 v1, 0x18

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 196
    const/4 v0, 0x4

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->d:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 197
    const/16 v0, 0x8

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    shr-long v4, v2, v6

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0xc

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 198
    const/16 v0, 0x10

    .line 199
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 198
    shr-long v4, v2, v6

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x14

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 200
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 224
    const/16 v0, 0x18

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/internal/d/b/d;
    .locals 1

    .prologue
    .line 229
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->g:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-object v0
.end method
