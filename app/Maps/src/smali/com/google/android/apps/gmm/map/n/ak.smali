.class public final Lcom/google/android/apps/gmm/map/n/ak;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/SharedPreferences;)Lcom/google/android/apps/gmm/map/f/a/i;
    .locals 12
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const-wide/16 v10, -0x1

    const/4 v6, 0x0

    const/high16 v7, -0x3b860000    # -1000.0f

    .line 94
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    const-string v1, "lat"

    .line 95
    const/high16 v2, -0x3b860000    # -1000.0f

    invoke-interface {p0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    cmpl-float v2, v1, v7

    if-nez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 104
    :catch_0
    move-exception v0

    move-object v0, v6

    .line 107
    :goto_0
    return-object v0

    .line 95
    :cond_0
    float-to-double v2, v1

    const-string v1, "lng"

    .line 96
    const/high16 v4, -0x3b860000    # -1000.0f

    invoke-interface {p0, v1, v4}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v1

    cmpl-float v4, v1, v7

    if-nez v4, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 107
    :catch_1
    move-exception v0

    move-object v0, v6

    goto :goto_0

    .line 96
    :cond_1
    float-to-double v8, v1

    invoke-direct {v0, v2, v3, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 95
    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    const-string v0, "zoom"

    .line 97
    const/high16 v1, -0x3b860000    # -1000.0f

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    cmpl-float v1, v0, v7

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    const-string v0, "tilt"

    .line 98
    const/high16 v1, -0x3b860000    # -1000.0f

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    cmpl-float v1, v0, v7

    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3
    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    const-string v0, "bearing"

    .line 99
    const/high16 v1, -0x3b860000    # -1000.0f

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    cmpl-float v1, v0, v7

    if-nez v1, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    .line 100
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 101
    const-string v1, "cameraTimestampMs"

    const-wide/16 v2, -0x1

    invoke-interface {p0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v1, v2, v10

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 102
    :cond_5
    new-instance v1, Lcom/google/android/apps/gmm/map/f/a/i;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/a/i;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;J)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v1

    goto/16 :goto_0
.end method
