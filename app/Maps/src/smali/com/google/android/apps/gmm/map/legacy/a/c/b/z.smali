.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/b/a/ab;

.field final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;",
            ">;"
        }
    .end annotation
.end field

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final e:Lcom/google/android/apps/gmm/map/t/as;

.field f:Lcom/google/android/apps/gmm/map/b/a/j;

.field private final g:[Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    .line 271
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    .line 272
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->d:Ljava/util/HashSet;

    .line 292
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 293
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->g:[Ljava/lang/String;

    .line 294
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->e:Lcom/google/android/apps/gmm/map/t/as;

    .line 295
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/ad;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/t/as;)V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 270
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    .line 271
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    .line 272
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->d:Ljava/util/HashSet;

    .line 285
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->d:Lcom/google/android/apps/gmm/map/b/a/ab;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 286
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->g:[Ljava/lang/String;

    .line 287
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->e:Lcom/google/android/apps/gmm/map/t/as;

    .line 288
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a(Lcom/google/android/apps/gmm/map/internal/c/ad;)V

    .line 289
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 347
    const/4 v0, 0x0

    .line 348
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 349
    iget v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->b:F

    div-float/2addr v0, v4

    add-float/2addr v0, v3

    .line 350
    cmpl-float v3, v0, v1

    if-lez v3, :cond_3

    :goto_1
    move v1, v0

    .line 353
    goto :goto_0

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 355
    iget v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->b:F

    div-float/2addr v0, v4

    add-float/2addr v0, v3

    .line 356
    cmpl-float v3, v0, v1

    if-lez v3, :cond_1

    move v1, v0

    .line 357
    goto :goto_2

    .line 360
    :cond_2
    mul-float v0, v1, v4

    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/ad;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v7, 0x0

    .line 299
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->j:[I

    array-length v2, v1

    move v0, v7

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 300
    if-ltz v3, :cond_0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->g:[Ljava/lang/String;

    array-length v4, v4

    if-ge v3, v4, :cond_0

    .line 301
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->d:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->g:[Ljava/lang/String;

    aget-object v3, v5, v3

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 299
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 304
    :cond_1
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->f:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 310
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_4

    move v0, v7

    :goto_1
    if-lez v0, :cond_5

    .line 311
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, v7

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    cmpl-float v0, v0, v10

    if-eqz v0, :cond_5

    .line 312
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, v7

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    neg-float v1, v0

    .line 316
    :goto_2
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_6

    move v0, v7

    :goto_3
    if-ne v0, v9, :cond_7

    .line 317
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 318
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->b:J

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->g:Ljava/lang/String;

    move-object v3, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;-><init>(FLcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/be;JLjava/lang/String;I)V

    .line 317
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    :cond_2
    :goto_4
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->i:Z

    if-eqz v0, :cond_3

    .line 329
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 331
    :cond_3
    return-void

    .line 310
    :cond_4
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_1

    .line 312
    :cond_5
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->h:F

    goto :goto_2

    .line 316
    :cond_6
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_3

    .line 319
    :cond_7
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v0, :cond_8

    move v0, v7

    :goto_5
    if-le v0, v9, :cond_2

    .line 320
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 321
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->b:J

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->g:Ljava/lang/String;

    move-object v3, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;-><init>(FLcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/be;JLjava/lang/String;I)V

    .line 320
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 322
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, v9

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    cmpl-float v0, v0, v10

    if-eqz v0, :cond_9

    .line 323
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    aget-object v0, v0, v9

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    neg-float v1, v0

    .line 324
    :goto_6
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 325
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->b:J

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->g:Ljava/lang/String;

    move-object v3, v2

    move v7, v9

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;-><init>(FLcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/be;JLjava/lang/String;I)V

    .line 324
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 319
    :cond_8
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_5

    .line 323
    :cond_9
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/ad;->h:F

    goto :goto_6
.end method

.method public final b()I
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int v7, v0, v2

    .line 373
    if-nez v7, :cond_1

    .line 410
    :cond_0
    return v1

    .line 377
    :cond_1
    new-array v8, v7, [I

    move v6, v4

    .line 380
    :goto_0
    if-ge v6, v7, :cond_5

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 389
    :goto_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    if-nez v2, :cond_3

    move v0, v1

    .line 403
    :goto_2
    aput v0, v8, v6

    .line 380
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int v2, v6, v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    goto :goto_1

    .line 392
    :cond_3
    iget-object v9, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    array-length v10, v9

    move v3, v4

    move v2, v4

    :goto_3
    if-ge v3, v10, :cond_4

    aget v5, v9, v3

    .line 393
    add-int/2addr v5, v2

    .line 392
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v5

    goto :goto_3

    .line 399
    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    array-length v0, v0

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v1, :cond_6

    .line 400
    shl-int/lit8 v0, v2, 0x1

    goto :goto_2

    .line 406
    :cond_5
    aget v0, v8, v4

    move v11, v1

    move v1, v0

    move v0, v11

    .line 407
    :goto_4
    if-ge v0, v7, :cond_0

    .line 408
    aget v2, v8, v0

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/s;->c(II)I

    move-result v2

    .line 407
    add-int/lit8 v1, v0, 0x1

    move v0, v1

    move v1, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_2
.end method
