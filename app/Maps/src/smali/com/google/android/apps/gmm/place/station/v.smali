.class public Lcom/google/android/apps/gmm/place/station/v;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Landroid/content/Context;)I
    .locals 3

    .prologue
    const/16 v0, 0x3c

    .line 65
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 66
    const/16 v2, 0xa0

    if-gt v1, v2, :cond_1

    .line 67
    const/16 v0, 0x13

    .line 75
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    const/16 v2, 0xf0

    if-gt v1, v2, :cond_2

    .line 69
    const/16 v0, 0x1e

    goto :goto_0

    .line 70
    :cond_2
    const/16 v2, 0x140

    if-gt v1, v2, :cond_3

    .line 71
    const/16 v0, 0x26

    goto :goto_0

    .line 72
    :cond_3
    const/16 v2, 0x1e0

    if-gt v1, v2, :cond_0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/vp;)Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 85
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/maps/g/vp;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    .line 86
    iget v1, v0, Lcom/google/maps/g/a/i;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/l;->a(I)Lcom/google/maps/g/a/l;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    :cond_2
    sget-object v3, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    if-ne v1, v3, :cond_1

    iget v1, v0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    .line 87
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/b;

    .line 88
    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    invoke-static {p0}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;)I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/base/views/c/b;-><init>(Ljava/lang/String;Lcom/google/r/b/a/acy;F)V

    move-object v0, v1

    .line 91
    :goto_2
    return-object v0

    .line 86
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 91
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/vx;)Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 208
    sget-object v3, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    invoke-static {p0}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;)I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {p1}, Lcom/google/maps/g/vx;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Lcom/google/maps/g/a/ee;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_0

    move-object v2, v0

    :goto_1
    if-nez v2, :cond_4

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    move-object v2, v1

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/b;

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/base/views/c/b;-><init>(Ljava/lang/String;Lcom/google/r/b/a/acy;F)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/vf;)Ljava/lang/CharSequence;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/high16 v2, -0x1000000

    .line 349
    invoke-virtual {p1}, Lcom/google/maps/g/vf;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 350
    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_1
    sget-object v6, Lcom/google/maps/g/a/eh;->k:Lcom/google/maps/g/a/eh;

    if-ne v1, v6, :cond_0

    .line 351
    invoke-static {}, Lcom/google/maps/g/a/ee;->newBuilder()Lcom/google/maps/g/a/eg;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/maps/g/a/eg;->a(Lcom/google/maps/g/a/ee;)Lcom/google/maps/g/a/eg;

    move-result-object v6

    .line 358
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v7, 0x8

    if-ne v1, v7, :cond_2

    move v1, v4

    :goto_0
    if-eqz v1, :cond_6

    .line 359
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    invoke-virtual {v1}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    .line 361
    :goto_1
    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-static {}, Lcom/google/maps/g/a/o;->newBuilder()Lcom/google/maps/g/a/q;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/maps/g/a/q;->a(Lcom/google/maps/g/a/o;)Lcom/google/maps/g/a/q;

    move-result-object v7

    if-ne v1, v2, :cond_3

    const-string v0, "#8A000000"

    :goto_2
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 358
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 361
    :cond_3
    const-string v0, "#00688C"

    goto :goto_2

    :cond_4
    iget v1, v7, Lcom/google/maps/g/a/q;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v7, Lcom/google/maps/g/a/q;->a:I

    iput-object v0, v7, Lcom/google/maps/g/a/q;->d:Ljava/lang/Object;

    iget-object v0, v6, Lcom/google/maps/g/a/eg;->c:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/maps/g/a/q;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v7, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v4, v0, Lcom/google/n/ao;->d:Z

    iget v0, v6, Lcom/google/maps/g/a/eg;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v6, Lcom/google/maps/g/a/eg;->a:I

    .line 363
    invoke-virtual {v6}, Lcom/google/maps/g/a/eg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 366
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/map/i/b/b;->a(Landroid/content/Context;Lcom/google/maps/g/a/ee;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 367
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 371
    :goto_3
    return-object v0

    :cond_5
    move-object v0, v3

    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/maps/g/vi;)Ljava/lang/CharSequence;
    .locals 5
    .param p2    # Lcom/google/maps/g/vi;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 394
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    .line 396
    if-eqz p2, :cond_0

    .line 397
    sget-object v0, Lcom/google/android/apps/gmm/place/station/w;->a:[I

    invoke-virtual {p2}, Lcom/google/maps/g/vi;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 409
    :cond_0
    :goto_0
    const-string v0, "%s"

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    return-object v0

    .line 399
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/d;->ac:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v3, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    goto :goto_0

    .line 403
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v3, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    goto :goto_0

    .line 407
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/d;->aq:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v3, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v4, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 408
    iget-object v0, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v3, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v3}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    goto :goto_0

    .line 397
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/maps/g/vf;)Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 451
    invoke-virtual {p0}, Lcom/google/maps/g/vf;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 452
    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_1
    sget-object v3, Lcom/google/maps/g/a/eh;->l:Lcom/google/maps/g/a/eh;

    if-ne v1, v3, :cond_0

    .line 453
    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v0

    .line 456
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_0

    .line 460
    :goto_1
    return-object v0

    .line 456
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 460
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Lcom/google/maps/g/vx;)Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/maps/g/vx;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 101
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/b;->b(Lcom/google/maps/g/a/ee;)Ljava/lang/String;

    move-result-object v0

    .line 102
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_0

    .line 106
    :goto_1
    return-object v0

    .line 102
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 106
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 299
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/d;

    .line 300
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/d;->o()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 301
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/d;->p()Ljava/lang/String;

    move-result-object v0

    .line 304
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 128
    invoke-virtual {p0}, Lcom/google/maps/g/vx;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 129
    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_1
    sget-object v5, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v1, v5, :cond_0

    .line 130
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 131
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v5, 0x8

    if-ne v1, v5, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    .line 132
    new-instance v1, Lcom/google/android/libraries/curvular/a;

    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    move-object v0, v1

    .line 135
    :goto_2
    return-object v0

    :cond_2
    move v1, v3

    .line 130
    goto :goto_0

    :cond_3
    move v1, v3

    .line 131
    goto :goto_1

    .line 135
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static b(Ljava/util/List;)Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 315
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/c;

    .line 316
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/c;->i()Ljava/lang/String;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_0

    .line 321
    :goto_1
    return-object v0

    .line 317
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 321
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static c(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 160
    invoke-virtual {p0}, Lcom/google/maps/g/vx;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 161
    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_1
    sget-object v5, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v1, v5, :cond_0

    .line 162
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 163
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v5, 0x4

    if-ne v1, v5, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    .line 164
    new-instance v1, Lcom/google/android/libraries/curvular/a;

    .line 165
    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    move-object v0, v1

    .line 168
    :goto_2
    return-object v0

    :cond_2
    move v1, v3

    .line 162
    goto :goto_0

    :cond_3
    move v1, v3

    .line 163
    goto :goto_1

    .line 168
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static c(Ljava/util/List;)Ljava/lang/String;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 331
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/c;

    .line 333
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/c;->j()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/d;

    .line 334
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/d;->u()Ljava/lang/String;

    move-result-object v0

    .line 335
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    .line 340
    :goto_1
    return-object v0

    .line 335
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 340
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static d(Lcom/google/maps/g/vx;)Lcom/google/android/libraries/curvular/a;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 191
    invoke-virtual {p0}, Lcom/google/maps/g/vx;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 192
    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_1
    sget-object v5, Lcom/google/maps/g/a/eh;->e:Lcom/google/maps/g/a/eh;

    if-ne v1, v5, :cond_0

    .line 193
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_2

    move v1, v2

    :goto_0
    if-nez v1, :cond_0

    .line 194
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v1, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v5, 0x4

    if-ne v1, v5, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_0

    .line 195
    new-instance v1, Lcom/google/android/libraries/curvular/a;

    .line 196
    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    move-object v0, v1

    .line 199
    :goto_2
    return-object v0

    :cond_2
    move v1, v3

    .line 193
    goto :goto_0

    :cond_3
    move v1, v3

    .line 194
    goto :goto_1

    .line 199
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
