.class public Lcom/google/android/apps/gmm/directions/i/ae;
.super Lcom/google/android/apps/gmm/directions/i/bg;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/j;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Lcom/google/android/libraries/curvular/c;

.field private final f:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Lcom/google/android/libraries/curvular/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final k:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final m:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private n:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final o:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final p:Lcom/google/android/apps/gmm/directions/h/v;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final q:Lcom/google/android/apps/gmm/directions/i/bi;

.field private final r:Ljava/lang/Boolean;

.field private final s:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/android/apps/gmm/directions/h/z;)V
    .locals 10
    .param p7    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 69
    .line 70
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    sget-object v7, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    .line 71
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/i/b/a;->a(Landroid/content/Context;)I

    move-result v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    move-object/from16 v9, p7

    .line 69
    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/directions/i/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/r/b/a/acy;ILcom/google/android/apps/gmm/directions/h/z;)V

    .line 72
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/r/b/a/acy;ILcom/google/android/apps/gmm/directions/h/z;)V
    .locals 6
    .param p9    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 90
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/i/bg;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/h/z;)V

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/ae;->c:Landroid/content/Context;

    .line 94
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/i/ae;->q:Lcom/google/android/apps/gmm/directions/i/bi;

    .line 95
    invoke-static {p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->r:Ljava/lang/Boolean;

    .line 96
    invoke-interface {p4}, Lcom/google/android/apps/gmm/directions/h/y;->d()Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->s:Ljava/lang/Boolean;

    .line 98
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    :cond_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->d:Ljava/lang/String;

    .line 99
    invoke-static {p2}, Lcom/google/android/apps/gmm/directions/f/d/h;->c(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/fz;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/maps/g/a/fz;IZ)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->e:Lcom/google/android/libraries/curvular/c;

    .line 100
    invoke-interface {p4}, Lcom/google/android/apps/gmm/directions/h/y;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->f:Ljava/lang/String;

    .line 101
    invoke-interface {p4}, Lcom/google/android/apps/gmm/directions/h/y;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->g:Ljava/lang/String;

    .line 102
    invoke-static {p2}, Lcom/google/android/apps/gmm/directions/f/d/h;->c(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/fz;

    move-result-object v1

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v2, :cond_8

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_1
    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_a

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Landroid/content/Context;Lcom/google/maps/g/a/fz;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->h:Ljava/lang/String;

    .line 103
    const-string v0, " "

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/maps/g/a/dl;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/maps/g/a/dl;->l:Lcom/google/maps/g/a/dl;

    aput-object v3, v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ao;Ljava/lang/String;[Lcom/google/maps/g/a/dl;)Ljava/lang/CharSequence;

    move-result-object v0

    if-nez v0, :cond_b

    const/4 v0, 0x0

    :goto_6
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->i:Ljava/lang/String;

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x0

    .line 105
    :goto_7
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->j:Lcom/google/android/libraries/curvular/c;

    .line 107
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    if-nez v1, :cond_d

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v0

    :goto_8
    iget-object v2, v0, Lcom/google/maps/g/a/fw;->c:Ljava/util/List;

    .line 108
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    const/4 v0, 0x0

    :goto_9
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->k:Ljava/lang/String;

    .line 109
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    const/4 v0, 0x0

    :goto_a
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->l:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 110
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-gt v0, v1, :cond_15

    const/4 v0, 0x0

    :goto_b
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->m:Ljava/lang/String;

    .line 111
    const-string v0, ""

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_16

    :cond_2
    const/4 v1, 0x1

    :goto_c
    if-eqz v1, :cond_3

    const/4 v0, 0x0

    :cond_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->o:Ljava/lang/String;

    .line 112
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit16 v0, v0, 0x1000

    const/16 v1, 0x1000

    if-ne v0, v1, :cond_17

    const/4 v0, 0x1

    :goto_d
    if-eqz v0, :cond_19

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->b:Lcom/google/android/apps/gmm/map/r/a/e;

    if-eqz v0, :cond_19

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    if-nez v1, :cond_18

    invoke-static {}, Lcom/google/maps/g/a/bi;->d()Lcom/google/maps/g/a/bi;

    move-result-object v0

    :goto_e
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->b:Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/e;->a()Lcom/google/android/apps/gmm/map/r/a/h;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/i/bd;->a(Lcom/google/maps/g/a/bi;Lcom/google/android/apps/gmm/map/r/a/h;)Lcom/google/android/apps/gmm/directions/i/bd;

    move-result-object v0

    :goto_f
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->p:Lcom/google/android/apps/gmm/directions/h/v;

    .line 113
    return-void

    .line 98
    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    sget v1, Lcom/google/android/apps/gmm/l;->oC:I

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_7

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_10
    invoke-virtual {v0}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_7
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_10

    .line 102
    :cond_8
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto/16 :goto_3

    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_a
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/i;->b:[I

    invoke-virtual {v1}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/apps/gmm/l;->oB:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->ox:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->oz:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->ov:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    .line 103
    :cond_b
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 104
    :cond_c
    sget v0, Lcom/google/android/apps/gmm/f;->bG:I

    .line 105
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto/16 :goto_7

    .line 107
    :cond_d
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    goto/16 :goto_8

    .line 108
    :cond_e
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-virtual {v0}, Lcom/google/maps/g/a/da;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_10

    :cond_f
    const/4 v0, 0x1

    :goto_11
    if-nez v0, :cond_11

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-virtual {v0}, Lcom/google/maps/g/a/da;->h()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    :cond_10
    const/4 v0, 0x0

    goto :goto_11

    :cond_11
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-virtual {v0}, Lcom/google/maps/g/a/da;->d()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_9

    .line 109
    :cond_12
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    iget-object v1, v0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/i;

    iget v1, v1, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_13

    const/4 v1, 0x1

    :goto_12
    if-nez v1, :cond_14

    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_13
    const/4 v1, 0x0

    goto :goto_12

    :cond_14
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/b;

    iget-object v0, v0, Lcom/google/maps/g/a/da;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    int-to-float v3, p8

    invoke-direct {v1, v0, p7, v3}, Lcom/google/android/apps/gmm/base/views/c/b;-><init>(Ljava/lang/String;Lcom/google/r/b/a/acy;F)V

    move-object v0, v1

    goto/16 :goto_a

    .line 110
    :cond_15
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/j;->z:I

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_b

    .line 111
    :cond_16
    const/4 v1, 0x0

    goto/16 :goto_c

    .line 112
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_d

    :cond_18
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->r:Lcom/google/maps/g/a/bi;

    goto/16 :goto_e

    :cond_19
    sget-object v0, Lcom/google/android/apps/gmm/directions/i/bd;->a:Lcom/google/android/apps/gmm/directions/i/bd;

    goto/16 :goto_f

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/libraries/curvular/c;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->e:Lcom/google/android/libraries/curvular/c;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->s:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->i:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->n:Ljava/lang/CharSequence;

    if-nez v0, :cond_6

    .line 309
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ae;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/bg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_7

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ae;->h:Ljava/lang/String;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_8

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ae;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_9

    :cond_2
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ae;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_a

    :cond_3
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ae;->i:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_b

    :cond_4
    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/ae;->k:Ljava/lang/String;

    if-eqz v1, :cond_5

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_c

    :cond_5
    :goto_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->n:Ljava/lang/CharSequence;

    .line 311
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->n:Ljava/lang/CharSequence;

    return-object v0

    .line 309
    :cond_7
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_0

    :cond_8
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_1

    :cond_9
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_2

    :cond_a
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_3

    :cond_b
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_4

    :cond_c
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_5
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/android/libraries/curvular/c;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->j:Lcom/google/android/libraries/curvular/c;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->k:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->l:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->q:Lcom/google/android/apps/gmm/directions/i/bi;

    sget-object v1, Lcom/google/android/apps/gmm/directions/i/bi;->b:Lcom/google/android/apps/gmm/directions/i/bi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Lcom/google/android/apps/gmm/directions/h/v;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->p:Lcom/google/android/apps/gmm/directions/h/v;

    return-object v0
.end method

.method public final t()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->q:Lcom/google/android/apps/gmm/directions/i/bi;

    sget-object v1, Lcom/google/android/apps/gmm/directions/i/bi;->a:Lcom/google/android/apps/gmm/directions/i/bi;

    if-ne v0, v1, :cond_0

    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->r:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final v()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/ae;->o:Ljava/lang/String;

    return-object v0
.end method
