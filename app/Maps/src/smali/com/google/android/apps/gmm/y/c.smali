.class public Lcom/google/android/apps/gmm/y/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/y/g;

.field final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/y/h;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field e:Landroid/content/res/Configuration;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/apps/gmm/y/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/y/c;->a:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/y/g;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/y/c;->b:Lcom/google/android/apps/gmm/y/g;

    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/y/c;->c:Ljava/util/List;

    .line 46
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 91
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/y/c;->e:Landroid/content/res/Configuration;

    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/c;->d:Ljava/util/List;

    .line 114
    :goto_1
    return-object v0

    :cond_1
    move v2, v1

    .line 91
    goto :goto_0

    .line 94
    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/y/c;->e:Landroid/content/res/Configuration;

    .line 96
    iget-object v2, p0, Lcom/google/android/apps/gmm/y/c;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_3

    move v2, v0

    :goto_2
    if-eqz v2, :cond_4

    .line 97
    iget-object v2, p0, Lcom/google/android/apps/gmm/y/c;->b:Lcom/google/android/apps/gmm/y/g;

    iget v2, v2, Lcom/google/android/apps/gmm/y/g;->h:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 98
    sget v3, Lcom/google/android/apps/gmm/l;->bQ:I

    new-array v0, v0, [Ljava/lang/Object;

    aput-object v2, v0, v1

    .line 99
    invoke-virtual {p1, v3, v0}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/y/c;->d:Ljava/util/List;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/c;->d:Ljava/util/List;

    goto :goto_1

    :cond_3
    move v2, v1

    .line 96
    goto :goto_2

    .line 103
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/y/c;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ltz v2, :cond_5

    :goto_3
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/c;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/h;

    .line 106
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/y/h;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 107
    sget v0, Lcom/google/android/apps/gmm/l;->jG:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 109
    :cond_7
    if-nez p1, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v2, v0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, v0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    sget-object v6, Lcom/google/android/apps/gmm/y/h;->a:Ljava/util/TimeZone;

    move-object v1, p1

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;JJLjava/util/TimeZone;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 113
    :cond_9
    iput-object v7, p0, Lcom/google/android/apps/gmm/y/c;->d:Ljava/util/List;

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/c;->d:Ljava/util/List;

    goto/16 :goto_1
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 68
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/gmm/y/c;->b:Lcom/google/android/apps/gmm/y/g;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/g;->b(I)Lcom/google/android/apps/gmm/y/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/y/g;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
