.class public Lcom/google/android/apps/gmm/place/g/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/g/a/b;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/text/SpannableString;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/b;->a:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/b;->a:Ljava/util/List;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 53
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 57
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->S()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    .line 60
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->t()Ljava/lang/String;

    .line 62
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->U()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-ltz v4, :cond_1

    move v1, v2

    :goto_1
    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v1, v3

    .line 57
    goto :goto_0

    :cond_1
    move v1, v3

    .line 62
    goto :goto_1

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/b;->a:Ljava/util/List;

    .line 64
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->U()Ljava/util/List;

    move-result-object v5

    move v4, v3

    .line 65
    :goto_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-ge v4, v1, :cond_3

    const/4 v1, 0x3

    if-ge v4, v1, :cond_3

    .line 66
    iget-object v6, p0, Lcom/google/android/apps/gmm/place/g/b;->a:Ljava/util/List;

    new-instance v7, Lcom/google/android/apps/gmm/place/g/a;

    .line 68
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v8

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/uf;

    invoke-direct {v7, v8, v9, v1}, Lcom/google/android/apps/gmm/place/g/a;-><init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/maps/g/uf;)V

    .line 66
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 71
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->V()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_5

    :cond_4
    move v1, v2

    :goto_3
    if-nez v1, :cond_8

    .line 72
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/acq;

    iget-object v1, v1, Lcom/google/r/b/a/acq;->q:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ee;->d()Lcom/google/maps/g/ee;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/ee;

    iget-object v1, v1, Lcom/google/maps/g/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/uf;->h()Lcom/google/maps/g/uf;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/uf;

    iget-object v1, v1, Lcom/google/maps/g/uf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/hg;

    iget v4, v1, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v5, 0x8

    if-ne v4, v5, :cond_6

    :goto_4
    if-eqz v2, :cond_7

    invoke-virtual {v1}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v1

    .line 73
    :goto_5
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->V()Ljava/lang/String;

    move-result-object v0

    .line 75
    new-instance v2, Landroid/text/SpannableString;

    const-string v3, " -\u00a0"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/g/b;->b:Landroid/text/SpannableString;

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/b;->b:Landroid/text/SpannableString;

    new-instance v2, Landroid/text/style/TextAppearanceSpan;

    sget v3, Lcom/google/android/apps/gmm/m;->j:I

    invoke-direct {v2, p1, v3}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/g/b;->b:Landroid/text/SpannableString;

    .line 82
    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v3, v1

    add-int/lit8 v1, v1, -0x3

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/g/b;->b:Landroid/text/SpannableString;

    .line 83
    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x11

    .line 81
    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 87
    :goto_6
    return-void

    :cond_5
    move v1, v3

    .line 71
    goto/16 :goto_3

    :cond_6
    move v2, v3

    .line 72
    goto :goto_4

    :cond_7
    const-string v1, "Google"

    goto :goto_5

    .line 85
    :cond_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/b;->b:Landroid/text/SpannableString;

    goto :goto_6
.end method
