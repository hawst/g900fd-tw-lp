.class public Lcom/google/android/apps/gmm/terms/NavigationTermsFragment;
.super Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;-><init>()V

    .line 30
    const/4 v0, 0x1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "exitsOnBackPressed"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->setArguments(Landroid/os/Bundle;)V

    .line 31
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 21
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ag:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static i()Lcom/google/android/apps/gmm/terms/NavigationTermsFragment;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/terms/NavigationTermsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/terms/NavigationTermsFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method protected final b()V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ag:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    .line 37
    return-void
.end method

.method protected final o()Lcom/google/android/apps/gmm/terms/b/a;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/terms/a/b;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/terms/ProjectionTermsFragment;->a:Z

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/terms/a/b;-><init>(Lcom/google/android/apps/gmm/terms/b/b;Z)V

    return-object v0
.end method

.method protected final p()V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {}, Lcom/google/android/apps/gmm/terms/LocationSettingsFragment;->i()Lcom/google/android/apps/gmm/terms/LocationSettingsFragment;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 47
    return-void
.end method
