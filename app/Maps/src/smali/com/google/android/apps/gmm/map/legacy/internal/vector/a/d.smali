.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"


# instance fields
.field public final a:[F

.field public b:F

.field public c:F

.field public d:F

.field private final e:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/v/ao;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 80
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 60
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    .line 69
    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->c:F

    .line 72
    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->d:F

    .line 81
    iget v0, p2, Lcom/google/android/apps/gmm/v/ao;->e:I

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 82
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "LegacyRoadStrokeShaderState should only be usedon devices that don\'t support vertex texture fetching"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    aput v2, v0, v1

    .line 89
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v2, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->e:F

    .line 90
    return-void
.end method

.method public static a(IF)F
    .locals 4

    .prologue
    .line 110
    const v0, 0x400ccccd    # 2.2f

    const/high16 v1, 0x3f800000    # 1.0f

    int-to-float v2, p0

    sub-float v2, p1, v2

    const v3, 0x3f8ccccd    # 1.1f

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 4

    .prologue
    .line 142
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->c:I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->a:[F

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniform2fv(II[FI)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->d:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->e:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->c:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/e;->f:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/d;->e:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 162
    return-void
.end method
