.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field private static final b:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final c:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final d:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final e:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final f:Lcom/google/android/apps/gmm/map/b/a/y;

.field private static final g:[I

.field private static final h:[I

.field private static final i:[I

.field private static final j:F

.field private static final k:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/j;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field private l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field private final m:Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;

.field private final n:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final o:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final p:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final q:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final r:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final s:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final t:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final u:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final v:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final w:Z

.field private final x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const v3, -0xb504

    const v2, 0xb504

    const/4 v1, 0x0

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2, v1, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v1, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v1, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v3, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 64
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 66
    new-array v0, v4, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->g:[I

    .line 67
    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->h:[I

    .line 68
    new-array v0, v4, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->i:[I

    .line 82
    const/high16 v0, 0x3f400000    # 0.75f

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->j:F

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/i;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->k:Ljava/util/Comparator;

    return-void

    .line 66
    :array_0
    .array-data 4
        0xfb
        0xfc
        0xe4
    .end array-data

    .line 67
    :array_1
    .array-data 4
        0xc8
        0xcc
        0xdc
    .end array-data

    .line 68
    :array_2
    .array-data 4
        0x5
        0x0
        0x0
    .end array-data
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 233
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 234
    iget v0, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;->a:I

    const v1, 0x800a

    const/4 v2, 0x1

    .line 238
    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 235
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 240
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/k;

    iget v1, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;->b:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/k;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->m:Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;

    .line 241
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 242
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 243
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 244
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 245
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 246
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->s:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 247
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 248
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 249
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->v:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 251
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    .line 252
    if-eqz v0, :cond_0

    :cond_0
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->w:Z

    .line 253
    if-eqz v0, :cond_1

    :cond_1
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->x:Z

    .line 254
    return-void
.end method

.method private static a(IIZ)I
    .locals 2

    .prologue
    .line 579
    ushr-int/lit8 v0, p0, 0x18

    .line 580
    if-eqz p2, :cond_0

    const/16 v0, 0xff

    .line 581
    :goto_0
    const v1, 0xffffff

    and-int/2addr v1, p0

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v1

    return v0

    .line 580
    :cond_0
    invoke-static {v0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method private static a(ILcom/google/android/apps/gmm/map/b/a/y;F)I
    .locals 7

    .prologue
    .line 532
    const/high16 v0, -0x1000000

    and-int v1, p0, v0

    .line 533
    shr-int/lit8 v0, p0, 0x10

    and-int/lit16 v2, v0, 0xff

    .line 534
    shr-int/lit8 v0, p0, 0x8

    and-int/lit16 v3, v0, 0xff

    .line 535
    and-int/lit16 v4, p0, 0xff

    .line 537
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->i()F

    move-result v5

    div-float/2addr v0, v5

    float-to-int v0, v0

    .line 559
    if-gez v0, :cond_0

    .line 564
    neg-int v0, v0

    .line 566
    :cond_0
    mul-int/lit16 v0, v0, 0x4ccc

    shr-int/lit8 v0, v0, 0x10

    const v5, 0xb333

    add-int/2addr v0, v5

    int-to-float v0, v0

    .line 567
    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v5, p2

    sget v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->j:F

    mul-float/2addr v5, v6

    add-float/2addr v5, p2

    mul-float/2addr v0, v5

    .line 569
    float-to-int v0, v0

    .line 571
    mul-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    .line 572
    mul-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x10

    .line 573
    mul-int/2addr v0, v4

    shr-int/lit8 v0, v0, 0x10

    .line 575
    shl-int/lit8 v2, v2, 0x10

    or-int/2addr v1, v2

    shl-int/lit8 v2, v3, 0x8

    or-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;[Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/t/e;Lcom/google/android/apps/gmm/map/c/a;Ljava/util/List;Ljava/util/Set;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "[",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/internal/c/cp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/map/t/e;",
            "Lcom/google/android/apps/gmm/map/c/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;"
        }
    .end annotation

    .prologue
    .line 155
    new-instance v5, Ljava/util/ArrayList;

    const/16 v2, 0x80

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 156
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;-><init>()V

    .line 158
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/j;

    move-object/from16 v0, p5

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/indoor/c/u;->a(Lcom/google/android/apps/gmm/map/internal/c/j;Lcom/google/android/apps/gmm/map/c/a;)Z

    move-result v7

    .line 160
    :goto_0
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 161
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->a()Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v3

    .line 162
    instance-of v2, v3, Lcom/google/android/apps/gmm/map/internal/c/j;

    if-eqz v2, :cond_5

    move-object v2, v3

    .line 163
    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/j;

    .line 164
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v8, :cond_1

    iget-object v8, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v8, v8

    if-lez v8, :cond_1

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/j;->f()I

    move-result v8

    mul-int/lit8 v4, v4, 0x3

    shl-int/lit8 v9, v8, 0x2

    iget v10, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;->a:I

    add-int/2addr v4, v10

    add-int/2addr v4, v9

    const/16 v9, 0x4000

    if-le v4, v9, :cond_2

    iget v9, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;->a:I

    if-lez v9, :cond_2

    const/4 v4, 0x0

    :goto_2
    if-eqz v4, :cond_5

    .line 165
    move-object/from16 v0, p5

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/indoor/c/u;->a(Lcom/google/android/apps/gmm/map/internal/c/j;Lcom/google/android/apps/gmm/map/c/a;)Z

    move-result v4

    if-ne v4, v7, :cond_5

    .line 169
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/m;->j()[I

    move-result-object v4

    array-length v8, v4

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v8, :cond_3

    aget v9, v4, v3

    .line 173
    if-ltz v9, :cond_0

    array-length v10, p1

    if-ge v9, v10, :cond_0

    .line 174
    aget-object v9, p1, v9

    move-object/from16 v0, p7

    invoke-interface {v0, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 164
    :cond_1
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x9

    goto :goto_1

    :cond_2
    iput v4, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;->a:I

    iget v4, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;->b:I

    mul-int/lit8 v8, v8, 0x6

    add-int/2addr v4, v8

    iput v4, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;->b:I

    const/4 v4, 0x1

    goto :goto_2

    .line 178
    :cond_3
    invoke-interface/range {p5 .. p5}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 179
    :cond_4
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 184
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    goto :goto_0

    .line 190
    :cond_5
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->k:Ljava/util/Comparator;

    invoke-static {v5, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    .line 194
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v2

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v8, v12

    mul-double/2addr v8, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v8

    double-to-float v4, v8

    .line 195
    new-instance v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;

    move-object/from16 v0, p5

    invoke-direct {v7, p0, v6, v0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/j;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 196
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/j;

    .line 197
    invoke-direct {v7, v3, v2, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/j;F)V

    goto :goto_4

    .line 199
    :cond_6
    iget-object v3, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->m:Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/k;->a:Lcom/google/android/apps/gmm/shared/c/w;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/shared/c/w;)V

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->m:Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/k;->a:Lcom/google/android/apps/gmm/shared/c/w;

    const/4 v3, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/shared/c/w;->b:I

    .line 200
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    rem-int/lit8 v2, v2, 0x2

    shl-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    rem-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    const v3, 0x3f8147ae    # 1.01f

    mul-float/2addr v2, v3

    new-instance v3, Lcom/google/android/apps/gmm/v/bd;

    invoke-direct {v3, v2, v2}, Lcom/google/android/apps/gmm/v/bd;-><init>(FF)V

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v2

    if-lez v2, :cond_b

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v4, 0x4

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v2

    iput-object v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget-boolean v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->x:Z

    if-nez v2, :cond_8

    new-instance v2, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/t/l;->l:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v4, p0, v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x9

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Building "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v5, 0x201

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/4 v5, 0x0

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual {v2, p0}, Lcom/google/android/apps/gmm/map/t/ar;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x0

    iget-boolean v5, v2, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v5, :cond_7

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_7
    int-to-byte v4, v4

    iput-byte v4, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    :cond_8
    new-instance v2, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/t/l;->m:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v2, v4, p0, v5, v6}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x9

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Building "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v2, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-boolean v4, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->x:Z

    if-nez v4, :cond_9

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v5, 0x203

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v5, 0x302

    const/16 v6, 0x303

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    :cond_9
    invoke-virtual {v2, p0}, Lcom/google/android/apps/gmm/map/t/ar;->b(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p6

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x0

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_a

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_a
    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    :cond_b
    iget-object v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v2, 0x0

    iput-object v2, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 201
    return-object v7
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/map/internal/c/j;F)V
    .locals 13

    .prologue
    .line 268
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 269
    iget-object v10, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 270
    iget-object v0, v10, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    move v7, v0

    .line 271
    :goto_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    if-nez v0, :cond_2

    const/4 v0, 0x0

    .line 272
    :goto_1
    if-eqz v7, :cond_0

    if-nez v0, :cond_3

    .line 388
    :cond_0
    return-void

    .line 270
    :cond_1
    iget-object v0, v10, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x9

    move v7, v0

    goto :goto_0

    .line 271
    :cond_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    array-length v0, v0

    goto :goto_1

    .line 279
    :cond_3
    iget-object v11, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 280
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v3

    .line 283
    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->f:I

    .line 284
    iget v9, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->g:I

    .line 285
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->x:Z

    if-eqz v3, :cond_8

    .line 288
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v5, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 293
    :goto_2
    const/4 v2, 0x0

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    aget v2, v3, v2

    const/16 v3, 0xa0

    iget-boolean v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->w:Z

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(IIZ)I

    move-result v5

    .line 296
    const/4 v2, 0x1

    if-le v0, v2, :cond_9

    const/4 v0, 0x1

    .line 300
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/be;->e:[I

    aget v0, v1, v0

    const/16 v1, 0xa0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->w:Z

    .line 299
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(IIZ)I

    move-result v0

    move v8, v0

    .line 303
    :goto_3
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    if-eqz v0, :cond_d

    .line 304
    const/4 v0, 0x0

    move v6, v0

    :goto_4
    if-ge v6, v7, :cond_0

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v0

    .line 309
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v10, v6, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v11, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 311
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v11, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 312
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v11, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 313
    if-eqz v9, :cond_4

    .line 315
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x0

    const/4 v3, 0x0

    int-to-float v4, v9

    mul-float v4, v4, p3

    float-to-int v4, v4

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v3, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v4, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 316
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 317
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 318
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 322
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 323
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 324
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->s:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 327
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 328
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 329
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->s:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 330
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v2, 0x3

    invoke-virtual {v1, v8, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(II)V

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    int-to-short v2, v0

    add-int/lit8 v3, v0, 0x1

    int-to-short v3, v3

    add-int/lit8 v0, v0, 0x2

    int-to-short v0, v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    .line 335
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->x:Z

    if-nez v0, :cond_7

    .line 340
    const/4 v0, 0x0

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    aget-byte v1, v1, v6

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/j;->k:[B

    aget-byte v0, v2, v0

    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_5

    .line 341
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V

    .line 344
    :cond_5
    const/4 v0, 0x1

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    aget-byte v1, v1, v6

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/j;->k:[B

    aget-byte v0, v2, v0

    and-int/2addr v0, v1

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_6

    .line 345
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->s:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V

    .line 348
    :cond_6
    const/4 v0, 0x2

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    aget-byte v1, v1, v6

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/j;->k:[B

    aget-byte v0, v2, v0

    and-int/2addr v0, v1

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_7

    .line 349
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->s:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V

    .line 304
    :cond_7
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_4

    .line 290
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    const/4 v5, 0x0

    int-to-float v2, v2

    mul-float v2, v2, p3

    float-to-int v2, v2

    iput v4, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v2, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto/16 :goto_2

    .line 301
    :cond_9
    shr-int/lit8 v0, v5, 0x10

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v1, v5, 0x8

    and-int/lit16 v1, v1, 0xff

    and-int/lit16 v2, v5, 0xff

    add-int/lit16 v0, v0, 0x2fd

    shr-int/lit8 v0, v0, 0x2

    add-int/lit16 v1, v1, 0x2fd

    shr-int/lit8 v1, v1, 0x2

    add-int/lit16 v2, v2, 0x2fd

    shr-int/lit8 v2, v2, 0x2

    const/high16 v3, -0x1000000

    and-int/2addr v3, v5

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v0, v3

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    or-int/2addr v0, v2

    move v8, v0

    goto/16 :goto_3

    .line 340
    :cond_a
    const/4 v0, 0x0

    goto :goto_5

    .line 344
    :cond_b
    const/4 v0, 0x0

    goto :goto_6

    .line 348
    :cond_c
    const/4 v0, 0x0

    goto :goto_7

    .line 355
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->x:Z

    if-nez v0, :cond_10

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->i:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 357
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->h:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v11, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 361
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    if-eqz v0, :cond_f

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->b:[B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/l/k;->a(Lcom/google/android/apps/gmm/map/b/a/ax;[B)Ljava/util/List;

    move-result-object v0

    :goto_8
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_e
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 362
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v6, v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 364
    const/4 v0, 0x1

    move v9, v0

    :goto_9
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    if-ge v9, v0, :cond_e

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->u:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v6, v9, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 367
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V

    .line 369
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->n:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->o:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 364
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_9

    .line 361
    :cond_f
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->a:Lcom/google/android/apps/gmm/map/b/a/ax;

    iget-object v1, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->c:[I

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/internal/c/j;->d:[I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ax;->a([I[I)Ljava/util/List;

    move-result-object v0

    goto :goto_8

    .line 375
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v11, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 376
    const/4 v1, 0x0

    :goto_a
    if-ge v1, v7, :cond_0

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v6

    .line 378
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->t:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->s:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object v0, v10

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 379
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->q:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 380
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->r:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->s:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v2, 0x3

    invoke-virtual {v0, v8, v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(II)V

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    int-to-short v2, v6

    add-int/lit8 v3, v6, 0x1

    int-to-short v3, v3

    add-int/lit8 v4, v6, 0x2

    int-to-short v4, v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(III)V

    .line 376
    add-int/lit8 v1, v1, 0x1

    goto :goto_a
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V
    .locals 5

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b()I

    move-result v0

    .line 461
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 462
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1, p3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 463
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 464
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v1, p4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 466
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->m:Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;

    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v3, v0, 0x3

    add-int/lit8 v4, v0, 0x2

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;->a(IIII)V

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->v:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {p2, p1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->v:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v1, 0x0

    invoke-static {p5, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(ILcom/google/android/apps/gmm/map/b/a/y;F)I

    move-result v0

    .line 472
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->v:Lcom/google/android/apps/gmm/map/b/a/y;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p5, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a(ILcom/google/android/apps/gmm/map/b/a/y;F)I

    move-result v1

    .line 474
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 475
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 476
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->b(I)V

    .line 478
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v0, :cond_0

    .line 511
    const/4 v0, 0x0

    .line 514
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-nez v0, :cond_0

    .line 521
    const/4 v0, 0x0

    .line 523
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/h;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/lit8 v0, v0, 0x0

    goto :goto_0
.end method
