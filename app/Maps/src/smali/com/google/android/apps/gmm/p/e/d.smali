.class public Lcom/google/android/apps/gmm/p/e/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/p/e/e;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/p/e/a/c;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/p/e/e;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/p/e/f;

.field private final e:Lcom/google/android/apps/gmm/p/e/s;

.field private f:Lcom/google/android/apps/gmm/map/r/a/ae;

.field private g:Lcom/google/maps/g/a/hm;

.field private h:Lcom/google/android/apps/gmm/shared/net/a/l;

.field private i:D


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/p/e/a/c;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 2

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/p/e/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/p/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->e:Lcom/google/android/apps/gmm/p/e/s;

    .line 112
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 115
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    .line 128
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/d;->b:Lcom/google/android/apps/gmm/p/e/a/c;

    .line 129
    invoke-interface {p2}, Lcom/google/android/apps/gmm/map/c/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->d:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/net/a/l;->a(I)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/d;->i:D

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    .line 134
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/p/e/a/g;DDDLcom/google/android/apps/gmm/p/e/f;I[Lcom/google/android/apps/gmm/p/e/a/g;)V
    .locals 22

    .prologue
    .line 427
    aput-object p1, p10, p9

    .line 429
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/g;->j:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    .line 432
    if-nez v2, :cond_1

    .line 513
    :cond_0
    return-void

    .line 438
    :cond_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v3

    float-to-double v0, v3

    move-wide/from16 v18, v0

    .line 443
    move-object/from16 v0, p8

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->e:D

    move-object/from16 v0, p8

    iget-wide v6, v0, Lcom/google/android/apps/gmm/p/e/f;->a:D

    mul-double/2addr v4, v6

    div-double v4, v4, v18

    .line 444
    sub-double v4, p2, v4

    .line 448
    iget-wide v6, v2, Lcom/google/android/apps/gmm/p/e/e;->j:D

    cmpl-double v3, v6, v4

    if-ltz v3, :cond_2

    .line 449
    iget-wide v6, v2, Lcom/google/android/apps/gmm/p/e/e;->j:D

    sub-double v6, v6, p2

    .line 450
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    mul-double v6, v6, v18

    move-object/from16 v0, p8

    iget-wide v8, v0, Lcom/google/android/apps/gmm/p/e/f;->a:D

    div-double/2addr v6, v8

    .line 451
    add-double v6, v6, p4

    .line 453
    move-object/from16 v0, p8

    iget-wide v8, v0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    sub-double/2addr v6, v8

    move-object/from16 v0, p8

    iget-wide v8, v0, Lcom/google/android/apps/gmm/p/e/f;->k:D

    div-double/2addr v6, v8

    neg-double v8, v6

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    mul-double v6, v6, p6

    .line 454
    iget-wide v8, v2, Lcom/google/android/apps/gmm/p/e/e;->f:D

    cmpl-double v3, v6, v8

    if-lez v3, :cond_2

    iput-wide v6, v2, Lcom/google/android/apps/gmm/p/e/e;->f:D

    .line 462
    :cond_2
    if-nez p9, :cond_3

    move-wide v14, v4

    .line 465
    :goto_0
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/e/a/g;->h:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    .line 469
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v0, v2, [D

    move-object/from16 v21, v0

    .line 470
    const-wide/16 v4, 0x0

    .line 471
    const/4 v2, 0x0

    move v3, v2

    move-wide/from16 v16, v4

    :goto_1
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_6

    .line 474
    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/a/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->j:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    .line 475
    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 476
    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v4, v4, Lcom/google/r/b/a/ou;->l:F

    float-to-double v4, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 477
    iget-object v6, v6, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v6, v6, Lcom/google/r/b/a/ou;->m:F

    float-to-double v6, v6

    .line 475
    iget-boolean v8, v2, Lcom/google/android/apps/gmm/p/e/a/g;->l:Z

    if-eqz v8, :cond_4

    .line 479
    :goto_2
    aput-wide v4, v21, v3

    .line 480
    add-double v4, v4, v16

    .line 473
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-wide/from16 v16, v4

    goto :goto_1

    :cond_3
    move-wide/from16 v14, p2

    .line 462
    goto :goto_0

    .line 475
    :cond_4
    iget-boolean v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->m:Z

    if-eqz v2, :cond_5

    move-wide v4, v6

    goto :goto_2

    :cond_5
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    goto :goto_2

    .line 484
    :cond_6
    const/4 v2, 0x0

    move v13, v2

    :goto_3
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v13, v2, :cond_0

    .line 485
    move-object/from16 v0, v20

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/a/e;

    .line 486
    iget v3, v2, Lcom/google/android/apps/gmm/p/e/a/e;->b:F

    float-to-double v4, v3

    .line 488
    cmpl-double v3, v4, v14

    if-lez v3, :cond_7

    .line 489
    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 490
    iget-wide v6, v2, Lcom/google/android/apps/gmm/p/e/a/e;->d:D

    .line 491
    sub-double v4, v4, p2

    mul-double v4, v4, v18

    move-object/from16 v0, p8

    iget-wide v8, v0, Lcom/google/android/apps/gmm/p/e/f;->a:D

    div-double/2addr v4, v8

    .line 493
    add-double v4, v4, p4

    add-double/2addr v6, v4

    .line 497
    move-object/from16 v0, p8

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    move-object/from16 v0, p8

    iget-wide v10, v0, Lcom/google/android/apps/gmm/p/e/f;->k:D

    mul-double/2addr v8, v10

    add-double/2addr v4, v8

    .line 499
    const/4 v8, 0x6

    move/from16 v0, p9

    if-ge v0, v8, :cond_7

    cmpg-double v4, v6, v4

    if-gez v4, :cond_7

    .line 500
    const/4 v4, 0x0

    :goto_4
    move/from16 v0, p9

    if-ge v4, v0, :cond_9

    aget-object v5, p10, v4

    if-ne v5, v3, :cond_8

    const/4 v4, 0x1

    :goto_5
    if-nez v4, :cond_7

    .line 502
    aget-wide v4, v21, v13

    div-double v4, v4, v16

    mul-double v8, p6, v4

    .line 506
    iget v2, v2, Lcom/google/android/apps/gmm/p/e/a/e;->c:F

    float-to-double v4, v2

    add-int/lit8 v11, p9, 0x1

    move-object/from16 v2, p0

    move-object/from16 v10, p8

    move-object/from16 v12, p10

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/gmm/p/e/d;->a(Lcom/google/android/apps/gmm/p/e/a/g;DDDLcom/google/android/apps/gmm/p/e/f;I[Lcom/google/android/apps/gmm/p/e/a/g;)V

    .line 484
    :cond_7
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto :goto_3

    .line 500
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_9
    const/4 v4, 0x0

    goto :goto_5
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;)Lcom/google/android/apps/gmm/map/r/b/a;
    .locals 24
    .param p1    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 174
    if-nez p1, :cond_0

    .line 175
    const/4 v2, 0x0

    .line 193
    :goto_0
    return-object v2

    .line 178
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v14

    .line 180
    new-instance v10, Lcom/google/android/apps/gmm/p/e/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->d:Lcom/google/android/apps/gmm/p/e/f;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    move-object/from16 v0, p1

    invoke-direct {v10, v0, v2, v3}, Lcom/google/android/apps/gmm/p/e/f;-><init>(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/p/e/f;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    .line 182
    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v16

    .line 183
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v2, :cond_2

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iput-object v10, v0, Lcom/google/android/apps/gmm/p/e/d;->d:Lcom/google/android/apps/gmm/p/e/f;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v3, :cond_1

    new-instance v3, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_1
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v2, v3, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    iget-wide v6, v10, Lcom/google/android/apps/gmm/p/e/f;->a:D

    move-object/from16 v0, v16

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->e:D

    move-object/from16 v0, v16

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->f:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    mul-double/2addr v2, v6

    double-to-int v2, v2

    invoke-static {v5, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lez v8, :cond_26

    new-array v9, v8, [Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    if-ge v4, v8, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v2, v9, v4

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    invoke-static {v9}, Lcom/google/android/apps/gmm/map/b/a/ae;->b([Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v2

    const-wide/high16 v8, 0x4014000000000000L    # 5.0

    mul-double/2addr v8, v6

    double-to-int v4, v8

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v2

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v8, v3, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v9, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v9

    invoke-direct {v4, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v8, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v9, v3, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v9, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v11, v11, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v9

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v8, v9, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v2, v4, v8}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v3, v3, Lcom/google/r/b/a/ou;->f:I

    int-to-double v8, v3

    mul-double/2addr v6, v8

    double-to-int v3, v6

    invoke-static {v5, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->b:Lcom/google/android/apps/gmm/p/e/a/c;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v17

    if-nez v17, :cond_7

    move-object/from16 v0, v16

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    if-nez v2, :cond_4

    iget-boolean v2, v10, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    if-eqz v2, :cond_4

    iget-wide v2, v10, Lcom/google/android/apps/gmm/p/e/f;->g:D

    double-to-float v2, v2

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v2, 0x1

    move-object/from16 v0, v16

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    .line 185
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/ae;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_23

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 186
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v3, :cond_22

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/r/a/ad;

    :goto_6
    if-nez v3, :cond_5

    .line 187
    move-object/from16 v0, v16

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/r/b/c;->e:D

    move-object/from16 v0, v16

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/r/b/c;->f:D

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    const-wide v6, 0x40f86a0000000000L    # 100000.0

    iget-wide v8, v10, Lcom/google/android/apps/gmm/p/e/f;->a:D

    mul-double/2addr v6, v8

    invoke-virtual {v2, v3, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/b/a/y;D)Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v3

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_6

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_6
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v5, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 183
    :cond_7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/a/g;

    new-instance v4, Lcom/google/android/apps/gmm/p/e/e;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    invoke-direct {v4, v5, v2, v10, v6}, Lcom/google/android/apps/gmm/p/e/e;-><init>(Lcom/google/android/apps/gmm/map/r/a/ae;Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/f;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    iput-object v4, v2, Lcom/google/android/apps/gmm/p/e/a/g;->j:Ljava/lang/Object;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :cond_8
    iget-boolean v2, v10, Lcom/google/android/apps/gmm/p/e/f;->i:Z

    if-eqz v2, :cond_a

    iget-wide v2, v10, Lcom/google/android/apps/gmm/p/e/f;->c:D

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-eq v2, v3, :cond_a

    const/4 v2, 0x0

    move v13, v2

    :goto_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v13, v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v4, v3, Lcom/google/android/apps/gmm/p/e/a/g;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, v3, Lcom/google/android/apps/gmm/p/e/a/g;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v6, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    float-to-double v4, v4

    const/4 v6, 0x7

    new-array v12, v6, [Lcom/google/android/apps/gmm/p/e/a/g;

    const-wide/16 v6, 0x0

    iget-wide v8, v2, Lcom/google/android/apps/gmm/p/e/e;->b:D

    const/4 v11, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v12}, Lcom/google/android/apps/gmm/p/e/d;->a(Lcom/google/android/apps/gmm/p/e/a/g;DDDLcom/google/android/apps/gmm/p/e/f;I[Lcom/google/android/apps/gmm/p/e/a/g;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_9
    if-ge v3, v4, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    iget-wide v6, v2, Lcom/google/android/apps/gmm/p/e/e;->e:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/p/e/e;->f:D

    add-double/2addr v6, v8

    iput-wide v6, v2, Lcom/google/android/apps/gmm/p/e/e;->e:D

    const-wide/16 v6, 0x0

    iput-wide v6, v2, Lcom/google/android/apps/gmm/p/e/e;->f:D

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_9

    :cond_9
    add-int/lit8 v2, v13, 0x1

    move v13, v2

    goto :goto_8

    :cond_a
    const/4 v2, 0x0

    move v3, v2

    :goto_a
    move/from16 v0, v17

    if-ge v3, v0, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iput-wide v4, v2, Lcom/google/android/apps/gmm/p/e/e;->e:D

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_a

    :cond_b
    const-wide/16 v2, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v4

    new-array v7, v4, [D

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v4, v2

    :cond_c
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    iget-wide v8, v2, Lcom/google/android/apps/gmm/p/e/e;->d:D

    iget-wide v12, v2, Lcom/google/android/apps/gmm/p/e/e;->e:D

    mul-double/2addr v8, v12

    iput-wide v8, v2, Lcom/google/android/apps/gmm/p/e/e;->b:D

    iget-wide v8, v2, Lcom/google/android/apps/gmm/p/e/e;->b:D

    add-double/2addr v4, v8

    const/4 v3, 0x0

    :goto_b
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v11

    if-ge v3, v11, :cond_c

    iget-object v11, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v11, v11, Lcom/google/android/apps/gmm/p/e/a/g;->i:[Z

    aget-boolean v11, v11, v3

    if-eqz v11, :cond_d

    aget-wide v12, v7, v3

    add-double/2addr v12, v8

    aput-wide v12, v7, v3

    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_b

    :cond_e
    const-wide/16 v2, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v8, v4

    invoke-static {v2, v3, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    const-wide v8, 0x3fe999999999999aL    # 0.8

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/apps/gmm/p/e/d;->i:D

    mul-double/2addr v8, v12

    const-wide v12, 0x3fc999999999999aL    # 0.2

    mul-double/2addr v2, v12

    add-double/2addr v2, v8

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/d;->i:D

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->e:I

    move/from16 v0, v17

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    if-ltz v6, :cond_f

    const/4 v2, 0x1

    :goto_c
    if-nez v2, :cond_10

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_f
    const/4 v2, 0x0

    goto :goto_c

    :cond_10
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    move v3, v2

    :goto_d
    if-ge v3, v6, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    sub-int v9, v17, v3

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    iget-wide v12, v2, Lcom/google/android/apps/gmm/p/e/e;->b:D

    div-double/2addr v12, v4

    iput-wide v12, v2, Lcom/google/android/apps/gmm/p/e/e;->b:D

    iget-wide v12, v2, Lcom/google/android/apps/gmm/p/e/e;->b:D

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v9, v9, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v9, v9, Lcom/google/r/b/a/ou;->c:I

    const-wide/high16 v18, 0x4024000000000000L    # 10.0

    neg-int v9, v9

    int-to-double v0, v9

    move-wide/from16 v20, v0

    const-wide v22, 0x3fb999999999999aL    # 0.1

    mul-double v20, v20, v22

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v18

    cmpg-double v9, v12, v18

    if-lez v9, :cond_11

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/p/e/d;->c:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v9, Lcom/google/android/apps/gmm/map/r/b/b;

    iget-object v11, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-wide v12, v2, Lcom/google/android/apps/gmm/p/e/e;->b:D

    double-to-float v12, v12

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    iget v2, v2, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    invoke-direct {v9, v11, v12, v2}, Lcom/google/android/apps/gmm/map/r/b/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;FF)V

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_d

    :cond_11
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v2, :cond_12

    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_12
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v8, v2, Lcom/google/android/apps/gmm/map/r/b/e;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/d;->a:Ljava/util/ArrayList;

    add-int/lit8 v3, v17, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/p/e/e;

    const/4 v3, 0x0

    move v6, v3

    :goto_e
    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v3

    if-ge v6, v3, :cond_13

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v8, v2, Lcom/google/android/apps/gmm/p/e/e;->i:[Lcom/google/android/apps/gmm/map/r/a/ad;

    iget-object v9, v2, Lcom/google/android/apps/gmm/p/e/e;->h:Lcom/google/android/apps/gmm/p/e/f;

    iget-object v11, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v12, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    iget v12, v12, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    float-to-double v12, v12

    invoke-virtual {v9, v3, v11, v12, v13}, Lcom/google/android/apps/gmm/p/e/f;->a(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/b/a/y;D)Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v3

    aput-object v3, v8, v6

    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_e

    :cond_13
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/google/android/apps/gmm/p/e/d;->i:D

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    iget-object v3, v3, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v3, v3, Lcom/google/r/b/a/ou;->d:I

    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/net/a/l;->a(I)D

    move-result-wide v12

    cmpl-double v3, v8, v12

    if-lez v3, :cond_19

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v4, v3, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_14

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_14
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v4, v5, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    if-nez v4, :cond_15

    const/4 v3, 0x0

    :cond_15
    iput-object v3, v5, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    if-nez v3, :cond_16

    iget-boolean v3, v10, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    if-eqz v3, :cond_16

    iget-wide v4, v10, Lcom/google/android/apps/gmm/p/e/f;->g:D

    double-to-float v3, v4

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v3, 0x1

    move-object/from16 v0, v16

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    :cond_16
    const/4 v3, 0x0

    move v4, v3

    :goto_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v3

    if-ge v4, v3, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v5, v2, Lcom/google/android/apps/gmm/p/e/e;->i:[Lcom/google/android/apps/gmm/map/r/a/ad;

    aget-object v5, v5, v4

    if-eqz v5, :cond_18

    iget-object v6, v10, Lcom/google/android/apps/gmm/p/e/f;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v7, v5, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v6

    float-to-double v6, v6

    new-instance v8, Lcom/google/android/apps/gmm/map/r/a/ad;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/r/a/ad;-><init>()V

    iget-object v9, v5, Lcom/google/android/apps/gmm/map/r/a/ad;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iput-object v9, v8, Lcom/google/android/apps/gmm/map/r/a/ad;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v9, v5, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v9, v8, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-wide v12, v5, Lcom/google/android/apps/gmm/map/r/a/ad;->c:D

    iput-wide v12, v8, Lcom/google/android/apps/gmm/map/r/a/ad;->c:D

    iput-wide v6, v8, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    iget v5, v5, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    iput v5, v8, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v5, :cond_17

    new-instance v5, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_17
    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v5, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_18
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_f

    :cond_19
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    sget-object v6, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-eq v3, v6, :cond_1a

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v3

    const-wide v12, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v8, v8, v18

    mul-double/2addr v8, v12

    const-wide v12, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v12

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v12

    move-object/from16 v0, v16

    invoke-virtual {v0, v8, v9, v12, v13}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    :cond_1a
    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    iget v3, v3, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    const/4 v3, 0x1

    move-object/from16 v0, v16

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    const/4 v3, 0x1

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v6, :cond_1b

    new-instance v6, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_1b
    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-boolean v3, v6, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    iget-object v6, v3, Lcom/google/android/apps/gmm/p/e/a/g;->a:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget-object v3, v2, Lcom/google/android/apps/gmm/p/e/e;->k:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v8, :cond_1c

    new-instance v8, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v8, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_1c
    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v6, v8, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    if-nez v6, :cond_1d

    const/4 v3, 0x0

    :cond_1d
    iput-object v3, v8, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x0

    move v6, v3

    :goto_10
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v3

    if-ge v6, v3, :cond_21

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v8, v2, Lcom/google/android/apps/gmm/p/e/e;->i:[Lcom/google/android/apps/gmm/map/r/a/ad;

    aget-object v8, v8, v6

    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v9, :cond_1e

    new-instance v9, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v9, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_1e
    move-object/from16 v0, v16

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v9, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide/16 v8, 0x0

    cmpl-double v8, v4, v8

    if-lez v8, :cond_20

    aget-wide v8, v7, v6

    div-double/2addr v8, v4

    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v11, :cond_1f

    new-instance v11, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v11, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_1f
    move-object/from16 v0, v16

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v11, v11, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-interface {v11, v3, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_20
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_10

    :cond_21
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/d;->e:Lcom/google/android/apps/gmm/p/e/s;

    iget-object v2, v2, Lcom/google/android/apps/gmm/p/e/e;->g:Lcom/google/android/apps/gmm/p/e/a/g;

    move-object/from16 v0, v16

    invoke-virtual {v3, v2, v0}, Lcom/google/android/apps/gmm/p/e/s;->a(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/map/r/b/c;)V

    goto/16 :goto_4

    .line 186
    :cond_22
    const/4 v3, 0x0

    goto/16 :goto_6

    .line 191
    :cond_23
    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v14

    .line 192
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v4, :cond_24

    new-instance v4, Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>()V

    move-object/from16 v0, v16

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    :cond_24
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-wide v2, v4, Lcom/google/android/apps/gmm/map/r/b/e;->i:J

    .line 193
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v2, :cond_25

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "latitude and longitude must be set"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_25
    new-instance v2, Lcom/google/android/apps/gmm/map/r/b/a;

    move-object/from16 v0, v16

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    goto/16 :goto_0

    :cond_26
    move-object v2, v3

    goto/16 :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/base/a/b;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 159
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/ae;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->b:Lcom/google/android/apps/gmm/p/e/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/r/a/ae;)V

    .line 161
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/base/a/b;->a:Lcom/google/android/apps/gmm/navigation/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/b/a;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->b:Lcom/google/android/apps/gmm/p/e/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    iput-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/c;->a:Lcom/google/maps/g/a/hm;

    .line 163
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->LOCATION_DISPATCHER:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 146
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->b:Lcom/google/android/apps/gmm/p/e/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/d;->f:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/p/e/a/c;->a(Lcom/google/android/apps/gmm/map/r/a/ae;)V

    .line 149
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/e/d;->b:Lcom/google/android/apps/gmm/p/e/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/d;->g:Lcom/google/maps/g/a/hm;

    iput-object v1, v0, Lcom/google/android/apps/gmm/p/e/a/c;->a:Lcom/google/maps/g/a/hm;

    .line 151
    return-void
.end method
