.class public Lcom/google/android/apps/gmm/base/f/r;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/i/c;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 8

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 31
    const/4 v0, 0x7

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    .line 32
    sget v0, Lcom/google/android/apps/gmm/d;->ak:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v5

    .line 33
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v7

    const/4 v0, -0x2

    .line 34
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v2, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v4

    const/4 v0, 0x3

    const/4 v2, -0x1

    .line 35
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x4

    .line 39
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/i/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/c;->c()Ljava/lang/CharSequence;

    move-result-object v3

    .line 40
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/i/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/c;->d()Lcom/google/android/apps/gmm/base/l/a/u;

    move-result-object v0

    .line 38
    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/place/d/a/e;->a(Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/base/l/a/u;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x5

    new-array v2, v4, [Lcom/google/android/libraries/curvular/cu;

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/f/r;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v5

    .line 45
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v2, v7

    .line 43
    new-instance v3, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v3, v2}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v2, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x6

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/i/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/c;->f()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/i/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/i/c;->e()Lcom/google/android/libraries/curvular/cf;

    move-result-object v0

    new-array v4, v5, [Lcom/google/android/libraries/curvular/cu;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v0, v3, v6}, Lcom/google/android/apps/gmm/base/f/ae;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    invoke-virtual {v0, v4, v7}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    aput-object v0, v1, v2

    .line 31
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.LinearLayout"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 27
    check-cast p2, Lcom/google/android/apps/gmm/place/i/c;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/i/c;->b()Ljava/util/List;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/gmm/base/f/s;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    return-void
.end method
