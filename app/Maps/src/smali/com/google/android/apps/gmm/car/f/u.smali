.class Lcom/google/android/apps/gmm/car/f/u;
.super Lcom/google/android/apps/gmm/car/f/y;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/f/p;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/f/p;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/car/f/y;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 367
    invoke-static {}, Lcom/google/android/apps/gmm/car/n/c;->s()Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x1

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/car/f/y;
    .locals 2

    .prologue
    .line 399
    new-instance v0, Lcom/google/android/apps/gmm/car/f/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/car/f/t;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    return-object v0
.end method

.method public final j()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->f:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/h/p;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 373
    const-string v0, ""

    .line 375
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->j:Lcom/google/android/apps/gmm/car/f/r;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    .line 376
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/f/p;->f:Lcom/google/android/apps/gmm/car/h/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/f/p;->f:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/car/h/p;->b()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/car/h/p;->b(I)I

    move-result v1

    .line 375
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/f/r;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()Lcom/google/android/libraries/curvular/aq;
    .locals 5

    .prologue
    const v4, 0x106000d

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->f:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/h/p;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 382
    invoke-super {p0}, Lcom/google/android/apps/gmm/car/f/y;->k()Lcom/google/android/libraries/curvular/aq;

    move-result-object v0

    .line 388
    :goto_0
    return-object v0

    .line 384
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    .line 385
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->f:Lcom/google/android/apps/gmm/car/h/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/f/p;->f:Lcom/google/android/apps/gmm/car/h/p;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/car/h/p;->b()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/h/p;->d(I)Lcom/google/maps/g/a/fz;

    move-result-object v1

    .line 386
    new-instance v2, Lcom/google/android/apps/gmm/base/k/am;

    const/4 v0, 0x0

    .line 387
    invoke-static {v1, v4, v0}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/maps/g/a/fz;IZ)I

    move-result v0

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->b:Lcom/google/android/libraries/curvular/aq;

    :goto_1
    const/4 v3, 0x1

    .line 388
    invoke-static {v1, v4, v3}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/maps/g/a/fz;IZ)I

    move-result v1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/u;->a:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/f/p;->b:Lcom/google/android/libraries/curvular/aq;

    :goto_2
    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/base/k/am;-><init>(Lcom/google/android/libraries/curvular/aq;Lcom/google/android/libraries/curvular/aq;)V

    move-object v0, v2

    goto :goto_0

    .line 387
    :cond_1
    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v0

    goto :goto_1

    .line 388
    :cond_2
    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    goto :goto_2
.end method
