.class public Lcom/google/android/apps/gmm/q/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/q/d;


# instance fields
.field public a:Lcom/google/android/apps/gmm/q/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/gmm/q/i;->b:Landroid/content/Context;

    .line 20
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/q/a;)V
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/q/i;->a:Lcom/google/android/apps/gmm/q/a;

    .line 25
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/q/a;->b()V

    .line 26
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/q/c;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/i;->b:Landroid/content/Context;

    const-string v1, "uimode"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 47
    sget-object v1, Lcom/google/android/apps/gmm/q/j;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/q/c;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 58
    :goto_0
    return-void

    .line 49
    :pswitch_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    goto :goto_0

    .line 52
    :pswitch_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    goto :goto_0

    .line 55
    :pswitch_2
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    goto :goto_0

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/q/i;->b:Landroid/content/Context;

    const-string v1, "uimode"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 36
    invoke-virtual {v0}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/q/i;->a:Lcom/google/android/apps/gmm/q/a;

    .line 31
    return-void
.end method
