.class public Lcom/google/android/apps/gmm/v/q;
.super Lcom/google/android/apps/gmm/v/d;
.source "PG"


# static fields
.field public static final a:Landroid/view/animation/Interpolator;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/v/a/a;

.field private c:Lcom/google/android/apps/gmm/v/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/v/q;->a:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/c;)V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/d;-><init>(Lcom/google/android/apps/gmm/v/c;)V

    .line 37
    check-cast p1, Lcom/google/android/apps/gmm/v/a/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/v/q;->b:Lcom/google/android/apps/gmm/v/a/a;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/q;->b:Lcom/google/android/apps/gmm/v/a/a;

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ColorAnimationBehavior expects a ColorAnimation3D"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    const-wide/16 v0, 0x1f4

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/v/c;->setDuration(J)V

    .line 45
    sget-object v0, Lcom/google/android/apps/gmm/v/q;->a:Landroid/view/animation/Interpolator;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/c;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 46
    return-void
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 56
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/q;->c:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/q;->c:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 59
    :cond_0
    monitor-exit p0

    return-void

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/q;->c:Lcom/google/android/apps/gmm/v/g;

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->start()V

    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/q;->c()V

    .line 53
    return-void
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/q;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->a()V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/d;->j:Lcom/google/android/apps/gmm/v/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/c;->hasEnded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/q;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    :cond_0
    monitor-exit p0

    return-void

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
