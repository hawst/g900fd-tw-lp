.class public Lcom/google/android/apps/gmm/place/bv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/cj;


# instance fields
.field private a:Landroid/content/Context;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/place/an;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private c:Lcom/google/android/apps/gmm/x/o;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->c:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->c:Lcom/google/android/apps/gmm/x/o;

    .line 46
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-boolean v0, v0, Lcom/google/maps/g/ki;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->c:Lcom/google/android/apps/gmm/x/o;

    .line 47
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ai()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    move v0, v1

    .line 45
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 47
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bv;->a:Landroid/content/Context;

    .line 35
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/bv;->b:Lcom/google/android/apps/gmm/place/an;

    .line 36
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/bv;->c:Lcom/google/android/apps/gmm/x/o;

    .line 37
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dA:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 38
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 39
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->d:Lcom/google/android/apps/gmm/z/b/l;

    .line 40
    return-void
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/bv;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    const-string v0, ""

    .line 68
    :goto_0
    return-object v0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ai()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->u:Ljava/lang/String;

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bv;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->mE:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 60
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/bv;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 61
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bv;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->X:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 62
    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 63
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int v4, v3, v0

    .line 64
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 65
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0, v1, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 68
    goto :goto_0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 73
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/bv;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->c:Lcom/google/android/apps/gmm/x/o;

    .line 74
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->ai()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    move v0, v1

    .line 73
    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 74
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->b:Lcom/google/android/apps/gmm/place/an;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->b:Lcom/google/android/apps/gmm/place/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bv;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/an;->j(Lcom/google/android/apps/gmm/x/o;)V

    .line 83
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bv;->d:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
