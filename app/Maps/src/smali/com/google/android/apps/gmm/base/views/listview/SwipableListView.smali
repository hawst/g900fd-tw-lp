.class public Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;
.super Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/views/listview/b;

.field private b:Landroid/view/View;

.field private c:I

.field private k:Lcom/google/android/apps/gmm/base/views/listview/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 79
    new-instance v1, Lcom/google/android/apps/gmm/base/views/listview/b;

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    const/16 v2, 0xc8

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/base/views/listview/b;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    .line 82
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->setFocusable(Z)V

    .line 83
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    if-nez v0, :cond_0

    .line 232
    :goto_0
    return-void

    .line 227
    :cond_0
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->setPressed(Z)V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    goto :goto_0
.end method

.method private a(FF)V
    .locals 10

    .prologue
    const/16 v9, 0x16

    const/4 v8, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 165
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getFirstVisiblePosition()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_0
    shl-int/lit8 v4, v4, 0x10

    add-int/2addr v0, v4

    iget v4, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-eq v4, v2, :cond_0

    iget v4, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_2

    .line 166
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-ne v0, v8, :cond_5

    move v0, v2

    :goto_2
    if-eqz v0, :cond_1

    .line 167
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->setPressed(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->b:F

    sub-float v0, p1, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/view/View;->setTranslationX(F)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getMeasuredWidth()I

    move-result v3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v3, v3

    const v4, 0x3ecccccd    # 0.4f

    mul-float/2addr v3, v4

    div-float/2addr v0, v3

    const v3, 0x3f733333    # 0.95f

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 169
    :cond_1
    return-void

    .line 165
    :cond_2
    iget v4, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-eq v4, v8, :cond_0

    iget v4, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->d:I

    if-eq v4, v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Set state: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v2, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    goto :goto_1

    :cond_3
    iget v0, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->b:F

    sub-float v0, p1, v0

    iget v4, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->c:F

    sub-float v4, p2, v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v6, v7

    cmpl-float v5, v5, v6

    if-lez v5, :cond_4

    mul-float/2addr v0, v0

    mul-float/2addr v4, v4

    add-float/2addr v0, v4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iget v0, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->e:I

    int-to-double v6, v0

    cmpl-double v0, v4, v6

    if-lez v0, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Set state: "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v8, v3, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    goto/16 :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    .line 166
    goto/16 :goto_2

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Landroid/view/View;IJLcom/google/android/apps/gmm/base/views/listview/c;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Set state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v1, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a()V

    .line 198
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 195
    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/16 v7, 0x16

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    const/4 v3, 0x3

    if-ne v0, v3, :cond_1

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    move v0, v2

    .line 157
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 134
    goto :goto_0

    .line 138
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 139
    packed-switch v0, :pswitch_data_0

    .line 148
    :goto_2
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    move v1, v2

    :cond_3
    if-eqz v1, :cond_7

    move v0, v2

    .line 149
    goto :goto_1

    .line 141
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->k:Lcom/google/android/apps/gmm/base/views/listview/d;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->k:Lcom/google/android/apps/gmm/base/views/listview/d;

    float-to-int v5, v3

    float-to-int v6, v4

    invoke-virtual {p0, v5, v6}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->pointToPosition(II)I

    move-result v5

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/base/views/listview/d;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_4

    float-to-int v0, v3

    float-to-int v5, v4

    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->pointToPosition(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->c:I

    iget v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->c:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getFirstVisiblePosition()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getFirstVisiblePosition()I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    :goto_4
    shl-int/lit8 v6, v6, 0x10

    add-int/2addr v0, v6

    iput v3, v5, Lcom/google/android/apps/gmm/base/views/listview/b;->b:F

    iput v4, v5, Lcom/google/android/apps/gmm/base/views/listview/b;->c:F

    iput v0, v5, Lcom/google/android/apps/gmm/base/views/listview/b;->d:I

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    const/4 v3, 0x4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Set state: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v3, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    goto :goto_2

    .line 144
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a(FF)V

    goto/16 :goto_2

    .line 152
    :cond_7
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 153
    if-eqz v0, :cond_0

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Set state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v2, v1, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    goto/16 :goto_1

    :cond_8
    move v0, v1

    goto :goto_4

    .line 139
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/16 v5, 0x16

    const/4 v4, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-ne v0, v4, :cond_1

    move v0, v7

    :goto_0
    if-eqz v0, :cond_2

    .line 129
    :cond_0
    :goto_1
    return v7

    :cond_1
    move v0, v8

    .line 98
    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 103
    packed-switch v0, :pswitch_data_0

    .line 125
    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-ne v0, v9, :cond_4

    move v8, v7

    :cond_4
    if-nez v8, :cond_0

    .line 129
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v7

    goto :goto_1

    .line 106
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-ne v0, v9, :cond_7

    move v0, v7

    .line 107
    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget v3, v2, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-ne v3, v9, :cond_9

    iget v3, v2, Lcom/google/android/apps/gmm/base/views/listview/b;->b:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, v2, Lcom/google/android/apps/gmm/base/views/listview/b;->f:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_8

    move v1, v7

    :goto_4
    if-eqz v1, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Set state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v4, v2, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    .line 108
    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v1, v1, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-ne v1, v4, :cond_a

    move v1, v7

    :goto_6
    if-eqz v1, :cond_5

    .line 109
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->c:I

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 110
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->b:Landroid/view/View;

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->c:I

    new-instance v6, Lcom/google/android/apps/gmm/base/views/listview/c;

    invoke-direct {v6, p0}, Lcom/google/android/apps/gmm/base/views/listview/c;-><init>(Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;)V

    move-object v1, p0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a(Landroid/view/View;IJLcom/google/android/apps/gmm/base/views/listview/c;)V

    .line 112
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a:Lcom/google/android/apps/gmm/base/views/listview/b;

    iget v1, v1, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    if-nez v1, :cond_b

    move v1, v7

    :goto_7
    if-eqz v1, :cond_6

    .line 113
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a()V

    .line 115
    :cond_6
    if-eqz v0, :cond_3

    goto :goto_1

    :cond_7
    move v0, v8

    .line 106
    goto :goto_3

    :cond_8
    move v1, v8

    .line 107
    goto :goto_4

    :cond_9
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Set state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    iput v8, v2, Lcom/google/android/apps/gmm/base/views/listview/b;->a:I

    goto :goto_5

    :cond_a
    move v1, v8

    .line 108
    goto :goto_6

    :cond_b
    move v1, v8

    .line 112
    goto :goto_7

    .line 121
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->a(FF)V

    goto/16 :goto_2

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 32
    check-cast p1, Landroid/widget/ListAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 1

    .prologue
    .line 250
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/listview/ListViewProxy;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 252
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/listview/d;

    if-eqz v0, :cond_0

    .line 253
    check-cast p1, Lcom/google/android/apps/gmm/base/views/listview/d;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->k:Lcom/google/android/apps/gmm/base/views/listview/d;

    .line 257
    :goto_0
    return-void

    .line 255
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/SwipableListView;->k:Lcom/google/android/apps/gmm/base/views/listview/d;

    goto :goto_0
.end method
