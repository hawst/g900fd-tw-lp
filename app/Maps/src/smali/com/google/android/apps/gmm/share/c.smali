.class public Lcom/google/android/apps/gmm/share/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/share/b;


# instance fields
.field a:Ljava/lang/String;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/graphics/drawable/BitmapDrawable;

.field private final e:Landroid/support/v7/internal/widget/k;

.field private final f:[Lcom/google/android/apps/gmm/share/a/a;

.field private final g:Landroid/content/pm/ResolveInfo;

.field private final h:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/pm/ResolveInfo;Landroid/support/v7/internal/widget/k;[Lcom/google/android/apps/gmm/share/a/a;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/share/c;->b:Landroid/content/Context;

    .line 40
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/c;->c:Ljava/lang/String;

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/c;->d:Landroid/graphics/drawable/BitmapDrawable;

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/gmm/share/c;->e:Landroid/support/v7/internal/widget/k;

    .line 43
    iput-object p4, p0, Lcom/google/android/apps/gmm/share/c;->f:[Lcom/google/android/apps/gmm/share/a/a;

    .line 44
    iput-object p2, p0, Lcom/google/android/apps/gmm/share/c;->g:Landroid/content/pm/ResolveInfo;

    .line 45
    iput-object p5, p0, Lcom/google/android/apps/gmm/share/c;->a:Ljava/lang/String;

    .line 46
    const-string v0, "com.google.android.apps.docs.app.SendTextToClipboardActivity"

    iget-object v1, p2, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    sget-object v0, Lcom/google/b/f/t;->eV:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/c;->h:Lcom/google/android/apps/gmm/z/b/l;

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/share/c;->h:Lcom/google/android/apps/gmm/z/b/l;

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/c;->d:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/c;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 71
    iget-object v1, p0, Lcom/google/android/apps/gmm/share/c;->e:Landroid/support/v7/internal/widget/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/share/c;->g:Landroid/content/pm/ResolveInfo;

    invoke-virtual {v1, v2}, Landroid/support/v7/internal/widget/k;->a(Landroid/content/pm/ResolveInfo;)I

    move-result v1

    .line 72
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-object v8

    .line 79
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/share/c;->e:Landroid/support/v7/internal/widget/k;

    invoke-virtual {v2, v1}, Landroid/support/v7/internal/widget/k;->b(I)Landroid/content/Intent;

    move-result-object v1

    .line 80
    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 81
    const-string v3, "urlFormatMessageId"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 83
    if-eqz v2, :cond_3

    .line 84
    if-lez v3, :cond_2

    .line 85
    const-string v4, "android.intent.extra.TEXT"

    iget-object v5, p0, Lcom/google/android/apps/gmm/share/c;->b:Landroid/content/Context;

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/google/android/apps/gmm/share/c;->a:Ljava/lang/String;

    aput-object v7, v6, v0

    .line 86
    invoke-virtual {v5, v3, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 97
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/share/c;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    .line 98
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->startActivity(Landroid/content/Intent;)V

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/share/c;->f:[Lcom/google/android/apps/gmm/share/a/a;

    if-eqz v1, :cond_5

    .line 100
    iget-object v3, p0, Lcom/google/android/apps/gmm/share/c;->f:[Lcom/google/android/apps/gmm/share/a/a;

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_5

    aget-object v5, v3, v1

    .line 101
    iget-object v6, p0, Lcom/google/android/apps/gmm/share/c;->b:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/apps/gmm/share/c;->g:Landroid/content/pm/ResolveInfo;

    invoke-interface {v5, v6, v0, v7}, Lcom/google/android/apps/gmm/share/a/a;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/z/a/b;Landroid/content/pm/ResolveInfo;)V

    .line 100
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 88
    :cond_2
    const-string v3, "android.intent.extra.TEXT"

    iget-object v4, p0, Lcom/google/android/apps/gmm/share/c;->a:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v6, v7

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 91
    :cond_3
    if-lez v3, :cond_4

    .line 92
    const-string v2, "android.intent.extra.TEXT"

    iget-object v4, p0, Lcom/google/android/apps/gmm/share/c;->b:Landroid/content/Context;

    new-array v5, v6, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/gmm/share/c;->a:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-virtual {v4, v3, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 94
    :cond_4
    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, Lcom/google/android/apps/gmm/share/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 104
    :cond_5
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v1, v0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/BottomSheetDialogFragment;->dismiss()V

    goto/16 :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/share/c;->h:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
