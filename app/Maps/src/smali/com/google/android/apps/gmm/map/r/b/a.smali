.class public Lcom/google/android/apps/gmm/map/r/b/a;
.super Lcom/google/android/apps/gmm/p/d/f;
.source "PG"


# instance fields
.field public final h:Lcom/google/android/apps/gmm/map/b/a/u;

.field public i:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field public final j:J

.field public final k:Lcom/google/android/apps/gmm/map/r/b/e;

.field public final l:Lcom/google/android/apps/gmm/map/r/b/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/b/c;)V
    .locals 2

    .prologue
    .line 323
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/p/d/f;-><init>(Ljava/lang/String;)V

    .line 324
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    if-eqz v0, :cond_0

    .line 325
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/p/d/f;->setAccuracy(F)V

    .line 327
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->r:Z

    if-eqz v0, :cond_1

    .line 328
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->b:D

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/p/d/f;->setAltitude(D)V

    .line 330
    :cond_1
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    if-eqz v0, :cond_2

    .line 331
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/p/d/f;->setBearing(F)V

    .line 333
    :cond_2
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->e:D

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/p/d/f;->setLatitude(D)V

    .line 334
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->f:D

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/p/d/f;->setLongitude(D)V

    .line 335
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    if-eqz v0, :cond_3

    .line 336
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->h:F

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/p/d/f;->setSpeed(F)V

    .line 338
    :cond_3
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    if-eqz v0, :cond_4

    .line 339
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->i:J

    invoke-super {p0, v0, v1}, Lcom/google/android/apps/gmm/p/d/f;->setTime(J)V

    .line 341
    :cond_4
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->g:Z

    .line 346
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->v:Z

    if-eqz v0, :cond_8

    .line 347
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->j:J

    :goto_0
    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    .line 348
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->d:Landroid/os/Bundle;

    invoke-super {p0, v0}, Lcom/google/android/apps/gmm/p/d/f;->setExtras(Landroid/os/Bundle;)V

    .line 349
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 350
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->k:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v0, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/l;->b()Lcom/google/o/b/a/h;

    move-result-object v1

    iget v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    iput-object v1, p0, Lcom/google/android/apps/gmm/p/d/f;->c:Lcom/google/o/b/a/h;

    iput v0, p0, Lcom/google/android/apps/gmm/p/d/f;->d:I

    .line 351
    :cond_5
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    .line 352
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    .line 353
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->o:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v0, :cond_6

    .line 354
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->o:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->b()Lcom/google/o/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/d/f;->e:Lcom/google/o/b/a/h;

    .line 356
    :cond_6
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->w:Z

    if-eqz v0, :cond_7

    .line 357
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/b/c;->p:I

    iput v0, p0, Lcom/google/android/apps/gmm/p/d/f;->f:I

    .line 359
    :cond_7
    return-void

    .line 347
    :cond_8
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 62
    const-string v1, "networkLocationType"

    const-string v2, "wifi"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    if-eqz p0, :cond_0

    .line 65
    const-string v1, "levelId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/l;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    const-string v1, "levelNumberE3"

    iget v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/indoor/d/f;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/high16 v4, -0x80000000

    .line 76
    invoke-virtual {p0}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 77
    if-nez v1, :cond_1

    .line 99
    :cond_0
    :goto_0
    return-object v0

    .line 81
    :cond_1
    const-string v2, "levelId"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 82
    const-string v3, "levelNumberE3"

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 85
    if-eqz v2, :cond_0

    .line 86
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/l;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/l;

    move-result-object v3

    .line 87
    if-eqz v3, :cond_3

    .line 88
    if-ne v1, v4, :cond_2

    .line 90
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Missing level number for "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 91
    const-string v2, "LOCATION"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v0, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-direct {v0, v3, v1}, Lcom/google/android/apps/gmm/map/indoor/d/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/l;I)V

    goto :goto_0

    .line 95
    :cond_3
    const-string v1, "invalid feature id: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v1, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Landroid/location/Location;)I
    .locals 2

    .prologue
    const v0, 0x1869f

    .line 369
    if-nez p0, :cond_1

    .line 376
    :cond_0
    :goto_0
    return v0

    .line 372
    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 376
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public static c(Landroid/location/Location;)I
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 387
    if-nez p0, :cond_1

    .line 393
    :cond_0
    :goto_0
    return v0

    .line 390
    :cond_1
    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 393
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/a/w;)D
    .locals 2

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 704
    :goto_0
    if-nez v0, :cond_1

    const-wide/16 v0, 0x0

    :goto_1
    return-wide v0

    .line 701
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    .line 702
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    goto :goto_0

    .line 704
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/n;)F
    .locals 12

    .prologue
    const-wide v10, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 845
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 846
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/n;->a:I

    int-to-double v4, v4

    mul-double/2addr v4, v10

    .line 847
    iget v6, p1, Lcom/google/android/apps/gmm/map/b/a/n;->b:I

    int-to-double v6, v6

    mul-double/2addr v6, v10

    .line 846
    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    .line 848
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 12

    .prologue
    .line 853
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 854
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v4

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v10

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    .line 855
    const/4 v0, 0x0

    aget v0, v8, v0

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 442
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v2, :cond_1

    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    .line 446
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v2, v0

    .line 442
    goto :goto_0

    .line 445
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 446
    :goto_2
    if-eqz v2, :cond_0

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/az;->f:I

    const/16 v3, 0x10

    and-int/2addr v2, v3

    if-eqz v2, :cond_4

    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_1

    .line 445
    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    move v2, v0

    .line 446
    goto :goto_3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 486
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v2, :cond_1

    .line 541
    :cond_0
    :goto_0
    return v0

    .line 489
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 491
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/u;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/u;

    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    .line 495
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    .line 499
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAccuracy()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v3

    float-to-double v4, v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAccuracy()Z

    move-result v3

    .line 500
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v6

    float-to-double v6, v6

    .line 499
    if-eqz v2, :cond_c

    if-nez v3, :cond_a

    move v2, v0

    :goto_3
    if-eqz v2, :cond_0

    .line 503
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAltitude()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getAltitude()D

    move-result-wide v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAltitude()Z

    move-result v3

    .line 504
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAltitude()D

    move-result-wide v6

    .line 503
    if-eqz v2, :cond_10

    if-nez v3, :cond_e

    move v2, v0

    :goto_4
    if-eqz v2, :cond_0

    .line 507
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v3

    float-to-double v4, v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v3

    .line 508
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v6

    float-to-double v6, v6

    .line 507
    if-eqz v2, :cond_14

    if-nez v3, :cond_12

    move v2, v0

    :goto_5
    if-eqz v2, :cond_0

    .line 511
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_16

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    :cond_4
    move v2, v1

    :goto_6
    if-eqz v2, :cond_0

    .line 514
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-nez v2, :cond_17

    move v2, v1

    :goto_7
    if-eqz v2, :cond_0

    .line 517
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-nez v2, :cond_18

    move v2, v1

    :goto_8
    if-eqz v2, :cond_0

    .line 520
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getProvider()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getProvider()Ljava/lang/String;

    move-result-object v3

    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_19

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    :cond_5
    move v2, v1

    :goto_9
    if-eqz v2, :cond_0

    .line 523
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v3

    float-to-double v4, v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v3

    .line 524
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v6

    float-to-double v6, v6

    .line 523
    if-eqz v2, :cond_1c

    if-nez v3, :cond_1a

    move v2, v0

    :goto_a
    if-eqz v2, :cond_0

    .line 527
    iget-boolean v2, p1, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getTime()J

    move-result-wide v4

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    .line 528
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getTime()J

    move-result-wide v6

    .line 527
    if-eqz v2, :cond_20

    if-nez v3, :cond_1e

    move v2, v0

    :goto_b
    if-eqz v2, :cond_0

    .line 532
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 535
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_22

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_22

    :cond_6
    move v2, v1

    :goto_c
    if-eqz v2, :cond_0

    .line 538
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eq v2, v3, :cond_7

    if-eqz v2, :cond_23

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_23

    :cond_7
    move v2, v1

    :goto_d
    if-eqz v2, :cond_0

    move v0, v1

    .line 541
    goto/16 :goto_0

    :cond_8
    move v2, v0

    .line 491
    goto/16 :goto_1

    :cond_9
    move v2, v0

    .line 495
    goto/16 :goto_2

    .line 499
    :cond_a
    cmpl-double v2, v4, v6

    if-nez v2, :cond_b

    move v2, v1

    goto/16 :goto_3

    :cond_b
    move v2, v0

    goto/16 :goto_3

    :cond_c
    if-eqz v3, :cond_d

    move v2, v0

    goto/16 :goto_3

    :cond_d
    move v2, v1

    goto/16 :goto_3

    .line 503
    :cond_e
    cmpl-double v2, v4, v6

    if-nez v2, :cond_f

    move v2, v1

    goto/16 :goto_4

    :cond_f
    move v2, v0

    goto/16 :goto_4

    :cond_10
    if-eqz v3, :cond_11

    move v2, v0

    goto/16 :goto_4

    :cond_11
    move v2, v1

    goto/16 :goto_4

    .line 507
    :cond_12
    cmpl-double v2, v4, v6

    if-nez v2, :cond_13

    move v2, v1

    goto/16 :goto_5

    :cond_13
    move v2, v0

    goto/16 :goto_5

    :cond_14
    if-eqz v3, :cond_15

    move v2, v0

    goto/16 :goto_5

    :cond_15
    move v2, v1

    goto/16 :goto_5

    :cond_16
    move v2, v0

    .line 511
    goto/16 :goto_6

    :cond_17
    move v2, v0

    .line 514
    goto/16 :goto_7

    :cond_18
    move v2, v0

    .line 517
    goto/16 :goto_8

    :cond_19
    move v2, v0

    .line 520
    goto/16 :goto_9

    .line 523
    :cond_1a
    cmpl-double v2, v4, v6

    if-nez v2, :cond_1b

    move v2, v1

    goto/16 :goto_a

    :cond_1b
    move v2, v0

    goto/16 :goto_a

    :cond_1c
    if-eqz v3, :cond_1d

    move v2, v0

    goto/16 :goto_a

    :cond_1d
    move v2, v1

    goto/16 :goto_a

    .line 527
    :cond_1e
    cmp-long v2, v4, v6

    if-nez v2, :cond_1f

    move v2, v1

    goto/16 :goto_b

    :cond_1f
    move v2, v0

    goto/16 :goto_b

    :cond_20
    if-eqz v3, :cond_21

    move v2, v0

    goto/16 :goto_b

    :cond_21
    move v2, v1

    goto/16 :goto_b

    :cond_22
    move v2, v0

    .line 535
    goto/16 :goto_c

    :cond_23
    move v2, v0

    .line 538
    goto :goto_d
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 470
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/u;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getProvider()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 475
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 476
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getTime()J

    move-result-wide v2

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 477
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 478
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 479
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAltitude()D

    move-result-wide v2

    double-to-int v1, v2

    add-int/2addr v0, v1

    .line 480
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 481
    return v0
.end method

.method public setAccuracy(F)V
    .locals 1

    .prologue
    .line 760
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setAltitude(D)V
    .locals 1

    .prologue
    .line 765
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setBearing(F)V
    .locals 1

    .prologue
    .line 770
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 775
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLatitude(D)V
    .locals 1

    .prologue
    .line 780
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setLongitude(D)V
    .locals 1

    .prologue
    .line 785
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setProvider(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 790
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setSpeed(F)V
    .locals 1

    .prologue
    .line 795
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public setTime(J)V
    .locals 1

    .prologue
    .line 800
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 743
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v1

    .line 744
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v2, "source"

    .line 745
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getProvider()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v2, "point"

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 746
    invoke-virtual {v0, v2, v3}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v2

    const-string v3, "accuracy"

    .line 747
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x11

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v2

    const-string v3, "speed"

    .line 748
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x13

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " m/s"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v2

    const-string v3, "bearing"

    .line 749
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v0

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x17

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " degrees"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-virtual {v2, v3, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v2, "time"

    new-instance v3, Ljava/util/Date;

    .line 750
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/b/a;->getTime()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v2, "relativetime"

    new-instance v3, Ljava/util/Date;

    .line 751
    iget-wide v4, p0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v1, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "level"

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/a;->i:Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 752
    :goto_3
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "routeSnappingInfo"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    .line 753
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "gpsInfo"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    .line 754
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 755
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 747
    :cond_0
    const-string v0, "n/a"

    goto/16 :goto_0

    .line 748
    :cond_1
    const-string v0, "n/a"

    goto :goto_1

    .line 749
    :cond_2
    const-string v0, "n/a"

    goto :goto_2

    .line 751
    :cond_3
    const-string v0, "n/a"

    goto :goto_3
.end method
