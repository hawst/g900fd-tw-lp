.class public Lcom/google/android/apps/gmm/car/CarConnectionService;
.super Landroid/app/Service;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/apps/gmm/car/k;

.field private c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lcom/google/android/apps/gmm/car/CarConnectionService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/CarConnectionService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/apps/gmm/car/CarConnectionService;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xa

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onBind(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 24
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 25
    sget-object v0, Lcom/google/android/apps/gmm/car/CarConnectionService;->a:Ljava/lang/String;

    .line 27
    new-instance v1, Lcom/google/android/apps/gmm/car/b;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/car/b;-><init>()V

    .line 28
    new-instance v2, Lcom/google/android/apps/gmm/car/k;

    new-instance v3, Lcom/google/android/apps/gmm/car/f;

    .line 29
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/car/f;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/e;)V

    invoke-direct {v2, p0, v1, v3}, Lcom/google/android/apps/gmm/car/k;-><init>(Landroid/app/Service;Lcom/google/android/apps/gmm/car/e;Lcom/google/android/apps/gmm/car/f;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/car/CarConnectionService;->b:Lcom/google/android/apps/gmm/car/k;

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/CarConnectionService;->b:Lcom/google/android/apps/gmm/car/k;

    sget-object v1, Lcom/google/android/apps/gmm/car/k;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/k;->e:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    .line 31
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/apps/gmm/car/CarConnectionService;->a:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/CarConnectionService;->c:Z

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/CarConnectionService;->b:Lcom/google/android/apps/gmm/car/k;

    invoke-static {}, Lcom/google/android/apps/gmm/car/k;->a()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/CarConnectionService;->b:Lcom/google/android/apps/gmm/car/k;

    .line 41
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 42
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/car/CarConnectionService;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x12

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onStartCommand(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/CarConnectionService;->c:Z

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "just_in_case"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    sget-object v0, Lcom/google/android/apps/gmm/car/CarConnectionService;->a:Ljava/lang/String;

    .line 50
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/car/CarConnectionService;->c:Z

    .line 51
    return v3
.end method
