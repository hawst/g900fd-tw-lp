.class public Lcom/google/android/apps/gmm/map/internal/vector/h;
.super Lcom/google/android/apps/gmm/shared/c/a/d;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field j:I

.field k:I

.field l:Z

.field m:Z

.field n:Z

.field private o:Z

.field private p:Z

.field private q:I

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lcom/google/android/apps/gmm/map/internal/vector/g;

.field private t:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/ref/WeakReference;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;",
            ">;",
            "Lcom/google/android/apps/gmm/map/c/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1348
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 1910
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->r:Ljava/util/ArrayList;

    .line 1911
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->n:Z

    .line 1350
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->j:I

    .line 1351
    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->k:I

    .line 1352
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->l:Z

    .line 1353
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->q:I

    .line 1354
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    .line 1355
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1392
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    if-eqz v0, :cond_1

    .line 1393
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/g;->b()V

    .line 1394
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    .line 1395
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    if-ne v1, p0, :cond_0

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1397
    :cond_1
    return-void
.end method

.method private f()V
    .locals 23

    .prologue
    .line 1399
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/vector/g;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/g;-><init>(Ljava/lang/ref/WeakReference;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    .line 1400
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    .line 1401
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    .line 1403
    const/4 v4, 0x0

    .line 1404
    const/16 v16, 0x0

    .line 1405
    const/4 v6, 0x0

    .line 1406
    const/4 v15, 0x0

    .line 1407
    const/4 v14, 0x0

    .line 1408
    const/4 v13, 0x0

    .line 1409
    const/4 v12, 0x0

    .line 1410
    const/4 v5, 0x0

    .line 1411
    const/4 v11, 0x0

    .line 1412
    const/4 v10, 0x0

    .line 1413
    const/4 v9, 0x0

    .line 1414
    const/4 v8, 0x0

    move v7, v10

    move-object/from16 v17, v4

    move v10, v12

    move v12, v14

    move v14, v6

    move v6, v9

    move v9, v5

    move-object v5, v8

    move v8, v11

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    .line 1417
    :goto_0
    :try_start_0
    sget-object v18, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v18
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 1419
    :goto_1
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->o:Z

    if-eqz v4, :cond_1

    .line 1420
    monitor-exit v18
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1706
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v5

    .line 1707
    :try_start_2
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v4, :cond_0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a()V

    .line 1708
    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->e()V

    .line 1709
    monitor-exit v5

    return-void

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 1420
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->r:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1424
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->r:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Runnable;

    move v5, v9

    move/from16 v16, v15

    move v15, v13

    move v9, v6

    move v13, v11

    move v6, v14

    move v14, v12

    move v11, v8

    move v12, v10

    move-object v8, v4

    move v10, v7

    .line 1588
    :goto_2
    monitor-exit v18
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1590
    if-eqz v8, :cond_1f

    .line 1591
    :try_start_4
    invoke-interface {v8}, Ljava/lang/Runnable;->run()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 1592
    const/4 v8, 0x0

    move v7, v10

    move v10, v12

    move v12, v14

    move v14, v6

    move v6, v9

    move v9, v5

    move-object v5, v8

    move v8, v11

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    .line 1593
    goto :goto_0

    .line 1429
    :cond_2
    const/4 v4, 0x0

    .line 1430
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->c:Z

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->b:Z

    move/from16 v19, v0

    move/from16 v0, v16

    move/from16 v1, v19

    if-eq v0, v1, :cond_35

    .line 1431
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->b:Z

    .line 1432
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->b:Z

    move/from16 v16, v0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/map/internal/vector/h;->c:Z

    .line 1433
    sget-object v16, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->notifyAll()V

    move/from16 v16, v4

    .line 1440
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->i:Z

    if-eqz v4, :cond_4

    .line 1444
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v4, :cond_3

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a()V

    .line 1445
    :cond_3
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->e()V

    .line 1446
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->i:Z

    .line 1447
    const/4 v8, 0x1

    .line 1451
    :cond_4
    if-eqz v12, :cond_6

    .line 1452
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v4, :cond_5

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a()V

    .line 1453
    :cond_5
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->e()V

    .line 1454
    const/4 v12, 0x0

    .line 1458
    :cond_6
    if-eqz v16, :cond_7

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v4, :cond_7

    .line 1462
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v4, :cond_7

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a()V

    .line 1466
    :cond_7
    if-eqz v16, :cond_9

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    if-eqz v4, :cond_9

    .line 1467
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    .line 1468
    if-nez v4, :cond_10

    const/4 v4, 0x0

    .line 1470
    :goto_4
    if-eqz v4, :cond_8

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/i;->a()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 1471
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->e()V

    .line 1479
    :cond_9
    if-eqz v16, :cond_a

    .line 1480
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/i;->b()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 1481
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/g;->b()V

    .line 1489
    :cond_a
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->d:Z

    if-nez v4, :cond_c

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->e:Z

    if-nez v4, :cond_c

    .line 1493
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v4, :cond_b

    .line 1494
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v4, :cond_b

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a()V

    .line 1496
    :cond_b
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->e:Z

    .line 1497
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->p:Z

    .line 1498
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1502
    :cond_c
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->d:Z

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->e:Z

    if-eqz v4, :cond_d

    .line 1506
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->e:Z

    .line 1507
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1510
    :cond_d
    if-eqz v9, :cond_e

    .line 1514
    const/4 v10, 0x0

    .line 1515
    const/4 v9, 0x0

    .line 1516
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->m:Z

    .line 1517
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1521
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->a()Z

    move-result v4

    if-eqz v4, :cond_1e

    .line 1524
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    if-nez v4, :cond_f

    .line 1525
    if-eqz v8, :cond_11

    .line 1526
    const/4 v8, 0x0

    .line 1541
    :cond_f
    :goto_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    if-eqz v4, :cond_34

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-nez v4, :cond_34

    .line 1542
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    .line 1543
    const/4 v14, 0x1

    .line 1544
    const/4 v13, 0x1

    .line 1545
    const/4 v11, 0x1

    move v4, v11

    move v11, v13

    .line 1548
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v13, :cond_1d

    .line 1549
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->n:Z

    if-eqz v13, :cond_33

    .line 1550
    const/4 v10, 0x1

    .line 1551
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->j:I

    .line 1552
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->k:I

    .line 1553
    const/4 v7, 0x1

    .line 1561
    const/4 v13, 0x1

    .line 1563
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->n:Z

    .line 1565
    :goto_7
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->l:Z

    .line 1566
    sget-object v14, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v14}, Ljava/lang/Object;->notifyAll()V

    move v14, v12

    move/from16 v16, v15

    move v12, v7

    move v15, v11

    move v11, v8

    move-object v8, v5

    move v5, v9

    move v9, v4

    move/from16 v22, v10

    move v10, v6

    move v6, v13

    move/from16 v13, v22

    .line 1567
    goto/16 :goto_2

    .line 1469
    :cond_10
    iget-boolean v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->i:Z

    goto/16 :goto_4

    .line 1527
    :cond_11
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    if-eq v0, v1, :cond_12

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    move-object/from16 v16, v0

    if-nez v16, :cond_15

    :cond_12
    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    const/4 v4, 0x1

    :goto_8
    if-eqz v4, :cond_f

    .line 1529
    :try_start_6
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    const-string v4, "GmmGLSurfaceView.start"

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/t;->a(Ljava/lang/String;)V

    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v4

    check-cast v4, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v16, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v4

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v16, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v0, v16

    if-ne v4, v0, :cond_18

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglGetDisplay failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 1530
    :catch_0
    move-exception v4

    .line 1531
    :try_start_7
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    move-object/from16 v0, p0

    if-ne v6, v0, :cond_13

    const/4 v6, 0x0

    iput-object v6, v5, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    :cond_13
    invoke-virtual {v5}, Ljava/lang/Object;->notifyAll()V

    .line 1532
    throw v4

    .line 1588
    :catchall_1
    move-exception v4

    monitor-exit v18
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 1706
    :catchall_2
    move-exception v4

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v5

    .line 1707
    :try_start_9
    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    if-eqz v6, :cond_14

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->g:Z

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a()V

    .line 1708
    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->e()V

    .line 1709
    monitor-exit v5
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_6

    throw v4

    .line 1527
    :cond_15
    :try_start_a
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/i;->c()V

    iget-boolean v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/i;->a:Z

    move/from16 v16, v0

    if-eqz v16, :cond_16

    const/4 v4, 0x1

    goto :goto_8

    :cond_16
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    move-object/from16 v16, v0

    if-eqz v16, :cond_17

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/i;->b:Lcom/google/android/apps/gmm/map/internal/vector/h;

    const/16 v16, 0x1

    move/from16 v0, v16

    iput-boolean v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/h;->i:Z

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_17
    const/4 v4, 0x0

    goto :goto_8

    .line 1529
    :cond_18
    const/4 v4, 0x2

    :try_start_b
    new-array v4, v4, [I

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-interface {v0, v1, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v4

    if-nez v4, :cond_19

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglInitialize failed"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_19
    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    if-nez v4, :cond_1b

    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->f:Ljavax/microedition/khronos/egl/EGLContext;

    :goto_9
    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->f:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v4, :cond_1a

    iget-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->f:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v16, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v0, v16

    if-ne v4, v0, :cond_1c

    :cond_1a
    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->f:Ljavax/microedition/khronos/egl/EGLContext;

    const-string v4, "createContext"

    iget-object v5, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v5}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v5

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v5

    :cond_1b
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->e:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    move-object/from16 v16, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v19, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v0, v1, v2}, Landroid/opengl/GLSurfaceView$EGLConfigChooser;->chooseConfig(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v16

    move-object/from16 v0, v16

    iput-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->f:Lcom/google/android/apps/gmm/map/internal/vector/e;

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v16, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v19, v0

    iget-object v0, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v20, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-interface {v4, v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/vector/e;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v4

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->f:Ljavax/microedition/khronos/egl/EGLContext;

    goto :goto_9

    :cond_1c
    const/4 v4, 0x0

    iput-object v4, v15, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    const-string v4, "GmmGLSurfaceView.start"

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/t;->b(Ljava/lang/String;)V
    :try_end_b
    .catch Ljava/lang/RuntimeException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 1534
    const/4 v4, 0x1

    :try_start_c
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->f:Z

    .line 1535
    const/4 v15, 0x1

    .line 1537
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    goto/16 :goto_5

    :cond_1d
    move v13, v11

    move v11, v4

    .line 1586
    :cond_1e
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->wait()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_1

    .line 1596
    :cond_1f
    if-eqz v6, :cond_32

    .line 1600
    :try_start_d
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v4, :cond_20

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "egl not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_20
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-nez v4, :cond_21

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "eglDisplay not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_21
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v4, :cond_22

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v5, "mEglConfig not initialized"

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_22
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a()V

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    if-eqz v4, :cond_2c

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->g:Lcom/google/android/apps/gmm/map/internal/vector/f;

    move-object/from16 v18, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v19, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v20, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v21, v0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/f;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    iput-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    :goto_a
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_23

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v18, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v0, v18

    if-ne v4, v0, :cond_2d

    :cond_23
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    const/4 v4, 0x0

    :goto_b
    if-eqz v4, :cond_2f

    .line 1601
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v6
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 1602
    const/4 v4, 0x1

    :try_start_e
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->h:Z

    .line 1603
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1604
    monitor-exit v6
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    .line 1611
    const/4 v4, 0x0

    move v7, v4

    .line 1616
    :goto_c
    if-eqz v15, :cond_31

    .line 1617
    :try_start_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/vector/g;->f:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v6}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v6

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/g;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    if-eqz v4, :cond_24

    :cond_24
    move-object v4, v6

    check-cast v4, Ljavax/microedition/khronos/opengles/GL10;

    .line 1619
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/map/internal/vector/i;->a(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1620
    const/4 v15, 0x0

    move-object v6, v4

    .line 1623
    :goto_d
    if-eqz v16, :cond_26

    .line 1627
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    .line 1628
    if-eqz v4, :cond_25

    .line 1629
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/g;->e:Ljavax/microedition/khronos/egl/EGLConfig;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v4, v6, v0}, Lcom/google/android/apps/gmm/map/internal/vector/k;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1631
    :cond_25
    const/16 v16, 0x0

    .line 1634
    :cond_26
    if-eqz v13, :cond_28

    .line 1638
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    .line 1639
    if-eqz v4, :cond_27

    .line 1640
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4, v6, v10, v9}, Lcom/google/android/apps/gmm/map/internal/vector/k;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 1642
    :cond_27
    const/4 v13, 0x0

    .line 1649
    :cond_28
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    .line 1650
    if-eqz v4, :cond_29

    .line 1651
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4, v6}, Lcom/google/android/apps/gmm/map/internal/vector/k;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1657
    :cond_29
    sget-boolean v4, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v4, :cond_2a

    .line 1658
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    .line 1659
    if-eqz v4, :cond_2a

    .line 1660
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/vector/k;->c()V

    .line 1665
    :cond_2a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->s:Lcom/google/android/apps/gmm/map/internal/vector/g;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    move-object/from16 v17, v0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v19, v0

    invoke-interface/range {v17 .. v19}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v17

    if-nez v17, :cond_30

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    .line 1666
    :goto_e
    sparse-switch v4, :sswitch_data_0

    .line 1680
    const-string v17, "GLThread"

    const-string v17, "eglSwapBuffers"

    move-object/from16 v0, v17

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a(Ljava/lang/String;I)Ljava/lang/String;

    .line 1682
    sget-object v17, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v17
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    .line 1683
    const/4 v4, 0x1

    :try_start_10
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->p:Z

    .line 1684
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1685
    monitor-exit v17
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_5

    .line 1691
    :goto_f
    :sswitch_0
    :try_start_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->t:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;

    .line 1692
    if-eqz v4, :cond_2b

    .line 1693
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/vector/k;->d()V

    .line 1697
    :cond_2b
    if-eqz v12, :cond_36

    .line 1698
    const/4 v4, 0x1

    :goto_10
    move-object v5, v8

    move-object/from16 v17, v6

    move v8, v11

    move v6, v9

    move v9, v4

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    move/from16 v22, v12

    move v12, v14

    move v14, v7

    move v7, v10

    move/from16 v10, v22

    .line 1700
    goto/16 :goto_0

    .line 1600
    :cond_2c
    const/4 v4, 0x0

    iput-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    goto/16 :goto_a

    :cond_2d
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->c:Ljavax/microedition/khronos/egl/EGLDisplay;

    move-object/from16 v18, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v19, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    move-object/from16 v20, v0

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->f:Ljavax/microedition/khronos/egl/EGLContext;

    move-object/from16 v21, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    move-object/from16 v3, v21

    invoke-interface {v4, v0, v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_2e

    const-string v4, "EGLHelper"

    const-string v4, "eglMakeCurrent"

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/vector/g;->b:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v7}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v7

    invoke-static {v4, v7}, Lcom/google/android/apps/gmm/map/internal/vector/g;->a(Ljava/lang/String;I)Ljava/lang/String;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_2

    const/4 v4, 0x0

    goto/16 :goto_b

    :cond_2e
    const/4 v4, 0x1

    goto/16 :goto_b

    .line 1604
    :catchall_3
    move-exception v4

    :try_start_12
    monitor-exit v6
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_3

    :try_start_13
    throw v4

    .line 1606
    :cond_2f
    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v7
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    .line 1607
    const/4 v4, 0x1

    :try_start_14
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->h:Z

    .line 1608
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/h;->p:Z

    .line 1609
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v4}, Ljava/lang/Object;->notifyAll()V

    .line 1610
    monitor-exit v7

    move v7, v10

    move v10, v12

    move v12, v14

    move v14, v6

    move v6, v9

    move v9, v5

    move-object v5, v8

    move v8, v11

    move v11, v13

    move v13, v15

    move/from16 v15, v16

    goto/16 :goto_0

    :catchall_4
    move-exception v4

    monitor-exit v7
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_4

    :try_start_15
    throw v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_2

    .line 1665
    :cond_30
    const/16 v4, 0x3000

    goto/16 :goto_e

    .line 1673
    :sswitch_1
    const/4 v14, 0x1

    .line 1674
    goto/16 :goto_f

    .line 1685
    :catchall_5
    move-exception v4

    :try_start_16
    monitor-exit v17
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    :try_start_17
    throw v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_2

    .line 1709
    :catchall_6
    move-exception v4

    :try_start_18
    monitor-exit v5
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_6

    throw v4

    :cond_31
    move-object/from16 v6, v17

    goto/16 :goto_d

    :cond_32
    move v7, v6

    goto/16 :goto_c

    :cond_33
    move v13, v14

    move/from16 v22, v7

    move v7, v10

    move v10, v4

    move v4, v6

    move/from16 v6, v22

    goto/16 :goto_7

    :cond_34
    move v4, v11

    move v11, v13

    goto/16 :goto_6

    :cond_35
    move/from16 v16, v4

    goto/16 :goto_3

    :cond_36
    move v4, v5

    goto/16 :goto_10

    .line 1666
    :sswitch_data_0
    .sparse-switch
        0x3000 -> :sswitch_0
        0x300e -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1724
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 1725
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "renderMode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1727
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    .line 1728
    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->q:I

    .line 1729
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1730
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1718
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->c:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->d:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->p:Z

    if-nez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->j:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->k:I

    if-lez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->l:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->q:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1734
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    .line 1735
    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->q:I

    monitor-exit v1

    return v0

    .line 1736
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1852
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    .line 1853
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->o:Z

    .line 1854
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1855
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1857
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1859
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 1862
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1868
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    monitor-enter v1

    .line 1869
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/h;->a:Z

    monitor-exit v1

    return v0

    .line 1870
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 4

    .prologue
    .line 1360
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 1362
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->getId()J

    move-result-wide v0

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "GLThread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->setName(Ljava/lang/String;)V

    .line 1368
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/vector/h;->f()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1372
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/internal/vector/i;->a(Lcom/google/android/apps/gmm/map/internal/vector/h;)V

    .line 1373
    :goto_0
    return-void

    .line 1372
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/internal/vector/i;->a(Lcom/google/android/apps/gmm/map/internal/vector/h;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLSurfaceView;->a:Lcom/google/android/apps/gmm/map/internal/vector/i;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/gmm/map/internal/vector/i;->a(Lcom/google/android/apps/gmm/map/internal/vector/h;)V

    throw v0
.end method
