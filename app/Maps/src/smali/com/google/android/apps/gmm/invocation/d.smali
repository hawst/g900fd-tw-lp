.class public Lcom/google/android/apps/gmm/invocation/d;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/invocation/a/b;
.implements Lcom/google/android/apps/gmm/invocation/c;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/r/b/a/ft;",
            "Lcom/google/android/apps/gmm/invocation/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/google/android/apps/gmm/invocation/a/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/invocation/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 50
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/r/b/a/ft;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/invocation/d;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/invocation/d;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/invocation/d;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 141
    invoke-virtual {p1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    sget-object v3, Lcom/google/r/b/a/av;->t:Lcom/google/r/b/a/av;

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 140
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/a/b;->a(Ljava/lang/String;Lcom/google/r/b/a/gb;Lcom/google/r/b/a/av;Ljava/lang/String;Z)Ljava/lang/String;

    .line 146
    invoke-static {}, Lcom/google/b/c/dn;->g()Lcom/google/b/c/dn;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/invocation/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/invocation/f;-><init>(Lcom/google/android/apps/gmm/invocation/d;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 154
    new-instance v2, Lcom/google/android/apps/gmm/invocation/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/invocation/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 155
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const-class v0, Lcom/google/r/b/a/ft;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iput-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    .line 156
    :goto_0
    iput-object p1, v2, Lcom/google/android/apps/gmm/invocation/b;->a:Ljava/lang/String;

    .line 157
    iput-object p0, v2, Lcom/google/android/apps/gmm/invocation/b;->d:Lcom/google/android/apps/gmm/invocation/c;

    .line 158
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 155
    :cond_0
    iput-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    goto :goto_0

    .line 158
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->d:Lcom/google/android/apps/gmm/invocation/c;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    new-instance v3, Lcom/google/android/apps/gmm/invocation/a;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/invocation/a;-><init>()V

    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/n;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v0, 0x1

    iget-object v1, v2, Lcom/google/android/apps/gmm/invocation/b;->a:Ljava/lang/String;

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ft;

    const/4 v5, 0x2

    iget v0, v0, Lcom/google/r/b/a/ft;->o:I

    int-to-long v6, v0

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_3

    :cond_5
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v0, :cond_11

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->c:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v1, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/search/aj;-><init>()V

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v5, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/search/aj;)V

    sget-object v0, Lcom/google/b/f/bc;->b:Lcom/google/b/f/bc;

    new-instance v5, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    iput-object v0, v1, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    if-eqz v0, :cond_6

    const/4 v1, 0x3

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v1, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_6
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->e:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->f:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->g:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_7
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/apps/gmm/directions/a/f;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/maps/g/a/hm;

    invoke-static {}, Lcom/google/r/b/a/fn;->newBuilder()Lcom/google/r/b/a/fp;

    move-result-object v7

    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v0, v7, Lcom/google/r/b/a/fp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v7, Lcom/google/r/b/a/fp;->a:I

    iget v0, v1, Lcom/google/maps/g/a/hm;->h:I

    iput v0, v7, Lcom/google/r/b/a/fp;->b:I

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    sget-object v8, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v9

    invoke-static {v1, v8, v0, v9}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/afz;

    move-result-object v0

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget-object v1, v7, Lcom/google/r/b/a/fp;->c:Lcom/google/n/ao;

    iget-object v8, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v7, Lcom/google/r/b/a/fp;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v7, Lcom/google/r/b/a/fp;->a:I

    invoke-virtual {v7}, Lcom/google/r/b/a/fp;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/fn;

    const/16 v1, 0x8

    sget-object v7, Lcom/google/r/b/a/b/n;->g:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v7}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_4

    :cond_a
    invoke-interface {v5}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/a;->a()Lcom/google/maps/g/a/hm;

    move-result-object v1

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    sget-object v5, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v6

    invoke-static {v1, v5, v0, v6}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/afz;

    move-result-object v0

    new-instance v5, Lcom/google/android/apps/gmm/directions/f/b;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/directions/f/b;-><init>()V

    iput-object v1, v5, Lcom/google/android/apps/gmm/directions/f/b;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, v5, Lcom/google/android/apps/gmm/directions/f/b;->b:Lcom/google/r/b/a/afz;

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/directions/f/b;->d:Lcom/google/maps/a/a;

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/al;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/directions/f/b;->f:Lcom/google/maps/g/a/al;

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s()Lcom/google/android/apps/gmm/car/a/g;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    iput-boolean v0, v5, Lcom/google/android/apps/gmm/directions/f/b;->i:Z

    sget-object v0, Lcom/google/b/f/bc;->a:Lcom/google/b/f/bc;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    iput-object v0, v5, Lcom/google/android/apps/gmm/directions/f/b;->h:Lcom/google/maps/g/hy;

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/directions/f/b;->e:Lcom/google/o/b/a/v;

    :cond_b
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/directions/f/b;->a()Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v1

    sget-object v0, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    sget-object v5, Lcom/google/r/b/a/acy;->n:Lcom/google/r/b/a/acy;

    iget-object v6, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    sget-object v6, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    invoke-static {v0, v5, v6}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/map/c/a;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v6, v5, v0}, Lcom/google/android/apps/gmm/directions/d/a;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/r/b/a/aha;Ljava/util/List;Ljava/util/List;)Lcom/google/r/b/a/agd;

    move-result-object v0

    const/4 v1, 0x4

    sget-object v5, Lcom/google/r/b/a/b/az;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v5}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v1, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_c
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->j:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->k:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_d
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    if-nez v0, :cond_e

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v0

    const-string v1, "ExternalInvocationRequest"

    const-string v5, "Cannot get camera. An empty camera is used instead."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_e
    iget-object v1, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static {v1, v0, v5, v6}, Lcom/google/android/apps/gmm/place/ci;->a(Lcom/google/android/apps/gmm/base/activities/o;Lcom/google/maps/a/a;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    const/4 v1, 0x5

    iget-object v5, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v1, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_f
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->b:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_10

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->b:Ljava/util/Set;

    sget-object v1, Lcom/google/r/b/a/ft;->l:Lcom/google/r/b/a/ft;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_10
    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->c:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    new-instance v5, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/n;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {v5, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v6, 0x1

    new-instance v7, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/n;->e:Lcom/google/e/a/a/a/d;

    invoke-direct {v7, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v8, 0x1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    if-nez v0, :cond_12

    const/4 v0, 0x0

    :goto_5
    iget-object v9, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v8, 0x5

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v0

    if-eqz v0, :cond_13

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_6
    iget-object v9, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v8, 0x2

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->e()Z

    move-result v0

    if-eqz v0, :cond_14

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_7
    iget-object v9, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v8, 0x3

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->f()Z

    move-result v0

    if-eqz v0, :cond_15

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_8
    iget-object v9, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v9, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v8, 0x4

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->g()Z

    move-result v0

    if-eqz v0, :cond_16

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    :goto_9
    iget-object v1, v7, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v8, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    iget-object v0, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6, v7}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v0, 0x7

    iget-object v1, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v0, v5}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    :cond_11
    iput-object v4, v3, Lcom/google/android/apps/gmm/invocation/a;->b:Lcom/google/e/a/a/a/b;

    iget-object v0, v2, Lcom/google/android/apps/gmm/invocation/b;->d:Lcom/google/android/apps/gmm/invocation/c;

    iput-object v0, v3, Lcom/google/android/apps/gmm/invocation/a;->a:Lcom/google/android/apps/gmm/invocation/c;

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 160
    return-void

    .line 158
    :cond_12
    sget-object v9, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v9}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    goto :goto_5

    :cond_13
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_6

    :cond_14
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_7

    :cond_15
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_8

    :cond_16
    sget-object v0, Lcom/google/e/a/a/a/b;->a:Ljava/lang/Boolean;

    goto :goto_9
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/invocation/d;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/fragments/GenericSpinnerFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/invocation/d;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/gmm/invocation/d;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 119
    invoke-static {p1}, Lcom/google/android/apps/gmm/l/i;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 120
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 121
    sget-object v0, Lcom/google/android/apps/gmm/invocation/d;->a:Ljava/lang/String;

    .line 136
    :goto_1
    return-void

    .line 120
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :cond_2
    invoke-static {p1}, Lcom/google/android/apps/gmm/l/i;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a()V

    .line 127
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/invocation/d;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 129
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/invocation/e;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/invocation/e;-><init>(Lcom/google/android/apps/gmm/invocation/d;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 3

    .prologue
    .line 92
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/u;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/invocation/b/u;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 95
    sget-object v1, Lcom/google/r/b/a/ft;->h:Lcom/google/r/b/a/ft;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v1, Lcom/google/r/b/a/ft;->i:Lcom/google/r/b/a/ft;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/google/r/b/a/ft;->c:Lcom/google/r/b/a/ft;

    new-instance v1, Lcom/google/android/apps/gmm/invocation/b/p;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/invocation/b/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/a;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/invocation/b/a;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 100
    sget-object v1, Lcom/google/r/b/a/ft;->e:Lcom/google/r/b/a/ft;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    sget-object v1, Lcom/google/r/b/a/ft;->f:Lcom/google/r/b/a/ft;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    sget-object v1, Lcom/google/r/b/a/ft;->g:Lcom/google/r/b/a/ft;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/l;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/invocation/b/l;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 105
    sget-object v1, Lcom/google/r/b/a/ft;->j:Lcom/google/r/b/a/ft;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    sget-object v1, Lcom/google/r/b/a/ft;->k:Lcom/google/r/b/a/ft;

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    sget-object v0, Lcom/google/r/b/a/ft;->b:Lcom/google/r/b/a/ft;

    new-instance v1, Lcom/google/android/apps/gmm/invocation/b/j;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/invocation/b/j;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 108
    sget-object v0, Lcom/google/r/b/a/ft;->l:Lcom/google/r/b/a/ft;

    new-instance v1, Lcom/google/android/apps/gmm/invocation/b/s;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/invocation/b/s;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 109
    sget-object v0, Lcom/google/r/b/a/ft;->m:Lcom/google/r/b/a/ft;

    new-instance v1, Lcom/google/android/apps/gmm/invocation/b/h;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/invocation/b/h;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/apps/gmm/invocation/a;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3

    .prologue
    .line 82
    invoke-static {p3}, Lcom/google/android/apps/gmm/f/b;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/invocation/g;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/invocation/g;-><init>(Lcom/google/android/apps/gmm/invocation/d;Lcom/google/android/apps/gmm/invocation/a;)V

    new-instance v1, Lcom/google/android/apps/gmm/invocation/h;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/invocation/h;-><init>(Lcom/google/android/apps/gmm/invocation/d;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/f/b;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 87
    :goto_0
    sget-object v0, Lcom/google/android/apps/gmm/invocation/d;->a:Ljava/lang/String;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Cannot handle external invocation. error code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 88
    return-void

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/z/a/b;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/gmm/invocation/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/invocation/i;-><init>(Lcom/google/android/apps/gmm/invocation/d;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/e/a/a/a/b;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    const/4 v5, 0x1

    .line 55
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v5

    :goto_0
    if-eqz v0, :cond_2

    .line 56
    sget-object v0, Lcom/google/android/apps/gmm/invocation/d;->a:Ljava/lang/String;

    const-string v2, "Empty string in request despite initial non empty check."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 77
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 55
    goto :goto_0

    .line 62
    :cond_2
    invoke-static {}, Lcom/google/r/b/a/fr;->d()Lcom/google/r/b/a/fr;

    move-result-object v1

    .line 60
    if-eqz p2, :cond_6

    const/16 v0, 0x19

    invoke-virtual {p2, v5, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_7

    :goto_3
    check-cast v0, Lcom/google/r/b/a/fr;

    .line 63
    iget-object v2, p0, Lcom/google/android/apps/gmm/invocation/d;->b:Ljava/util/Map;

    iget v1, v0, Lcom/google/r/b/a/fr;->b:I

    invoke-static {v1}, Lcom/google/r/b/a/ft;->a(I)Lcom/google/r/b/a/ft;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    :cond_3
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, Lcom/google/android/apps/gmm/invocation/a/a;

    .line 65
    if-nez v6, :cond_8

    .line 66
    sget-object v1, Lcom/google/android/apps/gmm/invocation/d;->a:Ljava/lang/String;

    iget v0, v0, Lcom/google/r/b/a/fr;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/ft;->a(I)Lcom/google/r/b/a/ft;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    :cond_4
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x19

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No matching executor for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/fragments/GenericSpinnerFragment;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 68
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/z/a/b;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/apps/gmm/invocation/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/invocation/i;-><init>(Lcom/google/android/apps/gmm/invocation/d;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_1

    :cond_6
    move-object v0, v4

    .line 60
    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3

    .line 70
    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/invocation/d;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xf

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Using executor "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 73
    invoke-interface {v6}, Lcom/google/android/apps/gmm/invocation/a/a;->a()Lcom/google/r/b/a/gb;

    move-result-object v2

    sget-object v3, Lcom/google/r/b/a/av;->u:Lcom/google/r/b/a/av;

    move-object v1, p1

    .line 72
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/a/b;->a(Ljava/lang/String;Lcom/google/r/b/a/gb;Lcom/google/r/b/a/av;Ljava/lang/String;Z)Ljava/lang/String;

    .line 75
    invoke-interface {v6, p2}, Lcom/google/android/apps/gmm/invocation/a/a;->a(Lcom/google/e/a/a/a/b;)V

    goto/16 :goto_1
.end method
