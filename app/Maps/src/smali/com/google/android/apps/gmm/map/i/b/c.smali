.class public final Lcom/google/android/apps/gmm/map/i/b/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/gmm/map/i/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/i/b/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/res/Resources;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 136
    sget v0, Lcom/google/android/apps/gmm/j;->K:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, p1, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 113
    sget v0, Lcom/google/android/apps/gmm/l;->nR:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 33
    if-nez p1, :cond_0

    .line 34
    sget-object v0, Lcom/google/android/apps/gmm/map/i/b/c;->a:Ljava/lang/String;

    const-string v3, "Cannot get transfer description for null route name"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 35
    const-string p1, ""

    .line 37
    :cond_0
    if-nez p3, :cond_1

    .line 38
    sget-object v0, Lcom/google/android/apps/gmm/map/i/b/c;->a:Ljava/lang/String;

    const-string v3, "Cannot get transfer description for null head sign"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    const-string p3, ""

    .line 41
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    .line 42
    sget v0, Lcom/google/android/apps/gmm/l;->nT:I

    new-array v3, v5, [Ljava/lang/Object;

    aput-object p1, v3, v1

    aput-object p3, v3, v2

    invoke-virtual {p0, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 44
    :goto_1
    return-object v0

    :cond_3
    move v0, v1

    .line 41
    goto :goto_0

    .line 44
    :cond_4
    sget v0, Lcom/google/android/apps/gmm/l;->nU:I

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v1

    aput-object p2, v3, v2

    aput-object p3, v3, v5

    invoke-virtual {p0, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 63
    sget v0, Lcom/google/android/apps/gmm/l;->nQ:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 64
    invoke-static {p0, p1, p2, p4}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    .line 63
    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/maps/g/a/bm;)Ljava/lang/String;
    .locals 4
    .param p0    # Lcom/google/maps/g/a/bm;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 123
    if-nez p0, :cond_0

    .line 124
    const-string v0, ""

    .line 132
    :goto_0
    return-object v0

    .line 126
    :cond_0
    iget v2, p0, Lcom/google/maps/g/a/bm;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    move v2, v0

    :goto_1
    if-eqz v2, :cond_2

    .line 127
    invoke-virtual {p0}, Lcom/google/maps/g/a/bm;->g()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 126
    goto :goto_1

    .line 129
    :cond_2
    iget v2, p0, Lcom/google/maps/g/a/bm;->a:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v0, :cond_3

    :goto_2
    if-eqz v0, :cond_4

    .line 130
    invoke-virtual {p0}, Lcom/google/maps/g/a/bm;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 129
    goto :goto_2

    .line 132
    :cond_4
    const-string v0, ""

    goto :goto_0
.end method

.method public static b(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 82
    sget v0, Lcom/google/android/apps/gmm/l;->nS:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 83
    invoke-static {p0, p1, p2, p4}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    .line 82
    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 101
    sget v0, Lcom/google/android/apps/gmm/l;->nP:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    .line 102
    invoke-static {p0, p1, p2, p4}, Lcom/google/android/apps/gmm/map/i/b/c;->a(Landroid/content/res/Resources;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p3, v1, v2

    .line 101
    invoke-virtual {p0, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
