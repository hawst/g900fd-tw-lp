.class public Lcom/google/android/apps/gmm/map/o/bc;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;

.field static final b:I


# instance fields
.field final c:[Lcom/google/android/apps/gmm/map/o/bd;

.field final d:Lcom/google/android/apps/gmm/v/ad;

.field private final e:[F

.field private final f:Lcom/google/android/apps/gmm/v/x;

.field private final g:Lcom/google/android/apps/gmm/v/m;

.field private final h:Lcom/google/android/apps/gmm/v/bg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/map/o/bc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/o/bc;->a:Ljava/lang/String;

    .line 41
    invoke-static {}, Lcom/google/android/apps/gmm/map/t/l;->values()[Lcom/google/android/apps/gmm/map/t/l;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/google/android/apps/gmm/map/o/bc;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ad;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/16 v1, 0x14

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    .line 62
    new-instance v1, Lcom/google/android/apps/gmm/v/x;

    const/high16 v2, -0x7f010000

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/v/x;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->f:Lcom/google/android/apps/gmm/v/x;

    .line 71
    new-instance v1, Lcom/google/android/apps/gmm/v/bg;

    new-instance v2, Lcom/google/android/apps/gmm/v/ar;

    invoke-direct {v2, v0, v0, v0}, Lcom/google/android/apps/gmm/v/ar;-><init>(IIZ)V

    const/4 v3, 0x7

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/v/bg;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->h:Lcom/google/android/apps/gmm/v/bg;

    .line 80
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/bc;->d:Lcom/google/android/apps/gmm/v/ad;

    .line 81
    sget v1, Lcom/google/android/apps/gmm/map/o/bc;->b:I

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/o/bd;

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->c:[Lcom/google/android/apps/gmm/map/o/bd;

    .line 82
    :goto_0
    sget v1, Lcom/google/android/apps/gmm/map/o/bc;->b:I

    if-ge v0, v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->c:[Lcom/google/android/apps/gmm/map/o/bd;

    new-instance v2, Lcom/google/android/apps/gmm/map/o/bd;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/o/bd;-><init>()V

    aput-object v2, v1, v0

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/4 v1, 0x1

    const/16 v2, 0x303

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bc;->g:Lcom/google/android/apps/gmm/v/m;

    .line 87
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/v/bg;Lcom/google/android/apps/gmm/map/t/l;)Lcom/google/android/apps/gmm/map/o/be;
    .locals 7

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bc;->c:[Lcom/google/android/apps/gmm/map/o/bd;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/t/l;->ordinal()I

    move-result v1

    aget-object v1, v0, v1

    .line 95
    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/o/bd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/be;

    .line 96
    if-nez v0, :cond_0

    .line 97
    new-instance v2, Lcom/google/android/apps/gmm/map/o/bg;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/o/bg;-><init>()V

    new-instance v3, Lcom/google/android/apps/gmm/map/t/p;

    invoke-direct {v3, p2}, Lcom/google/android/apps/gmm/map/t/p;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bc;->h:Lcom/google/android/apps/gmm/v/bg;

    if-eq p1, v0, :cond_1

    invoke-virtual {v3, p1}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/t/am;

    const-class v4, Lcom/google/android/apps/gmm/map/o/bh;

    iget v5, p1, Lcom/google/android/apps/gmm/v/ci;->t:I

    const v6, 0x84c0

    sub-int/2addr v5, v6

    const/4 v6, -0x1

    invoke-direct {v0, v4, v5, v6}, Lcom/google/android/apps/gmm/map/t/am;-><init>(Ljava/lang/Class;II)V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bc;->g:Lcom/google/android/apps/gmm/v/m;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bc;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    const/4 v5, 0x1

    invoke-direct {v4, v3, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/o/be;

    invoke-direct {v0, v2, p1, v3}, Lcom/google/android/apps/gmm/map/o/be;-><init>(Lcom/google/android/apps/gmm/map/o/bg;Lcom/google/android/apps/gmm/v/bg;Lcom/google/android/apps/gmm/map/t/p;)V

    .line 98
    invoke-virtual {v1, p1, v0}, Lcom/google/android/apps/gmm/map/o/bd;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_0
    return-object v0

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bc;->f:Lcom/google/android/apps/gmm/v/x;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/ai;)V

    goto :goto_0
.end method

.method public a(FFFFFFFFFFFFFLcom/google/android/apps/gmm/map/o/be;)V
    .locals 12

    .prologue
    .line 332
    sub-float v1, p1, p3

    add-float v1, v1, p5

    .line 333
    sub-float v2, p2, p4

    add-float v2, v2, p6

    .line 336
    sub-float v3, p1, p3

    sub-float v3, v3, p5

    .line 337
    sub-float v4, p2, p4

    sub-float v4, v4, p6

    .line 340
    add-float v5, p1, p3

    sub-float v5, v5, p5

    .line 341
    add-float v6, p2, p4

    sub-float v6, v6, p6

    .line 344
    add-float v7, p1, p3

    add-float v7, v7, p5

    .line 345
    add-float v8, p2, p4

    add-float v8, v8, p6

    .line 351
    const/4 v9, 0x0

    cmpg-float v9, v1, v9

    if-gez v9, :cond_1

    const/4 v9, 0x0

    cmpg-float v9, v3, v9

    if-gez v9, :cond_1

    const/4 v9, 0x0

    cmpg-float v9, v5, v9

    if-gez v9, :cond_1

    const/4 v9, 0x0

    cmpg-float v9, v7, v9

    if-gez v9, :cond_1

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    const/4 v9, 0x0

    cmpg-float v9, v2, v9

    if-gez v9, :cond_2

    const/4 v9, 0x0

    cmpg-float v9, v4, v9

    if-gez v9, :cond_2

    const/4 v9, 0x0

    cmpg-float v9, v6, v9

    if-gez v9, :cond_2

    const/4 v9, 0x0

    cmpg-float v9, v8, v9

    if-ltz v9, :cond_0

    .line 359
    :cond_2
    cmpl-float v9, v1, p12

    if-lez v9, :cond_3

    cmpl-float v9, v3, p12

    if-lez v9, :cond_3

    cmpl-float v9, v5, p12

    if-lez v9, :cond_3

    cmpl-float v9, v7, p12

    if-gtz v9, :cond_0

    .line 363
    :cond_3
    cmpl-float v9, v2, p13

    if-lez v9, :cond_4

    cmpl-float v9, v4, p13

    if-lez v9, :cond_4

    cmpl-float v9, v6, p13

    if-lez v9, :cond_4

    cmpl-float v9, v8, p13

    if-gtz v9, :cond_0

    .line 372
    :cond_4
    move-object/from16 v0, p14

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/o/be;->a:Lcom/google/android/apps/gmm/map/o/bg;

    .line 373
    iget v10, v9, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    add-int/lit8 v10, v10, 0x4

    invoke-virtual {v9, v10}, Lcom/google/android/apps/gmm/map/o/bg;->a(I)V

    .line 375
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v11, 0x0

    aput v1, v10, v11

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v10, 0x1

    aput v2, v1, v10

    .line 379
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v2, 0x2

    aput p11, v1, v2

    .line 380
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v2, 0x3

    aput p7, v1, v2

    .line 381
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v2, 0x4

    aput p10, v1, v2

    .line 384
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v2, 0x5

    aput v3, v1, v2

    .line 385
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v2, 0x6

    aput v4, v1, v2

    .line 386
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v2, 0x7

    aput p11, v1, v2

    .line 387
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0x8

    aput p7, v1, v2

    .line 388
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0x9

    aput p8, v1, v2

    .line 391
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0xa

    aput v5, v1, v2

    .line 392
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0xb

    aput v6, v1, v2

    .line 393
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0xc

    aput p11, v1, v2

    .line 394
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0xd

    aput p9, v1, v2

    .line 395
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0xe

    aput p8, v1, v2

    .line 398
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0xf

    aput v7, v1, v2

    .line 399
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0x10

    aput v8, v1, v2

    .line 400
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0x11

    aput p11, v1, v2

    .line 401
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0x12

    aput p9, v1, v2

    .line 402
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v2, 0x13

    aput p10, v1, v2

    .line 404
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/o/bg;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    iget v1, v9, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    add-int/lit8 v1, v1, 0x4

    iput v1, v9, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    iget v1, v9, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    add-int/lit8 v1, v1, 0x6

    iput v1, v9, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/b/b;Lcom/google/android/apps/gmm/map/t/l;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    .line 128
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->h:Lcom/google/android/apps/gmm/v/bg;

    invoke-virtual {p0, v1, p2}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/bg;Lcom/google/android/apps/gmm/map/t/l;)Lcom/google/android/apps/gmm/map/o/be;

    move-result-object v1

    .line 129
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/o/be;->a:Lcom/google/android/apps/gmm/map/o/bg;

    .line 130
    iget v1, v2, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    add-int/lit8 v1, v1, 0x4

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/o/bg;->a(I)V

    move v1, v0

    .line 134
    :goto_0
    const/4 v3, 0x4

    if-ge v0, v3, :cond_0

    .line 135
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/o/b/b;->a(I)Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v3

    .line 136
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    add-int/lit8 v5, v1, 0x1

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    aput v6, v4, v1

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    add-int/lit8 v4, v5, 0x1

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    aput v3, v1, v5

    .line 138
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    add-int/lit8 v3, v4, 0x1

    aput v7, v1, v4

    .line 139
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    add-int/lit8 v4, v3, 0x1

    aput v7, v1, v3

    .line 140
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    add-int/lit8 v1, v4, 0x1

    aput v7, v3, v4

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/o/bg;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    iget v0, v2, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    add-int/lit8 v0, v0, 0x4

    iput v0, v2, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    iget v0, v2, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    add-int/lit8 v0, v0, 0x6

    iput v0, v2, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    .line 144
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/by;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 12

    .prologue
    .line 158
    if-nez p1, :cond_1

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 164
    :cond_1
    const/4 v1, 0x0

    cmpg-float v1, p4, v1

    if-lez v1, :cond_0

    .line 172
    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    int-to-float v1, v1

    .line 173
    iget v2, p1, Lcom/google/android/apps/gmm/v/by;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/v/by;->a:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float/2addr v2, v3

    mul-float v2, v2, p4

    sub-float v2, p2, v2

    .line 174
    iget v3, p1, Lcom/google/android/apps/gmm/v/by;->c:I

    iget v4, p1, Lcom/google/android/apps/gmm/v/by;->a:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float v3, v3, p4

    add-float/2addr v3, v2

    .line 175
    const/4 v4, 0x0

    cmpg-float v4, v2, v4

    if-gez v4, :cond_2

    const/4 v4, 0x0

    cmpg-float v4, v3, v4

    if-ltz v4, :cond_0

    :cond_2
    cmpl-float v4, v2, v1

    if-lez v4, :cond_3

    cmpl-float v1, v3, v1

    if-gtz v1, :cond_0

    .line 181
    :cond_3
    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v1

    int-to-float v1, v1

    .line 182
    iget v4, p1, Lcom/google/android/apps/gmm/v/by;->d:I

    iget v5, p1, Lcom/google/android/apps/gmm/v/by;->b:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    mul-float v4, v4, p4

    sub-float v4, p3, v4

    .line 183
    iget v5, p1, Lcom/google/android/apps/gmm/v/by;->d:I

    iget v6, p1, Lcom/google/android/apps/gmm/v/by;->b:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    mul-float v5, v5, p4

    add-float/2addr v5, v4

    .line 184
    const/4 v6, 0x0

    cmpg-float v6, v4, v6

    if-gez v6, :cond_4

    const/4 v6, 0x0

    cmpg-float v6, v5, v6

    if-ltz v6, :cond_0

    :cond_4
    cmpl-float v6, v4, v1

    if-lez v6, :cond_5

    cmpl-float v1, v5, v1

    if-gtz v1, :cond_0

    .line 195
    :cond_5
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->e:Lcom/google/android/apps/gmm/v/bg;

    move-object/from16 v0, p6

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/bg;Lcom/google/android/apps/gmm/map/t/l;)Lcom/google/android/apps/gmm/map/o/be;

    move-result-object v1

    .line 196
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/o/be;->a:Lcom/google/android/apps/gmm/map/o/bg;

    .line 197
    iget v6, v1, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    add-int/lit8 v6, v6, 0x4

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/o/bg;->a(I)V

    .line 198
    iget v6, p1, Lcom/google/android/apps/gmm/v/by;->f:F

    .line 199
    iget v7, p1, Lcom/google/android/apps/gmm/v/by;->h:F

    .line 200
    iget v8, p1, Lcom/google/android/apps/gmm/v/by;->e:F

    .line 201
    iget v9, p1, Lcom/google/android/apps/gmm/v/by;->g:F

    .line 203
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v11, 0x0

    aput v2, v10, v11

    .line 205
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v11, 0x1

    aput v5, v10, v11

    .line 206
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v11, 0x2

    aput p5, v10, v11

    .line 207
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v11, 0x3

    aput v8, v10, v11

    .line 208
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v11, 0x4

    aput v7, v10, v11

    .line 211
    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v11, 0x5

    aput v2, v10, v11

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v10, 0x6

    aput v4, v2, v10

    .line 213
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/4 v10, 0x7

    aput p5, v2, v10

    .line 214
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v10, 0x8

    aput v8, v2, v10

    .line 215
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v8, 0x9

    aput v6, v2, v8

    .line 218
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v8, 0xa

    aput v3, v2, v8

    .line 219
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v8, 0xb

    aput v4, v2, v8

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v4, 0xc

    aput p5, v2, v4

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v4, 0xd

    aput v9, v2, v4

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v4, 0xe

    aput v6, v2, v4

    .line 225
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v4, 0xf

    aput v3, v2, v4

    .line 226
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v3, 0x10

    aput v5, v2, v3

    .line 227
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v3, 0x11

    aput p5, v2, v3

    .line 228
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v3, 0x12

    aput v9, v2, v3

    .line 229
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    const/16 v3, 0x13

    aput v7, v2, v3

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bc;->e:[F

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/o/bg;->b:Ljava/nio/FloatBuffer;

    invoke-virtual {v3, v2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    iget v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    add-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    iget v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    add-int/lit8 v2, v2, 0x6

    iput v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    goto/16 :goto_0
.end method
