.class public Lcom/google/android/apps/gmm/map/f/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public volatile a:Z

.field public b:Lcom/google/android/apps/gmm/map/f/g;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .param p1    # Landroid/content/res/Resources;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/f;->a:Z

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/f;->c:Landroid/content/res/Resources;

    .line 69
    return-void
.end method

.method public static a(F)F
    .locals 8

    .prologue
    const-wide v6, 0x4076800000000000L    # 360.0

    .line 162
    invoke-static {p0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    :cond_0
    const/4 v0, 0x0

    .line 165
    :goto_0
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-ltz v1, :cond_1

    .line 166
    float-to-double v0, v0

    sub-double/2addr v0, v6

    double-to-float v0, v0

    goto :goto_0

    .line 168
    :cond_1
    :goto_1
    float-to-double v2, v0

    const-wide/16 v4, 0x0

    cmpg-double v1, v2, v4

    if-gez v1, :cond_2

    .line 169
    float-to-double v0, v0

    add-double/2addr v0, v6

    double-to-float v0, v0

    goto :goto_1

    .line 171
    :cond_2
    return v0

    :cond_3
    move v0, p0

    goto :goto_0
.end method


# virtual methods
.method public final a(FLcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 3

    .prologue
    const/high16 v1, 0x41a80000    # 21.0f

    const/high16 v0, 0x40000000    # 2.0f

    .line 137
    .line 139
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/f;->b:Lcom/google/android/apps/gmm/map/f/g;

    if-eqz v2, :cond_0

    .line 140
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/f;->b:Lcom/google/android/apps/gmm/map/f/g;

    invoke-interface {v2, p2}, Lcom/google/android/apps/gmm/map/f/g;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 141
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/f;->b:Lcom/google/android/apps/gmm/map/f/g;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/f/g;->a()F

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 143
    :cond_0
    invoke-static {p1, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 144
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 147
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 94
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    .line 95
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/map/f/f;->a(Lcom/google/android/apps/gmm/map/f/a/c;)V

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/c;)V
    .locals 10

    .prologue
    const/high16 v1, 0x41f00000    # 30.0f

    const/high16 v9, 0x41600000    # 14.0f

    const/high16 v8, 0x41200000    # 10.0f

    const/4 v2, 0x0

    .line 104
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 107
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/gmm/map/f/f;->a(FLcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    .line 108
    iput v4, p1, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/f;->c:Landroid/content/res/Resources;

    if-eqz v0, :cond_4

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/f;->c:Landroid/content/res/Resources;

    .line 113
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/f/f;->c:Landroid/content/res/Resources;

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    iget v5, v5, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v0, v5

    .line 114
    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(I)I

    move-result v5

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v0, v5

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v5

    mul-float/2addr v0, v5

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v0, v6

    const/high16 v5, 0x20000000

    sub-int v0, v5, v0

    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-le v5, v0, :cond_2

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    :goto_0
    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 118
    :goto_1
    iput-object v3, p1, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 121
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/f;->a:Z

    if-eqz v0, :cond_5

    const/high16 v0, 0x42b20000    # 89.0f

    .line 122
    :goto_2
    iget v1, p1, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 123
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_1

    move v0, v2

    .line 126
    :cond_1
    iput v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    .line 129
    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/f;->a(F)F

    move-result v0

    iput v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    .line 132
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/e;->a(Lcom/google/android/apps/gmm/map/f/a/e;)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v0

    .line 133
    iput-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 134
    return-void

    .line 114
    :cond_2
    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    neg-int v6, v0

    if-ge v5, v6, :cond_3

    neg-int v0, v0

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    goto :goto_0

    :cond_3
    iget v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    goto :goto_0

    .line 116
    :cond_4
    invoke-virtual {v3, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->h(Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_1

    .line 121
    :cond_5
    const/high16 v0, 0x41800000    # 16.0f

    cmpl-float v0, v4, v0

    if-ltz v0, :cond_6

    const/high16 v0, 0x42820000    # 65.0f

    goto :goto_2

    :cond_6
    cmpl-float v0, v4, v9

    if-lez v0, :cond_7

    const/high16 v0, 0x42340000    # 45.0f

    sub-float v1, v4, v9

    const/high16 v3, 0x41a00000    # 20.0f

    mul-float/2addr v1, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    goto :goto_2

    :cond_7
    cmpl-float v0, v4, v8

    if-lez v0, :cond_8

    sub-float v0, v4, v8

    const/high16 v3, 0x41700000    # 15.0f

    mul-float/2addr v0, v3

    const/high16 v3, 0x40800000    # 4.0f

    div-float/2addr v0, v3

    add-float/2addr v0, v1

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_2
.end method
