.class Lcom/google/android/apps/gmm/p/e/f;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field a:D

.field b:D

.field c:D

.field d:Lcom/google/android/apps/gmm/map/b/a/y;

.field e:D

.field f:Z

.field g:D

.field h:D

.field i:Z

.field j:D

.field k:D

.field final l:Lcom/google/android/apps/gmm/shared/net/a/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/p/e/f;Lcom/google/android/apps/gmm/shared/net/a/l;)V
    .locals 18

    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 86
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/p/e/f;->l:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 87
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->a:D

    .line 88
    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->b:D

    .line 91
    if-nez p2, :cond_3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    :goto_0
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->c:D

    .line 95
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasAccuracy()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    .line 99
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/apps/gmm/p/e/f;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 100
    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v4, v4, Lcom/google/r/b/a/ou;->i:I

    int-to-double v4, v4

    add-double/2addr v4, v2

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->e:D

    .line 103
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasBearing()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 104
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getBearing()F

    move-result v4

    float-to-double v4, v4

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->g:D

    .line 105
    if-eqz p2, :cond_5

    move-object/from16 v0, p2

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->g:D

    .line 108
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v7

    float-to-double v8, v7

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/google/android/apps/gmm/p/e/f;->g:D

    double-to-float v5, v10

    .line 109
    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x43340000    # 180.0f

    cmpl-float v5, v4, v5

    if-lez v5, :cond_0

    const/high16 v5, 0x43b40000    # 360.0f

    sub-float v4, v5, v4

    :cond_0
    float-to-double v4, v4

    .line 110
    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v7, v7, Lcom/google/r/b/a/ou;->j:I

    int-to-double v10, v7

    .line 108
    const-wide v12, 0x4046800000000000L    # 45.0

    const-wide/high16 v14, 0x3ff0000000000000L    # 1.0

    neg-double v2, v2

    const-wide/high16 v16, 0x403e000000000000L    # 30.0

    div-double v2, v2, v16

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    sub-double v2, v14, v2

    mul-double/2addr v12, v2

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    div-double/2addr v4, v2

    const-wide/16 v2, 0x0

    if-eqz v6, :cond_1

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    neg-double v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    mul-double/2addr v2, v6

    :cond_1
    add-double/2addr v4, v12

    add-double/2addr v2, v4

    add-double/2addr v2, v10

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    .line 112
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_6

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    :goto_3
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    .line 114
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    .line 136
    :goto_4
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 137
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getSpeed()F

    move-result v2

    float-to-double v2, v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->c:D

    mul-double/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    .line 138
    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    const-wide/high16 v4, 0x4049000000000000L    # 50.0

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    .line 139
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v4

    .line 138
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->k:D

    .line 140
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->i:Z

    .line 154
    :goto_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    if-eqz v2, :cond_2

    .line 155
    :cond_2
    return-void

    .line 91
    :cond_3
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->b:D

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->b:D

    sub-double/2addr v2, v4

    goto/16 :goto_0

    .line 96
    :cond_4
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v2, v2, Lcom/google/r/b/a/ou;->u:I

    int-to-float v2, v2

    const v3, 0x3f2aacda    # 0.6667f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    goto/16 :goto_1

    .line 105
    :cond_5
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->g:D

    goto/16 :goto_2

    .line 112
    :cond_6
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    goto :goto_3

    .line 115
    :cond_7
    if-eqz p2, :cond_9

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    if-eqz v2, :cond_9

    .line 120
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->g:D

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->g:D

    .line 121
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/f;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/f;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v2, v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->a:D

    div-double/2addr v2, v4

    .line 122
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v2, v6

    add-double/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    .line 124
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    cmpg-double v2, v2, v4

    if-gez v2, :cond_8

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    :goto_6
    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    .line 126
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    goto/16 :goto_4

    .line 124
    :cond_8
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    goto :goto_6

    .line 128
    :cond_9
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->f:Z

    goto/16 :goto_4

    .line 141
    :cond_a
    if-eqz p2, :cond_b

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->i:Z

    if-eqz v2, :cond_b

    .line 145
    move-object/from16 v0, p2

    iget-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    .line 146
    const-wide/high16 v2, 0x4049000000000000L    # 50.0

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/apps/gmm/p/e/f;->k:D

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v6, v8

    add-double/2addr v4, v6

    .line 147
    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/p/e/f;->k:D

    .line 148
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->i:Z

    goto/16 :goto_5

    .line 150
    :cond_b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/f;->i:Z

    goto/16 :goto_5
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/r/a/ad;Lcom/google/android/apps/gmm/map/b/a/y;D)D
    .locals 7

    .prologue
    .line 172
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/f;->a:D

    iget-object v4, p0, Lcom/google/android/apps/gmm/p/e/f;->l:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 173
    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v4, v4, Lcom/google/r/b/a/ou;->i:I

    int-to-double v4, v4

    mul-double/2addr v2, v4

    div-double v2, v0, v2

    .line 175
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->c:D

    double-to-float v0, v0

    double-to-float v1, p3

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/high16 v1, 0x43b40000    # 360.0f

    sub-float v0, v1, v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/f;->l:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 176
    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->j:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    .line 177
    const-wide/high16 v4, -0x4020000000000000L    # -0.5

    mul-double/2addr v2, v2

    mul-double/2addr v0, v0

    add-double/2addr v0, v2

    mul-double/2addr v0, v4

    return-wide v0
.end method

.method final a(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/b/a/y;D)Lcom/google/android/apps/gmm/map/r/a/ad;
    .locals 11

    .prologue
    const/4 v0, 0x0

    .line 288
    const/4 v4, 0x0

    .line 289
    if-eqz p1, :cond_0

    .line 290
    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/f;->a:D

    iget-wide v6, p0, Lcom/google/android/apps/gmm/p/e/f;->k:D

    const-wide/high16 v8, 0x4008000000000000L    # 3.0

    mul-double/2addr v6, v8

    iget-object v1, p0, Lcom/google/android/apps/gmm/p/e/f;->l:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 291
    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v1, v1, Lcom/google/r/b/a/ou;->i:I

    int-to-double v8, v1

    add-double/2addr v6, v8

    mul-double/2addr v2, v6

    .line 294
    invoke-virtual {p1, p2, v2, v3, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/b/a/y;DZ)[Lcom/google/android/apps/gmm/map/r/a/ad;

    move-result-object v6

    .line 298
    const-wide/high16 v2, -0x10000000000000L    # Double.NEGATIVE_INFINITY

    move v5, v0

    .line 299
    :goto_0
    array-length v0, v6

    if-ge v5, v0, :cond_0

    .line 300
    aget-object v0, v6, v5

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/p/e/f;->a(Lcom/google/android/apps/gmm/map/r/a/ad;Lcom/google/android/apps/gmm/map/b/a/y;D)D

    move-result-wide v0

    .line 301
    cmpl-double v7, v0, v2

    if-lez v7, :cond_1

    .line 303
    aget-object v2, v6, v5

    .line 299
    :goto_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v4, v2

    move-wide v2, v0

    goto :goto_0

    .line 307
    :cond_0
    return-object v4

    :cond_1
    move-wide v0, v2

    move-object v2, v4

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 316
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "timeSinceLastUpdateSec"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/f;->c:D

    .line 317
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "positionStdMeters"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/f;->e:D

    .line 318
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "positionStdMeters"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/f;->e:D

    .line 319
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "distanceMeanMeters"

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/f;->i:Z

    if-eqz v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/apps/gmm/p/e/f;->j:D

    .line 320
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "distanceStdMeters"

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/e/f;->i:Z

    if-eqz v0, :cond_1

    iget-wide v4, p0, Lcom/google/android/apps/gmm/p/e/f;->k:D

    .line 321
    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "bearingMeanDeg"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/f;->g:D

    .line 322
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "bearingStdDeg"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/e/f;->h:D

    .line 323
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 324
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 320
    :cond_0
    const-string v0, "--"

    goto :goto_0

    .line 321
    :cond_1
    const-string v0, "--"

    goto :goto_1
.end method
