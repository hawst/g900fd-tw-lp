.class Lcom/google/android/apps/gmm/z/c/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/z/c/k;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/z/c/k;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 238
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/c/m;->b:Ljava/lang/String;

    .line 240
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/k;->a()Lcom/google/android/apps/gmm/z/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 255
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 257
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/z/c/k;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/z/c/k;->e:J

    sub-long/2addr v2, v4

    .line 258
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/m;->b:Ljava/lang/String;

    const-wide/16 v4, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/c/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;JJ)Landroid/database/Cursor;

    move-result-object v1

    .line 260
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    .line 261
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 266
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/m;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/z/c/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)I

    move-result v1

    .line 268
    if-lez v1, :cond_0

    .line 269
    sget-object v2, Lcom/google/android/apps/gmm/z/c/k;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x22

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Deleted "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " expired events"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/z/c/k;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/apps/gmm/z/b/c;

    iget-object v7, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    .line 271
    iget-object v7, v7, Lcom/google/android/apps/gmm/z/c/k;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v6, v1, v7}, Lcom/google/android/apps/gmm/z/b/c;-><init>(ILcom/google/android/apps/gmm/shared/c/f;)V

    aput-object v6, v3, v5

    .line 270
    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 273
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/k;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/d;->d()V

    .line 279
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    monitor-enter v1

    .line 280
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/z/c/k;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/m;->b:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "initForAccount for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/m;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/z/c/k;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/z/c/k;->f:Z

    .line 286
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 275
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/c/m;->a:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/c/k;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/d;->d()V

    throw v1

    .line 286
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
