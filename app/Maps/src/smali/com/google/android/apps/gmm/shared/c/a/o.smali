.class public Lcom/google/android/apps/gmm/shared/c/a/o;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field volatile a:[Lcom/google/android/apps/gmm/shared/c/a/i;

.field private b:[Lcom/google/android/apps/gmm/shared/c/a/i;

.field private c:[Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>()V
    .locals 4

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->a:[Lcom/google/android/apps/gmm/shared/c/a/i;

    .line 51
    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/a/p;->values()[Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    array-length v1, v0

    .line 52
    new-array v0, v1, [Lcom/google/android/apps/gmm/shared/c/a/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    .line 53
    new-array v0, v1, [Ljava/util/Set;

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->c:[Ljava/util/Set;

    .line 54
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 55
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->c:[Ljava/util/Set;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    aput-object v3, v2, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/reflect/Method;)Lcom/google/android/apps/gmm/shared/c/a/p;
    .locals 2

    .prologue
    .line 218
    const-class v0, Lcom/google/android/apps/gmm/shared/c/a/q;

    invoke-virtual {p0, v0}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/c/a/q;

    .line 219
    if-nez v0, :cond_2

    .line 220
    invoke-virtual {p0}, Ljava/lang/reflect/Method;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lcom/google/android/apps/gmm/shared/c/a/q;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/c/a/q;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 221
    :goto_0
    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    .line 223
    :cond_0
    :goto_1
    return-object v0

    .line 220
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/a/q;->a()Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    goto :goto_0

    .line 223
    :cond_2
    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/a/q;->a()Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 4

    .prologue
    .line 121
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->a:[Lcom/google/android/apps/gmm/shared/c/a/i;

    .line 122
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/c/a/p;->ordinal()I

    move-result v0

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    aget-object v1, v1, v0

    .line 124
    if-nez v1, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Tried to unregister thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", which isn\'t registered."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 128
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->c:[Ljava/util/Set;

    aget-object v1, v1, v0

    .line 129
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Attempt to unregister the executor for "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 131
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", which still has the following objects active: "

    .line 132
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 133
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". These are likely EventBus listeners you forgot to unregister."

    .line 134
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    const/4 v2, 0x0

    aput-object v2, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/shared/c/a/i;)V
    .locals 6

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->CURRENT:Lcom/google/android/apps/gmm/shared/c/a/p;

    if-ne p1, v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot register an executor for CURRENT."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 102
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->a:[Lcom/google/android/apps/gmm/shared/c/a/i;

    .line 103
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/c/a/p;->ordinal()I

    move-result v0

    .line 104
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 105
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x31

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Thread "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " already has a ThreadExecutor registered: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 108
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    aput-object p2, v1, v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    monitor-exit p0

    return-void
.end method

.method protected final declared-synchronized a(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 156
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->a:[Lcom/google/android/apps/gmm/shared/c/a/i;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/shared/c/a/o;->a()[Lcom/google/android/apps/gmm/shared/c/a/i;

    move-result-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/c/a/p;->ordinal()I

    move-result v1

    aget-object v0, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 157
    if-nez v0, :cond_1

    .line 158
    const/4 v0, 0x0

    .line 161
    :goto_0
    monitor-exit p0

    return v0

    .line 160
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->c:[Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/c/a/p;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    const/4 v0, 0x1

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a()[Lcom/google/android/apps/gmm/shared/c/a/i;
    .locals 2

    .prologue
    .line 141
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    .line 142
    iput-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->a:[Lcom/google/android/apps/gmm/shared/c/a/i;

    .line 143
    array-length v0, v1

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/shared/c/a/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    monitor-exit p0

    return-object v1

    .line 141
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final declared-synchronized b(Lcom/google/android/apps/gmm/shared/c/a/p;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 171
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->c:[Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/c/a/p;->ordinal()I

    move-result v1

    aget-object v0, v0, v1

    .line 172
    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Thread "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " isn\'t acquired by object "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 175
    :cond_0
    :try_start_1
    invoke-interface {v0, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 176
    monitor-exit p0

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 180
    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/a/p;->values()[Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v0

    array-length v1, v0

    .line 181
    const-class v0, Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    .line 182
    monitor-enter p0

    .line 183
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 184
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/shared/c/a/o;->b:[Lcom/google/android/apps/gmm/shared/c/a/i;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    .line 185
    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/a/p;->values()[Lcom/google/android/apps/gmm/shared/c/a/p;

    move-result-object v3

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 183
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 188
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    mul-int/lit8 v1, v1, 0x28

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 190
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "[Registered:"

    .line 191
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 192
    invoke-virtual {v2}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " Unregistered:"

    .line 193
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 194
    invoke-static {v2}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/EnumSet;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    .line 195
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 196
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 188
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
