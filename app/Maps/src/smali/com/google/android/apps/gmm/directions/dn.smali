.class Lcom/google/android/apps/gmm/directions/dn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final d:Landroid/view/animation/Interpolator;


# instance fields
.field final a:Lcom/google/android/apps/gmm/directions/al;

.field b:Lcom/google/android/apps/gmm/directions/av;

.field c:Lcom/google/android/apps/gmm/directions/dq;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/dn;->d:Landroid/view/animation/Interpolator;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/directions/al;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    .line 84
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 351
    new-array v3, v0, [I

    .line 352
    invoke-virtual {p1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 353
    new-array v4, v0, [I

    .line 354
    invoke-virtual {p0, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 355
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    const/4 v5, 0x0

    aget v3, v3, v1

    aget v4, v4, v1

    sub-int/2addr v3, v4

    int-to-float v6, v3

    move v3, v1

    move v4, v2

    move v7, v1

    move v8, v2

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 360
    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 361
    sget-object v1, Lcom/google/android/apps/gmm/directions/dn;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 362
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 363
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/o/h/a/eq;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 184
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/maps/g/a/hm;)Lcom/google/o/h/a/dq;

    move-result-object v4

    .line 185
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/directions/av;->t()Lcom/google/o/h/a/en;

    move-result-object v5

    .line 187
    const-string v0, ""

    .line 188
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v6, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v3, v6, :cond_2

    move v3, v2

    :goto_0
    if-nez v3, :cond_0

    .line 189
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_0
    new-instance v6, Lcom/google/android/apps/gmm/suggest/k;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/suggest/k;-><init>()V

    .line 193
    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, p3}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 194
    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, p5}, Lcom/google/android/apps/gmm/suggest/l;->b(Ljava/lang/String;)V

    .line 195
    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    .line 196
    sget-object v0, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 197
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    .line 198
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/eq;)V

    .line 199
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/en;)V

    .line 201
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/directions/av;->u()Lcom/google/b/c/cv;

    move-result-object v5

    sget-object v0, Lcom/google/android/apps/gmm/directions/dp;->a:[I

    invoke-virtual {p4}, Lcom/google/o/h/a/eq;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    .line 200
    :goto_1
    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/b;)V

    .line 202
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/suggest/l;->a(Z)V

    .line 203
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Z)V

    .line 204
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Z)V

    .line 205
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->d(Z)V

    .line 206
    iget-object v0, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/suggest/l;->e(Z)V

    .line 210
    sget-object v0, Lcom/google/b/f/t;->cE:Lcom/google/b/f/t;

    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    iput-object v0, v3, Lcom/google/android/apps/gmm/suggest/l;->c:Lcom/google/b/f/t;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    .line 213
    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/o/h/a/dq;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(I)V

    .line 216
    const v0, 0x12000003

    iget-object v3, v6, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/suggest/l;->b(I)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    .line 223
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->h()Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    move-result-object v3

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 225
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v4

    .line 226
    invoke-static {v0, v6, v3, v3}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)Lcom/google/android/apps/gmm/suggest/SuggestFragment;

    move-result-object v0

    .line 225
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v4, v3, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 231
    if-eqz v6, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_4

    :goto_3
    if-eqz v2, :cond_1

    .line 232
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    .line 233
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    const/high16 v0, 0x41800000    # 16.0f

    .line 234
    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    .line 235
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    .line 232
    invoke-static {v6, v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/f/a/a;)V

    .line 237
    :cond_1
    return-void

    :cond_2
    move v3, v1

    .line 188
    goto/16 :goto_0

    :pswitch_0
    move v0, v1

    .line 201
    :goto_4
    new-instance v3, Lcom/google/android/apps/gmm/suggest/e/b;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/directions/av;->d()Lcom/google/maps/g/a/hm;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/directions/av;->e()Lcom/google/r/b/a/afz;

    move-result-object v8

    invoke-direct {v3, v5, v0, v7, v8}, Lcom/google/android/apps/gmm/suggest/e/b;-><init>(Ljava/util/List;ILcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;)V

    move-object v0, v3

    goto/16 :goto_1

    :pswitch_1
    move v0, v2

    goto :goto_4

    .line 213
    :cond_3
    const/4 v0, 0x2

    goto/16 :goto_2

    :cond_4
    move v2, v1

    .line 231
    goto :goto_3

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method final a()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 136
    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 137
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/directions/al;->isResumed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 143
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v3

    .line 141
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/dq;->a:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v5, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v5, :cond_1

    move v2, v1

    :goto_1
    if-eqz v2, :cond_2

    sget v2, Lcom/google/android/apps/gmm/f;->fF:I

    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v4, v4, Lcom/google/android/apps/gmm/directions/dq;->c:Landroid/widget/ImageView;

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 142
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/dq;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v5, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v2, v5, :cond_9

    move v2, v1

    :goto_3
    if-eqz v2, :cond_a

    sget v0, Lcom/google/android/apps/gmm/f;->fF:I

    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 143
    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v2, v0

    .line 141
    goto :goto_1

    :cond_2
    :try_start_1
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v2, v1

    :goto_5
    if-eqz v2, :cond_4

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_6

    move v2, v1

    :goto_6
    if-eqz v2, :cond_7

    :cond_4
    move v2, v1

    :goto_7
    if-nez v2, :cond_8

    sget v2, Lcom/google/android/apps/gmm/f;->eG:I

    goto :goto_2

    :cond_5
    move v2, v0

    goto :goto_5

    :cond_6
    move v2, v0

    goto :goto_6

    :cond_7
    move v2, v0

    goto :goto_7

    :cond_8
    sget v2, Lcom/google/android/apps/gmm/f;->eG:I

    goto :goto_2

    :cond_9
    move v2, v0

    .line 142
    goto :goto_3

    :cond_a
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_e

    :cond_b
    move v2, v1

    :goto_8
    if-eqz v2, :cond_c

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v2

    if-nez v2, :cond_c

    iget-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v2, :cond_f

    move v2, v1

    :goto_9
    if-eqz v2, :cond_d

    :cond_c
    move v0, v1

    :cond_d
    if-nez v0, :cond_10

    sget v0, Lcom/google/android/apps/gmm/f;->eG:I

    goto :goto_4

    :cond_e
    move v2, v0

    goto :goto_8

    :cond_f
    move v2, v0

    goto :goto_9

    :cond_10
    sget v0, Lcom/google/android/apps/gmm/f;->eG:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4
.end method

.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v6, 0x1

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->l()Z

    move-result v0

    if-nez v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dq;->a:Landroid/widget/TextView;

    if-ne p1, v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->aJ:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 111
    new-instance v2, Lcom/google/android/apps/gmm/directions/av;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/directions/av;-><init>(Lcom/google/android/apps/gmm/directions/av;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    sget-object v3, Lcom/google/android/apps/gmm/suggest/e/c;->c:Lcom/google/android/apps/gmm/suggest/e/c;

    sget-object v4, Lcom/google/o/h/a/eq;->a:Lcom/google/o/h/a/eq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/l;->fj:I

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/dn;->a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/o/h/a/eq;Ljava/lang/String;)V

    move v0, v6

    .line 127
    :goto_1
    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->d()V

    goto :goto_0

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dq;->b:Landroid/widget/TextView;

    if-ne p1, v0, :cond_3

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->aH:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 117
    new-instance v2, Lcom/google/android/apps/gmm/directions/av;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/directions/av;-><init>(Lcom/google/android/apps/gmm/directions/av;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->b()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    sget-object v3, Lcom/google/android/apps/gmm/suggest/e/c;->d:Lcom/google/android/apps/gmm/suggest/e/c;

    sget-object v4, Lcom/google/o/h/a/eq;->c:Lcom/google/o/h/a/eq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    sget v5, Lcom/google/android/apps/gmm/l;->fi:I

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/dn;->a(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/o/h/a/eq;Ljava/lang/String;)V

    move v0, v6

    .line 118
    goto :goto_1

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dq;->f:Landroid/view/View;

    if-ne p1, v0, :cond_5

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/t;->aK:Lcom/google/b/f/t;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 123
    sget-object v0, Lcom/google/b/f/t;->aK:Lcom/google/b/f/t;

    new-instance v2, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    iget-object v3, v2, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    iget v4, v3, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v4, v4, 0x80

    iput v4, v3, Lcom/google/maps/g/ia;->a:I

    iput-boolean v1, v3, Lcom/google/maps/g/ia;->f:Z

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/al;->j()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/directions/av;->a(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/dn;->b:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/av;->k()Z

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/dn;->a:Lcom/google/android/apps/gmm/directions/al;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/directions/al;->a(Lcom/google/maps/g/hy;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/dn;->a()V

    new-instance v1, Lcom/google/android/apps/gmm/directions/do;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/directions/do;-><init>(Lcom/google/android/apps/gmm/directions/dn;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v3, v0, Lcom/google/android/apps/gmm/directions/dq;->e:Landroid/view/View;

    if-eqz v2, :cond_4

    const/high16 v0, 0x43340000    # 180.0f

    :goto_2
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    new-instance v5, Landroid/view/animation/RotateAnimation;

    const/4 v7, 0x0

    invoke-direct {v5, v7, v0, v2, v4}, Landroid/view/animation/RotateAnimation;-><init>(FFFF)V

    const-wide/16 v8, 0xc8

    invoke-virtual {v5, v8, v9}, Landroid/view/animation/Animation;->setDuration(J)V

    sget-object v0, Lcom/google/android/apps/gmm/directions/dn;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v5, v0}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    invoke-virtual {v5, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v3, v5}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dq;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->d:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/dn;->a(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dq;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->c:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/dn;->a(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dq;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->b:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/dn;->a(Landroid/view/View;Landroid/view/View;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dq;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/dn;->c:Lcom/google/android/apps/gmm/directions/dq;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/dq;->a:Landroid/widget/TextView;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/dn;->a(Landroid/view/View;Landroid/view/View;)V

    move v0, v6

    .line 124
    goto/16 :goto_1

    .line 123
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    const/high16 v0, -0x3ccc0000    # -180.0f

    goto :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_1
.end method
