.class public Lcom/google/android/apps/gmm/map/n/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/f/g;
.implements Lcom/google/android/apps/gmm/map/t/ak;


# static fields
.field private static final l:I

.field private static final m:I

.field private static final n:I

.field private static final o:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ai;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private volatile A:Z

.field private volatile B:Z

.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/internal/b/b;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/n/ab;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/internal/b/i;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:Lcom/google/android/apps/gmm/map/o/p;

.field public final e:Lcom/google/android/apps/gmm/map/ui/j;

.field volatile f:Z

.field public g:Ljava/lang/String;

.field h:Lcom/google/android/apps/gmm/map/t/af;

.field public final i:Lcom/google/android/apps/gmm/v/ad;

.field public j:Lcom/google/android/apps/gmm/map/c/a;

.field public volatile k:Z

.field private final p:Lcom/google/android/apps/gmm/map/f/o;

.field private final q:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final r:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final s:[I

.field private volatile t:Z

.field private volatile u:Z

.field private final v:Lcom/google/android/apps/gmm/map/legacy/internal/b/k;

.field private w:Lcom/google/android/apps/gmm/map/t/b;

.field private x:Lcom/google/android/apps/gmm/map/t/b;

.field private y:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/16 v5, 0xff

    const/16 v4, 0x80

    const/16 v3, 0x40

    .line 64
    const/16 v0, 0xec

    const/16 v1, 0xe9

    const/16 v2, 0xe1

    invoke-static {v5, v0, v1, v2}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/n/w;->l:I

    .line 67
    invoke-static {v5, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/n/w;->m:I

    .line 70
    invoke-static {v5, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/map/n/w;->n:I

    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v0, v1}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/n/w;->o:Lcom/google/b/c/dn;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/legacy/internal/b/i;Lcom/google/android/apps/gmm/v/ad;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 326
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->q:Ljava/util/HashSet;

    .line 128
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->r:Ljava/util/HashSet;

    .line 134
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->s:[I

    .line 155
    new-instance v0, Lcom/google/android/apps/gmm/map/n/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/x;-><init>(Lcom/google/android/apps/gmm/map/n/w;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->v:Lcom/google/android/apps/gmm/map/legacy/internal/b/k;

    .line 191
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/n/w;->z:Z

    .line 197
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->g:Ljava/lang/String;

    .line 205
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/map/n/w;->A:Z

    .line 210
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/n/w;->B:Z

    .line 273
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/n/w;->k:Z

    .line 327
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 329
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->y_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->g:Ljava/lang/String;

    .line 331
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/n/w;->p:Lcom/google/android/apps/gmm/map/f/o;

    .line 332
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    .line 333
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    .line 334
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    .line 336
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/n/w;->i:Lcom/google/android/apps/gmm/v/ad;

    .line 337
    iget-object v0, p3, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->h:Lcom/google/android/apps/gmm/map/t/af;

    .line 338
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->h:Lcom/google/android/apps/gmm/map/t/af;

    sget-object v1, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/t/b;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/t/af;->a(I)V

    .line 340
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/n/w;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 341
    invoke-direct {p0, p4}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)Z

    .line 342
    invoke-direct {p0, p4, v3, v2, v2}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZZ)V

    .line 344
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/n/w;->c(Lcom/google/android/apps/gmm/map/t/b;)V

    .line 347
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/c/a;->z_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    new-instance v0, Lcom/google/android/apps/gmm/map/ui/j;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/map/ui/j;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->e:Lcom/google/android/apps/gmm/map/ui/j;

    .line 352
    :goto_0
    return-void

    .line 350
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->e:Lcom/google/android/apps/gmm/map/ui/j;

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/v/ad;)V
    .locals 6

    .prologue
    .line 305
    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v5, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    move-object v0, p1

    move-object v1, p4

    move-object v2, p3

    move-object v4, p2

    .line 308
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/ai;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    .line 305
    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/n/w;-><init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/legacy/internal/b/i;Lcom/google/android/apps/gmm/v/ad;)V

    .line 315
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/t/b;)I
    .locals 2

    .prologue
    .line 282
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    if-eq p0, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    if-eq v0, v1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->n:I

    .line 290
    :goto_0
    return v0

    .line 285
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/map/n/aa;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/t/b;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 290
    const/4 v0, -0x1

    goto :goto_0

    .line 286
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/map/n/w;->l:I

    goto :goto_0

    .line 287
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/map/n/w;->m:I

    goto :goto_0

    .line 288
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/map/n/w;->m:I

    goto :goto_0

    .line 289
    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/map/n/w;->n:I

    goto :goto_0

    .line 285
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZ)V
    .locals 4

    .prologue
    .line 675
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 691
    :cond_0
    :goto_0
    return-void

    .line 679
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    .line 680
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/o/p;->b(Lcom/google/android/apps/gmm/map/o/ak;)V

    .line 683
    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    if-eqz v0, :cond_3

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 686
    if-eqz p2, :cond_3

    move-object v0, p1

    .line 687
    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/map/j/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/map/j/ai;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Z)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 690
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a()V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZZ)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->GL_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 620
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 671
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a()V

    .line 632
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 633
    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 634
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->v:Lcom/google/android/apps/gmm/map/legacy/internal/b/k;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->j:Lcom/google/android/apps/gmm/map/legacy/internal/b/k;

    .line 635
    sget-boolean v1, Lcom/google/android/apps/gmm/map/util/c;->k:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->y:Z

    .line 643
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/n/w;->B:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->p:Z

    .line 644
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    if-eqz v1, :cond_2

    .line 645
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/n/w;->k:Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->c:Lcom/google/android/apps/gmm/map/legacy/a/b/d;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/map/legacy/a/b/d;->q:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c()V

    .line 647
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/n/w;->t:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->m:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 648
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/n/w;->u:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->n:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v2, v1, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 649
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 652
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Ljava/lang/String;)V

    .line 654
    if-eqz p3, :cond_5

    .line 655
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/c/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/map/j/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-direct {v3, v0, v1}, Lcom/google/android/apps/gmm/map/j/ai;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Z)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 658
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 660
    if-eqz p2, :cond_6

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    .line 662
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->h:Lcom/google/android/apps/gmm/map/t/af;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    if-eqz v0, :cond_6

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->h:Lcom/google/android/apps/gmm/map/t/af;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 664
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->h:Lcom/google/android/apps/gmm/map/t/af;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a(Lcom/google/android/apps/gmm/map/t/af;)V

    .line 668
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    .line 669
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/map/o/ak;)V

    goto/16 :goto_0
.end method

.method private b(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)Z
    .locals 4

    .prologue
    .line 868
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->B_()Landroid/accounts/Account;

    move-result-object v0

    .line 869
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    .line 870
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->b:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 871
    if-eqz v1, :cond_0

    .line 872
    if-nez v0, :cond_2

    .line 873
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/r;->b(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    .line 879
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    .line 880
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/bd;->c:Lcom/google/android/apps/gmm/map/internal/d/ag;

    .line 881
    if-eqz v1, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 882
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/d/ag;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 883
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/ag;->a(Landroid/accounts/Account;)V

    .line 885
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/b;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/b;-><init>(Landroid/accounts/Account;)V

    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z

    move-result v0

    return v0

    .line 875
    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->v:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/d/r;->c(Lcom/google/android/apps/gmm/map/b/a/ai;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/o;->j:F

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)F
    .locals 7

    .prologue
    const/high16 v1, 0x41a80000    # 21.0f

    .line 452
    .line 453
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v4

    .line 454
    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v0

    move v3, v1

    :goto_0
    if-ge v2, v5, :cond_1

    .line 455
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 456
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    if-nez v6, :cond_0

    move v0, v1

    :goto_1
    invoke-static {v3, v0}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 454
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 456
    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->g:Lcom/google/android/apps/gmm/map/internal/b/e;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/internal/b/e;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    goto :goto_1

    .line 458
    :cond_1
    monitor-exit v4

    .line 459
    return v3

    .line 458
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/a;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 850
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/n/w;->d()Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v0

    .line 851
    if-eqz v0, :cond_0

    .line 852
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 853
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->d()V

    .line 857
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v2

    .line 858
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 859
    sget-object v3, Lcom/google/android/apps/gmm/map/n/ac;->d:Lcom/google/android/apps/gmm/map/n/ac;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    .line 860
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/n/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/n/ab;->b:Lcom/google/android/apps/gmm/map/n/ac;

    .line 859
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/n/ac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 861
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/n/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)Z

    .line 858
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 864
    :cond_2
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ah;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 890
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    .line 891
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/bd;->b:Lcom/google/android/apps/gmm/map/internal/d/r;

    .line 892
    if-eqz v0, :cond_0

    .line 893
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/ah;->b:Lcom/google/e/a/a/a/b;

    .line 894
    if-eqz v1, :cond_3

    .line 895
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/r;->a(Lcom/google/e/a/a/a/b;)V

    .line 901
    :cond_0
    :goto_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ah;->a:Z

    if-eqz v0, :cond_2

    .line 904
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    .line 905
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/bd;->c:Lcom/google/android/apps/gmm/map/internal/d/ag;

    .line 906
    if-eqz v0, :cond_1

    .line 907
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/ag;->b()V

    .line 910
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/n/w;->d()Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-result-object v0

    .line 911
    if-eqz v0, :cond_2

    .line 912
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->c()V

    .line 915
    :cond_2
    return-void

    .line 897
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/r;->e()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V
    .locals 4

    .prologue
    .line 398
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v1

    .line 400
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/map/n/ab;

    sget-object v2, Lcom/google/android/apps/gmm/map/n/ac;->a:Lcom/google/android/apps/gmm/map/n/ac;

    invoke-direct {v0, v2, p1}, Lcom/google/android/apps/gmm/map/n/ab;-><init>(Lcom/google/android/apps/gmm/map/n/ac;Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 401
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->i:Lcom/google/android/apps/gmm/v/ad;

    new-instance v1, Lcom/google/android/apps/gmm/map/n/z;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/n/z;-><init>(Lcom/google/android/apps/gmm/map/n/w;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 404
    return-void

    .line 402
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)V
    .locals 4

    .prologue
    .line 930
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/legacy/internal/b/i;)Z

    .line 931
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v1

    .line 932
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/map/n/ab;

    sget-object v2, Lcom/google/android/apps/gmm/map/n/ac;->d:Lcom/google/android/apps/gmm/map/n/ac;

    invoke-direct {v0, v2, p1}, Lcom/google/android/apps/gmm/map/n/ab;-><init>(Lcom/google/android/apps/gmm/map/n/ac;Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 934
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 935
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 936
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->i:Lcom/google/android/apps/gmm/v/ad;

    new-instance v1, Lcom/google/android/apps/gmm/map/n/z;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/n/z;-><init>(Lcom/google/android/apps/gmm/map/n/w;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 937
    return-void

    .line 935
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/p/b;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 820
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->o:D

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 821
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/p/c;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 805
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/n/w;->u:Z

    .line 806
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v2

    .line 807
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 808
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 809
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/map/n/w;->u:Z

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->n:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v5, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v4, v0, v5}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 807
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 811
    :cond_1
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/p/d;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 840
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/n/w;->B:Z

    .line 841
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v1

    .line 842
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 843
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/n/w;->B:Z

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->p:Z

    goto :goto_1

    .line 845
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 840
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 845
    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/p/i;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 828
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/n/w;->A:Z

    .line 829
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v2

    .line 830
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 833
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/n/w;->A:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->y:Z

    goto :goto_0

    .line 835
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 833
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 835
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/p/j;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 794
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/p/f;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/n/w;->t:Z

    .line 795
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v2

    .line 796
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 797
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 798
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/map/n/w;->t:Z

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->m:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v5, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v4, v0, v5}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 796
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 800
    :cond_1
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 373
    const-string v0, "OverlayManager.onContextChanged"

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/t;->a(Ljava/lang/String;)V

    .line 379
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/n/w;->z:Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->i:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/n/w;->p:Lcom/google/android/apps/gmm/map/f/o;

    invoke-interface {v0, v1, v3}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 382
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_1

    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    .line 384
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/n/w;->h:Lcom/google/android/apps/gmm/map/t/af;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/t/af;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/a;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/n/w;->p:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/n/w;->j:Lcom/google/android/apps/gmm/map/c/a;

    invoke-virtual {v0, v4, v5, v6}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 385
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/map/o/ak;)V

    .line 382
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 379
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 388
    :cond_1
    const-string v0, "OverlayManager"

    const-string v1, "Surface created"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 389
    const-string v0, "OverlayManager.onContextChanged"

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/t;->b(Ljava/lang/String;)V

    .line 390
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V
    .locals 4

    .prologue
    .line 412
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v1

    .line 414
    :try_start_0
    new-instance v0, Lcom/google/android/apps/gmm/map/n/ab;

    sget-object v2, Lcom/google/android/apps/gmm/map/n/ac;->b:Lcom/google/android/apps/gmm/map/n/ac;

    invoke-direct {v0, v2, p1}, Lcom/google/android/apps/gmm/map/n/ab;-><init>(Lcom/google/android/apps/gmm/map/n/ac;Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    .line 415
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 416
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 417
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->i:Lcom/google/android/apps/gmm/v/ad;

    new-instance v1, Lcom/google/android/apps/gmm/map/n/z;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/n/z;-><init>(Lcom/google/android/apps/gmm/map/n/w;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 418
    return-void

    .line 416
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 4

    .prologue
    .line 508
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->i:Lcom/google/android/apps/gmm/v/ad;

    new-instance v1, Lcom/google/android/apps/gmm/map/n/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/map/n/y;-><init>(Lcom/google/android/apps/gmm/map/n/w;Lcom/google/android/apps/gmm/map/t/b;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 519
    monitor-exit p0

    return-void

    .line 508
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 16

    .prologue
    const/4 v15, 0x3

    const/4 v14, 0x2

    const/4 v7, -0x1

    const/4 v6, 0x0

    const/4 v9, 0x1

    .line 473
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->x:Lcom/google/android/apps/gmm/map/t/b;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->x:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->x:Lcom/google/android/apps/gmm/map/t/b;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v4

    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/n/w;->z:Z

    if-nez v1, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v6

    :goto_1
    if-ge v2, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/n/w;->h:Lcom/google/android/apps/gmm/map/t/af;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a(Lcom/google/android/apps/gmm/map/t/af;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 473
    :cond_1
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->x:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->x:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->e:Lcom/google/android/apps/gmm/map/ui/j;

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/n/w;->f:Z

    :cond_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v2, v6

    :goto_2
    if-ge v2, v3, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :catchall_0
    move-exception v1

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/map/t/b;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/n/w;->b(Lcom/google/android/apps/gmm/map/t/b;)V

    goto :goto_0

    .line 474
    :cond_4
    const/4 v1, 0x1

    :try_start_4
    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/n/w;->z:Z

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v6

    :goto_3
    if-ge v3, v5, :cond_8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/n/ab;

    sget-object v2, Lcom/google/android/apps/gmm/map/n/aa;->b:[I

    iget-object v8, v1, Lcom/google/android/apps/gmm/map/n/ab;->b:Lcom/google/android/apps/gmm/map/n/ac;

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/map/n/ac;->ordinal()I

    move-result v8

    aget v2, v2, v8

    packed-switch v2, :pswitch_data_0

    :cond_6
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    :pswitch_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    const/4 v2, 0x1

    const/4 v8, 0x1

    const/4 v10, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v8, v10}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZZ)V

    goto :goto_4

    :catchall_1
    move-exception v1

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v1

    :pswitch_1
    :try_start_5
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    const/4 v2, 0x1

    const/4 v8, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZ)V

    goto :goto_4

    :pswitch_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    const/4 v8, 0x1

    const/4 v10, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8, v10}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZ)V

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/n/ad;

    move-object v2, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/n/ad;->c:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    const/4 v8, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8, v10, v11}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->d:Lcom/google/android/apps/gmm/map/o/p;

    iget-object v8, v1, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v1, Lcom/google/android/apps/gmm/map/n/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/n/ad;->c:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    invoke-interface {v2, v8, v1}, Lcom/google/android/apps/gmm/map/o/p;->a(Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/ak;)V

    goto :goto_4

    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-eq v8, v2, :cond_7

    move v2, v9

    :goto_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/n/w;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    const/4 v10, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2, v10}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZ)V

    iget-object v8, v1, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    const/4 v10, 0x1

    const/4 v11, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v10, v2, v11}, Lcom/google/android/apps/gmm/map/n/w;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;ZZZ)V

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/n/ab;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/b;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    goto :goto_4

    :cond_7
    move v2, v6

    goto :goto_5

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->p:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/f/o;->f()V

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 475
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->e:Lcom/google/android/apps/gmm/map/ui/j;

    if-eqz v1, :cond_e

    .line 476
    monitor-enter p0

    :try_start_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/n/w;->f:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/n/w;->f:Z

    monitor-exit p0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    if-eqz v1, :cond_e

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->q:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->r:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->clear()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->s:[I

    aput v7, v1, v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->s:[I

    aget v4, v1, v6

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v10

    move v8, v6

    :goto_6
    :try_start_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v8, v1, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/n/w;->p:Lcom/google/android/apps/gmm/map/f/o;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/n/w;->q:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->r:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/n/w;->s:[I

    move v5, v6

    move v3, v7

    :goto_7
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_a

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->d:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    invoke-interface {v2, v11, v12}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/Collection;)V

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->h()I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result v2

    if-le v2, v3, :cond_16

    :goto_8
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v3, v2

    goto :goto_7

    :catchall_2
    move-exception v1

    :try_start_8
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    throw v1

    :cond_a
    if-eqz v13, :cond_b

    :try_start_9
    array-length v1, v13

    if-lez v1, :cond_b

    const/4 v1, 0x0

    aput v3, v13, v1

    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->s:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    if-le v1, v4, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->s:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    :goto_9
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v4, v1

    goto :goto_6

    :cond_c
    monitor-exit v10
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/w;->e:Lcom/google/android/apps/gmm/map/ui/j;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/n/w;->q:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/n/w;->r:Ljava/util/HashSet;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/n/w;->w:Lcom/google/android/apps/gmm/map/t/b;

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    if-eqz v8, :cond_e

    if-eq v4, v7, :cond_f

    :goto_a
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/ui/j;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/ui/j;->a(Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_10

    invoke-virtual {v3}, Ljava/util/HashSet;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_10

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/ui/j;->a:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->ir:I

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v10, v9

    aput-object v8, v10, v14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v10, v15

    const/4 v4, 0x4

    aput-object v7, v10, v4

    invoke-virtual {v1, v3, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_b
    sget-object v3, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    if-eq v5, v3, :cond_d

    sget-object v3, Lcom/google/android/apps/gmm/map/t/b;->d:Lcom/google/android/apps/gmm/map/t/b;

    if-ne v5, v3, :cond_13

    :cond_d
    sget v3, Lcom/google/android/apps/gmm/d;->aR:I

    sget v4, Lcom/google/android/apps/gmm/d;->ar:I

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    new-instance v6, Lcom/google/android/apps/gmm/map/ui/k;

    invoke-direct {v6, v2, v3, v4, v1}, Lcom/google/android/apps/gmm/map/ui/k;-><init>(Lcom/google/android/apps/gmm/map/ui/j;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 478
    :cond_e
    :goto_c
    return-void

    .line 476
    :catchall_3
    move-exception v1

    :try_start_a
    monitor-exit v10
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    throw v1

    :cond_f
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/util/Calendar;->get(I)I

    move-result v4

    goto :goto_a

    :cond_10
    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-virtual {v3}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_11

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/ui/j;->a:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->is:I

    new-array v7, v9, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v6

    invoke-virtual {v1, v3, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    :cond_11
    invoke-virtual {v3}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_12

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/ui/j;->a:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->iu:I

    new-array v8, v15, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v9

    aput-object v7, v8, v14

    invoke-virtual {v1, v3, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    :cond_12
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/ui/j;->a:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/apps/gmm/l;->it:I

    new-array v7, v15, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v9

    aput-object v8, v7, v14

    invoke-virtual {v1, v3, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_b

    :cond_13
    sget-object v3, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    if-ne v5, v3, :cond_14

    sget v3, Lcom/google/android/apps/gmm/d;->aR:I

    sget v4, Lcom/google/android/apps/gmm/d;->ar:I

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    new-instance v6, Lcom/google/android/apps/gmm/map/ui/k;

    invoke-direct {v6, v2, v3, v4, v1}, Lcom/google/android/apps/gmm/map/ui/k;-><init>(Lcom/google/android/apps/gmm/map/ui/j;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto :goto_c

    :cond_14
    sget v3, Lcom/google/android/apps/gmm/d;->ar:I

    sget v4, Lcom/google/android/apps/gmm/d;->aR:I

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/ui/j;->b:Landroid/widget/TextView;

    new-instance v6, Lcom/google/android/apps/gmm/map/ui/k;

    invoke-direct {v6, v2, v3, v4, v1}, Lcom/google/android/apps/gmm/map/ui/k;-><init>(Lcom/google/android/apps/gmm/map/ui/j;IILjava/lang/String;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_c

    :cond_15
    move v1, v4

    goto/16 :goto_9

    :cond_16
    move v2, v3

    goto/16 :goto_8

    .line 474
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 1

    .prologue
    .line 718
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/w;->x:Lcom/google/android/apps/gmm/map/t/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 719
    monitor-exit p0

    return-void

    .line 718
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/legacy/internal/b/i;
    .locals 2

    .prologue
    .line 770
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v1

    .line 771
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->y:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    monitor-exit v1

    return-object v0

    .line 772
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final e()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 780
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/n/w;->a:Ljava/util/List;

    monitor-enter v4

    .line 781
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v1

    :goto_0
    if-ge v3, v5, :cond_3

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/w;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    .line 783
    sget-object v6, Lcom/google/android/apps/gmm/map/n/w;->o:Lcom/google/b/c/dn;

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v6, v7}, Lcom/google/b/c/dn;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 784
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    if-eqz v6, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->r:Lcom/google/android/apps/gmm/map/legacy/internal/b/l;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/i;

    iget-boolean v6, v6, Lcom/google/android/apps/gmm/map/legacy/internal/b/i;->l:Z

    if-eqz v6, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/l;->d:Z

    if-eqz v0, :cond_0

    move v0, v2

    :goto_1
    if-nez v0, :cond_2

    .line 785
    monitor-exit v4

    move v0, v1

    .line 788
    :goto_2
    return v0

    :cond_0
    move v0, v1

    .line 784
    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1

    .line 781
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 788
    :cond_3
    monitor-exit v4

    move v0, v2

    goto :goto_2

    .line 789
    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
