.class public final Lcom/google/android/apps/gmm/ab/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/ab/a;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/Object;

.field private d:Lcom/google/android/gms/common/api/o;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/ab/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/ab/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/ab/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->c:Ljava/lang/Object;

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->e:Ljava/util/List;

    .line 73
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/content/Context;

    iput-object p1, p0, Lcom/google/android/apps/gmm/ab/b;->b:Landroid/content/Context;

    .line 74
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/ab/b;Z)V
    .locals 7

    .prologue
    .line 32
    iget-object v1, p0, Lcom/google/android/apps/gmm/ab/b;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v2, "Programmer error in this class"

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    if-eqz p1, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/ab/e;

    sget-object v3, Lcom/google/android/gms/wearable/p;->b:Lcom/google/android/gms/wearable/i;

    iget-object v4, p0, Lcom/google/android/apps/gmm/ab/b;->d:Lcom/google/android/gms/common/api/o;

    iget-object v5, v0, Lcom/google/android/apps/gmm/ab/e;->a:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/android/apps/gmm/ab/e;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/apps/gmm/ab/e;->c:[B

    invoke-interface {v3, v4, v5, v6, v0}, Lcom/google/android/gms/wearable/i;->a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/s;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->d:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->c()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 78
    iget-object v1, p0, Lcom/google/android/apps/gmm/ab/b;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->d:Lcom/google/android/gms/common/api/o;

    if-nez v0, :cond_0

    .line 80
    new-instance v0, Lcom/google/android/gms/common/api/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/ab/b;->b:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/p;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/wearable/p;->d:Lcom/google/android/gms/common/api/a;

    .line 81
    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/ab/d;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/ab/d;-><init>(Lcom/google/android/apps/gmm/ab/b;)V

    .line 82
    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/ab/c;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/ab/c;-><init>(Lcom/google/android/apps/gmm/ab/b;)V

    .line 92
    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/gms/common/api/p;

    move-result-object v0

    .line 99
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->d:Lcom/google/android/gms/common/api/o;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->d:Lcom/google/android/gms/common/api/o;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/o;->b()V

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/ab/b;->e:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/ab/e;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/apps/gmm/ab/e;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
