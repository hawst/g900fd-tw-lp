.class public Lcom/google/android/apps/gmm/map/f/a/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/f/a/e;


# instance fields
.field public final b:F

.field public final c:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/f/a/e;-><init>(FF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/a/e;->a:Lcom/google/android/apps/gmm/map/f/a/e;

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    .line 38
    iput p2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    .line 39
    return-void
.end method

.method public static a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;
    .locals 4

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/e;

    mul-float v1, p0, v2

    div-float/2addr v1, p2

    sub-float/2addr v1, v3

    mul-float/2addr v2, p1

    div-float/2addr v2, p3

    sub-float/2addr v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/f/a/e;-><init>(FF)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/f/a/e;)Lcom/google/android/apps/gmm/map/f/a/e;
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/high16 v3, -0x40800000    # -1.0f

    .line 95
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 96
    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 97
    :goto_1
    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 98
    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_2

    .line 102
    :goto_2
    return-object p0

    .line 95
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/c/s;->a(FFF)F

    move-result v0

    goto :goto_0

    .line 96
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/s;->a(FFF)F

    move-result v1

    goto :goto_1

    .line 102
    :cond_2
    new-instance p0, Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/f/a/e;-><init>(FF)V

    goto :goto_2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    if-ne p0, p1, :cond_1

    .line 50
    :cond_0
    :goto_0
    return v0

    .line 45
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/f/a/e;

    if-eqz v2, :cond_3

    .line 46
    check-cast p1, Lcom/google/android/apps/gmm/map/f/a/e;

    .line 47
    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    .line 48
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 50
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 60
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "x"

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    .line 61
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "y"

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    .line 62
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 63
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
