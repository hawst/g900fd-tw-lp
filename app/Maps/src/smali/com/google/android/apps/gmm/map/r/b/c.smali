.class public Lcom/google/android/apps/gmm/map/r/b/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:F

.field public b:D

.field public c:F

.field public d:Landroid/os/Bundle;

.field public e:D

.field public f:D

.field public g:Ljava/lang/String;

.field public h:F

.field public i:J

.field public j:J

.field k:Lcom/google/android/apps/gmm/map/indoor/d/f;

.field public l:Lcom/google/android/apps/gmm/map/b/a/u;

.field public m:Lcom/google/android/apps/gmm/map/r/b/e;

.field public n:Lcom/google/android/apps/gmm/map/r/b/d;

.field o:Lcom/google/android/apps/gmm/map/b/a/j;

.field p:I

.field public q:Z

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Z

.field public v:Z

.field w:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 945
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    .line 946
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->r:Z

    .line 947
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    .line 948
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    .line 949
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    .line 950
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->v:Z

    .line 951
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->w:Z

    return-void
.end method


# virtual methods
.method public final a(DD)Lcom/google/android/apps/gmm/map/r/b/c;
    .locals 7

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 981
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/r/b/c;->e:D

    .line 982
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/r/b/c;->f:D

    .line 983
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    mul-double v2, p1, v4

    double-to-int v1, v2

    mul-double v2, p3, v4

    double-to-int v2, v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 984
    return-object p0
.end method

.method public final a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1051
    if-nez p1, :cond_0

    .line 1075
    :goto_0
    return-object p0

    .line 1054
    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->a:F

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->q:Z

    :cond_1
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->b:D

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->r:Z

    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->c:F

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->s:Z

    :cond_3
    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/r/b/c;->a(DD)Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-virtual {p1}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->g:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->h:F

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->t:Z

    .line 1055
    :cond_4
    invoke-virtual {p1}, Landroid/location/Location;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->d:Landroid/os/Bundle;

    .line 1056
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/indoor/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->k:Lcom/google/android/apps/gmm/map/indoor/d/f;

    if-eqz v0, :cond_5

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->d:Landroid/os/Bundle;

    .line 1057
    :cond_5
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_9

    .line 1058
    check-cast p1, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 1059
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->v:Z

    .line 1060
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->j:J

    .line 1061
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-eqz v0, :cond_6

    .line 1062
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/b/a;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->i:J

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    .line 1064
    :cond_6
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v0, :cond_7

    .line 1065
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/r/b/e;-><init>(Lcom/google/android/apps/gmm/map/r/b/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->m:Lcom/google/android/apps/gmm/map/r/b/e;

    .line 1067
    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_8

    .line 1068
    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/d;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/b/a;->l:Lcom/google/android/apps/gmm/map/r/b/d;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/r/b/d;-><init>(Lcom/google/android/apps/gmm/map/r/b/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    .line 1070
    :cond_8
    iget-object v0, p1, Lcom/google/android/apps/gmm/p/d/f;->e:Lcom/google/o/b/a/h;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/o/b/a/h;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->o:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 1071
    iget v0, p1, Lcom/google/android/apps/gmm/p/d/f;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->p:I

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->w:Z

    goto/16 :goto_0

    .line 1073
    :cond_9
    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->i:J

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/r/b/c;->u:Z

    goto/16 :goto_0
.end method

.method public final a(Z)Lcom/google/android/apps/gmm/map/r/b/c;
    .locals 1

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eq v0, p1, :cond_0

    .line 1126
    if-eqz p1, :cond_2

    new-instance v0, Lcom/google/android/apps/gmm/map/r/b/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/b/d;-><init>()V

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/c;->n:Lcom/google/android/apps/gmm/map/r/b/d;

    .line 1128
    :cond_0
    return-object p0

    .line 1125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1126
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
