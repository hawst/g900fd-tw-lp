.class public final Lcom/google/android/apps/gmm/shared/c/s;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:F


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 16
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/gmm/shared/c/s;->a:F

    return-void
.end method

.method public static a(F)F
    .locals 3

    .prologue
    const/high16 v2, 0x43b40000    # 360.0f

    .line 40
    move v0, p0

    :goto_0
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 41
    add-float/2addr v0, v2

    goto :goto_0

    .line 43
    :cond_0
    :goto_1
    cmpl-float v1, v0, v2

    if-ltz v1, :cond_1

    .line 44
    sub-float/2addr v0, v2

    goto :goto_1

    .line 46
    :cond_1
    return v0
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 241
    invoke-static {p0, p2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public static a(FFFFF)F
    .locals 2

    .prologue
    .line 260
    .line 261
    invoke-static {p4, p3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    sub-float/2addr v0, p2

    sub-float v1, p3, p2

    div-float/2addr v0, v1

    .line 263
    sub-float v1, p1, p0

    mul-float/2addr v0, v1

    add-float/2addr v0, p0

    return v0
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 70
    if-ltz p0, :cond_0

    .line 71
    add-int v0, p0, p1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, p1

    .line 73
    :goto_0
    return v0

    :cond_0
    div-int v0, p0, p1

    goto :goto_0
.end method

.method public static a(III)I
    .locals 1

    .prologue
    .line 234
    invoke-static {p0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(JJJ)J
    .locals 2

    .prologue
    .line 227
    invoke-static {p0, p1, p4, p5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 220
    add-int/lit8 v0, p0, -0x1

    and-int/2addr v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(F)F
    .locals 2

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 110
    cmpl-float v0, p0, v1

    if-ltz v0, :cond_1

    .line 111
    sub-float/2addr p0, v1

    .line 115
    :cond_0
    :goto_0
    return p0

    .line 112
    :cond_1
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    .line 113
    add-float/2addr p0, v1

    goto :goto_0
.end method

.method public static b(FFF)F
    .locals 1

    .prologue
    .line 250
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method public static b(II)I
    .locals 1

    .prologue
    .line 85
    div-int/lit8 v0, p1, 0x2

    add-int/2addr v0, p0

    div-int/2addr v0, p1

    return v0
.end method

.method public static c(F)F
    .locals 2

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 155
    const/high16 v0, 0x43340000    # 180.0f

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_1

    .line 156
    sub-float/2addr p0, v1

    .line 160
    :cond_0
    :goto_0
    return p0

    .line 157
    :cond_1
    const/high16 v0, -0x3ccc0000    # -180.0f

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    .line 158
    add-float/2addr p0, v1

    goto :goto_0
.end method

.method public static c(II)I
    .locals 2

    .prologue
    .line 185
    mul-int v0, p0, p1

    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/shared/c/s;->d(II)I

    move-result v1

    div-int/2addr v0, v1

    return v0
.end method

.method public static d(F)F
    .locals 2

    .prologue
    .line 180
    float-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    sget v1, Lcom/google/android/apps/gmm/shared/c/s;->a:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public static d(II)I
    .locals 2

    .prologue
    .line 191
    if-lez p0, :cond_0

    if-gtz p1, :cond_1

    .line 192
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Greatest common divisor should be computed on numbers greater than zero."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :cond_1
    :goto_0
    if-eqz p1, :cond_2

    .line 199
    rem-int v0, p0, p1

    move p0, p1

    move p1, v0

    .line 200
    goto :goto_0

    .line 202
    :cond_2
    return p0
.end method

.method public static e(II)I
    .locals 0

    .prologue
    .line 212
    .line 213
    :goto_0
    if-ge p1, p0, :cond_0

    .line 214
    shl-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 216
    :cond_0
    return p1
.end method
