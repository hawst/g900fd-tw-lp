.class public Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;
.super Landroid/widget/LinearLayout;
.source "PG"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field b:Landroid/widget/ImageView;

.field public c:Landroid/widget/LinearLayout;

.field public d:Lcom/google/android/apps/gmm/base/d/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->a:Landroid/content/Context;

    .line 45
    return-void
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 4

    .prologue
    .line 49
    sget v0, Lcom/google/android/apps/gmm/g;->bb:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->c:Landroid/widget/LinearLayout;

    .line 52
    sget v0, Lcom/google/android/apps/gmm/g;->aQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->b:Landroid/widget/ImageView;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/f;->bv:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    const/4 v1, 0x0

    .line 53
    invoke-static {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->createFromStream(Ljava/io/InputStream;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 55
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aO:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 57
    new-instance v2, Lcom/google/android/apps/gmm/base/d/d;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/base/d/d;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->d:Lcom/google/android/apps/gmm/base/d/d;

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->b:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->d:Lcom/google/android/apps/gmm/base/d/d;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 59
    return-void
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 98
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRtlPropertiesChanged(I)V

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->d:Lcom/google/android/apps/gmm/base/d/d;

    if-ne p1, v0, :cond_0

    :goto_0
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/d/d;->f:Z

    .line 100
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setBucketValues([I[Ljava/lang/String;)V
    .locals 5
    .param p2    # [Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 66
    if-eqz p2, :cond_0

    array-length v0, p1

    array-length v1, p2

    if-eq v0, v1, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    array-length v1, p2

    array-length v2, p2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x67

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected bucket label list length "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " does not equal number of histogram buckets "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 75
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    array-length v1, p1

    if-le v0, v1, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->c:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    goto :goto_0

    .line 80
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/h;->f:I

    invoke-static {v0, v1, v3}, Landroid/widget/TextView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 88
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->d:Lcom/google/android/apps/gmm/base/d/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/d/d;->a([I)V

    .line 91
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    array-length v0, p1

    if-ge v1, v0, :cond_4

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-nez p2, :cond_3

    move-object v2, v3

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 92
    :cond_3
    aget-object v2, p2, v1

    goto :goto_3

    .line 94
    :cond_4
    return-void
.end method
