.class Lcom/google/android/apps/gmm/navigation/navui/p;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/navui/o;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/o;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/car/a/a;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/car/a/a;->a:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->d:Z

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/o;->e()V

    .line 179
    :cond_0
    return-void

    .line 175
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->f:Z

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/navui/o;->e()V

    .line 169
    :cond_0
    return-void

    .line 165
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 10
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 158
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/p;->a:Lcom/google/android/apps/gmm/navigation/navui/o;

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_5

    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iget-object v5, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->a:Lcom/google/android/apps/gmm/x/a;

    sget-object v6, Lcom/google/android/apps/gmm/navigation/g/c;->a:Lcom/google/android/apps/gmm/x/l;

    const-string v0, "bundled"

    iget-object v7, v6, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/x/a;->b(Lcom/google/android/apps/gmm/x/l;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_4

    sget-object v5, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x2b

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Serializable with id \""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "\""

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " loaded from memory."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iput-object v0, v4, Lcom/google/android/apps/gmm/navigation/navui/b/b;->j:Lcom/google/android/apps/gmm/base/g/c;

    :goto_3
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/navui/o;->e()V

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_9

    move v0, v1

    :goto_4
    if-nez v0, :cond_3

    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->b:Lcom/google/android/apps/gmm/navigation/navui/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/q;->a()V

    .line 159
    :cond_3
    return-void

    .line 158
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/x/f;

    invoke-direct {v0, v5, v6}, Lcom/google/android/apps/gmm/x/f;-><init>(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/l;)V

    iget-object v7, v5, Lcom/google/android/apps/gmm/x/a;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    sget-object v8, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v7, v0, v8}, Lcom/google/android/apps/gmm/shared/c/a/j;->b(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/x/f;->a:Ljava/io/Serializable;

    invoke-virtual {v5, v6, v0}, Lcom/google/android/apps/gmm/x/a;->b(Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_2

    :cond_5
    iget v0, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->c:I

    if-gtz v0, :cond_7

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    :goto_5
    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Lcom/google/android/apps/gmm/navigation/navui/b/b;

    goto :goto_3

    :cond_7
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget v4, v3, Lcom/google/android/apps/gmm/navigation/navui/o;->c:I

    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/navigation/g/b/f;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;I)Lcom/google/android/apps/gmm/navigation/g/b/f;

    move-result-object v0

    goto :goto_5

    :cond_9
    move v0, v2

    goto :goto_4
.end method
