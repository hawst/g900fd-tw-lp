.class Lcom/google/android/apps/gmm/map/internal/d/b/u;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:[B

.field b:I

.field final c:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<[I>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(IILcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 3

    .prologue
    .line 3048
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3049
    mul-int/lit16 v0, p1, 0x400

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    .line 3050
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->b:I

    .line 3052
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/v;

    const/4 v1, 0x3

    const-string v2, "GenericDiskCache.hashBis"

    invoke-direct {v0, p0, v1, p3, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/v;-><init>(Lcom/google/android/apps/gmm/map/internal/d/b/u;ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 3059
    return-void
.end method

.method static a(J[I)[I
    .locals 10

    .prologue
    const/4 v0, 0x0

    const-wide/16 v8, 0x1fd3

    const/16 v6, 0x16

    .line 3116
    invoke-static {p0, p1, v6}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    .line 3117
    rem-long v4, v2, v8

    long-to-int v1, v4

    aput v1, p2, v0

    .line 3118
    invoke-static {v2, v3, v6}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    .line 3119
    const/4 v1, 0x1

    rem-long v4, v2, v8

    long-to-int v4, v4

    aput v4, p2, v1

    .line 3120
    invoke-static {v2, v3, v6}, Ljava/lang/Long;->rotateRight(JI)J

    move-result-wide v2

    .line 3121
    const/4 v1, 0x2

    rem-long/2addr v2, v8

    long-to-int v2, v2

    aput v2, p2, v1

    .line 3122
    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_1

    .line 3123
    aget v1, p2, v0

    if-gez v1, :cond_0

    .line 3124
    aget v1, p2, v0

    add-int/lit16 v1, v1, 0x1fd3

    aput v1, p2, v0

    .line 3122
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3127
    :cond_1
    return-object p2
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/internal/d/b/ae;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 3089
    .line 3090
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    mul-int/lit16 v0, v0, 0x400

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    add-int/lit16 v3, v0, 0x3fc

    invoke-static {v2, v0, v3, v1}, Ljava/util/Arrays;->fill([BIIB)V

    move v2, v1

    .line 3091
    :goto_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->c:I

    if-ge v2, v0, :cond_2

    .line 3092
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->a:[B

    mul-int/lit8 v3, v2, 0x14

    add-int/lit8 v3, v3, 0x8

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v4

    int-to-long v4, v4

    add-int/lit8 v3, v3, 0x4

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v0

    int-to-long v6, v0

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    const/16 v0, 0x20

    shl-long/2addr v4, v0

    or-long/2addr v4, v6

    .line 3093
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    .line 3094
    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    invoke-static {v4, v5, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a(J[I)[I

    move-result-object v4

    shl-int/lit8 v3, v3, 0xa

    move v0, v1

    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_0

    aget v5, v4, v0

    shr-int/lit8 v5, v5, 0x3

    aget v6, v4, v0

    and-int/lit8 v6, v6, 0x7

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    add-int/2addr v5, v3

    aget-byte v8, v7, v5

    const/4 v9, 0x1

    shl-int v6, v9, v6

    or-int/2addr v6, v8

    int-to-byte v6, v6

    aput-byte v6, v7, v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 3091
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 3097
    :cond_2
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->b:I

    if-lt v0, v1, :cond_3

    .line 3098
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ae;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->b:I

    .line 3100
    :cond_3
    return-void
.end method

.method final a([II)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3151
    shl-int/lit8 v3, p2, 0xa

    move v0, v1

    .line 3152
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_1

    .line 3153
    aget v4, p1, v0

    shr-int/lit8 v4, v4, 0x3

    .line 3154
    aget v5, p1, v0

    and-int/lit8 v5, v5, 0x7

    .line 3155
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/u;->a:[B

    add-int/2addr v4, v3

    aget-byte v4, v6, v4

    shl-int v5, v2, v5

    and-int/2addr v4, v5

    if-nez v4, :cond_0

    .line 3159
    :goto_1
    return v1

    .line 3152
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 3159
    goto :goto_1
.end method
