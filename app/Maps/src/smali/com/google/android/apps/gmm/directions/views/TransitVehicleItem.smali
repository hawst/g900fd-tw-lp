.class public Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/i/a/a;

.field public b:Lcom/google/android/apps/gmm/map/i/a/c;

.field c:Z

.field public d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public e:I

.field public f:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public g:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public h:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->c:Z

    .line 82
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a:Lcom/google/android/apps/gmm/map/i/a/a;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->aq:[I

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 86
    sget v1, Lcom/google/android/apps/gmm/n;->ar:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 87
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 89
    iput v1, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->e:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    .line 90
    return-void
.end method


# virtual methods
.method public a()V
    .locals 14

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 224
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setCompoundDrawablePadding(I)V

    .line 225
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setText(Ljava/lang/CharSequence;)V

    .line 231
    :goto_1
    return-void

    .line 223
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 227
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setCompoundDrawablePadding(I)V

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->g:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->h:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->f:Ljava/lang/Integer;

    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->getTextSize()F

    move-result v4

    float-to-int v4, v4

    .line 228
    new-instance v5, Landroid/text/SpannableStringBuilder;

    invoke-direct {v5}, Landroid/text/SpannableStringBuilder;-><init>()V

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const/16 v7, 0x20

    invoke-virtual {v5, v7}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const/4 v3, 0x0

    const/4 v9, 0x0

    int-to-double v10, v4

    const-wide v12, 0x3fdaaaaaaaaaaaabL    # 0.4166666666666667

    mul-double/2addr v10, v12

    double-to-int v10, v10

    invoke-virtual {v8, v3, v9, v10, v4}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    new-instance v3, Landroid/text/style/ImageSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v8, v4}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    const/16 v4, 0x11

    invoke-virtual {v5, v3, v6, v7, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_4
    if-eqz v0, :cond_6

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_5

    const/16 v3, 0x20

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    :cond_5
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x20

    invoke-virtual {v5, v4}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    const/16 v0, 0x20

    invoke-virtual {v5, v0}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    new-instance v4, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v4, v1}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/16 v1, 0x11

    invoke-virtual {v5, v4, v3, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    if-eqz v2, :cond_6

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v2, 0x11

    invoke-virtual {v5, v1, v3, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    :cond_6
    :goto_2
    invoke-virtual {p0, v5}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v5, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2
.end method
