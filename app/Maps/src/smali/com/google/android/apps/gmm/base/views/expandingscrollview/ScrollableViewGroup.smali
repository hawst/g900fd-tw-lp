.class public abstract Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;
.super Landroid/view/ViewGroup;
.source "PG"


# static fields
.field private static final j:Landroid/view/animation/Interpolator;


# instance fields
.field private a:[F

.field private b:I

.field private c:I

.field private d:I

.field private e:F

.field private f:Landroid/view/VelocityTracker;

.field private g:Z

.field private h:Z

.field private i:Z

.field final r:[I

.field public s:Z

.field t:I

.field public u:Landroid/widget/Scroller;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/v;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/v;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->j:Landroid/view/animation/Interpolator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 35
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    .line 36
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    .line 42
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    .line 44
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->g:Z

    .line 45
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    .line 46
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->i:Z

    .line 47
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->t:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 73
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->setFocusable(Z)V

    .line 75
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b:I

    .line 77
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->d:I

    .line 78
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->c:I

    .line 79
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->j:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    .line 61
    return-void

    .line 35
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 36
    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    .line 36
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    .line 42
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    .line 44
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->g:Z

    .line 45
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    .line 46
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->i:Z

    .line 47
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->t:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 73
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->setFocusable(Z)V

    .line 75
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b:I

    .line 77
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->d:I

    .line 78
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->c:I

    .line 79
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->j:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    .line 65
    return-void

    .line 35
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 36
    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 68
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    .line 36
    new-array v0, v2, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    .line 42
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    .line 44
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->g:Z

    .line 45
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    .line 46
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->i:Z

    .line 47
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->t:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 73
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->setFocusable(Z)V

    .line 75
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 76
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b:I

    .line 77
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->d:I

    .line 78
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->c:I

    .line 79
    new-instance v1, Landroid/widget/Scroller;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->j:Landroid/view/animation/Interpolator;

    invoke-direct {v1, v0, v2}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    .line 69
    return-void

    .line 35
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 36
    :array_1
    .array-data 4
        -0x7fffffff
        0x7fffffff
    .end array-data
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 171
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b()V

    .line 173
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->g:Z

    .line 175
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    .line 176
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 178
    return-void
.end method

.method private a(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 88
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    if-eqz v0, :cond_1

    .line 89
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    .line 117
    :cond_0
    :goto_0
    return v1

    .line 93
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 97
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    aput v3, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    aput v3, v0, v2

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    .line 99
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a()V

    move v1, v2

    .line 100
    goto :goto_0

    .line 102
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    goto :goto_0

    .line 109
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    aget v3, v3, v1

    sub-float/2addr v0, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    aget v4, v4, v2

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b:I

    int-to-float v4, v4

    cmpl-float v4, v0, v4

    if-gtz v4, :cond_3

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_5

    :cond_3
    move v0, v2

    :goto_1
    iget v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b:I

    int-to-float v4, v4

    cmpl-float v4, v3, v4

    if-gtz v4, :cond_4

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->b:I

    neg-int v4, v4

    int-to-float v4, v4

    cmpg-float v3, v3, v4

    if-gez v3, :cond_6

    :cond_4
    move v3, v2

    :goto_2
    if-eqz v3, :cond_7

    if-nez v0, :cond_7

    move v0, v2

    :goto_3
    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    aput v3, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    aput v1, v0, v2

    .line 111
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a()V

    move v1, v2

    .line 112
    goto :goto_0

    :cond_5
    move v0, v1

    .line 109
    goto :goto_1

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_3

    .line 93
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method a(I)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aget v0, v0, v1

    if-ge p1, v0, :cond_1

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aget p1, v0, v1

    .line 286
    :cond_0
    :goto_0
    return p1

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aget v0, v0, v2

    if-le p1, v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aget p1, v0, v2

    goto :goto_0
.end method

.method protected a(F)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public final a(IZI)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 298
    if-eqz p2, :cond_0

    .line 299
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(I)I

    move-result p1

    .line 301
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v0

    sub-int v4, p1, v0

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v2

    move v3, v1

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 303
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->invalidate()V

    .line 304
    return-void
.end method

.method b()V
    .locals 0

    .prologue
    .line 307
    return-void
.end method

.method c()V
    .locals 0

    .prologue
    .line 310
    return-void
.end method

.method public computeScroll()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrY()I

    move-result v0

    .line 245
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->i:Z

    .line 246
    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->scrollTo(II)V

    .line 247
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->i:Z

    .line 248
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->invalidate()V

    .line 250
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    .line 251
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(F)V

    .line 252
    iput v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    .line 255
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v1}, Landroid/widget/Scroller;->getFinalY()I

    move-result v1

    .line 256
    if-ne v0, v1, :cond_1

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 258
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->g:Z

    if-eqz v0, :cond_1

    .line 259
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->g:Z

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->c()V

    .line 264
    :cond_1
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x1

    const/4 v3, 0x0

    .line 122
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    if-nez v1, :cond_0

    .line 125
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    .line 127
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    invoke-virtual {v1, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 129
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    if-nez v1, :cond_3

    .line 130
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 155
    :cond_1
    :goto_0
    return v9

    .line 134
    :cond_2
    if-ne v0, v9, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    if-eqz v0, :cond_1

    .line 135
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    .line 136
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->performClick()Z

    move-result v9

    goto :goto_0

    .line 141
    :cond_3
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 150
    :pswitch_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    move v0, v9

    :goto_1
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->s:Z

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->c:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->d:I

    int-to-float v1, v1

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_4

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->d:I

    neg-int v1, v1

    int-to-float v1, v1

    cmpg-float v1, v0, v1

    if-gez v1, :cond_7

    :cond_4
    neg-float v4, v0

    iput v4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->e:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollX()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    float-to-int v4, v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aget v7, v5, v3

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->r:[I

    aget v8, v5, v9

    move v5, v3

    move v6, v3

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->invalidate()V

    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->f:Landroid/view/VelocityTracker;

    .line 151
    :cond_5
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    goto :goto_0

    .line 143
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    aget v0, v0, v9

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    aput v2, v1, v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    aput v2, v1, v9

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a:[F

    aget v1, v1, v9

    sub-float/2addr v0, v1

    .line 144
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->getScrollY()I

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(I)I

    move-result v0

    invoke-virtual {p0, v3, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->scrollTo(II)V

    .line 145
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->h:Z

    goto/16 :goto_0

    :cond_6
    move v0, v3

    .line 150
    goto/16 :goto_1

    :cond_7
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(F)V

    goto :goto_2

    :cond_8
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->a(F)V

    goto :goto_2

    .line 141
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public scrollTo(II)V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->scrollTo(II)V

    .line 228
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->i:Z

    if-nez v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->u:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->abortAnimation()V

    .line 231
    :cond_0
    return-void
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 161
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ScrollableViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    .line 162
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v0

    return v0
.end method
