.class public Lcom/google/android/apps/gmm/map/internal/c/co;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field public b:I

.field c:B

.field d:I

.field public e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/m;",
            ">;"
        }
    .end annotation
.end field

.field f:Lcom/google/android/apps/gmm/map/internal/c/bs;

.field public g:J

.field h:[Ljava/lang/String;

.field public i:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public j:[Lcom/google/android/apps/gmm/map/internal/c/cj;

.field public k:J

.field l:I

.field m:[Lcom/google/android/apps/gmm/map/internal/c/i;

.field n:Z

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 143
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/co;->d:I

    .line 146
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/c/co;->g:J

    .line 149
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/co;->o:I

    .line 150
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/co;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 152
    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/c/co;->k:J

    .line 153
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/c/co;->l:I

    .line 155
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/c/co;->n:Z

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/cm;
    .locals 21

    .prologue
    .line 252
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->f:Lcom/google/android/apps/gmm/map/internal/c/bs;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->b:I

    move-object/from16 v0, p0

    iget-byte v6, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->c:B

    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->d:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->h:[Ljava/lang/String;

    if-nez v8, :cond_0

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/String;

    :goto_0
    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->o:I

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->e:Ljava/util/List;

    if-nez v11, :cond_1

    .line 261
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->j:[Lcom/google/android/apps/gmm/map/internal/c/cj;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->g:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->k:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->l:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->m:[Lcom/google/android/apps/gmm/map/internal/c/i;

    move-object/from16 v19, v0

    if-nez v19, :cond_2

    const/16 v19, 0x0

    move/from16 v0, v19

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/i;

    move-object/from16 v19, v0

    :goto_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->n:Z

    move/from16 v20, v0

    invoke-direct/range {v2 .. v20}, Lcom/google/android/apps/gmm/map/internal/c/cm;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bp;IBI[Ljava/lang/String;[Ljava/lang/String;ILjava/util/List;Lcom/google/android/apps/gmm/map/b/a/ai;[Lcom/google/android/apps/gmm/map/internal/c/cj;JJI[Lcom/google/android/apps/gmm/map/internal/c/i;Z)V

    return-object v2

    .line 252
    :cond_0
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->h:[Ljava/lang/String;

    goto :goto_0

    .line 261
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->e:Ljava/util/List;

    goto :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/co;->m:[Lcom/google/android/apps/gmm/map/internal/c/i;

    move-object/from16 v19, v0

    goto :goto_2
.end method
