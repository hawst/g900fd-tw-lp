.class Lcom/google/android/apps/gmm/base/k/aj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/k/ag;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ai;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/aj;->a:Ljava/util/List;

    .line 175
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Z)Landroid/graphics/drawable/Drawable;
    .locals 7

    .prologue
    .line 179
    new-instance v2, Lcom/google/android/apps/gmm/base/k/ah;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/k/ah;-><init>()V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/aj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/k/ai;

    .line 181
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/k/ai;->b:Z

    if-ne v1, p2, :cond_0

    .line 182
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v4, v0, Lcom/google/android/apps/gmm/base/k/ai;->c:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 188
    const/4 v1, 0x0

    .line 189
    iget-object v5, v0, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    if-eqz v5, :cond_1

    .line 190
    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    invoke-interface {v5, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v5

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/k/ai;->e:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v5, v6}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    .line 193
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/k/ai;->a:[I

    invoke-virtual {v2, v0, v4, v1}, Lcom/google/android/apps/gmm/base/k/ah;->a([ILandroid/graphics/drawable/Drawable;Landroid/graphics/ColorFilter;)V

    goto :goto_0

    .line 196
    :cond_2
    return-object v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 201
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/k/aj;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/base/k/aj;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/k/aj;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/aj;->a:Ljava/util/List;

    .line 202
    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/aj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method
