.class public final enum Lcom/google/android/apps/gmm/map/j/aa;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/j/s;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/j/aa;",
        ">;",
        "Lcom/google/android/apps/gmm/map/j/s;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/j/aa;

.field private static final synthetic b:[Lcom/google/android/apps/gmm/map/j/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/j/aa;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/j/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/j/aa;->a:Lcom/google/android/apps/gmm/map/j/aa;

    .line 19
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/j/aa;

    sget-object v1, Lcom/google/android/apps/gmm/map/j/aa;->a:Lcom/google/android/apps/gmm/map/j/aa;

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/apps/gmm/map/j/aa;->b:[Lcom/google/android/apps/gmm/map/j/aa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/j/aa;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/gmm/map/j/aa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/j/aa;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/j/aa;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/apps/gmm/map/j/aa;->b:[Lcom/google/android/apps/gmm/map/j/aa;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/j/aa;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/j/aa;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)Lcom/google/android/apps/gmm/map/j/q;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/apps/gmm/map/j/z;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/j/z;-><init>(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/j/r;)V

    return-object v0
.end method
