.class public Lcom/google/android/apps/gmm/startpage/f/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/e/l;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Lcom/google/r/b/a/agd;

.field private final c:Lcom/google/android/apps/gmm/map/r/a/f;

.field private final d:Ljava/lang/CharSequence;

.field private final e:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/map/r/a/f;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/t;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 60
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/f/t;->b:Lcom/google/r/b/a/agd;

    .line 61
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/f/t;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 62
    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/f/t;->d:Ljava/lang/CharSequence;

    .line 63
    iput-object p5, p0, Lcom/google/android/apps/gmm/startpage/f/t;->e:Ljava/lang/CharSequence;

    .line 64
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/t;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/t;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/t;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v0, :cond_0

    .line 235
    :goto_0
    return-object v5

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/t;->b:Lcom/google/r/b/a/agd;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/t;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/f/a;->a(Lcom/google/r/b/a/agd;Landroid/content/Context;)Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v0

    .line 230
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/t;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v1

    .line 231
    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/f/t;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 232
    invoke-interface {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/android/apps/gmm/map/r/a/f;Z)Lcom/google/android/apps/gmm/directions/a/c;

    move-result-object v0

    .line 233
    sget-object v2, Lcom/google/android/apps/gmm/directions/a/g;->b:Lcom/google/android/apps/gmm/directions/a/g;

    invoke-interface {v1, v0, v2, v4}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/android/apps/gmm/directions/a/c;Lcom/google/android/apps/gmm/directions/a/g;Z)V

    goto :goto_0
.end method
