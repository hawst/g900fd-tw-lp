.class public Lcom/google/android/apps/gmm/directions/f/a/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/r/a/ae;

.field public final b:Lcom/google/android/apps/gmm/directions/f/a/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final c:Z

.field public final d:Z

.field public final e:[Lcom/google/android/apps/gmm/map/r/a/s;

.field public final f:Z

.field public final g:Z

.field public final h:Lcom/google/android/apps/gmm/map/r/a/ag;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final i:Lcom/google/android/apps/gmm/map/b/a/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final j:Lcom/google/android/apps/gmm/map/ag;

.field public final k:Z

.field public final l:Lcom/google/android/apps/gmm/map/r/a/v;

.field public final m:Lcom/google/android/apps/gmm/map/o/b/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    const-string v1, "routes"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 42
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->c:Lcom/google/android/apps/gmm/directions/f/a/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->b:Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 43
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->d:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->c:Z

    .line 44
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->d:Z

    .line 45
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->b:[Lcom/google/android/apps/gmm/map/r/a/s;

    const-string v1, "textureTypes"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/s;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->e:[Lcom/google/android/apps/gmm/map/r/a/s;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->e:[Lcom/google/android/apps/gmm/map/r/a/s;

    array-length v1, v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 47
    :cond_3
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->f:Z

    .line 48
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->g:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->g:Z

    .line 49
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 50
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 51
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->j:Lcom/google/android/apps/gmm/map/ag;

    const-string v1, "pinType"

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    check-cast v0, Lcom/google/android/apps/gmm/map/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->j:Lcom/google/android/apps/gmm/map/ag;

    .line 52
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->k:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->k:Z

    .line 54
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    const-string v1, "calloutsDisplayMode"

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/v;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 56
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/a/b;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    const-string v1, "calloutStyleClass"

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    check-cast v0, Lcom/google/android/apps/gmm/map/o/b/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 156
    instance-of v2, p1, Lcom/google/android/apps/gmm/directions/f/a/a;

    if-nez v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return v0

    .line 160
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 162
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 163
    goto :goto_0

    .line 166
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_3
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->b:Lcom/google/android/apps/gmm/directions/f/a/c;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->b:Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 167
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_4
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->c:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->c:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->d:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->d:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->e:[Lcom/google/android/apps/gmm/map/r/a/s;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->e:[Lcom/google/android/apps/gmm/map/r/a/s;

    .line 170
    invoke-static {v2, v3}, Ljava/util/Arrays;->deepEquals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->f:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->f:Z

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->g:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->g:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 173
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_5
    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 174
    if-eq v2, v3, :cond_6

    if-eqz v2, :cond_a

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_6
    move v2, v1

    :goto_4
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->j:Lcom/google/android/apps/gmm/map/ag;

    iget-object v3, p1, Lcom/google/android/apps/gmm/directions/f/a/a;->j:Lcom/google/android/apps/gmm/map/ag;

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_7
    move v2, v0

    .line 166
    goto :goto_1

    :cond_8
    move v2, v0

    .line 167
    goto :goto_2

    :cond_9
    move v2, v0

    .line 173
    goto :goto_3

    :cond_a
    move v2, v0

    .line 174
    goto :goto_4
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 180
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->b:Lcom/google/android/apps/gmm/directions/f/a/c;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->c:Z

    .line 182
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->d:Z

    .line 183
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->e:[Lcom/google/android/apps/gmm/map/r/a/s;

    .line 184
    invoke-static {v2}, Ljava/util/Arrays;->deepHashCode([Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->f:Z

    .line 185
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->g:Z

    .line 186
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/f/a/a;->j:Lcom/google/android/apps/gmm/map/ag;

    aput-object v2, v0, v1

    .line 180
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
