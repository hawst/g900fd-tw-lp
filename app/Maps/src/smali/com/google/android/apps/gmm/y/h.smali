.class public final Lcom/google/android/apps/gmm/y/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/y/h;",
        ">;"
    }
.end annotation


# static fields
.field static final a:Ljava/util/TimeZone;


# instance fields
.field final b:Lcom/google/android/apps/gmm/y/g;

.field final c:Ljava/util/Calendar;

.field d:Ljava/util/Calendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/y/h;->a:Ljava/util/TimeZone;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/y/g;Lcom/google/d/a/a/pj;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    .line 40
    iget-object v0, p2, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ox;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/d/a/a/ox;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    .line 41
    iget-object v0, p2, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ox;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/d/a/a/ox;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    .line 42
    return-void
.end method

.method private a(Lcom/google/d/a/a/ox;)Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 49
    iget v0, p1, Lcom/google/d/a/a/ox;->d:I

    rem-int/lit8 v0, v0, 0x18

    .line 50
    iget v1, p1, Lcom/google/d/a/a/ox;->c:I

    .line 52
    sget-object v2, Lcom/google/android/apps/gmm/y/h;->a:Ljava/util/TimeZone;

    invoke-static {v2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Calendar;->clear()V

    const/16 v3, 0xb

    invoke-virtual {v2, v3, v0}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    invoke-virtual {v2, v0, v1}, Ljava/util/Calendar;->set(II)V

    return-object v2
.end method

.method public static a(Lcom/google/d/a/a/pj;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/d/a/a/pj;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/y/h;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-static {}, Lcom/google/android/apps/gmm/y/g;->values()[Lcom/google/android/apps/gmm/y/g;

    move-result-object v2

    .line 87
    array-length v3, v2

    new-instance v4, Ljava/util/ArrayList;

    if-ltz v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const-wide/16 v6, 0x5

    int-to-long v8, v3

    add-long/2addr v6, v8

    div-int/lit8 v0, v3, 0xa

    int-to-long v8, v0

    add-long/2addr v6, v8

    const-wide/32 v8, 0x7fffffff

    cmp-long v0, v6, v8

    if-lez v0, :cond_2

    const v0, 0x7fffffff

    :goto_1
    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 88
    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_4

    aget-object v1, v2, v0

    .line 89
    new-instance v5, Lcom/google/android/apps/gmm/y/h;

    invoke-direct {v5, v1, p0}, Lcom/google/android/apps/gmm/y/h;-><init>(Lcom/google/android/apps/gmm/y/g;Lcom/google/d/a/a/pj;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 87
    :cond_2
    const-wide/32 v8, -0x80000000

    cmp-long v0, v6, v8

    if-gez v0, :cond_3

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_3
    long-to-int v0, v6

    goto :goto_1

    .line 91
    :cond_4
    return-object v4
.end method

.method public static a(Lcom/google/d/a/a/pj;Lcom/google/d/a/a/pj;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/d/a/a/pj;",
            "Lcom/google/d/a/a/pj;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/y/h;",
            ">;"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ox;

    iget-object v1, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/d/a/a/ox;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget v2, v0, Lcom/google/d/a/a/ox;->e:I

    iget v0, v1, Lcom/google/d/a/a/ox;->e:I

    if-ne v2, v0, :cond_1

    rem-int/lit8 v0, v2, 0x7

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/g;->a(I)Lcom/google/android/apps/gmm/y/g;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    new-instance v2, Ljava/util/ArrayList;

    if-ltz v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 73
    :cond_1
    if-ge v0, v2, :cond_2

    add-int/lit8 v0, v0, 0x7

    :cond_2
    move v1, v2

    :goto_1
    if-ge v1, v0, :cond_0

    rem-int/lit8 v2, v1, 0x7

    invoke-static {v2}, Lcom/google/android/apps/gmm/y/g;->a(I)Lcom/google/android/apps/gmm/y/g;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 74
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    const-wide/16 v4, 0x5

    int-to-long v6, v1

    add-long/2addr v4, v6

    div-int/lit8 v0, v1, 0xa

    int-to-long v0, v0

    add-long/2addr v0, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v4, v0, v4

    if-lez v4, :cond_5

    const v0, 0x7fffffff

    :goto_2
    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 75
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/g;

    .line 76
    new-instance v3, Lcom/google/android/apps/gmm/y/h;

    invoke-direct {v3, v0, p1}, Lcom/google/android/apps/gmm/y/h;-><init>(Lcom/google/android/apps/gmm/y/g;Lcom/google/d/a/a/pj;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 74
    :cond_5
    const-wide/32 v4, -0x80000000

    cmp-long v4, v0, v4

    if-gez v4, :cond_6

    const/high16 v0, -0x80000000

    goto :goto_2

    :cond_6
    long-to-int v0, v0

    goto :goto_2

    .line 78
    :cond_7
    return-object v2
.end method

.method public static b(Lcom/google/d/a/a/pj;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/d/a/a/pj;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/y/h;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 99
    invoke-static {}, Lcom/google/d/a/a/pj;->newBuilder()Lcom/google/d/a/a/pl;

    move-result-object v0

    sget-object v1, Lcom/google/d/a/a/pm;->b:Lcom/google/d/a/a/pm;

    .line 100
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/d/a/a/pl;->a:I

    iget v1, v1, Lcom/google/d/a/a/pm;->c:I

    iput v1, v0, Lcom/google/d/a/a/pl;->b:I

    .line 101
    invoke-static {}, Lcom/google/d/a/a/ox;->newBuilder()Lcom/google/d/a/a/oz;

    move-result-object v1

    .line 102
    iget v2, v1, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/d/a/a/oz;->a:I

    iput v4, v1, Lcom/google/d/a/a/oz;->c:I

    .line 103
    iget v2, v1, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/d/a/a/oz;->a:I

    iput v4, v1, Lcom/google/d/a/a/oz;->b:I

    .line 101
    iget-object v2, v0, Lcom/google/d/a/a/pl;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/d/a/a/oz;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v0, Lcom/google/d/a/a/pl;->a:I

    .line 104
    invoke-static {}, Lcom/google/d/a/a/ox;->newBuilder()Lcom/google/d/a/a/oz;

    move-result-object v1

    .line 105
    iget v2, v1, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/d/a/a/oz;->a:I

    iput v4, v1, Lcom/google/d/a/a/oz;->c:I

    .line 106
    iget v2, v1, Lcom/google/d/a/a/oz;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/d/a/a/oz;->a:I

    iput v4, v1, Lcom/google/d/a/a/oz;->b:I

    .line 104
    iget-object v2, v0, Lcom/google/d/a/a/pl;->d:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/d/a/a/oz;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/d/a/a/pl;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v0, Lcom/google/d/a/a/pl;->a:I

    .line 107
    invoke-virtual {v0}, Lcom/google/d/a/a/pl;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/pj;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/y/h;->a(Lcom/google/d/a/a/pj;Lcom/google/d/a/a/pj;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/16 v2, 0xb

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    .line 146
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    .line 147
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/y/h;)Z
    .locals 4

    .prologue
    const/16 v3, 0xc

    const/16 v2, 0xb

    .line 160
    iget-object v0, p1, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    .line 161
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    .line 162
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    .line 163
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    .line 164
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/y/g;->a()Lcom/google/android/apps/gmm/y/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/y/g;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    .line 165
    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    if-gt v0, v3, :cond_0

    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/y/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/y/h;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 167
    iget-object v0, p1, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    iput-object v0, p0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    .line 168
    const/4 v0, 0x1

    .line 170
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 28
    check-cast p1, Lcom/google/android/apps/gmm/y/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    iget-object v1, p1, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->compareTo(Ljava/util/Calendar;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/y/h;->b:Lcom/google/android/apps/gmm/y/g;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/y/g;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 213
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/text/DateFormat;->getTimeInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    .line 214
    sget-object v1, Lcom/google/android/apps/gmm/y/h;->a:Ljava/util/TimeZone;

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 215
    const-string v1, "%s \u2013 %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/y/h;->c:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/y/h;->d:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
