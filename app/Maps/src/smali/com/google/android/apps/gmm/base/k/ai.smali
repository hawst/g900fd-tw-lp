.class Lcom/google/android/apps/gmm/base/k/ai;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[I

.field public final b:Z

.field public final c:I

.field public final d:Lcom/google/android/libraries/curvular/aq;

.field public final e:Landroid/graphics/PorterDuff$Mode;

.field public final f:Landroid/graphics/drawable/Drawable;

.field public final g:I


# direct methods
.method public constructor <init>([IZILcom/google/android/libraries/curvular/aq;Landroid/graphics/PorterDuff$Mode;I)V
    .locals 1

    .prologue
    .line 225
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 226
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/ai;->a:[I

    .line 227
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/base/k/ai;->b:Z

    .line 228
    iput p3, p0, Lcom/google/android/apps/gmm/base/k/ai;->c:I

    .line 229
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    .line 230
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/k/ai;->e:Landroid/graphics/PorterDuff$Mode;

    .line 231
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/ai;->f:Landroid/graphics/drawable/Drawable;

    .line 232
    iput p6, p0, Lcom/google/android/apps/gmm/base/k/ai;->g:I

    .line 233
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 247
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/k/ai;

    if-nez v2, :cond_1

    .line 259
    :cond_0
    :goto_0
    return v0

    .line 251
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/base/k/ai;

    .line 252
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->a:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/ai;->a:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->b:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/base/k/ai;->b:Z

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/ai;->c:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    .line 255
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->e:Landroid/graphics/PorterDuff$Mode;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/ai;->e:Landroid/graphics/PorterDuff$Mode;

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 259
    goto :goto_0

    :cond_3
    move v2, v0

    .line 255
    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 264
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->a:[I

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ai;->e:Landroid/graphics/PorterDuff$Mode;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
