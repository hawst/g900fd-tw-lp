.class public Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;
.super Landroid/widget/ImageView;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/directions/views/d;

.field public b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->b:I

    .line 25
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->a:Lcom/google/android/apps/gmm/directions/views/d;

    if-nez v0, :cond_0

    .line 39
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->a:Lcom/google/android/apps/gmm/directions/views/d;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->b:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/views/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/directions/views/d;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
