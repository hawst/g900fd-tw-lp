.class public Lcom/google/android/apps/gmm/map/ah;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/android/apps/gmm/map/ag;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/google/android/apps/gmm/map/b/c;

.field private final c:Landroid/content/res/Resources;

.field private final d:Lcom/google/android/apps/gmm/map/x;

.field private e:Lcom/google/android/apps/gmm/map/b/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:Lcom/google/android/apps/gmm/map/b/a/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private g:Lcom/google/android/apps/gmm/map/ag;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private h:Lcom/google/android/apps/gmm/map/ai;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/apps/gmm/map/b/d;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x41100000    # 9.0f

    .line 36
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lcom/google/android/apps/gmm/map/ag;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 40
    sput-object v0, Lcom/google/android/apps/gmm/map/ah;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->d:Lcom/google/android/apps/gmm/map/ag;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    sget-object v0, Lcom/google/android/apps/gmm/map/ah;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->e:Lcom/google/android/apps/gmm/map/ag;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    sget-object v0, Lcom/google/android/apps/gmm/map/ah;->a:Ljava/util/EnumMap;

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->f:Lcom/google/android/apps/gmm/map/ag;

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/c;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/x;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->i:Landroid/util/SparseArray;

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/ah;->b:Lcom/google/android/apps/gmm/map/b/c;

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/ah;->c:Landroid/content/res/Resources;

    .line 85
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/ah;->d:Lcom/google/android/apps/gmm/map/x;

    .line 86
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/ag;)Lcom/google/android/apps/gmm/map/b/d;
    .locals 4

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->d:Lcom/google/android/apps/gmm/map/x;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/x;->a(Lcom/google/android/apps/gmm/map/ag;)I

    move-result v1

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->i:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/d;

    .line 190
    if-nez v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->b:Lcom/google/android/apps/gmm/map/b/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ah;->c:Landroid/content/res/Resources;

    const/4 v3, 0x3

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/apps/gmm/map/b/c;->a(Landroid/content/res/Resources;II)Lcom/google/android/apps/gmm/map/b/d;

    move-result-object v0

    .line 193
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ah;->i:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 195
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->g:Lcom/google/android/apps/gmm/map/ag;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    .line 178
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/map/ag;)Lcom/google/android/apps/gmm/map/b/d;

    move-result-object v0

    .line 179
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/b/d;->a()I

    move-result v1

    .line 180
    neg-int v2, v1

    div-int/lit8 v2, v2, 0x2

    .line 181
    add-int/2addr v1, v2

    .line 182
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/b/d;->b()I

    move-result v0

    neg-int v0, v0

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v0, v1, v3}, Landroid/graphics/Rect;->set(IIII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    monitor-exit p0

    return-object p1

    .line 177
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->g:Lcom/google/android/apps/gmm/map/ag;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 149
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->f:Lcom/google/android/apps/gmm/map/b/a/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Lcom/google/android/apps/gmm/map/f/o;Z)Lcom/google/android/apps/gmm/map/b/b;
    .locals 4

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->f:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->g:Lcom/google/android/apps/gmm/map/ag;

    if-ne p3, v0, :cond_0

    .line 108
    const/4 p5, 0x0

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ah;->b()V

    .line 115
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/ah;->f:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 116
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/ah;->g:Lcom/google/android/apps/gmm/map/ag;

    .line 118
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/map/ag;)Lcom/google/android/apps/gmm/map/b/d;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->b:Lcom/google/android/apps/gmm/map/b/c;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/b/c;->a(Lcom/google/android/apps/gmm/map/b/d;)Lcom/google/android/apps/gmm/map/b/g;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    invoke-interface {v1, p2}, Lcom/google/android/apps/gmm/map/b/g;->a(Lcom/google/android/apps/gmm/map/b/a/q;)V

    .line 123
    if-eqz p5, :cond_2

    .line 125
    new-instance v1, Lcom/google/android/apps/gmm/map/t/al;

    new-instance v2, Lcom/google/android/apps/gmm/map/t/a/a;

    const/4 v3, 0x0

    .line 126
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/b/d;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/map/t/a/a;-><init>(FF)V

    invoke-direct {v1, v2, p1}, Lcom/google/android/apps/gmm/map/t/al;-><init>(Lcom/google/android/apps/gmm/v/c;Lcom/google/android/apps/gmm/v/ad;)V

    .line 127
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/t/al;->a(Lcom/google/android/apps/gmm/map/b/i;)V

    .line 133
    :goto_0
    sget-object v0, Lcom/google/android/apps/gmm/map/ah;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p3}, Ljava/util/EnumMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->h:Lcom/google/android/apps/gmm/map/ai;

    if-nez v0, :cond_1

    if-eqz p4, :cond_1

    .line 135
    new-instance v0, Lcom/google/android/apps/gmm/map/ai;

    invoke-direct {v0, p0, p4}, Lcom/google/android/apps/gmm/map/ai;-><init>(Lcom/google/android/apps/gmm/map/ah;Lcom/google/android/apps/gmm/map/f/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->h:Lcom/google/android/apps/gmm/map/ai;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->h:Lcom/google/android/apps/gmm/map/ai;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->b:Lcom/google/android/apps/gmm/map/b/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/b/c;->a(Lcom/google/android/apps/gmm/map/b/b;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 130
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/b/d;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/b/g;->a(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/b/a;)V
    .locals 1

    .prologue
    .line 157
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    if-ne v0, p1, :cond_0

    .line 158
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/ah;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    :cond_0
    monitor-exit p0

    return-void

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 2

    .prologue
    .line 199
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/map/ah;->a:Ljava/util/EnumMap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->g:Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 203
    if-eqz v0, :cond_0

    .line 204
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/b/g;->a(F)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 207
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->g:Lcom/google/android/apps/gmm/map/ag;

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/map/ag;)Lcom/google/android/apps/gmm/map/b/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/b/d;->a()I

    move-result v1

    int-to-float v1, v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/b/g;->a(F)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 163
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->b:Lcom/google/android/apps/gmm/map/b/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/b/c;->b(Lcom/google/android/apps/gmm/map/b/b;)V

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->e:Lcom/google/android/apps/gmm/map/b/g;

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->f:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ah;->g:Lcom/google/android/apps/gmm/map/ag;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    :cond_0
    monitor-exit p0

    return-void

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
