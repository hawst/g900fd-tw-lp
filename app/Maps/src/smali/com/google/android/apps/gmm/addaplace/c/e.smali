.class Lcom/google/android/apps/gmm/addaplace/c/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/n;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/addaplace/c/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/addaplace/c/b;)V
    .locals 0

    .prologue
    .line 393
    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->c:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 402
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->c:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->lh:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 407
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    invoke-static {v0}, Lcom/google/android/apps/gmm/addaplace/c/b;->a(Lcom/google/android/apps/gmm/addaplace/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    .line 413
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 412
    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 413
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 418
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 424
    :cond_0
    :goto_0
    return-object v1

    .line 422
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 436
    :goto_0
    return-object v4

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->h:Z

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eq v2, v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->h:Z

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->b:Lcom/google/android/apps/gmm/addaplace/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    .line 435
    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/addaplace/a;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    goto :goto_0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/e;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x0

    return-object v0
.end method
