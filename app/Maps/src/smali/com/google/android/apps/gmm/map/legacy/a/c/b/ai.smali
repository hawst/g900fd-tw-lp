.class public final enum Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum d:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum g:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field public static final enum i:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

.field private static final synthetic j:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "AT_CENTER"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 142
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "ABOVE_CENTER"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "RIGHT_OF_CENTER"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 144
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "BELOW_CENTER"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->d:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "LEFT_OF_CENTER"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 146
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "BOTTOM_RIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 147
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "BOTTOM_LEFT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->g:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 148
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "TOP_RIGHT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    const-string v1, "TOP_LEFT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->i:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 140
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->d:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->g:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->i:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->j:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 140
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;
    .locals 2

    .prologue
    .line 152
    packed-switch p0, :pswitch_data_0

    .line 163
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown position"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    .line 161
    :goto_0
    return-object v0

    .line 154
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->e:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 155
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 156
    :pswitch_4
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 157
    :pswitch_5
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->i:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 158
    :pswitch_6
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 159
    :pswitch_7
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->d:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 160
    :pswitch_8
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->g:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 161
    :pswitch_9
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    goto :goto_0

    .line 152
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;
    .locals 1

    .prologue
    .line 140
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->j:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/ai;

    return-object v0
.end method
