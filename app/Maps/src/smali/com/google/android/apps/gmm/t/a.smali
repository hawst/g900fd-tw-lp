.class public Lcom/google/android/apps/gmm/t/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/t/a/a;


# instance fields
.field a:Z

.field b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/fragments/OobFragment;",
            ">;"
        }
    .end annotation
.end field

.field private c:Z

.field private f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/t/a;->a:Z

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/t/a;->c:Z

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/t/a;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/gmm/t/a;->e()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/t/a;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/t/a;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/t/a;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/t/a;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 174
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/t/a;->c:Z

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/h/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/h/a;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 176
    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 3

    .prologue
    .line 125
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/t/a;->a:Z

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/t/a;->b:Ljava/util/List;

    if-nez v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/t/a;->b:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/t/a;->b:Ljava/util/List;

    invoke-static {}, Lcom/google/android/apps/gmm/terms/TermsFragment;->i()Lcom/google/android/apps/gmm/terms/TermsFragment;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/login/LoginPromoFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/t/a;->b:Ljava/util/List;

    invoke-static {}, Lcom/google/android/apps/gmm/login/LoginPromoFragment;->i()Lcom/google/android/apps/gmm/login/LoginPromoFragment;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/t/a;->b:Ljava/util/List;

    invoke-static {}, Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;->i()Lcom/google/android/apps/gmm/ulr/UlrPromoFragment;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Runnable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/gmm/t/a;->f:Ljava/lang/Runnable;

    .line 62
    iget-object v1, p0, Lcom/google/android/apps/gmm/t/a;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/gmm/t/a;->e()V

    .line 86
    :goto_0
    return-void

    .line 66
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/t/a;->c:Z

    .line 69
    new-instance v1, Lcom/google/android/apps/gmm/t/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/t/b;-><init>(Lcom/google/android/apps/gmm/t/a;Ljava/lang/Runnable;)V

    .line 85
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/t/a;->a:Z

    .line 137
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 98
    new-instance v1, Lcom/google/android/apps/gmm/t/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/t/c;-><init>(Lcom/google/android/apps/gmm/t/a;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 116
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/t/a;->c:Z

    return v0
.end method
