.class public Lcom/google/android/apps/gmm/navigation/navui/r;
.super Lcom/google/android/apps/gmm/navigation/navui/f;
.source "PG"


# instance fields
.field public final d:Lcom/google/android/apps/gmm/navigation/c/h;

.field public e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

.field private final f:Lcom/google/android/apps/gmm/navigation/commonui/g;

.field private final g:Lcom/google/android/apps/gmm/directions/a/d;

.field private final h:Lcom/google/android/apps/gmm/navigation/commonui/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/navigation/c/h;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/d;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/navigation/navui/f;-><init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;)V

    .line 135
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/s;-><init>(Lcom/google/android/apps/gmm/navigation/navui/r;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->h:Lcom/google/android/apps/gmm/navigation/commonui/h;

    .line 60
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->d:Lcom/google/android/apps/gmm/navigation/c/h;

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->h:Lcom/google/android/apps/gmm/navigation/commonui/h;

    invoke-direct {v0, p1, p5, v1}, Lcom/google/android/apps/gmm/navigation/commonui/g;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/navigation/commonui/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->f:Lcom/google/android/apps/gmm/navigation/commonui/g;

    .line 62
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->g:Lcom/google/android/apps/gmm/directions/a/d;

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/navigation/navui/r;F)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->b:Lcom/google/android/apps/gmm/navigation/navui/h;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/h;->a(Ljava/lang/Float;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->b:Lcom/google/android/apps/gmm/navigation/navui/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/h;->c()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/navigation/navui/r;Lcom/google/android/apps/gmm/map/j/w;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->a:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/g;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v4

    const-class v0, Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v0, 0x2

    const-class v1, Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/navigation/j/a/e;

    const-class v0, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {p1, v2, v0}, Lcom/google/android/apps/gmm/map/j/w;->a(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/gmm/navigation/j/a/e;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Z)V

    invoke-interface {v1, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->b:Lcom/google/android/apps/gmm/navigation/navui/h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/h;->a(Ljava/lang/Float;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/navigation/navui/r;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->d:Lcom/google/android/apps/gmm/navigation/c/h;

    invoke-virtual {v0, v2, v1, v2}, Lcom/google/android/apps/gmm/navigation/c/a;->a(ZZZ)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->b:Lcom/google/android/apps/gmm/navigation/navui/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/h;->c()V

    goto :goto_1
.end method

.method private static b(Lcom/google/android/apps/gmm/navigation/navui/b/a;)Lcom/google/maps/g/a/fw;
    .locals 2

    .prologue
    .line 126
    if-eqz p0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v0, :cond_1

    .line 130
    :cond_0
    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    goto :goto_0
.end method


# virtual methods
.method public final G_()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->f:Lcom/google/android/apps/gmm/navigation/commonui/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method public a()Lcom/google/android/apps/gmm/map/o/b/h;
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/google/android/apps/gmm/map/o/b/h;->a:Lcom/google/android/apps/gmm/map/o/b/h;

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)Lcom/google/android/apps/gmm/map/r/a/v;
    .locals 4

    .prologue
    .line 219
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/v;->b:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 223
    :goto_1
    const/4 v1, 0x0

    .line 224
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_1

    .line 225
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v3, v2

    if-eqz v2, :cond_1

    .line 226
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 229
    :cond_1
    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v1, v2, :cond_2

    .line 230
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/v;->a:Lcom/google/android/apps/gmm/map/r/a/v;

    .line 233
    :cond_2
    return-object v0

    .line 219
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/v;->c:Lcom/google/android/apps/gmm/map/r/a/v;

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 78
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_2

    .line 114
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 78
    goto :goto_0

    .line 81
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 82
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 88
    iget-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v4, v0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 89
    if-eqz p2, :cond_3

    iget-object v0, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_d

    move v0, v2

    :goto_2
    if-eqz v0, :cond_3

    .line 90
    iget-object v0, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v5, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-eq v4, v0, :cond_e

    :cond_3
    move v0, v2

    .line 92
    :goto_3
    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 93
    if-eqz p2, :cond_4

    iget-object v3, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v3, :cond_f

    move v3, v2

    :goto_4
    if-eqz v3, :cond_4

    .line 94
    iget-object v3, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Ljava/util/List;)Z

    move-result v3

    if-nez v3, :cond_10

    :cond_4
    move v6, v2

    .line 96
    :goto_5
    if-eqz p2, :cond_5

    iget-object v3, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v3, :cond_11

    move v3, v2

    :goto_6
    if-eqz v3, :cond_5

    .line 97
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v4, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eq v3, v4, :cond_12

    :cond_5
    move v5, v2

    .line 99
    :goto_7
    if-eqz p2, :cond_6

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    iget-boolean v4, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    if-eq v3, v4, :cond_13

    :cond_6
    move v3, v2

    .line 102
    :goto_8
    invoke-static {p1}, Lcom/google/android/apps/gmm/navigation/navui/r;->b(Lcom/google/android/apps/gmm/navigation/navui/b/a;)Lcom/google/maps/g/a/fw;

    move-result-object v4

    invoke-static {p2}, Lcom/google/android/apps/gmm/navigation/navui/r;->b(Lcom/google/android/apps/gmm/navigation/navui/b/a;)Lcom/google/maps/g/a/fw;

    move-result-object v7

    if-eq v4, v7, :cond_14

    move v4, v2

    .line 104
    :goto_9
    if-nez v0, :cond_7

    if-nez v5, :cond_7

    if-nez v6, :cond_7

    if-nez v4, :cond_7

    if-eqz v3, :cond_8

    .line 105
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->a:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/g;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_15

    move v0, v2

    :goto_a
    if-nez v0, :cond_16

    .line 108
    :cond_8
    :goto_b
    if-eqz p2, :cond_b

    .line 109
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v3, :cond_9

    sget-object v3, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v3, :cond_19

    :cond_9
    move v0, v2

    :goto_c
    iget-object v3, p2, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v3, v4, :cond_a

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v3, v4, :cond_1a

    :cond_a
    move v3, v2

    :goto_d
    if-eq v0, v3, :cond_c

    :cond_b
    move v1, v2

    .line 111
    :cond_c
    if-eqz v1, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->g:Lcom/google/android/apps/gmm/directions/a/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/navigation/navui/r;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)Lcom/google/android/apps/gmm/map/r/a/v;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/r;->a()Lcom/google/android/apps/gmm/map/o/b/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V

    goto/16 :goto_1

    :cond_d
    move v0, v1

    .line 89
    goto/16 :goto_2

    :cond_e
    move v0, v1

    .line 90
    goto/16 :goto_3

    :cond_f
    move v3, v1

    .line 93
    goto/16 :goto_4

    :cond_10
    move v6, v1

    .line 94
    goto :goto_5

    :cond_11
    move v3, v1

    .line 96
    goto :goto_6

    :cond_12
    move v5, v1

    .line 97
    goto :goto_7

    :cond_13
    move v3, v1

    .line 99
    goto :goto_8

    :cond_14
    move v4, v1

    .line 102
    goto :goto_9

    :cond_15
    move v0, v1

    .line 105
    goto :goto_a

    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v3, v4, v3

    if-nez v3, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->g:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/d;->c()V

    goto :goto_b

    :cond_17
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v3, v4, v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v4, v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-eqz v4, :cond_18

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->g:Lcom/google/android/apps/gmm/directions/a/d;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/map/b/a/q;Z)V

    goto :goto_b

    :cond_18
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/navui/b/a;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->g:Lcom/google/android/apps/gmm/directions/a/d;

    new-instance v6, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iput-object v0, v6, Lcom/google/android/apps/gmm/directions/f/a/b;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    sget-object v0, Lcom/google/android/apps/gmm/map/i/r;->a:Lcom/google/android/apps/gmm/map/i/r;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/directions/f/a/b;->a(Lcom/google/android/apps/gmm/map/i/s;)Lcom/google/android/apps/gmm/directions/f/a/b;

    move-result-object v0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->f:Z

    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->g:Z

    iput-object v4, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->h:Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->k:Z

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/navigation/navui/r;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)Lcom/google/android/apps/gmm/map/r/a/v;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->l:Lcom/google/android/apps/gmm/map/r/a/v;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/r;->a()Lcom/google/android/apps/gmm/map/o/b/h;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/gmm/directions/f/a/b;->m:Lcom/google/android/apps/gmm/map/o/b/h;

    new-instance v3, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    invoke-interface {v5, v3}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    goto/16 :goto_b

    :cond_19
    move v0, v1

    .line 109
    goto/16 :goto_c

    :cond_1a
    move v3, v1

    goto/16 :goto_d
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/f;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->f:Lcom/google/android/apps/gmm/navigation/commonui/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->g:Lcom/google/android/apps/gmm/directions/a/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/r;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/navigation/navui/r;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)Lcom/google/android/apps/gmm/map/r/a/v;

    move-result-object v1

    .line 118
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/r;->a()Lcom/google/android/apps/gmm/map/o/b/h;

    move-result-object v2

    .line 117
    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/a/d;->a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V

    .line 119
    return-void
.end method
