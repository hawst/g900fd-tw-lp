.class Lcom/google/android/apps/gmm/streetview/internal/ba;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field final b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(III)V
    .locals 2

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->c:I

    .line 16
    iput p2, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->d:I

    .line 17
    iput p3, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->a:I

    .line 18
    shl-int/lit8 v0, p3, 0x10

    shl-int/lit8 v1, p2, 0x8

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->b:I

    .line 19
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39
    instance-of v1, p1, Lcom/google/android/apps/gmm/streetview/internal/ba;

    if-eqz v1, :cond_0

    .line 40
    check-cast p1, Lcom/google/android/apps/gmm/streetview/internal/ba;

    .line 41
    iget v1, p1, Lcom/google/android/apps/gmm/streetview/internal/ba;->c:I

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->c:I

    if-ne v1, v2, :cond_0

    iget v1, p1, Lcom/google/android/apps/gmm/streetview/internal/ba;->d:I

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->d:I

    if-ne v1, v2, :cond_0

    iget v1, p1, Lcom/google/android/apps/gmm/streetview/internal/ba;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/internal/ba;->a:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 43
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TileKey x:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " y:"

    .line 49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " z:"

    .line 50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ba;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
