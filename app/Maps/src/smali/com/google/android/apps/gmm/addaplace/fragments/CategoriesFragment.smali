.class public Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/abn;",
        ">;"
    }
.end annotation


# instance fields
.field private c:Lcom/google/r/b/a/abn;

.field private d:Lcom/google/maps/g/gy;

.field private e:Lcom/google/android/apps/gmm/addaplace/c/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Z)Lcom/google/android/apps/gmm/base/fragments/g;
    .locals 1

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/g;->a:Lcom/google/android/apps/gmm/base/fragments/g;

    return-object v0
.end method

.method public final bridge synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 2

    .prologue
    .line 27
    check-cast p1, Lcom/google/r/b/a/abn;

    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->c:Lcom/google/r/b/a/abn;

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->c:Lcom/google/r/b/a/abn;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->b:Lcom/google/android/apps/gmm/base/fragments/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/a/b;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/addaplace/c/h;->a(Lcom/google/r/b/a/abn;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 54
    const-string v0, "LAT_LNG_KEY"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->d:Lcom/google/maps/g/gy;

    .line 55
    const-string v0, "CATEGORIES_RESPONSE_KEY"

    .line 56
    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abn;

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->c:Lcom/google/r/b/a/abn;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/c/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/addaplace/c/h;-><init>(Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    .line 59
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 64
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 66
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 69
    const-class v1, Lcom/google/android/apps/gmm/addaplace/b/b;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 70
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 71
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x1

    .line 76
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->c:Lcom/google/r/b/a/abn;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->c:Lcom/google/r/b/a/abn;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/addaplace/c/h;->a(Lcom/google/r/b/a/abn;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v1, v2

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/apq;->d:I

    invoke-static {}, Lcom/google/r/b/a/abj;->newBuilder()Lcom/google/r/b/a/abl;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->d:Lcom/google/maps/g/gy;

    if-nez v4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_2
    iget-object v5, v3, Lcom/google/r/b/a/abl;->b:Lcom/google/n/ao;

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v7, v5, Lcom/google/n/ao;->d:Z

    iget v2, v3, Lcom/google/r/b/a/abl;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v3, Lcom/google/r/b/a/abl;->a:I

    iget v2, v3, Lcom/google/r/b/a/abl;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v3, Lcom/google/r/b/a/abl;->a:I

    iput v0, v3, Lcom/google/r/b/a/abl;->c:I

    invoke-virtual {v3}, Lcom/google/r/b/a/abl;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/abj;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, p0, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    iput-boolean v7, v0, Lcom/google/android/apps/gmm/addaplace/c/h;->b:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->e:Lcom/google/android/apps/gmm/addaplace/c/h;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 109
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 110
    const-string v0, "CATEGORIES_RESPONSE_KEY"

    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/fragments/CategoriesFragment;->c:Lcom/google/r/b/a/abn;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 111
    return-void
.end method
