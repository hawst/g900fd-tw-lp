.class public abstract Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;
.super Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/reportmapissue/p;


# instance fields
.field private a:Z

.field public m:Lcom/google/android/apps/gmm/reportmapissue/o;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a(Z)I
.end method

.method public final a_(Lcom/google/e/a/a/a/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 76
    iput-object v8, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 86
    :goto_0
    return-void

    .line 81
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->a:Z

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->a(Z)I

    move-result v4

    invoke-static {p1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v5

    move v1, v2

    :goto_1
    array-length v0, v5

    if-ge v1, v0, :cond_1

    aget-object v0, v5, v1

    const/16 v6, 0x15

    invoke-virtual {v0, v3, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    if-ne v0, v4, :cond_2

    move v2, v3

    :cond_1
    if-nez v2, :cond_3

    .line 82
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v8}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_0

    .line 81
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 84
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->b_()V

    goto :goto_0
.end method

.method public b_()V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method public final c_()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    .line 114
    return-void
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 118
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->m:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/o;->f()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0, p0}, Lcom/google/android/apps/gmm/reportmapissue/o;->a(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/reportmapissue/p;)Lcom/google/android/apps/gmm/reportmapissue/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 65
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->onCreate(Landroid/os/Bundle;)V

    .line 36
    if-nez p1, :cond_1

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 40
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->aa()Lcom/google/maps/g/om;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/om;->f:Lcom/google/maps/g/om;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->a:Z

    .line 45
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->o()V

    .line 46
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 43
    :cond_1
    const-string v0, "is_poi"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->a:Z

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->onDestroy()V

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/o;->f()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->m:Lcom/google/android/apps/gmm/reportmapissue/o;

    .line 58
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 50
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 51
    const-string v0, "is_poi"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 52
    return-void
.end method
