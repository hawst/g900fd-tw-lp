.class public Lcom/google/android/apps/gmm/navigation/commonui/c/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/commonui/c/b;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/o/a/c;

.field private final b:Lcom/google/android/apps/gmm/navigation/a/d;

.field private final c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

.field private final d:Z

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/navigation/commonui/c/d;ZZ)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/o/a/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->a:Lcom/google/android/apps/gmm/o/a/c;

    .line 33
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/navigation/a/d;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 34
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    .line 35
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->d:Z

    .line 36
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->e:Z

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->a:Lcom/google/android/apps/gmm/o/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->a:Lcom/google/android/apps/gmm/o/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 66
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->a:Lcom/google/android/apps/gmm/o/a/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->a:Lcom/google/android/apps/gmm/o/a/c;

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/d;->c()V

    .line 68
    const/4 v0, 0x0

    return-object v0

    .line 66
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 73
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->a:Lcom/google/android/apps/gmm/o/a/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->a:Lcom/google/android/apps/gmm/o/a/c;

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/o/a/c;->d(Z)Z

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/d;->c()V

    .line 75
    const/4 v0, 0x0

    return-object v0

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 80
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->b:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/navigation/a/d;->b(Z)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/d;->c()V

    .line 82
    const/4 v0, 0x0

    return-object v0

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/d;->a()V

    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/d;->b()V

    .line 94
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/d;->c()V

    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/c/c;->c:Lcom/google/android/apps/gmm/navigation/commonui/c/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/d;->d()V

    .line 106
    const/4 v0, 0x0

    return-object v0
.end method
