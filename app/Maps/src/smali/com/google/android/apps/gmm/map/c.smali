.class public Lcom/google/android/apps/gmm/map/c;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/map/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/d;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/map/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/h;-><init>()V

    return-void
.end method

.method public static a(F)Lcom/google/android/apps/gmm/map/a;
    .locals 1

    .prologue
    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/map/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/j;-><init>(F)V

    return-object v0
.end method

.method public static a(FF)Lcom/google/android/apps/gmm/map/a;
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/map/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/map/i;-><init>(FF)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;)Lcom/google/android/apps/gmm/map/a;
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/google/android/apps/gmm/map/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;F)Lcom/google/android/apps/gmm/map/a;
    .locals 1

    .prologue
    .line 209
    new-instance v0, Lcom/google/android/apps/gmm/map/m;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/map/m;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;F)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;
    .locals 1
    .param p2    # Landroid/graphics/Rect;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 252
    new-instance v0, Lcom/google/android/apps/gmm/map/n;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/map/n;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;I)Lcom/google/android/apps/gmm/map/a;
    .locals 6

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/apps/gmm/map/f;

    move-object v1, p0

    move v2, p1

    move v3, p1

    move v4, p1

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/r;IIII)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;III)Lcom/google/android/apps/gmm/map/a;
    .locals 8

    .prologue
    .line 386
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Map size should not be 0"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/g;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p3

    move v6, p3

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/g;-><init>(Lcom/google/android/apps/gmm/map/b/a/r;IIIIII)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;IIII)Lcom/google/android/apps/gmm/map/a;
    .locals 6

    .prologue
    .line 322
    new-instance v0, Lcom/google/android/apps/gmm/map/f;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f;-><init>(Lcom/google/android/apps/gmm/map/b/a/r;IIII)V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/a;
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/map/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/k;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/f/o;F)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 6

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    add-float/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v5

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/b;ILcom/google/android/apps/gmm/map/b/a/r;IIIIII)V
    .locals 16

    .prologue
    .line 38
    invoke-interface/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/b;->c()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v3

    add-int v2, p5, p6

    sub-int v4, p3, v2

    add-int v2, p7, p8

    sub-int v5, p4, v2

    if-lez v4, :cond_0

    if-lez v5, :cond_0

    const/4 v2, 0x1

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x98

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "View size is too small after padding. Map width: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p3

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " map height: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p4

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " padding[l,r,t,b]: ["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p5

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p7

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ","

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move/from16 v0, p8

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    iget v2, v3, Lcom/google/android/apps/gmm/map/f/o;->i:F

    float-to-double v2, v2

    move-object/from16 v0, p2

    invoke-static {v0, v5, v4, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/r;IID)D

    move-result-wide v6

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v8, v9, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v10, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v8, v9, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v8

    iget v2, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v2, v9, :cond_3

    const/high16 v2, 0x40000000    # 2.0f

    iget v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v9

    iget v9, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v2, v9

    :goto_1
    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v3, v9

    new-instance v9, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v10, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v10

    const/high16 v10, 0x40000000    # 2.0f

    rem-int/2addr v2, v10

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v3, v8

    invoke-direct {v9, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/a;->a()Lcom/google/android/apps/gmm/map/f/a/c;

    move-result-object v8

    iput-object v9, v8, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/q;

    iget v9, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v10, v9

    const-wide v12, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v10, v11}, Ljava/lang/Math;->exp(D)D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    move-result-wide v10

    const-wide v14, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v10, v14

    mul-double/2addr v10, v12

    const-wide v12, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v10, v12

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v12

    invoke-direct {v3, v10, v11, v12, v13}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iput-object v3, v8, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    double-to-float v2, v6

    iput v2, v8, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    move/from16 v0, p5

    move/from16 v1, p6

    if-ne v0, v1, :cond_2

    move/from16 v0, p7

    move/from16 v1, p8

    if-eq v0, v1, :cond_4

    :cond_2
    move/from16 v0, p5

    int-to-float v2, v0

    const/high16 v3, 0x3f000000    # 0.5f

    int-to-float v4, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    move/from16 v0, p7

    int-to-float v3, v0

    const/high16 v4, 0x3f000000    # 0.5f

    int-to-float v5, v5

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    move/from16 v0, p3

    int-to-float v4, v0

    move/from16 v0, p4

    int-to-float v5, v0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/f/a/e;->a(FFFF)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v2

    iput-object v2, v8, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    :goto_2
    new-instance v2, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v4, v8, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v5, v8, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v6, v8, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v7, v8, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b;->a(Lcom/google/android/apps/gmm/map/f/a/a;I)V

    return-void

    :cond_3
    iget v2, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v9, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, v9

    goto/16 :goto_1

    :cond_4
    sget-object v2, Lcom/google/android/apps/gmm/map/f/a/e;->a:Lcom/google/android/apps/gmm/map/f/a/e;

    iput-object v2, v8, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    goto :goto_2
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/q;F)Lcom/google/android/apps/gmm/map/a;
    .locals 2

    .prologue
    .line 233
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/apps/gmm/map/n;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/apps/gmm/map/n;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)V

    return-object v1
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/q;FLandroid/graphics/Rect;)Lcom/google/android/apps/gmm/map/a;
    .locals 1

    .prologue
    .line 285
    new-instance v0, Lcom/google/android/apps/gmm/map/e;

    invoke-direct {v0, p2, p0, p1}, Lcom/google/android/apps/gmm/map/e;-><init>(Landroid/graphics/Rect;Lcom/google/android/apps/gmm/map/b/a/q;F)V

    return-object v0
.end method
