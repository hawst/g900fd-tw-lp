.class public abstract Lcom/google/android/apps/gmm/map/o/ag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/aa;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/internal/c/aa;)V
    .locals 0
    .param p2    # Lcom/google/android/apps/gmm/map/internal/c/aa;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/ag;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 65
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/ag;->b:Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 66
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/z;)Lcom/google/android/apps/gmm/map/internal/c/z;
    .locals 14

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 140
    if-nez p0, :cond_0

    .line 166
    :goto_0
    return-object v8

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v12

    .line 145
    if-ltz v12, :cond_1

    move v0, v11

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    move v0, v10

    goto :goto_1

    :cond_2
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13, v12}, Ljava/util/ArrayList;-><init>(I)V

    move v9, v10

    .line 146
    :goto_2
    if-ge v9, v12, :cond_5

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 148
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v0, :cond_3

    move v0, v11

    :goto_3
    if-eqz v0, :cond_4

    .line 150
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->i:Lcom/google/android/apps/gmm/map/internal/c/bn;

    .line 151
    if-eqz v6, :cond_6

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bn;

    const v1, -0x7dfeff

    .line 154
    iget v2, v6, Lcom/google/android/apps/gmm/map/internal/c/bn;->b:I

    .line 155
    iget v3, v6, Lcom/google/android/apps/gmm/map/internal/c/bn;->c:I

    .line 156
    iget v4, v6, Lcom/google/android/apps/gmm/map/internal/c/bn;->d:F

    .line 157
    iget v5, v6, Lcom/google/android/apps/gmm/map/internal/c/bn;->e:F

    .line 158
    iget v6, v6, Lcom/google/android/apps/gmm/map/internal/c/bn;->f:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/c/bn;-><init>(IIIFFI)V

    .line 160
    :goto_4
    iget-object v1, v7, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/internal/c/be;->b()Lcom/google/android/apps/gmm/map/internal/c/bh;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/bh;->h:Lcom/google/android/apps/gmm/map/internal/c/bn;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/be;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bh;)V

    .line 161
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;)V

    invoke-interface {v13, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    :goto_5
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_2

    :cond_3
    move v0, v10

    .line 148
    goto :goto_3

    .line 163
    :cond_4
    invoke-interface {v13, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 166
    :cond_5
    new-instance v8, Lcom/google/android/apps/gmm/map/internal/c/z;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v8, v13, v0}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    goto :goto_0

    :cond_6
    move-object v0, v8

    goto :goto_4
.end method


# virtual methods
.method public a(ILcom/google/android/apps/gmm/map/o/z;Lcom/google/android/apps/gmm/map/internal/c/m;)Lcom/google/android/apps/gmm/map/o/d;
    .locals 10

    .prologue
    .line 72
    instance-of v0, p3, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_c

    .line 73
    check-cast p3, Lcom/google/android/apps/gmm/map/internal/c/an;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ag;->b:Lcom/google/android/apps/gmm/map/internal/c/aa;

    if-eqz v0, :cond_5

    iget-object v2, p3, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ag;->b:Lcom/google/android/apps/gmm/map/internal/c/aa;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/z;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    array-length v4, v3

    if-ltz v4, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const-wide/16 v6, 0x5

    int-to-long v8, v4

    add-long/2addr v6, v8

    div-int/lit8 v0, v4, 0xa

    int-to-long v4, v0

    add-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_3

    const v0, 0x7fffffff

    :goto_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v4, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v1, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    move-object v0, v1

    :goto_2
    iget-object v1, p3, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/o/ag;->a(Lcom/google/android/apps/gmm/map/internal/c/z;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/internal/c/an;->m()Lcom/google/android/apps/gmm/map/internal/c/ao;

    move-result-object v2

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    iput-object v1, v2, Lcom/google/android/apps/gmm/map/internal/c/ao;->k:Lcom/google/android/apps/gmm/map/internal/c/z;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/ao;->a()Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v1

    const/4 v3, 0x0

    sget-object v7, Lcom/google/android/apps/gmm/map/t/l;->y:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v8, 0x0

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/o/z;->f:Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;

    iget-object v4, p2, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    iget-object v5, p2, Lcom/google/android/apps/gmm/map/o/z;->b:Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;

    iget-object v6, p2, Lcom/google/android/apps/gmm/map/o/z;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    move v2, p1

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aj;->a(Lcom/google/android/apps/gmm/map/internal/c/an;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/o/h;Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;Lcom/google/android/apps/gmm/map/t/l;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ag;

    move-result-object v0

    .line 87
    :goto_3
    return-object v0

    .line 73
    :cond_3
    const-wide/32 v6, -0x80000000

    cmp-long v0, v4, v6

    if-gez v0, :cond_4

    const/high16 v0, -0x80000000

    goto :goto_1

    :cond_4
    long-to-int v0, v4

    goto :goto_1

    :cond_5
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/internal/c/an;->f()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v2, p3, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const-string v1, "http://mt0.google.com/vt/icon/name=icons/spotlight/measle_spotlight_L.png&scale=4"

    const/4 v3, 0x4

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(Ljava/lang/String;I)V

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/z;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    if-nez v3, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    array-length v4, v3

    if-ltz v4, :cond_7

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    :cond_8
    const-wide/16 v6, 0x5

    int-to-long v8, v4

    add-long/2addr v6, v8

    div-int/lit8 v0, v4, 0xa

    int-to-long v4, v0

    add-long/2addr v4, v6

    const-wide/32 v6, 0x7fffffff

    cmp-long v0, v4, v6

    if-lez v0, :cond_9

    const v0, 0x7fffffff

    :goto_5
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v4, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-direct {v1, v4, v0}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    move-object v0, v1

    goto :goto_2

    :cond_9
    const-wide/32 v6, -0x80000000

    cmp-long v0, v4, v6

    if-gez v0, :cond_a

    const/high16 v0, -0x80000000

    goto :goto_5

    :cond_a
    long-to-int v0, v4

    goto :goto_5

    :cond_b
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/o/ag;->a(Lcom/google/android/apps/gmm/map/internal/c/z;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v0

    goto/16 :goto_2

    .line 74
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/ag;->b:Lcom/google/android/apps/gmm/map/internal/c/aa;

    if-eqz v0, :cond_d

    .line 76
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/ag;->b:Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/high16 v4, 0x3e800000    # 0.25f

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/ag;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/apps/gmm/map/t/l;->y:Lcom/google/android/apps/gmm/map/t/l;

    const/4 v8, 0x0

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/o/z;->i:Lcom/google/android/apps/gmm/map/o/bj;

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/o/z;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    iget-object v9, p2, Lcom/google/android/apps/gmm/map/o/z;->c:Lcom/google/android/apps/gmm/map/o/h;

    move v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/o/bj;->a(ILcom/google/android/apps/gmm/map/internal/c/aa;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;FLcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/g/b;Lcom/google/android/apps/gmm/map/o/h;)Lcom/google/android/apps/gmm/map/o/bi;

    move-result-object v0

    goto/16 :goto_3

    .line 87
    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_3
.end method

.method public abstract a(Lcom/google/android/apps/gmm/map/internal/c/m;)Z
.end method
