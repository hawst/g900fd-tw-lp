.class public final Lcom/google/android/apps/gmm/feedback/d/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/feedback/d/c;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Lcom/google/android/libraries/curvular/aw;

.field private e:Lcom/google/b/f/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/d/c;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/libraries/curvular/aw;Lcom/google/b/f/t;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/d/d;->a:Lcom/google/android/apps/gmm/feedback/d/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    iput-object p2, p0, Lcom/google/android/apps/gmm/feedback/d/d;->b:Ljava/lang/Integer;

    .line 160
    iput-object p3, p0, Lcom/google/android/apps/gmm/feedback/d/d;->c:Ljava/lang/Integer;

    .line 161
    iput-object p4, p0, Lcom/google/android/apps/gmm/feedback/d/d;->d:Lcom/google/android/libraries/curvular/aw;

    .line 162
    iput-object p5, p0, Lcom/google/android/apps/gmm/feedback/d/d;->e:Lcom/google/b/f/t;

    .line 163
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/d;->a:Lcom/google/android/apps/gmm/feedback/d/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/d/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/d/d;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/d;->a:Lcom/google/android/apps/gmm/feedback/d/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/d/c;->b:Landroid/widget/AdapterView$OnItemClickListener;

    const/4 v3, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/d/d;->a:Lcom/google/android/apps/gmm/feedback/d/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/feedback/d/c;->g:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/gmm/feedback/d/d;->b:Ljava/lang/Integer;

    invoke-interface {v2, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    int-to-long v4, v2

    move-object v2, v1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 173
    return-object v1
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/d;->a:Lcom/google/android/apps/gmm/feedback/d/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/d/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/d/d;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/d;->e:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/d/d;->d:Lcom/google/android/libraries/curvular/aw;

    return-object v0
.end method
