.class public Lcom/google/android/apps/gmm/place/z;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/b/b;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field private a:Lcom/google/android/apps/gmm/place/bf;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/maps/g/hy;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Ljava/lang/String;Z)V
    .locals 3
    .param p3    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    .line 109
    instance-of v1, v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    if-eqz v1, :cond_0

    .line 110
    check-cast v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 111
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    .line 116
    invoke-static {v0, p1, p3, p5, p6}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    move-result-object v0

    .line 119
    if-nez p4, :cond_1

    sget-object p4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    :cond_1
    iput-object p4, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->i:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 121
    if-eqz p2, :cond_2

    .line 122
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0

    .line 124
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0
.end method


# virtual methods
.method public final Y_()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 44
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)Lcom/google/android/apps/gmm/shared/net/i;
    .locals 1
    .param p2    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/z;->a:Lcom/google/android/apps/gmm/place/bf;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/place/bf;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)Lcom/google/android/apps/gmm/place/ci;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/place/bf;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/bf;-><init>(Lcom/google/android/apps/gmm/base/activities/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/z;->a:Lcom/google/android/apps/gmm/place/bf;

    .line 39
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 7
    .param p4    # Lcom/google/android/apps/gmm/suggest/d/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0, p3}, Lcom/google/android/apps/gmm/z/b/f;-><init>(Lcom/google/maps/g/hy;)V

    .line 64
    if-eqz p4, :cond_0

    .line 65
    invoke-virtual {p4}, Lcom/google/android/apps/gmm/suggest/d/e;->b()Lcom/google/b/f/bf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/bf;)Lcom/google/android/apps/gmm/z/b/f;

    .line 67
    :cond_0
    iget-object v1, p2, Lcom/google/android/apps/gmm/suggest/e/d;->n:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v3, v2, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v2, Lcom/google/maps/g/ia;->a:I

    iput-object v1, v2, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    .line 68
    :cond_2
    sget-object v1, Lcom/google/b/f/t;->gr:Lcom/google/b/f/t;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    .line 70
    const/4 v2, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v3

    check-cast v3, Lcom/google/maps/g/hy;

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/z;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/maps/g/hy;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Ljava/lang/String;Z)V

    .line 73
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 7
    .param p3    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 54
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    aput-object p3, v1, v6

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/maps/g/hy;

    move-result-object v3

    .line 55
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/z;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/maps/g/hy;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Ljava/lang/String;Z)V

    .line 57
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Ljava/lang/String;)V
    .locals 7
    .param p3    # Lcom/google/b/f/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 84
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    aput-object p3, v1, v6

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/maps/g/hy;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 85
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/z;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/maps/g/hy;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Ljava/lang/String;Z)V

    .line 87
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 49
    return-void
.end method
