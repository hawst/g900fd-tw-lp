.class public Lcom/google/android/apps/gmm/util/b/x;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/av;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/util/b/q;",
        ">;",
        "Lcom/google/android/libraries/curvular/av;"
    }
.end annotation


# static fields
.field private static final a:Lcom/google/android/libraries/curvular/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/am",
            "<",
            "Lcom/google/android/apps/gmm/util/b/q;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/util/b/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/util/b/y;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/util/b/x;->a:Lcom/google/android/libraries/curvular/am;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 185
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/r;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 104
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-direct {v0, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 105
    sget-object v1, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    iget-object v1, v1, Lcom/google/android/apps/gmm/util/b/ac;->f:Lcom/google/android/apps/gmm/util/b/ad;

    invoke-interface {v1, p0}, Lcom/google/android/apps/gmm/util/b/ad;->a(Landroid/content/Context;)Lcom/google/android/libraries/curvular/bd;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/util/b/r;->a:Ljava/lang/Class;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 108
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p1, Lcom/google/android/apps/gmm/util/b/r;->b:Lcom/google/android/libraries/curvular/ce;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 111
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x43200000    # 160.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 112
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 113
    const/high16 v2, 0x40000000    # 2.0f

    .line 114
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 115
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v3, v3, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v2

    .line 113
    invoke-virtual {v0, v1, v2}, Landroid/view/View;->measure(II)V

    .line 116
    return-object v0
.end method

.method static a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/ac;->f:Lcom/google/android/apps/gmm/util/b/ad;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/util/b/ad;->a(Landroid/content/Context;)Lcom/google/android/libraries/curvular/bd;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 4

    .prologue
    .line 121
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 122
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/util/b/x;->a:Lcom/google/android/libraries/curvular/am;

    .line 125
    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x2

    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/util/b/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/q;->c()Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->Y:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x3

    .line 129
    new-instance v2, Lcom/google/android/libraries/curvular/c/f;

    sget-object v3, Lcom/google/android/libraries/curvular/c/a;->d:Lcom/google/android/libraries/curvular/c/a;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/c/f;-><init>(Ljava/lang/Enum;)V

    aput-object v2, v1, v0

    const/4 v0, 0x4

    .line 130
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/util/b/x;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 121
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-class v1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 38
    check-cast p2, Lcom/google/android/apps/gmm/util/b/q;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/util/b/q;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/util/b/l;->a:Lcom/google/android/apps/gmm/util/b/ac;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/ac;->f:Lcom/google/android/apps/gmm/util/b/ad;

    invoke-interface {v0, p3}, Lcom/google/android/apps/gmm/util/b/ad;->a(Landroid/content/Context;)Lcom/google/android/libraries/curvular/bd;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    :cond_0
    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/r;

    invoke-static {p3, v0}, Lcom/google/android/apps/gmm/util/b/x;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/r;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/r;

    const-class v4, Lcom/google/android/apps/gmm/util/b/z;

    new-instance v5, Lcom/google/android/apps/gmm/util/b/ab;

    iget-object v6, v0, Lcom/google/android/apps/gmm/util/b/r;->a:Ljava/lang/Class;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/r;->b:Lcom/google/android/libraries/curvular/ce;

    invoke-direct {v5, v6, v0, v1, v3}, Lcom/google/android/apps/gmm/util/b/ab;-><init>(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;II)V

    invoke-virtual {p4, v4, v5}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_1
.end method
