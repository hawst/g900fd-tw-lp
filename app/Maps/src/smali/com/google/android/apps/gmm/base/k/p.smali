.class public Lcom/google/android/apps/gmm/base/k/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cp;


# static fields
.field static final a:Ljava/lang/String;

.field static final b:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Landroid/view/ViewTreeObserver$OnPreDrawListener;",
            ">;"
        }
    .end annotation
.end field

.field static final c:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Landroid/view/View$OnClickListener;",
            ">;"
        }
    .end annotation
.end field

.field private static final i:Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private final e:Lcom/google/android/libraries/curvular/bd;

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:Landroid/view/View$OnLongClickListener;

.field private final h:Lcom/google/android/apps/gmm/z/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 101
    sput-boolean v0, Lcom/google/android/libraries/curvular/bo;->a:Z

    .line 102
    sput-boolean v0, Lcom/google/android/libraries/curvular/bo;->b:Z

    .line 103
    sput-boolean v0, Lcom/google/android/libraries/curvular/bo;->c:Z

    .line 106
    const-class v0, Lcom/google/android/apps/gmm/base/k/p;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/k/p;->a:Ljava/lang/String;

    .line 108
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/k/p;->d:Lcom/google/android/libraries/curvular/bk;

    .line 110
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/k/p;->b:Lcom/google/android/libraries/curvular/bk;

    .line 1065
    new-instance v0, Lcom/google/android/apps/gmm/base/k/z;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/k/z;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/k/p;->i:Landroid/view/View$OnLayoutChangeListener;

    .line 1084
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/k/p;->c:Lcom/google/android/libraries/curvular/bk;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 1

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/p;->e:Lcom/google/android/libraries/curvular/bd;

    .line 120
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/p;->h:Lcom/google/android/apps/gmm/z/a/b;

    .line 121
    new-instance v0, Lcom/google/android/apps/gmm/base/k/q;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/apps/gmm/base/k/q;-><init>(Lcom/google/android/apps/gmm/base/k/p;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/p;->f:Landroid/view/View$OnClickListener;

    .line 133
    new-instance v0, Lcom/google/android/apps/gmm/base/k/s;

    invoke-direct {v0, p0, p2}, Lcom/google/android/apps/gmm/base/k/s;-><init>(Lcom/google/android/apps/gmm/base/k/p;Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/p;->g:Landroid/view/View$OnLongClickListener;

    .line 143
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;Ljava/lang/Object;Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)Z
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Enum",
            "<+",
            "Lcom/google/android/libraries/curvular/cn;",
            ">;",
            "Ljava/lang/Object;",
            "TT;",
            "Landroid/view/View;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 149
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/k/j;

    if-eqz v2, :cond_88

    .line 150
    sget-object v3, Lcom/google/android/apps/gmm/base/k/r;->a:[I

    move-object/from16 v2, p1

    check-cast v2, Lcom/google/android/apps/gmm/base/k/j;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/k/j;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 276
    sget-object v2, Lcom/google/android/apps/gmm/base/k/p;->a:Ljava/lang/String;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unhandled property: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 295
    :cond_0
    :goto_0
    const/4 v2, 0x0

    :goto_1
    return v2

    .line 152
    :pswitch_0
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    if-eqz v2, :cond_1

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    check-cast p4, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p4

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->c:Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, p4

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Lcom/google/android/apps/gmm/navigation/navui/az;

    move-result-object v2

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a()V

    const/4 v2, 0x1

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 154
    :pswitch_1
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    check-cast p2, Ljava/lang/Boolean;

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;

    new-instance v2, Lcom/google/android/apps/gmm/base/views/p;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/base/views/p;-><init>(Z)V

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/base/views/p;->b:Z

    move-object/from16 v0, p4

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->a:Z

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/views/p;->a:Z

    move-object/from16 v0, p4

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->b:Z

    const/4 v2, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 156
    :pswitch_2
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    if-eqz v2, :cond_3

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Class;

    if-eqz v2, :cond_3

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    check-cast p2, Ljava/lang/Class;

    new-instance v2, Lcom/google/android/libraries/curvular/bq;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/k/p;->e:Lcom/google/android/libraries/curvular/bd;

    move-object/from16 v0, p2

    invoke-direct {v2, v3, v0}, Lcom/google/android/libraries/curvular/bq;-><init>(Lcom/google/android/libraries/curvular/bd;Ljava/lang/Class;)V

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->b:Landroid/support/v4/view/ag;

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/ag;)V

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(I)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 158
    :pswitch_3
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    if-eqz v2, :cond_6

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->b:Landroid/support/v4/view/ag;

    check-cast v2, Lcom/google/android/libraries/curvular/bq;

    if-eqz v2, :cond_6

    check-cast p2, Lcom/google/android/libraries/curvular/ce;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bq;->b:Ljava/util/List;

    move-object/from16 v0, p2

    invoke-interface {v2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    if-gez v2, :cond_4

    const/4 v2, -0x2

    :cond_4
    const/4 v3, -0x2

    if-eq v2, v3, :cond_5

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v3

    if-eq v2, v3, :cond_5

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(I)V

    :cond_5
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 160
    :pswitch_4
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    if-eqz v2, :cond_7

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->b:Landroid/support/v4/view/ag;

    check-cast v2, Lcom/google/android/libraries/curvular/bq;

    if-eqz v2, :cond_7

    move-object/from16 v0, p2

    instance-of v3, v0, Ljava/util/List;

    if-eqz v3, :cond_7

    check-cast p2, Ljava/util/List;

    iget-object v3, v2, Lcom/google/android/libraries/curvular/bq;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    iget-object v3, v2, Lcom/google/android/libraries/curvular/bq;->b:Ljava/util/List;

    move-object/from16 v0, p2

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v2}, Lcom/google/android/libraries/curvular/bq;->b()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 162
    :pswitch_5
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    if-eqz v2, :cond_9

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p4

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->g:Z

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(I)V

    :cond_8
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 164
    :pswitch_6
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;

    if-eqz v2, :cond_b

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Number;

    if-eqz v2, :cond_a

    check-cast p4, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;

    check-cast p2, Ljava/lang/Number;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_a

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->a:F

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/FixedAspectRatioLayout;->requestLayout()V

    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 166
    :pswitch_7
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;

    if-eqz v2, :cond_c

    check-cast p4, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p4

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->d:Z

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 168
    :pswitch_8
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    if-eqz v2, :cond_d

    check-cast p4, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p4

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->e:Z

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 170
    :pswitch_9
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;

    if-eqz v2, :cond_10

    if-eqz p2, :cond_f

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    iget v5, v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->b:I

    if-eq v5, v4, :cond_f

    iget-object v5, v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->a:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_e

    iget-object v5, v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    :cond_e
    iput v4, v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->b:I

    if-eqz v3, :cond_f

    iget v3, v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->b:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/e;->aB:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/apps/gmm/e;->aA:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->a(III)Landroid/graphics/Bitmap;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->a:Landroid/graphics/Bitmap;

    :cond_f
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 172
    :pswitch_a
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    if-eqz v2, :cond_12

    if-eqz p2, :cond_11

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->g:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    :cond_11
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 174
    :pswitch_b
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    if-eqz v2, :cond_14

    if-eqz p2, :cond_13

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->h:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    :cond_13
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 176
    :pswitch_c
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;

    if-eqz v2, :cond_17

    if-eqz p2, :cond_16

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->c:I

    move-object/from16 v0, p4

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->b:Z

    if-eqz v3, :cond_15

    const v2, 0x7fffffff

    :cond_15
    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->setMaxLines(I)V

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->requestLayout()V

    :cond_16
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 178
    :pswitch_d
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/c/a;

    if-eqz v2, :cond_19

    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    if-eqz v2, :cond_18

    check-cast p2, Lcom/google/android/apps/gmm/base/views/c/a;

    check-cast p4, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/base/views/c/a;->a:I

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/apps/gmm/base/views/c/a;->b:I

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/apps/gmm/base/views/c/a;->c:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->setDate(III)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_18
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    if-eqz v2, :cond_19

    check-cast p2, Lcom/google/android/apps/gmm/base/views/c/a;

    check-cast p4, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/base/views/c/a;->a:I

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/apps/gmm/base/views/c/a;->b:I

    move-object/from16 v0, p2

    iget v4, v0, Lcom/google/android/apps/gmm/base/views/c/a;->c:I

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    const-string v6, "selected_day"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "year"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "month"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "week_start"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;->setMonthParams(Ljava/util/HashMap;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_19
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 180
    :pswitch_e
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    if-eqz v2, :cond_1b

    if-eqz p2, :cond_1a

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    :cond_1a
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_1b
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 182
    :pswitch_f
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v2, :cond_1f

    check-cast p4, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-nez p2, :cond_1d

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->b:Lcom/google/android/apps/gmm/map/i/a/c;

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/apps/gmm/util/r;->a()Z

    move-result v3

    if-eqz v3, :cond_1c

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :goto_2
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_1c
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_1d
    check-cast p2, Lcom/google/android/apps/gmm/base/views/c/b;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/c/b;->a:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/c/b;->b:Lcom/google/r/b/a/acy;

    move-object/from16 v0, p2

    iget v5, v0, Lcom/google/android/apps/gmm/base/views/c/b;->c:F

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v6

    invoke-virtual {v2, v6}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    new-instance v2, Lcom/google/android/apps/gmm/directions/views/n;

    move-object/from16 v0, p4

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/directions/views/n;-><init>(Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;)V

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->b:Lcom/google/android/apps/gmm/map/i/a/c;

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a:Lcom/google/android/apps/gmm/map/i/a/a;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    move-object/from16 v0, p4

    iget-object v7, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->b:Lcom/google/android/apps/gmm/map/i/a/c;

    invoke-interface/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/lang/String;Lcom/google/r/b/a/acy;FLandroid/content/res/Resources;Lcom/google/android/apps/gmm/map/i/a/c;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/gmm/util/r;->a()Z

    move-result v3

    if-eqz v3, :cond_1e

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v4, v2, v5}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_1e
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_1f
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 184
    :pswitch_10
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v2, :cond_20

    check-cast p4, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    check-cast p2, Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->d:Ljava/lang/String;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_20
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 186
    :pswitch_11
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;

    if-eqz v2, :cond_22

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_21

    check-cast p4, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->d:I

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->requestLayout()V

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->invalidate()V

    :cond_21
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_22
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 188
    :pswitch_12
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/CompactIconList;

    if-eqz v2, :cond_24

    if-eqz p2, :cond_23

    check-cast p4, Lcom/google/android/apps/gmm/base/views/CompactIconList;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p4

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->a:Z

    if-eq v3, v2, :cond_23

    move-object/from16 v0, p4

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/base/views/CompactIconList;->a:Z

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->requestLayout()V

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/CompactIconList;->invalidate()V

    :cond_23
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_24
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 190
    :pswitch_13
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;

    if-eqz v2, :cond_25

    if-eqz p2, :cond_25

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->setExpanded(ZLcom/google/android/apps/gmm/base/views/p;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_25
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 192
    :pswitch_14
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;

    if-eqz v2, :cond_27

    if-eqz p2, :cond_26

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    :cond_26
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_27
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 194
    :pswitch_15
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/FiveStarView;

    if-eqz v2, :cond_29

    if-nez p2, :cond_28

    const/4 v2, 0x0

    :goto_3
    check-cast p4, Lcom/google/android/apps/gmm/base/views/FiveStarView;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->setValue(F)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_28
    check-cast p2, Ljava/lang/Number;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Number;->floatValue()F

    move-result v2

    goto :goto_3

    :cond_29
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;

    if-eqz v2, :cond_2d

    if-nez p2, :cond_2a

    const/4 v2, 0x0

    :goto_4
    check-cast p4, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;

    if-ltz v2, :cond_2b

    const/4 v3, 0x5

    if-gt v2, v3, :cond_2b

    const/4 v3, 0x1

    :goto_5
    if-nez v3, :cond_2c

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_2a
    check-cast p2, Ljava/lang/Number;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Number;->intValue()I

    move-result v2

    goto :goto_4

    :cond_2b
    const/4 v3, 0x0

    goto :goto_5

    :cond_2c
    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->e:I

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->b()V

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_2d
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 196
    :pswitch_16
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/FiveStarView;

    if-eqz v2, :cond_2e

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/k/k;

    if-eqz v2, :cond_2e

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/FiveStarView;

    check-cast p2, Lcom/google/android/apps/gmm/base/k/k;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/k/k;->b:Lcom/google/android/libraries/curvular/aw;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/k/k;->c:Lcom/google/android/libraries/curvular/aw;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/k/k;->d:Lcom/google/android/libraries/curvular/aw;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/FiveStarView;->c:Landroid/graphics/drawable/Drawable;

    iput-object v4, v2, Lcom/google/android/apps/gmm/base/views/FiveStarView;->b:Landroid/graphics/drawable/Drawable;

    iput-object v5, v2, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->a()V

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/k/k;->a:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/views/FiveStarView;->setIsInteractive(Z)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_2e
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 198
    :pswitch_17
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;

    if-eqz v2, :cond_31

    check-cast p4, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;

    check-cast p2, Ljava/lang/CharSequence;

    const/16 v2, 0x3a

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Landroid/text/TextUtils;->indexOf(Ljava/lang/CharSequence;C)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2f

    const/16 v3, 0x3a

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Landroid/text/TextUtils;->lastIndexOf(Ljava/lang/CharSequence;C)I

    move-result v3

    if-eq v2, v3, :cond_30

    :cond_2f
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->c:Landroid/widget/TextView;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_6
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_30
    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->a:Landroid/widget/TextView;

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v4, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->b:Landroid/widget/TextView;

    add-int/lit8 v4, v2, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/place/station/views/TransitDepartureTimeView;->c:Landroid/widget/TextView;

    add-int/lit8 v2, v2, 0x1

    invoke-interface/range {p2 .. p2}, Ljava/lang/CharSequence;->length()I

    move-result v4

    move-object/from16 v0, p2

    invoke-interface {v0, v2, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    :cond_31
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 200
    :pswitch_18
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;

    if-eqz v2, :cond_32

    check-cast p4, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;

    check-cast p2, Lcom/google/android/apps/gmm/base/views/c/c;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/c;->a:[I

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/c/c;->b:[Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/base/views/HorizontalHistogramLabelledBarView;->setBucketValues([I[Ljava/lang/String;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_32
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 202
    :pswitch_19
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    if-eqz v2, :cond_34

    if-eqz p2, :cond_33

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->i:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    :cond_33
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_34
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 204
    :pswitch_1a
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;

    if-eqz v2, :cond_36

    if-eqz p2, :cond_35

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/google/android/libraries/curvular/au;->b_(Landroid/content/Context;)I

    move-result v3

    iget v4, v2, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b:I

    if-eq v4, v3, :cond_35

    iput v3, v2, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->requestLayout()V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->invalidate()V

    :cond_35
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_36
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 206
    :pswitch_1b
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v2, :cond_38

    if-eqz p2, :cond_37

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/google/android/libraries/curvular/au;->b_(Landroid/content/Context;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->e:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    :cond_37
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_38
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 208
    :pswitch_1c
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;

    if-eqz v2, :cond_3a

    check-cast p4, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;

    if-nez p2, :cond_39

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;->setIcons(Ljava/util/List;)V

    :goto_7
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_39
    check-cast p2, Lcom/google/android/apps/gmm/base/views/c/d;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/d;->a:Ljava/util/List;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/c/d;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/c/d;->c:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-object/from16 v0, p4

    invoke-static {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/navigation/navui/views/h;->a(Lcom/google/android/apps/gmm/navigation/navui/views/MultiIconView;Ljava/util/List;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V

    goto :goto_7

    :cond_3a
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 210
    :pswitch_1d
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;

    if-eqz v2, :cond_3c

    if-eqz p2, :cond_3b

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->g:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/IntermediateStopSchematicView;->invalidate()V

    :cond_3b
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_3c
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    if-eqz v2, :cond_3e

    if-eqz p2, :cond_3d

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->f:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/SegmentSchematicView;->invalidate()V

    :cond_3d
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_3e
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 212
    :pswitch_1e
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;

    if-eqz v2, :cond_40

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/d;

    if-eqz v2, :cond_3f

    check-cast p4, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;

    check-cast p2, Lcom/google/android/apps/gmm/directions/views/d;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->a:Lcom/google/android/apps/gmm/directions/views/d;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->a()V

    :cond_3f
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_40
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;

    if-eqz v2, :cond_42

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/d;

    if-eqz v2, :cond_41

    check-cast p4, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;

    check-cast p2, Lcom/google/android/apps/gmm/directions/views/d;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->a:Lcom/google/android/apps/gmm/directions/views/d;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->a()V

    :cond_41
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_42
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 214
    :pswitch_1f
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;

    if-eqz v2, :cond_43

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->b:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_43
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;

    if-eqz v2, :cond_44

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->b:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/navui/ManeuverImageView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_44
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 216
    :pswitch_20
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;

    if-eqz v2, :cond_46

    if-eqz p2, :cond_45

    check-cast p4, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p4

    iget v3, v0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a:I

    if-eq v3, v2, :cond_45

    if-lez v2, :cond_45

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->a:I

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->requestLayout()V

    :cond_45
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_46
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 218
    :pswitch_21
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;

    if-eqz v2, :cond_48

    if-eqz p2, :cond_47

    check-cast p4, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p4

    iget v3, v0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->b:I

    if-eq v3, v2, :cond_47

    if-lez v2, :cond_47

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->b:I

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/base/views/CompactMultiTextLinearLayout;->requestLayout()V

    :cond_47
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_48
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 220
    :pswitch_22
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;

    if-eqz v2, :cond_4a

    if-eqz p2, :cond_49

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;

    check-cast p2, Lcom/google/android/libraries/curvular/aw;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    if-eq v4, v3, :cond_49

    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->a:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->requestLayout()V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/TransitVehiclesList;->invalidate()V

    :cond_49
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_4a
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 222
    :pswitch_23
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    if-eqz v2, :cond_4c

    if-eqz p2, :cond_4b

    check-cast p4, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setMinScaleX(F)V

    :cond_4b
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_4c
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 224
    :pswitch_24
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    if-eqz v2, :cond_4e

    if-eqz p2, :cond_4d

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    check-cast p2, Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setMinTextSize(F)V

    :cond_4d
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_4e
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 226
    :pswitch_25
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;

    if-eqz v2, :cond_51

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/k/l;

    if-eqz v2, :cond_51

    check-cast p4, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;

    check-cast p2, Lcom/google/android/apps/gmm/base/k/l;

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/base/k/l;->a:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4f

    const/4 v2, 0x1

    :goto_8
    if-eqz v2, :cond_50

    move-object/from16 v0, p2

    iget v2, v0, Lcom/google/android/apps/gmm/base/k/l;->a:F

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->c:F

    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->d:F

    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->e:F

    :goto_9
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_4f
    const/4 v2, 0x0

    goto :goto_8

    :cond_50
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/apps/gmm/base/k/l;->b:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    move-object/from16 v0, p4

    iput v3, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->d:F

    move-object/from16 v0, p2

    iget v3, v0, Lcom/google/android/apps/gmm/base/k/l;->c:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/navui/NextTurnTextView;->e:F

    goto :goto_9

    :cond_51
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 228
    :pswitch_26
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    if-nez v2, :cond_52

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_52
    check-cast p4, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/l;

    if-eqz v2, :cond_53

    check-cast p2, Lcom/google/android/apps/gmm/directions/views/l;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->f:Lcom/google/android/apps/gmm/directions/views/l;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_53
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/libraries/curvular/b/i;

    if-eqz v2, :cond_54

    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    new-instance v2, Lcom/google/android/apps/gmm/base/k/w;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/gmm/base/k/w;-><init>(Lcom/google/android/apps/gmm/base/k/p;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;)V

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->f:Lcom/google/android/apps/gmm/directions/views/l;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_54
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 230
    :pswitch_27
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    if-eqz v2, :cond_56

    check-cast p4, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/m;

    if-eqz v2, :cond_55

    check-cast p2, Lcom/google/android/apps/gmm/directions/views/m;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->e:Lcom/google/android/apps/gmm/directions/views/m;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_55
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/libraries/curvular/b/i;

    if-eqz v2, :cond_57

    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    new-instance v2, Lcom/google/android/apps/gmm/base/k/u;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/gmm/base/k/u;-><init>(Lcom/google/android/apps/gmm/base/k/p;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;)V

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/views/SwipeableDatePicker;->e:Lcom/google/android/apps/gmm/directions/views/m;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_56
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    if-eqz v2, :cond_57

    check-cast p4, Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/libraries/curvular/b/i;

    if-eqz v2, :cond_57

    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    new-instance v2, Lcom/google/android/apps/gmm/base/k/v;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/gmm/base/k/v;-><init>(Lcom/google/android/apps/gmm/base/k/p;Lcom/google/android/libraries/curvular/b/i;Lcom/google/maps/mapsactivities/android/MapsActivityMonthView;)V

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/android/datetimepicker/date/MonthView;->z:Lcom/android/datetimepicker/date/i;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_57
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 232
    :pswitch_28
    move-object/from16 v0, p4

    instance-of v2, v0, Landroid/widget/EditText;

    if-nez v2, :cond_58

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_58
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/libraries/curvular/b/i;

    if-nez v2, :cond_59

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_59
    check-cast p4, Landroid/widget/EditText;

    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    new-instance v2, Lcom/google/android/apps/gmm/base/k/x;

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/apps/gmm/base/k/x;-><init>(Lcom/google/android/apps/gmm/base/k/p;Landroid/widget/EditText;Lcom/google/android/libraries/curvular/b/i;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    .line 234
    :pswitch_29
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/FiveStarView;

    if-eqz v2, :cond_5a

    check-cast p2, Lcom/google/android/libraries/curvular/b/i;

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/FiveStarView;

    new-instance v3, Lcom/google/android/apps/gmm/base/k/t;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/gmm/base/k/t;-><init>(Lcom/google/android/apps/gmm/base/k/p;Lcom/google/android/libraries/curvular/b/i;Landroid/view/View;)V

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/FiveStarView;->e:Lcom/google/android/apps/gmm/base/views/s;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_5a
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 236
    :pswitch_2a
    check-cast p4, Lcom/google/android/libraries/curvular/DelegatingViewGroup;

    check-cast p2, Lcom/google/android/libraries/curvular/at;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/google/android/libraries/curvular/DelegatingViewGroup;->a:Lcom/google/android/libraries/curvular/at;

    const/4 v2, 0x1

    goto/16 :goto_1

    .line 238
    :pswitch_2b
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    if-eqz v2, :cond_5b

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    check-cast p2, Lcom/google/android/apps/gmm/base/views/d;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->h:Lcom/google/android/apps/gmm/base/views/d;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_5b
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 240
    :pswitch_2c
    check-cast p2, Lcom/google/android/apps/gmm/map/b/a/q;

    check-cast p4, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    if-eqz p2, :cond_5c

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->setPinLatLng(Lcom/google/android/apps/gmm/map/b/a/q;)V

    :goto_a
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_5c
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v2, :cond_5d

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->c:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    if-eqz v3, :cond_5d

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_5e

    const/4 v2, 0x1

    :goto_b
    if-eqz v2, :cond_5d

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v2, :cond_5d

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/ah;->b()V

    :cond_5d
    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    goto :goto_a

    :cond_5e
    const/4 v2, 0x0

    goto :goto_b

    .line 242
    :pswitch_2d
    move-object/from16 v0, p4

    instance-of v2, v0, Landroid/widget/TextView;

    if-eqz v2, :cond_60

    if-eqz p2, :cond_5f

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/util/List;

    if-eqz v2, :cond_60

    :cond_5f
    check-cast p4, Landroid/widget/TextView;

    check-cast p2, Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/directions/views/h;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/directions/views/h;-><init>(Ljava/util/List;Landroid/widget/TextView;)V

    sget v3, Lcom/google/android/apps/gmm/g;->cG:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/h;->a()V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/h;->b()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_60
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 244
    :pswitch_2e
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    if-eqz v2, :cond_61

    check-cast p4, Lcom/google/android/apps/gmm/base/views/MapViewContainer;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/views/MapViewContainer;->setInteractive(Z)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_61
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 246
    :pswitch_2f
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    if-eqz v2, :cond_63

    check-cast p4, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    check-cast p2, Lcom/google/android/apps/gmm/base/k/m;

    if-nez p2, :cond_62

    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->c:Lcom/google/android/apps/gmm/map/r/a/ag;

    :goto_c
    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->c:Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, p4

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Lcom/google/android/apps/gmm/navigation/navui/az;

    move-result-object v2

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_62
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/k/m;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->c:Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/k/m;->b:Lcom/google/android/apps/gmm/navigation/navui/ay;

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->d:Lcom/google/android/apps/gmm/navigation/navui/ay;

    goto :goto_c

    :cond_63
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 248
    :pswitch_30
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    if-eqz v2, :cond_66

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/k/n;

    if-eqz v2, :cond_66

    check-cast p4, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    check-cast p2, Lcom/google/android/apps/gmm/base/k/n;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/k/n;->a:Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v2

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/k/n;->b:Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v3

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/k/n;->c:Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v4

    move-object/from16 v0, p2

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/k/n;->d:Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-interface {v5, v6}, Lcom/google/android/libraries/curvular/au;->c_(Landroid/content/Context;)I

    move-result v5

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/k/n;->e:Lcom/google/android/libraries/curvular/au;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/google/android/libraries/curvular/au;->b_(Landroid/content/Context;)I

    move-result v6

    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/base/k/n;->f:F

    move-object/from16 v0, p2

    iget-boolean v8, v0, Lcom/google/android/apps/gmm/base/k/n;->g:Z

    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/google/android/apps/gmm/base/k/n;->h:Lcom/google/android/libraries/curvular/aq;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getContext()Landroid/content/Context;

    move-result-object v10

    invoke-interface {v9, v10}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v9

    move-object/from16 v0, p2

    iget v10, v0, Lcom/google/android/apps/gmm/base/k/n;->i:F

    move-object/from16 v0, p2

    iget v11, v0, Lcom/google/android/apps/gmm/base/k/n;->j:F

    move-object/from16 v0, p2

    iget v12, v0, Lcom/google/android/apps/gmm/base/k/n;->k:F

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/apps/gmm/base/k/n;->l:Landroid/graphics/Typeface;

    move-object/from16 v0, p2

    iget v14, v0, Lcom/google/android/apps/gmm/base/k/n;->m:I

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->f:I

    move-object/from16 v0, p4

    iput v3, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->g:I

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    int-to-float v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setMinTextSize(F)V

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    int-to-float v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setMinTextSize(F)V

    move-object/from16 v0, p4

    iput v5, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->h:I

    move-object/from16 v0, p4

    iput v6, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    move-object/from16 v0, p4

    iput v7, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->j:F

    move-object/from16 v0, p4

    iput-boolean v8, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->k:Z

    move-object/from16 v0, p4

    iput v9, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->l:I

    move-object/from16 v0, p4

    iput v10, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->m:F

    move-object/from16 v0, p4

    iput v11, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->n:F

    move-object/from16 v0, p4

    iput v12, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->o:F

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v2, v13}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setTypeface(Landroid/graphics/Typeface;)V

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v2, v13}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setTypeface(Landroid/graphics/Typeface;)V

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_64

    const/4 v2, 0x1

    :goto_d
    if-eqz v2, :cond_65

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v2, v14}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setTextAlignment(I)V

    move-object/from16 v0, p4

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v2, v14}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setTextAlignment(I)V

    :goto_e
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_64
    const/4 v2, 0x0

    goto :goto_d

    :cond_65
    invoke-static {v14}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a(I)I

    move-result v2

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setGravity(I)V

    move-object/from16 v0, p4

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setGravity(I)V

    goto :goto_e

    :cond_66
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 250
    :pswitch_31
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;

    if-eqz v2, :cond_68

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_67

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p4

    iput v2, v0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->a:I

    :cond_67
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_68
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 252
    :pswitch_32
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    if-eqz v2, :cond_6a

    if-eqz p2, :cond_69

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->c:Landroid/graphics/Paint;

    invoke-virtual {v4, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->invalidate()V

    :cond_69
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_6a
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 254
    :pswitch_33
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;

    if-eqz v2, :cond_6b

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/c/g;

    if-eqz v2, :cond_6b

    check-cast p4, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;

    check-cast p2, Lcom/google/android/apps/gmm/base/views/c/g;

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->setProperties(Lcom/google/android/apps/gmm/base/views/c/g;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_6b
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 256
    :pswitch_34
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    if-eqz v2, :cond_6d

    if-eqz p2, :cond_6c

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    iput v3, v2, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->f:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/NodeSchematicView;->invalidate()V

    :cond_6c
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_6d
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 258
    :pswitch_35
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v2, :cond_6f

    if-eqz p2, :cond_6e

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    :goto_f
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_6e
    check-cast p4, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->g:Ljava/lang/Integer;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    goto :goto_f

    :cond_6f
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 260
    :pswitch_36
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v2, :cond_71

    if-eqz p2, :cond_70

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    :goto_10
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_70
    check-cast p4, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->h:Ljava/lang/Integer;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    goto :goto_10

    :cond_71
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 262
    :pswitch_37
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    if-eqz v2, :cond_73

    if-eqz p2, :cond_72

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->b(Ljava/lang/Object;Landroid/view/View;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    :goto_11
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_72
    check-cast p4, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;

    const/4 v2, 0x0

    move-object/from16 v0, p4

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->f:Ljava/lang/Integer;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/apps/gmm/directions/views/TransitVehicleItem;->a()V

    goto :goto_11

    :cond_73
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 264
    :pswitch_38
    sget v2, Lcom/google/android/libraries/curvular/bh;->g:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/cm;

    if-eqz p2, :cond_74

    sget-object v3, Lcom/google/android/apps/gmm/base/k/o;->h:Lcom/google/android/apps/gmm/base/k/o;

    move-object/from16 v0, p2

    if-ne v0, v3, :cond_77

    :cond_74
    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->cancel()V

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationX(F)V

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationY(F)V

    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->b:Lcom/google/android/libraries/curvular/bk;

    iget-object v4, v2, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    if-eqz v4, :cond_76

    iget-object v4, v2, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    :goto_12
    check-cast v3, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v3, :cond_75

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->b:Lcom/google/android/libraries/curvular/bk;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    :cond_75
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_76
    const/4 v3, 0x0

    goto :goto_12

    :cond_77
    move-object/from16 v0, p2

    instance-of v3, v0, Lcom/google/android/apps/gmm/base/k/o;

    if-eqz v3, :cond_7a

    check-cast p2, Lcom/google/android/apps/gmm/base/k/o;

    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->b:Lcom/google/android/libraries/curvular/bk;

    iget-object v4, v2, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    if-eqz v4, :cond_79

    iget-object v4, v2, Lcom/google/android/libraries/curvular/cm;->h:Ljava/util/Map;

    invoke-interface {v4, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    :goto_13
    check-cast v3, Landroid/view/ViewTreeObserver$OnPreDrawListener;

    if-eqz v3, :cond_78

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :cond_78
    new-instance v3, Lcom/google/android/apps/gmm/base/k/y;

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/apps/gmm/base/k/y;-><init>(Lcom/google/android/apps/gmm/base/k/p;Landroid/view/View;Lcom/google/android/apps/gmm/base/k/o;)V

    invoke-virtual/range {p4 .. p4}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    sget-object v4, Lcom/google/android/apps/gmm/base/k/p;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-virtual {v2, v4, v3}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_79
    const/4 v3, 0x0

    goto :goto_13

    :cond_7a
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 266
    :pswitch_39
    if-nez p2, :cond_7b

    sget-object v2, Lcom/google/android/apps/gmm/base/k/p;->i:Landroid/view/View$OnLayoutChangeListener;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/view/View;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    sget v2, Lcom/google/android/libraries/curvular/bh;->g:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/cm;

    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->c:Lcom/google/android/libraries/curvular/bk;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_7b
    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Number;

    if-eqz v2, :cond_7c

    sget-object v2, Lcom/google/android/apps/gmm/base/k/p;->i:Landroid/view/View$OnLayoutChangeListener;

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    sget v2, Lcom/google/android/libraries/curvular/bh;->g:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/cm;

    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->c:Lcom/google/android/libraries/curvular/bk;

    check-cast p2, Ljava/lang/Number;

    move-object/from16 v0, p2

    invoke-virtual {v2, v3, v0}, Lcom/google/android/libraries/curvular/cm;->a(Lcom/google/android/libraries/curvular/bk;Ljava/lang/Object;)V

    sget-object v2, Lcom/google/android/apps/gmm/base/k/p;->i:Landroid/view/View$OnLayoutChangeListener;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v3, p4

    invoke-interface/range {v2 .. v11}, Landroid/view/View$OnLayoutChangeListener;->onLayoutChange(Landroid/view/View;IIIIIIII)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_7c
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 268
    :pswitch_3a
    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_7d

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/p;->h:Lcom/google/android/apps/gmm/z/a/b;

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/base/k/g;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/a/b;)Lcom/google/android/apps/gmm/base/k/g;

    move-result-object v2

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/base/k/g;->d:Z

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_7d
    if-nez p2, :cond_7e

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_7e
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 270
    :pswitch_3b
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/apps/gmm/z/b/l;

    if-nez v2, :cond_7f

    if-nez p2, :cond_80

    :cond_7f
    check-cast p2, Lcom/google/android/apps/gmm/z/b/l;

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/l;)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/p;->h:Lcom/google/android/apps/gmm/z/a/b;

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/base/k/g;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/a/b;)Lcom/google/android/apps/gmm/base/k/g;

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_80
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 272
    :pswitch_3c
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;

    if-eqz v2, :cond_84

    move-object/from16 v0, p2

    instance-of v2, v0, Ljava/lang/Boolean;

    if-eqz v2, :cond_81

    check-cast p4, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual/range {p2 .. p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->b:Landroid/view/View;

    if-eqz v3, :cond_82

    sget v2, Lcom/google/android/apps/gmm/f;->bh:I

    :goto_14
    invoke-virtual {v4, v2}, Landroid/view/View;->setBackgroundResource(I)V

    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/ScrollableViewDivider;->c:Landroid/view/View;

    if-eqz v3, :cond_83

    sget v2, Lcom/google/android/apps/gmm/d;->u:I

    :goto_15
    invoke-virtual {v4, v2}, Landroid/view/View;->setBackgroundResource(I)V

    :cond_81
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_82
    sget v2, Lcom/google/android/apps/gmm/f;->bg:I

    goto :goto_14

    :cond_83
    sget v2, Lcom/google/android/apps/gmm/d;->t:I

    goto :goto_15

    :cond_84
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 274
    :pswitch_3d
    check-cast p2, Lcom/google/android/apps/gmm/base/views/c/k;

    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    if-eqz v2, :cond_87

    if-nez p2, :cond_85

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_85
    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    if-eqz v3, :cond_86

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/c/k;->c:Lcom/google/android/libraries/curvular/aw;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    :goto_16
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/c/k;->a:Ljava/lang/String;

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/views/c/k;->b:Lcom/google/android/apps/gmm/util/webimageview/b;

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/views/c/k;->e:Lcom/google/android/apps/gmm/util/webimageview/g;

    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/base/views/c/k;->d:I

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/gmm/util/webimageview/BaseWebImageView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/gmm/util/webimageview/g;I)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_86
    const/4 v5, 0x0

    goto :goto_16

    :cond_87
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 278
    :cond_88
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/google/android/libraries/curvular/g;

    if-eqz v2, :cond_0

    .line 279
    sget-object v2, Lcom/google/android/apps/gmm/base/k/r;->b:[I

    check-cast p1, Lcom/google/android/libraries/curvular/g;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/libraries/curvular/g;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto/16 :goto_0

    .line 281
    :pswitch_3e
    move-object/from16 v0, p4

    instance-of v2, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v2, :cond_89

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_89
    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/libraries/curvular/bc;

    if-nez v2, :cond_8a

    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8a
    check-cast p4, Landroid/support/v7/widget/RecyclerView;

    check-cast p2, Lcom/google/android/libraries/curvular/bc;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/p;->e:Lcom/google/android/libraries/curvular/bd;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v2, v0, v1}, Lcom/google/android/libraries/curvular/c/b;->a(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/libraries/curvular/bc;Landroid/support/v7/widget/RecyclerView;)Lcom/google/android/libraries/curvular/c/j;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/base/k/ap;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/k/p;->e:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v3, v4, v2}, Lcom/google/android/apps/gmm/base/k/ap;-><init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/libraries/curvular/c/j;)V

    move-object/from16 v0, p4

    invoke-virtual {v0, v3}, Landroid/support/v7/widget/RecyclerView;->setRecycledViewPool(Landroid/support/v7/widget/bv;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    .line 284
    :pswitch_3f
    invoke-static/range {p4 .. p4}, Lcom/google/android/libraries/curvular/cb;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cb;

    move-result-object v2

    if-eqz p2, :cond_8c

    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->d:Lcom/google/android/libraries/curvular/bk;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/k/p;->f:Landroid/view/View$OnClickListener;

    if-nez v4, :cond_8b

    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->a:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_17
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8b
    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->a:Ljava/util/Map;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_17

    :cond_8c
    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->d:Lcom/google/android/libraries/curvular/bk;

    const/4 v4, 0x0

    if-nez v4, :cond_8d

    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->a:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_17

    :cond_8d
    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->a:Ljava/util/Map;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_17

    .line 286
    :pswitch_40
    invoke-static/range {p4 .. p4}, Lcom/google/android/libraries/curvular/cb;->a(Landroid/view/View;)Lcom/google/android/libraries/curvular/cb;

    move-result-object v2

    if-eqz p2, :cond_8f

    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->d:Lcom/google/android/libraries/curvular/bk;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/k/p;->g:Landroid/view/View$OnLongClickListener;

    if-nez v4, :cond_8e

    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->b:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_18
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8e
    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->b:Ljava/util/Map;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_18

    :cond_8f
    sget-object v3, Lcom/google/android/apps/gmm/base/k/p;->d:Lcom/google/android/libraries/curvular/bk;

    const/4 v4, 0x0

    if-nez v4, :cond_90

    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->b:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_18

    :cond_90
    iget-object v2, v2, Lcom/google/android/libraries/curvular/cb;->b:Ljava/util/Map;

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_18

    .line 288
    :pswitch_41
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;

    if-eqz v2, :cond_91

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->a(Ljava/lang/Object;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->d:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->b()V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/FiveStarTextView;->a()V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_91
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 290
    :pswitch_42
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    if-eqz v2, :cond_92

    if-eqz p2, :cond_92

    move-object/from16 v2, p4

    check-cast v2, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/j;->c(Ljava/lang/Object;Landroid/view/View;)Landroid/content/res/ColorStateList;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setTextColor(Landroid/content/res/ColorStateList;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_92
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
    .end packed-switch

    .line 279
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
    .end packed-switch
.end method
