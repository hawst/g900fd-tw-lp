.class public Lcom/google/android/apps/gmm/navigation/f/c/b;
.super Lcom/google/android/apps/gmm/navigation/commonui/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/commonui/c/e;
.implements Lcom/google/android/apps/gmm/navigation/f/c/a;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field private final a:Lcom/google/android/apps/gmm/navigation/f/q;

.field private final b:Lcom/google/android/apps/gmm/navigation/f/r;

.field private final c:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final d:Lcom/google/android/libraries/curvular/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/navigation/f/c/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

.field private final f:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

.field private final g:Lcom/google/android/apps/gmm/navigation/a/d;

.field private h:Z

.field private i:Lcom/google/android/apps/gmm/navigation/f/b/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/f/c/c;)V
    .locals 3

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/a;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->h:Z

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/navigation/f/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/f/b/b;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/f/b/b;->c()Lcom/google/android/apps/gmm/navigation/f/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->i:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 51
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    const-string v1, "host"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/navigation/f/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    .line 52
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->b:Lcom/google/android/apps/gmm/navigation/f/r;

    const-string v1, "stateController"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/f/r;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->b:Lcom/google/android/apps/gmm/navigation/f/r;

    .line 53
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    const-string v1, "eventBus"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 54
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->e:Lcom/google/android/libraries/curvular/ag;

    const-string v1, "modelBinder"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/android/libraries/curvular/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->d:Lcom/google/android/libraries/curvular/ag;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/commonui/c/g;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/base/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->f:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    .line 57
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->c:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->g:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/f/c/c;->f:Ljava/lang/Runnable;

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->a:Ljava/lang/Runnable;

    .line 61
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->f:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    .line 99
    return-void
.end method

.method public final Z_()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->b:Lcom/google/android/apps/gmm/navigation/f/r;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/f/r;->d()V

    .line 146
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->d:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 87
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ae;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 77
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/j/ae;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->h:Z

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->f:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->h:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->b:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;->c()V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->d:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/f/t;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 65
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/f/t;->a:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 66
    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 73
    :goto_1
    return-void

    :cond_0
    move v2, v0

    .line 66
    goto :goto_0

    .line 70
    :cond_1
    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->i:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 71
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v3, v4, :cond_2

    sget-object v4, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v3, v4, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/navigation/commonui/c/g;->a(Z)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->d:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->f:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    .line 93
    return-void
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->i:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/f/q;->b()V

    .line 135
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->i:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->a:Lcom/google/android/apps/gmm/navigation/f/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/f/q;->a()V

    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/navigation/commonui/c/l;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->f:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/navigation/commonui/c/f;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->e:Lcom/google/android/apps/gmm/navigation/commonui/c/g;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/gmm/navigation/commonui/c/e;
    .locals 0

    .prologue
    .line 162
    return-object p0
.end method

.method public final n()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->g:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->i:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v1, v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final p()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->b:Lcom/google/android/apps/gmm/navigation/f/r;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/f/r;->a(Ljava/lang/Float;)V

    .line 129
    return-object v1
.end method

.method public final q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->i:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 118
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/c/b;->i:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/d;->e:Ljava/lang/String;

    goto :goto_0
.end method
