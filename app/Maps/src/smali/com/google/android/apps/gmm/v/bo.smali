.class public Lcom/google/android/apps/gmm/v/bo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:I

.field m:I

.field public n:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput v0, p0, Lcom/google/android/apps/gmm/v/bo;->d:I

    .line 69
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/bo;->a:Ljava/lang/String;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/v/bo;->b:Ljava/lang/String;

    .line 73
    iput v0, p0, Lcom/google/android/apps/gmm/v/bo;->n:I

    .line 74
    return-void
.end method

.method public static a(ILjava/lang/String;)I
    .locals 3

    .prologue
    .line 101
    invoke-static {p0, p1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    .line 102
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unable to get "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " handle"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_0
    const-string v1, "ShaderProgram"

    const-string v2, "glGetUniformLocation"

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return v0
.end method

.method private b(ILjava/lang/String;)I
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 211
    invoke-static {p1}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 212
    if-eqz v1, :cond_2

    .line 213
    invoke-static {v1, p2}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 214
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 215
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 216
    const v2, 0x8b81

    invoke-static {v1, v2, v0, v6}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 217
    aget v0, v0, v6

    if-nez v0, :cond_2

    .line 218
    const v0, 0x8b31

    if-ne p1, v0, :cond_0

    const-string v0, "VertexShader"

    .line 220
    :goto_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Could not compile shader "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    .line 222
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 226
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Shader compile failed "

    .line 228
    invoke-static {v6}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 218
    :cond_0
    const-string v0, "FragmentShader"

    goto :goto_0

    .line 228
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 231
    :cond_2
    return v1
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 89
    const-string v0, "uMVPMatrix"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/v/bo;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/v/bo;->n:I

    .line 90
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 6

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 159
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/v/bo;->c:Z

    if-ne v0, v3, :cond_1

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/bo;->d:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget v3, v3, Lcom/google/android/apps/gmm/v/aq;->b:I

    if-ne v0, v3, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v2

    .line 166
    :cond_1
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-nez v0, :cond_2

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/bo;->d:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget v3, v3, Lcom/google/android/apps/gmm/v/aq;->b:I

    if-eq v0, v3, :cond_0

    .line 171
    :cond_2
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/bo;->c:Z

    .line 172
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/bo;->c:Z

    if-eqz v0, :cond_a

    .line 173
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-eqz v0, :cond_3

    .line 174
    iput v2, p0, Lcom/google/android/apps/gmm/v/bo;->m:I

    .line 175
    iput v4, p0, Lcom/google/android/apps/gmm/v/bo;->n:I

    .line 178
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/v/bo;->m:I

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    const-string v3, "Attempt to overwrite existing shader program: %s"

    new-array v4, v1, [Ljava/lang/Object;

    iget v5, p0, Lcom/google/android/apps/gmm/v/bo;->m:I

    .line 180
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    .line 178
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3, v4}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_1

    .line 183
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/bo;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/bo;->b:Ljava/lang/String;

    const v4, 0x8b31

    invoke-direct {p0, v4, v0}, Lcom/google/android/apps/gmm/v/bo;->b(ILjava/lang/String;)I

    move-result v4

    if-nez v4, :cond_7

    :cond_6
    :goto_2
    iput v2, p0, Lcom/google/android/apps/gmm/v/bo;->m:I

    .line 193
    :goto_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/ad;->i:Lcom/google/android/apps/gmm/v/aq;

    iget v0, v0, Lcom/google/android/apps/gmm/v/aq;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/bo;->d:I

    move v2, v1

    .line 195
    goto :goto_0

    .line 183
    :cond_7
    const v0, 0x8b30

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/gmm/v/bo;->b(ILjava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {v0, v4}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v4, "ShaderState"

    const-string v5, "glAttachShader"

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v3, "ShaderState"

    const-string v4, "glAttachShader"

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/v/bo;->b(I)V

    invoke-static {v0}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v3, v1, [I

    const v4, 0x8b82

    invoke-static {v0, v4, v3, v2}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v3, v3, v2

    if-eq v3, v1, :cond_9

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v3, "Shader link failed "

    invoke-static {v2}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    move v2, v0

    goto :goto_2

    .line 185
    :cond_a
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v0, :cond_b

    .line 186
    iget v0, p0, Lcom/google/android/apps/gmm/v/bo;->m:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 191
    :goto_5
    iput v2, p0, Lcom/google/android/apps/gmm/v/bo;->m:I

    goto :goto_3

    .line 189
    :cond_b
    iput v4, p0, Lcom/google/android/apps/gmm/v/bo;->n:I

    goto :goto_5
.end method

.method public b(I)V
    .locals 2

    .prologue
    .line 121
    const/4 v0, 0x0

    const-string v1, "aPosition"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 122
    const/4 v0, 0x1

    const-string v1, "aNormal"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 126
    const/4 v0, 0x2

    const-string v1, "aColor"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 130
    const/4 v0, 0x4

    const-string v1, "aTextureCoord"

    invoke-static {p1, v0, v1}, Landroid/opengl/GLES20;->glBindAttribLocation(IILjava/lang/String;)V

    .line 134
    return-void
.end method
