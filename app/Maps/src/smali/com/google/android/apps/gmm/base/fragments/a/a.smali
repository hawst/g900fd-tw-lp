.class public final enum Lcom/google/android/apps/gmm/base/fragments/a/a;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/fragments/a/a;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/fragments/a/a;

.field public static final enum b:Lcom/google/android/apps/gmm/base/fragments/a/a;

.field public static final enum c:Lcom/google/android/apps/gmm/base/fragments/a/a;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/base/fragments/a/a;


# instance fields
.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 14
    new-instance v0, Lcom/google/android/apps/gmm/base/fragments/a/a;

    const-string v1, "ACTIVITY_FRAGMENT"

    const-string v2, "activity-fragment"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/base/fragments/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/base/fragments/a/a;

    const-string v1, "LAYERED_FRAGMENT"

    const-string v2, "layered-fragment"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/base/fragments/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/base/fragments/a/a;

    const-string v1, "DIALOG_FRAGMENT"

    const-string v2, "dialog-fragment"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/base/fragments/a/a;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 8
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/fragments/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->e:[Lcom/google/android/apps/gmm/base/fragments/a/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    .line 35
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/fragments/a/a;
    .locals 1

    .prologue
    .line 8
    const-class v0, Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/a/a;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/fragments/a/a;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->e:[Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/fragments/a/a;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/fragments/a/a;

    return-object v0
.end method
