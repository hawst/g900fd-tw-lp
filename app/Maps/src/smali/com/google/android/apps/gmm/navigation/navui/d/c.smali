.class public Lcom/google/android/apps/gmm/navigation/navui/d/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/b/f;
.implements Lcom/google/android/apps/gmm/navigation/navui/d/a;


# instance fields
.field private final A:Z

.field private B:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private C:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private D:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final E:Ljava/lang/Runnable;

.field private F:Lcom/google/android/libraries/curvular/ct;

.field final a:Lcom/google/android/apps/gmm/map/r/a/ag;

.field b:Lcom/google/android/apps/gmm/base/activities/x;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field c:Landroid/view/View;

.field private final d:Lcom/google/android/apps/gmm/directions/h/h;

.field private final e:Lcom/google/android/apps/gmm/navigation/navui/ay;

.field private final f:Lcom/google/android/apps/gmm/shared/c/c/c;

.field private final g:Lcom/google/android/apps/gmm/navigation/navui/d/b;

.field private final h:Z

.field private i:Z

.field private j:Z

.field private final k:Lcom/google/android/apps/gmm/directions/views/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

.field private final m:Lcom/google/android/apps/gmm/navigation/navui/views/d;

.field private final n:Lcom/google/android/apps/gmm/navigation/navui/views/f;

.field private final o:Lcom/google/android/apps/gmm/navigation/navui/views/f;

.field private p:Lcom/google/android/apps/gmm/navigation/g/b/k;

.field private q:Ljava/lang/CharSequence;

.field private r:Ljava/lang/CharSequence;

.field private s:Z

.field private final t:Lcom/google/android/apps/gmm/base/k/m;

.field private final u:Lcom/google/android/apps/gmm/directions/views/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private v:Lcom/google/android/apps/gmm/base/views/c/d;

.field private w:Z

.field private final x:Z

.field private final y:Z

.field private z:Lcom/google/android/apps/gmm/navigation/navui/d/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/ay;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/navigation/navui/d/b;Lcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;ZZZZLcom/google/android/apps/gmm/navigation/navui/d/a;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 7

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 153
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->e:Lcom/google/android/apps/gmm/navigation/navui/ay;

    .line 154
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->f:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 155
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->g:Lcom/google/android/apps/gmm/navigation/navui/d/b;

    .line 157
    iget-object v1, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-ne p1, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    .line 158
    invoke-static {p1}, Lcom/google/android/apps/gmm/directions/views/c;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lcom/google/android/apps/gmm/directions/views/c;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)Lcom/google/android/apps/gmm/directions/views/d;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->k:Lcom/google/android/apps/gmm/directions/views/d;

    .line 159
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 160
    iput-object p7, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->m:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 161
    new-instance v1, Lcom/google/android/apps/gmm/base/k/m;

    invoke-direct {v1, p1, p3}, Lcom/google/android/apps/gmm/base/k/m;-><init>(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/navui/ay;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->t:Lcom/google/android/apps/gmm/base/k/m;

    .line 162
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    if-eqz v1, :cond_5

    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/views/c;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v2}, Lcom/google/android/apps/gmm/directions/views/c;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)Lcom/google/android/apps/gmm/directions/views/d;

    move-result-object v1

    :goto_3
    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->u:Lcom/google/android/apps/gmm/directions/views/d;

    .line 163
    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    invoke-virtual {p8, v1, v2}, Lcom/google/android/apps/gmm/navigation/navui/views/g;->a(ZZ)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->n:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    .line 164
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    invoke-virtual {p8, v1, v2}, Lcom/google/android/apps/gmm/navigation/navui/views/g;->a(ZZ)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->o:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    .line 165
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->y:Z

    .line 166
    move/from16 v0, p12

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->A:Z

    .line 167
    if-nez p15, :cond_6

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    .line 157
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 158
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 162
    :cond_2
    iget v3, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    const/16 v4, 0x3c

    if-le v3, v4, :cond_3

    iget v3, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    const/16 v4, 0x1f4

    if-le v3, v4, :cond_3

    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v3, Lcom/google/maps/g/a/ez;->q:Lcom/google/maps/g/a/ez;

    if-ne v1, v3, :cond_4

    const/4 v1, 0x0

    goto :goto_2

    :cond_4
    const/4 v1, 0x1

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 167
    :cond_6
    check-cast p15, Ljava/lang/Runnable;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->E:Ljava/lang/Runnable;

    .line 168
    iget-object v1, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 170
    const/4 v4, 0x0

    move-object v1, p1

    move-object v2, p4

    move/from16 v5, p9

    move-object/from16 v6, p14

    invoke-static/range {v1 .. v6}, Lcom/google/android/apps/gmm/directions/i/n;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/maps/g/a/al;Lcom/google/android/apps/gmm/map/i/a/a;ZLjava/lang/Runnable;)Lcom/google/android/apps/gmm/directions/i/n;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->d:Lcom/google/android/apps/gmm/directions/h/h;

    .line 172
    move/from16 v0, p9

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a(Z)V

    .line 173
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->x:Z

    .line 174
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->s:Z

    .line 176
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    if-eqz v1, :cond_7

    .line 177
    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    .line 197
    :goto_4
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->z:Lcom/google/android/apps/gmm/navigation/navui/d/a;

    .line 198
    return-void

    .line 179
    :cond_7
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->i:Z

    .line 180
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    .line 181
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    .line 182
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    .line 183
    const/4 v1, 0x0

    .line 184
    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/r/a/an;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->j:Z

    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C()V

    goto :goto_4
.end method

.method private B()V
    .locals 19

    .prologue
    .line 248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->e:Lcom/google/android/apps/gmm/navigation/navui/ay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 249
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 250
    iget-boolean v4, v4, Lcom/google/android/apps/gmm/navigation/navui/d/e;->a:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    if-eqz v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 251
    iget v5, v5, Lcom/google/android/apps/gmm/navigation/navui/d/e;->c:I

    :goto_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 252
    iget v7, v6, Lcom/google/android/apps/gmm/navigation/navui/d/e;->e:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 253
    iget v8, v6, Lcom/google/android/apps/gmm/navigation/navui/d/e;->f:F

    const/4 v9, 0x0

    .line 248
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ag;->a()Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v3

    const/4 v6, 0x1

    invoke-virtual/range {v2 .. v9}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ai;ZIZFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Landroid/text/Spannable;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/navui/ay;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    sget v4, Lcom/google/android/apps/gmm/l;->dW:I

    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v6, v2, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v6, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v2, v4}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v3, v2, v4

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    .line 255
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->e:Lcom/google/android/apps/gmm/navigation/navui/ay;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 257
    iget v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 258
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 259
    iget-boolean v11, v2, Lcom/google/android/apps/gmm/navigation/navui/d/e;->a:Z

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 260
    iget v12, v2, Lcom/google/android/apps/gmm/navigation/navui/d/e;->c:I

    :goto_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 261
    iget v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/e;->i:I

    move/from16 v17, v2

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 262
    iget v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/e;->g:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 263
    iget v14, v5, Lcom/google/android/apps/gmm/navigation/navui/d/e;->e:F

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 264
    iget v15, v5, Lcom/google/android/apps/gmm/navigation/navui/d/e;->f:F

    const/16 v16, 0x0

    .line 255
    new-instance v8, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v5, v8, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v6, v2}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v8, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v5, v12}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, v9, Lcom/google/android/apps/gmm/navigation/navui/ay;->b:Lcom/google/android/apps/gmm/shared/c/c/c;

    const/4 v5, 0x1

    const/4 v6, 0x1

    if-eqz v11, :cond_7

    sget-object v7, Lcom/google/android/apps/gmm/navigation/navui/ay;->g:Lcom/google/android/apps/gmm/shared/c/c/j;

    :goto_3
    invoke-virtual/range {v2 .. v8}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/r/a/ag;->a()Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_4
    const/4 v10, 0x0

    move-object/from16 v0, v18

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->o:Lcom/google/android/apps/gmm/map/r/a/ai;

    if-eqz v3, :cond_9

    if-eqz v2, :cond_0

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/r/a/ag;->a()Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ai;->c()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_9

    :cond_0
    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->o:Lcom/google/android/apps/gmm/map/r/a/ai;

    :cond_1
    :goto_5
    if-nez v10, :cond_a

    const-string v2, ""

    :goto_6
    if-eqz v10, :cond_2

    invoke-virtual {v10}, Lcom/google/android/apps/gmm/map/r/a/ai;->e()Z

    move-result v3

    if-nez v3, :cond_b

    :cond_2
    iget-object v3, v9, Lcom/google/android/apps/gmm/navigation/navui/ay;->a:Landroid/content/Context;

    move-object/from16 v0, v18

    move/from16 v1, v17

    invoke-static {v3, v0, v1}, Lcom/google/android/apps/gmm/directions/views/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ag;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    if-eqz v3, :cond_b

    iget-object v5, v9, Lcom/google/android/apps/gmm/navigation/navui/ay;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    const/high16 v5, 0x3f800000    # 1.0f

    const-string v6, "\u00a0"

    new-instance v7, Lcom/google/android/apps/gmm/shared/c/c/d;

    invoke-direct {v7, v3, v5}, Lcom/google/android/apps/gmm/shared/c/c/d;-><init>(Landroid/graphics/drawable/Drawable;F)V

    invoke-static {v7, v6}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(Lcom/google/android/apps/gmm/shared/c/c/d;Ljava/lang/String;)Landroid/text/Spannable;

    move-result-object v3

    :goto_7
    if-eqz v3, :cond_3

    iget-object v5, v9, Lcom/google/android/apps/gmm/navigation/navui/ay;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    new-instance v6, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v6, v5, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    const-string v3, " "

    const-string v5, "%s"

    invoke-virtual {v6, v5}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v5, v6, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    const-string v3, "%s"

    invoke-virtual {v6, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    iput-object v3, v6, Lcom/google/android/apps/gmm/shared/c/c/i;->b:Ljava/lang/Object;

    const-string v2, "%s"

    invoke-virtual {v6, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    :cond_3
    iget-object v3, v9, Lcom/google/android/apps/gmm/navigation/navui/ay;->f:Lcom/google/android/apps/gmm/shared/c/c/e;

    sget v5, Lcom/google/android/apps/gmm/l;->dX:I

    new-instance v6, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v7, v3, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v7, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v3, v5}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    iget-object v3, v6, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    iget-object v7, v8, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iput-object v3, v6, Lcom/google/android/apps/gmm/shared/c/c/h;->a:Lcom/google/android/apps/gmm/shared/c/c/j;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v2, v3, v4

    invoke-virtual {v6, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    .line 266
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, " "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    aput-object v4, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    .line 267
    return-void

    .line 251
    :cond_4
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    iget v5, v5, Lcom/google/android/apps/gmm/navigation/navui/d/e;->b:I

    goto/16 :goto_0

    .line 260
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    iget v12, v2, Lcom/google/android/apps/gmm/navigation/navui/d/e;->b:I

    goto/16 :goto_1

    .line 261
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    iget v2, v2, Lcom/google/android/apps/gmm/navigation/navui/d/e;->h:I

    move/from16 v17, v2

    goto/16 :goto_2

    .line 255
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_9
    if-eqz v2, :cond_1

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/apps/gmm/map/r/a/ag;->a()Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v10

    goto/16 :goto_5

    :cond_a
    const/4 v13, 0x1

    invoke-virtual/range {v9 .. v16}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ai;ZIZFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Landroid/text/Spannable;

    move-result-object v2

    goto/16 :goto_6

    :cond_b
    const/4 v3, 0x0

    goto/16 :goto_7
.end method

.method private C()V
    .locals 11

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 271
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 273
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    .line 275
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->s:Z

    if-eqz v4, :cond_1

    .line 276
    :goto_1
    if-gtz v1, :cond_2

    .line 277
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q:Ljava/lang/CharSequence;

    .line 288
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->f:Lcom/google/android/apps/gmm/shared/c/c/c;

    const/4 v4, 0x0

    move-object v5, v7

    move-object v6, v7

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->r:Ljava/lang/CharSequence;

    .line 290
    return-void

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 272
    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    goto :goto_0

    :cond_1
    move v1, v0

    .line 275
    goto :goto_1

    .line 279
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 280
    iget v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/e;->c:I

    move v4, v0

    .line 282
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->f:Lcom/google/android/apps/gmm/shared/c/c/c;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 284
    iget-boolean v5, v5, Lcom/google/android/apps/gmm/navigation/navui/d/e;->a:Z

    if-eqz v5, :cond_4

    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v6, v5, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v8, Landroid/text/style/StyleSpan;

    invoke-direct {v8, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    new-instance v6, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v8, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 285
    iget v8, v8, Lcom/google/android/apps/gmm/navigation/navui/d/e;->d:F

    iget-object v9, v6, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v10, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v10, v8}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    iget-object v8, v6, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v9, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v4, v3

    .line 282
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q:Ljava/lang/CharSequence;

    goto :goto_2

    .line 280
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->l:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/e;->b:I

    move v4, v0

    goto :goto_3

    :cond_4
    move-object v5, v7

    .line 284
    goto :goto_4
.end method


# virtual methods
.method public final A()Lcom/google/android/libraries/curvular/ct;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->F:Lcom/google/android/libraries/curvular/ct;

    if-nez v0, :cond_0

    .line 456
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/d/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/d/d;-><init>(Lcom/google/android/apps/gmm/navigation/navui/d/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->F:Lcom/google/android/libraries/curvular/ct;

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->F:Lcom/google/android/libraries/curvular/ct;

    return-object v0
.end method

.method public final a()Lcom/google/android/apps/gmm/directions/h/h;
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->d:Lcom/google/android/apps/gmm/directions/h/h;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/x;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/base/activities/x;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->b:Lcom/google/android/apps/gmm/base/activities/x;

    if-eqz v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->b:Lcom/google/android/apps/gmm/base/activities/x;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/activities/x;->a:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 447
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->b:Lcom/google/android/apps/gmm/base/activities/x;

    .line 448
    if-eqz p1, :cond_1

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->c:Landroid/view/View;

    iput-object v0, p1, Lcom/google/android/apps/gmm/base/activities/x;->a:Landroid/view/View;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 451
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->E:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 496
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 207
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->s:Z

    .line 208
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->i:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->i:Z

    .line 209
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/navui/b/a;->f:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 210
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v3, v3, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v3, v4, v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->p:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 212
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C()V

    .line 214
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    if-nez v3, :cond_1

    .line 245
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    new-instance v3, Lcom/google/android/apps/gmm/navigation/navui/d/i;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/navigation/navui/d/i;-><init>(Lcom/google/android/apps/gmm/navigation/navui/d/a;)V

    .line 220
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->i:Z

    if-eqz v4, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v4, v0

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    const/16 v4, 0x1324

    if-le v0, v4, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    .line 221
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 222
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/r/a/ag;->a()Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 224
    :goto_3
    if-eqz v0, :cond_6

    if-eqz v1, :cond_6

    .line 225
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B()V

    .line 232
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->i:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/r/a/an;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->j:Z

    .line 241
    invoke-interface {v3}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->q()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q()Ljava/lang/Boolean;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 242
    invoke-interface {v3}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->o()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->o()Ljava/lang/Boolean;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 243
    :cond_2
    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->z:Lcom/google/android/apps/gmm/navigation/navui/d/a;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 220
    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v1, v2

    .line 222
    goto :goto_3

    .line 227
    :cond_6
    iput-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    .line 228
    iput-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    .line 229
    iput-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    goto :goto_4
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 408
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->v:Lcom/google/android/apps/gmm/base/views/c/d;

    if-nez v0, :cond_2

    .line 409
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->w:Ljava/util/List;

    .line 411
    :goto_1
    new-instance v4, Lcom/google/android/apps/gmm/base/views/c/d;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->m:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 412
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->o:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    :goto_2
    invoke-direct {v4, v0, v5, v1}, Lcom/google/android/apps/gmm/base/views/c/d;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->v:Lcom/google/android/apps/gmm/base/views/c/d;

    .line 413
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->d:Lcom/google/android/apps/gmm/directions/h/h;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/directions/h/h;->a(Z)V

    .line 414
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    move v0, v2

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 415
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B()V

    .line 418
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_7

    :goto_4
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 419
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C()V

    .line 422
    :cond_2
    return-void

    :cond_3
    move v0, v3

    .line 410
    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 412
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->n:Lcom/google/android/apps/gmm/navigation/navui/views/f;

    goto :goto_2

    :cond_6
    move v0, v3

    .line 414
    goto :goto_3

    :cond_7
    move v2, v3

    .line 418
    goto :goto_4
.end method

.method public final b()Lcom/google/android/apps/gmm/navigation/navui/d/a;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->z:Lcom/google/android/apps/gmm/navigation/navui/d/a;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->g:Lcom/google/android/apps/gmm/navigation/navui/d/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 315
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->g:Lcom/google/android/apps/gmm/navigation/navui/d/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/d/b;->b(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 321
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 326
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->k:Lcom/google/android/apps/gmm/directions/views/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/apps/gmm/directions/views/d;
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->k:Lcom/google/android/apps/gmm/directions/views/d;

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->r:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 366
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/base/k/m;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->t:Lcom/google/android/apps/gmm/base/k/m;

    return-object v0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->u:Lcom/google/android/apps/gmm/directions/views/d;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->i:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->j:Z

    if-eqz v0, :cond_1

    .line 429
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 430
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/d/c;->q()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 426
    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 429
    goto :goto_0

    :cond_1
    move v1, v2

    .line 430
    goto :goto_1
.end method

.method public final p()Lcom/google/android/apps/gmm/directions/views/d;
    .locals 1

    .prologue
    .line 435
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->u:Lcom/google/android/apps/gmm/directions/views/d;

    return-object v0
.end method

.method public final q()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->v:Lcom/google/android/apps/gmm/base/views/c/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/c/d;->a:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 398
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->y:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->A:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/google/android/apps/gmm/base/views/c/d;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->v:Lcom/google/android/apps/gmm/base/views/c/d;

    return-object v0
.end method

.method public final u()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->D:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final w()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->B:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final x()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 490
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->C:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final y()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 388
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->w:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 393
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->x:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
