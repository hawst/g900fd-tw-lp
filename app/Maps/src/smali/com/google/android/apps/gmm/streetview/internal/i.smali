.class final Lcom/google/android/apps/gmm/streetview/internal/i;
.super Ljava/io/FilterInputStream;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/streetview/internal/j;

.field private final b:Lorg/apache/http/client/methods/AbortableHttpRequest;


# direct methods
.method private constructor <init>(Ljava/io/InputStream;Lcom/google/android/apps/gmm/streetview/internal/j;Lorg/apache/http/client/methods/AbortableHttpRequest;)V
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 158
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/i;->a:Lcom/google/android/apps/gmm/streetview/internal/j;

    .line 159
    iput-object p3, p0, Lcom/google/android/apps/gmm/streetview/internal/i;->b:Lorg/apache/http/client/methods/AbortableHttpRequest;

    .line 160
    if-nez p3, :cond_0

    .line 161
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "abortableRequest"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    return-void
.end method

.method public static a(Ljava/io/InputStream;Lcom/google/android/apps/gmm/streetview/internal/j;Lorg/apache/http/client/methods/AbortableHttpRequest;)Ljava/io/InputStream;
    .locals 1

    .prologue
    .line 144
    if-nez p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/streetview/internal/i;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/streetview/internal/i;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/gmm/streetview/internal/j;Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final close()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/i;->a:Lcom/google/android/apps/gmm/streetview/internal/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/i;->b:Lorg/apache/http/client/methods/AbortableHttpRequest;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/j;->b(Lorg/apache/http/client/methods/AbortableHttpRequest;)V

    .line 169
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 170
    return-void
.end method
