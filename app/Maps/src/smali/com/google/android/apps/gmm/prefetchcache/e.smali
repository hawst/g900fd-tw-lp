.class Lcom/google/android/apps/gmm/prefetchcache/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/activities/y;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V
    .locals 0

    .prologue
    .line 119
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    :goto_0
    return-void

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->b:Lcom/google/r/b/a/a/af;

    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetchcache/y;->a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/r;I)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    iput v2, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v1}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    goto :goto_0

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    if-eqz v0, :cond_2

    .line 131
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    const/16 v1, 0x64

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;I)V

    goto :goto_0

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-static {}, Lcom/google/android/apps/gmm/shared/c/h;->b()J

    move-result-wide v4

    const-wide/32 v6, 0x6400000

    cmp-long v0, v4, v6

    if-ltz v0, :cond_3

    move v0, v1

    :goto_1
    if-nez v0, :cond_5

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->c:Z

    if-eqz v1, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    const-string v1, "This dialog isn\'t supported after prefetch started"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    move v0, v2

    .line 134
    goto :goto_1

    .line 136
    :cond_4
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/gmm/l;->jt:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->js:I

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->jE:I

    new-instance v3, Lcom/google/android/apps/gmm/prefetchcache/m;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/prefetchcache/m;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/prefetchcache/l;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/prefetchcache/l;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a(Landroid/app/AlertDialog;)V

    goto/16 :goto_0

    .line 138
    :cond_5
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a:Ljava/lang/String;

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/e;->a:Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a()V

    goto/16 :goto_0
.end method
