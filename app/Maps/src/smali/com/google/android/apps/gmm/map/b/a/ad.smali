.class public Lcom/google/android/apps/gmm/map/b/a/ad;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final synthetic b:Z


# instance fields
.field public a:I

.field private c:[I

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 839
    const-class v0, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/gmm/map/b/a/ad;->b:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 845
    const/16 v0, 0x10

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>(I)V

    .line 846
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 842
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->d:Z

    .line 849
    sget-boolean v0, Lcom/google/android/apps/gmm/map/b/a/ad;->b:Z

    if-nez v0, :cond_0

    if-gtz p1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 850
    :cond_0
    mul-int/lit8 v0, p1, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    .line 852
    iput v1, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    .line 853
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/ab;
    .locals 4

    .prologue
    const/4 v2, 0x3

    const/4 v3, 0x0

    .line 912
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->d:Z

    if-eqz v0, :cond_0

    .line 915
    const/4 v0, 0x6

    new-array v0, v0, [I

    .line 916
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 917
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    invoke-static {v1, v3, v0, v2, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 922
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;-><init>([I)V

    return-object v1

    .line 919
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    mul-int/lit8 v0, v0, 0x3

    new-array v0, v0, [I

    .line 920
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    mul-int/lit8 v2, v2, 0x3

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 5

    .prologue
    .line 896
    mul-int/lit8 v0, p1, 0x3

    .line 897
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    aget v2, v2, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    add-int/lit8 v0, v0, 0x2

    aget v0, v4, v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(III)V

    return-object v1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 888
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v4, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    mul-int/lit8 v5, v5, 0x3

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    array-length v6, v6

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    array-length v5, v5

    shl-int/lit8 v5, v5, 0x1

    new-array v5, v5, [I

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    iget v7, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    mul-int/lit8 v7, v7, 0x3

    invoke-static {v6, v0, v5, v0, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v5, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    :cond_0
    iget v5, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    mul-int/lit8 v5, v5, 0x3

    iget v6, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    if-lez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    add-int/lit8 v7, v5, -0x3

    aget v6, v6, v7

    if-ne v2, v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    add-int/lit8 v7, v5, -0x2

    aget v6, v6, v7

    if-ne v3, v6, :cond_1

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    add-int/lit8 v7, v5, -0x1

    aget v6, v6, v7

    if-ne v4, v6, :cond_1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->d:Z

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    aput v2, v0, v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    add-int/lit8 v2, v5, 0x1

    aput v3, v0, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->c:[I

    add-int/lit8 v2, v5, 0x2

    aput v4, v0, v2

    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    move v0, v1

    goto :goto_0
.end method
