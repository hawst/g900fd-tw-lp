.class public final Lcom/google/android/apps/gmm/car/j/y;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/libraries/curvular/bk;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/y;->a:Lcom/google/android/libraries/curvular/bk;

    .line 29
    iput p1, p0, Lcom/google/android/apps/gmm/car/j/y;->b:I

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/bk;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/libraries/curvular/bk;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/j/y;->a:Lcom/google/android/libraries/curvular/bk;

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/car/j/y;->b:I

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/y;->a:Lcom/google/android/libraries/curvular/bk;

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/car/j/y;->b:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 71
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->isClickable()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    return-object v0

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/y;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {p1, v0}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
