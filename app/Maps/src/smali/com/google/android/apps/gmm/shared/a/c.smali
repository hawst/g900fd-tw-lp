.class public Lcom/google/android/apps/gmm/shared/a/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/apps/gmm/i/a;

.field final c:Lcom/google/android/apps/gmm/shared/a/e;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final d:Lcom/google/android/gms/common/api/v;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/gms/common/api/v",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/apps/gmm/shared/a/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/shared/a/c;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/i/a;Lcom/google/android/apps/gmm/shared/a/e;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/shared/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/shared/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/shared/a/d;-><init>(Lcom/google/android/apps/gmm/shared/a/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/a/c;->d:Lcom/google/android/gms/common/api/v;

    .line 92
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/a/c;->b:Lcom/google/android/apps/gmm/i/a;

    .line 93
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/a/c;->c:Lcom/google/android/apps/gmm/shared/a/e;

    .line 94
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/a/e;)Lcom/google/android/apps/gmm/shared/a/c;
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/shared/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 79
    invoke-static {p0}, Lcom/google/android/apps/gmm/i/a;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/i/a;

    move-result-object v1

    .line 80
    if-nez v1, :cond_0

    .line 81
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    .line 83
    :cond_0
    sget-object v0, Lcom/google/android/gms/feedback/a;->a:Lcom/google/android/gms/common/api/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/i/a;->a(Lcom/google/android/gms/common/api/a;)Lcom/google/android/apps/gmm/i/a;

    .line 84
    invoke-static {}, Lcom/google/android/apps/gmm/i/a;->c()Lcom/google/android/gms/common/api/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/i/a;->a(Lcom/google/android/gms/common/api/q;)Lcom/google/android/apps/gmm/i/a;

    .line 86
    invoke-static {}, Lcom/google/android/apps/gmm/i/a;->d()Lcom/google/android/gms/common/api/r;

    move-result-object v0

    .line 85
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/i/a;->a(Lcom/google/android/gms/common/api/r;)Lcom/google/android/apps/gmm/i/a;

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/shared/a/c;

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/gmm/shared/a/c;-><init>(Lcom/google/android/apps/gmm/i/a;Lcom/google/android/apps/gmm/shared/a/e;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108
    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/a/c;->b:Lcom/google/android/apps/gmm/i/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    if-nez v3, :cond_0

    iget-object v3, v2, Lcom/google/android/apps/gmm/i/a;->b:Lcom/google/android/gms/common/api/p;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    :cond_0
    iget-object v3, v2, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    .line 109
    sget-object v2, Lcom/google/android/apps/gmm/shared/a/c;->a:Ljava/lang/String;

    invoke-interface {v3}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v4

    if-eqz p2, :cond_2

    move v2, v0

    :goto_0
    if-eqz p1, :cond_1

    move v1, v0

    :cond_1
    if-eqz p1, :cond_3

    .line 113
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x27

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, ", original size:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, "x"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x4d

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Feedback(using Gcore isConnected="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", hasBundle="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", hasScreenshot="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    invoke-static {v3, p1, p2, p3}, Lcom/google/android/gms/feedback/a;->a(Lcom/google/android/gms/common/api/o;Landroid/graphics/Bitmap;Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/android/gms/common/api/s;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/a/c;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/s;->a(Lcom/google/android/gms/common/api/v;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/a/c;->b:Lcom/google/android/apps/gmm/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/i/a;->a()V

    .line 122
    return-void

    :cond_2
    move v2, v1

    .line 109
    goto :goto_0

    .line 113
    :cond_3
    const-string v0, ""

    goto :goto_1
.end method
