.class public Lcom/google/android/apps/gmm/iamhere/e/c;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/o/e/b;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 79
    iget v1, p0, Lcom/google/o/e/b;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_0

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 81
    :cond_1
    invoke-virtual {p0}, Lcom/google/o/e/b;->l()[B

    move-result-object v0

    .line 83
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/iamhere/e/c;->a(Lcom/google/o/e/b;[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The place id is not normalized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 86
    :cond_2
    const/16 v1, 0xb

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/o/e/b;[B)Z
    .locals 3

    .prologue
    .line 36
    invoke-static {}, Lcom/google/o/e/b;->newBuilder()Lcom/google/o/e/d;

    move-result-object v1

    iget-object v0, p0, Lcom/google/o/e/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ds;->d()Lcom/google/d/a/a/ds;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    invoke-virtual {v1, v0}, Lcom/google/o/e/d;->a(Lcom/google/d/a/a/ds;)Lcom/google/o/e/d;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/google/o/e/b;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 38
    invoke-virtual {p0}, Lcom/google/o/e/b;->d()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/o/e/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/o/e/d;->a:I

    iput-object v1, v0, Lcom/google/o/e/d;->b:Ljava/lang/Object;

    .line 40
    :cond_1
    invoke-virtual {v0}, Lcom/google/o/e/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/e/b;

    invoke-virtual {v0}, Lcom/google/o/e/b;->l()[B

    move-result-object v0

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    return v0
.end method
