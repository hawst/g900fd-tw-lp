.class public final enum Lcom/google/android/apps/gmm/map/ag;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/ag;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/ag;

.field public static final enum b:Lcom/google/android/apps/gmm/map/ag;

.field public static final enum c:Lcom/google/android/apps/gmm/map/ag;

.field public static final enum d:Lcom/google/android/apps/gmm/map/ag;

.field public static final enum e:Lcom/google/android/apps/gmm/map/ag;

.field public static final enum f:Lcom/google/android/apps/gmm/map/ag;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/map/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 7
    new-instance v0, Lcom/google/android/apps/gmm/map/ag;

    const-string v1, "NORMAL"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    .line 8
    new-instance v0, Lcom/google/android/apps/gmm/map/ag;

    const-string v1, "AD"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/ag;->b:Lcom/google/android/apps/gmm/map/ag;

    .line 9
    new-instance v0, Lcom/google/android/apps/gmm/map/ag;

    const-string v1, "MINI"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/ag;->c:Lcom/google/android/apps/gmm/map/ag;

    .line 10
    new-instance v0, Lcom/google/android/apps/gmm/map/ag;

    const-string v1, "SANTA"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/map/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/ag;->d:Lcom/google/android/apps/gmm/map/ag;

    .line 11
    new-instance v0, Lcom/google/android/apps/gmm/map/ag;

    const-string v1, "NORTH_POLE"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/map/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/ag;->e:Lcom/google/android/apps/gmm/map/ag;

    .line 12
    new-instance v0, Lcom/google/android/apps/gmm/map/ag;

    const-string v1, "NORTH_POLE_SANTA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/ag;->f:Lcom/google/android/apps/gmm/map/ag;

    .line 6
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/ag;

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->b:Lcom/google/android/apps/gmm/map/ag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->c:Lcom/google/android/apps/gmm/map/ag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->d:Lcom/google/android/apps/gmm/map/ag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->e:Lcom/google/android/apps/gmm/map/ag;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/ag;->f:Lcom/google/android/apps/gmm/map/ag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/ag;->g:[Lcom/google/android/apps/gmm/map/ag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 6
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/ag;
    .locals 1

    .prologue
    .line 6
    const-class v0, Lcom/google/android/apps/gmm/map/ag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/ag;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/ag;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Lcom/google/android/apps/gmm/map/ag;->g:[Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/ag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/ag;

    return-object v0
.end method
