.class public Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;
.super Landroid/app/Activity;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 52
    sget-object v0, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x16

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Got result "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 53
    if-ne p2, v2, :cond_0

    .line 54
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->setResult(I)V

    .line 58
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->finish()V

    .line 59
    return-void

    .line 56
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->setResult(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 25
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 26
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 28
    :goto_0
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->ag:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v3

    .line 30
    const-string v0, "location"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 31
    const-string v4, "gps"

    invoke-virtual {v0, v4}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    .line 33
    if-eqz v1, :cond_1

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 35
    sget-object v0, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->a:Ljava/lang/String;

    .line 36
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->setResult(I)V

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->finish()V

    .line 48
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 26
    goto :goto_0

    .line 40
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->a:Ljava/lang/String;

    .line 41
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v3, "googlenav.tos"

    const-string v4, ""

    const/4 v5, 0x0

    .line 43
    invoke-static {v3, v4, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 44
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-class v5, Lcom/google/android/maps/MapsActivity;

    invoke-direct {v0, v1, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 46
    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/car/firstrun/GmmProjectedFirstRunActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method
