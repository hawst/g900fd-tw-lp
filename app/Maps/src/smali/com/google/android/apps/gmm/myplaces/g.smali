.class Lcom/google/android/apps/gmm/myplaces/g;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field final a:Lcom/google/e/a/a/a/b;

.field final b:Lcom/google/android/apps/gmm/myplaces/h;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/h;)V
    .locals 2

    .prologue
    .line 73
    sget-object v0, Lcom/google/r/b/a/el;->bP:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/z;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 74
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/e/a/a/a/b;

    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/g;->a:Lcom/google/e/a/a/a/b;

    .line 75
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/myplaces/h;

    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/g;->b:Lcom/google/android/apps/gmm/myplaces/h;

    .line 76
    return-void
.end method

.method static a(ZLcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/b;Lcom/google/android/apps/gmm/myplaces/h;)Lcom/google/android/apps/gmm/myplaces/g;
    .locals 9
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/map/internal/d/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v1, 0x1

    .line 98
    if-nez p6, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 100
    :cond_0
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/maps/d/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 101
    if-eqz p0, :cond_6

    const/4 v0, 0x0

    .line 103
    :goto_0
    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 105
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/maps/g/b/l;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 106
    if-eqz p1, :cond_1

    .line 107
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v1, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 109
    :cond_1
    if-eqz p2, :cond_2

    .line 110
    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v6, p2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 112
    :cond_2
    if-eqz p3, :cond_3

    .line 113
    new-instance v3, Lcom/google/e/a/a/a/b;

    sget-object v4, Lcom/google/maps/g/b/m;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v3, v4}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 114
    iget-wide v4, p3, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v7, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 115
    iget-wide v4, p3, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v3, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v8, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 116
    iget-object v4, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v8, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 118
    :cond_3
    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v6, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 120
    if-eqz p4, :cond_4

    .line 121
    iget-object v0, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v7, p4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 125
    :cond_4
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/z;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 126
    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 129
    if-eqz p5, :cond_5

    .line 132
    invoke-virtual {p5}, Lcom/google/android/apps/gmm/map/internal/d/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 130
    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v6, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 135
    :cond_5
    new-instance v1, Lcom/google/android/apps/gmm/myplaces/g;

    invoke-direct {v1, v0, p6}, Lcom/google/android/apps/gmm/myplaces/g;-><init>(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/myplaces/h;)V

    return-object v1

    :cond_6
    move v0, v1

    .line 101
    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/16 v5, 0x1a

    const/4 v4, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 151
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v2}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_5

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_6

    :cond_0
    move v0, v2

    :goto_1
    if-eqz v0, :cond_b

    .line 153
    invoke-virtual {p1, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 154
    invoke-static {v0, v2, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/gmm/myplaces/g;->c:I

    .line 157
    invoke-static {v0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/g;->d:Ljava/lang/String;

    .line 158
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_7

    move v0, v2

    :goto_2
    if-nez v0, :cond_1

    invoke-virtual {p1, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    :cond_1
    move v0, v2

    :goto_3
    if-eqz v0, :cond_2

    .line 159
    invoke-virtual {p1, v4, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/g;->e:Lcom/google/e/a/a/a/b;

    .line 161
    :cond_2
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_9

    move v0, v2

    :goto_4
    if-nez v0, :cond_3

    invoke-virtual {p1, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    :cond_3
    move v0, v2

    :goto_5
    if-eqz v0, :cond_4

    .line 163
    invoke-virtual {p1, v6, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/myplaces/g;->f:Lcom/google/e/a/a/a/b;

    .line 169
    :cond_4
    :goto_6
    iget v0, p0, Lcom/google/android/apps/gmm/myplaces/g;->c:I

    packed-switch v0, :pswitch_data_0

    .line 177
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->k:Lcom/google/android/apps/gmm/shared/net/k;

    :goto_7
    return-object v0

    :cond_5
    move v0, v1

    .line 151
    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    move v0, v1

    .line 158
    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    move v0, v1

    .line 161
    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_5

    .line 166
    :cond_b
    iput v4, p0, Lcom/google/android/apps/gmm/myplaces/g;->c:I

    goto :goto_6

    .line 171
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_7

    .line 173
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_7

    .line 175
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->l:Lcom/google/android/apps/gmm/shared/net/k;

    goto :goto_7

    .line 169
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 185
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/k;->i:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq p1, v0, :cond_0

    .line 186
    const/4 v0, 0x0

    .line 188
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final al_()Z
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x1

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/g;->a:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 5
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 194
    if-nez p1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/myplaces/g;->c:I

    if-ne v1, v0, :cond_0

    .line 195
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/g;->b:Lcom/google/android/apps/gmm/myplaces/h;

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/g;->d:Ljava/lang/String;

    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/g;->e:Lcom/google/e/a/a/a/b;

    iget-object v4, p0, Lcom/google/android/apps/gmm/myplaces/g;->f:Lcom/google/e/a/a/a/b;

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/google/android/apps/gmm/myplaces/h;->a(ZLjava/lang/String;Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;)V

    .line 199
    return-void

    .line 194
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 195
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method
