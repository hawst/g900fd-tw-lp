.class Lcom/google/android/apps/gmm/navigation/a/x;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/maps/g/a/al;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/maps/g/a/al;)V
    .locals 0

    .prologue
    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/x;->a:Ljava/lang/String;

    .line 188
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/x;->b:Lcom/google/maps/g/a/al;

    .line 189
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 202
    if-ne p0, p1, :cond_1

    .line 218
    :cond_0
    :goto_0
    return v0

    .line 205
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 206
    goto :goto_0

    .line 208
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 209
    goto :goto_0

    .line 211
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/navigation/a/x;

    .line 212
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/x;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/a/x;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 213
    goto :goto_0

    .line 215
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/x;->b:Lcom/google/maps/g/a/al;

    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/a/x;->b:Lcom/google/maps/g/a/al;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 216
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/x;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 196
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/x;->b:Lcom/google/maps/g/a/al;

    iget v1, v1, Lcom/google/maps/g/a/al;->e:I

    add-int/2addr v0, v1

    .line 197
    return v0
.end method
