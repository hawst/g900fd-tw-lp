.class public Lcom/google/android/apps/gmm/aa/d/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/aa/d/q;


# static fields
.field static final a:Ljava/lang/String;

.field static final b:I


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final e:Lcom/google/android/apps/gmm/aa/d/o;

.field private final f:Lcom/google/android/apps/gmm/aa/d/i;

.field private final g:Lcom/google/android/apps/gmm/map/util/b/g;

.field private h:Lcom/google/android/apps/gmm/shared/c/a/a;

.field private final i:Ljava/util/concurrent/PriorityBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lcom/google/android/apps/gmm/aa/d/k;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/gmm/aa/d/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    .line 56
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/apps/gmm/aa/d/h;->b:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/aa/d/o;Lcom/google/android/apps/gmm/shared/net/a/b;Ljava/util/concurrent/PriorityBlockingQueue;Lcom/google/android/apps/gmm/aa/d/i;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/aa/d/o;",
            "Lcom/google/android/apps/gmm/shared/net/a/b;",
            "Ljava/util/concurrent/PriorityBlockingQueue",
            "<",
            "Lcom/google/android/apps/gmm/aa/d/k;",
            ">;",
            "Lcom/google/android/apps/gmm/aa/d/i;",
            "Lcom/google/android/apps/gmm/map/util/b/g;",
            ")V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/h;->c:Landroid/content/Context;

    .line 115
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/d/h;->e:Lcom/google/android/apps/gmm/aa/d/o;

    .line 116
    iput-object p3, p0, Lcom/google/android/apps/gmm/aa/d/h;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 117
    iput-object p4, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    .line 118
    iput-object p5, p0, Lcom/google/android/apps/gmm/aa/d/h;->f:Lcom/google/android/apps/gmm/aa/d/i;

    .line 119
    iput-object p6, p0, Lcom/google/android/apps/gmm/aa/d/h;->g:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 121
    invoke-interface {p6, p5}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 122
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/aa/d/o;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/logging/m;)Lcom/google/android/apps/gmm/aa/d/h;
    .locals 10

    .prologue
    .line 92
    new-instance v2, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v2}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    .line 93
    new-instance v0, Lcom/google/android/apps/gmm/aa/d/i;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p6

    move-object v6, p5

    move-object v7, p1

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/aa/d/i;-><init>(Landroid/content/Context;Ljava/util/concurrent/PriorityBlockingQueue;Lcom/google/android/apps/gmm/aa/d/r;Lcom/google/android/apps/gmm/aa/d/t;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/aa/d/o;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/navigation/logging/m;)V

    .line 95
    new-instance v3, Lcom/google/android/apps/gmm/aa/d/h;

    move-object v4, p0

    move-object v5, p5

    move-object/from16 v6, p6

    move-object v7, v2

    move-object v8, v0

    move-object/from16 v9, p7

    invoke-direct/range {v3 .. v9}, Lcom/google/android/apps/gmm/aa/d/h;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/aa/d/o;Lcom/google/android/apps/gmm/shared/net/a/b;Ljava/util/concurrent/PriorityBlockingQueue;Lcom/google/android/apps/gmm/aa/d/i;Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 97
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_TTS_SYNTHESIS:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-static {v0, p2, p1}, Lcom/google/android/apps/gmm/shared/c/a/a;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/shared/c/a/a;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/aa/d/h;->h:Lcom/google/android/apps/gmm/shared/c/a/a;

    iget-object v0, v3, Lcom/google/android/apps/gmm/aa/d/h;->f:Lcom/google/android/apps/gmm/aa/d/i;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->NETWORK_TTS_SYNTHESIS:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 98
    return-object v3
.end method

.method static a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/a/b;Ljava/lang/String;)Lcom/google/android/apps/gmm/aa/d/l;
    .locals 3

    .prologue
    .line 384
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/shared/net/a/b;->y()Lcom/google/r/b/a/amh;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/amh;->d:I

    .line 385
    new-instance v1, Lcom/google/android/apps/gmm/aa/d/m;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/aa/d/m;-><init>()V

    .line 386
    iput-object p2, v1, Lcom/google/android/apps/gmm/aa/d/m;->a:Ljava/lang/String;

    .line 387
    invoke-static {p0}, Lcom/google/android/apps/gmm/aa/b/a;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/aa/d/m;->b:Ljava/util/Locale;

    .line 388
    iput v0, v1, Lcom/google/android/apps/gmm/aa/d/m;->c:I

    sget-object v0, Lcom/google/android/apps/gmm/aa/d/n;->a:Lcom/google/android/apps/gmm/aa/d/n;

    .line 389
    iput-object v0, v1, Lcom/google/android/apps/gmm/aa/d/m;->d:Lcom/google/android/apps/gmm/aa/d/n;

    .line 390
    iget-object v0, v1, Lcom/google/android/apps/gmm/aa/d/m;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Text must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/aa/d/m;->b:Ljava/util/Locale;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Locale must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/aa/d/m;->d:Lcom/google/android/apps/gmm/aa/d/n;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "SynthesisMode must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/aa/d/l;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/aa/d/l;-><init>(Lcom/google/android/apps/gmm/aa/d/m;)V

    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/util/Locale;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/shared/net/a/b;)Ljava/lang/String;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 359
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    move-object v1, v0

    .line 360
    :goto_0
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/shared/net/a/b;->y()Lcom/google/r/b/a/amh;

    move-result-object v3

    iget-object v0, v3, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    .line 361
    :cond_0
    :goto_1
    if-nez v2, :cond_3

    .line 362
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    .line 363
    const/4 v0, 0x0

    .line 379
    :goto_2
    return-object v0

    .line 359
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    move-object v1, v0

    goto :goto_0

    .line 360
    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v2, v3, Lcom/google/r/b/a/amh;->f:Ljava/lang/Object;

    goto :goto_1

    .line 366
    :cond_3
    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/bo;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 368
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 373
    :goto_3
    const-string v3, "$LOCALE"

    const-string v4, "UTF-8"

    invoke-static {v0, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 374
    const-string v2, "$TEXT"

    const-string v3, "UTF-8"

    invoke-static {p0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 375
    const-string v2, "$LAT_E7"

    .line 376
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "UTF-8"

    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 375
    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 377
    const-string v2, "$LONG_E7"

    .line 378
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->d()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    const-string v3, "UTF-8"

    invoke-static {v1, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 377
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 370
    :cond_4
    invoke-virtual {p1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "-"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/io/File;
    .locals 3

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->e:Lcom/google/android/apps/gmm/aa/d/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/h;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/h;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-static {v1, v2, p1}, Lcom/google/android/apps/gmm/aa/d/h;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/a/b;Ljava/lang/String;)Lcom/google/android/apps/gmm/aa/d/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/aa/d/o;->a(Lcom/google/android/apps/gmm/aa/d/l;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 395
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/h;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "#flush() - queue size: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 396
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/PriorityBlockingQueue;->clear()V

    .line 397
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/aa/d/k;)V
    .locals 8

    .prologue
    .line 137
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/PriorityBlockingQueue;->drainTo(Ljava/util/Collection;)I

    const/4 v1, 0x0

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/d/k;

    iget-object v4, v0, Lcom/google/android/apps/gmm/aa/d/k;->a:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/apps/gmm/aa/d/k;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v1, v0

    :cond_1
    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/PriorityBlockingQueue;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 138
    return-void

    .line 137
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/aa/d/k;

    iget-object v3, v0, Lcom/google/android/apps/gmm/aa/d/k;->b:Lcom/google/android/apps/gmm/aa/d/s;

    iget-object v4, p1, Lcom/google/android/apps/gmm/aa/d/k;->b:Lcom/google/android/apps/gmm/aa/d/s;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/aa/d/s;->compareTo(Ljava/lang/Enum;)I

    move-result v3

    if-ltz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    iget-wide v4, v0, Lcom/google/android/apps/gmm/aa/d/k;->c:J

    iget-wide v6, v1, Lcom/google/android/apps/gmm/aa/d/k;->c:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/aa/d/h;->i:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v3, v0}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/d/h;->a()V

    .line 402
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->g:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/h;->f:Lcom/google/android/apps/gmm/aa/d/i;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/h;->h:Lcom/google/android/apps/gmm/shared/c/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/a;->quit()Z

    .line 404
    return-void
.end method
