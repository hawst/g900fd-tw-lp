.class public Lcom/google/android/apps/gmm/navigation/f/l;
.super Lcom/google/android/apps/gmm/navigation/commonui/a;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/a;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/l;->b:Z

    .line 25
    const-string v0, "gmmActivity"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/f/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 26
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 36
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/d;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/f/l;->b:Z

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Z)V

    .line 45
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/f/t;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49
    iget-object v3, p1, Lcom/google/android/apps/gmm/navigation/f/t;->a:Lcom/google/android/apps/gmm/navigation/f/b/a;

    .line 50
    if-eqz v3, :cond_0

    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_0

    .line 51
    iget-object v2, v3, Lcom/google/android/apps/gmm/navigation/f/b/a;->e:Lcom/google/android/apps/gmm/navigation/g/b/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/d;->d:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v2, v3, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/f/l;->b:Z

    .line 52
    return-void

    :cond_2
    move v2, v0

    .line 50
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 31
    return-void
.end method
