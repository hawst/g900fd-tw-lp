.class public final enum Lcom/google/android/apps/gmm/v/ab;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/v/ab;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/v/ab;

.field public static final enum b:Lcom/google/android/apps/gmm/v/ab;

.field public static final enum c:Lcom/google/android/apps/gmm/v/ab;

.field public static final enum d:Lcom/google/android/apps/gmm/v/ab;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/v/ab;


# instance fields
.field public final e:Z

.field public final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/v/ab;

    const-string v1, "NOT_LIVE"

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/apps/gmm/v/ab;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/v/ab;->a:Lcom/google/android/apps/gmm/v/ab;

    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/v/ab;

    const-string v1, "NOT_LIVE_WITH_NEW_CONTEXT"

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/apps/gmm/v/ab;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/v/ab;->b:Lcom/google/android/apps/gmm/v/ab;

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/v/ab;

    const-string v1, "LIVE"

    invoke-direct {v0, v1, v4, v3, v2}, Lcom/google/android/apps/gmm/v/ab;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/v/ab;->c:Lcom/google/android/apps/gmm/v/ab;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/v/ab;

    const-string v1, "LIVE_WITH_NEW_CONTEXT"

    invoke-direct {v0, v1, v5, v3, v3}, Lcom/google/android/apps/gmm/v/ab;-><init>(Ljava/lang/String;IZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/v/ab;->d:Lcom/google/android/apps/gmm/v/ab;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/ab;

    sget-object v1, Lcom/google/android/apps/gmm/v/ab;->a:Lcom/google/android/apps/gmm/v/ab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/v/ab;->b:Lcom/google/android/apps/gmm/v/ab;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/v/ab;->c:Lcom/google/android/apps/gmm/v/ab;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/v/ab;->d:Lcom/google/android/apps/gmm/v/ab;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/v/ab;->g:[Lcom/google/android/apps/gmm/v/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/v/ab;->e:Z

    .line 35
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/v/ab;->f:Z

    .line 36
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/v/ab;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/gmm/v/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/ab;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/v/ab;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/apps/gmm/v/ab;->g:[Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/v/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/v/ab;

    return-object v0
.end method
