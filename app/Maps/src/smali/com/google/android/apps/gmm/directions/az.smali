.class Lcom/google/android/apps/gmm/directions/az;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/a/e;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 293
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/az;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 294
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/apps/gmm/directions/f/a/c;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/az;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v2, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/az;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    .line 303
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->f()Landroid/graphics/Rect;

    move-result-object v2

    .line 304
    if-eqz p1, :cond_0

    .line 305
    iget v0, v2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/az;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 306
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/e;->ay:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    sub-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->top:I

    .line 309
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a/c;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    .line 311
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getMeasuredWidth()I

    move-result v4

    iget v5, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/Rect;->top:I

    .line 313
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->getMeasuredHeight()I

    move-result v1

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v1, v2

    invoke-direct {v0, v3, v4, v5, v1}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    .line 315
    :goto_1
    return-object v0

    .line 301
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/MapFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/g;->bF:I

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v0, :cond_3

    if-ne v0, v2, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    .line 315
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    goto :goto_1
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 321
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/az;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-eqz v3, :cond_3

    iget-object v2, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/MapFragment;->isVisible()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_0
    move v0, v1

    :cond_1
    :goto_2
    return v0

    :cond_2
    move v2, v1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/MapFragment;->getView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    if-nez v2, :cond_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method
