.class public Lcom/google/android/apps/gmm/map/internal/d/c/b/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/c/b/f;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Z

.field public d:[B

.field public e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/internal/d/c/b/d",
            "<*>;"
        }
    .end annotation
.end field

.field public f:J

.field private g:Z

.field private final h:Ljava/util/concurrent/CountDownLatch;

.field private i:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    .line 89
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->h:Ljava/util/concurrent/CountDownLatch;

    .line 90
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a:Ljava/util/ArrayList;

    .line 91
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->i:J

    .line 92
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 1

    .prologue
    .line 127
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    monitor-exit p0

    return-void

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(J)V
    .locals 1

    .prologue
    .line 411
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 412
    monitor-exit p0

    return-void

    .line 411
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 438
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->g:Z

    .line 439
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    .line 440
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/c;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/c;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 441
    monitor-exit p0

    return-void

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    monitor-exit p0

    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 112
    monitor-enter p0

    :try_start_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/e/a/a/a/b;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 453
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->g:Z

    .line 454
    const/4 v0, 0x3

    const/16 v1, 0x15

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v1, v0

    .line 455
    const/4 v0, 0x7

    const/16 v4, 0x1c

    invoke-virtual {p1, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 456
    const/16 v4, 0xc8

    if-ne v1, v4, :cond_7

    if-eqz v0, :cond_7

    .line 457
    const/4 v1, 0x4

    const/16 v4, 0x13

    invoke-virtual {p1, v1, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->f:J

    .line 458
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 459
    const-string v1, "image/svg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 460
    const/4 v0, 0x6

    const/16 v1, 0x19

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    .line 461
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    .line 462
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/h;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;[B)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    .line 478
    :goto_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v2, :cond_6

    move v0, v2

    .line 484
    :goto_1
    monitor-exit p0

    return v0

    .line 463
    :cond_0
    :try_start_1
    const-string v1, "image/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 464
    const/4 v0, 0x6

    const/16 v1, 0x19

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    .line 465
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    .line 466
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/b;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;[B)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 453
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 467
    :cond_1
    :try_start_2
    const-string v1, "application/binary"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "application/octet-stream"

    .line 468
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 469
    :cond_2
    const/4 v0, 0x6

    const/16 v1, 0x19

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    .line 470
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    goto :goto_0

    .line 471
    :cond_3
    const-string v1, "text/html"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 472
    const/4 v0, 0x6

    const/16 v1, 0x19

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    .line 473
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    goto :goto_0

    .line 475
    :cond_4
    const-string v1, "Resource"

    const-string v4, "Unhandled content-type: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    goto :goto_0

    .line 475
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move v0, v3

    .line 478
    goto :goto_1

    .line 479
    :cond_7
    const/16 v0, 0x130

    if-eq v1, v0, :cond_8

    .line 482
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    move v0, v3

    .line 484
    goto/16 :goto_1
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 136
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized d()V
    .locals 1

    .prologue
    .line 304
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    .line 305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d:[B

    .line 306
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    monitor-exit p0

    return-void

    .line 304
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 359
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->a(Landroid/content/res/Resources;F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V

    goto :goto_0
.end method

.method public final declared-synchronized f()J
    .locals 2

    .prologue
    .line 418
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->i:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()V
    .locals 2

    .prologue
    .line 508
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 509
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/f;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    .line 508
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 511
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 512
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->h:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    monitor-exit p0

    return-void

    .line 508
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
