.class Lcom/google/android/apps/gmm/place/bl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/hotels/a/g;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    .line 107
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    sget v2, Lcom/google/android/apps/gmm/l;->iU:I

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/h;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Lcom/google/e/a/a/a/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 93
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/e/a/a/a/b;)V

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->a()V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bl;->a:Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/hotels/a/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/hotels/a/h;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method
