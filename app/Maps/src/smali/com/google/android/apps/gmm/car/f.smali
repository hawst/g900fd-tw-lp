.class public Lcom/google/android/apps/gmm/car/f;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Ljava/lang/Object;

.field final c:Lcom/google/android/apps/gmm/base/a;

.field final d:Lcom/google/android/apps/gmm/car/e;

.field final e:Lcom/google/android/apps/gmm/map/util/b/g;

.field f:Lcom/google/android/gms/common/api/o;

.field g:Lcom/google/android/gms/car/ag;

.field h:Lcom/google/android/gms/car/aq;

.field i:Lcom/google/android/apps/gmm/car/p;

.field j:Lcom/google/android/apps/gmm/car/aa;

.field k:Lcom/google/android/apps/gmm/car/az;

.field l:Lcom/google/android/apps/gmm/car/i/ab;

.field m:Lcom/google/android/apps/gmm/car/bc;

.field n:Z

.field o:Z

.field p:Z

.field final q:Ljava/lang/Object;

.field final r:Lcom/google/android/apps/gmm/car/bb;

.field final s:Lcom/google/android/apps/gmm/car/a/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/car/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/e;)V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f;->b:Ljava/lang/Object;

    .line 130
    new-instance v0, Lcom/google/android/apps/gmm/car/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/g;-><init>(Lcom/google/android/apps/gmm/car/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f;->q:Ljava/lang/Object;

    .line 282
    new-instance v0, Lcom/google/android/apps/gmm/car/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/h;-><init>(Lcom/google/android/apps/gmm/car/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f;->r:Lcom/google/android/apps/gmm/car/bb;

    .line 301
    new-instance v0, Lcom/google/android/apps/gmm/car/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i;-><init>(Lcom/google/android/apps/gmm/car/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f;->s:Lcom/google/android/apps/gmm/car/a/h;

    .line 169
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f;->c:Lcom/google/android/apps/gmm/base/a;

    .line 170
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/e;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/f;->d:Lcom/google/android/apps/gmm/car/e;

    .line 171
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/f;->e:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 172
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/f;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 35
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/f;->b:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f;->g:Lcom/google/android/gms/car/ag;

    if-nez v3, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    monitor-exit v2

    :goto_0
    return-void

    :cond_0
    sget-object v3, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x24

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onNavigationFocusRequestEvent("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/f;->g:Lcom/google/android/gms/car/ag;

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v3, v4, v5, v0}, Lcom/google/android/gms/car/ag;->a(III)V
    :try_end_1
    .catch Lcom/google/android/gms/car/ao; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_2
    :try_start_2
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v1, Lcom/google/android/apps/gmm/car/f;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2f

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "reportNavigationStateToCar: car not connected: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method
