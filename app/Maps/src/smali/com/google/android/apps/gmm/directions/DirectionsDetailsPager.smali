.class public Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;
.super Lcom/google/android/apps/gmm/base/views/GmmViewPager;
.source "PG"


# instance fields
.field h:Z

.field private i:Lcom/google/android/apps/gmm/map/r/a/ag;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;-><init>(Landroid/content/Context;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->h:Z

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->j:I

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->h:Z

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->j:I

    .line 52
    return-void
.end method


# virtual methods
.method public final K_()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/u;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->b()I

    move-result v3

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/b/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    if-gez v3, :cond_1

    :cond_0
    move-object v0, v2

    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/a/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/a/a/a;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    invoke-static {p0}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    invoke-virtual {p0, v2}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 57
    return-void

    .line 56
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/directions/u;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/directions/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/b/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0, v3}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/v;->a(Lcom/google/android/apps/gmm/map/r/a/w;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected final e()V
    .locals 6

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_0

    iget v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->j:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->a()Landroid/support/v4/view/ag;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/b/a;->c:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-nez v0, :cond_1

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    sget v1, Lcom/google/android/apps/gmm/g;->ab:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    :goto_1
    if-ltz v2, :cond_3

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v1

    instance-of v5, v1, Lcom/google/android/apps/gmm/directions/bi;

    if-eqz v5, :cond_2

    check-cast v1, Lcom/google/android/apps/gmm/directions/bi;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/directions/bi;->a()Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    if-eqz v1, :cond_2

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    iget v5, v4, Lcom/google/android/apps/gmm/map/r/a/ag;->h:I

    if-ne v1, v5, :cond_2

    move v1, v2

    :goto_2
    if-ltz v1, :cond_4

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->smoothScrollToPositionFromTop(II)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    iput v3, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->j:I

    iput-object v4, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    :cond_3
    const/4 v1, -0x1

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    goto :goto_0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->h:Z

    if-eqz v0, :cond_0

    .line 88
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 91
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPager;->h:Z

    if-eqz v0, :cond_0

    .line 97
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/views/GmmViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 101
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
