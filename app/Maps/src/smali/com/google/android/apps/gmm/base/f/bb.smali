.class public Lcom/google/android/apps/gmm/base/f/bb;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/i/h;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 446
    return-void
.end method

.method static synthetic a(Lcom/google/android/libraries/curvular/aw;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 18

    .prologue
    .line 91
    const/4 v2, 0x4

    new-array v3, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v4, 0x0

    const-wide/high16 v6, 0x4052000000000000L    # 72.0

    const-wide/high16 v8, 0x4052000000000000L    # 72.0

    const-wide/high16 v10, 0x4061000000000000L    # 136.0

    const-wide/high16 v12, 0x4054000000000000L    # 80.0

    new-instance v5, Lcom/google/android/libraries/curvular/as;

    new-instance v14, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_0

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v14, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_1

    double-to-int v7, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v6, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_2

    double-to-int v8, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_3

    double-to-int v9, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x1

    iput v9, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v5, v14, v6, v7, v8}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x1

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x2

    const-wide/high16 v6, 0x4030000000000000L    # 16.0

    const-wide/high16 v8, 0x4030000000000000L    # 16.0

    const-wide/high16 v10, 0x4054000000000000L    # 80.0

    const-wide/high16 v12, 0x4038000000000000L    # 24.0

    new-instance v5, Lcom/google/android/libraries/curvular/as;

    new-instance v14, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_4

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_4
    invoke-direct {v14, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_5

    double-to-int v7, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v2, Landroid/util/TypedValue;->data:I

    :goto_5
    invoke-direct {v6, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_6

    double-to-int v8, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_6
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_7

    double-to-int v9, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x1

    iput v9, v2, Landroid/util/TypedValue;->data:I

    :goto_7
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v5, v14, v6, v7, v8}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bD:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, p0

    invoke-static {v6, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v4, v5

    new-instance v5, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v5, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.ImageView"

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v4

    aput-object v4, v3, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    :cond_0
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_1
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_2
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_3
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v9

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x11

    iput v9, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    :cond_4
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4

    :cond_5
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_5

    :cond_6
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_6

    :cond_7
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v9

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x11

    iput v9, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_7
.end method

.method static synthetic a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/libraries/curvular/cs;Lcom/google/android/libraries/curvular/cs;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 18

    .prologue
    .line 91
    const/16 v2, 0xa

    new-array v3, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x1

    const-wide/high16 v6, 0x4048000000000000L    # 48.0

    new-instance v5, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_1

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x2

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v5, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->aH:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v4, 0x2

    const-wide/high16 v6, 0x4030000000000000L    # 16.0

    const-wide/high16 v8, 0x4030000000000000L    # 16.0

    const-wide/high16 v10, 0x4054000000000000L    # 80.0

    const-wide/high16 v12, 0x4038000000000000L    # 24.0

    new-instance v5, Lcom/google/android/libraries/curvular/as;

    new-instance v14, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_2

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v14, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_3

    double-to-int v7, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v6, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v10, v11}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_4

    double-to-int v8, v10

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_5

    double-to-int v9, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x1

    iput v9, v2, Landroid/util/TypedValue;->data:I

    :goto_4
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    invoke-direct {v5, v14, v6, v7, v8}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x3

    const/16 v4, 0x10

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x4

    sget-object v4, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v5, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    new-instance v5, Lcom/google/android/libraries/curvular/a;

    invoke-direct {v5}, Lcom/google/android/libraries/curvular/a;-><init>()V

    sget-object v6, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x5

    sget-object v4, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    move-object/from16 v0, p2

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v4, 0x6

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v5

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v5}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v5, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v3, v4

    const/4 v2, 0x7

    sget-object v4, Lcom/google/android/libraries/curvular/g;->s:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, p0

    invoke-static {v4, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    aput-object v4, v3, v2

    const/16 v2, 0x8

    aput-object p3, v3, v2

    const/16 v2, 0x9

    aput-object p4, v3, v2

    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    :cond_1
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v8

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x12

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_2
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_3
    const-wide/high16 v16, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v16

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_4
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v10

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    :cond_5
    const-wide/high16 v10, 0x4060000000000000L    # 128.0

    mul-double/2addr v10, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v10, v11, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v9

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v10, 0xffffff

    and-int/2addr v9, v10

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x11

    iput v9, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4
.end method

.method static synthetic a(Ljava/lang/CharSequence;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 91
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/libraries/curvular/cu;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/google/android/libraries/curvular/g;->aD:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v5

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    sget-object v2, Lcom/google/android/libraries/curvular/g;->O:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    aput-object v1, v0, v4

    new-array v1, v6, [Lcom/google/android/libraries/curvular/cu;

    sget v2, Lcom/google/android/apps/gmm/m;->l:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->d(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v5

    sget v2, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bT:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, p0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v0, v1

    new-instance v1, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v1, v0}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v0, "android.widget.TextView"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    invoke-virtual {v0, p1, v4}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 20

    .prologue
    .line 95
    const/16 v2, 0xe

    new-array v11, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v3, 0x0

    .line 96
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->J()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v4, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v11, v3

    const/4 v2, 0x1

    const/4 v3, 0x1

    .line 97
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v11, v2

    const/4 v2, 0x2

    const/4 v3, -0x1

    .line 98
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    aput-object v3, v11, v2

    const/4 v10, 0x3

    const/4 v2, 0x3

    const/4 v3, 0x1

    .line 103
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 104
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/h;->c()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/f;->eE:I

    .line 105
    sget v6, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v5

    .line 106
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v6}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/libraries/curvular/ce;

    check-cast v6, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/place/i/h;->a()Ljava/lang/CharSequence;

    move-result-object v6

    .line 107
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/place/i/h;->b()Ljava/lang/CharSequence;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    const/4 v13, 0x1

    .line 109
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bN:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v9, v12

    .line 101
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/bc;->a(ILjava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v3

    const/4 v2, 0x1

    new-array v4, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const/16 v6, 0x8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-static {v2, v6, v7}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    invoke-virtual {v3, v4, v2}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;Z)V

    aput-object v3, v11, v10

    const/4 v12, 0x4

    const/4 v2, 0x1

    .line 114
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 115
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/i/h;->F()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->cM:I

    .line 116
    sget v5, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    .line 117
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v5}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/curvular/ce;

    check-cast v5, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/i/h;->E()Ljava/lang/CharSequence;

    move-result-object v5

    .line 118
    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    sget v8, Lcom/google/android/apps/gmm/m;->l:I

    invoke-static {v8}, Lcom/google/android/libraries/curvular/c;->d(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget v8, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v8}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->bT:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v6

    .line 119
    sget v7, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x1

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    const/4 v13, 0x0

    .line 121
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v10}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/libraries/curvular/ce;

    check-cast v10, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v10}, Lcom/google/android/apps/gmm/place/i/h;->H()Ljava/lang/Boolean;

    move-result-object v10

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    const/16 v15, 0x8

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    invoke-static {v10, v14, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v13

    .line 113
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/by;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v3, 0x5

    const/16 v2, 0x9

    new-array v4, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    .line 125
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->K()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    sget-object v6, Lcom/google/android/apps/gmm/base/k/j;->ah:Lcom/google/android/apps/gmm/base/k/j;

    invoke-static {v6, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    const/4 v5, 0x1

    .line 126
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x2

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->G()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-static {v2, v6, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x3

    .line 128
    const-wide/high16 v6, 0x4052000000000000L    # 72.0

    const-wide/high16 v8, 0x4052000000000000L    # 72.0

    const-wide/high16 v12, 0x4061000000000000L    # 136.0

    const-wide/high16 v14, 0x4054000000000000L    # 80.0

    new-instance v10, Lcom/google/android/libraries/curvular/as;

    new-instance v16, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_2

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_0
    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_3

    double-to-int v7, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v2, Landroid/util/TypedValue;->data:I

    :goto_1
    invoke-direct {v6, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_4

    double-to-int v8, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_2
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_5

    double-to-int v9, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v9, v12

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x1

    iput v9, v2, Landroid/util/TypedValue;->data:I

    :goto_3
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    move-object/from16 v0, v16

    invoke-direct {v10, v0, v6, v7, v8}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ar:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x4

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 129
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_6

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_4
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x5

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 130
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_7

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_5
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x6

    const/4 v5, 0x1

    .line 131
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->S:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x7

    const/4 v5, 0x3

    new-array v5, v5, [Lcom/google/android/libraries/curvular/cu;

    const/4 v6, 0x0

    sget v7, Lcom/google/android/apps/gmm/l;->kx:I

    .line 135
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    .line 136
    const/4 v7, 0x2

    new-array v7, v7, [Lcom/google/android/libraries/curvular/cu;

    const/4 v8, 0x0

    sget v9, Lcom/google/android/apps/gmm/m;->m:I

    invoke-static {v9}, Lcom/google/android/libraries/curvular/c;->d(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    sget v9, Lcom/google/android/apps/gmm/k;->aa:I

    invoke-static {v9}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v9

    sget-object v10, Lcom/google/android/libraries/curvular/g;->bT:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v9}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v7}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    .line 137
    sget v7, Lcom/google/android/apps/gmm/d;->aw:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v5, v6

    .line 134
    new-instance v6, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v6, v5}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v5, "android.widget.TextView"

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-virtual {v6, v5}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    aput-object v5, v4, v2

    const/16 v5, 0x8

    const/4 v2, 0x4

    new-array v6, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    const-wide/high16 v8, 0x4010000000000000L    # 4.0

    .line 141
    new-instance v10, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_8

    double-to-int v8, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_6
    invoke-direct {v10, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v7, 0x1

    .line 142
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->I()Ljava/lang/CharSequence;

    move-result-object v2

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v7, 0x2

    .line 143
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->F()Lcom/google/android/libraries/curvular/cf;

    move-result-object v8

    const/4 v2, 0x0

    if-eqz v8, :cond_0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v8, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v8

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v8}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_0
    sget-object v8, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x3

    .line 144
    sget v7, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    aput-object v7, v6, v2

    .line 140
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v6}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v6, "android.widget.TextView"

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v4, v5

    .line 124
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.LinearLayout"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v3

    const/4 v12, 0x6

    const/4 v2, 0x1

    const/4 v3, 0x1

    .line 150
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 151
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/h;->m()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/f;->cL:I

    .line 152
    sget v6, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v5

    .line 153
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v6}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/libraries/curvular/ce;

    check-cast v6, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/place/i/h;->j()Ljava/lang/CharSequence;

    move-result-object v6

    .line 154
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/place/i/h;->k()Ljava/lang/CharSequence;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x2

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    const/4 v13, 0x0

    const-wide/high16 v14, 0x4010000000000000L    # 4.0

    .line 156
    new-instance v16, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v10

    if-eqz v10, :cond_9

    double-to-int v14, v14

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x1

    iput v14, v10, Landroid/util/TypedValue;->data:I

    :goto_7
    move-object/from16 v0, v16

    invoke-direct {v0, v10}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v10, Lcom/google/android/libraries/curvular/g;->I:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v10, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v13

    const/4 v10, 0x1

    sget v13, Lcom/google/android/apps/gmm/f;->de:I

    .line 157
    sget v14, Lcom/google/android/apps/gmm/d;->as:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->J:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v9, v10

    .line 148
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/bc;->a(ILjava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v3

    .line 158
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->n()Ljava/lang/Boolean;

    move-result-object v4

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->l()Ljava/lang/Boolean;

    move-result-object v2

    .line 158
    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->b(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    .line 159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->H()Ljava/lang/Boolean;

    move-result-object v2

    .line 158
    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    .line 160
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->G()Ljava/lang/Boolean;

    move-result-object v2

    .line 158
    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->b(Lcom/google/android/libraries/curvular/ah;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const/16 v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/4 v3, 0x7

    const/16 v2, 0xa

    new-array v4, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v5, 0x0

    .line 164
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->n()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    sget-object v7, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v7, v6}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    const/16 v7, 0x8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v8, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-static {v2, v6, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x1

    const/4 v5, 0x1

    .line 165
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x2

    .line 166
    const-wide/high16 v6, 0x4030000000000000L    # 16.0

    const-wide/high16 v8, 0x4030000000000000L    # 16.0

    const-wide/high16 v12, 0x4054000000000000L    # 80.0

    const-wide/high16 v14, 0x4038000000000000L    # 24.0

    new-instance v10, Lcom/google/android/libraries/curvular/as;

    new-instance v16, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_a

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_8
    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v6, Lcom/google/android/libraries/curvular/b;

    invoke-static {v8, v9}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_b

    double-to-int v7, v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x1

    iput v7, v2, Landroid/util/TypedValue;->data:I

    :goto_9
    invoke-direct {v6, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v7, Lcom/google/android/libraries/curvular/b;

    invoke-static {v12, v13}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_c

    double-to-int v8, v12

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x1

    iput v8, v2, Landroid/util/TypedValue;->data:I

    :goto_a
    invoke-direct {v7, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v14, v15}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_d

    double-to-int v9, v14

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v9, v12

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x1

    iput v9, v2, Landroid/util/TypedValue;->data:I

    :goto_b
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    move-object/from16 v0, v16

    invoke-direct {v10, v0, v6, v7, v8}, Lcom/google/android/libraries/curvular/as;-><init>(Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;Lcom/google/android/libraries/curvular/b;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bi:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x3

    const-wide/high16 v6, 0x4028000000000000L    # 12.0

    .line 167
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_e

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_c
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bm:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v5, 0x4

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    .line 170
    new-instance v8, Lcom/google/android/libraries/curvular/b;

    invoke-static {v6, v7}, Lcom/google/b/g/a;->a(D)Z

    move-result v2

    if-eqz v2, :cond_f

    double-to-int v6, v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x1

    iput v6, v2, Landroid/util/TypedValue;->data:I

    :goto_d
    invoke-direct {v8, v2}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v2, Lcom/google/android/libraries/curvular/g;->bh:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v6, 0x0

    const/4 v7, 0x2

    .line 171
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bF:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x6

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Integer;

    const/4 v6, 0x0

    const/4 v7, 0x2

    .line 172
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    sget-object v6, Lcom/google/android/libraries/curvular/g;->bB:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v2, 0x7

    .line 173
    sget-object v5, Lcom/google/android/apps/gmm/base/h/c;->b:Lcom/google/android/apps/gmm/base/h/c;

    sget-object v6, Lcom/google/android/libraries/curvular/g;->l:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/16 v2, 0x8

    .line 174
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/base/f/bb;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    aput-object v5, v4, v2

    const/16 v5, 0x9

    .line 175
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->m()Lcom/google/android/libraries/curvular/cf;

    move-result-object v6

    const/4 v2, 0x0

    if-eqz v6, :cond_1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-static {v6, v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v6

    new-instance v2, Lcom/google/android/libraries/curvular/u;

    invoke-direct {v2, v6}, Lcom/google/android/libraries/curvular/u;-><init>(Lcom/google/android/libraries/curvular/b/i;)V

    :cond_1
    sget-object v6, Lcom/google/android/libraries/curvular/g;->aR:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v4, v5

    .line 163
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v4}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v4, "android.widget.TableLayout"

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v3

    const/16 v8, 0x8

    sget v2, Lcom/google/android/apps/gmm/f;->el:I

    .line 179
    sget v3, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v5

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->d()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    sget v2, Lcom/google/android/apps/gmm/l;->ko:I

    .line 181
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v6

    .line 182
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->f()Ljava/lang/String;

    move-result-object v7

    const/4 v4, 0x0

    const/4 v2, 0x1

    new-array v9, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v10, 0x0

    .line 184
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->e()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    const/16 v13, 0x8

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    invoke-static {v2, v12, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v9, v10

    .line 178
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    const/4 v10, 0x1

    new-array v10, v10, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    const/16 v13, 0x10

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v10, v12

    invoke-static {v5, v10}, Lcom/google/android/apps/gmm/base/f/bb;->a(Lcom/google/android/libraries/curvular/aw;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v5

    const/4 v10, 0x3

    new-array v10, v10, [Lcom/google/android/libraries/curvular/cu;

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->be:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v10, v12

    const/4 v12, 0x1

    const/4 v13, 0x1

    new-array v13, v13, [Lcom/google/android/libraries/curvular/cu;

    const/4 v14, 0x0

    sget v15, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v15}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-static {v6, v13}, Lcom/google/android/apps/gmm/base/f/bb;->a(Ljava/lang/CharSequence;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    aput-object v6, v10, v12

    const/4 v12, 0x2

    const/4 v6, 0x2

    new-array v13, v6, [Lcom/google/android/libraries/curvular/cu;

    const/4 v6, 0x0

    sget v14, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v14}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    aput-object v14, v13, v6

    const/4 v14, 0x1

    const-wide/high16 v16, 0x4010000000000000L    # 4.0

    new-instance v15, Lcom/google/android/libraries/curvular/b;

    invoke-static/range {v16 .. v17}, Lcom/google/b/g/a;->a(D)Z

    move-result v6

    if-eqz v6, :cond_10

    move-wide/from16 v0, v16

    double-to-int v0, v0

    move/from16 v16, v0

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x1

    move/from16 v0, v16

    iput v0, v6, Landroid/util/TypedValue;->data:I

    :goto_e
    invoke-direct {v15, v6}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v6, Lcom/google/android/libraries/curvular/g;->as:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v6

    aput-object v6, v13, v14

    invoke-static {v7, v13}, Lcom/google/android/apps/gmm/base/f/bb;->a(Ljava/lang/CharSequence;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    aput-object v6, v10, v12

    new-instance v6, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v6, v10}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v7, "android.widget.LinearLayout"

    sget-object v10, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v10, v7}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/android/libraries/curvular/cu;

    const/4 v10, 0x0

    const/16 v12, 0x10

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    aput-object v12, v7, v10

    invoke-static/range {v2 .. v7}, Lcom/google/android/apps/gmm/base/f/bb;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/libraries/curvular/cs;Lcom/google/android/libraries/curvular/cs;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    invoke-virtual {v2, v9}, Lcom/google/android/libraries/curvular/cs;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v8

    const/16 v3, 0x9

    .line 188
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->g()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/a/y;->a()Ljava/lang/CharSequence;

    move-result-object v4

    .line 189
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->g()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/a/y;->c()Ljava/lang/CharSequence;

    move-result-object v5

    .line 190
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->g()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/a/y;->ah_()Lcom/google/android/libraries/curvular/cf;

    move-result-object v6

    .line 191
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->g()Lcom/google/android/apps/gmm/base/l/a/y;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/l/a/y;->f()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v7

    const/4 v2, 0x1

    new-array v8, v2, [Lcom/google/android/libraries/curvular/cu;

    const/4 v9, 0x0

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->h()Ljava/lang/Boolean;

    move-result-object v2

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    sget-object v12, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v12, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    const/16 v12, 0x8

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    sget-object v13, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v12

    invoke-static {v2, v10, v12}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v8, v9

    .line 187
    invoke-static {v4, v5, v6, v7, v8}, Lcom/google/android/apps/gmm/base/f/be;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v3

    const/16 v10, 0xa

    const/4 v2, 0x1

    .line 197
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/i/h;->r()Ljava/lang/Boolean;

    move-result-object v3

    .line 198
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/h;->u()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/f;->eC:I

    .line 199
    sget v6, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v6}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v5

    .line 200
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v6}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/libraries/curvular/ce;

    check-cast v6, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/place/i/h;->s()Ljava/lang/CharSequence;

    move-result-object v6

    .line 201
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/place/i/h;->t()Ljava/lang/CharSequence;

    move-result-object v7

    .line 202
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v8}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/libraries/curvular/ce;

    check-cast v8, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/place/i/h;->M()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v8

    const/4 v9, 0x0

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    .line 195
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/bc;->a(ILjava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v3

    .line 203
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->q()Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->p()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v10

    const/16 v12, 0xb

    const/4 v2, 0x1

    const/4 v3, 0x1

    .line 208
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 209
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v4}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/libraries/curvular/ce;

    check-cast v4, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/place/i/h;->B()Lcom/google/android/libraries/curvular/cf;

    move-result-object v4

    .line 210
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v5}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/libraries/curvular/ce;

    check-cast v5, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/place/i/h;->x()Lcom/google/android/libraries/curvular/aw;

    move-result-object v5

    .line 211
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v6}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/libraries/curvular/ce;

    check-cast v6, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/place/i/h;->v()Ljava/lang/CharSequence;

    move-result-object v6

    .line 212
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v7}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/libraries/curvular/ce;

    check-cast v7, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v7}, Lcom/google/android/apps/gmm/place/i/h;->A()Ljava/lang/CharSequence;

    move-result-object v7

    .line 213
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v8}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/libraries/curvular/ce;

    check-cast v8, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/place/i/h;->N()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    const/4 v10, 0x0

    const/4 v13, 0x1

    .line 214
    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bC:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v13}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v13

    aput-object v13, v9, v10

    const/4 v13, 0x1

    .line 215
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v10}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/libraries/curvular/ce;

    check-cast v10, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v10}, Lcom/google/android/apps/gmm/place/i/h;->w()Lcom/google/android/libraries/curvular/aq;

    move-result-object v10

    sget-object v14, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v14, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v13

    .line 206
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/bc;->a(ILjava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v3

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->y()Ljava/lang/Boolean;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v2}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/curvular/ce;

    check-cast v2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/i/h;->p()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    sget-object v5, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v5, v4}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v4

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v6, v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/google/android/libraries/curvular/br;->a(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/16 v12, 0xc

    const/4 v2, 0x1

    .line 220
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 221
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/i/h;->i()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->ep:I

    .line 222
    sget v5, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->kp:I

    .line 223
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 224
    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    sget v8, Lcom/google/android/apps/gmm/m;->l:I

    invoke-static {v8}, Lcom/google/android/libraries/curvular/c;->d(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget v8, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v8}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->bT:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v6

    .line 225
    sget v7, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    .line 226
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v8}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/libraries/curvular/ce;

    check-cast v8, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/place/i/h;->L()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    const/4 v13, 0x0

    .line 227
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v10}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/libraries/curvular/ce;

    check-cast v10, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v10}, Lcom/google/android/apps/gmm/place/i/h;->z()Ljava/lang/Boolean;

    move-result-object v10

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    const/16 v15, 0x8

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    invoke-static {v10, v14, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v13

    .line 219
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/by;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    const/16 v12, 0xd

    const/4 v2, 0x1

    .line 231
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 232
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v3}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/libraries/curvular/ce;

    check-cast v3, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/place/i/h;->D()Lcom/google/android/libraries/curvular/cf;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->da:I

    .line 233
    sget v5, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v5}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v4

    sget v5, Lcom/google/android/apps/gmm/l;->kz:I

    .line 234
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Integer;)Ljava/lang/CharSequence;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/android/libraries/curvular/cu;

    const/4 v7, 0x0

    .line 235
    const/4 v8, 0x2

    new-array v8, v8, [Lcom/google/android/libraries/curvular/cu;

    const/4 v9, 0x0

    sget v10, Lcom/google/android/apps/gmm/m;->l:I

    invoke-static {v10}, Lcom/google/android/libraries/curvular/c;->d(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v10

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bJ:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget v10, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v10}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v10

    sget-object v13, Lcom/google/android/libraries/curvular/g;->bT:Lcom/google/android/libraries/curvular/g;

    invoke-static {v13, v10}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v8}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    sget-object v9, Lcom/google/android/libraries/curvular/g;->bS:Lcom/google/android/libraries/curvular/g;

    invoke-static {v9, v8}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, Lcom/google/android/libraries/curvular/ar;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/ar;

    move-result-object v6

    .line 236
    sget v7, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    .line 237
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v8}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/android/libraries/curvular/ce;

    check-cast v8, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/place/i/h;->O()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v8

    const/4 v9, 0x1

    new-array v9, v9, [Lcom/google/android/libraries/curvular/cu;

    const/4 v13, 0x0

    .line 238
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v10}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/google/android/libraries/curvular/ce;

    check-cast v10, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {v10}, Lcom/google/android/apps/gmm/place/i/h;->C()Ljava/lang/Boolean;

    move-result-object v10

    const/4 v14, 0x0

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    sget-object v15, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    invoke-static {v15, v14}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v14

    const/16 v15, 0x8

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    sget-object v16, Lcom/google/android/libraries/curvular/g;->ce:Lcom/google/android/libraries/curvular/g;

    move-object/from16 v0, v16

    invoke-static {v0, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v15

    invoke-static {v10, v14, v15}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v10

    aput-object v10, v9, v13

    .line 230
    invoke-static/range {v2 .. v9}, Lcom/google/android/apps/gmm/base/f/by;->a(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/cf;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/libraries/curvular/cu;Lcom/google/android/libraries/curvular/aq;Lcom/google/android/apps/gmm/z/b/l;[Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    aput-object v2, v11, v12

    .line 95
    new-instance v2, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v2, v11}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v3, "android.widget.LinearLayout"

    sget-object v4, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v4, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v2

    return-object v2

    .line 128
    :cond_2
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_0

    :cond_3
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_1

    :cond_4
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_2

    :cond_5
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v9

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v9, v12

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x11

    iput v9, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_3

    .line 129
    :cond_6
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_4

    .line 130
    :cond_7
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_5

    .line 141
    :cond_8
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_6

    .line 156
    :cond_9
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v14, v14, v18

    sget-object v10, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v14, v15, v10}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v14

    new-instance v10, Landroid/util/TypedValue;

    invoke-direct {v10}, Landroid/util/TypedValue;-><init>()V

    const v15, 0xffffff

    and-int/2addr v14, v15

    shl-int/lit8 v14, v14, 0x8

    or-int/lit8 v14, v14, 0x11

    iput v14, v10, Landroid/util/TypedValue;->data:I

    goto/16 :goto_7

    .line 166
    :cond_a
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v6, v6, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_8

    :cond_b
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v8, v8, v18

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v7

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v8, 0xffffff

    and-int/2addr v7, v8

    shl-int/lit8 v7, v7, 0x8

    or-int/lit8 v7, v7, 0x11

    iput v7, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_9

    :cond_c
    const-wide/high16 v8, 0x4060000000000000L    # 128.0

    mul-double/2addr v8, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v8, v9, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v8

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v9, 0xffffff

    and-int/2addr v8, v9

    shl-int/lit8 v8, v8, 0x8

    or-int/lit8 v8, v8, 0x11

    iput v8, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_a

    :cond_d
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v12, v14

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v12, v13, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v9

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v12, 0xffffff

    and-int/2addr v9, v12

    shl-int/lit8 v9, v9, 0x8

    or-int/lit8 v9, v9, 0x11

    iput v9, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_b

    .line 167
    :cond_e
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_c

    .line 170
    :cond_f
    const-wide/high16 v12, 0x4060000000000000L    # 128.0

    mul-double/2addr v6, v12

    sget-object v2, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v6, v7, v2}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v6

    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    const v7, 0xffffff

    and-int/2addr v6, v7

    shl-int/lit8 v6, v6, 0x8

    or-int/lit8 v6, v6, 0x11

    iput v6, v2, Landroid/util/TypedValue;->data:I

    goto/16 :goto_d

    .line 178
    :cond_10
    const-wide/high16 v18, 0x4060000000000000L    # 128.0

    mul-double v16, v16, v18

    sget-object v6, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    move-wide/from16 v0, v16

    invoke-static {v0, v1, v6}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v16

    new-instance v6, Landroid/util/TypedValue;

    invoke-direct {v6}, Landroid/util/TypedValue;-><init>()V

    const v17, 0xffffff

    and-int v16, v16, v17

    shl-int/lit8 v16, v16, 0x8

    or-int/lit8 v16, v16, 0x11

    move/from16 v0, v16

    iput v0, v6, Landroid/util/TypedValue;->data:I

    goto/16 :goto_e
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 91
    check-cast p2, Lcom/google/android/apps/gmm/place/i/h;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/i/h;->n()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/apps/gmm/base/f/bd;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/place/i/h;->o()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p4, v0, v1}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Ljava/util/List;)V

    :cond_0
    return-void
.end method
