.class public Lcom/google/android/apps/gmm/map/internal/c/ba;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# instance fields
.field public final a:I

.field public final b:[I

.field public final c:[[B

.field public final d:[Lcom/google/android/apps/gmm/map/b/a/ax;

.field public final e:Lcom/google/android/apps/gmm/map/internal/c/be;

.field private final f:J

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:[I


# direct methods
.method private constructor <init>(JIII[I[[B[Lcom/google/android/apps/gmm/map/b/a/ax;Lcom/google/android/apps/gmm/map/internal/c/be;Ljava/lang/String;[I)V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->f:J

    .line 77
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->g:I

    .line 78
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->h:I

    .line 79
    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->a:I

    .line 80
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->b:[I

    .line 81
    iput-object p7, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    .line 82
    iput-object p8, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->d:[Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 83
    iput-object p9, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 84
    iput-object p10, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->i:Ljava/lang/String;

    .line 85
    iput-object p11, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->j:[I

    .line 86
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/cd;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/ba;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 138
    iget-object v0, p0, Lcom/google/maps/b/a/cd;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 139
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v0, :cond_0

    const/4 v10, 0x0

    .line 141
    :goto_0
    invoke-virtual {p0}, Lcom/google/maps/b/a/cd;->a()Lcom/google/maps/b/a/dm;

    move-result-object v1

    .line 142
    iget-object v0, v1, Lcom/google/maps/b/a/dm;->a:Lcom/google/maps/b/a/cz;

    iget v6, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 145
    iget-object v0, v1, Lcom/google/maps/b/a/dm;->b:Lcom/google/maps/b/a/cs;

    iget v2, v0, Lcom/google/maps/b/a/cs;->b:I

    .line 146
    new-array v8, v2, [[B

    .line 147
    new-array v7, v2, [I

    move v0, v12

    .line 148
    :goto_1
    if-ge v0, v2, :cond_2

    .line 149
    invoke-virtual {v1, v0}, Lcom/google/maps/b/a/dm;->b(I)Lcom/google/maps/b/a/cu;

    move-result-object v3

    iget v4, v3, Lcom/google/maps/b/a/cu;->c:I

    new-array v4, v4, [B

    iget-object v5, v3, Lcom/google/maps/b/a/cu;->a:[B

    iget v9, v3, Lcom/google/maps/b/a/cu;->b:I

    iget v3, v3, Lcom/google/maps/b/a/cu;->c:I

    invoke-static {v5, v9, v4, v12, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-object v4, v8, v0

    .line 150
    aput v12, v7, v0

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 139
    :cond_0
    iget-object v0, v2, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v10

    goto :goto_0

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v10, v0, v12

    goto :goto_0

    .line 154
    :cond_2
    invoke-virtual {v1}, Lcom/google/maps/b/a/dm;->a()Lcom/google/maps/b/a/bo;

    move-result-object v0

    .line 155
    const/4 v1, 0x1

    new-array v9, v1, [Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 157
    invoke-virtual {v0}, Lcom/google/maps/b/a/bo;->a()Lcom/google/maps/b/a/cu;

    move-result-object v1

    .line 158
    iget-object v2, v0, Lcom/google/maps/b/a/bo;->b:Lcom/google/maps/b/a/cz;

    iget v2, v2, Lcom/google/maps/b/a/cz;->b:I

    .line 159
    iget-object v3, v0, Lcom/google/maps/b/a/bo;->c:Lcom/google/maps/b/a/cy;

    iget v4, v3, Lcom/google/maps/b/a/cy;->b:I

    new-array v4, v4, [I

    iget-object v5, v3, Lcom/google/maps/b/a/cy;->a:[I

    iget v3, v3, Lcom/google/maps/b/a/cy;->b:I

    invoke-static {v5, v12, v4, v12, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    iget-object v0, v0, Lcom/google/maps/b/a/bo;->d:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 161
    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 156
    invoke-static {v1, v2, v4, v0, v3}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Lcom/google/maps/b/a/cu;I[IILcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;

    move-result-object v0

    aput-object v0, v9, v12

    .line 165
    iget-object v0, p0, Lcom/google/maps/b/a/cd;->c:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v1, -0x80000000

    xor-int v5, v0, v1

    .line 167
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ba;

    const-wide/16 v2, 0x0

    .line 169
    iget-object v0, p0, Lcom/google/maps/b/a/cd;->b:Lcom/google/maps/b/a/cz;

    iget v4, v0, Lcom/google/maps/b/a/cz;->b:I

    const-string v11, ""

    new-array v12, v12, [I

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/ba;-><init>(JIII[I[[B[Lcom/google/android/apps/gmm/map/b/a/ax;Lcom/google/android/apps/gmm/map/internal/c/be;Ljava/lang/String;[I)V

    return-object v1
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/ba;
    .locals 13

    .prologue
    .line 96
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v6

    .line 98
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 99
    new-array v8, v1, [[B

    .line 100
    new-array v7, v1, [I

    .line 101
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 102
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    aput v2, v7, v0

    .line 103
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v2

    .line 104
    new-array v2, v2, [B

    aput-object v2, v8, v0

    .line 105
    aget-object v2, v8, v0

    invoke-interface {p0, v2}, Ljava/io/DataInput;->readFully([B)V

    .line 101
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 108
    :cond_0
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 109
    new-array v9, v1, [Lcom/google/android/apps/gmm/map/b/a/ax;

    .line 110
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 111
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    invoke-static {p0, v2}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/ax;

    move-result-object v2

    aput-object v2, v9, v0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 113
    :cond_1
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v1

    new-instance v11, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v11, v1, v0}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 116
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v1

    .line 117
    new-array v12, v1, [I

    .line 118
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v1, :cond_2

    .line 119
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v2

    aput v2, v12, v0

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 122
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ba;

    .line 123
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v2, v0

    .line 124
    iget v4, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    .line 125
    iget v5, p2, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    .line 130
    iget-object v0, v11, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v0, :cond_3

    const/4 v10, 0x0

    .line 131
    :goto_3
    iget v0, v11, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    .line 132
    iget-object v0, v11, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v0, :cond_5

    const/4 v11, 0x0

    :goto_4
    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/ba;-><init>(JIII[I[[B[Lcom/google/android/apps/gmm/map/b/a/ax;Lcom/google/android/apps/gmm/map/internal/c/be;Ljava/lang/String;[I)V

    return-object v1

    .line 130
    :cond_3
    iget-object v0, v11, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v10, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-nez v10, :cond_4

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v10

    goto :goto_3

    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v10, 0x0

    aget-object v10, v0, v10

    goto :goto_3

    .line 132
    :cond_5
    iget-object v0, v11, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto :goto_4
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->f:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->g:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->h:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 183
    const/16 v0, 0xd

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->j:[I

    return-object v0
.end method

.method public final k()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 268
    move v0, v1

    move v2, v1

    .line 269
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->d:[Lcom/google/android/apps/gmm/map/b/a/ax;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 270
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->d:[Lcom/google/android/apps/gmm/map/b/a/ax;

    aget-object v3, v3, v0

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v4, v4

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-nez v5, :cond_0

    move v3, v1

    :goto_1
    add-int/2addr v3, v4

    shl-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1c

    add-int/2addr v2, v3

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 270
    :cond_0
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v3, v3

    goto :goto_1

    :cond_1
    move v0, v1

    move v3, v1

    .line 273
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 274
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    aget-object v4, v4, v0

    array-length v4, v4

    add-int/2addr v3, v4

    .line 273
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 276
    :cond_2
    add-int/2addr v2, v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 278
    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->i:Ljava/lang/String;

    .line 279
    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x34

    return v0

    .line 278
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v0

    goto :goto_3

    .line 279
    :cond_4
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x28

    goto :goto_4
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 285
    new-instance v2, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "zGrade"

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->g:I

    .line 286
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "zWithinGrade"

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->h:I

    .line 287
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "encodings"

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->b:[I

    .line 288
    invoke-static {v3}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "nRasters"

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    array-length v3, v3

    .line 289
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/b/a/al;

    invoke-direct {v4}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v4, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "1st raster size"

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    array-length v3, v3

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    aget-object v1, v3, v1

    array-length v1, v1

    .line 290
    :cond_4
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "style"

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/ba;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 291
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 292
    invoke-virtual {v2}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
