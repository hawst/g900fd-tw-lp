.class public final Lcom/google/android/apps/gmm/base/views/d/a;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/base/g/c;)Ljava/lang/String;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const-wide v6, 0x412e848000000000L    # 1000000.0

    const/4 v2, 0x0

    .line 29
    if-eqz p1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    if-nez v0, :cond_1

    .line 42
    :cond_0
    :goto_0
    return-object v2

    .line 33
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/n;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v4, v6

    double-to-int v1, v4

    .line 34
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v4, v6

    double-to-int v4, v4

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/b/a/n;-><init>(II)V

    .line 35
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/n;)F

    move-result v1

    .line 37
    float-to-double v4, v1

    const-wide v6, 0x4122ebc000000000L    # 620000.0

    cmpl-double v0, v4, v6

    if-gez v0, :cond_0

    .line 41
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 42
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v0

    float-to-int v1, v1

    move v4, v3

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
