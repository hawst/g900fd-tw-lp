.class public Lcom/google/android/apps/gmm/place/reservation/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/reservation/b/e;


# instance fields
.field a:I

.field private b:Landroid/widget/NumberPicker$OnValueChangeListener;

.field private c:Landroid/content/Context;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/k;->c:Landroid/content/Context;

    .line 37
    iput p2, p0, Lcom/google/android/apps/gmm/place/reservation/k;->a:I

    .line 38
    iget v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->a:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->a:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_1

    .line 39
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->a:I

    .line 41
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x14

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/widget/NumberPicker$OnValueChangeListener;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->b:Landroid/widget/NumberPicker$OnValueChangeListener;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/reservation/l;-><init>(Lcom/google/android/apps/gmm/place/reservation/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->b:Landroid/widget/NumberPicker$OnValueChangeListener;

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->b:Landroid/widget/NumberPicker$OnValueChangeListener;

    return-object v0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/k;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/j;->H:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0x14

    const/4 v1, 0x1

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v8}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->d:Ljava/util/List;

    move v0, v1

    .line 91
    :goto_0
    if-gt v0, v8, :cond_0

    .line 92
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/k;->d:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/reservation/k;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/j;->H:I

    new-array v5, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/k;->d:Ljava/util/List;

    return-object v0
.end method
