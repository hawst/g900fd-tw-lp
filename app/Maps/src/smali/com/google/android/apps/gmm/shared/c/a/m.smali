.class Lcom/google/android/apps/gmm/shared/c/a/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Ljava/lang/Runnable;

.field final b:Ljava/util/concurrent/Semaphore;


# direct methods
.method constructor <init>(Ljava/lang/Runnable;Ljava/util/concurrent/Semaphore;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Semaphore;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/c/a/m;->a:Ljava/lang/Runnable;

    .line 54
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/c/a/m;->b:Ljava/util/concurrent/Semaphore;

    .line 55
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/m;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/m;->b:Ljava/util/concurrent/Semaphore;

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/m;->b:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 90
    :cond_0
    return-void

    .line 85
    :catchall_0
    move-exception v0

    .line 86
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/m;->b:Ljava/util/concurrent/Semaphore;

    if-eqz v1, :cond_1

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/gmm/shared/c/a/m;->b:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    :cond_1
    throw v0
.end method
