.class public Lcom/google/android/apps/gmm/map/o/b/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final o:Lcom/google/android/apps/gmm/map/o/b/g;

.field public static final p:Lcom/google/android/apps/gmm/map/o/b/g;


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:F

.field public final e:F

.field public final f:F

.field public final g:F

.field public final h:F

.field public final i:F

.field public final j:F

.field public final k:I

.field public final l:I

.field public final m:F

.field public final n:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b/f;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const v5, 0x40eaaaab

    const v7, 0x402aaaab

    .line 119
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/g;

    const v1, 0x3eaaaaab

    const v2, 0x40aaaaab

    const v3, 0x41555556

    const/high16 v4, 0x41200000    # 10.0f

    sget v9, Lcom/google/android/apps/gmm/f;->u:I

    sget v10, Lcom/google/android/apps/gmm/f;->q:I

    const/high16 v11, 0x40c00000    # 6.0f

    move v6, v5

    move v8, v7

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/gmm/map/o/b/g;-><init>(FFFFFFFFIIFLcom/google/b/c/dn;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    .line 130
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/g;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    sget-object v2, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v2, v2, Lcom/google/android/apps/gmm/map/o/b/g;->c:F

    sget-object v3, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v3, v3, Lcom/google/android/apps/gmm/map/o/b/g;->e:F

    sget-object v4, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v4, v4, Lcom/google/android/apps/gmm/map/o/b/g;->f:F

    sget-object v5, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v5, v5, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    sget-object v6, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v6, v6, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    sget-object v7, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v7, v7, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    sget-object v8, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v8, v8, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    sget v9, Lcom/google/android/apps/gmm/f;->v:I

    sget v10, Lcom/google/android/apps/gmm/f;->r:I

    sget-object v11, Lcom/google/android/apps/gmm/map/o/b/g;->o:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v11, v11, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/gmm/map/o/b/g;-><init>(FFFFFFFFIIFLcom/google/b/c/dn;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/g;->p:Lcom/google/android/apps/gmm/map/o/b/g;

    return-void
.end method

.method public constructor <init>(FFFFFFFFIIFLcom/google/b/c/dn;)V
    .locals 1
    .param p12    # Lcom/google/b/c/dn;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(FFFFFFFFIIF",
            "Lcom/google/b/c/dn",
            "<",
            "Lcom/google/android/apps/gmm/map/o/b/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    iput p1, p0, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    .line 152
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->c:F

    .line 153
    iput p3, p0, Lcom/google/android/apps/gmm/map/o/b/g;->e:F

    .line 154
    iput p4, p0, Lcom/google/android/apps/gmm/map/o/b/g;->f:F

    .line 155
    iput p5, p0, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    .line 156
    iput p6, p0, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    .line 157
    iput p7, p0, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    .line 158
    iput p8, p0, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    .line 159
    iput p9, p0, Lcom/google/android/apps/gmm/map/o/b/g;->k:I

    .line 160
    iput p10, p0, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    .line 161
    iput p11, p0, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    .line 162
    const/high16 v0, 0x40000000    # 2.0f

    div-float v0, p1, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    .line 163
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    add-float/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    .line 164
    if-eqz p12, :cond_0

    .line 166
    :goto_0
    iput-object p12, p0, Lcom/google/android/apps/gmm/map/o/b/g;->n:Lcom/google/b/c/dn;

    .line 167
    return-void

    .line 164
    :cond_0
    const-class v0, Lcom/google/android/apps/gmm/map/o/b/f;

    .line 166
    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/jp;->a(Ljava/lang/Iterable;)Lcom/google/b/c/dn;

    move-result-object p12

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 171
    if-ne p0, p1, :cond_1

    .line 180
    :cond_0
    :goto_0
    return v0

    .line 175
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/o/b/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 176
    goto :goto_0

    .line 179
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/o/b/g;

    .line 180
    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->c:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->e:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->f:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->f:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->k:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->k:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 197
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->a:F

    .line 198
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->b:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->c:F

    .line 199
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->d:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->e:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->f:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->g:F

    .line 200
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->h:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->j:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->k:I

    .line 201
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/b/g;->m:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    .line 197
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
