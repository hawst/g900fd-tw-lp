.class public Lcom/google/android/apps/gmm/base/activities/c;
.super Landroid/app/Activity;
.source "PG"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;
.implements Lcom/google/android/apps/gmm/base/activities/o;


# static fields
.field private static final t:Lcom/google/android/apps/gmm/map/x;

.field private static final u:Lcom/google/android/apps/gmm/base/activities/n;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private final D:Lcom/google/android/apps/gmm/l/ak;

.field private E:Lcom/google/android/apps/gmm/base/activities/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private F:Z

.field private G:Lcom/google/android/apps/gmm/v/e;

.field public a:Lcom/google/android/apps/gmm/base/j/b;

.field public b:Lcom/google/android/apps/gmm/map/MapFragment;

.field public c:Lcom/google/b/f/t;

.field public final d:Lcom/google/android/apps/gmm/map/t;

.field public e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

.field public f:Z

.field public g:Z

.field public final h:Lcom/google/android/apps/gmm/util/a/a;

.field public final i:Lcom/google/android/apps/gmm/util/b;

.field public final j:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/apps/gmm/l/av;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/google/android/apps/gmm/base/views/h;

.field public l:Z

.field m:Z

.field public n:Z

.field public o:Ljava/lang/Object;

.field public p:Ljava/lang/Object;

.field public q:Lcom/google/android/apps/gmm/base/activities/ae;

.field public r:Lcom/google/android/libraries/curvular/bd;

.field public s:Lcom/google/android/apps/gmm/aa/a/e;

.field private v:Lcom/google/android/apps/gmm/l/i;

.field private w:Lcom/google/android/apps/gmm/l/p;

.field private x:Lcom/google/android/apps/gmm/l/am;

.field private y:Lcom/google/android/apps/gmm/l/ar;

.field private z:Lcom/google/android/apps/gmm/base/activities/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/android/apps/gmm/map/al;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/al;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/c;->t:Lcom/google/android/apps/gmm/map/x;

    .line 207
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/d;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/c;->u:Lcom/google/android/apps/gmm/base/activities/n;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 164
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 260
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/c;->u:Lcom/google/android/apps/gmm/base/activities/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    .line 263
    sget-object v0, Lcom/google/b/f/t;->fX:Lcom/google/b/f/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->c:Lcom/google/b/f/t;

    .line 265
    new-instance v0, Lcom/google/android/apps/gmm/map/t;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 276
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    .line 280
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->B:Z

    .line 292
    new-instance v0, Lcom/google/android/apps/gmm/util/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/util/a/a;-><init>(Lcom/google/android/apps/gmm/shared/c/a/p;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->h:Lcom/google/android/apps/gmm/util/a/a;

    .line 294
    new-instance v0, Lcom/google/android/apps/gmm/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    .line 296
    new-instance v0, Lcom/google/android/apps/gmm/l/ak;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/l/ak;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->D:Lcom/google/android/apps/gmm/l/ak;

    .line 305
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->j:Ljava/util/Stack;

    .line 310
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->l:Z

    .line 314
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->m:Z

    .line 354
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->n:Z

    .line 359
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/activities/f;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->o:Ljava/lang/Object;

    .line 414
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/activities/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->p:Ljava/lang/Object;

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;
    .locals 2

    .prologue
    .line 229
    move-object v0, p0

    :goto_0
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v1, :cond_0

    .line 230
    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    return-object v0

    .line 231
    :cond_0
    instance-of v1, v0, Landroid/content/ContextWrapper;

    if-eqz v1, :cond_1

    .line 232
    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    .line 234
    :cond_1
    new-instance v0, Ljava/lang/ClassCastException;

    invoke-direct {v0}, Ljava/lang/ClassCastException;-><init>()V

    throw v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1412
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    .line 1413
    if-le v0, p1, :cond_0

    .line 1414
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    add-int/lit8 v0, v0, -0x1

    sub-int/2addr v0, p1

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->getBackStackEntryAt(I)Landroid/app/FragmentManager$BackStackEntry;

    move-result-object v0

    .line 1415
    :goto_0
    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    return-object v0

    .line 1414
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1415
    :cond_1
    invoke-interface {v0}, Landroid/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Landroid/app/Fragment;",
            ">;",
            "Lcom/google/android/apps/gmm/base/fragments/a/a;",
            ")",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1263
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1264
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    .line 1265
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1266
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1267
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/o;)V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Lcom/google/android/apps/gmm/l/o;)V

    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/l/o;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1054
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 1061
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1054
    goto :goto_0

    .line 1059
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/l/o;->a()V

    .line 1060
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->x_()Lcom/google/android/apps/gmm/util/replay/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;-><init>(Landroid/content/Intent;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/util/replay/a;->a(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static g()Lcom/google/android/apps/gmm/map/x;
    .locals 1

    .prologue
    .line 1610
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/c;->t:Lcom/google/android/apps/gmm/map/x;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 7
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 709
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v4

    move v1, v3

    :goto_0
    if-ge v1, v4, :cond_2

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v2

    :goto_1
    if-nez v1, :cond_3

    .line 722
    :cond_0
    :goto_2
    return-object v0

    .line 709
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    .line 712
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v1

    .line 713
    :goto_3
    if-eqz v1, :cond_0

    .line 714
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v0, v1

    .line 715
    goto :goto_2

    .line 717
    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/activities/n;->b()Ljava/lang/Object;

    move-result-object v4

    if-eq v1, v4, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    move v1, v2

    :goto_4
    if-eqz v1, :cond_0

    .line 720
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v1

    goto :goto_3

    :cond_5
    move v1, v3

    .line 717
    goto :goto_4
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 599
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/activities/n;->a()V

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/c;->u:Lcom/google/android/apps/gmm/base/activities/n;

    if-eq v0, v1, :cond_0

    .line 601
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->b()V

    .line 606
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 607
    :goto_0
    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    if-lez v1, :cond_1

    .line 608
    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0

    .line 612
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->N()Lcom/google/android/apps/gmm/tutorial/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/tutorial/a/a;->e()Z

    .line 615
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 616
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;-><init>()V

    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 617
    return-void
.end method

.method public final a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1208
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 1210
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    if-ne p2, v0, :cond_1

    .line 1211
    invoke-static {p0}, Lcom/google/android/apps/gmm/base/fragments/m;->a(Lcom/google/android/apps/gmm/base/activities/c;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1255
    :goto_0
    return-void

    .line 1218
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 1219
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    .line 1218
    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1220
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/base/activities/c;->b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0

    .line 1225
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1226
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1228
    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    if-ne p2, v2, :cond_4

    .line 1231
    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 1232
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 1233
    if-eqz v2, :cond_2

    .line 1234
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1236
    :cond_2
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1240
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(I)Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 1241
    iget-object v3, v3, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    .line 1240
    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1242
    sget-object v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    .line 1243
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 1244
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1250
    :cond_3
    :goto_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2c

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1251
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 1254
    invoke-virtual {v0}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    goto :goto_0

    .line 1247
    :cond_4
    iget-object v2, p2, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v1, p1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_1
.end method

.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1955
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->k:Lcom/google/android/apps/gmm/base/views/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/h;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/h;->a:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iput v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->requestLayout()V

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v4, v4, v2}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v2

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v4, v4, v1}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    invoke-virtual {p1, v2, v1}, Landroid/view/View;->measure(II)V

    new-instance v1, Lcom/google/android/apps/gmm/base/views/j;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v1, v2, v4, v3}, Lcom/google/android/apps/gmm/base/views/j;-><init>(Landroid/view/View;II)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1956
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/n;)V
    .locals 6

    .prologue
    .line 651
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 652
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/c;->u:Lcom/google/android/apps/gmm/base/activities/n;

    if-eq v0, v1, :cond_0

    .line 653
    const-string v0, "GmmActivity"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x3e

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Tried to register ResetInterceptor "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " when "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is still registered."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 656
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    .line 657
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 3
    .param p1    # Ljava/lang/Runnable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 632
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 634
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xc

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "interceptor="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 635
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    new-instance v1, Lcom/google/android/apps/gmm/base/activities/h;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/activities/h;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/activities/n;->a(Ljava/lang/Runnable;)V

    .line 648
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 885
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v0, :cond_1

    .line 886
    const-string v0, "Dropping intent, not resumed: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 897
    :goto_0
    return-void

    .line 886
    :cond_0
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 890
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 891
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->w:Lcom/google/android/apps/gmm/l/p;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/l/p;->a(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/o;

    move-result-object v0

    .line 892
    if-nez v0, :cond_3

    .line 893
    const-string v0, "URL parsed to null intent: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 896
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/l/o;->a()V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 660
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/c;->u:Lcom/google/android/apps/gmm/base/activities/n;

    if-ne v0, v1, :cond_0

    .line 662
    const-string v0, "GmmActivity"

    const-string v1, "Tried to unregister a non-existant ResetInterceptor."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 664
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/c;->u:Lcom/google/android/apps/gmm/base/activities/n;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->z:Lcom/google/android/apps/gmm/base/activities/n;

    .line 665
    return-void
.end method

.method public final b(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V
    .locals 2

    .prologue
    .line 1301
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 1304
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 1305
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 1312
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->a:Lcom/google/android/apps/gmm/base/activities/p;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 1313
    return-void
.end method

.method final c()V
    .locals 4

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->G:Lcom/google/android/apps/gmm/v/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 1042
    return-void
.end method

.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 4

    .prologue
    .line 1770
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1773
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/base/activities/m;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/base/activities/m;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/StringBuilder;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->b(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 1799
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1800
    const/4 v0, 0x0

    .line 1803
    :goto_0
    return-object v0

    .line 1802
    :cond_0
    new-instance v0, Landroid/nfc/NdefMessage;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/nfc/NdefRecord;

    const/4 v3, 0x0

    .line 1803
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/nfc/NdefRecord;->createUri(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-direct {v0, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0
.end method

.method public final d()Landroid/app/Fragment;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1425
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 1426
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1427
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1433
    :goto_0
    return-object v0

    .line 1428
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1429
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    goto :goto_0

    .line 1430
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1431
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->c:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    goto :goto_0

    .line 1433
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1393
    :try_start_0
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1400
    :goto_0
    return v0

    .line 1394
    :catch_0
    move-exception v0

    .line 1399
    const-string v1, "GmmActivity"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1400
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1439
    invoke-super {p0, p1, p2, p3, p4}, Landroid/app/Activity;->dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1440
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->x_()Lcom/google/android/apps/gmm/util/replay/a;

    move-result-object v0

    invoke-interface {v0, p1, p3}, Lcom/google/android/apps/gmm/util/replay/a;->a(Ljava/lang/String;Ljava/io/Writer;)V

    .line 1441
    return-void
.end method

.method public final e()Lcom/google/android/apps/gmm/map/t;
    .locals 1

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/a;
    .locals 1

    .prologue
    .line 1466
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    return-object v0
.end method

.method public getActionBar()Landroid/app/ActionBar;
    .locals 3

    .prologue
    .line 848
    const-string v0, "GmmActivity"

    const-string v1, "getActionBar() should not be called, use GmmActivityFragment#getHeaderView() instead"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 850
    invoke-super {p0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 1629
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_1

    .line 1630
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    .line 1631
    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->i()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1632
    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->k()V

    .line 1662
    :cond_0
    :goto_0
    return-void

    .line 1639
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v1

    .line 1640
    instance-of v0, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 1642
    check-cast v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1643
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "lastFragment manages onBackPressed(), lastFragment="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1649
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/layout/MainLayout;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1656
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->n:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    .line 1661
    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    goto :goto_0
.end method

.method public i()V
    .locals 4

    .prologue
    .line 1878
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ax:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v2, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1880
    :cond_0
    return-void
.end method

.method public isDestroyed()Z
    .locals 1

    .prologue
    .line 1196
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->C:Z

    return v0
.end method

.method public final j()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1962
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->k:Lcom/google/android/apps/gmm/base/views/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/h;->a:Z

    if-nez v1, :cond_1

    .line 1963
    :cond_0
    :goto_0
    return-void

    .line 1962
    :cond_1
    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/views/h;->a:Z

    new-instance v1, Lcom/google/android/apps/gmm/base/views/j;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/apps/gmm/base/views/j;-><init>(Landroid/view/View;II)V

    new-instance v2, Lcom/google/android/apps/gmm/base/views/i;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/views/i;-><init>(Lcom/google/android/apps/gmm/base/views/h;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/h;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1716
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->D:Lcom/google/android/apps/gmm/l/ak;

    iget-object v0, v1, Lcom/google/android/apps/gmm/l/ak;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/l/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    .line 1745
    :goto_1
    return-void

    .line 1716
    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/l/ak;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v0, 0x1

    goto :goto_0

    .line 1722
    :cond_1
    if-ltz p1, :cond_2

    invoke-static {}, Lcom/google/android/apps/gmm/l/al;->values()[Lcom/google/android/apps/gmm/l/al;

    move-result-object v0

    array-length v0, v0

    if-ge p1, v0, :cond_2

    .line 1723
    invoke-static {}, Lcom/google/android/apps/gmm/l/al;->values()[Lcom/google/android/apps/gmm/l/al;

    move-result-object v0

    aget-object v0, v0, p1

    .line 1724
    :goto_2
    sget-object v1, Lcom/google/android/apps/gmm/base/activities/e;->a:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/l/al;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 1742
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_1

    .line 1723
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/l/al;->a:Lcom/google/android/apps/gmm/l/al;

    goto :goto_2

    .line 1726
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->O()Lcom/google/android/apps/gmm/aa/c/a/a;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/android/apps/gmm/aa/c/a/a;->a(ILandroid/content/Intent;)V

    goto :goto_1

    .line 1729
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->o()Lcom/google/android/apps/gmm/feedback/a/e;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/feedback/a/e;->a(I)V

    goto :goto_1

    .line 1732
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0, p0, p2, p3}, Lcom/google/android/apps/gmm/login/a/a;->a(Landroid/app/Activity;ILandroid/content/Intent;)V

    goto :goto_1

    .line 1735
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->p()Lcom/google/android/apps/gmm/j/a/a;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/j/a/a;->a(I)V

    goto :goto_1

    .line 1738
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->s()Lcom/google/android/apps/gmm/mylocation/b/b;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/apps/gmm/mylocation/b/b;->a(I)V

    goto :goto_1

    .line 1724
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1666
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->f:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 1667
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->h()V

    .line 1668
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 432
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/z/a/b;->a(Landroid/app/Activity;)V

    .line 434
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/base/a;->a(Landroid/content/Context;)Lcom/google/android/libraries/curvular/bd;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    .line 436
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 440
    new-instance v4, Lcom/google/android/apps/gmm/base/k/p;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    .line 441
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/apps/gmm/base/k/p;-><init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/b/a;)V

    .line 440
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 443
    new-instance v0, Lcom/google/android/libraries/curvular/c/b;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/c/b;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445
    new-instance v0, Lcom/google/android/libraries/curvular/j;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v0, v4}, Lcom/google/android/libraries/curvular/j;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    new-instance v4, Lcom/google/android/libraries/curvular/an;

    invoke-direct {v4, v3}, Lcom/google/android/libraries/curvular/an;-><init>(Ljava/util/List;)V

    iput-object v4, v0, Lcom/google/android/libraries/curvular/bd;->c:Lcom/google/android/libraries/curvular/cp;

    .line 447
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    iget-object v3, v0, Lcom/google/android/libraries/curvular/bd;->c:Lcom/google/android/libraries/curvular/cp;

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v3, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->g:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v3, v0}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 449
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 456
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->C:Z

    .line 457
    new-instance v3, Lcom/google/android/apps/gmm/shared/a/b;

    .line 472
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/shared/a/b;-><init>(Landroid/app/Activity;Lcom/google/android/apps/gmm/shared/net/a/b;)V

    .line 469
    invoke-static {v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Lcom/google/android/apps/gmm/shared/a/a;)V

    .line 474
    new-instance v0, Lcom/google/android/apps/gmm/l/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/l/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->v:Lcom/google/android/apps/gmm/l/i;

    .line 475
    new-instance v3, Lcom/google/android/apps/gmm/l/am;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/l/am;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->x:Lcom/google/android/apps/gmm/l/am;

    .line 476
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->x:Lcom/google/android/apps/gmm/l/am;

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/am;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v3, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 477
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/base/a;->a(Landroid/app/Activity;)Lcom/google/android/apps/gmm/base/j/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 479
    new-instance v0, Lcom/google/android/apps/gmm/l/p;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->v:Lcom/google/android/apps/gmm/l/i;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->x:Lcom/google/android/apps/gmm/l/am;

    invoke-direct {v0, p0, v3, v4}, Lcom/google/android/apps/gmm/l/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/i;Lcom/google/android/apps/gmm/l/am;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->w:Lcom/google/android/apps/gmm/l/p;

    .line 481
    new-instance v0, Lcom/google/android/apps/gmm/l/ar;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/l/ar;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->y:Lcom/google/android/apps/gmm/l/ar;

    .line 483
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/apps/gmm/m;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/c;->setTheme(I)V

    :cond_1
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/c;->setVolumeControlStream(I)V

    sget v0, Lcom/google/android/apps/gmm/h;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/c;->setContentView(I)V

    sget v0, Lcom/google/android/apps/gmm/g;->bD:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->e:Lcom/google/android/apps/gmm/base/layout/MainLayout;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v0, "GMM_MAP_FRAGMENT"

    invoke-virtual {v4, v0}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/MapFragment;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v0, :cond_6

    move v3, v1

    :goto_1
    if-nez p1, :cond_7

    move v0, v1

    :goto_2
    if-eq v3, v0, :cond_4

    const-string v0, "GmmActivity"

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x25

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "mapFragment = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, "; savedInstanceState = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v5, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    if-nez v0, :cond_12

    new-instance v0, Lcom/google/android/apps/gmm/map/MapFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/MapFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    move v0, v1

    :goto_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    sget-object v6, Lcom/google/android/apps/gmm/base/activities/c;->t:Lcom/google/android/apps/gmm/map/x;

    iget-object v7, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    if-nez v7, :cond_5

    if-nez v5, :cond_8

    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Should only be set once, and to non-null."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v3, v2

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    iput-object v6, v3, Lcom/google/android/apps/gmm/map/MapFragment;->b:Lcom/google/android/apps/gmm/map/x;

    iput-object v5, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-boolean v5, v3, Lcom/google/android/apps/gmm/map/MapFragment;->c:Z

    if-eqz v5, :cond_9

    iget-object v5, v3, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/MapFragment;->b()Landroid/graphics/Point;

    move-result-object v3

    iput-object v6, v5, Lcom/google/android/apps/gmm/map/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    iput-object v3, v5, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    :cond_9
    new-instance v3, Lcom/google/android/apps/gmm/base/activities/i;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/base/activities/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->G:Lcom/google/android/apps/gmm/v/e;

    if-eqz v0, :cond_a

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    const-string v5, "GMM_MAP_FRAGMENT"

    invoke-virtual {v0, v3, v5}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_a
    invoke-virtual {v4}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/activities/ae;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->o:Ljava/lang/Object;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->a()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->p()Lcom/google/android/apps/gmm/j/a/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/j/a/a;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->F()Lcom/google/android/apps/gmm/place/reservation/ai;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/reservation/ai;->b:Lcom/google/android/apps/gmm/place/reservation/ah;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/place/reservation/ah;->a(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_f

    new-instance v0, Lcom/google/android/apps/gmm/base/activities/a;

    invoke-direct {v0, p0, v3, v4}, Lcom/google/android/apps/gmm/base/activities/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/login/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)V

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->E:Lcom/google/android/apps/gmm/base/activities/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->E:Lcom/google/android/apps/gmm/base/activities/a;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->E:Lcom/google/android/apps/gmm/base/activities/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/a;->e:Lcom/google/android/apps/gmm/i/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/i/a;->a()V

    :cond_b
    if-nez p1, :cond_10

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->A:Z

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->A:Z

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    .line 493
    :cond_c
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->D:Lcom/google/android/apps/gmm/l/ak;

    if-eqz p1, :cond_d

    const-string v0, "pendingRequests"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, v3, Lcom/google/android/apps/gmm/l/ak;->b:Ljava/util/HashMap;

    .line 495
    :cond_d
    new-instance v3, Lcom/google/android/apps/gmm/base/views/h;

    sget v0, Lcom/google/android/apps/gmm/g;->E:I

    .line 496
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/base/views/h;-><init>(Landroid/view/ViewGroup;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->k:Lcom/google/android/apps/gmm/base/views/h;

    .line 498
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v1, :cond_11

    :goto_6
    if-eqz v1, :cond_e

    .line 505
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->e:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_e

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 508
    :cond_e
    new-instance v1, Lcom/google/android/apps/gmm/aa/a/b;

    .line 509
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->q_()Lcom/google/android/apps/gmm/aa/a/d;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/apps/gmm/aa/a/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/aa/a/d;Lcom/google/android/apps/gmm/navigation/a/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->s:Lcom/google/android/apps/gmm/aa/a/e;

    .line 510
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->s:Lcom/google/android/apps/gmm/aa/a/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/aa/a/e;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 512
    invoke-static {p0}, Lcom/google/android/apps/gmm/base/g/g;->a(Landroid/content/Context;)V

    .line 516
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 517
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->setIntent(Landroid/content/Intent;)V

    .line 518
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/activities/c;->onNewIntent(Landroid/content/Intent;)V

    .line 521
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/a;->a(Lcom/google/android/libraries/curvular/bd;)V

    .line 536
    return-void

    .line 483
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_10
    move v0, v2

    goto/16 :goto_5

    :cond_11
    move v1, v2

    .line 498
    goto/16 :goto_6

    :cond_12
    move v0, v2

    goto/16 :goto_3
.end method

.method protected onDestroy()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1150
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->C:Z

    .line 1151
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->o:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 1154
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->f()V

    .line 1155
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->x:Lcom/google/android/apps/gmm/l/am;

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/am;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 1156
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1160
    invoke-static {}, Landroid/net/http/HttpResponseCache;->getInstalled()Landroid/net/http/HttpResponseCache;

    move-result-object v0

    .line 1161
    if-eqz v0, :cond_0

    .line 1162
    const-string v1, "HttpResponseCache (Request Count: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1163
    invoke-virtual {v0}, Landroid/net/http/HttpResponseCache;->getRequestCount()I

    move-result v2

    .line 1164
    invoke-virtual {v0}, Landroid/net/http/HttpResponseCache;->getNetworkCount()I

    move-result v3

    .line 1165
    invoke-virtual {v0}, Landroid/net/http/HttpResponseCache;->getHitCount()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x40

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Network Count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Hit Count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1162
    invoke-virtual {v0}, Landroid/net/http/HttpResponseCache;->flush()V

    .line 1169
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->E:Lcom/google/android/apps/gmm/base/activities/a;

    if-eqz v0, :cond_1

    .line 1170
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->E:Lcom/google/android/apps/gmm/base/activities/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v2, Lcom/google/android/apps/gmm/base/activities/b;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/base/activities/b;-><init>(Lcom/google/android/apps/gmm/base/activities/a;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 1174
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    iget-object v1, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/bd;->g:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v0}, Lcom/google/b/c/am;->clear()V

    .line 1182
    iput-object v7, p0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    .line 1184
    invoke-static {v7}, Lcom/google/android/apps/gmm/shared/c/m;->a(Lcom/google/android/apps/gmm/shared/a/a;)V

    .line 1186
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->s:Lcom/google/android/apps/gmm/aa/a/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/aa/a/e;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 1187
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 1188
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1384
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->N()Lcom/google/android/apps/gmm/tutorial/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/tutorial/a/a;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1385
    const/4 v0, 0x1

    .line 1387
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1368
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 1378
    :goto_0
    return v0

    .line 1372
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v2, :cond_1

    move v1, v2

    :cond_1
    if-eqz v1, :cond_2

    const/16 v0, 0x52

    if-ne p1, v0, :cond_2

    .line 1374
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/e/d;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/e/d;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    move v0, v2

    .line 1375
    goto :goto_0

    .line 1378
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onLowMemory()V
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    .line 248
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 864
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xd

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onNewIntent("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ")"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    .line 865
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->w:Lcom/google/android/apps/gmm/l/p;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/l/p;->a(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/o;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    .line 869
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-nez v0, :cond_2

    .line 878
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 865
    goto :goto_0

    .line 877
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->y:Lcom/google/android/apps/gmm/l/ar;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->w:Lcom/google/android/apps/gmm/l/p;

    iget-object v4, v3, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    if-eqz v4, :cond_3

    iget-object v4, v3, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/l/o;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    move v2, v1

    :cond_3
    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v2}, Lcom/google/android/apps/gmm/l/ai;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/activities/ae;->e:Z

    if-nez v2, :cond_0

    iget-object v2, v3, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    const/4 v4, 0x0

    iput-object v4, v3, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    iget-object v2, v2, Lcom/google/android/apps/gmm/l/o;->a:Landroid/content/Intent;

    sget-object v3, Lcom/google/android/apps/gmm/l/ar;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1e

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Turning on screen for intent: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x15

    if-ge v3, v4, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/ar;->d:Landroid/os/PowerManager$WakeLock;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/ar;->d:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v4, 0x64

    invoke-virtual {v3, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/ar;->d:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/ar;->d:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    sget-object v3, Lcom/google/android/apps/gmm/l/ar;->a:Ljava/lang/String;

    :cond_4
    :goto_2
    iget-object v3, v0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v3

    const v4, 0x680080

    invoke-virtual {v3, v4}, Landroid/view/Window;->addFlags(I)V

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/ar;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/base/activities/ae;->e:Z

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/ar;->c:Lcom/google/android/apps/gmm/l/an;

    iget-object v4, v3, Lcom/google/android/apps/gmm/l/an;->b:Landroid/hardware/Sensor;

    if-eqz v4, :cond_5

    iget-boolean v4, v3, Lcom/google/android/apps/gmm/l/an;->d:Z

    if-nez v4, :cond_5

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/l/an;->d:Z

    iget-object v1, v3, Lcom/google/android/apps/gmm/l/an;->a:Landroid/hardware/SensorManager;

    iget-object v4, v3, Lcom/google/android/apps/gmm/l/an;->b:Landroid/hardware/Sensor;

    const/4 v5, 0x2

    invoke-virtual {v1, v3, v4, v5}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_5
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/google/android/apps/gmm/l/as;

    invoke-direct {v3, v0, v2}, Lcom/google/android/apps/gmm/l/as;-><init>(Lcom/google/android/apps/gmm/l/ar;Landroid/content/Intent;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x2

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    new-instance v2, Lcom/google/android/apps/gmm/l/at;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/l/at;-><init>(Lcom/google/android/apps/gmm/l/ar;)V

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0xf

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_1

    :cond_6
    sget-object v3, Lcom/google/android/apps/gmm/l/ar;->a:Ljava/lang/String;

    goto :goto_2
.end method

.method protected onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1113
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_3

    .line 1118
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    .line 1119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->M:Z

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->Q:Landroid/animation/AnimatorSet;

    invoke-virtual {v2}, Landroid/animation/AnimatorSet;->cancel()V

    :cond_0
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/p;->S:Lcom/google/android/apps/gmm/util/a/e;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/util/a/e;->a()V

    iput-object v3, v0, Lcom/google/android/apps/gmm/base/activities/ae;->b:Lcom/google/android/apps/gmm/base/activities/p;

    .line 1120
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->d()V

    .line 1123
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->i:Lcom/google/android/apps/gmm/util/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    iput-object v3, v0, Lcom/google/android/apps/gmm/util/b;->c:Landroid/app/Dialog;

    .line 1124
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 1125
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->y()V

    .line 1127
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 1133
    if-eqz v0, :cond_4

    .line 1134
    new-array v1, v4, [Landroid/app/Activity;

    invoke-virtual {v0, v3, p0, v1}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 1141
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/x/a;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v2, Lcom/google/android/apps/gmm/x/e;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/x/e;-><init>(Lcom/google/android/apps/gmm/x/a;)V

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/c/a/j;->b(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 1143
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/b/h;

    invoke-direct {v1, v4}, Lcom/google/android/apps/gmm/navigation/b/h;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 1145
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 1146
    return-void
.end method

.method protected onRestart()V
    .locals 0

    .prologue
    .line 573
    invoke-super {p0}, Landroid/app/Activity;->onRestart()V

    .line 574
    return-void
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 905
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 906
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/b/h;

    invoke-direct {v1, v5}, Lcom/google/android/apps/gmm/navigation/b/h;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 911
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->E:Lcom/google/android/apps/gmm/base/activities/a;

    if-eqz v0, :cond_1

    .line 912
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->E:Lcom/google/android/apps/gmm/base/activities/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/a;->d:Lcom/google/android/gms/clearcut/a;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/clearcut/a;->a([B)Lcom/google/android/gms/clearcut/c;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/a;->b:Lcom/google/android/apps/gmm/login/a/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/clearcut/c;->a(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;

    move-result-object v1

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/activities/a;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/clearcut/c;->b(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/a;->e:Lcom/google/android/apps/gmm/i/a;

    iget-object v3, v0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    if-nez v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/i/a;->b:Lcom/google/android/gms/common/api/p;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/p;->a()Lcom/google/android/gms/common/api/o;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/i/a;->c:Lcom/google/android/gms/common/api/o;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/clearcut/c;->a(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/common/api/s;

    const-string v0, "ClearcutController"

    const-string v1, "Logged startup for play analytics."

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 916
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ax:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    sget-object v6, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v5, :cond_6

    move v0, v5

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->ax:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v7, Lcom/google/android/apps/gmm/d/a;->c:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v3, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->B:Z

    if-eqz v0, :cond_8

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->A:Z

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/r/b/a/av;->a:Lcom/google/r/b/a/av;

    :goto_2
    move-object v3, v0

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v3, v1, v6}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/av;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->B:Z

    .line 918
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    const/high16 v3, -0x40800000    # -1.0f

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->screenBrightness:F

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 920
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->w:Lcom/google/android/apps/gmm/l/p;

    iget-object v3, v0, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    if-nez v3, :cond_9

    move v0, v4

    :goto_4
    if-eqz v0, :cond_3

    .line 921
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "setOneBackTapBackground: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iput-boolean v5, p0, Lcom/google/android/apps/gmm/base/activities/c;->n:Z

    .line 925
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_a

    .line 1004
    :cond_4
    :goto_5
    return-void

    :cond_5
    move-object v1, v2

    .line 916
    goto/16 :goto_0

    :cond_6
    move v0, v4

    goto/16 :goto_1

    :cond_7
    sget-object v0, Lcom/google/r/b/a/av;->b:Lcom/google/r/b/a/av;

    goto :goto_2

    :cond_8
    sget-object v0, Lcom/google/r/b/a/av;->c:Lcom/google/r/b/a/av;

    move-object v3, v0

    goto :goto_3

    .line 920
    :cond_9
    iget-object v0, v0, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/o;->a:Landroid/content/Intent;

    const-string v3, "GMM_ENABLE_ONE_BACK_TAP"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    goto :goto_4

    .line 932
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->x()V

    .line 933
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->p:Ljava/lang/Object;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 934
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->c()V

    .line 936
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->A:Z

    if-eqz v0, :cond_b

    .line 937
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/activities/InitialGmmFragment;-><init>()V

    sget-object v6, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/base/fragments/a/a;->d:Ljava/lang/String;

    invoke-virtual {v0, v3, v6}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 941
    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->K()Lcom/google/android/apps/gmm/w/a/a;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/w/a/a;->a(Ljava/lang/String;)V

    .line 943
    iput-boolean v5, p0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    .line 946
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    .line 948
    if-eqz v0, :cond_c

    .line 949
    new-array v1, v4, [Landroid/app/Activity;

    invoke-virtual {v0, p0, p0, v1}, Landroid/nfc/NfcAdapter;->setNdefPushMessageCallback(Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;Landroid/app/Activity;[Landroid/app/Activity;)V

    .line 952
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->l:Z

    if-eqz v0, :cond_4

    .line 957
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->w:Lcom/google/android/apps/gmm/l/p;

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/p;->b:Lcom/google/android/apps/gmm/l/o;

    .line 963
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, -0x1

    invoke-direct {v0, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 964
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->F:Z

    .line 966
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->m:Z

    if-nez v0, :cond_e

    .line 967
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/k;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/base/activities/k;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/l/o;)V

    .line 976
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/l;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/base/activities/l;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 988
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v2

    invoke-interface {v2, v1, v0}, Lcom/google/android/apps/gmm/t/a/a;->a(Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 1003
    :cond_d
    :goto_6
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/activities/c;->A:Z

    goto/16 :goto_5

    .line 995
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->d()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 996
    if-eqz v1, :cond_f

    .line 997
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Lcom/google/android/apps/gmm/l/o;)V

    .line 999
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/activities/c;->G:Lcom/google/android/apps/gmm/v/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v1, v5}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_6
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 579
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 580
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->p()Lcom/google/android/apps/gmm/j/a/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/j/a/a;->b(Landroid/os/Bundle;)V

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->D:Lcom/google/android/apps/gmm/l/ak;

    iget-object v1, v0, Lcom/google/android/apps/gmm/l/ak;->b:Ljava/util/HashMap;

    if-eqz v1, :cond_0

    const-string v1, "pendingRequests"

    iget-object v0, v0, Lcom/google/android/apps/gmm/l/ak;->b:Ljava/util/HashMap;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->d()Lcom/google/android/apps/gmm/mylocation/b/f;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/mylocation/b/f;->b(Landroid/os/Bundle;)V

    .line 583
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->F()Lcom/google/android/apps/gmm/place/reservation/ai;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/reservation/ai;->b:Lcom/google/android/apps/gmm/place/reservation/ah;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v2, Lcom/google/android/apps/gmm/place/reservation/ah;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/place/reservation/ag;

    iget-object v5, v2, Lcom/google/android/apps/gmm/place/reservation/ah;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/reservation/ag;->e:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    sub-long/2addr v6, v8

    sget-wide v8, Lcom/google/android/apps/gmm/place/reservation/ah;->a:J

    cmp-long v1, v6, v8

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/place/reservation/ah;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_3
    iget-object v0, v2, Lcom/google/android/apps/gmm/place/reservation/ah;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "reservation_widget_saved_states"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 584
    :cond_4
    return-void
.end method

.method protected onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 540
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 541
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/h/c;->b()V

    .line 544
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->b()V

    .line 552
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->q()Lcom/google/android/apps/gmm/shared/net/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/o;->a()V

    .line 554
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/base/activities/c;->B:Z

    .line 555
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 559
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/av;->d:Lcom/google/r/b/a/av;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/av;)Ljava/lang/String;

    .line 561
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->e()V

    .line 562
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/h/c;->c()V

    .line 564
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->w()Lcom/google/android/apps/gmm/place/riddler/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/riddler/a;->a()V

    .line 568
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 569
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 5

    .prologue
    .line 240
    const-string v0, "GmmActivity"

    const-string v1, "onTrimMemory(%d)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t_()Lcom/google/android/apps/gmm/map/util/a/b;

    move-result-object v0

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/b;->a(F)I

    .line 242
    return-void
.end method

.method public onUserInteraction()V
    .locals 2

    .prologue
    .line 1914
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/e/f;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/e/f;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 1916
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->y:Lcom/google/android/apps/gmm/l/ar;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/l/ar;->a()V

    .line 1921
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2

    .prologue
    .line 1909
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/e/g;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/base/e/g;-><init>(Z)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 1910
    return-void
.end method
