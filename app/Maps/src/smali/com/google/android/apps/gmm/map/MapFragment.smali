.class public Lcom/google/android/apps/gmm/map/MapFragment;
.super Landroid/app/Fragment;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/t;

.field public b:Lcom/google/android/apps/gmm/map/x;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/o/ar;
    .locals 2

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    sget-object v0, Lcom/google/android/apps/gmm/map/o/ar;->d:Lcom/google/android/apps/gmm/map/o/ar;

    .line 113
    :goto_0
    return-object v0

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 111
    sget-object v0, Lcom/google/android/apps/gmm/map/o/ar;->e:Lcom/google/android/apps/gmm/map/o/ar;

    goto :goto_0

    .line 113
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/o/ar;->c:Lcom/google/android/apps/gmm/map/o/ar;

    goto :goto_0
.end method

.method public final b()Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 122
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    iget v1, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/e;->bs:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/e;->aJ:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 130
    :cond_0
    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 96
    invoke-super {p0, p1}, Landroid/app/Fragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->b()Landroid/graphics/Point;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->x()V

    .line 98
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 49
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->c:Z

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->b()Landroid/graphics/Point;

    move-result-object v2

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    .line 54
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->a()Lcom/google/android/apps/gmm/map/o/ar;

    move-result-object v2

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v3, Lcom/google/android/apps/gmm/g;->bE:I

    invoke-virtual {v1, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    new-instance v4, Lcom/google/android/apps/gmm/map/aj;

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/MapFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/google/android/apps/gmm/map/aj;-><init>(Landroid/content/Context;)V

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ai;->h:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/MapFragment;->b:Lcom/google/android/apps/gmm/map/x;

    move-object v1, p1

    .line 70
    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/t;->a(Landroid/view/LayoutInflater;Lcom/google/android/apps/gmm/map/o/ar;Landroid/widget/TextView;Lcom/google/android/apps/gmm/map/s;Lcom/google/android/apps/gmm/map/b/a/ai;ZLcom/google/android/apps/gmm/map/x;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->a()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 63
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->c:Z

    .line 64
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 65
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t;->e:Z

    .line 79
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 80
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->c()V

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t;->j:Z

    .line 91
    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    .line 92
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 84
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t;->j:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->b()V

    new-instance v1, Lcom/google/android/apps/gmm/map/u;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/u;-><init>(Lcom/google/android/apps/gmm/map/t;Landroid/view/View;)V

    .line 86
    return-void
.end method
