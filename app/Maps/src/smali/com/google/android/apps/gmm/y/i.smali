.class public Lcom/google/android/apps/gmm/y/i;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Lcom/google/d/a/a/ox;)Z
    .locals 4
    .param p0    # Lcom/google/d/a/a/ox;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 19
    if-eqz p0, :cond_2

    .line 20
    iget v0, p0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v3, 0x8

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 21
    iget v0, p0, Lcom/google/d/a/a/ox;->f:I

    invoke-static {v0}, Lcom/google/d/a/a/pc;->a(I)Lcom/google/d/a/a/pc;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/d/a/a/pc;->a:Lcom/google/d/a/a/pc;

    :cond_0
    sget-object v3, Lcom/google/d/a/a/pc;->a:Lcom/google/d/a/a/pc;

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 20
    goto :goto_0

    :cond_2
    move v0, v2

    .line 21
    goto :goto_1
.end method

.method protected static a(Lcom/google/d/a/a/pj;)Z
    .locals 5
    .param p0    # Lcom/google/d/a/a/pj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 39
    invoke-static {p0}, Lcom/google/android/apps/gmm/y/i;->c(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 40
    iget-object v0, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ox;

    if-eqz v0, :cond_1

    iget v0, v0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 41
    iget-object v0, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ox;

    if-eqz v0, :cond_3

    iget v0, v0, Lcom/google/d/a/a/ox;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v4, :cond_2

    move v0, v1

    :goto_2
    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    return v0

    :cond_0
    move v0, v2

    .line 40
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    .line 41
    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4
.end method

.method protected static b(Lcom/google/d/a/a/pj;)Z
    .locals 2
    .param p0    # Lcom/google/d/a/a/pj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 46
    invoke-static {p0}, Lcom/google/android/apps/gmm/y/i;->c(Lcom/google/d/a/a/pj;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/d/a/a/pj;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ox;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->a(Lcom/google/d/a/a/ox;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/d/a/a/pj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/d/a/a/ox;->d()Lcom/google/d/a/a/ox;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ox;

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/i;->a(Lcom/google/d/a/a/ox;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/google/d/a/a/pj;)Z
    .locals 2
    .param p0    # Lcom/google/d/a/a/pj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 32
    if-eqz p0, :cond_1

    .line 33
    iget-boolean v0, p0, Lcom/google/d/a/a/pj;->c:Z

    if-nez v0, :cond_1

    .line 34
    iget v0, p0, Lcom/google/d/a/a/pj;->b:I

    invoke-static {v0}, Lcom/google/d/a/a/pm;->a(I)Lcom/google/d/a/a/pm;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/d/a/a/pm;->a:Lcom/google/d/a/a/pm;

    :cond_0
    sget-object v1, Lcom/google/d/a/a/pm;->b:Lcom/google/d/a/a/pm;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
