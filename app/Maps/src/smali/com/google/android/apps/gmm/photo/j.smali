.class Lcom/google/android/apps/gmm/photo/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/photo/h;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->c:Lcom/google/android/apps/gmm/photo/s;

    invoke-static {}, Lcom/google/android/apps/gmm/photo/s;->a()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->e:Landroid/net/Uri;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "output"

    iget-object v3, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->e:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 114
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/List;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/photo/d;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v7, 0x0

    :goto_0
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->j:Lcom/google/b/f/t;

    aput-object v6, v5, v2

    sget-object v6, Lcom/google/b/f/t;->cG:Lcom/google/b/f/t;

    aput-object v6, v5, v3

    iput-object v5, v4, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v4

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    iget-object v0, v7, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    iget-object v4, v1, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->f:Lcom/google/android/apps/gmm/base/g/c;

    sget-object v5, Lcom/google/j/d/a/w;->f:Lcom/google/j/d/a/w;

    sget-object v6, Lcom/google/b/f/t;->j:Lcom/google/b/f/t;

    invoke-interface {v0, v4, v5, v6}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    if-nez p1, :cond_2

    sget v0, Lcom/google/android/apps/gmm/l;->ih:I

    sget v1, Lcom/google/android/apps/gmm/l;->ig:I

    invoke-static {v7, v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;II)V

    .line 135
    :cond_0
    :goto_1
    return-void

    .line 133
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v7

    goto :goto_0

    :cond_2
    if-eqz p3, :cond_3

    :goto_2
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->d:Z

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v7, p3, v0, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->a(Landroid/app/Activity;ZZI)Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/gmm/photo/m;

    iget-object v2, v1, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->f:Lcom/google/android/apps/gmm/base/g/c;

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/photo/m;-><init>(Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;Lcom/google/android/apps/gmm/base/g/c;ILjava/lang/String;Ljava/util/List;Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 7

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/b/f/cq;

    const/4 v5, 0x0

    sget-object v6, Lcom/google/b/f/t;->h:Lcom/google/b/f/t;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    sget-object v6, Lcom/google/b/f/t;->cG:Lcom/google/b/f/t;

    aput-object v6, v4, v5

    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->f:Lcom/google/android/apps/gmm/base/g/c;

    sget-object v3, Lcom/google/j/d/a/w;->f:Lcom/google/j/d/a/w;

    sget-object v4, Lcom/google/b/f/t;->h:Lcom/google/b/f/t;

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    if-eqz p1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    .line 128
    :cond_0
    :goto_1
    return-void

    .line 126
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "image/*"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 121
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/j;->a:Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;

    if-eqz p1, :cond_1

    const-string v0, "http://maps.google.com/intl/%s/help/maps/streetview/mobile/contribute/policies.html"

    :goto_0
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/photo/PhotoUploadConfirmFragment;->startActivity(Landroid/content/Intent;)V

    .line 142
    :cond_0
    return-void

    .line 140
    :cond_1
    const-string v0, "http://support.google.com/gmm/bin/answer.py?hl=%s&answer=1650741"

    goto :goto_0
.end method
