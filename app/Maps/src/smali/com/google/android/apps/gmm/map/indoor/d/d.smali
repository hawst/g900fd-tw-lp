.class public Lcom/google/android/apps/gmm/map/indoor/d/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/apps/gmm/map/b/a/j;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/indoor/d/d;->a:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/indoor/d/d;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 20
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/indoor/d/d;
    .locals 3

    .prologue
    const/16 v2, 0x1c

    .line 24
    if-nez p0, :cond_0

    .line 25
    const/4 v0, 0x0

    .line 31
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 29
    const/4 v1, 0x1

    .line 30
    invoke-virtual {p0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 29
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    .line 31
    new-instance v1, Lcom/google/android/apps/gmm/map/indoor/d/d;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/indoor/d/d;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/b/a/ad;)Lcom/google/android/apps/gmm/map/indoor/d/d;
    .locals 3

    .prologue
    .line 37
    if-nez p0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 43
    :goto_0
    return-object v0

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/google/maps/b/a/ad;->b()Ljava/lang/String;

    move-result-object v1

    .line 42
    invoke-virtual {p0}, Lcom/google/maps/b/a/ad;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v2

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/map/indoor/d/d;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/indoor/d/d;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;)V

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 56
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/indoor/d/d;

    if-nez v2, :cond_1

    .line 66
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 61
    goto :goto_0

    .line 64
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/indoor/d/d;

    .line 66
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/d;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/d;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_3
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/d;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/indoor/d/d;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_4
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v0

    goto :goto_1

    :cond_6
    move v2, v0

    goto :goto_2
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/d;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/indoor/d/d;->b:Lcom/google/android/apps/gmm/map/b/a/j;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
