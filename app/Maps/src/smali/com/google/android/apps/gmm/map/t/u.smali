.class public Lcom/google/android/apps/gmm/map/t/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/t/x;


# instance fields
.field private final a:F

.field private final b:F

.field private final c:F


# direct methods
.method public constructor <init>(FFF)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput p1, p0, Lcom/google/android/apps/gmm/map/t/u;->a:F

    .line 29
    iput p2, p0, Lcom/google/android/apps/gmm/map/t/u;->b:F

    .line 30
    iput p3, p0, Lcom/google/android/apps/gmm/map/t/u;->c:F

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 3

    .prologue
    .line 36
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 37
    :goto_0
    return v0

    .line 36
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/u;->a:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/u;->b:F

    neg-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/t/u;->c:F

    neg-float v2, v2

    .line 37
    invoke-virtual {p1, p2, v0, v1, v2}, Lcom/google/android/apps/gmm/map/t/ac;->a(Lcom/google/android/apps/gmm/map/b/a/y;FFF)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ad;Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 44
    if-nez p2, :cond_1

    .line 45
    :cond_0
    :goto_0
    return v0

    .line 44
    :cond_1
    iget v2, p0, Lcom/google/android/apps/gmm/map/t/u;->a:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/t/u;->b:F

    neg-float v3, v3

    iget v4, p0, Lcom/google/android/apps/gmm/map/t/u;->c:F

    neg-float v4, v4

    .line 45
    iget-object v5, p1, Lcom/google/android/apps/gmm/map/t/ad;->a:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/t/ad;->d:[F

    invoke-virtual {v5, p2, v6}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/t/ad;->c:[F

    aget v5, v5, v0

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/t/ad;->d:[F

    aget v6, v6, v0

    sub-float/2addr v5, v6

    add-float/2addr v3, v5

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/t/ad;->c:[F

    aget v5, v5, v1

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/t/ad;->d:[F

    aget v6, v6, v1

    sub-float/2addr v5, v6

    add-float/2addr v4, v5

    mul-float/2addr v3, v3

    mul-float/2addr v4, v4

    add-float/2addr v3, v4

    mul-float/2addr v2, v2

    cmpg-float v2, v3, v2

    if-gtz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
