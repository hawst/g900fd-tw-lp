.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[I

.field public b:I

.field public c:I

.field public d:I

.field public e:F

.field public f:I


# direct methods
.method public constructor <init>([I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-eqz p1, :cond_0

    array-length v1, p1

    if-eqz v1, :cond_0

    move v1, v2

    :goto_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v1, v0

    goto :goto_0

    .line 47
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    .line 50
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    .line 51
    array-length v1, p1

    :goto_1
    if-ge v0, v1, :cond_2

    aget v3, p1, v0

    .line 52
    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57
    :cond_2
    array-length v0, p1

    rem-int/lit8 v0, v0, 0x2

    if-ne v0, v2, :cond_3

    .line 58
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    shl-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    .line 60
    :cond_3
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 103
    if-ne p0, p1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 107
    goto :goto_0

    .line 110
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;

    .line 112
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 113
    goto :goto_0

    .line 115
    :cond_4
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->d:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_5
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :cond_6
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 122
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    if-nez v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([I)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    .line 134
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    .line 135
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    .line 136
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->d:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    .line 139
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->f:I

    return v0

    .line 133
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->a:[I

    invoke-static {v0}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->d:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/i;->e:F

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x5f

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "DashData{dashes="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", sum="

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", scale="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
