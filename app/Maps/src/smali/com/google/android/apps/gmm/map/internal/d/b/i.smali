.class Lcom/google/android/apps/gmm/map/internal/d/b/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/k;


# static fields
.field private static final a:Lcom/google/android/apps/gmm/map/internal/d/b/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 594
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/m;

    const/16 v1, 0x26

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/m;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/i;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 592
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 676
    const/4 v0, 0x7

    return v0
.end method

.method public final a([B)Lcom/google/android/apps/gmm/map/internal/d/b/n;
    .locals 10

    .prologue
    const-wide v8, 0xffffffffL

    const/16 v2, 0x22

    const/16 v6, 0x20

    .line 656
    const/4 v0, 0x0

    .line 657
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    .line 658
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 659
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;-><init>()V

    .line 660
    iput-object p0, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 661
    const/4 v1, 0x5

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x9

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->c:J

    .line 662
    const/16 v1, 0xd

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v2, v1

    const/16 v1, 0x11

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    int-to-long v4, v1

    and-long/2addr v4, v8

    shl-long/2addr v2, v6

    or-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->b:J

    .line 664
    const/16 v1, 0x15

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->e:I

    .line 666
    const/16 v1, 0x19

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->g:I

    .line 667
    const/16 v1, 0x1d

    invoke-static {p1, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->h:I

    .line 668
    const/16 v1, 0x21

    aget-byte v1, p1, v1

    iput-byte v1, v0, Lcom/google/android/apps/gmm/map/internal/d/b/o;->i:B

    .line 669
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/o;->a()Lcom/google/android/apps/gmm/map/internal/d/b/n;

    move-result-object v0

    .line 671
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/b/n;[B)V
    .locals 8

    .prologue
    const/16 v7, 0x22

    const/16 v6, 0x20

    .line 639
    const/4 v0, 0x0

    const/16 v1, 0x26

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 641
    const/4 v0, 0x4

    const/4 v1, 0x7

    aput-byte v1, p2, v0

    .line 642
    const/4 v0, 0x5

    .line 643
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->c:J

    .line 642
    shr-long v4, v2, v6

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x9

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 644
    const/16 v0, 0xd

    .line 645
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->b:J

    .line 644
    shr-long v4, v2, v6

    long-to-int v1, v4

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    const/16 v0, 0x11

    long-to-int v1, v2

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 646
    const/16 v0, 0x15

    .line 647
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->e:I

    .line 646
    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 648
    const/16 v0, 0x19

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->g:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 649
    const/16 v0, 0x1d

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->h:I

    invoke-static {p2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 650
    const/16 v0, 0x21

    iget-byte v1, p1, Lcom/google/android/apps/gmm/map/internal/d/b/n;->i:B

    aput-byte v1, p2, v0

    .line 652
    invoke-static {p2, v7}, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a([BI)I

    move-result v0

    .line 651
    invoke-static {p2, v7, v0}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BII)V

    .line 653
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 681
    const/16 v0, 0x26

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/internal/d/b/d;
    .locals 1

    .prologue
    .line 686
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/i;->a:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-object v0
.end method
