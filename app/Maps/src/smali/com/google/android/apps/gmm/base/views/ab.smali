.class Lcom/google/android/apps/gmm/base/views/ab;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field private h:I

.field private i:Z

.field private j:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(IIIIIZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/ab;->a:I

    .line 193
    iput p3, p0, Lcom/google/android/apps/gmm/base/views/ab;->b:I

    .line 194
    iput p2, p0, Lcom/google/android/apps/gmm/base/views/ab;->c:I

    .line 195
    iput p4, p0, Lcom/google/android/apps/gmm/base/views/ab;->d:I

    .line 197
    sub-int v0, p5, p1

    sub-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ab;->j:I

    .line 199
    iput-boolean p6, p0, Lcom/google/android/apps/gmm/base/views/ab;->i:Z

    .line 201
    iput v1, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    .line 202
    iput v1, p0, Lcom/google/android/apps/gmm/base/views/ab;->e:I

    .line 204
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ab;->f:I

    .line 205
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/ab;->e:I

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ab;->g:I

    .line 206
    return-void
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 7

    .prologue
    .line 209
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 210
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 212
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v1

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v3, v4

    .line 213
    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v2

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v4, v5

    .line 217
    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    add-int/2addr v5, v3

    iget v6, p0, Lcom/google/android/apps/gmm/base/views/ab;->j:I

    if-le v5, v6, :cond_0

    .line 220
    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    if-eqz v5, :cond_0

    .line 221
    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    .line 222
    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->g:I

    iput v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->e:I

    .line 226
    :cond_0
    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->f:I

    iget v6, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    add-int/2addr v6, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    iput v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->f:I

    .line 227
    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->g:I

    iget v6, p0, Lcom/google/android/apps/gmm/base/views/ab;->e:I

    add-int/2addr v4, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, p0, Lcom/google/android/apps/gmm/base/views/ab;->g:I

    .line 229
    if-eqz p2, :cond_1

    .line 230
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/base/views/ab;->i:Z

    if-nez v4, :cond_2

    .line 231
    iget v4, p0, Lcom/google/android/apps/gmm/base/views/ab;->a:I

    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    add-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v4, v5

    iput v4, p2, Landroid/graphics/Rect;->left:I

    .line 232
    iget v4, p2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v4

    iput v1, p2, Landroid/graphics/Rect;->right:I

    .line 237
    :goto_0
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/ab;->e:I

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/ab;->c:I

    add-int/2addr v1, v4

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v0, v1

    iput v0, p2, Landroid/graphics/Rect;->top:I

    .line 238
    iget v0, p2, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v2

    iput v0, p2, Landroid/graphics/Rect;->bottom:I

    .line 241
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    .line 242
    return-void

    .line 234
    :cond_2
    iget v4, p0, Lcom/google/android/apps/gmm/base/views/ab;->a:I

    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->j:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/apps/gmm/base/views/ab;->h:I

    sub-int/2addr v4, v5

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v4, v5

    iput v4, p2, Landroid/graphics/Rect;->right:I

    .line 235
    iget v4, p2, Landroid/graphics/Rect;->right:I

    sub-int v1, v4, v1

    iput v1, p2, Landroid/graphics/Rect;->left:I

    goto :goto_0
.end method
