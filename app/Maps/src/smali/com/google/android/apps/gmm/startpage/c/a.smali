.class public Lcom/google/android/apps/gmm/startpage/c/a;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/startpage/e/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/libraries/curvular/bk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/android/libraries/curvular/cq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v0}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/startpage/c/a;->a:Lcom/google/android/libraries/curvular/bk;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 4

    .prologue
    .line 50
    const/4 v0, 0x5

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const-class v2, Lcom/google/android/apps/gmm/startpage/c/a;

    .line 52
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->Y:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/c/a;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x2

    .line 55
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/e/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/c/a;->g:Lcom/google/android/libraries/curvular/c/a;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x3

    sget-object v2, Lcom/google/android/apps/gmm/startpage/c/a;->a:Lcom/google/android/libraries/curvular/bk;

    .line 56
    sget-object v3, Lcom/google/android/libraries/curvular/g;->Z:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    sget v2, Lcom/google/android/apps/gmm/d;->I:I

    .line 57
    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 50
    invoke-static {v1}, Lcom/google/android/apps/gmm/util/b/f;->a([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 2

    .prologue
    .line 26
    check-cast p2, Lcom/google/android/apps/gmm/startpage/e/f;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->c()Lcom/google/android/apps/gmm/startpage/e/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/e/a;

    const-class v1, Lcom/google/android/apps/gmm/startpage/c/r;

    invoke-virtual {p4, v1, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/cardui/g/b;->a()Lcom/google/android/apps/gmm/util/b/h;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/util/b/f;->a(Lcom/google/android/apps/gmm/util/b/h;Lcom/google/android/libraries/curvular/bc;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->b()Lcom/google/android/apps/gmm/cardui/g/b;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/cardui/d/b;->b(Lcom/google/android/apps/gmm/cardui/g/b;Lcom/google/android/libraries/curvular/bc;)V

    invoke-interface {p2}, Lcom/google/android/apps/gmm/startpage/e/f;->d()Lcom/google/android/apps/gmm/startpage/e/g;

    move-result-object v0

    invoke-static {v0, p4}, Lcom/google/android/apps/gmm/startpage/c/d;->a(Lcom/google/android/apps/gmm/base/l/a/k;Lcom/google/android/libraries/curvular/bc;)V

    return-void
.end method
