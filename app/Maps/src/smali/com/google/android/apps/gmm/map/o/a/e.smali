.class public Lcom/google/android/apps/gmm/map/o/a/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/a/a;


# instance fields
.field public final a:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/map/b/a/ab;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:[F

.field private final d:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final e:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final f:Lcom/google/android/apps/gmm/map/o/b/a;

.field private final g:Lcom/google/android/apps/gmm/map/o/b/a;

.field private final h:Lcom/google/android/apps/gmm/map/o/a/b;

.field private final i:Lcom/google/android/apps/gmm/map/o/a/c;

.field private j:Lcom/google/android/apps/gmm/map/o/a/h;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/o/a/c;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ab;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/o/a/c;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/e;->c:[F

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/e;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0, v1, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/e;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/e;->g:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/map/o/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/e;->h:Lcom/google/android/apps/gmm/map/o/a/b;

    .line 83
    const-string v0, "placedCallouts"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/o/a/c;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/a/e;->i:Lcom/google/android/apps/gmm/map/o/a/c;

    .line 84
    if-eqz p2, :cond_1

    if-eqz p3, :cond_1

    .line 85
    new-instance v0, Lcom/google/android/apps/gmm/map/o/a/h;

    invoke-direct {v0, p2, p3}, Lcom/google/android/apps/gmm/map/o/a/h;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/ab;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    .line 86
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/a/e;->a:Ljava/util/List;

    .line 87
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    .line 90
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/e;->a:Ljava/util/List;

    .line 91
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/f/o;ZLcom/google/android/apps/gmm/map/o/b/a;)I
    .locals 11

    .prologue
    .line 294
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/a/e;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 295
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/e;->c:[F

    move-object/from16 v0, p6

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v1

    if-nez v1, :cond_1

    .line 296
    const/16 v1, 0x3e8

    .line 373
    :cond_0
    :goto_0
    return v1

    .line 298
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/e;->c:[F

    const/4 v3, 0x0

    aget v1, v1, v3

    float-to-int v1, v1

    int-to-float v1, v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/e;->c:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    float-to-int v3, v3

    int-to-float v3, v3

    iput v1, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 304
    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 305
    const/16 v1, 0x3e8

    goto :goto_0

    .line 310
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/e;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 311
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/e;->h:Lcom/google/android/apps/gmm/map/o/a/b;

    invoke-static {v2, p2, p3, p4, v1}, Lcom/google/android/apps/gmm/map/o/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)V

    .line 315
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/e;->i:Lcom/google/android/apps/gmm/map/o/a/c;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/map/o/a/c;->a(Lcom/google/android/apps/gmm/map/o/b/b;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 316
    const/16 v1, 0x3e8

    goto :goto_0

    .line 319
    :cond_3
    iget v3, v1, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    iget v4, v1, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    iget v5, v1, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    move-object/from16 v0, p8

    invoke-virtual {v0, v3, v4, v5, v1}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 323
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/a/e;->f:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 324
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/e;->h:Lcom/google/android/apps/gmm/map/o/a/b;

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/o/a/b;->b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;)V

    .line 327
    const/4 v1, 0x0

    .line 330
    move-object/from16 v0, p5

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/o/b/a;->b(Lcom/google/android/apps/gmm/map/o/b/a;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 331
    const/4 v1, 0x1

    .line 334
    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/e;->a:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 339
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/o/a/h;->a()V

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/a/h;->d:Lcom/google/b/c/hz;

    invoke-interface {v3}, Lcom/google/b/c/hz;->d()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v1

    :cond_5
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 340
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 341
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/a/e;->c:[F

    move-object/from16 v0, p6

    invoke-virtual {v0, v1, v7}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 342
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/a/e;->c:[F

    const/4 v8, 0x0

    aget v7, v7, v8

    float-to-int v7, v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/o/a/e;->c:[F

    const/4 v9, 0x1

    aget v8, v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    iput v7, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v8, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 347
    if-nez p7, :cond_7

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/o/a/h;->e:Ljava/util/Set;

    invoke-interface {v7, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 348
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/a/e;->g:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 350
    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-int v7, v7

    add-int/lit8 v7, v7, -0x14

    int-to-float v7, v7

    .line 351
    iget v8, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-int v8, v8

    add-int/lit8 v8, v8, -0x14

    int-to-float v8, v8

    .line 352
    iget v9, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-int v9, v9

    add-int/lit8 v9, v9, 0x14

    int-to-float v9, v9

    .line 353
    iget v10, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-int v10, v10

    add-int/lit8 v10, v10, 0x14

    int-to-float v10, v10

    .line 349
    invoke-virtual {v1, v7, v8, v9, v10}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 356
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 357
    const/16 v1, 0x3e8

    goto/16 :goto_0

    .line 361
    :cond_6
    invoke-virtual {v6, v5}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 362
    const/16 v1, 0x3e8

    goto/16 :goto_0

    .line 364
    :cond_7
    invoke-virtual {v6, v5}, Lcom/google/android/apps/gmm/map/o/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 365
    add-int/lit8 v1, v3, 0x1

    .line 366
    const/16 v3, 0xa

    if-le v1, v3, :cond_9

    .line 368
    const/16 v1, 0x3e8

    goto/16 :goto_0

    :cond_8
    move v1, v3

    :cond_9
    move v3, v1

    .line 371
    goto :goto_1

    :cond_a
    move v1, v3

    .line 373
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;Lcom/google/android/apps/gmm/map/f/o;)Z
    .locals 29

    .prologue
    .line 116
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->k:Lcom/google/android/apps/gmm/map/f/e;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/e;->a()Landroid/graphics/Rect;

    move-result-object v2

    .line 117
    new-instance v7, Lcom/google/android/apps/gmm/map/o/b/a;

    iget v3, v2, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget v4, v2, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iget v5, v2, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    invoke-direct {v7, v3, v4, v5, v2}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    .line 120
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->h:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    .line 121
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->b:Lcom/google/android/apps/gmm/map/o/b/c;

    move-object/from16 v21, v0

    .line 122
    new-instance v19, Lcom/google/android/apps/gmm/map/o/b/a;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/e;->h:Lcom/google/android/apps/gmm/map/o/a/b;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/o/a/b;->a(Lcom/google/android/apps/gmm/map/f/o;)F

    move-result v5

    .line 124
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-ne v2, v3, :cond_0

    const/4 v9, 0x1

    .line 130
    :goto_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 131
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 132
    new-instance v10, Lcom/google/android/apps/gmm/map/o/b/a;

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct {v10, v2, v8, v11, v12}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    .line 134
    const/16 v8, 0x3e8

    .line 137
    if-eqz v3, :cond_1b

    move-object/from16 v2, p0

    move-object/from16 v8, p2

    .line 138
    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/o/a/e;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/f/o;ZLcom/google/android/apps/gmm/map/o/b/a;)I

    move-result v8

    .line 143
    const/16 v2, 0x3e7

    if-le v8, v2, :cond_1b

    .line 144
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {}, Lcom/google/android/apps/gmm/map/o/b/f;->values()[Lcom/google/android/apps/gmm/map/o/b/f;

    move-result-object v11

    array-length v11, v11

    rem-int/2addr v2, v11

    invoke-static {}, Lcom/google/android/apps/gmm/map/o/b/f;->values()[Lcom/google/android/apps/gmm/map/o/b/f;

    move-result-object v11

    aget-object v13, v11, v2

    move-object/from16 v20, v4

    move v4, v8

    move-object v8, v3

    .line 146
    :goto_1
    move-object/from16 v0, v20

    if-eq v13, v0, :cond_1a

    move-object/from16 v11, p0

    move-object v12, v3

    move v14, v5

    move-object v15, v6

    move-object/from16 v16, v7

    move-object/from16 v17, p2

    move/from16 v18, v9

    .line 147
    invoke-direct/range {v11 .. v19}, Lcom/google/android/apps/gmm/map/o/a/e;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/f/o;ZLcom/google/android/apps/gmm/map/o/b/a;)I

    move-result v2

    .line 157
    if-ge v2, v4, :cond_19

    .line 161
    move-object/from16 v0, v19

    iget v4, v0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    move-object/from16 v0, v19

    iget v8, v0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    move-object/from16 v0, v19

    iget v11, v0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    move-object/from16 v0, v19

    iget v12, v0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-virtual {v10, v4, v8, v11, v12}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 164
    if-eqz v2, :cond_1

    move-object v4, v3

    move-object v8, v13

    .line 165
    :goto_2
    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    invoke-static {}, Lcom/google/android/apps/gmm/map/o/b/f;->values()[Lcom/google/android/apps/gmm/map/o/b/f;

    move-result-object v12

    array-length v12, v12

    rem-int/2addr v11, v12

    invoke-static {}, Lcom/google/android/apps/gmm/map/o/b/f;->values()[Lcom/google/android/apps/gmm/map/o/b/f;

    move-result-object v12

    aget-object v13, v12, v11

    move-object/from16 v20, v8

    move-object v8, v4

    move v4, v2

    .line 170
    goto :goto_1

    .line 124
    :cond_0
    const/4 v9, 0x0

    goto :goto_0

    :cond_1
    move v8, v2

    move-object/from16 v18, v3

    move-object/from16 v20, v13

    .line 175
    :goto_3
    const/4 v4, 0x0

    .line 176
    const/16 v2, 0x3e7

    if-le v8, v2, :cond_18

    .line 177
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/e;->a:Ljava/util/List;

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eq v2, v11, :cond_5

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/o/a/h;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-nez v2, :cond_2

    invoke-virtual {v11, v3}, Lcom/google/android/apps/gmm/map/o/a/h;->a(I)V

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/o/a/h;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    :cond_2
    move-object v15, v2

    .line 178
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/e;->a:Ljava/util/List;

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-interface {v2, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v11

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eq v2, v3, :cond_11

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/map/o/a/e;->j:Lcom/google/android/apps/gmm/map/o/a/h;

    iget-object v2, v12, Lcom/google/android/apps/gmm/map/o/a/h;->a:Ljava/util/List;

    invoke-interface {v2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, v12, Lcom/google/android/apps/gmm/map/o/a/h;->b:[I

    aget v3, v3, v11

    const/4 v13, -0x1

    if-ne v3, v13, :cond_3

    invoke-virtual {v12, v11}, Lcom/google/android/apps/gmm/map/o/a/h;->a(I)V

    iget-object v3, v12, Lcom/google/android/apps/gmm/map/o/a/h;->b:[I

    aget v3, v3, v11

    :cond_3
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v14

    .line 183
    :goto_5
    new-instance v11, Lcom/google/android/apps/gmm/map/o/a/g;

    .line 184
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->g:Lcom/google/android/apps/gmm/map/o/b/f;

    move-object/from16 v16, v0

    .line 185
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->c:Lcom/google/android/apps/gmm/map/o/b/g;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/o/b/g;->n:Lcom/google/b/c/dn;

    move-object/from16 v17, v0

    move-object/from16 v12, p2

    move-object v13, v7

    invoke-direct/range {v11 .. v17}, Lcom/google/android/apps/gmm/map/o/a/g;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/b/a/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/o/b/f;Lcom/google/b/c/dn;)V

    move v2, v4

    move v3, v8

    move-object/from16 v4, v18

    move-object/from16 v8, v20

    .line 187
    :goto_6
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/o/a/g;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_13

    .line 188
    add-int/lit8 v12, v2, 0x1

    .line 190
    invoke-virtual {v11}, Lcom/google/android/apps/gmm/map/o/a/g;->a()Lcom/google/android/apps/gmm/map/o/a/f;

    move-result-object v13

    .line 192
    iget-object v0, v13, Lcom/google/android/apps/gmm/map/o/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v21, v0

    iget-object v0, v13, Lcom/google/android/apps/gmm/map/o/a/f;->b:Lcom/google/android/apps/gmm/map/o/b/f;

    move-object/from16 v22, v0

    move-object/from16 v20, p0

    move/from16 v23, v5

    move-object/from16 v24, v6

    move-object/from16 v25, v7

    move-object/from16 v26, p2

    move/from16 v27, v9

    move-object/from16 v28, v19

    invoke-direct/range {v20 .. v28}, Lcom/google/android/apps/gmm/map/o/a/e;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/b/a;Lcom/google/android/apps/gmm/map/f/o;ZLcom/google/android/apps/gmm/map/o/b/a;)I

    move-result v2

    .line 202
    if-ge v2, v3, :cond_17

    .line 204
    iget-object v3, v13, Lcom/google/android/apps/gmm/map/o/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 205
    iget-object v4, v13, Lcom/google/android/apps/gmm/map/o/a/f;->b:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 206
    move-object/from16 v0, v19

    iget v8, v0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    move-object/from16 v0, v19

    iget v13, v0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    move-object/from16 v0, v19

    iget v14, v0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    move-object/from16 v0, v19

    iget v15, v0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    invoke-virtual {v10, v8, v13, v14, v15}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 209
    if-eqz v2, :cond_14

    .line 210
    const/16 v8, 0xa

    if-gt v2, v8, :cond_4

    const/16 v8, 0x28

    if-gt v12, v8, :cond_14

    :cond_4
    :goto_7
    move-object v8, v4

    move-object v4, v3

    move v3, v2

    move v2, v12

    .line 216
    goto :goto_6

    .line 177
    :cond_5
    const/4 v2, 0x1

    new-array v3, v2, [Lcom/google/android/apps/gmm/map/b/a/ab;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    aput-object v11, v3, v2

    if-nez v3, :cond_6

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_6
    array-length v11, v3

    if-ltz v11, :cond_7

    const/4 v2, 0x1

    :goto_8
    if-nez v2, :cond_8

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_7
    const/4 v2, 0x0

    goto :goto_8

    :cond_8
    const-wide/16 v12, 0x5

    int-to-long v14, v11

    add-long/2addr v12, v14

    div-int/lit8 v2, v11, 0xa

    int-to-long v14, v2

    add-long/2addr v12, v14

    const-wide/32 v14, 0x7fffffff

    cmp-long v2, v12, v14

    if-lez v2, :cond_9

    const v2, 0x7fffffff

    :goto_9
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v15, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_9
    const-wide/32 v14, -0x80000000

    cmp-long v2, v12, v14

    if-gez v2, :cond_a

    const/high16 v2, -0x80000000

    goto :goto_9

    :cond_a
    long-to-int v2, v12

    goto :goto_9

    :cond_b
    const/4 v2, 0x1

    new-array v3, v2, [Lcom/google/android/apps/gmm/map/b/a/ab;

    const/4 v2, 0x0

    move-object/from16 v0, v21

    iget-object v11, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    aput-object v11, v3, v2

    if-nez v3, :cond_c

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_c
    array-length v11, v3

    if-ltz v11, :cond_d

    const/4 v2, 0x1

    :goto_a
    if-nez v2, :cond_e

    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    :cond_d
    const/4 v2, 0x0

    goto :goto_a

    :cond_e
    const-wide/16 v12, 0x5

    int-to-long v14, v11

    add-long/2addr v12, v14

    div-int/lit8 v2, v11, 0xa

    int-to-long v14, v2

    add-long/2addr v12, v14

    const-wide/32 v14, 0x7fffffff

    cmp-long v2, v12, v14

    if-lez v2, :cond_f

    const v2, 0x7fffffff

    :goto_b
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v15, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    goto/16 :goto_4

    :cond_f
    const-wide/32 v14, -0x80000000

    cmp-long v2, v12, v14

    if-gez v2, :cond_10

    const/high16 v2, -0x80000000

    goto :goto_b

    :cond_10
    long-to-int v2, v12

    goto :goto_b

    .line 178
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/a/e;->b:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v14

    goto/16 :goto_5

    :cond_12
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/b/c;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v14

    goto/16 :goto_5

    :cond_13
    move v2, v3

    move-object v3, v4

    move-object v4, v8

    .line 222
    :cond_14
    :goto_c
    const/16 v6, 0x3e8

    if-ge v2, v6, :cond_16

    const/4 v2, 0x1

    .line 224
    :goto_d
    if-eqz v2, :cond_15

    .line 225
    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/l;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/b/f;FLcom/google/android/apps/gmm/map/legacy/a/c/b/n;)V

    .line 226
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/a/e;->i:Lcom/google/android/apps/gmm/map/o/a/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/o/a/c;->a:Ljava/util/List;

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    :cond_15
    return v2

    .line 222
    :cond_16
    const/4 v2, 0x0

    goto :goto_d

    :cond_17
    move v2, v3

    move-object v3, v4

    move-object v4, v8

    goto/16 :goto_7

    :cond_18
    move v2, v8

    move-object/from16 v3, v18

    move-object/from16 v4, v20

    goto :goto_c

    :cond_19
    move v2, v4

    move-object v4, v8

    move-object/from16 v8, v20

    goto/16 :goto_2

    :cond_1a
    move-object/from16 v18, v8

    move v8, v4

    goto/16 :goto_3

    :cond_1b
    move-object/from16 v18, v3

    move-object/from16 v20, v4

    goto/16 :goto_3
.end method
