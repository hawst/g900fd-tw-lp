.class public Lcom/google/android/apps/gmm/v/ao;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/v/ap;

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:Ljava/lang/String;

.field public k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    const/4 v3, 0x0

    .line 194
    const/4 v1, 0x0

    .line 198
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 200
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v2

    .line 202
    :goto_0
    if-eqz v2, :cond_2

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v2, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 203
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    .line 209
    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 210
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 211
    invoke-interface {v0, v1, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    .line 213
    const/4 v4, 0x2

    new-array v9, v4, [[I

    const/4 v4, 0x0

    const/16 v5, 0xf

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    aput-object v5, v9, v4

    const/4 v4, 0x1

    const/4 v5, 0x7

    new-array v5, v5, [I

    fill-array-data v5, :array_1

    aput-object v5, v9, v4

    .line 233
    const/4 v4, 0x2

    new-array v10, v4, [[I

    const/4 v4, 0x0

    const/4 v5, 0x3

    new-array v5, v5, [I

    fill-array-data v5, :array_2

    aput-object v5, v10, v4

    const/4 v4, 0x1

    const/4 v5, 0x3

    new-array v5, v5, [I

    fill-array-data v5, :array_3

    aput-object v5, v10, v4

    .line 239
    const/4 v4, 0x0

    move v8, v4

    move-object v6, v2

    move-object v7, v3

    :goto_1
    const/4 v2, 0x2

    if-ge v8, v2, :cond_1

    .line 240
    const/4 v2, 0x1

    new-array v5, v2, [I

    .line 241
    aget-object v2, v9, v8

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 243
    const/4 v2, 0x0

    aget v2, v5, v2

    new-array v3, v2, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 244
    aget-object v2, v9, v8

    const/4 v4, 0x0

    aget v4, v5, v4

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 246
    const/4 v2, 0x0

    aget v2, v5, v2

    if-lez v2, :cond_a

    .line 248
    const/4 v2, 0x0

    aget-object v2, v3, v2

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    aget-object v5, v10, v8

    .line 249
    invoke-interface {v0, v1, v2, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v2

    .line 250
    invoke-static {v0}, Lcom/google/android/apps/gmm/v/ao;->a(Ljavax/microedition/khronos/egl/EGL10;)V

    .line 253
    const/4 v4, 0x5

    new-array v4, v4, [I

    fill-array-data v4, :array_4

    .line 254
    const/4 v5, 0x0

    aget-object v3, v3, v5

    invoke-interface {v0, v1, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    .line 255
    invoke-static {v0}, Lcom/google/android/apps/gmm/v/ao;->a(Ljavax/microedition/khronos/egl/EGL10;)V

    .line 257
    invoke-interface {v0, v1, v3, v3, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    .line 258
    invoke-static {v0}, Lcom/google/android/apps/gmm/v/ao;->a(Ljavax/microedition/khronos/egl/EGL10;)V

    .line 259
    if-nez v4, :cond_2

    .line 260
    :goto_2
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move-object v6, v2

    move-object v7, v3

    goto :goto_1

    .line 200
    :cond_0
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_1
    move-object v2, v6

    move-object v3, v7

    .line 268
    :cond_2
    const/16 v4, 0x1f00

    invoke-static {v4}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->h:Ljava/lang/String;

    .line 269
    const/16 v4, 0x1f02

    invoke-static {v4}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->i:Ljava/lang/String;

    .line 270
    const/16 v4, 0x1f01

    invoke-static {v4}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->j:Ljava/lang/String;

    .line 272
    const/4 v4, 0x1

    new-array v4, v4, [I

    .line 273
    const/16 v5, 0xd33

    const/4 v6, 0x0

    invoke-static {v5, v4, v6}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 274
    const/4 v5, 0x0

    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/gmm/v/ao;->b:I

    .line 276
    const v5, 0x8b4c

    const/4 v6, 0x0

    invoke-static {v5, v4, v6}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 277
    const/4 v5, 0x0

    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/gmm/v/ao;->e:I

    .line 279
    const v5, 0x8b4d

    const/4 v6, 0x0

    invoke-static {v5, v4, v6}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 280
    const/4 v5, 0x0

    aget v5, v4, v5

    iput v5, p0, Lcom/google/android/apps/gmm/v/ao;->c:I

    .line 282
    const/4 v5, 0x2

    new-array v5, v5, [I

    .line 283
    const v6, 0x846e

    const/4 v7, 0x0

    invoke-static {v6, v5, v7}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 284
    const/4 v6, 0x1

    aget v5, v5, v6

    iput v5, p0, Lcom/google/android/apps/gmm/v/ao;->d:I

    .line 286
    const/16 v5, 0xd57

    const/4 v6, 0x0

    invoke-static {v5, v4, v6}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 287
    const/4 v5, 0x0

    aget v4, v4, v5

    iput v4, p0, Lcom/google/android/apps/gmm/v/ao;->f:I

    .line 288
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p0, Lcom/google/android/apps/gmm/v/ao;->f:I

    add-int/lit8 v6, v6, -0x1

    int-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v4, v4

    iput v4, p0, Lcom/google/android/apps/gmm/v/ao;->g:I

    .line 290
    const/16 v4, 0x1f03

    invoke-static {v4}, Landroid/opengl/GLES20;->glGetString(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_4

    sget-object v4, Lcom/google/android/apps/gmm/v/ap;->a:Lcom/google/android/apps/gmm/v/ap;

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    .line 292
    :goto_3
    if-eqz v3, :cond_3

    .line 294
    invoke-interface {v0, v1, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 295
    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 298
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ao;->k:Ljava/util/List;

    .line 299
    const-string v0, "Vendor "

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ao;->h:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 303
    :goto_4
    const-string v0, "Version "

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/ao;->i:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 304
    :goto_5
    iget v0, p0, Lcom/google/android/apps/gmm/v/ao;->b:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "maxTextureSize "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 305
    iget v0, p0, Lcom/google/android/apps/gmm/v/ao;->c:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "maxCombinedTextureImageUnits "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 306
    iget v0, p0, Lcom/google/android/apps/gmm/v/ao;->e:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "maxVertexTextureImageUnits "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 307
    iget v0, p0, Lcom/google/android/apps/gmm/v/ao;->d:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x21

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "maxSupportedLineWidth "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 308
    iget v0, p0, Lcom/google/android/apps/gmm/v/ao;->f:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "stencilBits "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 309
    iget v0, p0, Lcom/google/android/apps/gmm/v/ao;->g:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "stencilValueMax "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/ao;->k:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x10

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "GL_EXTENSIONS = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    return-void

    .line 290
    :cond_4
    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->k:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->k:Ljava/util/List;

    const-string v5, "GL_OES_TEXTURE_NPOT"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    sget-object v4, Lcom/google/android/apps/gmm/v/ap;->c:Lcom/google/android/apps/gmm/v/ap;

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    goto/16 :goto_3

    :cond_5
    iget-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->k:Ljava/util/List;

    const-string v5, "GL_IMG_TEXTURE_NPOT"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    sget-object v4, Lcom/google/android/apps/gmm/v/ap;->b:Lcom/google/android/apps/gmm/v/ap;

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    goto/16 :goto_3

    :cond_6
    iget-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->k:Ljava/util/List;

    const-string v5, "GL_NV_TEXTURE_NPOT_2D_MIPMAP"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    sget-object v4, Lcom/google/android/apps/gmm/v/ap;->b:Lcom/google/android/apps/gmm/v/ap;

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    goto/16 :goto_3

    :cond_7
    sget-object v4, Lcom/google/android/apps/gmm/v/ap;->a:Lcom/google/android/apps/gmm/v/ap;

    iput-object v4, p0, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    goto/16 :goto_3

    .line 299
    :cond_8
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 303
    :cond_9
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_a
    move-object v2, v6

    move-object v3, v7

    goto/16 :goto_2

    .line 213
    :array_0
    .array-data 4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3026
        0x8
        0x3025
        0x8
        0x3040
        0x4
        0x3038
    .end array-data

    :array_1
    .array-data 4
        0x3026
        0x8
        0x3025
        0x8
        0x3032
        0x0
        0x3038
    .end array-data

    .line 233
    :array_2
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data

    :array_3
    .array-data 4
        0x3098
        0x1
        0x3038
    .end array-data

    .line 253
    :array_4
    .array-data 4
        0x3057
        0x2
        0x3056
        0x2
        0x3038
    .end array-data
.end method

.method private static a(Ljavax/microedition/khronos/egl/EGL10;)V
    .locals 3

    .prologue
    .line 322
    invoke-interface {p0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 323
    const/16 v1, 0x3000

    if-ne v0, v1, :cond_0

    .line 328
    :goto_0
    return-void

    .line 327
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "eglError "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
