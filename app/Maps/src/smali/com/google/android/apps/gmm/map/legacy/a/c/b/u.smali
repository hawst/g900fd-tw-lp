.class public final enum Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

.field public static final enum b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

.field public static final enum c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    .line 56
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->d:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(I)Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;
    .locals 2

    .prologue
    .line 62
    packed-switch p0, :pswitch_data_0

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown justification"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    .line 65
    :goto_0
    return-object v0

    .line 64
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    goto :goto_0

    .line 65
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->c:Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    goto :goto_0

    .line 62
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;
    .locals 1

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->d:[Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/legacy/a/c/b/u;

    return-object v0
.end method
