.class Lcom/google/android/apps/gmm/appwidget/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lcom/google/android/apps/gmm/appwidget/c;->a:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 186
    iget-object v6, p0, Lcom/google/android/apps/gmm/appwidget/c;->a:Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;

    iget-object v1, v6, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v1, v6, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget-object v1, Lcom/google/android/apps/gmm/appwidget/e;->a:[I

    iget-object v8, v6, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->d:Lcom/google/maps/g/a/hm;

    invoke-virtual {v8}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v8

    aget v1, v1, v8

    packed-switch v1, :pswitch_data_0

    sget v1, Lcom/google/android/apps/gmm/f;->cb:I

    :goto_0
    invoke-static {v4, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    iget-object v1, v6, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v4, "android.hardware.location.gps"

    invoke-virtual {v1, v4}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v5

    :goto_1
    if-eqz v1, :cond_2

    iget-object v1, v6, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v1, v4, :cond_1

    move v1, v5

    :goto_2
    if-eqz v1, :cond_2

    :goto_3
    iget-object v4, v6, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->d:Lcom/google/maps/g/a/hm;

    move-object v1, v0

    move-object v3, v0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/g/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/maps/g/a/hm;Z)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "entry"

    sget-object v2, Lcom/google/android/apps/gmm/l/ae;->a:Lcom/google/android/apps/gmm/l/ae;

    iget-object v2, v2, Lcom/google/android/apps/gmm/l/ae;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.shortcut.NAME"

    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.shortcut.ICON"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {v6, v1, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->finish()V

    .line 187
    return-void

    .line 186
    :pswitch_0
    sget v1, Lcom/google/android/apps/gmm/f;->cc:I

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/google/android/apps/gmm/f;->ca:I

    goto :goto_0

    :pswitch_2
    sget v1, Lcom/google/android/apps/gmm/f;->cd:I

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_2

    :cond_2
    move v5, v3

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
