.class public final enum Lcom/google/android/apps/gmm/navigation/a/c;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/navigation/a/c;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/navigation/a/c;

.field public static final enum b:Lcom/google/android/apps/gmm/navigation/a/c;

.field public static final enum c:Lcom/google/android/apps/gmm/navigation/a/c;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/navigation/a/c;


# instance fields
.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/c;

    const-string v1, "LOUDER"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/navigation/a/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/c;->a:Lcom/google/android/apps/gmm/navigation/a/c;

    .line 34
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/c;

    const-string v1, "NORMAL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/navigation/a/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/c;->b:Lcom/google/android/apps/gmm/navigation/a/c;

    .line 35
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/c;

    const-string v1, "SOFTER"

    invoke-direct {v0, v1, v5, v3}, Lcom/google/android/apps/gmm/navigation/a/c;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/c;->c:Lcom/google/android/apps/gmm/navigation/a/c;

    .line 32
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/navigation/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/c;->a:Lcom/google/android/apps/gmm/navigation/a/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/c;->b:Lcom/google/android/apps/gmm/navigation/a/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/c;->c:Lcom/google/android/apps/gmm/navigation/a/c;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/c;->e:[Lcom/google/android/apps/gmm/navigation/a/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput p3, p0, Lcom/google/android/apps/gmm/navigation/a/c;->d:I

    .line 41
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/navigation/a/c;
    .locals 5

    .prologue
    .line 47
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aO:Lcom/google/android/apps/gmm/shared/b/c;

    const-class v3, Lcom/google/android/apps/gmm/navigation/a/c;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/c;->b:Lcom/google/android/apps/gmm/navigation/a/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-static {v3, v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/c;

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/navigation/a/c;
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/gmm/navigation/a/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/navigation/a/c;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/c;->e:[Lcom/google/android/apps/gmm/navigation/a/c;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/navigation/a/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/a/c;

    return-object v0
.end method
