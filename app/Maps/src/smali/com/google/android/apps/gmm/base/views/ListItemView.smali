.class public Lcom/google/android/apps/gmm/base/views/ListItemView;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/views/ah;

.field private final b:Lcom/google/android/apps/gmm/base/views/ah;

.field private c:Lcom/google/android/apps/gmm/base/views/ag;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 105
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    iput v1, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->d:I

    .line 94
    iput v1, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->e:I

    .line 107
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ah;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/views/ah;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    .line 108
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ah;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/base/views/ah;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    .line 110
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 111
    sget-object v2, Lcom/google/android/apps/gmm/n;->w:[I

    invoke-virtual {v0, p2, v2, p3, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 112
    sget v0, Lcom/google/android/apps/gmm/n;->P:I

    sget v3, Lcom/google/android/apps/gmm/m;->h:I

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/ah;->a(I)V

    sget v0, Lcom/google/android/apps/gmm/n;->J:I

    sget v3, Lcom/google/android/apps/gmm/m;->B:I

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/ah;->a(I)V

    sget v0, Lcom/google/android/apps/gmm/n;->F:I

    sget v3, Lcom/google/android/apps/gmm/m;->B:I

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    sget v0, Lcom/google/android/apps/gmm/g;->dv:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/n;->A:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    sget v3, Lcom/google/android/apps/gmm/n;->z:I

    invoke-virtual {v2, v3, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->d:I

    iput v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->e:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->setDividerPadding(I)V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->a()V

    sget v0, Lcom/google/android/apps/gmm/n;->Q:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/ah;->a(Z)V

    sget v0, Lcom/google/android/apps/gmm/n;->K:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/ah;->a(Z)V

    sget v0, Lcom/google/android/apps/gmm/n;->G:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    sget v0, Lcom/google/android/apps/gmm/g;->dv:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    if-eqz v3, :cond_3

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    :cond_1
    :goto_0
    sget v0, Lcom/google/android/apps/gmm/n;->M:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    sget v0, Lcom/google/android/apps/gmm/n;->H:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    sget v0, Lcom/google/android/apps/gmm/n;->O:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/ah;->b(I)V

    sget v0, Lcom/google/android/apps/gmm/n;->N:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->a(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/ah;->c(I)V

    sget v0, Lcom/google/android/apps/gmm/n;->I:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->a(I)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/base/views/ah;->c(I)V

    sget v0, Lcom/google/android/apps/gmm/n;->E:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->setShowDividers(I)V

    sget v0, Lcom/google/android/apps/gmm/n;->D:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    sget v0, Lcom/google/android/apps/gmm/n;->C:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    sget v0, Lcom/google/android/apps/gmm/n;->B:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/apps/gmm/e;->G:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    sget v0, Lcom/google/android/apps/gmm/n;->y:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->setMinimumHeight(I)V

    sget v0, Lcom/google/android/apps/gmm/n;->x:I

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/ag;->a(I)Lcom/google/android/apps/gmm/base/views/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->c:Lcom/google/android/apps/gmm/base/views/ag;

    sget v0, Lcom/google/android/apps/gmm/n;->L:I

    const/4 v1, -0x1

    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    if-gez v0, :cond_6

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->bx:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v1, v0

    :goto_2
    sget v0, Lcom/google/android/apps/gmm/g;->dL:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_5

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMarginEnd(I)V

    .line 113
    :cond_2
    :goto_3
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 114
    return-void

    .line 112
    :cond_3
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const v3, 0x7fffffff

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    goto/16 :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {v0, v3, v4, v1, v5}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_3

    :cond_6
    move v1, v0

    goto :goto_2
.end method

.method private static a(I)I
    .locals 1

    .prologue
    .line 77
    packed-switch p0, :pswitch_data_0

    .line 80
    const v0, 0x800003

    :goto_0
    return v0

    .line 78
    :pswitch_0
    const v0, 0x800005

    goto :goto_0

    .line 79
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 77
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a()V
    .locals 5

    .prologue
    .line 366
    sget v0, Lcom/google/android/apps/gmm/g;->bp:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    .line 369
    invoke-virtual {v0}, Landroid/view/View;->getPaddingStart()I

    move-result v1

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->d:I

    invoke-virtual {v0}, Landroid/view/View;->getPaddingEnd()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->e:I

    .line 368
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPaddingRelative(IIII)V

    .line 371
    :cond_0
    return-void
.end method

.method private a(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 540
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    move v2, v1

    move-object v1, p2

    :goto_1
    if-eq v1, v0, :cond_3

    const-string v3, "baseView doesn\'t contain targetView."

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    goto :goto_1

    .line 545
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v2

    .line 547
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    .line 548
    div-int/lit8 v2, v1, 0x2

    sub-int/2addr v0, v2

    .line 549
    add-int/2addr v1, v0

    .line 550
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p1, v2, v0, v3, v1}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public onFinishInflate()V
    .locals 2

    .prologue
    .line 171
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 173
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    sget v0, Lcom/google/android/apps/gmm/g;->dT:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    .line 174
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    sget v0, Lcom/google/android/apps/gmm/g;->dy:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    .line 175
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->a()V

    .line 176
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 516
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 518
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->c:Lcom/google/android/apps/gmm/base/views/ag;

    if-eqz v0, :cond_0

    .line 519
    sget v0, Lcom/google/android/apps/gmm/g;->bq:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v3, 0x8

    if-eq v0, v3, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/af;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->c:Lcom/google/android/apps/gmm/base/views/ag;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/views/ag;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 521
    :cond_0
    :goto_0
    return-void

    .line 519
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->a:Lcom/google/android/apps/gmm/base/views/ah;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-gt v3, v4, :cond_3

    :cond_1
    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_4

    :cond_2
    :goto_2
    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->a(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_4
    sget v0, Lcom/google/android/apps/gmm/g;->dv:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ListItemView;->b:Lcom/google/android/apps/gmm/base/views/ah;

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-gt v3, v4, :cond_6

    :cond_5
    move-object v0, v1

    goto :goto_2

    :cond_6
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v2, v0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->a(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setPressed(Z)V
    .locals 1

    .prologue
    .line 186
    if-eqz p1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/ListItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setPressed(Z)V

    goto :goto_0
.end method
