.class public Lcom/google/android/apps/gmm/search/an;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/am;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/search/ao;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/search/ao;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 4

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->d()V

    .line 71
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->a()Lcom/google/android/apps/gmm/base/placelists/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a()I

    move-result v1

    .line 61
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x3

    const/16 v3, 0x15

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    if-eq v0, v1, :cond_1

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->d()V

    goto :goto_0

    .line 66
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->a()Lcom/google/android/apps/gmm/base/placelists/a/a;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 67
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/al;->h()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 66
    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->a(Lcom/google/android/apps/gmm/base/placelists/a/a;Z)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/search/ao;->a(Lcom/google/android/apps/gmm/search/al;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->d()V

    goto :goto_0

    .line 67
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->d()V

    .line 42
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Lcom/google/android/apps/gmm/search/al;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/an;->a:Lcom/google/android/apps/gmm/search/ao;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/ao;->d()V

    .line 47
    return-void
.end method
