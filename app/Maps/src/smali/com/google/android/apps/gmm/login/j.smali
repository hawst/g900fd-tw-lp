.class Lcom/google/android/apps/gmm/login/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/login/a;

.field private b:Lcom/google/android/apps/gmm/login/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/login/a/b;)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lcom/google/android/apps/gmm/login/j;->a:Lcom/google/android/apps/gmm/login/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370
    iput-object p2, p0, Lcom/google/android/apps/gmm/login/j;->b:Lcom/google/android/apps/gmm/login/a/b;

    .line 371
    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 376
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 377
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378
    const-string v2, "accountType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 379
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 380
    :cond_0
    sget-object v2, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2b

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Account name or type is null, name: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 394
    :goto_0
    return-void

    .line 382
    :cond_1
    new-instance v2, Landroid/accounts/Account;

    invoke-direct {v2, v1, v0}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/j;->a:Lcom/google/android/apps/gmm/login/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/login/j;->a:Lcom/google/android/apps/gmm/login/a;

    const-string v4, "oauth2:https://www.googleapis.com/auth/mobilemaps.firstparty"

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/j;->a:Lcom/google/android/apps/gmm/login/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/j;->b:Lcom/google/android/apps/gmm/login/a/b;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/login/a/b;Z)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 387
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    goto :goto_0

    .line 383
    :cond_2
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/p;

    iget-object v5, v3, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v5

    iget-object v3, v3, Lcom/google/android/apps/gmm/login/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v0, v5, v3, v2, v4}, Lcom/google/android/apps/gmm/shared/net/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Landroid/accounts/Account;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 389
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    goto :goto_0

    .line 391
    :catch_2
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    goto :goto_0
.end method
