.class public Lcom/google/android/apps/gmm/o/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/o/a/c;


# static fields
.field static final synthetic c:Z


# instance fields
.field final a:Lcom/google/android/apps/gmm/shared/b/a;

.field b:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/apps/gmm/o/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/google/android/apps/gmm/o/f;

.field private final e:Lcom/google/android/apps/gmm/map/t;

.field private f:[Lcom/google/android/apps/gmm/o/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/gmm/o/e;->c:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/o/f;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/map/t;)V
    .locals 2

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/o/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/o/e;->d:Lcom/google/android/apps/gmm/o/f;

    .line 79
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/shared/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/e;->a:Lcom/google/android/apps/gmm/shared/b/a;

    .line 80
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/map/t;

    iput-object p3, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    .line 82
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->au:Lcom/google/android/apps/gmm/shared/b/c;

    const-class v1, Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 85
    :cond_3
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/gmm/o/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/e;->f:[Lcom/google/android/apps/gmm/o/a/b;

    .line 86
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/o/a/a;Z)V
    .locals 4

    .prologue
    .line 234
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/e;->f:[Lcom/google/android/apps/gmm/o/a/b;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 235
    iget-object v3, v3, Lcom/google/android/apps/gmm/o/a/b;->a:Lcom/google/android/apps/gmm/o/a/a;

    if-ne v3, p1, :cond_0

    .line 244
    :goto_1
    return-void

    .line 234
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239
    :cond_1
    if-eqz p2, :cond_2

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 242
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->remove(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 97
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 98
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 99
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 100
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 101
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 102
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 103
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    goto :goto_0

    .line 104
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 202
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 204
    sget-boolean v0, Lcom/google/android/apps/gmm/o/e;->c:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->k()V

    .line 214
    :cond_1
    :goto_0
    return-void

    .line 209
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->i()Z

    move-result v0

    if-nez v0, :cond_3

    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->m()V

    goto :goto_0

    .line 211
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->i()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->l()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->f:[Lcom/google/android/apps/gmm/o/a/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 112
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/e;->f:[Lcom/google/android/apps/gmm/o/a/b;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 113
    iget-object v4, v3, Lcom/google/android/apps/gmm/o/a/b;->a:Lcom/google/android/apps/gmm/o/a/a;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/o/a/b;->b:Z

    invoke-direct {p0, v4, v3}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/o/e;->b()V

    .line 118
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->d(Z)Z

    .line 119
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->b(Z)Z

    .line 120
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->c(Z)Z

    .line 121
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Z)Z

    .line 122
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->e(Z)Z

    .line 123
    return-void
.end method

.method public final varargs a([Lcom/google/android/apps/gmm/o/a/b;)V
    .locals 1

    .prologue
    .line 127
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 128
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/e;->f:[Lcom/google/android/apps/gmm/o/a/b;

    .line 129
    invoke-direct {p0}, Lcom/google/android/apps/gmm/o/e;->b()V

    .line 130
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Z)Z

    .line 131
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->b(Z)Z

    .line 132
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->c(Z)Z

    .line 133
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->d(Z)Z

    .line 134
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->e(Z)Z

    .line 135
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/o/a/a;)Z
    .locals 5

    .prologue
    .line 218
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/e;->f:[Lcom/google/android/apps/gmm/o/a/b;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 219
    iget-object v4, v3, Lcom/google/android/apps/gmm/o/a/b;->a:Lcom/google/android/apps/gmm/o/a/a;

    if-ne v4, p1, :cond_0

    .line 220
    iget-boolean v0, v3, Lcom/google/android/apps/gmm/o/a/b;->b:Z

    .line 223
    :goto_1
    return v0

    .line 218
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1
.end method

.method public final a(Z)Z
    .locals 7

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    .line 140
    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 141
    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v1

    .line 142
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/o;->c(Z)V

    .line 143
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v2}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/o/e;->d:Lcom/google/android/apps/gmm/o/f;

    invoke-static {v0, v2}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v4

    invoke-static {v2, v0}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v0

    const-string v5, "set1"

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v5, "set2"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0, v4}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v5

    new-instance v6, Lcom/google/b/c/jq;

    invoke-direct {v6, v4, v5, v0}, Lcom/google/b/c/jq;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    invoke-interface {v3, v6, v2}, Lcom/google/android/apps/gmm/o/f;->a(Lcom/google/b/c/ju;Ljava/util/EnumSet;)V

    .line 144
    return v1
.end method

.method public final b(Z)Z
    .locals 7

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    .line 150
    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 151
    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v1

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/o;->d(Z)V

    .line 153
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v2}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/o/e;->d:Lcom/google/android/apps/gmm/o/f;

    invoke-static {v0, v2}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v4

    invoke-static {v2, v0}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v0

    const-string v5, "set1"

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v5, "set2"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0, v4}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v5

    new-instance v6, Lcom/google/b/c/jq;

    invoke-direct {v6, v4, v5, v0}, Lcom/google/b/c/jq;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    invoke-interface {v3, v6, v2}, Lcom/google/android/apps/gmm/o/f;->a(Lcom/google/b/c/ju;Ljava/util/EnumSet;)V

    .line 154
    return v1
.end method

.method public final c(Z)Z
    .locals 7

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v0

    .line 160
    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 161
    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v1

    .line 162
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/o;->e(Z)V

    .line 163
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v2}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/o/e;->d:Lcom/google/android/apps/gmm/o/f;

    invoke-static {v0, v2}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v4

    invoke-static {v2, v0}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v0

    const-string v5, "set1"

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v5, "set2"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0, v4}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v5

    new-instance v6, Lcom/google/b/c/jq;

    invoke-direct {v6, v4, v5, v0}, Lcom/google/b/c/jq;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    invoke-interface {v3, v6, v2}, Lcom/google/android/apps/gmm/o/f;->a(Lcom/google/b/c/ju;Ljava/util/EnumSet;)V

    .line 164
    return v1
.end method

.method public final d(Z)Z
    .locals 7

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v1

    .line 170
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 171
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    .line 172
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->h()Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 178
    :goto_0
    return v0

    .line 176
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/o/e;->c()V

    .line 177
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v2}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/o/e;->d:Lcom/google/android/apps/gmm/o/f;

    invoke-static {v1, v2}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v4

    invoke-static {v2, v1}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v1

    const-string v5, "set1"

    if-nez v4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v5, "set2"

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {v1, v4}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v5

    new-instance v6, Lcom/google/b/c/jq;

    invoke-direct {v6, v4, v5, v1}, Lcom/google/b/c/jq;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    invoke-interface {v3, v6, v2}, Lcom/google/android/apps/gmm/o/f;->a(Lcom/google/b/c/ju;Ljava/util/EnumSet;)V

    goto :goto_0
.end method

.method public final e(Z)Z
    .locals 7

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v1

    .line 184
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;Z)V

    .line 185
    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/o/e;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    .line 186
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/o;->i()Z

    move-result v2

    if-ne v2, v0, :cond_0

    .line 192
    :goto_0
    return v0

    .line 190
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/o/e;->c()V

    .line 191
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/e;->b:Ljava/util/EnumSet;

    invoke-static {v2}, Ljava/util/EnumSet;->copyOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/o/e;->d:Lcom/google/android/apps/gmm/o/f;

    invoke-static {v1, v2}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v4

    invoke-static {v2, v1}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v1

    const-string v5, "set1"

    if-nez v4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v5, "set2"

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {v1, v4}, Lcom/google/b/c/jp;->a(Ljava/util/Set;Ljava/util/Set;)Lcom/google/b/c/ju;

    move-result-object v5

    new-instance v6, Lcom/google/b/c/jq;

    invoke-direct {v6, v4, v5, v1}, Lcom/google/b/c/jq;-><init>(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    invoke-interface {v3, v6, v2}, Lcom/google/android/apps/gmm/o/f;->a(Lcom/google/b/c/ju;Ljava/util/EnumSet;)V

    goto :goto_0
.end method
