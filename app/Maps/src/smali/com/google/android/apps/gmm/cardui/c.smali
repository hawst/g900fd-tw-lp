.class public abstract Lcom/google/android/apps/gmm/cardui/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/o;
.implements Lcom/google/android/apps/gmm/util/b/a;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/activities/c;

.field public final b:Lcom/google/android/apps/gmm/z/a;

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/cardui/f;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/o/h/a/a;

.field e:Ljava/lang/String;

.field public f:Lcom/google/o/h/a/nt;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field g:Z

.field public final h:Lcom/google/android/apps/gmm/cardui/b/b;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/curvular/bk",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/android/apps/gmm/cardui/a/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final k:Lcom/google/android/apps/gmm/cardui/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:Ljava/lang/Object;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/cardui/b/a;)V
    .locals 2
    .param p3    # Lcom/google/android/apps/gmm/cardui/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/cardui/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/cardui/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/cardui/d;-><init>(Lcom/google/android/apps/gmm/cardui/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->l:Ljava/lang/Object;

    .line 132
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 133
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/cardui/b/b;

    iput-object p2, p0, Lcom/google/android/apps/gmm/cardui/c;->h:Lcom/google/android/apps/gmm/cardui/b/b;

    .line 134
    new-instance v1, Lcom/google/android/apps/gmm/z/a;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/z/a;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    .line 135
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    .line 136
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->i:Ljava/util/List;

    .line 137
    iput-object p3, p0, Lcom/google/android/apps/gmm/cardui/c;->j:Lcom/google/android/apps/gmm/cardui/a/d;

    .line 138
    iput-object p4, p0, Lcom/google/android/apps/gmm/cardui/c;->k:Lcom/google/android/apps/gmm/cardui/b/a;

    .line 139
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/cardui/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 228
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f;

    .line 229
    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/f;->a:Lcom/google/o/h/a/br;

    invoke-virtual {v2}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/cardui/f;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/gmm/cardui/f;->d:Lcom/google/r/b/a/tf;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/util/b/m;->a(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)Ljava/util/List;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v3}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    iget-object v4, p0, Lcom/google/android/apps/gmm/cardui/c;->i:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/android/apps/gmm/cardui/f;Ljava/util/List;)Lcom/google/android/apps/gmm/cardui/e/c;

    goto :goto_0

    .line 231
    :cond_0
    return-void
.end method

.method private b(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 392
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/c;->g:Z

    if-nez v0, :cond_0

    .line 393
    const/4 v0, 0x0

    .line 400
    :goto_0
    return v0

    .line 395
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    .line 396
    if-eqz p2, :cond_3

    iget-object v2, p2, Lcom/google/android/apps/gmm/util/b/b;->a:Lcom/google/o/h/a/br;

    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/f;

    iget-object v4, v0, Lcom/google/android/apps/gmm/cardui/f;->a:Lcom/google/o/h/a/br;

    if-ne v4, v2, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/f;->c:Ljava/lang/String;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->j:Lcom/google/android/apps/gmm/cardui/a/d;

    iget-object v5, p0, Lcom/google/android/apps/gmm/cardui/c;->k:Lcom/google/android/apps/gmm/cardui/b/a;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/a/a;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;Lcom/google/android/apps/gmm/cardui/c;Lcom/google/android/apps/gmm/cardui/b/a;)V

    .line 400
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 396
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/apps/gmm/cardui/f;Ljava/util/List;)Lcom/google/android/apps/gmm/cardui/e/c;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/cardui/f;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;)",
            "Lcom/google/android/apps/gmm/cardui/e/c;"
        }
    .end annotation
.end method

.method public final a(Lcom/google/o/h/a/br;Ljava/util/List;Ljava/lang/String;ZLcom/google/r/b/a/tf;)Lcom/google/android/apps/gmm/cardui/e/c;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/o/h/a/br;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/m;",
            ">;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/r/b/a/tf;",
            ")",
            "Lcom/google/android/apps/gmm/cardui/e/c;"
        }
    .end annotation

    .prologue
    .line 319
    new-instance v0, Lcom/google/android/apps/gmm/cardui/f;

    invoke-direct {v0, p1, p3, p4, p5}, Lcom/google/android/apps/gmm/cardui/f;-><init>(Lcom/google/o/h/a/br;Ljava/lang/String;ZLcom/google/r/b/a/tf;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v1}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/c;->i:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/android/apps/gmm/cardui/f;Ljava/util/List;)Lcom/google/android/apps/gmm/cardui/e/c;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a()V
.end method

.method protected abstract a(I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 197
    const-string v0, "arg_key_cardui_shown_cards"

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    if-eqz v0, :cond_0

    .line 199
    const-string v0, "arg_key_cardui_card_action"

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 202
    const-string v0, "arg_key_cardui_card_id"

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/cardui/c;)V
    .locals 1

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/c;->b()V

    .line 222
    iget-object v0, p1, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/cardui/c;->a(Ljava/util/List;)V

    .line 223
    iget-object v0, p1, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    .line 224
    iget-object v0, p1, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    .line 225
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)V
    .locals 0
    .param p1    # Lcom/google/o/h/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 369
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/cardui/c;->b(Lcom/google/o/h/a/a;Lcom/google/android/apps/gmm/util/b/b;)Z

    .line 384
    return-void
.end method

.method public final a(Lcom/google/o/h/a/br;)V
    .locals 2
    .param p1    # Lcom/google/o/h/a/br;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/apps/gmm/cardui/e;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/cardui/e;-><init>(Lcom/google/android/apps/gmm/cardui/c;Lcom/google/o/h/a/br;)V

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/b/c/eg;->b(Ljava/util/Iterator;Lcom/google/b/a/ar;)I

    move-result v0

    .line 298
    if-ltz v0, :cond_0

    .line 299
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 300
    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 301
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/cardui/c;->a(I)V

    .line 303
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    .locals 4

    .prologue
    .line 308
    new-instance v0, Lcom/google/android/apps/gmm/cardui/f;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/cardui/f;-><init>(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/f;->a:Lcom/google/o/h/a/br;

    invoke-virtual {v1}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/cardui/f;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/apps/gmm/cardui/f;->d:Lcom/google/r/b/a/tf;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/util/b/m;->a(Ljava/util/List;Ljava/lang/String;Lcom/google/r/b/a/tf;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/libraries/curvular/bk;

    invoke-direct {v2}, Lcom/google/android/libraries/curvular/bk;-><init>()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/cardui/c;->i:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/android/apps/gmm/cardui/f;Ljava/util/List;)Lcom/google/android/apps/gmm/cardui/e/c;

    .line 309
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 234
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    .line 235
    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 238
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/cardui/c;->b()V

    .line 209
    const-string v0, "arg_key_cardui_shown_cards"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 210
    const-string v0, "arg_key_cardui_shown_cards"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/cardui/c;->a(Ljava/util/List;)V

    .line 212
    :cond_0
    const-string v0, "arg_key_cardui_card_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    const-string v0, "arg_key_cardui_card_action"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->d:Lcom/google/o/h/a/a;

    .line 215
    :cond_1
    const-string v0, "arg_key_cardui_card_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 216
    const-string v0, "arg_key_cardui_card_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->e:Ljava/lang/String;

    .line 218
    :cond_2
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 249
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/c;->g:Z

    .line 253
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->l:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->k()Lcom/google/android/apps/gmm/cardui/b/c;

    move-result-object v0

    .line 256
    instance-of v1, v0, Lcom/google/android/apps/gmm/cardui/n;

    if-eqz v1, :cond_0

    .line 257
    check-cast v0, Lcom/google/android/apps/gmm/cardui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/cardui/n;->a(Lcom/google/android/apps/gmm/cardui/o;)V

    .line 259
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->k()Lcom/google/android/apps/gmm/cardui/b/c;

    move-result-object v0

    .line 266
    instance-of v1, v0, Lcom/google/android/apps/gmm/cardui/n;

    if-eqz v1, :cond_0

    .line 267
    check-cast v0, Lcom/google/android/apps/gmm/cardui/n;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/cardui/n;->b(Lcom/google/android/apps/gmm/cardui/o;)V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/cardui/c;->l:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/cardui/c;->g:Z

    .line 271
    return-void
.end method

.method public abstract e()V
.end method
