.class Lcom/google/android/apps/gmm/v/al;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:Lcom/google/android/apps/gmm/v/am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/v/am",
            "<TT;>;"
        }
    .end annotation
.end field

.field final synthetic b:Lcom/google/android/apps/gmm/v/ak;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/v/ak;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/al;->b:Lcom/google/android/apps/gmm/v/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/al;->a:Lcom/google/android/apps/gmm/v/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/al;->b:Lcom/google/android/apps/gmm/v/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ak;->a:Lcom/google/android/apps/gmm/v/am;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public next()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/v/al;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/al;->a:Lcom/google/android/apps/gmm/v/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/am;->b:Lcom/google/android/apps/gmm/v/am;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/al;->a:Lcom/google/android/apps/gmm/v/am;

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/al;->a:Lcom/google/android/apps/gmm/v/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/am;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
