.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field static a:F


# instance fields
.field b:Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

.field final c:Z

.field d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field e:Lcom/google/android/apps/gmm/v/ci;

.field private final f:Lcom/google/android/apps/gmm/v/ao;

.field private final g:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field private final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;",
            ">;"
        }
    .end annotation
.end field

.field private final i:I

.field private final j:I

.field private final k:F

.field private l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a:F

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;Ljava/util/List;Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/indoor/c/r;Lcom/google/android/apps/gmm/v/bp;Lcom/google/android/apps/gmm/map/t/as;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;",
            ">;",
            "Lcom/google/android/apps/gmm/map/t/k;",
            "Lcom/google/android/apps/gmm/v/ao;",
            "Lcom/google/android/apps/gmm/map/indoor/c/r;",
            "Lcom/google/android/apps/gmm/v/bp;",
            "Lcom/google/android/apps/gmm/map/t/as;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 630
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->g:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 631
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->f:Lcom/google/android/apps/gmm/v/ao;

    .line 633
    iget v2, p2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->a:I

    const/16 v3, 0x12

    const/4 v4, 0x1

    .line 637
    iget v5, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 634
    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 639
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->h:Ljava/util/List;

    .line 640
    const/4 v5, 0x0

    .line 642
    const/4 v4, 0x0

    .line 644
    const/high16 v3, 0x3f800000    # 1.0f

    .line 645
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->h:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b()I

    move-result v6

    .line 646
    const/4 v2, 0x1

    if-le v6, v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->c:Z

    .line 647
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->c:Z

    if-eqz v2, :cond_1

    .line 650
    div-int/lit8 v2, v6, 0x10

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 651
    shl-int/lit8 v4, v2, 0x1

    .line 652
    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->h:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a()F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 653
    add-int/lit8 v4, v4, 0x0

    move v2, v5

    .line 661
    :goto_1
    const/4 v5, 0x1

    shl-int/2addr v5, v2

    if-ge v5, v4, :cond_2

    .line 662
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 646
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 655
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    .line 656
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b()I

    move-result v7

    .line 657
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a()F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 658
    add-int v3, v4, v7

    move v4, v3

    move v3, v2

    .line 659
    goto :goto_2

    .line 664
    :cond_2
    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->i:I

    .line 666
    sget v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-float v2, v2

    .line 667
    const v3, 0x3faaaaab

    mul-float/2addr v3, v2

    float-to-int v3, v3

    const/16 v4, 0x8

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->j:I

    .line 669
    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->j:I

    int-to-float v3, v3

    const v4, 0x3faaaaab

    mul-float/2addr v4, v2

    div-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->k:F

    .line 672
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v3, v4, v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    const/high16 v3, 0x43800000    # 256.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float v4, v2, v3

    .line 673
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a()Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;

    move-result-object v2

    .line 674
    const/4 v3, 0x0

    move v11, v3

    :goto_3
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v11, v3, :cond_5

    .line 675
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v5, v3

    check-cast v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    iget-object v3, v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v6, v6

    div-int/lit8 v12, v6, 0x3

    const/4 v6, 0x2

    if-lt v12, v6, :cond_3

    iget-object v6, v7, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-boolean v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->c:Z

    if-nez v8, :cond_4

    const/4 v5, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v10, 0x0

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;FZLcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;)V

    const/4 v3, 0x1

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->i:I

    const/4 v6, 0x1

    new-array v6, v6, [I

    const/4 v7, 0x0

    aput v11, v6, v7

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-static {v12, v3, v5, v6, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(IZI[ILcom/google/android/apps/gmm/map/legacy/a/c/a/b;)V

    :goto_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    .line 674
    :cond_3
    add-int/lit8 v3, v11, 0x1

    move v11, v3

    goto :goto_3

    .line 675
    :cond_4
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b()I

    move-result v5

    const/high16 v8, 0x45000000    # 2048.0f

    int-to-float v5, v5

    div-float v5, v8, v5

    iget-object v8, v7, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v7, v7, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int v7, v8, v7

    int-to-float v7, v7

    div-float v7, v5, v7

    const/4 v5, 0x1

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual/range {v2 .. v10}, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->a(Lcom/google/android/apps/gmm/map/b/a/ab;FZLcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/legacy/a/c/a/c;Lcom/google/android/apps/gmm/map/legacy/a/c/a/a;Lcom/google/android/apps/gmm/map/legacy/a/c/a/b;)V

    goto :goto_4

    .line 677
    :cond_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->g:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v2

    if-lez v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    const/4 v4, 0x4

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->j:I

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->i:I

    shl-int/2addr v4, v5

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->h:Ljava/util/List;

    iget v5, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->k:F

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->c:Z

    invoke-static {v2, v4, v5, v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Ljava/util/List;Landroid/graphics/Bitmap;FZ)V

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->g:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v0, p4

    if-eq v0, v2, :cond_b

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->e:Lcom/google/android/apps/gmm/map/t/l;

    move-object/from16 v0, p4

    if-eq v0, v2, :cond_b

    const/4 v2, 0x1

    :goto_5
    new-instance v5, Lcom/google/android/apps/gmm/map/t/ar;

    move-object/from16 v0, p4

    move-object/from16 v1, p8

    invoke-direct {v5, v0, v3, v1, v2}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v6, v6, Lcom/google/android/apps/gmm/v/av;->b:I

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x13

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Line "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "   "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    new-instance v2, Lcom/google/android/apps/gmm/v/ar;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->f:Lcom/google/android/apps/gmm/v/ao;

    const/4 v7, 0x1

    invoke-direct {v2, v4, v6, v7}, Lcom/google/android/apps/gmm/v/ar;-><init>(Landroid/graphics/Bitmap;Lcom/google/android/apps/gmm/v/ao;Z)V

    new-instance v4, Lcom/google/android/apps/gmm/v/ci;

    const/4 v6, 0x0

    invoke-direct {v4, v2, v6}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->e:Lcom/google/android/apps/gmm/v/ci;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->e:Lcom/google/android/apps/gmm/v/ci;

    const v4, 0x812f

    const/16 v6, 0x2901

    iget-boolean v7, v2, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v7, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_6
    iput v4, v2, Lcom/google/android/apps/gmm/v/ci;->k:I

    iput v6, v2, Lcom/google/android/apps/gmm/v/ci;->l:I

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/v/ci;->m:Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->e:Lcom/google/android/apps/gmm/v/ci;

    const/16 v4, 0x2601

    const/16 v6, 0x2601

    iget-boolean v7, v2, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v7, :cond_7

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_7
    iput v4, v2, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v6, v2, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/v/ci;->j:Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->e:Lcom/google/android/apps/gmm/v/ci;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    invoke-virtual {v5, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    const/4 v2, 0x0

    iget-boolean v3, v5, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_8

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_8
    int-to-byte v2, v2

    iput-byte v2, v5, Lcom/google/android/apps/gmm/v/aa;->w:B

    if-nez p6, :cond_c

    move-object/from16 v0, p11

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/4 v3, 0x1

    const/16 v4, 0x303

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    :cond_9
    :goto_6
    move-object/from16 v0, p7

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p9

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a()V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->l:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 679
    return-void

    .line 677
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_c
    move-object/from16 v0, p11

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v3, 0x302

    const/16 v4, 0x303

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/indoor/c/r;->a(Lcom/google/android/apps/gmm/map/t/ar;)V

    move-object/from16 v2, p7

    check-cast v2, Lcom/google/android/apps/gmm/v/cd;

    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->e:Ljava/util/Collection;

    invoke-interface {v3, v2}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p6

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    if-eqz v2, :cond_9

    move-object/from16 v0, p6

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/indoor/c/r;->b:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    move-object/from16 v0, p6

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_6

    :cond_d
    move v2, v5

    goto/16 :goto_1
.end method

.method public static a(F)V
    .locals 0

    .prologue
    .line 936
    sput p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a:F

    .line 937
    return-void
.end method

.method private static a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;",
            ">;",
            "Landroid/graphics/Canvas;",
            "Landroid/graphics/Paint;",
            "FFFI)V"
        }
    .end annotation

    .prologue
    .line 732
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    move/from16 v0, p6

    int-to-float v2, v0

    div-float/2addr v1, v2

    const/high16 v2, 0x41800000    # 16.0f

    mul-float v12, v1, v2

    .line 734
    const/4 v1, 0x0

    move v7, v1

    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v7, v1, :cond_7

    .line 735
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;

    .line 736
    iget v2, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->a:F

    iget v3, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->c:F

    iget v4, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->e:I

    iget-object v13, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aa;->f:[I

    mul-float v1, v2, p5

    add-float v14, p3, v1

    mul-float v1, v3, p5

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float v15, v1, v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    if-nez v13, :cond_2

    const/4 v1, 0x1

    move/from16 v0, p6

    if-ne v0, v1, :cond_1

    sub-float v2, v14, v15

    const/high16 v1, 0x3f000000    # 0.5f

    add-float v3, p4, v1

    add-float v4, v14, v15

    const/high16 v1, 0x3f000000    # 0.5f

    add-float v5, p4, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 734
    :cond_0
    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_0

    .line 736
    :cond_1
    sub-float v2, v14, v15

    const/4 v3, 0x0

    add-float v4, v14, v15

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v5, v1

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    const/4 v2, 0x1

    move/from16 v4, p4

    :goto_2
    move/from16 v0, p6

    if-ge v3, v0, :cond_0

    const/4 v1, 0x0

    move v8, v1

    move v1, v2

    move v2, v3

    move v3, v4

    :goto_3
    array-length v4, v13

    rem-int/lit8 v4, v4, 0x2

    if-gt v8, v4, :cond_6

    array-length v0, v13

    move/from16 v16, v0

    const/4 v4, 0x0

    move v11, v4

    move v9, v1

    move v10, v2

    :goto_4
    move/from16 v0, v16

    if-ge v11, v0, :cond_5

    aget v17, v13, v11

    move/from16 v0, v17

    int-to-float v1, v0

    const/high16 v2, 0x41800000    # 16.0f

    div-float/2addr v1, v2

    mul-float/2addr v1, v12

    add-float v5, v3, v1

    if-eqz v9, :cond_3

    sub-float v2, v14, v15

    add-float v4, v14, v15

    move-object/from16 v1, p1

    move-object/from16 v6, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_3
    if-nez v9, :cond_4

    const/4 v1, 0x1

    :goto_5
    add-int v10, v10, v17

    add-int/lit8 v2, v11, 0x1

    move v11, v2

    move v9, v1

    move v3, v5

    goto :goto_4

    :cond_4
    const/4 v1, 0x0

    goto :goto_5

    :cond_5
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move v2, v10

    move v1, v9

    goto :goto_3

    :cond_6
    move v4, v3

    move v3, v2

    move v2, v1

    goto :goto_2

    .line 749
    :cond_7
    return-void
.end method

.method private static a(Ljava/util/List;Landroid/graphics/Bitmap;FZ)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;",
            ">;",
            "Landroid/graphics/Bitmap;",
            "FZ)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 708
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 709
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 710
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 711
    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 712
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    .line 716
    if-eqz p3, :cond_0

    move v8, v0

    .line 717
    :goto_0
    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v8, :cond_1

    .line 718
    invoke-interface {p0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    .line 719
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b()I

    move-result v6

    .line 720
    int-to-float v4, v9

    .line 721
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    move v5, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    .line 723
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    move v5, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->a(Ljava/util/ArrayList;Landroid/graphics/Canvas;Landroid/graphics/Paint;FFFI)V

    .line 717
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 716
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    move v8, v0

    goto :goto_0

    .line 726
    :cond_1
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 820
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, -0x1

    .line 821
    if-gtz v2, :cond_1

    move v0, v1

    .line 831
    :cond_0
    :goto_0
    return v0

    .line 825
    :cond_1
    mul-int/lit8 v3, v2, 0x5

    .line 826
    iget v4, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->a:I

    if-lez v4, :cond_2

    iget v4, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->a:I

    add-int/2addr v4, v3

    const/16 v5, 0x4000

    if-gt v4, v5, :cond_0

    .line 829
    :cond_2
    iget v4, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->a:I

    add-int/2addr v3, v4

    iput v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->a:I

    .line 830
    iget v3, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->b:I

    mul-int/lit8 v2, v2, 0x3

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/ab;->f()Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x3

    :cond_3
    add-int/2addr v0, v2

    add-int/2addr v0, v3

    iput v0, p1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/y;->b:I

    move v0, v1

    .line 831
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 942
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->d:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    .line 943
    return v0
.end method

.method public final b()I
    .locals 5

    .prologue
    .line 951
    const/16 v0, 0x260

    .line 952
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/w;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;

    .line 953
    const/16 v2, 0xd0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    if-eqz v4, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v2, v2

    shl-int/lit8 v2, v2, 0x2

    add-int/lit16 v2, v2, 0xa0

    add-int/lit16 v2, v2, 0xd0

    :cond_0
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/z;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/2addr v0, v4

    mul-int/lit8 v0, v0, 0x18

    add-int/2addr v0, v2

    add-int/2addr v0, v1

    move v1, v0

    .line 954
    goto :goto_0

    .line 955
    :cond_1
    return v1
.end method
