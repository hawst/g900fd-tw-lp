.class public Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field a:Lcom/google/android/libraries/curvular/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/place/station/ai;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/place/station/b/c;)Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;
    .locals 3

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;-><init>()V

    .line 31
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v2, "viewmodel"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 33
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->setArguments(Landroid/os/Bundle;)V

    .line 34
    return-object v0
.end method


# virtual methods
.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/b/f/t;->fk:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    if-nez p1, :cond_0

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    .line 45
    :cond_0
    const-string v0, "viewmodel"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/station/ai;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->b:Lcom/google/android/apps/gmm/place/station/ai;

    .line 47
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 55
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/place/station/a/b;

    .line 56
    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 57
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->a:Lcom/google/android/libraries/curvular/ag;

    .line 58
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->a:Lcom/google/android/libraries/curvular/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->b:Lcom/google/android/apps/gmm/place/station/ai;

    invoke-interface {v1, v2}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 59
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    return-object v0
.end method

.method public onResume()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 69
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 71
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 73
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 74
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 75
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->b:Lcom/google/android/apps/gmm/place/station/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/station/ai;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->b:Lcom/google/android/apps/gmm/place/station/ai;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->a:Lcom/google/android/libraries/curvular/ag;

    new-instance v3, Lcom/google/android/apps/gmm/place/cl;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/place/cl;-><init>()V

    sget-object v4, Lcom/google/android/apps/gmm/place/cm;->c:Lcom/google/android/apps/gmm/place/cm;

    iput-object v4, v3, Lcom/google/android/apps/gmm/place/cl;->b:Lcom/google/android/apps/gmm/place/cm;

    iget-object v4, v2, Lcom/google/android/apps/gmm/place/station/ai;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/j;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v3, Lcom/google/android/apps/gmm/place/cl;->a:Ljava/util/List;

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/place/cl;->a()Lcom/google/android/apps/gmm/place/ck;

    move-result-object v4

    new-instance v3, Lcom/google/android/apps/gmm/place/station/ak;

    invoke-direct {v3, v2, v0, v1, v2}, Lcom/google/android/apps/gmm/place/station/ak;-><init>(Lcom/google/android/apps/gmm/place/station/ai;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/libraries/curvular/ag;Lcom/google/android/apps/gmm/place/station/ai;)V

    iget-object v1, v2, Lcom/google/android/apps/gmm/place/station/ai;->d:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/station/ai;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    sget-object v6, Lcom/google/b/f/t;->fj:Lcom/google/b/f/t;

    new-instance v7, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v7, v6}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v6}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v6

    check-cast v6, Lcom/google/maps/g/hy;

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/ci;->a(Lcom/google/android/apps/gmm/base/activities/o;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/b/c;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;Lcom/google/maps/g/hy;)Lcom/google/android/apps/gmm/place/ci;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 80
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    const-string v0, "viewmodel"

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/QuTimeTableFragment;->b:Lcom/google/android/apps/gmm/place/station/ai;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 65
    return-void
.end method
