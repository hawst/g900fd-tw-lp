.class public Lcom/google/android/apps/gmm/hotels/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/hotels/c/a;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/g/c;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

.field private final d:Lcom/google/android/apps/gmm/hotels/a/e;

.field private final e:Lcom/google/android/apps/gmm/z/b/l;

.field private final f:Lcom/google/android/apps/gmm/z/b/l;

.field private g:Lcom/google/android/apps/gmm/hotels/a/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;Lcom/google/android/apps/gmm/hotels/a/e;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/apps/gmm/hotels/c/b;->b:Landroid/content/Context;

    .line 48
    iput-object p2, p0, Lcom/google/android/apps/gmm/hotels/c/b;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 49
    iput-object p3, p0, Lcom/google/android/apps/gmm/hotels/c/b;->c:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    .line 50
    iput-object p4, p0, Lcom/google/android/apps/gmm/hotels/c/b;->d:Lcom/google/android/apps/gmm/hotels/a/e;

    .line 52
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->Y()Lcom/google/android/apps/gmm/hotels/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->g:Lcom/google/android/apps/gmm/hotels/a/c;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->a:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->a:Lcom/google/android/apps/gmm/base/g/c;

    if-nez v0, :cond_1

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/gmm/hotels/c/b;->f:Lcom/google/android/apps/gmm/z/b/l;

    .line 55
    return-void

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v2, v5, [Lcom/google/b/f/cq;

    sget-object v3, Lcom/google/b/f/t;->cJ:Lcom/google/b/f/t;

    aput-object v3, v2, v4

    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v1, v5, [Lcom/google/b/f/cq;

    sget-object v2, Lcom/google/b/f/t;->cK:Lcom/google/b/f/t;

    aput-object v2, v1, v4

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->d:Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c/b;->b:Landroid/content/Context;

    const/4 v2, 0x1

    const/16 v3, 0x12

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Landroid/content/Context;ZILcom/google/android/apps/gmm/hotels/a/i;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->c:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c/b;->d:Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/hotels/a/e;->a:Lcom/google/android/apps/gmm/hotels/a/i;

    sget-object v2, Lcom/google/android/apps/gmm/hotels/a/f;->a:Lcom/google/android/apps/gmm/hotels/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->a(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/f;)Landroid/app/DialogFragment;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    .line 65
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->e:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->d:Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c/b;->b:Landroid/content/Context;

    const/4 v2, 0x1

    const/16 v3, 0x12

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/hotels/a/i;->a(Landroid/content/Context;ZILcom/google/android/apps/gmm/hotels/a/i;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->c:Lcom/google/android/apps/gmm/hotels/HotelBookingPageFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/hotels/c/b;->d:Lcom/google/android/apps/gmm/hotels/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/hotels/a/e;->b:Lcom/google/android/apps/gmm/hotels/a/i;

    sget-object v2, Lcom/google/android/apps/gmm/hotels/a/f;->b:Lcom/google/android/apps/gmm/hotels/a/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->a(Lcom/google/android/apps/gmm/hotels/a/i;Lcom/google/android/apps/gmm/hotels/a/f;)Landroid/app/DialogFragment;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/hotels/HotelDatePickerDialogFragment;->a:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->f:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/hotels/c/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->g:Lcom/google/android/apps/gmm/hotels/a/c;

    if-nez v0, :cond_0

    .line 110
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    .line 113
    :cond_0
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->g:Lcom/google/android/apps/gmm/hotels/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/4 v2, 0x6

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v2

    .line 115
    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 116
    new-instance v5, Lcom/google/android/apps/gmm/hotels/c/c;

    iget-object v6, p0, Lcom/google/android/apps/gmm/hotels/c/b;->b:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/apps/gmm/hotels/c/b;->g:Lcom/google/android/apps/gmm/hotels/a/c;

    .line 119
    iget-object v7, v7, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/16 v8, 0xb

    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, p0, v6, v4, v7}, Lcom/google/android/apps/gmm/hotels/c/c;-><init>(Lcom/google/android/apps/gmm/hotels/c/b;Landroid/content/Context;Lcom/google/e/a/a/a/b;Ljava/lang/String;)V

    .line 116
    invoke-virtual {v1, v5}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 121
    :cond_1
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->g:Lcom/google/android/apps/gmm/hotels/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/hotels/c/b;->g:Lcom/google/android/apps/gmm/hotels/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/hotels/a/c;->a:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
