.class public Lcom/google/android/apps/gmm/base/i/f;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/map/h/f;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->e:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 181
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p0, v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;)Lcom/google/r/b/a/ado;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v2, 0x40000000    # 2.0f

    const/4 v8, 0x1

    .line 50
    invoke-static {}, Lcom/google/r/b/a/ado;->newBuilder()Lcom/google/r/b/a/adq;

    move-result-object v4

    .line 51
    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->a(Landroid/content/res/Configuration;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v5, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v8, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    int-to-float v3, v0

    if-nez v5, :cond_0

    :cond_0
    mul-float/2addr v3, v2

    float-to-int v3, v3

    invoke-static {}, Lcom/google/maps/a/m;->newBuilder()Lcom/google/maps/a/o;

    move-result-object v6

    iget v7, v6, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v6, Lcom/google/maps/a/o;->a:I

    iput v3, v6, Lcom/google/maps/a/o;->c:I

    iget v3, v6, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v6, Lcom/google/maps/a/o;->a:I

    iput v0, v6, Lcom/google/maps/a/o;->b:I

    invoke-virtual {v6}, Lcom/google/maps/a/o;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/m;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v4}, Lcom/google/r/b/a/adq;->c()V

    iget-object v3, v4, Lcom/google/r/b/a/adq;->b:Ljava/util/List;

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    if-eqz v5, :cond_2

    sget v0, Lcom/google/android/apps/gmm/e;->bv:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    move v3, v0

    :goto_0
    int-to-float v6, v3

    if-nez v5, :cond_3

    move v0, v1

    :goto_1
    mul-float/2addr v0, v6

    float-to-int v0, v0

    invoke-static {}, Lcom/google/maps/a/m;->newBuilder()Lcom/google/maps/a/o;

    move-result-object v1

    iget v2, v1, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/maps/a/o;->a:I

    iput v0, v1, Lcom/google/maps/a/o;->c:I

    iget v0, v1, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/maps/a/o;->a:I

    iput v3, v1, Lcom/google/maps/a/o;->b:I

    invoke-virtual {v1}, Lcom/google/maps/a/o;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/m;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    iget v0, v0, Landroid/content/res/Configuration;->screenWidthDp:I

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    invoke-static {v8, v0, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    move v3, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Lcom/google/r/b/a/adq;->c()V

    iget-object v1, v4, Lcom/google/r/b/a/adq;->b:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    invoke-direct {v2}, Lcom/google/n/ao;-><init>()V

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v9, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v8, v2, Lcom/google/n/ao;->d:Z

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, v4, Lcom/google/r/b/a/adq;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lcom/google/r/b/a/adq;->a:I

    iput-boolean v5, v4, Lcom/google/r/b/a/adq;->c:Z

    .line 52
    invoke-virtual {v4}, Lcom/google/r/b/a/adq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ado;

    return-object v0
.end method

.method public static a(Lcom/google/r/b/a/ajk;Lcom/google/android/apps/gmm/map/h/f;)Lcom/google/r/b/a/aje;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 120
    if-nez p0, :cond_1

    move-object v0, v3

    .line 141
    :cond_0
    :goto_0
    return-object v0

    .line 126
    :cond_1
    iget-boolean v0, p0, Lcom/google/r/b/a/ajk;->c:Z

    if-eqz v0, :cond_3

    .line 127
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_3

    :cond_2
    move-object v0, v3

    .line 128
    goto :goto_0

    .line 133
    :cond_3
    iget-object v0, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v4, 0x2

    if-ge v0, v4, :cond_4

    move-object v0, v3

    .line 134
    goto :goto_0

    .line 137
    :cond_4
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_1
    iget-object v4, p0, Lcom/google/r/b/a/ajk;->b:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/aje;->h()Lcom/google/r/b/a/aje;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aje;

    .line 138
    invoke-virtual {v0}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    :goto_2
    if-nez v1, :cond_0

    move-object v0, v3

    .line 139
    goto :goto_0

    :cond_5
    move v0, v2

    .line 137
    goto :goto_1

    :cond_6
    move v1, v2

    .line 138
    goto :goto_2
.end method
