.class abstract Lcom/google/android/apps/gmm/shared/c/a/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;
.implements Ljava/util/concurrent/Delayed;


# instance fields
.field a:J

.field b:Lcom/google/android/apps/gmm/shared/c/f;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/shared/c/a/g;->a:J

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/g;->b:Lcom/google/android/apps/gmm/shared/c/f;

    .line 22
    return-void
.end method

.method static a(Ljava/lang/Runnable;)Lcom/google/android/apps/gmm/shared/c/a/g;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/shared/c/a/h;-><init>(Ljava/lang/Runnable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/Delayed;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 54
    check-cast p1, Lcom/google/android/apps/gmm/shared/c/a/g;

    .line 58
    iget-wide v0, p0, Lcom/google/android/apps/gmm/shared/c/a/g;->a:J

    iget-wide v2, p1, Lcom/google/android/apps/gmm/shared/c/a/g;->a:J

    sub-long/2addr v0, v2

    .line 60
    cmp-long v2, v0, v4

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 17
    check-cast p1, Ljava/util/concurrent/Delayed;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/shared/c/a/g;->a(Ljava/util/concurrent/Delayed;)I

    move-result v0

    return v0
.end method

.method public final getDelay(Ljava/util/concurrent/TimeUnit;)J
    .locals 4

    .prologue
    const-wide/16 v0, 0x0

    .line 45
    iget-wide v2, p0, Lcom/google/android/apps/gmm/shared/c/a/g;->a:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_0

    .line 48
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/shared/c/a/g;->a:J

    iget-object v2, p0, Lcom/google/android/apps/gmm/shared/c/a/g;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    goto :goto_0
.end method
