.class public Lcom/google/android/apps/gmm/startpage/f/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/e/e;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/apps/gmm/iamhere/c/o;

.field private final c:Lcom/google/android/apps/gmm/startpage/e/d;

.field private final d:Z

.field private final e:Lcom/google/android/apps/gmm/base/activities/c;

.field private final f:Z

.field private final g:Lcom/google/android/apps/gmm/map/s/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/o/h/a/hb;Lcom/google/android/apps/gmm/map/s/a;)V
    .locals 4

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/f/g;->e:Lcom/google/android/apps/gmm/base/activities/c;

    .line 42
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/f/g;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 43
    new-instance v1, Lcom/google/android/apps/gmm/startpage/f/f;

    .line 45
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v2

    .line 46
    iget-object v0, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, p1, v2, v0}, Lcom/google/android/apps/gmm/startpage/f/f;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/x/o;Z)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/g;->c:Lcom/google/android/apps/gmm/startpage/e/d;

    .line 47
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    if-nez v0, :cond_1

    const/16 v0, 0x63

    .line 49
    :goto_1
    iput v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->a:I

    .line 50
    iget-boolean v0, p3, Lcom/google/o/h/a/hb;->j:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->d:Z

    .line 51
    iget-boolean v0, p3, Lcom/google/o/h/a/hb;->k:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->f:Z

    .line 52
    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/f/g;->g:Lcom/google/android/apps/gmm/map/s/a;

    .line 53
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_1
    iget-object v0, p2, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;I)Lcom/google/android/apps/gmm/base/l/a/y;
    .locals 3

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->a:I

    if-lt p2, v0, :cond_0

    add-int/lit8 p2, p2, 0x1

    .line 65
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/base/l/ac;

    check-cast p1, Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 66
    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-le v2, p2, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    :goto_0
    sget-object v2, Lcom/google/j/d/a/e;->d:Lcom/google/j/d/a/e;

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/apps/gmm/base/l/ac;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/x/o;Lcom/google/j/d/a/e;)V

    return-object v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/startpage/f/g;->d:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/startpage/f/g;->f:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/f/g;->g:Lcom/google/android/apps/gmm/map/s/a;

    .line 58
    sget-object v3, Lcom/google/android/apps/gmm/startpage/f/h;->a:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/f/g;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 59
    iget-object v3, v2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v3, v4, :cond_1

    iget-object v3, v2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v3, v4, :cond_1

    iget-object v3, v2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v3, v4, :cond_1

    iget-object v2, v2, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->d:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v2, v3, :cond_2

    :cond_1
    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    .line 57
    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :pswitch_0
    move v2, v0

    .line 58
    goto :goto_0

    :cond_2
    move v2, v1

    .line 59
    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 58
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->c:Lcom/google/android/apps/gmm/startpage/e/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/e/d;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->hk:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->c:Lcom/google/android/apps/gmm/startpage/e/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/e/d;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/l;->hl:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->hg:I

    goto :goto_0
.end method

.method public final f()Lcom/google/android/apps/gmm/startpage/e/d;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->c:Lcom/google/android/apps/gmm/startpage/e/d;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->c:Lcom/google/android/apps/gmm/startpage/e/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/e/d;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/l;->hi:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->hn:I

    goto :goto_0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/f/g;->e:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/f/g;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v2, Lcom/google/j/d/a/e;->d:Lcom/google/j/d/a/e;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/iamhere/c/o;Lcom/google/j/d/a/e;)V

    .line 81
    const/4 v0, 0x0

    return-object v0
.end method
