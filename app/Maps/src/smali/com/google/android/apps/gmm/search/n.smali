.class Lcom/google/android/apps/gmm/search/n;
.super Lcom/google/android/apps/gmm/search/o;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/search/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/search/l;)V
    .locals 0

    .prologue
    .line 147
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/n;->a:Lcom/google/android/apps/gmm/search/l;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/search/o;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    .line 150
    sget-object v0, Lcom/google/android/apps/gmm/search/l;->a:Ljava/lang/String;

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/n;->a:Lcom/google/android/apps/gmm/search/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    .line 152
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    .line 153
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 154
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/n;->a:Lcom/google/android/apps/gmm/search/l;

    iget v3, v0, Lcom/google/android/apps/gmm/search/l;->i:F

    sub-float v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, v0, Lcom/google/android/apps/gmm/search/l;->g:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 158
    sget-object v0, Lcom/google/android/apps/gmm/search/l;->a:Ljava/lang/String;

    .line 159
    iget-object v3, p0, Lcom/google/android/apps/gmm/search/n;->a:Lcom/google/android/apps/gmm/search/l;

    iget-object v0, v3, Lcom/google/android/apps/gmm/search/l;->d:Lcom/google/android/apps/gmm/search/p;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/search/p;->a()V

    new-instance v4, Lcom/google/android/apps/gmm/search/aj;

    iget-object v0, v3, Lcom/google/android/apps/gmm/search/l;->h:Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/search/aj;-><init>(Lcom/google/android/apps/gmm/search/ai;)V

    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    const/4 v5, 0x1

    iget-object v6, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    iget v7, v6, Lcom/google/maps/g/ia;->a:I

    or-int/lit16 v7, v7, 0x80

    iput v7, v6, Lcom/google/maps/g/ia;->a:I

    iput-boolean v5, v6, Lcom/google/maps/g/ia;->f:Z

    sget-object v5, Lcom/google/b/f/bc;->h:Lcom/google/b/f/bc;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/z/b/f;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    iget-object v5, v3, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v5, v5, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/apps/gmm/search/aj;->d:Lcom/google/maps/a/a;

    iput-object v0, v4, Lcom/google/android/apps/gmm/search/aj;->n:Lcom/google/maps/g/hy;

    const/4 v0, 0x0

    iput v0, v4, Lcom/google/android/apps/gmm/search/aj;->c:I

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/apps/gmm/search/aj;->u:Z

    iget-object v0, v3, Lcom/google/android/apps/gmm/search/l;->h:Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    new-instance v5, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v4

    invoke-direct {v5, v4, v0}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    iput-object v3, v5, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    iget-object v0, v3, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    iput-object v5, v3, Lcom/google/android/apps/gmm/search/l;->m:Lcom/google/android/apps/gmm/search/al;

    iput v1, v3, Lcom/google/android/apps/gmm/search/l;->i:F

    iput-object v2, v3, Lcom/google/android/apps/gmm/search/l;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, v3, Lcom/google/android/apps/gmm/search/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/search/l;->k:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 161
    :cond_0
    return-void

    .line 156
    :cond_1
    iget-object v3, v0, Lcom/google/android/apps/gmm/search/l;->k:Lcom/google/android/apps/gmm/map/b/a/r;

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v4

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    iget-object v6, v0, Lcom/google/android/apps/gmm/search/l;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v6

    const-wide v8, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v6, v8

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    invoke-static {v6, v7}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->atan(D)D

    move-result-wide v6

    const-wide v10, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v6, v10

    mul-double/2addr v6, v8

    const-wide v8, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v8, v3, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v8, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    iget v6, v0, Lcom/google/android/apps/gmm/search/l;->f:I

    int-to-double v6, v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_2

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v4

    iget-object v6, v0, Lcom/google/android/apps/gmm/search/l;->j:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v6

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v3, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    mul-double/2addr v4, v6

    iget v0, v0, Lcom/google/android/apps/gmm/search/l;->f:I

    int-to-double v6, v0

    cmpl-double v0, v4, v6

    if-lez v0, :cond_3

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
