.class public Lcom/google/android/apps/gmm/place/riddler/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/base/activities/c;

.field private c:Lcom/google/android/apps/gmm/place/riddler/d/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/apps/gmm/place/riddler/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/riddler/c;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/place/af;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/riddler/c;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 31
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/place/af;->H()Lcom/google/android/apps/gmm/place/riddler/d/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/d/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/riddler/c;->c:Lcom/google/android/apps/gmm/place/riddler/d/d;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 40
    .line 46
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p2, v1, :cond_1

    .line 47
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v4, p3

    sub-double/2addr v2, v4

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 48
    iget-object v4, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v4, v1

    sget-object v4, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 49
    iget-object v5, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v4

    aget v4, v5, v4

    sub-int/2addr v1, v4

    int-to-double v4, v1

    mul-double/2addr v2, v4

    double-to-int v1, v2

    .line 51
    :goto_0
    if-gez v1, :cond_0

    .line 52
    sget-object v1, Lcom/google/android/apps/gmm/place/riddler/c;->b:Ljava/lang/String;

    const-string v2, "Calculated a negative shift amount for riddler."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 56
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/riddler/c;->c:Lcom/google/android/apps/gmm/place/riddler/d/d;

    neg-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/riddler/d/d;->a(I)V

    .line 57
    return-void

    :cond_0
    move v0, v1

    goto :goto_1

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method
