.class public Lcom/google/android/apps/gmm/place/reservation/m;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:J

.field private static final c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/r/b/a/a/b;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Lcom/google/b/a/aa;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/aa",
            "<",
            "Lcom/google/r/b/a/a/b;",
            "Lcom/google/maps/g/oy;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final e:Lcom/google/android/apps/gmm/shared/b/a;

.field private final f:Lcom/google/android/apps/gmm/shared/c/f;

.field private g:Lcom/google/r/b/a/a/h;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/place/reservation/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->a:Ljava/lang/String;

    .line 35
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/place/reservation/m;->b:J

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/n;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/reservation/n;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->c:Ljava/util/Comparator;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/o;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/reservation/o;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->d:Lcom/google/b/a/aa;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->g:Lcom/google/r/b/a/a/h;

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->h:Z

    .line 68
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/shared/b/a;

    iput-object p1, p0, Lcom/google/android/apps/gmm/place/reservation/m;->e:Lcom/google/android/apps/gmm/shared/b/a;

    .line 69
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p2, p0, Lcom/google/android/apps/gmm/place/reservation/m;->f:Lcom/google/android/apps/gmm/shared/c/f;

    .line 70
    return-void
.end method

.method private b()Lcom/google/r/b/a/a/h;
    .locals 5

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->g:Lcom/google/r/b/a/a/h;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->g:Lcom/google/r/b/a/a/h;

    .line 151
    :goto_0
    return-object v0

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/m;->e:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aF:Lcom/google/android/apps/gmm/shared/b/c;

    sget-object v3, Lcom/google/r/b/a/a/f;->PARSER:Lcom/google/n/ax;

    .line 148
    invoke-static {}, Lcom/google/r/b/a/a/f;->d()Lcom/google/r/b/a/a/f;

    move-result-object v0

    .line 145
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;[B)[B

    move-result-object v1

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    :goto_1
    check-cast v0, Lcom/google/r/b/a/a/f;

    .line 148
    invoke-static {}, Lcom/google/r/b/a/a/f;->newBuilder()Lcom/google/r/b/a/a/h;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/r/b/a/a/h;->a(Lcom/google/r/b/a/a/f;)Lcom/google/r/b/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->g:Lcom/google/r/b/a/a/h;

    .line 149
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->h:Z

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->g:Lcom/google/r/b/a/a/h;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 145
    goto :goto_1
.end method

.method private c()V
    .locals 4

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->h:Z

    if-nez v0, :cond_0

    .line 167
    :goto_0
    return-void

    .line 160
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->b()Lcom/google/r/b/a/a/h;

    move-result-object v0

    .line 161
    iget-object v1, v0, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->e:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aF:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;)V

    .line 166
    :cond_1
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->h:Z

    goto :goto_0

    .line 164
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/m;->e:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aF:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0}, Lcom/google/r/b/a/a/h;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Lcom/google/n/at;)V

    goto :goto_1
.end method

.method private d()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->b()Lcom/google/r/b/a/a/h;

    move-result-object v4

    .line 173
    iget-object v0, v4, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-ltz v1, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 174
    invoke-virtual {v4}, Lcom/google/r/b/a/a/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/b;

    .line 175
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/reservation/m;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v8

    iget-wide v10, v0, Lcom/google/r/b/a/a/b;->d:J

    sub-long/2addr v8, v10

    sget-wide v10, Lcom/google/android/apps/gmm/place/reservation/m;->b:J

    cmp-long v1, v8, v10

    if-lez v1, :cond_3

    move v1, v2

    :goto_2
    if-nez v1, :cond_2

    .line 176
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move v1, v3

    .line 175
    goto :goto_2

    .line 180
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, v4, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_5

    .line 181
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/reservation/m;->h:Z

    .line 182
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v4, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    iget v0, v4, Lcom/google/r/b/a/a/h;->a:I

    and-int/lit8 v0, v0, -0x2

    iput v0, v4, Lcom/google/r/b/a/a/h;->a:I

    .line 183
    invoke-virtual {v4}, Lcom/google/r/b/a/a/h;->c()V

    invoke-interface {v5}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    iget-object v3, v4, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    new-instance v5, Lcom/google/n/ao;

    invoke-direct {v5}, Lcom/google/n/ao;-><init>()V

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v5, Lcom/google/n/ao;->d:Z

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 185
    :cond_5
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/accounts/Account;Lcom/google/android/apps/gmm/map/b/a/j;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/Account;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    monitor-enter p0

    if-nez p1, :cond_0

    .line 109
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->a:Ljava/lang/String;

    const-string v1, "account should not be null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 131
    :goto_0
    monitor-exit p0

    return-object v0

    .line 112
    :cond_0
    if-nez p2, :cond_1

    .line 113
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->a:Ljava/lang/String;

    const-string v1, "restaurantId should not be null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 114
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 116
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->a()V

    .line 118
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->b()Lcom/google/r/b/a/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/a/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    goto :goto_0

    .line 122
    :cond_2
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 123
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->b()Lcom/google/r/b/a/a/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/r/b/a/a/h;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/b;

    .line 124
    iget-object v1, v0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v1, Ljava/lang/String;

    :goto_2
    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 125
    iget-wide v6, v0, Lcom/google/r/b/a/a/b;->c:J

    iget-wide v8, p2, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    cmp-long v1, v6, v8

    if-nez v1, :cond_3

    .line 126
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 124
    :cond_4
    :try_start_2
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_5

    iput-object v2, v0, Lcom/google/r/b/a/a/b;->b:Ljava/lang/Object;

    :cond_5
    move-object v1, v2

    goto :goto_2

    .line 130
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->c:Ljava/util/Comparator;

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 131
    sget-object v1, Lcom/google/android/apps/gmm/place/reservation/m;->d:Lcom/google/b/a/aa;

    instance-of v0, v3, Ljava/util/RandomAccess;

    if-eqz v0, :cond_7

    new-instance v0, Lcom/google/b/c/et;

    invoke-direct {v0, v3, v1}, Lcom/google/b/c/et;-><init>(Ljava/util/List;Lcom/google/b/a/aa;)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Lcom/google/b/c/eu;

    invoke-direct {v0, v3, v1}, Lcom/google/b/c/eu;-><init>(Ljava/util/List;Lcom/google/b/a/aa;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->d()V

    .line 136
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    monitor-exit p0

    return-void

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/accounts/Account;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/maps/g/oy;J)V
    .locals 6

    .prologue
    .line 80
    monitor-enter p0

    if-nez p1, :cond_0

    .line 81
    :try_start_0
    sget-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->a:Ljava/lang/String;

    const-string v1, "account should not be null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    :goto_0
    monitor-exit p0

    return-void

    .line 84
    :cond_0
    if-nez p2, :cond_1

    .line 85
    :try_start_1
    sget-object v0, Lcom/google/android/apps/gmm/place/reservation/m;->a:Ljava/lang/String;

    const-string v1, "restaurantId should not be null"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 89
    :cond_1
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->d()V

    .line 90
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->b()Lcom/google/r/b/a/a/h;

    move-result-object v1

    invoke-static {}, Lcom/google/r/b/a/a/b;->newBuilder()Lcom/google/r/b/a/a/d;

    move-result-object v0

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 91
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/r/b/a/a/d;->a:I

    iput-object v2, v0, Lcom/google/r/b/a/a/d;->b:Ljava/lang/Object;

    .line 92
    iget-wide v2, p2, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v4, v0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/r/b/a/a/d;->a:I

    iput-wide v2, v0, Lcom/google/r/b/a/a/d;->c:J

    .line 93
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/reservation/m;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget v4, v0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/r/b/a/a/d;->a:I

    iput-wide v2, v0, Lcom/google/r/b/a/a/d;->d:J

    .line 94
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget-object v2, v0, Lcom/google/r/b/a/a/d;->e:Lcom/google/n/ao;

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object p3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v3, 0x0

    iput-object v3, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/n/ao;->d:Z

    iget v2, v0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/r/b/a/a/d;->a:I

    .line 95
    iget v2, v0, Lcom/google/r/b/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x10

    iput v2, v0, Lcom/google/r/b/a/a/d;->a:I

    iput-wide p4, v0, Lcom/google/r/b/a/a/d;->f:J

    .line 96
    invoke-virtual {v0}, Lcom/google/r/b/a/a/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/b;

    .line 90
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v1}, Lcom/google/r/b/a/a/h;->c()V

    iget-object v1, v1, Lcom/google/r/b/a/a/h;->b:Ljava/util/List;

    new-instance v2, Lcom/google/n/ao;

    invoke-direct {v2}, Lcom/google/n/ao;-><init>()V

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/reservation/m;->h:Z

    .line 98
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/reservation/m;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
