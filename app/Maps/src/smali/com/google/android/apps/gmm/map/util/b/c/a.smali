.class public Lcom/google/android/apps/gmm/map/util/b/c/a;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Lcom/google/b/c/hz",
            "<TV;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/b/c/hz;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)",
            "Lcom/google/b/c/hz",
            "<TV;>;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    invoke-static {}, Lcom/google/b/c/cb;->e()Lcom/google/b/c/cb;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    :goto_0
    return-object v0

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/c/hz;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/util/b/c/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/util/b/c/a",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 66
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/util/b/c/a;->a(Ljava/lang/Object;)Lcom/google/b/c/hz;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/util/b/c/a;->a(Ljava/lang/Object;)Lcom/google/b/c/hz;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/b/c/hz;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 68
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/util/b/c/a;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/util/b/c/a",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 78
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/util/b/c/a;->a(Ljava/lang/Object;)Lcom/google/b/c/hz;

    move-result-object v2

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/map/util/b/c/a;->a(Ljava/lang/Object;)Lcom/google/b/c/hz;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/b/c/hz;->removeAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 84
    if-ne p1, p0, :cond_0

    .line 85
    const/4 v0, 0x1

    .line 93
    :goto_0
    return v0

    .line 87
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/util/b/c/a;

    if-eqz v0, :cond_1

    .line 90
    check-cast p1, Lcom/google/android/apps/gmm/map/util/b/c/a;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 93
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/util/b/c/a;->a:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
