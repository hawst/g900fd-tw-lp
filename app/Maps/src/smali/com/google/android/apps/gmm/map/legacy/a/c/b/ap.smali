.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# static fields
.field static a:Landroid/graphics/Bitmap;

.field static d:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private static final e:Landroid/graphics/ColorMatrixColorFilter;

.field private static final f:Landroid/graphics/Paint;

.field private static final g:Landroid/graphics/BitmapFactory$Options;

.field private static final h:Ljava/lang/Object;


# instance fields
.field b:I

.field c:Lcom/google/android/apps/gmm/map/t/av;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 63
    new-instance v0, Landroid/graphics/ColorMatrixColorFilter;

    const/16 v1, 0x14

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1}, Landroid/graphics/ColorMatrixColorFilter;-><init>([F)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->e:Landroid/graphics/ColorMatrixColorFilter;

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->f:Landroid/graphics/Paint;

    .line 81
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    .line 114
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->h:Ljava/lang/Object;

    return-void

    .line 63
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/t/av;I)V
    .locals 1

    .prologue
    .line 383
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->b:I

    .line 384
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->c:Lcom/google/android/apps/gmm/map/t/av;

    .line 385
    iput p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->b:I

    .line 386
    return-void
.end method

.method private static a([B)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 393
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 394
    :try_start_0
    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    :goto_0
    iput-boolean v0, v3, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 395
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 396
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 397
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 398
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inMutable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 400
    const/4 v0, 0x0

    :try_start_1
    array-length v1, p0

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    invoke-static {p0, v0, v1, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 408
    :goto_1
    :try_start_2
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->d:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 409
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-lt v1, v3, :cond_1

    .line 410
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getAllocationByteCount()I

    move-result v1

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sget-object v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/2addr v3, v4

    if-lt v1, v3, :cond_1

    .line 411
    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1, v3, v4}, Landroid/graphics/Bitmap;->reconfigure(IILandroid/graphics/Bitmap$Config;)V

    .line 412
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 423
    :goto_2
    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->f:Landroid/graphics/Paint;

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->e:Landroid/graphics/ColorMatrixColorFilter;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 424
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 425
    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 426
    monitor-exit v2

    return-object v0

    :cond_0
    move v0, v1

    .line 394
    goto :goto_0

    .line 404
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 405
    const/4 v0, 0x0

    array-length v1, p0

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->g:Landroid/graphics/BitmapFactory$Options;

    invoke-static {p0, v0, v1, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    goto :goto_1

    .line 427
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 413
    :cond_1
    :try_start_3
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v1, v3, :cond_2

    .line 414
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ne v1, v3, :cond_2

    .line 416
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    goto :goto_2

    .line 420
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sget-object v3, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/cp;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Ljava/util/List;Lcom/google/android/apps/gmm/map/util/a/b;)Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/c/cp;",
            "Lcom/google/android/apps/gmm/v/ad;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/ar;",
            ">;",
            "Lcom/google/android/apps/gmm/map/util/a/b;",
            ")",
            "Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;"
        }
    .end annotation

    .prologue
    .line 136
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->d:Lcom/google/android/apps/gmm/map/util/a/i;

    if-nez v2, :cond_1

    .line 137
    sget-object v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->h:Ljava/lang/Object;

    monitor-enter v3

    .line 138
    :try_start_0
    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->d:Lcom/google/android/apps/gmm/map/util/a/i;

    if-nez v2, :cond_0

    .line 139
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aq;

    const/16 v4, 0xa

    const-string v5, "GLShaderOp_bitmap"

    move-object/from16 v0, p5

    invoke-direct {v2, v4, v0, v5}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/aq;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    sput-object v2, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->d:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 149
    :cond_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    :cond_1
    invoke-interface/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/internal/c/cp;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Lcom/google/android/apps/gmm/map/internal/c/ba;

    .line 154
    iget-object v4, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->e:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 155
    iget v2, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    if-eqz v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_3

    iget v2, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->m:I

    .line 156
    :goto_1
    iget v3, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_2
    if-eqz v3, :cond_5

    iget v3, v4, Lcom/google/android/apps/gmm/map/internal/c/be;->n:I

    .line 158
    :goto_3
    const/4 v5, 0x0

    .line 159
    const/4 v4, 0x0

    .line 161
    iget v6, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->a:I

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v2, v4

    move-object v3, v5

    .line 178
    :goto_4
    new-instance v4, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;

    invoke-direct {v4, v3, v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;-><init>(Lcom/google/android/apps/gmm/map/t/av;I)V

    return-object v4

    .line 149
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 155
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    const v2, -0x4c2e01

    goto :goto_1

    .line 156
    :cond_4
    const/4 v3, 0x0

    goto :goto_2

    :cond_5
    const v3, -0x744a03

    goto :goto_3

    .line 163
    :pswitch_1
    new-instance v11, Lcom/google/android/apps/gmm/map/t/av;

    const/4 v4, 0x0

    invoke-direct {v11, v2, v3, v4}, Lcom/google/android/apps/gmm/map/t/av;-><init>(IIZ)V

    .line 165
    new-instance v14, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    new-instance v3, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v3, v9}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v14, v2, v0, v3, v4}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    const/4 v10, 0x0

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    array-length v2, v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_6

    const/4 v2, 0x1

    :goto_5
    if-nez v2, :cond_7

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    :cond_7
    const/4 v2, 0x0

    move v12, v2

    :goto_6
    const/4 v2, 0x3

    if-ge v12, v2, :cond_c

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->b:[I

    aget v2, v2, v12

    const/4 v3, 0x3

    if-eq v2, v3, :cond_8

    const/4 v2, 0x1

    :goto_7
    if-nez v2, :cond_9

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_8
    const/4 v2, 0x0

    goto :goto_7

    :cond_9
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    aget-object v2, v2, v12

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a([B)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/v/ap;->d:Z

    move v13, v3

    if-nez v13, :cond_20

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v6, 0x0

    invoke-static {v2, v3, v6}, Lcom/google/android/apps/gmm/v/ar;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v8, v2

    :goto_8
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    new-instance v2, Lcom/google/android/apps/gmm/v/ar;

    new-instance v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ar;

    invoke-direct {v3, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ar;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/v/ar;-><init>(Lcom/google/android/apps/gmm/v/as;IIIIZ)V

    new-instance v3, Lcom/google/android/apps/gmm/v/ci;

    invoke-direct {v3, v2, v12}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    if-nez v13, :cond_a

    const/4 v2, 0x0

    const/4 v8, 0x0

    int-to-float v4, v4

    int-to-float v13, v6

    div-float/2addr v4, v13

    int-to-float v5, v5

    int-to-float v13, v7

    div-float/2addr v5, v13

    invoke-virtual {v3, v2, v8, v4, v5}, Lcom/google/android/apps/gmm/v/ci;->a(FFFF)V

    :cond_a
    const/16 v2, 0x2601

    const/16 v4, 0x2601

    iget-boolean v5, v3, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_b

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_b
    iput v2, v3, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, v3, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/v/ci;->j:Z

    invoke-virtual {v14, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    mul-int v2, v6, v7

    add-int/2addr v10, v2

    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_6

    :cond_c
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    const/4 v3, 0x1

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    invoke-virtual {v14, v11}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v3, 0x302

    const/16 v4, 0x303

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v3, 0x207

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    const/4 v2, 0x0

    iget-boolean v3, v14, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_d

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_d
    int-to-byte v2, v2

    iput-byte v2, v14, Lcom/google/android/apps/gmm/v/aa;->w:B

    move-object/from16 v0, p4

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v10

    move-object v3, v11

    .line 167
    goto/16 :goto_4

    .line 169
    :pswitch_2
    new-instance v11, Lcom/google/android/apps/gmm/map/t/av;

    const/4 v4, 0x1

    invoke-direct {v11, v2, v3, v4}, Lcom/google/android/apps/gmm/map/t/av;-><init>(IIZ)V

    .line 171
    const/4 v3, 0x0

    new-instance v14, Lcom/google/android/apps/gmm/v/ax;

    invoke-direct {v14}, Lcom/google/android/apps/gmm/v/ax;-><init>()V

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    if-ltz v15, :cond_e

    const/high16 v2, 0x39800000

    const/high16 v4, 0x39800000

    iget-object v5, v14, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v6, 0x0

    aput v2, v5, v6

    iget-object v2, v14, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v5, 0x4

    aput v4, v2, v5

    :goto_9
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    array-length v2, v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_f

    const/4 v2, 0x1

    :goto_a
    if-nez v2, :cond_10

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_e
    const/high16 v2, 0x3f800000    # 1.0f

    const/16 v4, 0x1000

    neg-int v5, v15

    shr-int/2addr v4, v5

    int-to-float v4, v4

    div-float/2addr v2, v4

    iget-object v4, v14, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v5, 0x0

    aput v2, v4, v5

    iget-object v4, v14, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v5, 0x4

    aput v2, v4, v5

    goto :goto_9

    :cond_f
    const/4 v2, 0x0

    goto :goto_a

    :cond_10
    const/4 v2, 0x2

    new-array v0, v2, [Lcom/google/android/apps/gmm/v/ci;

    move-object/from16 v16, v0

    const/4 v2, 0x0

    move v10, v2

    move v12, v3

    :goto_b
    const/4 v2, 0x2

    if-ge v10, v2, :cond_16

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->b:[I

    aget v2, v2, v10

    const/4 v3, 0x3

    if-eq v2, v3, :cond_11

    const/4 v2, 0x1

    :goto_c
    if-nez v2, :cond_12

    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2}, Ljava/lang/IllegalStateException;-><init>()V

    throw v2

    :cond_11
    const/4 v2, 0x0

    goto :goto_c

    :cond_12
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->c:[[B

    aget-object v2, v2, v10

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->a([B)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->g:Lcom/google/android/apps/gmm/v/ao;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ao;->a:Lcom/google/android/apps/gmm/v/ap;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/v/ap;->d:Z

    move v13, v3

    if-nez v13, :cond_1f

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    const/4 v6, 0x0

    invoke-static {v2, v3, v6}, Lcom/google/android/apps/gmm/v/ar;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object v8, v2

    :goto_d
    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    new-instance v2, Lcom/google/android/apps/gmm/v/ar;

    new-instance v3, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ar;

    invoke-direct {v3, v8}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ar;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v8, 0x0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/v/ar;-><init>(Lcom/google/android/apps/gmm/v/as;IIIIZ)V

    new-instance v3, Lcom/google/android/apps/gmm/v/ci;

    invoke-direct {v3, v2, v10}, Lcom/google/android/apps/gmm/v/ci;-><init>(Lcom/google/android/apps/gmm/v/ar;I)V

    aput-object v3, v16, v10

    if-nez v13, :cond_13

    aget-object v2, v16, v10

    const/4 v3, 0x0

    const/4 v8, 0x0

    int-to-float v4, v4

    int-to-float v13, v6

    div-float/2addr v4, v13

    int-to-float v5, v5

    int-to-float v13, v7

    div-float/2addr v5, v13

    invoke-virtual {v2, v3, v8, v4, v5}, Lcom/google/android/apps/gmm/v/ci;->a(FFFF)V

    :cond_13
    aget-object v2, v16, v10

    const/16 v3, 0x2601

    const/16 v4, 0x2601

    iget-boolean v5, v2, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v5, :cond_14

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_14
    iput v3, v2, Lcom/google/android/apps/gmm/v/ci;->h:I

    iput v4, v2, Lcom/google/android/apps/gmm/v/ci;->i:I

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/v/ci;->j:Z

    aget-object v2, v16, v10

    iget-boolean v3, v2, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v3, :cond_15

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_15
    iget-object v3, v2, Lcom/google/android/apps/gmm/v/ci;->u:Lcom/google/android/apps/gmm/v/ax;

    iget-object v4, v14, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v5, 0x0

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ax;->a:[F

    const/4 v8, 0x0

    const/16 v13, 0x9

    invoke-static {v4, v5, v3, v8, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-wide v4, v2, Lcom/google/android/apps/gmm/v/ci;->v:J

    const-wide/16 v18, 0x1

    add-long v4, v4, v18

    iput-wide v4, v2, Lcom/google/android/apps/gmm/v/ci;->v:J

    mul-int v2, v6, v7

    add-int v3, v12, v2

    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move v12, v3

    goto/16 :goto_b

    :cond_16
    const/4 v3, 0x0

    const/4 v2, 0x0

    :goto_e
    iget-object v4, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->d:[Lcom/google/android/apps/gmm/map/b/a/ax;

    array-length v4, v4

    if-ge v2, v4, :cond_18

    iget-object v4, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->d:[Lcom/google/android/apps/gmm/map/b/a/ax;

    aget-object v4, v4, v2

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v5, :cond_17

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v5, v5

    if-lez v5, :cond_17

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x3

    :goto_f
    mul-int/lit8 v4, v4, 0x3

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_e

    :cond_17
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v4, v4

    div-int/lit8 v4, v4, 0x9

    goto :goto_f

    :cond_18
    const/4 v2, 0x2

    const/4 v4, 0x0

    invoke-static {v3, v2, v4, v15}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v2

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v7, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v7}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    const/4 v2, 0x0

    move v10, v2

    :goto_10
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->d:[Lcom/google/android/apps/gmm/map/b/a/ax;

    array-length v2, v2

    if-ge v10, v2, :cond_1b

    iget-object v2, v9, Lcom/google/android/apps/gmm/map/internal/c/ba;->d:[Lcom/google/android/apps/gmm/map/b/a/ax;

    aget-object v2, v2, v10

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v3, :cond_19

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v3, v3

    if-lez v3, :cond_19

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x3

    move v8, v3

    :goto_11
    const/4 v3, 0x0

    :goto_12
    if-ge v3, v8, :cond_1a

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v13, v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v13, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    invoke-virtual {v13, v7}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_12

    :cond_19
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v3, v3

    div-int/lit8 v3, v3, 0x9

    move v8, v3

    goto :goto_11

    :cond_1a
    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->e()V

    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_10

    :cond_1b
    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->k()I

    move-result v2

    if-nez v2, :cond_1c

    const-string v2, "GlShaderOp"

    const-string v3, "Empty vertex data"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x0

    :goto_13
    move-object v3, v11

    .line 173
    goto/16 :goto_4

    .line 171
    :cond_1c
    new-instance v3, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    new-instance v4, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v4, v9}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v3, v2, v0, v4, v5}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    const/4 v2, 0x4

    const/4 v4, 0x1

    invoke-virtual {v13, v2, v4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IZ)Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    move-result-object v4

    iget v2, v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/2addr v2, v12

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    invoke-virtual {v3, v11}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    const/4 v4, 0x0

    aget-object v4, v16, v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    const/4 v4, 0x1

    aget-object v4, v16, v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v5, 0x302

    const/16 v6, 0x303

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/v/bk;->a(II)Lcom/google/android/apps/gmm/v/bl;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v5, 0x207

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    const/4 v4, 0x0

    iget-boolean v5, v3, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v5, :cond_1d

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1d
    int-to-byte v4, v4

    iput-byte v4, v3, Lcom/google/android/apps/gmm/v/aa;->w:B

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_13

    .line 175
    :pswitch_3
    new-instance v3, Lcom/google/android/apps/gmm/v/x;

    invoke-direct {v3, v2}, Lcom/google/android/apps/gmm/v/x;-><init>(I)V

    .line 176
    new-instance v2, Lcom/google/android/apps/gmm/map/t/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/t/j;->b:Lcom/google/android/apps/gmm/map/t/j;

    new-instance v6, Lcom/google/android/apps/gmm/map/t/as;

    invoke-direct {v6, v9}, Lcom/google/android/apps/gmm/map/t/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;)V

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v2, v4, v0, v6, v7}, Lcom/google/android/apps/gmm/map/t/ar;-><init>(Lcom/google/android/apps/gmm/map/t/k;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/t/as;Z)V

    move-object/from16 v0, p3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/v;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/v;->a(I)Lcom/google/android/apps/gmm/v/co;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/co;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/ad;->h:Lcom/google/android/apps/gmm/v/bk;

    const/16 v4, 0x207

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/bk;->a(I)Lcom/google/android/apps/gmm/v/bm;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/t/ar;->a(Lcom/google/android/apps/gmm/v/ai;)V

    const/4 v3, 0x0

    iget-boolean v4, v2, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v4, :cond_1e

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1e
    int-to-byte v3, v3

    iput-byte v3, v2, Lcom/google/android/apps/gmm/v/aa;->w:B

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    move-object v3, v5

    goto/16 :goto_4

    :cond_1f
    move-object v8, v2

    goto/16 :goto_d

    :cond_20
    move-object v8, v2

    goto/16 :goto_8

    .line 161
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 444
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->b:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 449
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/ap;->b:I

    add-int/lit8 v0, v0, 0x0

    return v0
.end method
