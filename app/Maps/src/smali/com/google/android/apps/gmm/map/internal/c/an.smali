.class public Lcom/google/android/apps/gmm/map/internal/c/an;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/m;


# static fields
.field static final a:[I

.field private static final x:Ljava/lang/String;


# instance fields
.field private final A:I

.field private final B:Ljava/lang/String;

.field private final C:Lcom/google/android/apps/gmm/map/internal/c/be;

.field private final D:I

.field private final E:[I

.field private final F:I

.field private final G:I

.field private final H:I

.field private final I:Ljava/lang/String;

.field private final J:Ljava/lang/String;

.field public final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field public final c:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/j;

.field public final f:F

.field public final g:I

.field public final h:[Lcom/google/android/apps/gmm/map/internal/c/a;

.field public final i:Lcom/google/android/apps/gmm/map/internal/c/z;

.field public final j:Lcom/google/android/apps/gmm/map/internal/c/z;

.field public final k:[Lcom/google/android/apps/gmm/map/internal/c/e;

.field public final l:Lcom/google/android/apps/gmm/map/internal/c/e;

.field public final m:Lcom/google/android/apps/gmm/map/indoor/d/g;

.field public final n:Lcom/google/android/apps/gmm/map/internal/c/c;

.field public final o:Lcom/google/android/apps/gmm/map/internal/c/cf;

.field public final p:Lcom/google/android/apps/gmm/map/internal/c/cu;

.field public final q:Lcom/google/b/f/cq;

.field public final r:Lcom/google/android/apps/gmm/map/internal/c/bb;

.field public final t:Ljava/lang/String;

.field public final u:Ljava/lang/String;

.field public final v:Ljava/lang/Integer;

.field public final w:Z

.field private final y:J

.field private final z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/an;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/an;->x:Ljava/lang/String;

    .line 46
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/an;->a:[I

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/ao;)V
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->c:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->y:J

    .line 159
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->g:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->z:I

    .line 160
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->h:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->A:I

    .line 161
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 162
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->i:[Lcom/google/android/apps/gmm/map/internal/c/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 163
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 164
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->k:Lcom/google/android/apps/gmm/map/internal/c/z;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 165
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->l:[Lcom/google/android/apps/gmm/map/internal/c/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 166
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->m:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->B:Ljava/lang/String;

    .line 167
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->n:Lcom/google/android/apps/gmm/map/internal/c/be;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->C:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 168
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->o:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->H:I

    .line 169
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->p:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->I:Ljava/lang/String;

    .line 170
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->q:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->D:I

    .line 171
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->r:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->F:I

    .line 172
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->s:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x1e

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->G:I

    .line 173
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->t:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->f:F

    .line 174
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->u:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    .line 175
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->v:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->J:Ljava/lang/String;

    .line 176
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->w:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    .line 177
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->x:Lcom/google/android/apps/gmm/map/internal/c/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->l:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 178
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->y:[I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->E:[I

    .line 179
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->z:Lcom/google/android/apps/gmm/map/indoor/d/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->m:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 180
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->A:Lcom/google/android/apps/gmm/map/internal/c/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 181
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->B:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 182
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->C:Lcom/google/android/apps/gmm/map/internal/c/cu;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->p:Lcom/google/android/apps/gmm/map/internal/c/cu;

    .line 183
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->D:Lcom/google/b/f/cq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->q:Lcom/google/b/f/cq;

    .line 184
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->G:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->r:Lcom/google/android/apps/gmm/map/internal/c/bb;

    .line 185
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->H:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    .line 186
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->I:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    .line 189
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 190
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 193
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->E:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->v:Ljava/lang/Integer;

    .line 194
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->F:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->w:Z

    .line 195
    return-void

    .line 172
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/ao;->s:I

    goto :goto_0
.end method

.method public static a(Lcom/google/maps/b/a/ao;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/an;
    .locals 29

    .prologue
    .line 365
    const/4 v4, 0x1

    new-array v0, v4, [Lcom/google/android/apps/gmm/map/internal/c/a;

    move-object/from16 v17, v0

    .line 366
    const/4 v12, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->d()Lcom/google/maps/b/a/a;

    move-result-object v4

    .line 367
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 366
    invoke-virtual {v4}, Lcom/google/maps/b/a/a;->a()Lcom/google/maps/b/a/b;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/maps/b/a/b;->a()Lcom/google/maps/b/a/cu;

    move-result-object v5

    invoke-static {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/maps/b/a/cu;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    iget-object v7, v4, Lcom/google/maps/b/a/a;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v7, v7, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v7, :cond_0

    iget-object v7, v4, Lcom/google/maps/b/a/a;->b:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    :cond_0
    const/4 v8, 0x0

    const/high16 v9, 0x7fc00000    # NaNf

    const/high16 v10, 0x7fc00000    # NaNf

    const/high16 v11, 0x7fc00000    # NaNf

    iget-object v7, v4, Lcom/google/maps/b/a/a;->c:Lcom/google/maps/b/a/cr;

    iget-boolean v7, v7, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v7, :cond_1

    invoke-virtual {v4}, Lcom/google/maps/b/a/a;->b()Lcom/google/maps/b/a/cb;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/maps/b/a/cb;->a()Lcom/google/maps/b/a/cu;

    move-result-object v7

    invoke-static {v7, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/maps/b/a/cu;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v8

    invoke-virtual {v4}, Lcom/google/maps/b/a/a;->b()Lcom/google/maps/b/a/cb;

    move-result-object v6

    iget-object v6, v6, Lcom/google/maps/b/a/cb;->a:Lcom/google/maps/b/a/cz;

    iget v6, v6, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v6, v6

    const/high16 v7, 0x41200000    # 10.0f

    div-float v9, v6, v7

    invoke-virtual {v4}, Lcom/google/maps/b/a/a;->b()Lcom/google/maps/b/a/cb;

    move-result-object v6

    iget-object v6, v6, Lcom/google/maps/b/a/cb;->c:Lcom/google/maps/b/a/cz;

    iget v6, v6, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v6, v6

    const/high16 v7, 0x41000000    # 8.0f

    div-float v10, v6, v7

    invoke-virtual {v4}, Lcom/google/maps/b/a/a;->b()Lcom/google/maps/b/a/cb;

    move-result-object v4

    iget-object v4, v4, Lcom/google/maps/b/a/cb;->b:Lcom/google/maps/b/a/cz;

    iget v4, v4, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v4, v4

    const/high16 v6, 0x41000000    # 8.0f

    div-float v11, v4, v6

    :cond_1
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/a;

    const/4 v6, 0x0

    const/high16 v7, 0x7fc00000    # NaNf

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;IFLcom/google/android/apps/gmm/map/b/a/y;FFF)V

    aput-object v4, v17, v12

    .line 370
    const/4 v4, 0x0

    aget-object v4, v17, v4

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v18, v0

    .line 375
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/maps/b/a/ao;->i:Lcom/google/maps/b/a/cz;

    iget-boolean v4, v4, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v4, :cond_c

    .line 376
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/maps/b/a/ao;->i:Lcom/google/maps/b/a/cz;

    iget v5, v4, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v6

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v4, v6, v5}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 384
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->a()Lcom/google/maps/b/a/al;

    move-result-object v5

    .line 383
    move-object/from16 v0, p1

    invoke-static {v5, v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/z;->a(Lcom/google/maps/b/a/al;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v19

    .line 385
    const/4 v5, 0x0

    .line 386
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/maps/b/a/ao;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v6, v6, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v6, :cond_d

    .line 388
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->b()Lcom/google/maps/b/a/al;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v5, v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/z;->a(Lcom/google/maps/b/a/al;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v5

    .line 389
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 390
    const/4 v6, 0x0

    .line 391
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/maps/b/a/ao;->d:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    .line 390
    invoke-static {v7}, Lcom/google/android/apps/gmm/map/internal/c/e;->b(I)Lcom/google/android/apps/gmm/map/internal/c/e;

    move-result-object v7

    aput-object v7, v4, v6

    .line 396
    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/maps/b/a/ao;->f:Lcom/google/maps/b/a/cz;

    iget v0, v6, Lcom/google/maps/b/a/cz;->b:I

    move/from16 v20, v0

    .line 399
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v0, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    move/from16 v21, v0

    .line 400
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/maps/b/a/ao;->g:Lcom/google/maps/b/a/cz;

    iget v6, v6, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v6, v6

    const/high16 v7, 0x41000000    # 8.0f

    div-float v22, v6, v7

    .line 404
    const/4 v6, 0x0

    .line 406
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/maps/b/a/ao;->e:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    .line 405
    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_e

    const/4 v7, 0x1

    :goto_2
    if-eqz v7, :cond_2

    .line 408
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/maps/b/a/ao;->e:Lcom/google/maps/b/a/cz;

    iget v6, v6, Lcom/google/maps/b/a/cz;->b:I

    .line 407
    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_f

    const/4 v6, 0x1

    :goto_3
    if-eqz v6, :cond_10

    .line 409
    const/16 v6, 0x800

    .line 415
    :cond_2
    :goto_4
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/maps/b/a/ao;->e:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    .line 414
    and-int/lit8 v7, v7, 0x4

    if-eqz v7, :cond_11

    const/4 v7, 0x1

    :goto_5
    if-eqz v7, :cond_3

    .line 416
    const/high16 v7, 0x20000

    or-int/2addr v6, v7

    .line 419
    :cond_3
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/maps/b/a/ao;->e:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    .line 418
    and-int/lit8 v7, v7, 0x10

    if-eqz v7, :cond_12

    const/4 v7, 0x1

    :goto_6
    if-eqz v7, :cond_4

    .line 420
    or-int/lit16 v6, v6, 0x2000

    .line 424
    :cond_4
    sget-object v7, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 430
    const/4 v8, 0x0

    .line 431
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/maps/b/a/ao;->p:Lcom/google/maps/b/a/cr;

    iget-boolean v9, v9, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v9, :cond_25

    .line 433
    or-int/lit8 v9, v6, 0x10

    .line 434
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->j()Lcom/google/maps/b/a/ai;

    move-result-object v10

    .line 435
    iget-object v6, v10, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-boolean v6, v6, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v6, :cond_24

    .line 436
    iget-object v6, v10, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-boolean v6, v6, Lcom/google/maps/b/a/db;->c:Z

    if-eqz v6, :cond_13

    .line 437
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v7, v10, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v12, v7, Lcom/google/maps/b/a/db;->b:J

    iget-object v7, v10, Lcom/google/maps/b/a/ai;->d:Lcom/google/maps/b/a/db;

    iget-wide v14, v7, Lcom/google/maps/b/a/db;->b:J

    invoke-direct {v6, v12, v13, v14, v15}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    .line 442
    :goto_7
    iget-object v7, v10, Lcom/google/maps/b/a/ai;->e:Lcom/google/maps/b/a/cr;

    iget-boolean v7, v7, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v7, :cond_23

    .line 443
    invoke-virtual {v10}, Lcom/google/maps/b/a/ai;->d()Ljava/lang/String;

    move-result-object v7

    .line 445
    :goto_8
    iget-object v8, v10, Lcom/google/maps/b/a/ai;->f:Lcom/google/maps/b/a/cz;

    iget v8, v8, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v8, :cond_14

    const/4 v8, 0x1

    :goto_9
    if-eqz v8, :cond_22

    .line 446
    or-int/lit16 v8, v9, 0x400

    move-object/from16 v16, v6

    move-object v6, v7

    .line 449
    :goto_a
    if-nez v6, :cond_6

    .line 450
    move-object/from16 v0, v19

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    .line 451
    if-nez v5, :cond_15

    const-string v6, ""

    .line 452
    :goto_b
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_5

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_5

    .line 454
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x1

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const/16 v9, 0xa

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 456
    :cond_5
    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_16

    invoke-virtual {v7, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 459
    :cond_6
    :goto_c
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->d()Lcom/google/maps/b/a/a;

    move-result-object v7

    iget-object v7, v7, Lcom/google/maps/b/a/a;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v7, v7, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v7, :cond_17

    .line 462
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->d()Lcom/google/maps/b/a/a;

    move-result-object v7

    iget-object v7, v7, Lcom/google/maps/b/a/a;->a:Lcom/google/maps/b/a/cz;

    iget v7, v7, Lcom/google/maps/b/a/cz;->b:I

    .line 461
    invoke-static {v7}, Lcom/google/android/apps/gmm/map/internal/c/e;->c(I)Lcom/google/android/apps/gmm/map/internal/c/e;

    move-result-object v7

    .line 469
    :goto_d
    sget-object v23, Lcom/google/android/apps/gmm/map/internal/c/an;->a:[I

    .line 473
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/maps/b/a/ao;->k:Lcom/google/maps/b/a/cz;

    iget v9, v9, Lcom/google/maps/b/a/cz;->b:I

    const/high16 v10, -0x80000000

    xor-int v24, v9, v10

    .line 475
    const/4 v9, 0x0

    .line 476
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/maps/b/a/ao;->q:Lcom/google/maps/b/a/cr;

    iget-boolean v10, v10, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v10, :cond_21

    .line 477
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->k()Lcom/google/maps/b/a/ag;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/gmm/map/indoor/d/g;->a(Lcom/google/maps/b/a/ag;)Lcom/google/android/apps/gmm/map/indoor/d/g;

    move-result-object v9

    move-object v15, v9

    .line 479
    :goto_e
    if-eqz v15, :cond_7

    iget-object v9, v15, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    if-lez v9, :cond_7

    .line 480
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, v18

    invoke-static {v0, v15, v9}, Lcom/google/android/apps/gmm/map/internal/c/an;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/indoor/d/g;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 483
    :cond_7
    const/4 v9, 0x0

    .line 484
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/maps/b/a/ao;->n:Lcom/google/maps/b/a/cr;

    iget-boolean v10, v10, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v10, :cond_20

    .line 485
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->h()Lcom/google/maps/b/a/z;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/gmm/map/internal/c/cf;->a(Lcom/google/maps/b/a/z;)Lcom/google/android/apps/gmm/map/internal/c/cf;

    move-result-object v9

    move-object v14, v9

    .line 487
    :goto_f
    if-eqz v14, :cond_8

    .line 488
    move-object/from16 v0, v19

    invoke-static {v14, v0}, Lcom/google/android/apps/gmm/map/internal/c/an;->a(Lcom/google/android/apps/gmm/map/internal/c/cf;Lcom/google/android/apps/gmm/map/internal/c/z;)V

    .line 490
    or-int/lit8 v8, v8, 0x10

    .line 493
    :cond_8
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/maps/b/a/ao;->l:Lcom/google/maps/b/a/cr;

    iget-boolean v9, v9, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v9, :cond_18

    .line 494
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->f()Lcom/google/maps/b/a/c;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/gmm/map/internal/c/c;->a(Lcom/google/maps/b/a/c;)Lcom/google/android/apps/gmm/map/internal/c/c;

    move-result-object v9

    move-object v13, v9

    .line 495
    :goto_10
    if-eqz v13, :cond_9

    .line 497
    or-int/lit8 v8, v8, 0x10

    .line 499
    :cond_9
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/maps/b/a/ao;->m:Lcom/google/maps/b/a/cr;

    iget-boolean v9, v9, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v9, :cond_1d

    .line 500
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->g()Lcom/google/maps/b/a/t;

    move-result-object v11

    if-eqz v11, :cond_a

    iget-object v9, v11, Lcom/google/maps/b/a/t;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v9, v9, Lcom/google/maps/b/a/cr;->c:Z

    if-nez v9, :cond_19

    :cond_a
    const/4 v9, 0x0

    .line 501
    :goto_11
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/maps/b/a/ao;->o:Lcom/google/maps/b/a/cr;

    iget-boolean v10, v10, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v10, :cond_1e

    .line 502
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->i()Lcom/google/maps/b/a/cp;

    move-result-object v11

    new-instance v10, Lcom/google/android/apps/gmm/map/internal/c/cu;

    invoke-virtual {v11}, Lcom/google/maps/b/a/cp;->a()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11}, Lcom/google/maps/b/a/cp;->b()Ljava/lang/String;

    move-result-object v25

    invoke-virtual {v11}, Lcom/google/maps/b/a/cp;->d()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, v25

    invoke-direct {v10, v12, v0, v11}, Lcom/google/android/apps/gmm/map/internal/c/cu;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :goto_12
    const-string v11, ""

    .line 505
    const-string v12, ""

    .line 507
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/b/a/ao;->p:Lcom/google/maps/b/a/cr;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    move/from16 v25, v0

    if-eqz v25, :cond_1f

    .line 508
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->j()Lcom/google/maps/b/a/ai;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/maps/b/a/ai;->a:Lcom/google/maps/b/a/cr;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    move/from16 v25, v0

    if-eqz v25, :cond_b

    .line 509
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->j()Lcom/google/maps/b/a/ai;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/maps/b/a/ai;->a()Ljava/lang/String;

    move-result-object v11

    .line 511
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->j()Lcom/google/maps/b/a/ai;

    move-result-object v25

    move-object/from16 v0, v25

    iget-object v0, v0, Lcom/google/maps/b/a/ai;->b:Lcom/google/maps/b/a/cr;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget-boolean v0, v0, Lcom/google/maps/b/a/cr;->c:Z

    move/from16 v25, v0

    if-eqz v25, :cond_1f

    .line 512
    invoke-virtual/range {p0 .. p0}, Lcom/google/maps/b/a/ao;->j()Lcom/google/maps/b/a/ai;

    move-result-object v12

    invoke-virtual {v12}, Lcom/google/maps/b/a/ai;->b()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v28, v12

    move-object v12, v11

    move-object/from16 v11, v28

    .line 516
    :goto_13
    new-instance v25, Lcom/google/android/apps/gmm/map/internal/c/ao;

    invoke-direct/range {v25 .. v25}, Lcom/google/android/apps/gmm/map/internal/c/ao;-><init>()V

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/b/a/ao;->h:Lcom/google/maps/b/a/db;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/google/maps/b/a/db;->b:J

    move-wide/from16 v26, v0

    move-wide/from16 v0, v26

    move-object/from16 v2, v25

    iput-wide v0, v2, Lcom/google/android/apps/gmm/map/internal/c/ao;->c:J

    .line 518
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/maps/b/a/ao;->j:Lcom/google/maps/b/a/cz;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, v25

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->g:I

    .line 519
    move/from16 v0, v24

    move-object/from16 v1, v25

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->h:I

    .line 520
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 521
    move-object/from16 v0, v18

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 522
    move-object/from16 v0, v16

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 523
    move-object/from16 v0, v17

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->i:[Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 524
    move-object/from16 v0, v19

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 525
    move-object/from16 v0, v25

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->k:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 526
    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->l:[Lcom/google/android/apps/gmm/map/internal/c/e;

    const/4 v4, 0x0

    .line 527
    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->m:Ljava/lang/String;

    .line 528
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->n:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 529
    move/from16 v0, v20

    move-object/from16 v1, v25

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->q:I

    .line 530
    move/from16 v0, v21

    move-object/from16 v1, v25

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->r:I

    const/4 v4, -0x1

    .line 531
    move-object/from16 v0, v25

    iput v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->s:I

    .line 532
    move/from16 v0, v22

    move-object/from16 v1, v25

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->t:F

    .line 533
    move-object/from16 v0, v25

    iput v8, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->u:I

    const/4 v4, 0x0

    .line 534
    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->v:Ljava/lang/String;

    .line 535
    move-object/from16 v0, v25

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->w:Ljava/lang/String;

    .line 536
    move-object/from16 v0, v25

    iput-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->x:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 537
    move-object/from16 v0, v23

    move-object/from16 v1, v25

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->y:[I

    .line 538
    move-object/from16 v0, v25

    iput-object v15, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->z:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 539
    move-object/from16 v0, v25

    iput-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->A:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 540
    move-object/from16 v0, v25

    iput-object v9, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->G:Lcom/google/android/apps/gmm/map/internal/c/bb;

    .line 541
    move-object/from16 v0, v25

    iput-object v14, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->B:Lcom/google/android/apps/gmm/map/internal/c/cf;

    sget-object v4, Lcom/google/b/f/t;->ga:Lcom/google/b/f/t;

    .line 542
    move-object/from16 v0, v25

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->D:Lcom/google/b/f/cq;

    .line 543
    move-object/from16 v0, v25

    iput-object v12, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->H:Ljava/lang/String;

    .line 544
    move-object/from16 v0, v25

    iput-object v11, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->I:Ljava/lang/String;

    .line 545
    move-object/from16 v0, v25

    iput-object v10, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->C:Lcom/google/android/apps/gmm/map/internal/c/cu;

    .line 546
    invoke-virtual/range {v25 .. v25}, Lcom/google/android/apps/gmm/map/internal/c/ao;->a()Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v4

    return-object v4

    .line 378
    :cond_c
    const-string v4, "PointOfInterest"

    const-string v5, "PointOfInterest has no style index"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 379
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/c/z;->a:Lcom/google/android/apps/gmm/map/internal/c/bk;

    goto/16 :goto_0

    .line 393
    :cond_d
    const/4 v4, 0x0

    new-array v4, v4, [Lcom/google/android/apps/gmm/map/internal/c/e;

    goto/16 :goto_1

    .line 405
    :cond_e
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 407
    :cond_f
    const/4 v6, 0x0

    goto/16 :goto_3

    .line 411
    :cond_10
    const/16 v6, 0x1000

    goto/16 :goto_4

    .line 414
    :cond_11
    const/4 v7, 0x0

    goto/16 :goto_5

    .line 418
    :cond_12
    const/4 v7, 0x0

    goto/16 :goto_6

    .line 439
    :cond_13
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v7, v10, Lcom/google/maps/b/a/ai;->c:Lcom/google/maps/b/a/db;

    iget-wide v12, v7, Lcom/google/maps/b/a/db;->b:J

    invoke-direct {v6, v12, v13}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(J)V

    goto/16 :goto_7

    .line 445
    :cond_14
    const/4 v8, 0x0

    goto/16 :goto_9

    .line 451
    :cond_15
    iget-object v6, v5, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    goto/16 :goto_b

    .line 456
    :cond_16
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 465
    :cond_17
    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/e;->c:Lcom/google/android/apps/gmm/map/internal/c/e;

    goto/16 :goto_d

    .line 494
    :cond_18
    const/4 v9, 0x0

    move-object v13, v9

    goto/16 :goto_10

    .line 500
    :cond_19
    const/4 v9, 0x0

    iget-object v10, v11, Lcom/google/maps/b/a/t;->b:Lcom/google/maps/b/a/cr;

    iget-boolean v10, v10, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v10, :cond_1b

    invoke-virtual {v11}, Lcom/google/maps/b/a/t;->b()Lcom/google/maps/b/a/bl;

    move-result-object v9

    iget-object v9, v9, Lcom/google/maps/b/a/bl;->a:Lcom/google/maps/b/a/cz;

    iget v9, v9, Lcom/google/maps/b/a/cz;->b:I

    const/4 v10, 0x1

    if-eq v9, v10, :cond_1a

    const/4 v10, 0x2

    if-ne v9, v10, :cond_1c

    :cond_1a
    const/4 v9, 0x1

    :cond_1b
    :goto_14
    new-instance v10, Lcom/google/android/apps/gmm/map/internal/c/bb;

    invoke-virtual {v11}, Lcom/google/maps/b/a/t;->a()Lcom/google/maps/b/a/s;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/maps/b/a/s;->a()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v9}, Lcom/google/android/apps/gmm/map/internal/c/bb;-><init>(Ljava/lang/String;Z)V

    move-object v9, v10

    goto/16 :goto_11

    :cond_1c
    const/4 v9, 0x0

    goto :goto_14

    :cond_1d
    const/4 v9, 0x0

    goto/16 :goto_11

    .line 502
    :cond_1e
    const/4 v10, 0x0

    goto/16 :goto_12

    :cond_1f
    move-object/from16 v28, v12

    move-object v12, v11

    move-object/from16 v11, v28

    goto/16 :goto_13

    :cond_20
    move-object v14, v9

    goto/16 :goto_f

    :cond_21
    move-object v15, v9

    goto/16 :goto_e

    :cond_22
    move-object/from16 v16, v6

    move v8, v9

    move-object v6, v7

    goto/16 :goto_a

    :cond_23
    move-object v7, v8

    goto/16 :goto_8

    :cond_24
    move-object v6, v7

    goto/16 :goto_7

    :cond_25
    move-object/from16 v16, v7

    move-object/from16 v28, v8

    move v8, v6

    move-object/from16 v6, v28

    goto/16 :goto_a
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/ay;)Lcom/google/android/apps/gmm/map/internal/c/an;
    .locals 34

    .prologue
    .line 216
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v13

    .line 217
    new-array v14, v13, [Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 218
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 219
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    .line 220
    const/4 v4, 0x0

    move v12, v4

    :goto_0
    if-ge v12, v13, :cond_4

    .line 221
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v6

    const/high16 v7, 0x7fc00000    # NaNf

    const/4 v4, 0x1

    and-int/2addr v4, v6

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_0

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v7, 0x41200000    # 10.0f

    div-float v7, v4, v7

    :cond_0
    const/4 v8, 0x0

    const/high16 v9, 0x7fc00000    # NaNf

    const/high16 v10, 0x7fc00000    # NaNf

    const/high16 v11, 0x7fc00000    # NaNf

    const/4 v4, 0x2

    and-int/2addr v4, v6

    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/b/a/aw;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v8

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v9, 0x41200000    # 10.0f

    div-float v9, v4, v9

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v10, 0x41000000    # 8.0f

    div-float v10, v4, v10

    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    int-to-float v4, v4

    const/high16 v11, 0x41000000    # 8.0f

    div-float v11, v4, v11

    :cond_1
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/a;

    invoke-direct/range {v4 .. v11}, Lcom/google/android/apps/gmm/map/internal/c/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;IFLcom/google/android/apps/gmm/map/b/a/y;FFF)V

    aput-object v4, v14, v12

    .line 220
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    goto :goto_0

    .line 221
    :cond_2
    const/4 v4, 0x0

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    .line 226
    :cond_4
    const/4 v4, 0x0

    aget-object v4, v14, v4

    iget-object v12, v4, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 228
    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->b:Lcom/google/android/apps/gmm/map/internal/c/n;

    .line 229
    iget-object v4, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v4, :cond_5

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_5
    iget-object v0, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->e:Lcom/google/android/apps/gmm/map/indoor/d/g;

    move-object/from16 v16, v0

    .line 231
    if-eqz v16, :cond_6

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 232
    move-object/from16 v0, v16

    invoke-static {v12, v0, v15}, Lcom/google/android/apps/gmm/map/internal/c/an;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/indoor/d/g;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 236
    :cond_6
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v5

    new-instance v17, Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-object/from16 v0, v17

    invoke-direct {v0, v5, v4}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    .line 239
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/z;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v18

    .line 241
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/z;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v19

    .line 245
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 246
    new-array v0, v5, [Lcom/google/android/apps/gmm/map/internal/c/e;

    move-object/from16 v20, v0

    .line 248
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v5, :cond_7

    .line 249
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/e;->b(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/e;

    move-result-object v6

    aput-object v6, v20, v4

    .line 248
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 253
    :cond_7
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v21

    .line 256
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v22

    .line 257
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readByte()B

    move-result v23

    .line 260
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readInt()I

    move-result v24

    .line 262
    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 263
    and-int/lit8 v5, v24, 0x1

    if-eqz v5, :cond_b

    const/4 v5, 0x1

    :goto_4
    if-eqz v5, :cond_c

    .line 264
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v4

    move-object v11, v4

    .line 268
    :goto_5
    move/from16 v0, v24

    and-int/lit16 v4, v0, 0x4000

    if-eqz v4, :cond_e

    const/4 v4, 0x1

    :goto_6
    if-eqz v4, :cond_f

    const/high16 v4, 0x3f000000    # 0.5f

    move v10, v4

    .line 271
    :goto_7
    const/4 v4, 0x0

    .line 272
    and-int/lit8 v5, v24, 0x20

    if-eqz v5, :cond_10

    const/4 v5, 0x1

    :goto_8
    if-eqz v5, :cond_20

    .line 273
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v4

    move-object v9, v4

    .line 277
    :goto_9
    const/4 v4, 0x0

    .line 278
    and-int/lit8 v5, v24, 0x40

    if-eqz v5, :cond_11

    const/4 v5, 0x1

    :goto_a
    if-eqz v5, :cond_8

    .line 279
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v4

    .line 282
    :cond_8
    iget-object v5, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v5, :cond_9

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_9
    iget-object v0, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->d:Lcom/google/android/apps/gmm/map/internal/c/cf;

    move-object/from16 v25, v0

    .line 283
    if-eqz v25, :cond_a

    .line 284
    move-object/from16 v0, v25

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/an;->a(Lcom/google/android/apps/gmm/map/internal/c/cf;Lcom/google/android/apps/gmm/map/internal/c/z;)V

    .line 288
    :cond_a
    move/from16 v0, v24

    and-int/lit16 v5, v0, 0x80

    if-eqz v5, :cond_12

    const/4 v5, 0x1

    :goto_b
    if-eqz v5, :cond_13

    .line 289
    invoke-interface/range {p0 .. p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v5

    .line 299
    :goto_c
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1f

    if-eqz v25, :cond_1f

    .line 300
    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/cf;->b:Ljava/lang/String;

    move-object v8, v5

    .line 303
    :goto_d
    move/from16 v0, v24

    and-int/lit16 v5, v0, 0x200

    if-eqz v5, :cond_16

    const/4 v5, 0x1

    :goto_e
    if-eqz v5, :cond_17

    .line 305
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/map/internal/c/e;->b(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/e;

    move-result-object v5

    .line 312
    :goto_f
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v26

    .line 313
    sget-object v6, Lcom/google/android/apps/gmm/map/internal/c/an;->a:[I

    .line 314
    if-lez v26, :cond_18

    .line 315
    move/from16 v0, v26

    new-array v6, v0, [I

    .line 316
    const/4 v7, 0x0

    :goto_10
    move/from16 v0, v26

    if-ge v7, v0, :cond_18

    .line 317
    invoke-static/range {p0 .. p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v27

    aput v27, v6, v7

    .line 316
    add-int/lit8 v7, v7, 0x1

    goto :goto_10

    .line 263
    :cond_b
    const/4 v5, 0x0

    goto/16 :goto_4

    .line 265
    :cond_c
    and-int/lit8 v5, v24, 0x2

    if-eqz v5, :cond_d

    const/4 v5, 0x1

    :goto_11
    if-eqz v5, :cond_21

    .line 266
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->e()Lcom/google/android/apps/gmm/map/b/a/k;

    move-result-object v4

    move-object v11, v4

    goto/16 :goto_5

    .line 265
    :cond_d
    const/4 v5, 0x0

    goto :goto_11

    .line 268
    :cond_e
    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_f
    const/4 v4, 0x0

    move v10, v4

    goto/16 :goto_7

    .line 272
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_8

    .line 278
    :cond_11
    const/4 v5, 0x0

    goto :goto_a

    .line 288
    :cond_12
    const/4 v5, 0x0

    goto :goto_b

    .line 291
    :cond_13
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    .line 292
    move-object/from16 v0, v19

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    .line 293
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_14

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_14

    .line 295
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x1

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const/16 v7, 0xa

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 297
    :cond_14
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_15

    invoke-virtual {v7, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_c

    :cond_15
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_c

    .line 303
    :cond_16
    const/4 v5, 0x0

    goto/16 :goto_e

    .line 308
    :cond_17
    sget-object v5, Lcom/google/android/apps/gmm/map/internal/c/e;->c:Lcom/google/android/apps/gmm/map/internal/c/e;

    goto/16 :goto_f

    .line 320
    :cond_18
    iget-object v7, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v7, :cond_19

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_19
    iget-object v0, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->c:Lcom/google/android/apps/gmm/map/internal/c/c;

    move-object/from16 v26, v0

    .line 321
    iget-object v7, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v7, :cond_1a

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_1a
    iget-object v0, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->f:Lcom/google/android/apps/gmm/map/internal/c/cu;

    move-object/from16 v27, v0

    .line 322
    iget-object v7, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->a:Lcom/google/e/a/a/a/b;

    if-eqz v7, :cond_1b

    invoke-virtual {v13}, Lcom/google/android/apps/gmm/map/internal/c/n;->a()V

    :cond_1b
    iget-object v13, v13, Lcom/google/android/apps/gmm/map/internal/c/n;->g:Lcom/google/android/apps/gmm/map/internal/c/bb;

    .line 325
    const-string v28, ""

    .line 326
    const-string v29, ""

    .line 328
    new-instance v30, Lcom/google/android/apps/gmm/map/internal/c/ao;

    invoke-direct/range {v30 .. v30}, Lcom/google/android/apps/gmm/map/internal/c/ao;-><init>()V

    .line 329
    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->a:I

    int-to-long v0, v7

    move-wide/from16 v32, v0

    move-wide/from16 v0, v32

    move-object/from16 v2, v30

    iput-wide v0, v2, Lcom/google/android/apps/gmm/map/internal/c/ao;->c:J

    .line 330
    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->c:I

    move-object/from16 v0, v30

    iput v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->g:I

    .line 331
    move-object/from16 v0, p2

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ay;->d:I

    move-object/from16 v0, v30

    iput v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->h:I

    .line 332
    move-object/from16 v0, v30

    iput-object v15, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 333
    move-object/from16 v0, v30

    iput-object v12, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 334
    move-object/from16 v0, v30

    iput-object v11, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 335
    move-object/from16 v0, v30

    iput-object v14, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->i:[Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 336
    move-object/from16 v0, v18

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 337
    move-object/from16 v0, v19

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->k:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 338
    move-object/from16 v0, v20

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->l:[Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 339
    move-object/from16 v0, v30

    iput-object v9, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->m:Ljava/lang/String;

    .line 340
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v7, :cond_1c

    const/4 v7, 0x0

    :goto_12
    move-object/from16 v0, v30

    iput-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->n:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 341
    move-object/from16 v0, v17

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    move-object/from16 v0, v30

    iput v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->o:I

    .line 342
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v7, :cond_1e

    const/4 v7, 0x0

    :goto_13
    move-object/from16 v0, v30

    iput-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->p:Ljava/lang/String;

    .line 343
    move/from16 v0, v21

    move-object/from16 v1, v30

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->q:I

    .line 344
    move/from16 v0, v22

    move-object/from16 v1, v30

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->r:I

    .line 345
    move/from16 v0, v23

    move-object/from16 v1, v30

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->s:I

    .line 346
    move-object/from16 v0, v30

    iput v10, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->t:F

    .line 347
    move/from16 v0, v24

    move-object/from16 v1, v30

    iput v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->u:I

    .line 348
    move-object/from16 v0, v30

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->v:Ljava/lang/String;

    .line 349
    move-object/from16 v0, v30

    iput-object v8, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->w:Ljava/lang/String;

    .line 350
    move-object/from16 v0, v30

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->x:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 351
    move-object/from16 v0, v30

    iput-object v6, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->y:[I

    .line 352
    move-object/from16 v0, v16

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->z:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 353
    move-object/from16 v0, v26

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->A:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 354
    move-object/from16 v0, v30

    iput-object v13, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->G:Lcom/google/android/apps/gmm/map/internal/c/bb;

    .line 355
    move-object/from16 v0, v25

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->B:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 356
    move-object/from16 v0, v27

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->C:Lcom/google/android/apps/gmm/map/internal/c/cu;

    sget-object v4, Lcom/google/b/f/t;->ga:Lcom/google/b/f/t;

    .line 357
    move-object/from16 v0, v30

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->D:Lcom/google/b/f/cq;

    .line 358
    move-object/from16 v0, v28

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->H:Ljava/lang/String;

    .line 359
    move-object/from16 v0, v29

    move-object/from16 v1, v30

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/c/ao;->I:Ljava/lang/String;

    .line 360
    invoke-virtual/range {v30 .. v30}, Lcom/google/android/apps/gmm/map/internal/c/ao;->a()Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v4

    return-object v4

    .line 340
    :cond_1c
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v9, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v9, v9

    if-nez v9, :cond_1d

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v7

    goto/16 :goto_12

    :cond_1d
    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v9, 0x0

    aget-object v7, v7, v9

    goto/16 :goto_12

    .line 342
    :cond_1e
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto :goto_13

    :cond_1f
    move-object v8, v5

    goto/16 :goto_d

    :cond_20
    move-object v9, v4

    goto/16 :goto_9

    :cond_21
    move-object v11, v4

    goto/16 :goto_5
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/indoor/d/g;Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 551
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/indoor/d/g;->a:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 553
    iget v1, v0, Lcom/google/android/apps/gmm/map/indoor/d/f;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 555
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/v;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)F

    move-result v0

    .line 556
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v2

    double-to-float v1, v2

    .line 557
    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 563
    :goto_0
    return-void

    .line 560
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/an;->x:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No level number found for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 561
    invoke-static {p2}, Lcom/google/android/apps/gmm/map/internal/c/v;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/c/cf;Lcom/google/android/apps/gmm/map/internal/c/z;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 569
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    .line 570
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 571
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cf;->d:Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 575
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 570
    goto :goto_1

    .line 571
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    goto :goto_2

    .line 569
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static l()Lcom/google/android/apps/gmm/map/internal/c/ao;
    .locals 1

    .prologue
    .line 909
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ao;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/ao;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 600
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->y:J

    return-wide v0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->C:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->C:Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->p:[I

    aget v0, v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 610
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->z:I

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 615
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->A:I

    return v0
.end method

.method public final e()Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 1

    .prologue
    .line 620
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    return-object v0
.end method

.method public f()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 729
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v2, v2, 0x400

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/c/be;
    .locals 1

    .prologue
    .line 625
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->C:Lcom/google/android/apps/gmm/map/internal/c/be;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 583
    const/4 v0, 0x7

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 640
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->D:I

    return v0
.end method

.method public final j()[I
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->E:[I

    return-object v0
.end method

.method public final k()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 842
    const/16 v0, 0x78

    .line 843
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 844
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x78

    .line 846
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->B:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 847
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->B:Ljava/lang/String;

    if-nez v2, :cond_3

    move v2, v1

    :goto_1
    add-int/2addr v0, v2

    .line 850
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    if-eqz v2, :cond_f

    .line 851
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    array-length v5, v2

    move v4, v1

    move v3, v1

    .line 852
    :goto_2
    if-ge v4, v5, :cond_6

    .line 853
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    aget-object v6, v2, v4

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/internal/c/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v2, :cond_4

    move v2, v1

    :goto_3
    add-int/lit8 v7, v2, 0x28

    iget-object v2, v6, Lcom/google/android/apps/gmm/map/internal/c/a;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v2, :cond_5

    move v2, v1

    :goto_4
    add-int/2addr v2, v7

    add-int/2addr v3, v2

    .line 852
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_2

    .line 844
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x4

    shl-int/lit8 v0, v0, 0x2

    shl-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x28

    goto :goto_0

    .line 847
    :cond_3
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x4

    shl-int/lit8 v2, v2, 0x2

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x28

    goto :goto_1

    .line 853
    :cond_4
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/y;->l()I

    move-result v2

    goto :goto_3

    :cond_5
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/y;->l()I

    move-result v2

    goto :goto_4

    :cond_6
    move v2, v3

    .line 857
    :goto_5
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    if-eqz v3, :cond_7

    .line 858
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    array-length v6, v3

    move v4, v1

    move v3, v1

    .line 859
    :goto_6
    if-ge v4, v6, :cond_8

    .line 860
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/e;->a()I

    move-result v5

    add-int/2addr v5, v3

    .line 859
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v5

    goto :goto_6

    :cond_7
    move v3, v1

    .line 863
    :cond_8
    add-int/2addr v3, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 865
    if-nez v2, :cond_9

    move v2, v1

    :goto_7
    add-int/2addr v3, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 866
    if-nez v2, :cond_a

    move v2, v1

    :goto_8
    add-int/2addr v3, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 867
    if-nez v2, :cond_b

    move v2, v1

    :goto_9
    add-int/2addr v3, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->C:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 868
    if-nez v2, :cond_c

    move v2, v1

    :goto_a
    add-int/2addr v3, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->I:Ljava/lang/String;

    .line 869
    if-nez v2, :cond_d

    move v2, v1

    :goto_b
    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->J:Ljava/lang/String;

    .line 870
    if-nez v3, :cond_e

    :goto_c
    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 872
    return v0

    .line 865
    :cond_9
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/z;->a()I

    move-result v2

    goto :goto_7

    .line 866
    :cond_a
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/z;->a()I

    move-result v2

    goto :goto_8

    .line 867
    :cond_b
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/j;->d()I

    move-result v2

    goto :goto_9

    .line 868
    :cond_c
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v2

    goto :goto_a

    .line 869
    :cond_d
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    div-int/lit8 v2, v2, 0x4

    shl-int/lit8 v2, v2, 0x2

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v2, v2, 0x28

    goto :goto_b

    .line 870
    :cond_e
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x28

    goto :goto_c

    :cond_f
    move v2, v1

    goto :goto_5
.end method

.method public final m()Lcom/google/android/apps/gmm/map/internal/c/ao;
    .locals 4

    .prologue
    .line 913
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ao;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/ao;-><init>()V

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->y:J

    .line 914
    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->c:J

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->z:I

    .line 915
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->g:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->A:I

    .line 916
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->h:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 917
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 918
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 919
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 920
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->i:[Lcom/google/android/apps/gmm/map/internal/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 921
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 922
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->k:Lcom/google/android/apps/gmm/map/internal/c/z;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 923
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->l:[Lcom/google/android/apps/gmm/map/internal/c/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->B:Ljava/lang/String;

    .line 924
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->m:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->C:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 925
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->n:Lcom/google/android/apps/gmm/map/internal/c/be;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->H:I

    .line 926
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->o:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->I:Ljava/lang/String;

    .line 927
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->p:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->D:I

    .line 928
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->q:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->F:I

    .line 929
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->r:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->G:I

    .line 930
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->s:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    .line 931
    iput v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->u:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->J:Ljava/lang/String;

    .line 932
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->v:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    .line 933
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->w:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->l:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 934
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->x:Lcom/google/android/apps/gmm/map/internal/c/e;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->E:[I

    .line 935
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->y:[I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->m:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 936
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->z:Lcom/google/android/apps/gmm/map/indoor/d/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 937
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->A:Lcom/google/android/apps/gmm/map/internal/c/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 938
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->B:Lcom/google/android/apps/gmm/map/internal/c/cf;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->p:Lcom/google/android/apps/gmm/map/internal/c/cu;

    .line 939
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->C:Lcom/google/android/apps/gmm/map/internal/c/cu;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->q:Lcom/google/b/f/cq;

    .line 940
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->D:Lcom/google/b/f/cq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    .line 941
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->H:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    .line 942
    iput-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/ao;->I:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 877
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "renderOpIndex"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->y:J

    .line 878
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "serverId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 879
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "absolutePositions"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->h:[Lcom/google/android/apps/gmm/map/internal/c/a;

    .line 880
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "primaryLabelGroup"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->i:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 881
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "secondaryLabelGroup"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->j:Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 882
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "relativePositions"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->k:[Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 883
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "snippet"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->B:Ljava/lang/String;

    .line 884
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "style"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->C:Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 885
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "styleIdIndex"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->H:I

    .line 886
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "styleId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->I:Ljava/lang/String;

    .line 887
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "rank"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->D:I

    .line 888
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "minZoomLevel"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->F:I

    .line 889
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "maxZoomLevel"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->G:I

    .line 890
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "attributes"

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    .line 891
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "supplementalLogString"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->J:Ljava/lang/String;

    .line 892
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "name"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->d:Ljava/lang/String;

    .line 893
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "primaryRelativePosition"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->l:Lcom/google/android/apps/gmm/map/internal/c/e;

    .line 894
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "providerIndices"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->E:[I

    .line 895
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "tileCoords"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 896
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "location"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 897
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "indoorRelation"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->m:Lcom/google/android/apps/gmm/map/indoor/d/g;

    .line 898
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "adMetadata"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->n:Lcom/google/android/apps/gmm/map/internal/c/c;

    .line 899
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "trafficIncidentMetadata"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->o:Lcom/google/android/apps/gmm/map/internal/c/cf;

    .line 900
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "clientLeafVe"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->q:Lcom/google/b/f/cq;

    .line 901
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "aliasType"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->v:Ljava/lang/Integer;

    .line 902
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_18

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_18
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "layerBackendSpec"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->t:Ljava/lang/String;

    .line 903
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "layerBackendFeatureId"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->u:Ljava/lang/String;

    .line 904
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1a
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 905
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
