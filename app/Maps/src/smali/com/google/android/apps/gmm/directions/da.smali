.class final enum Lcom/google/android/apps/gmm/directions/da;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/directions/da;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum b:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum c:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum d:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum e:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum f:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum g:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum h:Lcom/google/android/apps/gmm/directions/da;

.field public static final enum i:Lcom/google/android/apps/gmm/directions/da;

.field private static final synthetic j:[Lcom/google/android/apps/gmm/directions/da;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 125
    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "NODE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "BLOCK_TRANSFER"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->b:Lcom/google/android/apps/gmm/directions/da;

    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "SEGMENT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->c:Lcom/google/android/apps/gmm/directions/da;

    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "AGENCY_INFO"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->d:Lcom/google/android/apps/gmm/directions/da;

    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "INTERMEDIATE_STOP"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->e:Lcom/google/android/apps/gmm/directions/da;

    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "NON_TRANSIT_STEP"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->f:Lcom/google/android/apps/gmm/directions/da;

    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "INVISIBLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->g:Lcom/google/android/apps/gmm/directions/da;

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "EXPANDABLE_NOTICE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->h:Lcom/google/android/apps/gmm/directions/da;

    new-instance v0, Lcom/google/android/apps/gmm/directions/da;

    const-string v1, "COPYRIGHT"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/directions/da;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->i:Lcom/google/android/apps/gmm/directions/da;

    .line 124
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/gmm/directions/da;

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->a:Lcom/google/android/apps/gmm/directions/da;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->b:Lcom/google/android/apps/gmm/directions/da;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->c:Lcom/google/android/apps/gmm/directions/da;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->d:Lcom/google/android/apps/gmm/directions/da;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/directions/da;->e:Lcom/google/android/apps/gmm/directions/da;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/directions/da;->f:Lcom/google/android/apps/gmm/directions/da;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/directions/da;->g:Lcom/google/android/apps/gmm/directions/da;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/directions/da;->h:Lcom/google/android/apps/gmm/directions/da;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/directions/da;->i:Lcom/google/android/apps/gmm/directions/da;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/directions/da;->j:[Lcom/google/android/apps/gmm/directions/da;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 124
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/directions/da;
    .locals 1

    .prologue
    .line 124
    const-class v0, Lcom/google/android/apps/gmm/directions/da;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/da;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/directions/da;
    .locals 1

    .prologue
    .line 124
    sget-object v0, Lcom/google/android/apps/gmm/directions/da;->j:[Lcom/google/android/apps/gmm/directions/da;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/directions/da;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/directions/da;

    return-object v0
.end method
