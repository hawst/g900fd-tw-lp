.class public Lcom/google/android/apps/gmm/navigation/g/b/f;
.super Lcom/google/android/apps/gmm/navigation/g/b/b;
.source "PG"


# instance fields
.field public final d:Z

.field public final e:Z

.field public final f:Z

.field public final g:Lcom/google/android/apps/gmm/navigation/g/b/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/g/b/g;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/navigation/g/b/b;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/c;)V

    .line 59
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/g;->d:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->d:Z

    .line 60
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/g;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    .line 61
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/g;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    .line 62
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 63
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/navigation/g/b/f;I)Lcom/google/android/apps/gmm/navigation/g/b/f;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 216
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    if-gt v0, p1, :cond_2

    .line 250
    :goto_1
    return-object p0

    .line 221
    :cond_2
    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/b/g;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/navigation/g/b/g;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/f;)V

    .line 223
    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 224
    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 225
    iget-object v4, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v5, v4, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v4, v5, v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 227
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 228
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 230
    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p1, :cond_5

    .line 231
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 232
    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, v4, :cond_4

    .line 234
    add-int/lit8 v0, v0, -0x1

    .line 236
    :cond_4
    if-ltz v0, :cond_3

    .line 237
    invoke-interface {v6, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 238
    invoke-interface {v5, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 242
    :cond_5
    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/b/j;

    .line 243
    iget-object v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/navigation/g/b/j;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/i;)V

    .line 245
    invoke-interface {v6, v4}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    new-array v0, v1, [Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/w;

    .line 244
    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(I[Lcom/google/android/apps/gmm/map/r/a/w;)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/b/j;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 246
    new-array v0, v1, [Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/navigation/g/b/k;

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/b/j;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 248
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/g/b/j;->a()Lcom/google/android/apps/gmm/navigation/g/b/i;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/apps/gmm/navigation/g/b/g;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    .line 250
    new-instance p0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/navigation/g/b/f;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/g;)V

    goto :goto_1
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->e:Z

    if-nez v0, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    const/16 v1, 0x1388

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
