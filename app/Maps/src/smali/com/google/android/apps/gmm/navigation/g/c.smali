.class public Lcom/google/android/apps/gmm/navigation/g/c;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# static fields
.field public static final a:Lcom/google/android/apps/gmm/x/l;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/l",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lcom/google/android/apps/gmm/navigation/g/b/g;

.field c:Lcom/google/android/apps/gmm/x/a;

.field public d:Lcom/google/android/apps/gmm/map/util/b/g;

.field e:Lcom/google/android/apps/gmm/navigation/b/c;

.field f:Lcom/google/android/apps/gmm/shared/c/f;

.field public g:Lcom/google/android/apps/gmm/navigation/g/f;

.field public h:Lcom/google/android/apps/gmm/navigation/g/e;

.field public i:Lcom/google/android/apps/gmm/base/g/c;

.field public final j:Landroid/os/Handler;

.field public final k:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/gmm/navigation/g/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/x/l;

    const-string v1, "ArrivedAtPlacemark"

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/x/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/navigation/g/c;->a:Lcom/google/android/apps/gmm/x/l;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 87
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/g/c;-><init>(Landroid/os/Handler;)V

    .line 88
    return-void
.end method

.method private constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 367
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/g/d;-><init>(Lcom/google/android/apps/gmm/navigation/g/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/c;->k:Ljava/lang/Runnable;

    .line 92
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Landroid/os/Handler;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/c;->j:Landroid/os/Handler;

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/navigation/g/c;)V
    .locals 4

    .prologue
    .line 55
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/navigation/g/b/f;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/g;)V

    new-instance v2, Lcom/google/android/apps/gmm/navigation/j/a/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/j/a/b;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/f;Lcom/google/android/apps/gmm/navigation/g/b/d;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/navigation/b/c;Lcom/google/android/apps/gmm/shared/net/ad;)V
    .locals 2

    .prologue
    .line 102
    const-string v0, "gmmStorage"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/x/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/c;->c:Lcom/google/android/apps/gmm/x/a;

    .line 103
    invoke-interface {p3}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 104
    const-string v0, "navigationServiceController"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/navigation/b/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/g/c;->e:Lcom/google/android/apps/gmm/navigation/b/c;

    .line 106
    invoke-interface {p3}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/c;->f:Lcom/google/android/apps/gmm/shared/c/f;

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/b/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/b/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/c;->b:Lcom/google/android/apps/gmm/navigation/g/b/g;

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/g/f;-><init>(Lcom/google/android/apps/gmm/navigation/g/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/c;->g:Lcom/google/android/apps/gmm/navigation/g/f;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/c;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/c;->g:Lcom/google/android/apps/gmm/navigation/g/f;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 116
    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/c;->a:Lcom/google/android/apps/gmm/x/l;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/x/a;->d(Lcom/google/android/apps/gmm/x/l;)V

    .line 117
    return-void
.end method
