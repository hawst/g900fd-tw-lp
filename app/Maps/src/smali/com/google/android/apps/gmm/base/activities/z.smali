.class public abstract enum Lcom/google/android/apps/gmm/base/activities/z;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/activities/z;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/activities/z;

.field public static final enum b:Lcom/google/android/apps/gmm/base/activities/z;

.field private static final synthetic c:[Lcom/google/android/apps/gmm/base/activities/z;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/aa;

    const-string v1, "SLIDE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/activities/aa;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    .line 161
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/ab;

    const-string v1, "FADE_ZOOM"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/base/activities/ab;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/z;->b:Lcom/google/android/apps/gmm/base/activities/z;

    .line 139
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/activities/z;

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->b:Lcom/google/android/apps/gmm/base/activities/z;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/base/activities/z;->c:[Lcom/google/android/apps/gmm/base/activities/z;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/activities/z;
    .locals 1

    .prologue
    .line 139
    const-class v0, Lcom/google/android/apps/gmm/base/activities/z;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/z;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/activities/z;
    .locals 1

    .prologue
    .line 139
    sget-object v0, Lcom/google/android/apps/gmm/base/activities/z;->c:[Lcom/google/android/apps/gmm/base/activities/z;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/activities/z;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/activities/z;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)Landroid/animation/Animator;
.end method

.method public abstract b(Landroid/view/View;)Landroid/animation/Animator;
.end method

.method public abstract c(Landroid/view/View;)V
.end method
