.class public Lcom/google/android/apps/gmm/place/j/aa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/y;
.implements Lcom/google/android/apps/gmm/place/br;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/google/android/apps/gmm/base/j/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/j/b;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/aa;->c:Lcom/google/android/apps/gmm/base/j/b;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/aa;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->on:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/j/aa;->a:Landroid/content/Context;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/aa;->b:Lcom/google/android/apps/gmm/x/o;

    .line 78
    return-void
.end method

.method public final ah_()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/aa;->c:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/aa;->b:Lcom/google/android/apps/gmm/x/o;

    .line 62
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v2, Lcom/google/j/d/a/w;->f:Lcom/google/j/d/a/w;

    .line 61
    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/aa;->c:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->C()Lcom/google/android/apps/gmm/photo/a/a;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/aa;->b:Lcom/google/android/apps/gmm/x/o;

    .line 67
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    const/4 v2, 0x0

    .line 66
    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/photo/a/a;->a(Lcom/google/android/apps/gmm/base/g/c;Z)V

    .line 70
    return-object v3
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 45
    const-string v0, ""

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/aw;
    .locals 2

    .prologue
    .line 40
    sget v0, Lcom/google/android/apps/gmm/f;->cJ:I

    sget v1, Lcom/google/android/apps/gmm/d;->X:I

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/c;->a(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v0

    return-object v0
.end method
