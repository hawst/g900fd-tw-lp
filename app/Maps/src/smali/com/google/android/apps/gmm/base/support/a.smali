.class public Lcom/google/android/apps/gmm/base/support/a;
.super Landroid/widget/PopupMenu;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/views/c/e;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 29
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/view/ContextThemeWrapper;

    sget v1, Lcom/google/android/apps/gmm/m;->d:I

    invoke-direct {v0, p1, v1}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    :goto_0
    invoke-direct {p0, v0, p2}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/a;->b:Ljava/util/List;

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/support/a;->a:Landroid/content/Context;

    .line 31
    return-void

    :cond_0
    move-object v0, p1

    .line 29
    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/views/c/e;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/support/a;->getMenu()Landroid/view/Menu;

    move-result-object v5

    .line 57
    invoke-interface {v5}, Landroid/view/Menu;->clear()V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 60
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    :cond_0
    return-void

    .line 65
    :cond_1
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/c/e;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/c/e;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_0
    move v4, v0

    .line 69
    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_0

    .line 70
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/c/e;

    .line 72
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v6, 0x1b

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Element "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " is null"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/e;->a:Ljava/lang/CharSequence;

    if-eqz v1, :cond_5

    .line 75
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/support/a;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/e;->a:Ljava/lang/CharSequence;

    .line 76
    invoke-interface {v5, v3, v3, v4, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    move-result-object v6

    .line 78
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/e;->c:Lcom/google/android/libraries/curvular/aw;

    if-eqz v1, :cond_3

    .line 79
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/e;->c:Lcom/google/android/libraries/curvular/aw;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/support/a;->a:Landroid/content/Context;

    invoke-interface {v1, v7}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 82
    :cond_3
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/views/c/e;->f:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 83
    invoke-interface {v6, v2}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    .line 85
    :try_start_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/e;->g:Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 91
    :cond_4
    new-instance v1, Lcom/google/android/apps/gmm/base/support/b;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/support/a;->a:Landroid/content/Context;

    invoke-direct {v1, p0, v7, v6, v0}, Lcom/google/android/apps/gmm/base/support/b;-><init>(Lcom/google/android/apps/gmm/base/support/a;Landroid/content/Context;Landroid/view/MenuItem;Lcom/google/android/apps/gmm/base/views/c/e;)V

    invoke-interface {v6, v1}, Landroid/view/MenuItem;->setActionProvider(Landroid/view/ActionProvider;)Landroid/view/MenuItem;

    .line 69
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 86
    :catch_0
    move-exception v0

    .line 87
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_6
    move v0, v3

    goto/16 :goto_0
.end method

.method public show()V
    .locals 3

    .prologue
    .line 115
    invoke-super {p0}, Landroid/widget/PopupMenu;->show()V

    .line 116
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/a;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/c/e;

    .line 118
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/c/e;->h:Ljava/lang/Runnable;

    if-eqz v2, :cond_0

    .line 119
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/c/e;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 116
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 122
    :cond_1
    return-void
.end method
