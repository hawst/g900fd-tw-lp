.class public Lcom/google/android/apps/gmm/map/internal/c/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/q;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;II)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    .line 41
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    .line 42
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->d:I

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    .line 44
    return-void
.end method

.method private constructor <init>(Ljava/util/List;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/q;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    .line 49
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->d:I

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public static a(Ljava/io/DataInput;I)Lcom/google/android/apps/gmm/map/internal/c/p;
    .locals 4

    .prologue
    .line 62
    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v2

    .line 64
    const/4 v0, 0x0

    .line 67
    const/16 v3, 0xc

    if-lt p1, v3, :cond_1

    .line 68
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    .line 74
    :cond_0
    :goto_0
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-direct {v3, v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/p;-><init>(Ljava/lang/String;II)V

    return-object v3

    .line 70
    :cond_1
    const-string v3, "/road_shields/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 71
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/Iterable;)Lcom/google/android/apps/gmm/map/internal/c/p;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable",
            "<",
            "Lcom/google/maps/b/a/y;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/internal/c/p;"
        }
    .end annotation

    .prologue
    const/4 v2, -0x1

    .line 86
    .line 87
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 88
    invoke-interface {p0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a/y;

    .line 89
    new-instance v5, Lcom/google/android/apps/gmm/map/internal/c/q;

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/map/internal/c/q;-><init>(Lcom/google/maps/b/a/y;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    if-ne v1, v2, :cond_1

    iget-object v5, v0, Lcom/google/maps/b/a/y;->a:Lcom/google/maps/b/a/cz;

    iget-boolean v5, v5, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v5, :cond_1

    .line 91
    iget-object v0, v0, Lcom/google/maps/b/a/y;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->b:I

    move v0, v1

    :goto_1
    move v1, v0

    .line 93
    goto :goto_0

    .line 94
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/p;

    invoke-direct {v0, v3, v1}, Lcom/google/android/apps/gmm/map/internal/c/p;-><init>(Ljava/util/List;I)V

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 145
    if-ne p0, p1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return v0

    .line 148
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 149
    goto :goto_0

    .line 151
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 152
    goto :goto_0

    .line 154
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/p;

    .line 155
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_5
    move v2, v0

    :goto_2
    if-eqz v2, :cond_6

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    if-eq v2, v3, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v2, v1

    goto :goto_1

    :cond_8
    move v2, v1

    goto :goto_2
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 131
    const/4 v0, 0x1

    .line 133
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    if-eqz v1, :cond_1

    .line 137
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_1
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    add-int/2addr v0, v1

    .line 140
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 161
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 162
    const-string v0, "Icon{"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "url="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/q;

    .line 168
    const-string v3, "layer="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 171
    :cond_1
    const-string v0, ", scaleDownFactor="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", attributes="

    .line 172
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/p;->d:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x7d

    .line 173
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 174
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
