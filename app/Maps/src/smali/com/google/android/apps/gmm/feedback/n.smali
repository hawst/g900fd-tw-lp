.class Lcom/google/android/apps/gmm/feedback/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/feedback/l;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/l;)V
    .locals 0

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/feedback/h;)V
    .locals 7
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 228
    iget-object v2, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v2}, Lcom/google/android/apps/gmm/feedback/l;->c(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    .line 229
    iget-object v3, p1, Lcom/google/android/apps/gmm/feedback/h;->a:Lcom/google/android/apps/gmm/feedback/i;

    .line 230
    sget-object v4, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    if-ne v3, v4, :cond_1

    .line 231
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/feedback/l;->b:Z

    .line 232
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/feedback/l;->c:Z

    .line 234
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/util/b/g;->b(Ljava/lang/Object;)V

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iput-object v6, v0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iput-boolean v1, v4, Lcom/google/android/apps/gmm/feedback/l;->b:Z

    .line 240
    sget-object v4, Lcom/google/android/apps/gmm/feedback/q;->a:[I

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/feedback/i;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 310
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x33

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected StatusType for FeedbackFlowStatusEvent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 242
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/l;->d(Lcom/google/android/apps/gmm/feedback/l;)V

    goto :goto_0

    .line 245
    :pswitch_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-object v3, v3, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-object v3, v3, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    iget-object v4, v3, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v4, v4, Lcom/google/android/apps/gmm/feedback/g;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    if-eqz v4, :cond_2

    iget-object v3, v3, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    iget-object v3, v3, Lcom/google/android/apps/gmm/feedback/g;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/feedback/a/f;->b()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-eqz v0, :cond_4

    .line 246
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->g:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0

    .line 249
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/feedback/l;->c:Z

    goto :goto_0

    .line 253
    :pswitch_2
    :try_start_0
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->N()Lcom/google/android/apps/gmm/tutorial/a/a;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 264
    invoke-interface {v0}, Lcom/google/android/apps/gmm/tutorial/a/a;->i()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 265
    invoke-interface {v0}, Lcom/google/android/apps/gmm/tutorial/a/a;->e()Z

    .line 268
    :cond_5
    :try_start_1
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 278
    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->i()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 280
    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->k()V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/feedback/l;->g:Z

    goto/16 :goto_0

    .line 257
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    .line 259
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 272
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    .line 274
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 283
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/l;->a(Lcom/google/android/apps/gmm/feedback/l;)V

    goto/16 :goto_0

    .line 288
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-object v1, p1, Lcom/google/android/apps/gmm/feedback/h;->b:Lcom/google/android/apps/gmm/feedback/e;

    iput-object v1, v0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    .line 289
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/util/b/g;->b(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 292
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/feedback/l;->c:Z

    if-eqz v1, :cond_0

    .line 293
    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/feedback/l;->c:Z

    .line 294
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->g:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 299
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-object v1, p1, Lcom/google/android/apps/gmm/feedback/h;->b:Lcom/google/android/apps/gmm/feedback/e;

    iput-object v1, v0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/e;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    sget-object v1, Lcom/google/android/apps/gmm/feedback/a/d;->n:Lcom/google/android/apps/gmm/feedback/a/d;

    if-ne v0, v1, :cond_7

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/feedback/l;->d()V

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/l;->e(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->a:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, v6}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 306
    :cond_7
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->g:Lcom/google/android/apps/gmm/feedback/i;

    iget-object v3, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    .line 307
    iget-object v3, v3, Lcom/google/android/apps/gmm/feedback/l;->f:Lcom/google/android/apps/gmm/feedback/e;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    .line 306
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 240
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/feedback/x;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/l;->b(Lcom/google/android/apps/gmm/feedback/l;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v3

    .line 182
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 220
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 182
    goto :goto_0

    .line 186
    :cond_2
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v2, "Sensation"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 187
    iget-object v0, v3, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->s()Z

    move-result v0

    if-nez v0, :cond_3

    .line 188
    sget-object v0, Lcom/google/android/apps/gmm/feedback/l;->a:Ljava/lang/String;

    goto :goto_1

    .line 193
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->h:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/feedback/l;->b:Z

    if-nez v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget v0, v0, Lcom/google/android/apps/gmm/feedback/l;->i:I

    if-gtz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/feedback/l;->h:Z

    if-nez v0, :cond_0

    .line 217
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->d:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    invoke-interface {v0, v2, v5}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/gmm/feedback/l;->a(ZLcom/google/android/apps/gmm/feedback/a/d;)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 174
    iget-object v3, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, v3, Lcom/google/android/apps/gmm/feedback/l;->h:Z

    .line 175
    return-void

    :cond_2
    move v2, v0

    .line 174
    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/o/a/d;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/feedback/l;->g:Z

    if-nez v0, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/o/a/d;->a:Lcom/google/android/apps/gmm/o/a/g;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/g;->c:Lcom/google/android/apps/gmm/o/a/g;

    if-ne v0, v1, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/feedback/l;->g:Z

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/n;->a:Lcom/google/android/apps/gmm/feedback/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/feedback/l;->a(Lcom/google/android/apps/gmm/feedback/l;)V

    goto :goto_0
.end method
