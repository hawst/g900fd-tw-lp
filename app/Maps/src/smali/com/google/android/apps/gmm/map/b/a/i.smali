.class public Lcom/google/android/apps/gmm/map/b/a/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    .line 12
    iput v0, p0, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    .line 13
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput p1, p0, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    .line 17
    iput p2, p0, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    .line 18
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 22
    if-ne p0, p1, :cond_1

    .line 31
    :cond_0
    :goto_0
    return v0

    .line 26
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 27
    goto :goto_0

    .line 30
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/i;

    .line 31
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    const v1, 0xffff

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    add-int/2addr v0, v1

    return v0
.end method
