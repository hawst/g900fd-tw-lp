.class public Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;
.super Landroid/app/Activity;
.source "PG"


# instance fields
.field a:Landroid/widget/EditText;

.field b:Landroid/widget/EditText;

.field c:Landroid/widget/CheckBox;

.field d:Lcom/google/maps/g/a/hm;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/graphics/Typeface;

.field private g:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 46
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->d:Lcom/google/maps/g/a/hm;

    .line 51
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/16 v7, 0x8

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 114
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 115
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->c:Z

    if-eqz v0, :cond_0

    .line 116
    sget v0, Lcom/google/android/apps/gmm/m;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->setTheme(I)V

    .line 118
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/k;->aa:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/bl;->f(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->f:Landroid/graphics/Typeface;

    .line 119
    sget v0, Lcom/google/android/apps/gmm/k;->ab:I

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->e(I)Lcom/google/android/libraries/curvular/bl;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/bl;->f(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->g:Landroid/graphics/Typeface;

    .line 121
    sget v0, Lcom/google/android/apps/gmm/h;->ag:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->setContentView(I)V

    .line 123
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->d:Lcom/google/maps/g/a/hm;

    .line 125
    sget v0, Lcom/google/android/apps/gmm/g;->dW:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->e:Landroid/widget/TextView;

    .line 126
    sget v0, Lcom/google/android/apps/gmm/g;->dq:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->b:Landroid/widget/EditText;

    .line 127
    sget v0, Lcom/google/android/apps/gmm/g;->aa:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->a:Landroid/widget/EditText;

    .line 129
    sget v0, Lcom/google/android/apps/gmm/g;->em:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->c:Landroid/widget/CheckBox;

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->d:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-eq v0, v2, :cond_2

    move v0, v5

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 132
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.location.gps"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 136
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/g;->ek:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 137
    sget v1, Lcom/google/android/apps/gmm/g;->bK:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 138
    sget v2, Lcom/google/android/apps/gmm/g;->bL:I

    invoke-virtual {v0, v2}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RadioButton;

    .line 139
    sget v3, Lcom/google/android/apps/gmm/g;->bJ:I

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RadioButton;

    .line 140
    sget v4, Lcom/google/android/apps/gmm/g;->bM:I

    invoke-virtual {v0, v4}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RadioButton;

    .line 141
    sget-object v8, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    invoke-virtual {v1, v8}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 142
    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 143
    sget-object v1, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    invoke-virtual {v3, v1}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 144
    sget-object v1, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    invoke-virtual {v4, v1}, Landroid/widget/RadioButton;->setTag(Ljava/lang/Object;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->d:Lcom/google/maps/g/a/hm;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 146
    new-instance v1, Lcom/google/android/apps/gmm/appwidget/a;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/appwidget/a;-><init>(Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 155
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->b:Z

    if-eqz v0, :cond_3

    :goto_1
    if-eqz v5, :cond_4

    move v0, v6

    :goto_2
    invoke-virtual {v3, v0}, Landroid/widget/RadioButton;->setVisibility(I)V

    .line 157
    sget v0, Lcom/google/android/apps/gmm/g;->cX:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 158
    new-instance v1, Lcom/google/android/apps/gmm/appwidget/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/appwidget/b;-><init>(Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;Landroid/widget/Button;)V

    .line 177
    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 178
    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 180
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setEnabled(Z)V

    .line 181
    new-instance v1, Lcom/google/android/apps/gmm/appwidget/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/appwidget/c;-><init>(Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    sget v1, Lcom/google/android/apps/gmm/g;->I:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 191
    new-instance v2, Lcom/google/android/apps/gmm/appwidget/d;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/appwidget/d;-><init>(Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->e:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->f:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 201
    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->c:Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setTypeface(Landroid/graphics/Typeface;)V

    .line 202
    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->a:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 203
    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->b:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->g:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    .line 204
    iget-object v2, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->f:Landroid/graphics/Typeface;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/appwidget/CreateDirectionsShortcutActivity;->f:Landroid/graphics/Typeface;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 206
    return-void

    :cond_2
    move v0, v6

    .line 131
    goto/16 :goto_0

    :cond_3
    move v5, v6

    .line 155
    goto :goto_1

    :cond_4
    move v0, v7

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 210
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 211
    return-void
.end method
