.class public Lcom/google/android/apps/gmm/v/m;
.super Lcom/google/android/apps/gmm/v/ai;
.source "PG"


# instance fields
.field private a:I

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->m:Lcom/google/android/apps/gmm/v/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/m;->a:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/v/m;->b:I

    .line 56
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->m:Lcom/google/android/apps/gmm/v/aj;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/ai;-><init>(Lcom/google/android/apps/gmm/v/aj;)V

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/m;->a:I

    .line 44
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/v/m;->b:I

    .line 68
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/m;->p:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput p1, p0, Lcom/google/android/apps/gmm/v/m;->a:I

    iput p2, p0, Lcom/google/android/apps/gmm/v/m;->b:I

    .line 69
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 3

    .prologue
    .line 114
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/v/m;

    .line 115
    if-nez p2, :cond_1

    .line 116
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 117
    iget v0, p0, Lcom/google/android/apps/gmm/v/m;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/m;->b:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 121
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    iget v1, v0, Lcom/google/android/apps/gmm/v/m;->a:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/m;->a:I

    if-ne v1, v2, :cond_2

    iget v0, v0, Lcom/google/android/apps/gmm/v/m;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/m;->b:I

    if-eq v0, v1, :cond_0

    .line 119
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/v/m;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/m;->b:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_0
.end method

.method final b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ai;)V
    .locals 1

    .prologue
    .line 125
    if-nez p2, :cond_0

    .line 126
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 128
    :cond_0
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 132
    instance-of v1, p1, Lcom/google/android/apps/gmm/v/m;

    if-eqz v1, :cond_0

    .line 133
    check-cast p1, Lcom/google/android/apps/gmm/v/m;

    .line 134
    iget v1, p0, Lcom/google/android/apps/gmm/v/m;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/v/m;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/v/m;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/v/m;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 136
    :cond_0
    return v0
.end method
