.class public Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;
.super Landroid/view/TextureView;
.source "PG"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/internal/vector/u;


# instance fields
.field public final b:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/android/apps/gmm/map/internal/vector/t;

.field public d:Lcom/google/android/apps/gmm/map/internal/vector/k;

.field public e:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

.field public f:Lcom/google/android/apps/gmm/map/internal/vector/q;

.field public g:Lcom/google/android/apps/gmm/map/internal/vector/r;

.field public h:I

.field public i:Z

.field private j:Z

.field private k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1774
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/u;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 1776
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    .line 133
    invoke-virtual {p0, p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 134
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1776
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    .line 142
    invoke-virtual {p0, p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 143
    return-void
.end method


# virtual methods
.method public final V_()V
    .locals 3

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->g:Z

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->b:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->b:Z

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->g:Z

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->h:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->c:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-void
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 155
    return-void

    .line 154
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 537
    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    .line 541
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    .line 542
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 544
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v0, :cond_3

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->a()I

    move-result v0

    .line 549
    :goto_0
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/vector/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/vector/k;->e()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/map/internal/vector/t;-><init>(Ljava/lang/ref/WeakReference;Lcom/google/android/apps/gmm/map/c/a/a;)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    .line 551
    if-eq v0, v1, :cond_1

    .line 552
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->a(I)V

    .line 554
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->start()V

    .line 556
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->j:Z

    .line 557
    return-void

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 569
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v0, :cond_0

    .line 570
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->b()V

    .line 572
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->j:Z

    .line 573
    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    .line 574
    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->d:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->e:Z

    if-eqz v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/gmm/map/internal/vector/t;->a(II)V

    .line 473
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->d:Z

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->notifyAll()V

    :goto_0
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->e:Z

    if-nez v2, :cond_0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/vector/t;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->a:Lcom/google/android/apps/gmm/map/internal/vector/u;

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_0
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 483
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/apps/gmm/map/internal/vector/t;->a(II)V

    .line 493
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 498
    return-void
.end method

.method public final setKeepEglContextOnDetach(Z)V
    .locals 1

    .prologue
    .line 247
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->k:Z

    .line 248
    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->b()V

    .line 251
    :cond_0
    return-void
.end method

.method public final setPreserveEGLContextOnPause(Z)V
    .locals 0

    .prologue
    .line 221
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->i:Z

    .line 222
    return-void
.end method
