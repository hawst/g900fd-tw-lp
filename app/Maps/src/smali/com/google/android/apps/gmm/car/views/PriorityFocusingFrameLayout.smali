.class public Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 20
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p2, Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    .line 21
    return-void
.end method


# virtual methods
.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 26
    const/16 v0, 0x82

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    .line 27
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 42
    :cond_0
    return-void

    .line 33
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_2

    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 36
    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 37
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 38
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    if-eq v1, v2, :cond_3

    .line 39
    invoke-virtual {v1, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 36
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;->a:Landroid/view/View;

    .line 47
    invoke-virtual {v0, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const/4 v0, 0x1

    .line 51
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0
.end method
