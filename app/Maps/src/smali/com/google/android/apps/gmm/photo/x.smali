.class Lcom/google/android/apps/gmm/photo/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/photo/f;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/photo/w;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/photo/w;)V
    .locals 0

    .prologue
    .line 617
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->e:Lcom/google/android/apps/gmm/photo/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/y;->b()V

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->gv:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 642
    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 643
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/photo/w;->a:Lcom/google/android/apps/gmm/photo/e;

    .line 645
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/aje;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 621
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    .line 628
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    .line 629
    iget-object v2, v2, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    invoke-virtual {v1, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    move-result-object v1

    .line 630
    invoke-virtual {v1, p1}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    move-result-object v1

    .line 631
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    .line 628
    iput-object v1, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->m:Lcom/google/b/c/cv;

    .line 633
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v1, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->e:Lcom/google/android/apps/gmm/photo/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/y;->b()V

    .line 634
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/photo/w;->a:Lcom/google/android/apps/gmm/photo/e;

    .line 635
    return-void

    .line 624
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/x;->a:Lcom/google/android/apps/gmm/photo/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/photo/w;->b:Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;

    iget v1, v1, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    invoke-static {v1, p2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/photo/PhotoViewerFragment;->g:I

    goto :goto_0
.end method
