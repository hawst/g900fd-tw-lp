.class public Lcom/google/android/apps/gmm/directions/f/d/h;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/maps/g/a/fz;IZ)I
    .locals 2
    .param p0    # Lcom/google/maps/g/a/fz;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 171
    if-nez p0, :cond_0

    .line 172
    sget-object p0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    .line 174
    :cond_0
    if-eqz p2, :cond_2

    .line 175
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/i;->b:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 183
    if-nez p1, :cond_1

    sget p1, Lcom/google/android/apps/gmm/d;->p:I

    .line 195
    :cond_1
    :goto_0
    return p1

    .line 177
    :pswitch_0
    sget p1, Lcom/google/android/apps/gmm/d;->l:I

    goto :goto_0

    .line 179
    :pswitch_1
    sget p1, Lcom/google/android/apps/gmm/d;->r:I

    goto :goto_0

    .line 181
    :pswitch_2
    sget p1, Lcom/google/android/apps/gmm/d;->n:I

    goto :goto_0

    .line 187
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/i;->b:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 195
    if-nez p1, :cond_1

    sget p1, Lcom/google/android/apps/gmm/d;->q:I

    goto :goto_0

    .line 189
    :pswitch_3
    sget p1, Lcom/google/android/apps/gmm/d;->m:I

    goto :goto_0

    .line 191
    :pswitch_4
    sget p1, Lcom/google/android/apps/gmm/d;->s:I

    goto :goto_0

    .line 193
    :pswitch_5
    sget p1, Lcom/google/android/apps/gmm/d;->o:I

    goto :goto_0

    .line 175
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 187
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v2, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    iget v0, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_2
    iget v0, v0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 41
    :cond_0
    :goto_3
    return-object v0

    .line 38
    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 39
    :cond_3
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_2

    .line 41
    :cond_4
    const/4 v0, 0x0

    goto :goto_3
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/a/fz;)Ljava/lang/String;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 213
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/i;->b:[I

    invoke-virtual {p1}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 221
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 215
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->mz:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 217
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->mA:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 219
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->my:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 213
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/google/maps/g/a/fz;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 262
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    .line 263
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Landroid/content/Context;Lcom/google/maps/g/a/fz;)Ljava/lang/String;

    move-result-object v0

    .line 273
    :goto_1
    return-object v0

    :cond_1
    move v0, v1

    .line 262
    goto :goto_0

    .line 265
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/directions/f/d/i;->b:[I

    invoke-virtual {p1}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    .line 273
    sget v0, Lcom/google/android/apps/gmm/l;->oD:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 267
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/l;->oy:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 269
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/l;->oA:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 271
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/l;->ow:I

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;
    .locals 9
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_1

    .line 55
    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;

    move-result-object v1

    .line 75
    :cond_0
    :goto_0
    return-object v1

    .line 58
    :cond_1
    const/4 v1, 0x0

    move v4, v3

    .line 60
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_6

    .line 61
    new-instance v6, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v6, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    move v2, v3

    .line 62
    :goto_2
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v0, v0, Lcom/google/maps/g/a/ea;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 63
    invoke-virtual {v6, v2}, Lcom/google/android/apps/gmm/map/r/a/p;->a(I)Lcom/google/android/apps/gmm/map/r/a/al;

    move-result-object v7

    .line 64
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v0, v0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    iget v0, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v5, :cond_3

    move v0, v5

    :goto_3
    if-eqz v0, :cond_7

    .line 65
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/r/a/al;->a:Lcom/google/maps/g/a/ff;

    iget-object v0, v0, Lcom/google/maps/g/a/ff;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fk;

    iget v0, v0, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v0}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    .line 66
    :cond_2
    if-nez v1, :cond_4

    .line 62
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_2

    :cond_3
    move v0, v3

    .line 64
    goto :goto_3

    .line 68
    :cond_4
    if-eq v1, v0, :cond_7

    .line 69
    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;

    move-result-object v1

    goto :goto_0

    .line 60
    :cond_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 75
    :cond_6
    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/hm;

    move-result-object v1

    goto :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_4
.end method

.method public static c(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/fz;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_0
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v1, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_1
    iget v0, v0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_6

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_3
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v1, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_4
    iget v0, v0, Lcom/google/maps/g/a/au;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    .line 127
    :cond_0
    :goto_5
    return-object v0

    .line 124
    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 125
    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_3

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_4

    .line 127
    :cond_6
    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    goto :goto_5
.end method

.method public static d(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/android/apps/gmm/z/b/l;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 339
    if-nez p0, :cond_0

    .line 345
    :goto_0
    return-object v1

    .line 342
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v4

    .line 343
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ao;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, v4, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v5, 0x2

    if-ne v0, v5, :cond_4

    move v0, v2

    :goto_3
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ao;->c()Ljava/lang/String;

    move-result-object v1

    :cond_1
    iput-object v1, v4, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 345
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    goto :goto_0

    :cond_2
    move v0, v3

    .line 343
    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move v0, v3

    .line 344
    goto :goto_3
.end method

.method public static e(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/be;
    .locals 5
    .param p0    # Lcom/google/android/apps/gmm/map/r/a/ao;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 390
    if-nez p0, :cond_0

    .line 396
    :goto_0
    return-object v1

    .line 393
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_1

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_1
    iget-object v4, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v4, :cond_2

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_2
    iget v0, v0, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_3
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_4
    iget-object v4, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v4, :cond_5

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v0

    :goto_5
    iget-object v0, v0, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/be;

    .line 394
    :goto_6
    if-eqz v0, :cond_7

    move-object v1, v0

    goto :goto_0

    .line 393
    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_1

    :cond_2
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_2

    :cond_3
    move v0, v3

    goto :goto_3

    :cond_4
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_4

    :cond_5
    iget-object v0, v0, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    goto :goto_5

    :cond_6
    move-object v0, v1

    goto :goto_6

    .line 396
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_8

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_7
    iget v0, v0, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v4, 0x8

    if-ne v0, v4, :cond_9

    move v0, v2

    :goto_8
    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v1, :cond_a

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v0

    :goto_9
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v1, :cond_b

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v0

    :goto_a
    move-object v1, v0

    goto :goto_0

    :cond_8
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_7

    :cond_9
    move v0, v3

    goto :goto_8

    :cond_a
    iget-object v0, v0, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_9

    :cond_b
    iget-object v1, v0, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_0

    :cond_c
    move-object v0, v1

    goto :goto_a
.end method
