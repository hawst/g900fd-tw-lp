.class public Lcom/google/android/apps/gmm/map/u/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/google/android/apps/gmm/map/u/a/e;",
        ">;"
    }
.end annotation


# instance fields
.field a:I

.field b:Lcom/google/android/apps/gmm/map/u/a/c;

.field c:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Lcom/google/android/apps/gmm/map/u/a/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/u/a/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->c:Ljava/util/Iterator;

    .line 254
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/u/b;->a()V

    .line 255
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 273
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->c:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->b:Lcom/google/android/apps/gmm/map/u/a/c;

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->b:Lcom/google/android/apps/gmm/map/u/a/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 282
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/u/b;->a:I

    .line 283
    return-void

    .line 280
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->b:Lcom/google/android/apps/gmm/map/u/a/c;

    goto :goto_0
.end method


# virtual methods
.method public hasNext()Z
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->b:Lcom/google/android/apps/gmm/map/u/a/c;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/u/b;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/u/b;->b:Lcom/google/android/apps/gmm/map/u/a/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/u/b;->b:Lcom/google/android/apps/gmm/map/u/a/c;

    iget v1, p0, Lcom/google/android/apps/gmm/map/u/b;->a:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/u/b;->a:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    aget-object v0, v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/u/b;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/u/b;->b:Lcom/google/android/apps/gmm/map/u/a/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/u/a/c;->a:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v2, v2

    if-lt v1, v2, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/u/b;->a()V

    :cond_0
    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 287
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
