.class Lcom/google/android/apps/gmm/search/w;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/search/SearchListFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/search/SearchListFragment;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/e;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 256
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/e;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/util/b/g;->b(Ljava/lang/Object;)V

    .line 258
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/e/e;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/e/e;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/search/SearchListFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    :cond_1
    return-void

    .line 256
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/hotels/a/a;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/search/SearchListFragment;->n:Z

    if-nez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    .line 250
    iget-object v1, v1, Lcom/google/android/apps/gmm/search/SearchListFragment;->m:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->k()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/search/SearchListFragment;->y:Lcom/google/android/apps/gmm/hotels/a/g;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/hotels/a/b;->a(Ljava/util/List;Lcom/google/android/apps/gmm/hotels/a/g;)V

    .line 252
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/SearchListFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 221
    if-eqz v0, :cond_0

    .line 224
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    iput-object v0, v1, Lcom/google/android/apps/gmm/search/SearchListFragment;->p:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/w;->a:Lcom/google/android/apps/gmm/search/SearchListFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/SearchListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/search/x;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/search/x;-><init>(Lcom/google/android/apps/gmm/search/w;Lcom/google/android/apps/gmm/map/r/b/a;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
