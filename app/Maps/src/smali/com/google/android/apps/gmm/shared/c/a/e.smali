.class public Lcom/google/android/apps/gmm/shared/c/a/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field private final a:Ljava/lang/Thread$UncaughtExceptionHandler;

.field private final b:Lcom/google/android/apps/gmm/map/c/a/a;


# direct methods
.method private constructor <init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p2, p0, Lcom/google/android/apps/gmm/shared/c/a/e;->b:Lcom/google/android/apps/gmm/map/c/a/a;

    .line 53
    if-nez p1, :cond_0

    .line 54
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/e;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/shared/c/a/e;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Thread;Lcom/google/android/apps/gmm/map/c/a/a;)V
    .locals 2
    .param p1    # Lcom/google/android/apps/gmm/map/c/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/e;

    .line 63
    invoke-virtual {p0}, Ljava/lang/Thread;->getUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/apps/gmm/shared/c/a/e;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 64
    invoke-virtual {p0, v0}, Ljava/lang/Thread;->setUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 65
    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/e;->b:Lcom/google/android/apps/gmm/map/c/a/a;

    invoke-static {p2, v0}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Throwable;Lcom/google/android/apps/gmm/map/c/a/a;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/shared/c/a/e;->a:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 102
    return-void

    .line 88
    :goto_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 89
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_1

    .line 91
    :cond_0
    invoke-virtual {v0, p2}, Ljava/lang/Throwable;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-object p2, v0

    .line 95
    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    .line 82
    :catch_1
    move-exception v0

    goto :goto_1
.end method
