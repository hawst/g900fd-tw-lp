.class Lcom/google/android/apps/gmm/x/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/x/l;

.field final synthetic b:Ljava/io/Serializable;

.field final synthetic c:Lcom/google/android/apps/gmm/x/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Lcom/google/android/apps/gmm/x/b;->c:Lcom/google/android/apps/gmm/x/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/x/b;->a:Lcom/google/android/apps/gmm/x/l;

    iput-object p3, p0, Lcom/google/android/apps/gmm/x/b;->b:Ljava/io/Serializable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 370
    iget-object v3, p0, Lcom/google/android/apps/gmm/x/b;->c:Lcom/google/android/apps/gmm/x/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/x/b;->a:Lcom/google/android/apps/gmm/x/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/x/b;->b:Ljava/io/Serializable;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    if-eqz v2, :cond_1

    :try_start_0
    sget-object v1, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x13

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Putting "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " with id = "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v6, Lcom/google/android/apps/gmm/x/t;

    invoke-direct {v6, v5, v3}, Lcom/google/android/apps/gmm/x/t;-><init>(Ljava/io/OutputStream;Lcom/google/android/apps/gmm/x/a;)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/io/ObjectOutputStream;->writeUTF(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v6, v1}, Ljava/io/ObjectOutputStream;->writeByte(I)V

    instance-of v1, v2, Lcom/google/android/apps/gmm/x/n;

    if-eqz v1, :cond_0

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/x/n;

    move-object v1, v0

    invoke-interface {v1, v6}, Lcom/google/android/apps/gmm/x/n;->a(Ljava/io/ObjectOutputStream;)V

    :goto_0
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/x/t;->close()V

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iget-object v3, v3, Lcom/google/android/apps/gmm/x/a;->d:Lcom/google/android/apps/gmm/x/i;

    invoke-interface {v3, v4, v1}, Lcom/google/android/apps/gmm/x/i;->a(Lcom/google/android/apps/gmm/x/l;[B)V

    .line 371
    :goto_1
    return-void

    .line 370
    :cond_0
    invoke-virtual {v6, v2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    sget-object v3, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    new-instance v4, Ljava/lang/RuntimeException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x15

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Failed to save item: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_1
    :try_start_1
    iget-object v1, v3, Lcom/google/android/apps/gmm/x/a;->d:Lcom/google/android/apps/gmm/x/i;

    invoke-interface {v1, v4}, Lcom/google/android/apps/gmm/x/i;->b(Lcom/google/android/apps/gmm/x/l;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
