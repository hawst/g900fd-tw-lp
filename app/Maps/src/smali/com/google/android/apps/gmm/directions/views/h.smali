.class public Lcom/google/android/apps/gmm/directions/views/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Landroid/widget/TextView;

.field public final c:Lcom/google/android/apps/gmm/map/i/a/a;

.field public final d:I

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Lcom/google/android/apps/gmm/map/i/a/c;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/widget/TextView;)V
    .locals 2
    .param p1    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;",
            "Landroid/widget/TextView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-static {}, Lcom/google/b/c/hj;->d()Ljava/util/TreeMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->e:Ljava/util/Map;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/directions/views/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/views/i;-><init>(Lcom/google/android/apps/gmm/directions/views/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->f:Lcom/google/android/apps/gmm/map/i/a/c;

    .line 75
    invoke-virtual {p2}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 76
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/views/h;->a:Ljava/util/List;

    .line 77
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/views/h;->b:Landroid/widget/TextView;

    .line 78
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/c/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/directions/views/h;->c:Lcom/google/android/apps/gmm/map/i/a/a;

    .line 79
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/i/b/a;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->d:I

    .line 81
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 82
    return-void
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->a:Ljava/util/List;

    if-nez v0, :cond_1

    .line 111
    :cond_0
    return-void

    .line 92
    :cond_1
    new-instance v2, Ljava/util/TreeSet;

    invoke-direct {v2}, Ljava/util/TreeSet;-><init>()V

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 94
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/i;

    iget v1, v1, Lcom/google/maps/g/a/i;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/l;->a(I)Lcom/google/maps/g/a/l;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    :cond_3
    sget-object v4, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    if-ne v1, v4, :cond_2

    .line 95
    iget-object v1, v0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/i;

    iget v1, v1, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v4, 0x2

    if-ne v1, v4, :cond_4

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 96
    iget-object v0, v0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 95
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 100
    :cond_5
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_6
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->c:Lcom/google/android/apps/gmm/map/i/a/a;

    sget-object v2, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    iget v3, p0, Lcom/google/android/apps/gmm/directions/views/h;->d:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/views/h;->b:Landroid/widget/TextView;

    .line 103
    invoke-virtual {v4}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/views/h;->f:Lcom/google/android/apps/gmm/map/i/a/c;

    .line 102
    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/lang/String;Lcom/google/r/b/a/acy;FLandroid/content/res/Resources;Lcom/google/android/apps/gmm/map/i/a/c;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 106
    if-eqz v0, :cond_6

    .line 107
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/h;->e:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2
.end method

.method public b()V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x4

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 117
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 118
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->b:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    :goto_0
    return-void

    .line 124
    :cond_0
    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    .line 125
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    :pswitch_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ee;

    .line 127
    sget-object v4, Lcom/google/android/apps/gmm/directions/views/j;->a:[I

    iget v1, v0, Lcom/google/maps/g/a/ee;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/eh;->a(I)Lcom/google/maps/g/a/eh;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/maps/g/a/eh;->a:Lcom/google/maps/g/a/eh;

    :cond_2
    invoke-virtual {v1}, Lcom/google/maps/g/a/eh;->ordinal()I

    move-result v1

    aget v1, v4, v1

    packed-switch v1, :pswitch_data_0

    iget v1, v0, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v12, :cond_4

    move v1, v2

    :goto_2
    if-eqz v1, :cond_6

    iget-object v0, v0, Lcom/google/maps/g/a/ee;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/i;->g()Lcom/google/maps/g/a/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/i;

    iget v1, v0, Lcom/google/maps/g/a/i;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/l;->a(I)Lcom/google/maps/g/a/l;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/maps/g/a/l;->a:Lcom/google/maps/g/a/l;

    :cond_3
    sget-object v4, Lcom/google/maps/g/a/l;->c:Lcom/google/maps/g/a/l;

    if-ne v1, v4, :cond_1

    iget v1, v0, Lcom/google/maps/g/a/i;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v13, :cond_5

    move v1, v2

    :goto_3
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/h;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/maps/g/a/i;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v5, v0, v1, v4}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(Landroid/graphics/drawable/Drawable;FF)Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_3

    :cond_6
    iget v1, v0, Lcom/google/maps/g/a/ee;->a:I

    and-int/lit8 v1, v1, 0x2

    if-ne v1, v13, :cond_7

    move v1, v2

    :goto_4
    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/o;

    iget v4, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v4, v4, 0x1

    if-ne v4, v2, :cond_8

    move v4, v2

    :goto_5
    if-eqz v4, :cond_c

    iget-object v0, v0, Lcom/google/maps/g/a/ee;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/o;->i()Lcom/google/maps/g/a/o;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/o;

    iget v1, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v1, v1, 0x4

    if-ne v1, v12, :cond_9

    move v1, v2

    :goto_6
    if-eqz v1, :cond_b

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "\u00a0"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v8, "\u00a0"

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x0

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v1, v5, v4}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v4

    iget-object v8, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v9, v8, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v10, Landroid/text/style/BackgroundColorSpan;

    invoke-direct {v10, v4}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v8, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget v4, v0, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v4, v4, 0x8

    const/16 v8, 0x8

    if-ne v4, v8, :cond_a

    move v4, v2

    :goto_7
    if-eqz v4, :cond_11

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_11

    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v8, v4, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v9, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    move-object v0, v1

    :goto_8
    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_7
    move v1, v3

    goto/16 :goto_4

    :cond_8
    move v4, v3

    goto/16 :goto_5

    :cond_9
    move v1, v3

    goto/16 :goto_6

    :cond_a
    move v4, v3

    goto :goto_7

    :cond_b
    invoke-virtual {v0}, Lcom/google/maps/g/a/o;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_c
    iget v0, v1, Lcom/google/maps/g/a/o;->a:I

    and-int/lit8 v0, v0, 0x4

    if-ne v0, v12, :cond_d

    move v0, v2

    :goto_9
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/maps/g/a/o;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/g;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/views/h;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    new-instance v4, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v4, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    const v0, 0x3ed55555

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-virtual {v5, v4, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/e;->a(Landroid/graphics/drawable/Drawable;FF)Landroid/text/Spannable;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_d
    move v0, v3

    goto :goto_9

    .line 129
    :cond_e
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 130
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_a
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    .line 131
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    if-eqz v3, :cond_f

    .line 132
    const/16 v3, 0x20

    invoke-virtual {v1, v3}, Landroid/text/SpannableStringBuilder;->append(C)Landroid/text/SpannableStringBuilder;

    .line 134
    :cond_f
    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_a

    .line 136
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/h;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_11
    move-object v0, v1

    goto :goto_8

    .line 127
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
