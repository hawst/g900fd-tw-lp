.class abstract Lcom/google/android/apps/gmm/map/m/t;
.super Lcom/google/android/apps/gmm/map/m/d;
.source "PG"


# instance fields
.field public e:F

.field public f:F

.field public g:F

.field public h:F

.field private final i:I


# direct methods
.method constructor <init>(ILcom/google/android/apps/gmm/map/m/r;FF)V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/apps/gmm/map/m/d;-><init>(Lcom/google/android/apps/gmm/map/m/r;FF)V

    .line 53
    const v0, 0x3f490fdb

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/t;->e:F

    .line 56
    const/high16 v0, 0x3e800000    # 0.25f

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/t;->f:F

    .line 59
    const/high16 v0, 0x3e000000    # 0.125f

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/t;->g:F

    .line 62
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/t;->h:F

    .line 69
    iput p1, p0, Lcom/google/android/apps/gmm/map/m/t;->i:I

    .line 70
    return-void
.end method


# virtual methods
.method protected abstract a(F)F
.end method

.method protected abstract a(Lcom/google/android/apps/gmm/map/m/k;I)F
.end method

.method public a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/map/m/k;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")",
            "Lcom/google/android/apps/gmm/map/m/e;"
        }
    .end annotation

    .prologue
    .line 97
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 101
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->b:Lcom/google/android/apps/gmm/map/m/e;

    .line 200
    :goto_0
    return-object v0

    .line 104
    :cond_0
    const/4 v5, 0x0

    .line 105
    const/4 v4, 0x0

    .line 106
    const/4 v3, 0x0

    .line 107
    const/4 v2, 0x0

    .line 110
    invoke-virtual {p3}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    .line 111
    iget v1, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    iget v6, p0, Lcom/google/android/apps/gmm/map/m/t;->i:I

    if-eq v1, v6, :cond_1

    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {p3}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {p3, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v8

    .line 122
    const/4 v1, 0x0

    move v6, v4

    move v7, v5

    move v4, v2

    move v5, v3

    move-object v2, v0

    move-object v3, v1

    .line 124
    :goto_1
    invoke-interface {v8}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 125
    invoke-interface {v8}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/m/k;

    .line 126
    iget v9, v1, Lcom/google/android/apps/gmm/map/m/k;->e:I

    iget v10, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    if-ne v9, v10, :cond_4

    .line 131
    iget v2, v1, Lcom/google/android/apps/gmm/map/m/k;->b:F

    .line 132
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/map/m/t;->a(F)F

    move-result v2

    .line 135
    iget v9, p0, Lcom/google/android/apps/gmm/map/m/t;->e:F

    cmpl-float v2, v2, v9

    if-ltz v2, :cond_2

    .line 140
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0

    .line 145
    :cond_2
    iget v2, v1, Lcom/google/android/apps/gmm/map/m/k;->c:F

    iget v9, p0, Lcom/google/android/apps/gmm/map/m/t;->i:I

    add-int/lit8 v9, v9, -0x1

    int-to-float v9, v9

    mul-float/2addr v2, v9

    iget v9, p0, Lcom/google/android/apps/gmm/map/m/t;->c:F

    div-float/2addr v2, v9

    .line 146
    iget v9, p0, Lcom/google/android/apps/gmm/map/m/t;->f:F

    cmpg-float v2, v2, v9

    if-gez v2, :cond_3

    .line 151
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0

    .line 155
    :cond_3
    if-eqz v3, :cond_8

    .line 156
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/map/m/t;->a(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v2

    const/4 v9, 0x0

    invoke-virtual {p0, v3, v9}, Lcom/google/android/apps/gmm/map/m/t;->a(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v9

    sub-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v7, v2

    .line 157
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v2

    const/4 v9, 0x0

    invoke-virtual {p0, v3, v9}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v9

    sub-float/2addr v2, v9

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v5, v2

    .line 158
    iget v2, v1, Lcom/google/android/apps/gmm/map/m/k;->e:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/map/m/t;->a(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v2

    iget v9, v3, Lcom/google/android/apps/gmm/map/m/k;->e:I

    add-int/lit8 v9, v9, -0x1

    .line 159
    invoke-virtual {p0, v3, v9}, Lcom/google/android/apps/gmm/map/m/t;->a(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v9

    sub-float/2addr v2, v9

    .line 158
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v6, v2

    .line 160
    iget v2, v1, Lcom/google/android/apps/gmm/map/m/k;->e:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v2

    iget v9, v3, Lcom/google/android/apps/gmm/map/m/k;->e:I

    add-int/lit8 v9, v9, -0x1

    .line 161
    invoke-virtual {p0, v3, v9}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v3

    sub-float/2addr v2, v3

    .line 160
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    add-float/2addr v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    :goto_2
    move v6, v4

    move v7, v5

    move v4, v2

    move v5, v3

    move-object v2, v1

    move-object v3, v1

    .line 164
    goto/16 :goto_1

    .line 167
    :cond_4
    add-float v1, v7, v6

    add-float v3, v5, v4

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/t;->h:F

    mul-float/2addr v3, v4

    cmpl-float v1, v1, v3

    if-lez v1, :cond_5

    .line 171
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto/16 :goto_0

    .line 175
    :cond_5
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v1

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v3

    sub-float/2addr v1, v3

    .line 176
    iget v3, v0, Lcom/google/android/apps/gmm/map/m/k;->e:I

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {p0, v0, v3}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v0

    iget v3, v2, Lcom/google/android/apps/gmm/map/m/k;->e:I

    add-int/lit8 v3, v3, -0x1

    .line 177
    invoke-virtual {p0, v2, v3}, Lcom/google/android/apps/gmm/map/m/t;->b(Lcom/google/android/apps/gmm/map/m/k;I)F

    move-result v2

    sub-float/2addr v0, v2

    .line 180
    mul-float v2, v1, v0

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_6

    .line 184
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto/16 :goto_0

    .line 189
    :cond_6
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/m/t;->d:F

    div-float/2addr v1, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v2, p0, Lcom/google/android/apps/gmm/map/m/t;->d:F

    div-float/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 190
    iget v1, p0, Lcom/google/android/apps/gmm/map/m/t;->g:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_7

    .line 194
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->b:Lcom/google/android/apps/gmm/map/m/e;

    goto/16 :goto_0

    .line 200
    :cond_7
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->c:Lcom/google/android/apps/gmm/map/m/e;

    goto/16 :goto_0

    :cond_8
    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    goto :goto_2
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract b(Lcom/google/android/apps/gmm/map/m/k;I)F
.end method
