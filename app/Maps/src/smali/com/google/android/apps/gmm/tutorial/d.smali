.class public Lcom/google/android/apps/gmm/tutorial/d;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/tutorial/a/a;


# instance fields
.field a:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:Lcom/google/android/apps/gmm/tutorial/i;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->c:Z

    return-void
.end method

.method private a(IILjava/util/List;Lcom/google/android/apps/gmm/tutorial/b;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/apps/gmm/tutorial/b;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 307
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 308
    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 310
    if-nez v0, :cond_1

    .line 311
    invoke-virtual {v2, p2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;

    move-object v1, v0

    .line 315
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 316
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 317
    const/4 v5, -0x1

    if-eq v0, v5, :cond_0

    .line 318
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_0

    .line 320
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 313
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;

    move-object v1, v0

    goto :goto_0

    .line 324
    :cond_2
    invoke-virtual {v1, v3, p4}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->a(Ljava/util/List;Lcom/google/android/apps/gmm/tutorial/b;)V

    .line 325
    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/tutorial/BaseTutorialView;->setVisibility(I)V

    .line 326
    sget v0, Lcom/google/android/apps/gmm/g;->eo:I

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 327
    iput-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    .line 328
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/tutorial/d;)Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/tutorial/d;)Z
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    return v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 334
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 335
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 337
    sget v0, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 338
    invoke-direct {p0}, Lcom/google/android/apps/gmm/tutorial/d;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/g;->dH:I

    .line 340
    :goto_0
    new-instance v2, Lcom/google/android/apps/gmm/tutorial/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/tutorial/e;-><init>(Lcom/google/android/apps/gmm/tutorial/d;)V

    .line 353
    sget v3, Lcom/google/android/apps/gmm/g;->dF:I

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/google/android/apps/gmm/tutorial/d;->a(IILjava/util/List;Lcom/google/android/apps/gmm/tutorial/b;)V

    .line 355
    return-void

    .line 339
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/g;->dG:I

    goto :goto_0
.end method

.method private k()V
    .locals 4

    .prologue
    .line 361
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 362
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 364
    sget v0, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    invoke-direct {p0}, Lcom/google/android/apps/gmm/tutorial/d;->l()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 366
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/g;->cz:I

    .line 367
    :goto_0
    new-instance v2, Lcom/google/android/apps/gmm/tutorial/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/tutorial/f;-><init>(Lcom/google/android/apps/gmm/tutorial/d;)V

    .line 380
    sget v3, Lcom/google/android/apps/gmm/g;->cx:I

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/google/android/apps/gmm/tutorial/d;->a(IILjava/util/List;Lcom/google/android/apps/gmm/tutorial/b;)V

    .line 382
    return-void

    .line 366
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/g;->cy:I

    goto :goto_0
.end method

.method private l()I
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    .line 497
    instance-of v1, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    if-eqz v1, :cond_0

    .line 498
    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->m:Landroid/view/View;

    .line 499
    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    .line 503
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final Y_()V
    .locals 1

    .prologue
    .line 288
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->Y_()V

    .line 289
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 290
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/activities/ak;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 243
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->an:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-nez v0, :cond_0

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-eqz v0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 243
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->e:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-lez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->an:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->an:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    :cond_2
    const/4 v0, -0x1

    sget v1, Lcom/google/android/apps/gmm/g;->dd:I

    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/base/f/bp;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v0

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget v0, Lcom/google/android/apps/gmm/g;->dd:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/apps/gmm/g;->bo:I

    :goto_2
    new-instance v2, Lcom/google/android/apps/gmm/tutorial/g;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/tutorial/g;-><init>(Lcom/google/android/apps/gmm/tutorial/d;)V

    sget v3, Lcom/google/android/apps/gmm/g;->bm:I

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/google/android/apps/gmm/tutorial/d;->a(IILjava/util/List;Lcom/google/android/apps/gmm/tutorial/b;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    sget v0, Lcom/google/android/apps/gmm/g;->bn:I

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 282
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/j/c;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 283
    new-instance v1, Lcom/google/android/apps/gmm/tutorial/i;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/tutorial/i;-><init>(Lcom/google/android/apps/gmm/shared/b/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    .line 284
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/layout/v;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->e()Z

    .line 264
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->d()V

    .line 265
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/base/placelists/u;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->e()Z

    .line 272
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->d()V

    .line 273
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/iamhere/t;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->e()Z

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->am:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    const/4 v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->am:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    .line 150
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/mylocation/e/c;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->c:Z

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->am:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-nez v1, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-eqz v0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ap:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    add-int/lit8 v0, v0, 0x1

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The count must be >= 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, v1, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ap:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ap:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->am:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    const/4 v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->am:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/apps/gmm/g;->C:I

    :goto_1
    new-instance v2, Lcom/google/android/apps/gmm/tutorial/h;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/tutorial/h;-><init>(Lcom/google/android/apps/gmm/tutorial/d;)V

    sget v3, Lcom/google/android/apps/gmm/g;->A:I

    invoke-direct {p0, v0, v3, v1, v2}, Lcom/google/android/apps/gmm/tutorial/d;->a(IILjava/util/List;Lcom/google/android/apps/gmm/tutorial/b;)V

    goto :goto_0

    :cond_4
    sget v0, Lcom/google/android/apps/gmm/g;->B:I

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/o/a/d;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 249
    iget-object v0, p1, Lcom/google/android/apps/gmm/o/a/d;->a:Lcom/google/android/apps/gmm/o/a/g;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/g;->b:Lcom/google/android/apps/gmm/o/a/g;

    if-ne v0, v1, :cond_0

    .line 250
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->e()Z

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->an:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    const/4 v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->an:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    .line 253
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/place/ao;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 257
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->e()Z

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->c()V

    .line 259
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/place/j/b;)V
    .locals 0
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/tutorial/d;->e()Z

    .line 278
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/place/p;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 125
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->at:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-nez v0, :cond_3

    iget v0, p1, Lcom/google/android/apps/gmm/place/p;->a:I

    if-ge v0, v4, :cond_8

    .line 126
    :cond_3
    :goto_1
    return-void

    .line 124
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->as:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The count must be >= 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->as:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->as:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-le v0, v4, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    goto :goto_0

    :cond_6
    if-eq v0, v5, :cond_7

    if-ne v0, v4, :cond_1

    :cond_7
    invoke-direct {p0}, Lcom/google/android/apps/gmm/tutorial/d;->k()V

    goto :goto_0

    .line 125
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->as:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    if-gez v1, :cond_9

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The count must be >= 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->as:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->as:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/google/android/apps/gmm/tutorial/d;->j()V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/search/h;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_2

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aq:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    add-int/lit8 v0, v0, 0x1

    if-gez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The count must be >= 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, v1, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->aq:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;I)V

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-eqz v0, :cond_7

    .line 115
    :cond_4
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v0, :cond_6

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->at:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->f:Z

    if-eqz v0, :cond_9

    .line 116
    :cond_6
    :goto_1
    return-void

    .line 114
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aq:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-le v0, v4, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    goto :goto_0

    :cond_8
    if-ne v0, v4, :cond_4

    invoke-direct {p0}, Lcom/google/android/apps/gmm/tutorial/d;->k()V

    goto :goto_0

    .line 115
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aq:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    invoke-direct {p0}, Lcom/google/android/apps/gmm/tutorial/d;->j()V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 491
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/tutorial/d;->c:Z

    .line 492
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 294
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/j/c;->b()V

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 296
    return-void
.end method

.method c()V
    .locals 3

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->at:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    const/4 v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->at:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    .line 227
    :cond_0
    return-void
.end method

.method d()V
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->b:Lcom/google/android/apps/gmm/tutorial/i;

    const/4 v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/tutorial/i;->a:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->ar:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;Z)V

    .line 233
    :cond_0
    return-void
.end method

.method public final e()Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->eo:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 465
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    .line 466
    const/4 v0, 0x1

    .line 468
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 473
    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 474
    iget-object v1, p0, Lcom/google/android/apps/gmm/tutorial/d;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 476
    :cond_0
    return v0
.end method
