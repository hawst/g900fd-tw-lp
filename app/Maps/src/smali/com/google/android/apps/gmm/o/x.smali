.class public Lcom/google/android/apps/gmm/o/x;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/o/v;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field final b:Lcom/google/android/apps/gmm/util/b;

.field final c:Lcom/google/android/apps/gmm/base/j/b;

.field final d:Lcom/google/android/apps/gmm/o/g;

.field final e:Lcom/google/android/apps/gmm/o/a/c;

.field f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation
.end field

.field g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation
.end field

.field final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation
.end field

.field i:Lcom/google/o/h/a/gx;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/o/w;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/util/b;Lcom/google/android/apps/gmm/base/j/b;Lcom/google/android/apps/gmm/o/g;Lcom/google/android/apps/gmm/o/a/c;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 194
    iput-object p2, p0, Lcom/google/android/apps/gmm/o/x;->b:Lcom/google/android/apps/gmm/util/b;

    .line 195
    iput-object p3, p0, Lcom/google/android/apps/gmm/o/x;->c:Lcom/google/android/apps/gmm/base/j/b;

    .line 196
    iput-object p4, p0, Lcom/google/android/apps/gmm/o/x;->d:Lcom/google/android/apps/gmm/o/g;

    .line 197
    iput-object p5, p0, Lcom/google/android/apps/gmm/o/x;->e:Lcom/google/android/apps/gmm/o/a/c;

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/o/x;->f()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/x;->f:Ljava/util/List;

    .line 199
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/x;->j:Ljava/util/List;

    .line 200
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/x;->g:Ljava/util/List;

    .line 201
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v7

    new-instance v0, Lcom/google/android/apps/gmm/o/ae;

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->a:Lcom/google/android/apps/gmm/o/a/a;

    sget v3, Lcom/google/android/apps/gmm/f;->fr:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->hF:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/gmm/g;->eb:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/o/ae;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/apps/gmm/o/a/a;ILjava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;I)V

    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    new-instance v0, Lcom/google/android/apps/gmm/o/ae;

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    sget v3, Lcom/google/android/apps/gmm/f;->fs:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->hD:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/gmm/g;->ec:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/o/ae;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/apps/gmm/o/a/a;ILjava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;I)V

    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    new-instance v0, Lcom/google/android/apps/gmm/o/ae;

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    sget v3, Lcom/google/android/apps/gmm/f;->cG:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->hB:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/gmm/g;->z:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/o/ae;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/apps/gmm/o/a/a;ILjava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;I)V

    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    new-instance v0, Lcom/google/android/apps/gmm/o/ae;

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    sget v3, Lcom/google/android/apps/gmm/f;->eK:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->hE:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/gmm/g;->cW:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/o/ae;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/apps/gmm/o/a/a;ILjava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;I)V

    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    new-instance v0, Lcom/google/android/apps/gmm/o/ae;

    sget-object v2, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    sget v3, Lcom/google/android/apps/gmm/f;->fl:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->hA:I

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v6, Lcom/google/android/apps/gmm/g;->dK:I

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/o/ae;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/apps/gmm/o/a/a;ILjava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;I)V

    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    invoke-virtual {v7}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/x;->k:Ljava/util/List;

    .line 202
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/o/aa;

    sget v2, Lcom/google/android/apps/gmm/f;->dl:I

    sget v3, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v4, Lcom/google/android/apps/gmm/l;->hC:I

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    new-array v6, v9, [Lcom/google/b/f/cq;

    sget-object v7, Lcom/google/b/f/t;->ft:Lcom/google/b/f/t;

    aput-object v7, v6, v8

    iput-object v6, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v6

    sget v2, Lcom/google/android/apps/gmm/f;->dd:I

    sget v7, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-static {v7}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v7

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/o/aa;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/libraries/curvular/aw;)V

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/x;->l:Ljava/util/List;

    .line 203
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/o/ab;

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->mW:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v9, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->bn:Lcom/google/b/f/t;

    aput-object v6, v4, v8

    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    invoke-direct {v1, p0, v5, v2, v3}, Lcom/google/android/apps/gmm/o/ab;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;)V

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    new-instance v1, Lcom/google/android/apps/gmm/o/ac;

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->gT:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v9, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->fO:Lcom/google/b/f/t;

    aput-object v6, v4, v8

    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    invoke-direct {v1, p0, v5, v2, v3}, Lcom/google/android/apps/gmm/o/ac;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;)V

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    new-instance v1, Lcom/google/android/apps/gmm/o/ad;

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->mT:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v9, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->fP:Lcom/google/b/f/t;

    aput-object v6, v4, v8

    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    invoke-direct {v1, p0, v5, v2, v3}, Lcom/google/android/apps/gmm/o/ad;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;)V

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/x;->m:Ljava/util/List;

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/o/x;->h:Ljava/util/List;

    .line 205
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation

    .prologue
    .line 487
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/x;->f:Ljava/util/List;

    return-object v0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/o/w;",
            ">;"
        }
    .end annotation

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/x;->k:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/x;->l:Ljava/util/List;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/x;->m:Ljava/util/List;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation

    .prologue
    .line 517
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/x;->h:Ljava/util/List;

    return-object v0
.end method

.method f()Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/an;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 209
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v7

    .line 211
    new-instance v0, Lcom/google/android/apps/gmm/o/y;

    sget v1, Lcom/google/android/apps/gmm/f;->eE:I

    .line 212
    sget v2, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/x;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->pl:I

    .line 213
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 214
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    new-array v4, v6, [Lcom/google/b/f/cq;

    sget-object v5, Lcom/google/b/f/t;->fo:Lcom/google/b/f/t;

    aput-object v5, v4, v8

    .line 215
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 216
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/apps/gmm/o/y;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;)V

    .line 211
    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/o/x;->i:Lcom/google/o/h/a/gx;

    if-eqz v0, :cond_1

    .line 242
    new-instance v0, Lcom/google/android/apps/gmm/o/z;

    sget v1, Lcom/google/android/apps/gmm/f;->df:I

    .line 243
    sget v2, Lcom/google/android/apps/gmm/d;->ar:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/c;->b(ILcom/google/android/libraries/curvular/aq;)Lcom/google/android/libraries/curvular/aw;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/o/x;->i:Lcom/google/o/h/a/gx;

    .line 244
    iget-object v1, v4, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_2

    check-cast v1, Ljava/lang/String;

    move-object v3, v1

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/o/x;->i:Lcom/google/o/h/a/gx;

    .line 245
    invoke-virtual {v1}, Lcom/google/o/h/a/gx;->d()Ljava/lang/String;

    move-result-object v4

    .line 246
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    new-array v5, v6, [Lcom/google/b/f/cq;

    sget-object v6, Lcom/google/b/f/t;->fs:Lcom/google/b/f/t;

    aput-object v6, v5, v8

    .line 247
    iput-object v5, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 248
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/o/z;-><init>(Lcom/google/android/apps/gmm/o/x;Lcom/google/android/libraries/curvular/aw;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/z/b/l;Lcom/google/android/libraries/curvular/aw;)V

    .line 242
    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 258
    :cond_1
    invoke-virtual {v7}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0

    .line 244
    :cond_2
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iput-object v3, v4, Lcom/google/o/h/a/gx;->f:Ljava/lang/Object;

    goto :goto_0
.end method
