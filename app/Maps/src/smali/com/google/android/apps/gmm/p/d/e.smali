.class public Lcom/google/android/apps/gmm/p/d/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:[J

.field static final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0xe

    .line 28
    new-array v0, v1, [J

    fill-array-data v0, :array_0

    .line 37
    sput-object v0, Lcom/google/android/apps/gmm/p/d/e;->a:[J

    sput v1, Lcom/google/android/apps/gmm/p/d/e;->b:I

    return-void

    .line 28
    :array_0
    .array-data 8
        0x1
        0x1
        0x4
        0x24
        0x240
        0x3840
        0x7e900
        0x1839900
        0x60e64000
        0x1ea8da4000L
        0xbf9f5410000L
        0x5a924ebb90000L
        0x32f24c498100000L
        0x1a1f45c862900000L    # 7.359807927867385E-183
    .end array-data
.end method

.method public static a(DDD)D
    .locals 14

    .prologue
    .line 55
    const-wide/16 v0, 0x0

    cmpg-double v0, p0, v0

    if-gtz v0, :cond_0

    .line 56
    const-wide/16 v0, 0x0

    .line 80
    :goto_0
    return-wide v0

    .line 66
    :cond_0
    div-double v0, p2, p4

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 67
    invoke-static/range {p0 .. p5}, Lcom/google/android/apps/gmm/p/d/d;->a(DDD)D

    move-result-wide v0

    goto :goto_0

    .line 72
    :cond_1
    sub-double v0, p2, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4010000000000000L    # 4.0

    mul-double v2, v2, p4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 73
    invoke-static/range {p0 .. p5}, Lcom/google/android/apps/gmm/p/d/d;->a(DDD)D

    move-result-wide v0

    goto :goto_0

    .line 77
    :cond_2
    mul-double v0, p4, p4

    div-double v4, p0, v0

    .line 78
    mul-double v0, p0, p0

    mul-double v2, p2, p2

    add-double/2addr v0, v2

    neg-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double v6, p4, p4

    mul-double/2addr v2, v6

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v6

    .line 79
    mul-double v0, p0, p2

    mul-double v2, p4, p4

    div-double v8, v0, v2

    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    :goto_1
    sget v1, Lcom/google/android/apps/gmm/p/d/e;->b:I

    if-ge v0, v1, :cond_3

    const-wide/high16 v10, 0x3fd0000000000000L    # 0.25

    mul-double v12, v8, v8

    mul-double/2addr v10, v12

    int-to-double v12, v0

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v10

    sget-object v1, Lcom/google/android/apps/gmm/p/d/e;->a:[J

    aget-wide v12, v1, v0

    long-to-double v12, v12

    div-double/2addr v10, v12

    add-double/2addr v2, v10

    const-wide v12, 0x3f50624dd2f1a9fcL    # 0.001

    cmpg-double v1, v10, v12

    if-ltz v1, :cond_3

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 80
    :cond_3
    mul-double v0, v4, v6

    mul-double/2addr v0, v2

    goto :goto_0
.end method
