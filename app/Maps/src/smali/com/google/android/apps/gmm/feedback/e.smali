.class Lcom/google/android/apps/gmm/feedback/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/feedback/a/g;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/base/activities/c;

.field b:Lcom/google/android/apps/gmm/feedback/g;

.field final c:Z

.field final d:Lcom/google/android/apps/gmm/feedback/a/d;

.field private final f:Z

.field private final g:Lcom/google/android/apps/gmm/feedback/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/google/android/apps/gmm/feedback/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/feedback/e;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;ZZLcom/google/android/apps/gmm/feedback/a/a;Lcom/google/android/apps/gmm/feedback/a/d;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 147
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/feedback/e;->c:Z

    .line 148
    iput-object p4, p0, Lcom/google/android/apps/gmm/feedback/e;->g:Lcom/google/android/apps/gmm/feedback/a/a;

    .line 149
    iput-object p5, p0, Lcom/google/android/apps/gmm/feedback/e;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 150
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/feedback/e;->f:Z

    .line 152
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LocationState["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "gps = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    invoke-static {v2}, Lcom/google/android/apps/gmm/p/b/b;->a(Lcom/google/android/apps/gmm/p/b/d;)Lcom/google/r/b/a/jl;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", cell = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    invoke-static {v2}, Lcom/google/android/apps/gmm/p/b/b;->a(Lcom/google/android/apps/gmm/p/b/d;)Lcom/google/r/b/a/jl;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", wifi = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    invoke-static {v0}, Lcom/google/android/apps/gmm/p/b/b;->a(Lcom/google/android/apps/gmm/p/b/d;)Lcom/google/r/b/a/jl;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->h:Ljava/lang/String;

    .line 154
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->f()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    .line 155
    if-nez v0, :cond_0

    const-string v0, "unknown"

    .line 156
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->i:Ljava/lang/String;

    .line 158
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    .line 157
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->b(Landroid/location/Location;)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->j:Ljava/lang/String;

    .line 159
    iput-object p6, p0, Lcom/google/android/apps/gmm/feedback/e;->k:Ljava/lang/String;

    .line 160
    return-void

    .line 156
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/location/LocationStatus;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 427
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 428
    invoke-virtual {p0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 430
    :cond_1
    return-void

    .line 427
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcom/google/userfeedback/android/api/UserFeedbackSpec;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 382
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 387
    :goto_1
    return-void

    .line 382
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 386
    :cond_2
    const-string v0, "text/plain"

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/userfeedback/android/api/UserFeedbackSpec;->addProductSpecificBinaryData(Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/userfeedback/android/api/UserFeedbackSpec;

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v3, 0x0

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v1, v3

    .line 173
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v2

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 179
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->H()Lcom/google/r/b/a/dr;

    move-result-object v7

    .line 181
    iget-object v4, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->x_()Lcom/google/android/apps/gmm/util/replay/a;

    move-result-object v5

    if-nez v5, :cond_1

    move-object v4, v3

    .line 188
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/e;->f:Z

    if-eqz v0, :cond_5

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0, p0}, Lcom/google/android/apps/gmm/feedback/v;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/feedback/a/g;)Lcom/google/android/apps/gmm/feedback/a/f;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 198
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/feedback/g;

    iget-object v5, p0, Lcom/google/android/apps/gmm/feedback/e;->g:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v6, p0, Lcom/google/android/apps/gmm/feedback/e;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    iget-object v8, p0, Lcom/google/android/apps/gmm/feedback/e;->h:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/gmm/feedback/e;->i:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/gmm/feedback/e;->j:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/apps/gmm/feedback/e;->k:Ljava/lang/String;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/feedback/g;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;Ljava/lang/String;Lcom/google/android/apps/gmm/feedback/a/f;Ljava/lang/String;Lcom/google/android/apps/gmm/feedback/a/a;Lcom/google/android/apps/gmm/feedback/a/d;Lcom/google/r/b/a/dr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    .line 210
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/feedback/e;->c:Z

    if-eqz v0, :cond_4

    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->d:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, p0}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 233
    :goto_3
    return-void

    .line 169
    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    goto :goto_0

    .line 181
    :cond_1
    invoke-static {v4}, Lcom/google/android/apps/gmm/util/replay/b;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v3

    :goto_4
    if-nez v0, :cond_3

    move-object v4, v3

    goto :goto_1

    :cond_2
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "event-track-"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6}, Ljava/util/Date;-><init>()V

    new-instance v8, Ljava/text/SimpleDateFormat;

    const-string v9, "yyyy-MM-dd_kk.mm.ss"

    sget-object v10, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v8, v9, v10}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-virtual {v8, v6}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ".xml"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/io/File;

    invoke-direct {v6, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_3
    invoke-interface {v5, v0}, Lcom/google/android/apps/gmm/util/replay/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 196
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/feedback/e;->e:Ljava/lang/String;

    goto :goto_2

    .line 214
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->f:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, p0}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_3

    .line 219
    :cond_5
    new-instance v0, Lcom/google/android/apps/gmm/feedback/g;

    iget-object v5, p0, Lcom/google/android/apps/gmm/feedback/e;->g:Lcom/google/android/apps/gmm/feedback/a/a;

    iget-object v6, p0, Lcom/google/android/apps/gmm/feedback/e;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    iget-object v8, p0, Lcom/google/android/apps/gmm/feedback/e;->h:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/apps/gmm/feedback/e;->i:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/apps/gmm/feedback/e;->j:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/apps/gmm/feedback/e;->k:Ljava/lang/String;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/feedback/g;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;Ljava/lang/String;Lcom/google/android/apps/gmm/feedback/a/f;Ljava/lang/String;Lcom/google/android/apps/gmm/feedback/a/a;Lcom/google/android/apps/gmm/feedback/a/d;Lcom/google/r/b/a/dr;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->b:Lcom/google/android/apps/gmm/feedback/g;

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->c:Lcom/google/android/apps/gmm/feedback/i;

    invoke-direct {v1, v2, p0}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto/16 :goto_3
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1    # Landroid/graphics/Bitmap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/e;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/feedback/h;

    sget-object v2, Lcom/google/android/apps/gmm/feedback/i;->b:Lcom/google/android/apps/gmm/feedback/i;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/feedback/h;-><init>(Lcom/google/android/apps/gmm/feedback/i;Lcom/google/android/apps/gmm/feedback/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 254
    return-void
.end method
