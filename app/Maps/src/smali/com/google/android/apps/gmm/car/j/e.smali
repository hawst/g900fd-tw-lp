.class public Lcom/google/android/apps/gmm/car/j/e;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I

.field static final b:Ljava/lang/String;


# instance fields
.field public final c:Lcom/google/android/apps/gmm/car/ad;

.field final d:Landroid/view/ViewGroup;

.field final e:Lcom/google/android/apps/gmm/car/j/h;

.field public final f:Landroid/os/Handler;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field i:Lcom/google/android/apps/gmm/car/j/l;

.field j:I

.field public final k:Ljava/lang/Runnable;

.field private final l:Landroid/view/View;

.field private final m:Landroid/view/View$OnTouchListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/j/e;->a:I

    .line 38
    const-class v0, Lcom/google/android/apps/gmm/car/j/e;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Landroid/view/ViewGroup;Lcom/google/android/apps/gmm/car/j/h;)V
    .locals 8
    .param p3    # Lcom/google/android/apps/gmm/car/j/h;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->f:Landroid/os/Handler;

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/car/j/f;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/j/f;-><init>(Lcom/google/android/apps/gmm/car/j/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->m:Landroid/view/View$OnTouchListener;

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/car/j/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/j/g;-><init>(Lcom/google/android/apps/gmm/car/j/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->k:Ljava/lang/Runnable;

    .line 59
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 60
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->d:Landroid/view/ViewGroup;

    .line 61
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 62
    if-nez p3, :cond_7

    .line 63
    new-instance v1, Lcom/google/android/apps/gmm/car/j/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/car/j/h;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    .line 64
    invoke-static {v0}, Lcom/google/android/apps/gmm/car/j/k;->a(Lcom/google/android/apps/gmm/base/a;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/car/j/k;->a(Lcom/google/android/apps/gmm/car/j/h;Lcom/google/android/apps/gmm/base/a;)V

    .line 75
    :goto_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    .line 76
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->u:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v0, "fake_my_location_disabled"

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->g:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/h;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    .line 79
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/h;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    const-string v2, "%.7f,%.7f"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 81
    :goto_1
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->u:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 82
    :cond_3
    iget-object v2, p1, Lcom/google/android/apps/gmm/car/ad;->i:Lcom/google/android/apps/gmm/mylocation/b/f;

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Ljava/lang/String;)V

    .line 85
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->x:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v0, "0"

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->h:Ljava/lang/String;

    .line 87
    sget-object v0, Lcom/google/android/apps/gmm/shared/b/c;->x:Lcom/google/android/apps/gmm/shared/b/c;

    const-string v2, "%.7f"

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    .line 88
    iget-wide v4, v4, Lcom/google/android/apps/gmm/car/j/h;->f:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 87
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 93
    :cond_5
    new-instance v0, Landroid/view/View;

    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->l:Landroid/view/View;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->l:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/car/j/e;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/e;->m:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->l:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 99
    sget-object v0, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    .line 104
    :goto_2
    return-void

    .line 67
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    .line 68
    invoke-static {v0}, Lcom/google/android/apps/gmm/car/j/a;->a(Lcom/google/android/apps/gmm/base/a;)Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/car/j/k;->a(Lcom/google/android/apps/gmm/car/j/h;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 71
    :cond_7
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    goto/16 :goto_0

    .line 79
    :cond_8
    const-string v0, "fake_my_location_disabled"

    goto/16 :goto_1

    .line 103
    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/e;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/e;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2
.end method

.method public static a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/gms/common/api/o;)Z
    .locals 2
    .param p1    # Lcom/google/android/gms/common/api/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 123
    invoke-static {p0}, Lcom/google/android/apps/gmm/car/j/k;->a(Lcom/google/android/apps/gmm/base/a;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 125
    :cond_1
    invoke-static {p1}, Lcom/google/android/apps/gmm/car/j/a;->a(Lcom/google/android/gms/common/api/o;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 128
    const/4 v0, 0x0

    goto :goto_0
.end method
