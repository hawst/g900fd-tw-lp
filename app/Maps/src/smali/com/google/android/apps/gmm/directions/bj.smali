.class public Lcom/google/android/apps/gmm/directions/bj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field c:Lcom/google/android/apps/gmm/shared/c/f;

.field final d:[Lcom/google/android/apps/gmm/map/r/a/w;

.field e:J

.field f:Ljava/util/concurrent/ScheduledExecutorService;

.field private final g:Lcom/google/android/apps/gmm/directions/bl;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/android/apps/gmm/directions/bj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/bj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;[Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/directions/bl;)V
    .locals 1
    .param p3    # Lcom/google/android/apps/gmm/directions/bl;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bj;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 68
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/bj;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 69
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/bj;->d:[Lcom/google/android/apps/gmm/map/r/a/w;

    .line 70
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/bj;->g:Lcom/google/android/apps/gmm/directions/bl;

    .line 71
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/directions/b/d;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 76
    iget-object v2, p1, Lcom/google/android/apps/gmm/directions/b/d;->a:Ljava/util/Map;

    .line 79
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bj;->d:[Lcom/google/android/apps/gmm/map/r/a/w;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bj;->d:[Lcom/google/android/apps/gmm/map/r/a/w;

    aget-object v3, v0, v1

    .line 81
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/fw;

    .line 83
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/maps/g/a/fw;)V

    .line 79
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bj;->g:Lcom/google/android/apps/gmm/directions/bl;

    if-eqz v0, :cond_2

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bj;->g:Lcom/google/android/apps/gmm/directions/bl;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/bl;->c()V

    .line 91
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bj;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/directions/bj;->e:J

    .line 92
    return-void
.end method
