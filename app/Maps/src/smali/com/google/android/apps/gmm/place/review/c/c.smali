.class public Lcom/google/android/apps/gmm/place/review/c/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/review/c/b;


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Lcom/google/android/apps/gmm/base/views/c/k;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/String;

.field private final e:Lcom/google/android/apps/gmm/place/review/c/e;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/apps/gmm/base/l/a/u;

.field private h:Ljava/lang/Float;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/place/review/c/e;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/c/c;->e:Lcom/google/android/apps/gmm/place/review/c/e;

    .line 38
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/review/c/c;->f:Ljava/lang/String;

    .line 39
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/review/c/c;->h:Ljava/lang/Float;

    .line 40
    iput-object p4, p0, Lcom/google/android/apps/gmm/place/review/c/c;->c:Ljava/lang/CharSequence;

    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/place/review/c/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/review/c/d;-><init>(Lcom/google/android/apps/gmm/place/review/c/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->g:Lcom/google/android/apps/gmm/base/l/a/u;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/c/c;->c:Ljava/lang/CharSequence;

    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Float;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/c/c;->h:Ljava/lang/Float;

    .line 63
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->h:Ljava/lang/Float;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->g:Lcom/google/android/apps/gmm/base/l/a/u;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->a:Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->b:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final g()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final h()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->e:Lcom/google/android/apps/gmm/place/review/c/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/review/c/e;->i()V

    .line 117
    const/4 v0, 0x0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/c/c;->e:Lcom/google/android/apps/gmm/place/review/c/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->c:Ljava/lang/CharSequence;

    .line 123
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->h:Ljava/lang/Float;

    .line 124
    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->a:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 125
    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/place/review/c/c;->d:Ljava/lang/String;

    .line 122
    invoke-interface {v2, v3, v4, v0, v5}, Lcom/google/android/apps/gmm/place/review/c/e;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    .line 127
    return-object v1

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c/c;->a:Ljava/lang/CharSequence;

    .line 125
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
