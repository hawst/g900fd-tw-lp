.class Lcom/google/android/apps/gmm/car/drawer/u;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/drawer/s;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/drawer/t;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/drawer/t;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bJ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/ad;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 136
    sget v0, Lcom/google/android/apps/gmm/car/drawer/t;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    new-instance v1, Lcom/google/android/apps/gmm/car/i/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    .line 145
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v4, v3}, Lcom/google/android/apps/gmm/car/i/o;-><init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;I)V

    .line 144
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/u;->a:Lcom/google/android/apps/gmm/car/drawer/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/t;->d:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/j;->a()V

    .line 148
    return-object v4
.end method
