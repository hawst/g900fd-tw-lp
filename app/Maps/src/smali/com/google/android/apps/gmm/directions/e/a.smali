.class public Lcom/google/android/apps/gmm/directions/e/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/a/d;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/map/c/a;

.field final c:Landroid/content/res/Resources;

.field final d:Lcom/google/android/apps/gmm/map/t;

.field final e:Lcom/google/android/apps/gmm/map/x;

.field f:Lcom/google/android/apps/gmm/map/i/h;

.field g:Lcom/google/android/apps/gmm/map/b/a/r;

.field h:Lcom/google/android/apps/gmm/map/b/a;

.field final i:Lcom/google/android/apps/gmm/directions/e/g;

.field private final j:Lcom/google/android/apps/gmm/directions/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/apps/gmm/directions/e/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/e/a;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/e;Lcom/google/android/apps/gmm/map/x;)V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/directions/e/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/directions/e/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    .line 100
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/e/a;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 101
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/e/a;->c:Landroid/content/res/Resources;

    .line 102
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    .line 103
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/e/a;->j:Lcom/google/android/apps/gmm/directions/a/e;

    .line 104
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/e/a;->e:Lcom/google/android/apps/gmm/map/x;

    .line 105
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/directions/f/a/a;Z)V
    .locals 6

    .prologue
    .line 118
    .line 119
    new-instance v5, Lcom/google/android/apps/gmm/directions/e/b;

    invoke-direct {v5, p0, p1}, Lcom/google/android/apps/gmm/directions/e/b;-><init>(Lcom/google/android/apps/gmm/directions/e/a;Lcom/google/android/apps/gmm/directions/f/a/a;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    monitor-enter v1

    .line 133
    if-nez p2, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/directions/e/g;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/g;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 134
    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/directions/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    monitor-exit v1

    .line 146
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    iput-object p1, v0, Lcom/google/android/apps/gmm/directions/e/g;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    iput-object v5, v0, Lcom/google/android/apps/gmm/directions/e/g;->b:Lcom/google/android/apps/gmm/directions/e/f;

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/e/g;->c:Z

    .line 141
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->b:Lcom/google/android/apps/gmm/map/c/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v4

    .line 144
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/e/a;->b:Lcom/google/android/apps/gmm/map/c/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/e/a;->e:Lcom/google/android/apps/gmm/map/x;

    const-string v0, "request"

    if-nez p1, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 141
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 144
    :cond_1
    const-string v0, "callback"

    if-nez v5, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    const-string v0, "threadPool"

    if-nez v4, :cond_3

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string v0, "drawableIdProvider"

    if-nez v3, :cond_4

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/directions/e/c;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/e/c;-><init>(Lcom/google/android/apps/gmm/directions/f/a/a;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/x;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/directions/e/f;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v4, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/directions/f/a/c;Lcom/google/android/apps/gmm/map/b/a/r;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/t;->e:Z

    if-eqz v3, :cond_1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v0

    .line 283
    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    iget v4, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->a:I

    iget v5, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->b:I

    add-int/2addr v4, v5

    if-le v3, v4, :cond_0

    .line 284
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget v3, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->c:I

    iget v4, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->d:I

    add-int/2addr v3, v4

    if-gt v0, v3, :cond_2

    .line 285
    :cond_0
    const/4 v0, 0x0

    .line 291
    :goto_1
    return v0

    :cond_1
    move-object v0, v1

    .line 282
    goto :goto_0

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    .line 289
    iget v3, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->a:I

    iget v4, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->b:I

    .line 290
    iget v5, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->c:I

    iget v6, p1, Lcom/google/android/apps/gmm/directions/f/a/c;->d:I

    .line 288
    invoke-static {p2, v3, v4, v5, v6}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b/a/r;IIII)Lcom/google/android/apps/gmm/map/a;

    move-result-object v3

    .line 287
    invoke-virtual {v0, v3, v1, v2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    move v0, v2

    .line 291
    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/directions/f/a/a;)V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/e/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;Z)V

    .line 114
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/q;Z)V
    .locals 2

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/e/a;->c()V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    sget-object v1, Lcom/google/android/apps/gmm/map/ag;->a:Lcom/google/android/apps/gmm/map/ag;

    invoke-virtual {v0, p1, v1, p2}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/ag;Z)Lcom/google/android/apps/gmm/map/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->h:Lcom/google/android/apps/gmm/map/b/a;

    .line 216
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/f/a/c;)V
    .locals 6
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/directions/f/a/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 232
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->j:Lcom/google/android/apps/gmm/directions/a/e;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/e;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 235
    sget-object v0, Lcom/google/android/apps/gmm/directions/e/a;->a:Ljava/lang/String;

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    if-eqz p1, :cond_0

    .line 243
    if-nez p2, :cond_2

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->j:Lcom/google/android/apps/gmm/directions/a/e;

    const/4 v1, 0x1

    .line 247
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/directions/a/e;->a(Z)Lcom/google/android/apps/gmm/directions/f/a/c;

    move-result-object p2

    .line 256
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v2, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/ah;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    .line 258
    new-instance v1, Lcom/google/android/apps/gmm/directions/f/a/c;

    .line 259
    iget v2, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->a:I

    iget v3, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->b:I

    .line 260
    iget v4, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->c:I

    add-int/2addr v0, v4

    iget v4, p2, Lcom/google/android/apps/gmm/directions/f/a/c;->d:I

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    .line 268
    invoke-direct {p0, v1, p1}, Lcom/google/android/apps/gmm/directions/e/a;->a(Lcom/google/android/apps/gmm/directions/f/a/c;Lcom/google/android/apps/gmm/map/b/a/r;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->j:Lcom/google/android/apps/gmm/directions/a/e;

    .line 270
    invoke-interface {v0, v5}, Lcom/google/android/apps/gmm/directions/a/e;->a(Z)Lcom/google/android/apps/gmm/directions/f/a/c;

    move-result-object v0

    .line 269
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/directions/e/a;->a(Lcom/google/android/apps/gmm/directions/f/a/c;Lcom/google/android/apps/gmm/map/b/a/r;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-direct {v0, v5, v5, v5, v5}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/directions/e/a;->a(Lcom/google/android/apps/gmm/directions/f/a/c;Lcom/google/android/apps/gmm/map/b/a/r;)Z

    goto :goto_0

    .line 256
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->f:Lcom/google/android/apps/gmm/map/i/h;

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->f:Lcom/google/android/apps/gmm/map/i/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/i/h;->a(Lcom/google/android/apps/gmm/map/r/a/v;Lcom/google/android/apps/gmm/map/o/b/h;)V

    .line 436
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    monitor-enter v1

    .line 327
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/e/g;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 328
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    new-instance v1, Lcom/google/android/apps/gmm/directions/f/a/b;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/directions/f/a/b;-><init>(Lcom/google/android/apps/gmm/directions/f/a/a;)V

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->e:Z

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/directions/f/a/b;->d:Z

    new-instance v0, Lcom/google/android/apps/gmm/directions/f/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/directions/f/a/a;-><init>(Lcom/google/android/apps/gmm/directions/f/a/b;)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/directions/e/a;->a(Lcom/google/android/apps/gmm/directions/f/a/a;Z)V

    .line 333
    :cond_0
    return-void

    .line 328
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 300
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 302
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    if-nez v0, :cond_1

    .line 320
    :goto_1
    return-void

    .line 302
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 306
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    monitor-enter v1

    .line 307
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/e/g;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/directions/e/g;->b:Lcom/google/android/apps/gmm/directions/e/f;

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->i:Lcom/google/android/apps/gmm/directions/e/g;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/e/g;->c:Z

    .line 310
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    if-nez v3, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->n()V

    .line 314
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->h:Lcom/google/android/apps/gmm/map/b/a;

    if-eqz v0, :cond_3

    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/a;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/a;->h:Lcom/google/android/apps/gmm/map/b/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/ah;->a(Lcom/google/android/apps/gmm/map/b/a;)V

    .line 316
    :cond_2
    iput-object v3, p0, Lcom/google/android/apps/gmm/directions/e/a;->h:Lcom/google/android/apps/gmm/map/b/a;

    .line 318
    :cond_3
    iput-object v3, p0, Lcom/google/android/apps/gmm/directions/e/a;->f:Lcom/google/android/apps/gmm/map/i/h;

    .line 319
    iput-object v3, p0, Lcom/google/android/apps/gmm/directions/e/a;->g:Lcom/google/android/apps/gmm/map/b/a/r;

    goto :goto_1

    .line 310
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 312
    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/legacy/internal/b/b;)V

    goto :goto_2
.end method
