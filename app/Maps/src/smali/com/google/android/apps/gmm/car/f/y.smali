.class Lcom/google/android/apps/gmm/car/f/y;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic b:Lcom/google/android/apps/gmm/car/f/p;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/f/p;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/f/y;->b:Lcom/google/android/apps/gmm/car/f/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/apps/gmm/car/f/y;
    .locals 2

    .prologue
    .line 257
    if-eqz p1, :cond_0

    .line 258
    new-instance v0, Lcom/google/android/apps/gmm/car/f/z;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/y;->b:Lcom/google/android/apps/gmm/car/f/p;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/car/f/z;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    .line 260
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/car/f/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/f/y;->b:Lcom/google/android/apps/gmm/car/f/p;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/car/f/v;-><init>(Lcom/google/android/apps/gmm/car/f/p;)V

    goto :goto_0
.end method

.method public a()Lcom/google/android/libraries/curvular/aw;
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 202
    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 221
    const/4 v0, 0x0

    return v0
.end method

.method public d()Lcom/google/android/apps/gmm/car/f/y;
    .locals 0

    .prologue
    .line 237
    return-object p0
.end method

.method public e()Lcom/google/android/apps/gmm/car/f/y;
    .locals 0

    .prologue
    .line 245
    return-object p0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x0

    return v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method public i()Lcom/google/android/apps/gmm/car/f/y;
    .locals 0

    .prologue
    .line 252
    return-object p0
.end method

.method public j()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 206
    const-string v0, ""

    return-object v0
.end method

.method public k()Lcom/google/android/libraries/curvular/aq;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/f/y;->b:Lcom/google/android/apps/gmm/car/f/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/f/p;->b:Lcom/google/android/libraries/curvular/aq;

    return-object v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 275
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "show spinner"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/y;->f()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "status msg id"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/y;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "travel time"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/y;->j()Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "waiting for ready"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/y;->g()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "has action"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/f/y;->c()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
