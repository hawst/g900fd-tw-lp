.class public Lcom/google/android/apps/gmm/map/i/j;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/b;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

.field public b:Lcom/google/android/apps/gmm/map/t/l;

.field public c:Lcom/google/android/apps/gmm/map/t/q;

.field public d:Lcom/google/android/apps/gmm/map/t/w;

.field public final e:Lcom/google/android/apps/gmm/map/r/a/w;

.field public final f:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/as;",
            ">;"
        }
    .end annotation
.end field

.field private j:Z

.field private k:Landroid/content/res/Resources;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/m;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/q;",
            ">;"
        }
    .end annotation
.end field

.field private n:Lcom/google/android/apps/gmm/map/i/l;

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/r/b/a/anw;",
            ">;"
        }
    .end annotation
.end field

.field private final p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/i/m;",
            "Lcom/google/android/apps/gmm/map/o/b/c;",
            ">;"
        }
    .end annotation
.end field

.field private final q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/al;",
            ">;"
        }
    .end annotation
.end field

.field private r:Landroid/content/Context;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/r;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;ZZ)V
    .locals 10

    .prologue
    const/4 v2, 0x4

    const/high16 v5, 0x40900000    # 4.5f

    const/4 v3, 0x3

    const/4 v8, 0x1

    .line 127
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/map/legacy/internal/b/b;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    .line 98
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->p:Ljava/util/Map;

    .line 101
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->q:Ljava/util/List;

    .line 128
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/i/j;->k:Landroid/content/res/Resources;

    .line 129
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/i/j;->j:Z

    .line 130
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/map/t/l;->j:Lcom/google/android/apps/gmm/map/t/l;

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->b:Lcom/google/android/apps/gmm/map/t/l;

    .line 134
    if-eqz p4, :cond_3

    if-eqz p5, :cond_2

    .line 138
    :cond_0
    :goto_1
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v1

    .line 139
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->c()[Lcom/google/android/apps/gmm/map/internal/c/aq;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/i/j;->b:Lcom/google/android/apps/gmm/map/t/l;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/i/j;->h:Lcom/google/android/apps/gmm/v/ad;

    move v9, p4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;II[Lcom/google/android/apps/gmm/map/internal/c/aq;FLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/v/ad;ZZ)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->g:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->i:Lcom/google/android/apps/gmm/map/legacy/internal/b/h;

    invoke-virtual {v1, v8}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(Z)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->d()V

    .line 142
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->d()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->l:Ljava/util/List;

    .line 144
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->e()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->o:Ljava/util/List;

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->i:Ljava/util/List;

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/anw;

    .line 176
    invoke-static {v0, v8}, Lcom/google/android/apps/gmm/map/r/c/a;->a(Lcom/google/r/b/a/anw;Z)Lcom/google/android/apps/gmm/map/internal/c/an;

    move-result-object v0

    .line 178
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/j;->i:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/map/o/as;

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-direct {v3, v0, v4, v6}, Lcom/google/android/apps/gmm/map/o/as;-><init>(Lcom/google/android/apps/gmm/map/internal/c/m;Lcom/google/android/apps/gmm/map/o/ak;Z)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 130
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/t/l;->i:Lcom/google/android/apps/gmm/map/t/l;

    goto :goto_0

    :cond_2
    move v2, v3

    .line 134
    goto :goto_1

    :cond_3
    if-nez p5, :cond_0

    move v2, v3

    goto :goto_1

    .line 181
    :cond_4
    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v5

    .line 182
    new-instance v1, Lcom/google/android/apps/gmm/map/t/w;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/t/w;-><init>(FLcom/google/android/apps/gmm/map/b/a/ab;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->d:Lcom/google/android/apps/gmm/map/t/w;

    .line 184
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->i()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->e:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 185
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->f:Ljava/lang/String;

    .line 186
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/r/a/r;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->g:Ljava/lang/String;

    .line 187
    return-void
.end method

.method public static a(Lcom/google/maps/g/a/er;)B
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 110
    sget-object v1, Lcom/google/android/apps/gmm/map/i/k;->a:[I

    invoke-virtual {p0}, Lcom/google/maps/g/a/er;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 121
    :goto_0
    :pswitch_0
    return v0

    .line 115
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 117
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 119
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->b()V

    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/i/j;->b()V

    .line 305
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 191
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/c/a;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->r:Landroid/content/Context;

    .line 194
    if-eqz p1, :cond_3

    .line 195
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/c/a;)V

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->m:Ljava/util/List;

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/m;

    .line 200
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/i/j;->m:Ljava/util/List;

    new-instance v4, Lcom/google/android/apps/gmm/map/i/e;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/i/j;->k:Landroid/content/res/Resources;

    invoke-direct {v4, p1, v5}, Lcom/google/android/apps/gmm/map/i/e;-><init>(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;)V

    const-string v5, "polyline measle"

    .line 202
    iput-object v5, v4, Lcom/google/android/apps/gmm/map/i/e;->a:Ljava/lang/String;

    sget-object v5, Lcom/google/android/apps/gmm/map/t/l;->q:Lcom/google/android/apps/gmm/map/t/l;

    .line 203
    iput-object v5, v4, Lcom/google/android/apps/gmm/map/i/e;->b:Lcom/google/android/apps/gmm/map/t/k;

    .line 204
    iget v5, v0, Lcom/google/android/apps/gmm/map/r/a/m;->b:I

    iput v5, v4, Lcom/google/android/apps/gmm/map/i/e;->c:I

    .line 205
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/m;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/i/e;->d:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/i/e;->a()Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    .line 200
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/q;

    .line 209
    iget-object v3, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v4, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v4, v0, v6}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_1

    .line 211
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/i/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/j;->m:Ljava/util/List;

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/map/i/j;->j:Z

    invoke-direct {v0, v2, p2, p1, v3}, Lcom/google/android/apps/gmm/map/i/l;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->n:Lcom/google/android/apps/gmm/map/i/l;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->n:Lcom/google/android/apps/gmm/map/i/l;

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v0, v6}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/apps/gmm/map/t/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/j;->b:Lcom/google/android/apps/gmm/map/t/l;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/t/q;-><init>(Lcom/google/android/apps/gmm/map/t/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v0, v6}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 216
    :cond_3
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/am;)V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/am;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 226
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->p:Ljava/util/Map;

    monitor-enter v1

    .line 227
    :try_start_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/o/am;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/j;->q:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 228
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 9

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->r:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/i/j;->p:Ljava/util/Map;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->p:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/o/b/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/j;->r:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/map/o/b/h;->a:Lcom/google/android/apps/gmm/map/o/b/h;

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/map/o/b/e;->a(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/o/b/h;Z)Lcom/google/android/apps/gmm/map/o/b/e;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/i/m;->a:Lcom/google/r/b/a/anw;

    invoke-virtual {v3}, Lcom/google/r/b/a/anw;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/m;->a:Lcom/google/r/b/a/anw;

    invoke-virtual {v0}, Lcom/google/r/b/a/anw;->d()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/map/o/b/c;->a(Lcom/google/android/apps/gmm/map/o/b/e;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/z;

    move-result-object v3

    new-instance v0, Lcom/google/android/apps/gmm/map/o/al;

    iget v4, v1, Lcom/google/android/apps/gmm/map/o/b/c;->b:I

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/o/al;-><init>(Lcom/google/android/apps/gmm/map/o/b/c;Lcom/google/android/apps/gmm/map/o/b/e;Lcom/google/android/apps/gmm/map/internal/c/z;IILcom/google/android/apps/gmm/map/o/ak;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->q:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 235
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/af;)V
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->a:Lcom/google/android/apps/gmm/map/legacy/internal/b/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/b/d;->a(Lcom/google/android/apps/gmm/map/t/af;)V

    .line 299
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->m:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/aa;

    .line 311
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/i/j;->h:Lcom/google/android/apps/gmm/v/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v3, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    goto :goto_0

    .line 313
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 315
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->n:Lcom/google/android/apps/gmm/map/i/l;

    if-eqz v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->h:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->n:Lcom/google/android/apps/gmm/map/i/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v1, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 317
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->n:Lcom/google/android/apps/gmm/map/i/l;

    .line 320
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v0, :cond_3

    .line 321
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->h:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->c:Lcom/google/android/apps/gmm/map/t/q;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v1, v4}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 323
    :cond_3
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/map/o/p;)V
    .locals 2

    .prologue
    .line 239
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/i/j;->p:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/j;->q:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
