.class public Lcom/google/android/apps/gmm/car/d/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# instance fields
.field final a:Lcom/google/android/apps/gmm/car/m/m;

.field final b:Lcom/google/android/apps/gmm/map/t;

.field final c:Landroid/os/Handler;

.field d:I

.field final e:Ljava/lang/Runnable;

.field private final f:Lcom/google/android/libraries/curvular/bd;

.field private final g:Lcom/google/android/apps/gmm/car/d/a;

.field private h:Landroid/view/View;

.field private final i:Landroid/view/View$OnKeyListener;

.field private final j:Landroid/view/View$OnGenericMotionListener;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/car/m/m;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/car/d/a;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->c:Landroid/os/Handler;

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/car/d/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/d/v;-><init>(Lcom/google/android/apps/gmm/car/d/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->e:Ljava/lang/Runnable;

    .line 160
    new-instance v0, Lcom/google/android/apps/gmm/car/d/w;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/d/w;-><init>(Lcom/google/android/apps/gmm/car/d/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->i:Landroid/view/View$OnKeyListener;

    .line 179
    new-instance v0, Lcom/google/android/apps/gmm/car/d/x;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/d/x;-><init>(Lcom/google/android/apps/gmm/car/d/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->j:Landroid/view/View$OnGenericMotionListener;

    .line 41
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/libraries/curvular/bd;

    iput-object p1, p0, Lcom/google/android/apps/gmm/car/d/t;->f:Lcom/google/android/libraries/curvular/bd;

    .line 42
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/m/m;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/d/t;->a:Lcom/google/android/apps/gmm/car/m/m;

    .line 43
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/map/t;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/d/t;->b:Lcom/google/android/apps/gmm/map/t;

    .line 44
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/car/d/a;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/d/t;->g:Lcom/google/android/apps/gmm/car/d/a;

    .line 45
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/d/t;I)V
    .locals 5

    .prologue
    const/high16 v1, 0x42480000    # 50.0f

    const/4 v2, 0x0

    const/high16 v0, -0x3db80000    # -50.0f

    .line 22
    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    move v0, v1

    :goto_1
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/t;->b:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/c;->a(FF)Lcom/google/android/apps/gmm/map/a;

    move-result-object v0

    const/16 v2, 0x64

    iput v2, v0, Lcom/google/android/apps/gmm/map/a;->a:I

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/map/t;->a(Lcom/google/android/apps/gmm/map/a;Lcom/google/android/apps/gmm/map/p;Z)V

    goto :goto_0

    :pswitch_2
    move v0, v2

    move v2, v1

    goto :goto_1

    :pswitch_3
    move v4, v2

    move v2, v0

    move v0, v4

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->g:Lcom/google/android/apps/gmm/car/d/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/d/a;->n:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/a;->a()V

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->f:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/d/y;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->h:Landroid/view/View;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->h:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/car/d/u;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/d/u;-><init>(Lcom/google/android/apps/gmm/car/d/t;)V

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/t;->i:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->h:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/t;->j:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->h:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method a(IZ)V
    .locals 2

    .prologue
    .line 113
    packed-switch p1, :pswitch_data_0

    .line 130
    :goto_0
    return-void

    .line 115
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/car/d/y;->a:I

    .line 129
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/t;->h:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 118
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/car/d/y;->b:I

    goto :goto_1

    .line 121
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/car/d/y;->d:I

    goto :goto_1

    .line 124
    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/car/d/y;->c:I

    goto :goto_1

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x13
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/d/t;->e()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->h:Landroid/view/View;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->g:Lcom/google/android/apps/gmm/car/d/a;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/d/a;->n:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/d/a;->a()V

    .line 69
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method

.method e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 144
    iget v0, p0, Lcom/google/android/apps/gmm/car/d/t;->d:I

    if-nez v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 147
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/car/d/t;->d:I

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/gmm/car/d/t;->a(IZ)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/d/t;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/d/t;->e:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 149
    iput v2, p0, Lcom/google/android/apps/gmm/car/d/t;->d:I

    goto :goto_0
.end method
