.class Lcom/google/android/apps/gmm/map/internal/d/c/e;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Landroid/graphics/Bitmap;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/util/webimageview/k;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/util/webimageview/k;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/e;->b:Lcom/google/android/apps/gmm/util/webimageview/k;

    .line 73
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/e;->b:Lcom/google/android/apps/gmm/util/webimageview/k;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/util/webimageview/k;->e:Z

    if-eqz v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/e;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/e;->b:Lcom/google/android/apps/gmm/util/webimageview/k;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/e;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/webimageview/k;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/e;->b:Lcom/google/android/apps/gmm/util/webimageview/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/util/webimageview/k;->a()V

    goto :goto_0
.end method
