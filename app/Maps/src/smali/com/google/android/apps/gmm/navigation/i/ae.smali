.class public Lcom/google/android/apps/gmm/navigation/i/ae;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/navigation/i/aa;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/i/aa;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/ae;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    .line 54
    return-void
.end method


# virtual methods
.method final a(Ljava/util/LinkedList;)V
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/ac;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual/range {p1 .. p1}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v13

    .line 72
    :cond_0
    invoke-interface {v13}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 73
    invoke-interface {v13}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/navigation/i/ac;

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    sget-object v1, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    if-eq v0, v1, :cond_0

    invoke-interface {v13}, Ljava/util/ListIterator;->previousIndex()I

    move-result v14

    const/4 v0, 0x0

    move v12, v0

    :goto_0
    const/4 v0, 0x1

    if-gt v12, v0, :cond_3

    invoke-interface {v13}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v13}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/apps/gmm/navigation/i/ac;

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    iget v1, v6, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget-object v1, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    iget v2, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/navigation/i/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43250000    # 165.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_4

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    iget v1, v6, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/u/a/e;->d:[Lcom/google/android/apps/gmm/map/u/a/f;

    aget-object v0, v0, v1

    iget-object v1, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    iget v2, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    aget-object v1, v1, v2

    const/4 v2, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/u/a/e;->d:[Lcom/google/android/apps/gmm/map/u/a/f;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/u/a/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v10, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/ac;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/ae;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget v4, v6, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    iget v5, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v7, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v8, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->f:[Lcom/google/android/apps/gmm/map/u/a/f;

    sget-object v9, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    sget-object v11, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/navigation/i/ac;-><init>(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/navigation/i/aa;IILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Lcom/google/android/apps/gmm/map/u/a/f;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;)V

    const/4 v1, 0x0

    :goto_1
    if-gt v1, v12, :cond_2

    if-eqz v1, :cond_1

    invoke-interface {v13}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    :cond_1
    invoke-interface {v13}, Ljava/util/ListIterator;->remove()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const-string v1, "Describer"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Created u-turn maneuver: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v13, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    :cond_3
    invoke-interface {v13}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    const/4 v0, 0x0

    :goto_2
    sub-int v2, v1, v14

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    invoke-interface {v13}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    float-to-double v0, v0

    iget-object v2, v8, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v2

    div-double/2addr v0, v2

    const-wide v2, 0x4052c00000000000L    # 75.0

    cmpl-double v0, v0, v2

    if-gtz v0, :cond_3

    add-int/lit8 v0, v12, 0x1

    move v12, v0

    goto/16 :goto_0

    .line 75
    :cond_5
    return-void
.end method

.method final b(Ljava/util/LinkedList;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/ac;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-virtual {p1}, Ljava/util/LinkedList;->listIterator()Ljava/util/ListIterator;

    move-result-object v13

    .line 83
    invoke-interface {v13}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    invoke-interface {v13}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    .line 86
    :cond_0
    invoke-interface {v13}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v13}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/ac;

    :goto_0
    move-object v12, v0

    .line 87
    :goto_1
    invoke-interface {v13}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    invoke-interface {v13}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/ac;

    .line 89
    invoke-virtual {v12}, Lcom/google/android/apps/gmm/navigation/i/ac;->a()I

    move-result v0

    const/16 v3, 0xf

    if-ge v0, v3, :cond_3

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/ac;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/ae;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    .line 97
    iget v4, v12, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    .line 98
    iget v5, v2, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    iget-object v6, v12, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 99
    iget-object v7, v2, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v8, v2, Lcom/google/android/apps/gmm/navigation/i/ac;->f:[Lcom/google/android/apps/gmm/map/u/a/f;

    iget-object v9, v2, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 100
    iget-object v10, v2, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    iget-object v11, v2, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    move-object v2, v1

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/navigation/i/ac;-><init>(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/navigation/i/aa;IILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Lcom/google/android/apps/gmm/map/u/a/f;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;)V

    .line 101
    const-string v2, "Describer"

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x19

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Dropping short sub-path: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 103
    invoke-interface {v13}, Ljava/util/ListIterator;->remove()V

    .line 104
    invoke-interface {v13}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    .line 105
    invoke-interface {v13}, Ljava/util/ListIterator;->remove()V

    .line 106
    invoke-interface {v13, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    :goto_2
    move-object v12, v0

    .line 110
    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 86
    goto :goto_0

    .line 111
    :cond_2
    return-void

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method
