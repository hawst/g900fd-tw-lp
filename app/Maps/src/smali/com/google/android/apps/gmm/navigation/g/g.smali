.class public Lcom/google/android/apps/gmm/navigation/g/g;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/r/a/ap;

.field b:Lcom/google/android/apps/gmm/map/util/b/g;

.field c:Lcom/google/android/apps/gmm/shared/net/a/l;

.field d:Lcom/google/android/apps/gmm/navigation/g/s;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/l;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-string v0, "destination"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/g/g;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 35
    const-string v0, "eventBus"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/g/g;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 36
    const-string v0, "navigationParameters"

    .line 37
    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/shared/net/a/l;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/g/g;->c:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 38
    return-void
.end method
