.class public Lcom/google/android/apps/gmm/map/internal/d/b/ah;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/b/as;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/map/internal/c/l;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;I)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "InMemoryTileCache: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p3, v1, p1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 22
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/l;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v1, v4, v4, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    invoke-direct {v0, p2, v1}, Lcom/google/android/apps/gmm/map/internal/c/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->b:Lcom/google/android/apps/gmm/map/internal/c/l;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/c/bo;)V
    .locals 2

    .prologue
    .line 27
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 29
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 53
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/e;->d()V

    .line 55
    monitor-exit v1

    .line 56
    const/4 v0, 0x1

    return v0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bo;)Z
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->b:Lcom/google/android/apps/gmm/map/internal/c/l;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a_(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->b:Lcom/google/android/apps/gmm/map/internal/c/l;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/internal/c/bp;)Z
    .locals 2

    .prologue
    .line 39
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c(Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 2

    .prologue
    .line 46
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    monitor-enter v1

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ah;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bo;

    monitor-exit v1

    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
