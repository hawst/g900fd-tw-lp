.class public Lcom/google/android/apps/gmm/map/offline/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/a/a;


# instance fields
.field final a:Ljava/io/File;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/offline/i;",
            "Lcom/google/android/apps/gmm/map/offline/g;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/map/offline/i;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/maps/b/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/map/offline/c;

.field private final f:Lcom/google/android/apps/gmm/shared/net/ad;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Landroid/content/Context;Ljava/io/File;)V
    .locals 2

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/offline/h;->f:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 127
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 128
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p3, Ljava/io/File;

    iput-object p3, p0, Lcom/google/android/apps/gmm/map/offline/h;->a:Ljava/io/File;

    .line 129
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/h;->b:Ljava/util/Map;

    .line 130
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/h;->c:Ljava/util/Map;

    .line 131
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/h;->d:Ljava/util/Map;

    .line 132
    new-instance v0, Lcom/google/android/apps/gmm/map/offline/c;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/offline/c;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/offline/h;->e:Lcom/google/android/apps/gmm/map/offline/c;

    .line 133
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/d/c/a/f;[Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 22
    .param p2    # [Landroid/graphics/Bitmap;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/offline/h;->b:Ljava/util/Map;

    new-instance v5, Lcom/google/android/apps/gmm/map/offline/i;

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/d/c/a/b;->a:Lcom/google/android/apps/gmm/map/internal/d/c/a/b;

    const-string v7, "0"

    invoke-direct {v5, v6, v7}, Lcom/google/android/apps/gmm/map/offline/i;-><init>(Lcom/google/android/apps/gmm/map/internal/d/c/a/b;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/offline/g;

    .line 204
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->b:[Ljava/lang/String;

    array-length v5, v5

    new-array v15, v5, [Lcom/google/android/apps/gmm/map/offline/a;

    .line 205
    const/4 v5, 0x0

    move v6, v5

    :goto_0
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v6, v5, :cond_4

    .line 206
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->b:[Ljava/lang/String;

    aget-object v7, v5, v6

    .line 207
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 209
    if-eqz p2, :cond_1

    aget-object v5, p2, v6

    if-eqz v5, :cond_1

    .line 214
    new-instance v5, Lcom/google/android/apps/gmm/map/offline/a;

    aget-object v8, p2, v6

    invoke-direct {v5, v8, v7}, Lcom/google/android/apps/gmm/map/offline/a;-><init>(Landroid/graphics/Bitmap;Ljava/lang/String;)V

    aput-object v5, v15, v6

    .line 205
    :cond_0
    :goto_1
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_0

    .line 215
    :cond_1
    if-eqz v4, :cond_3

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/offline/g;->a:Ljava/util/Map;

    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_3

    .line 217
    const/4 v5, 0x0

    .line 219
    :try_start_1
    invoke-virtual {v4, v7}, Lcom/google/android/apps/gmm/map/offline/g;->a(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v5

    .line 220
    new-instance v7, Lcom/google/android/apps/gmm/map/offline/a;

    invoke-static {v5}, Lcom/google/maps/b/e;->a(Ljava/io/InputStream;)Lcom/google/maps/b/e;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/apps/gmm/map/offline/a;-><init>(Lcom/google/maps/b/e;)V

    aput-object v7, v15, v6
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 225
    :try_start_2
    invoke-static {v5}, Lcom/google/b/e/k;->a(Ljava/io/Closeable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 201
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4

    .line 221
    :catch_0
    move-exception v4

    .line 222
    :try_start_3
    const-class v6, Lcom/google/android/apps/gmm/map/offline/h;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v4}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 223
    :try_start_4
    invoke-static {v5}, Lcom/google/b/e/k;->a(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    const/4 v12, 0x0

    .line 233
    :cond_2
    :goto_2
    monitor-exit p0

    return-object v12

    .line 223
    :catchall_1
    move-exception v4

    :try_start_5
    invoke-static {v5}, Lcom/google/b/e/k;->a(Ljava/io/Closeable;)V

    throw v4

    .line 227
    :cond_3
    const/4 v12, 0x0

    goto :goto_2

    .line 233
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/offline/h;->e:Lcom/google/android/apps/gmm/map/offline/c;

    move-object/from16 v16, v0

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_2a

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->c:F

    const/high16 v6, 0x40800000    # 4.0f

    mul-float/2addr v5, v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/offline/c;->a(Ljava/lang/String;F)Landroid/graphics/Rect;

    move-result-object v4

    move-object v6, v4

    :goto_3
    array-length v4, v15

    new-array v0, v4, [Landroid/graphics/Bitmap;

    move-object/from16 v17, v0

    const/4 v4, -0x1

    const/4 v5, 0x0

    :goto_4
    array-length v7, v15

    if-ge v5, v7, :cond_a

    aget-object v7, v15, v5

    if-eqz v7, :cond_9

    aget-object v7, v15, v5

    move-object/from16 v0, v16

    iget-object v8, v0, Lcom/google/android/apps/gmm/map/offline/c;->b:Landroid/content/res/Resources;

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v8, v6}, Lcom/google/android/apps/gmm/map/offline/a;->a(Lcom/google/android/apps/gmm/map/internal/d/c/a/f;Landroid/util/DisplayMetrics;Landroid/graphics/Rect;)Landroid/graphics/Bitmap;

    move-result-object v7

    aput-object v7, v17, v5

    aget-object v7, v17, v5

    if-nez v7, :cond_5

    sget-object v4, Lcom/google/android/apps/gmm/map/offline/c;->a:Ljava/lang/String;

    const-string v5, "Unable to load bitmap"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v4, v5, v6}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v12, 0x0

    goto :goto_2

    :cond_5
    aget-object v7, v15, v5

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    sget-object v8, Lcom/google/maps/b/l;->c:Lcom/google/maps/b/l;

    if-eq v7, v8, :cond_6

    aget-object v7, v15, v5

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    sget-object v8, Lcom/google/maps/b/l;->b:Lcom/google/maps/b/l;

    if-eq v7, v8, :cond_6

    const/4 v12, 0x0

    goto :goto_2

    :cond_6
    const/4 v7, -0x1

    if-eq v4, v7, :cond_8

    aget-object v7, v15, v5

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/offline/a;->c:Lcom/google/maps/b/h;

    aget-object v8, v15, v4

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/offline/a;->c:Lcom/google/maps/b/h;

    if-ne v7, v8, :cond_7

    aget-object v7, v15, v5

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    aget-object v4, v15, v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    if-eq v7, v4, :cond_8

    :cond_7
    const/4 v12, 0x0

    goto/16 :goto_2

    :cond_8
    move v4, v5

    :cond_9
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_a
    const/4 v12, 0x0

    new-instance v13, Landroid/graphics/Rect;

    invoke-direct {v13}, Landroid/graphics/Rect;-><init>()V

    const/4 v4, 0x0

    move v14, v4

    :goto_5
    move-object/from16 v0, v17

    array-length v4, v0

    if-ge v14, v4, :cond_1f

    aget-object v5, v15, v14

    if-eqz v5, :cond_29

    sget-object v4, Lcom/google/android/apps/gmm/map/offline/d;->a:[I

    iget-object v6, v5, Lcom/google/android/apps/gmm/map/offline/a;->b:Lcom/google/maps/b/l;

    invoke-virtual {v6}, Lcom/google/maps/b/l;->ordinal()I

    move-result v6

    aget v4, v4, v6

    packed-switch v4, :pswitch_data_0

    const/4 v12, 0x0

    goto/16 :goto_2

    :pswitch_0
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/offline/a;->c:Lcom/google/maps/b/h;

    iget v4, v4, Lcom/google/maps/b/h;->j:I

    int-to-float v4, v4

    const/high16 v6, 0x43200000    # 160.0f

    div-float/2addr v4, v6

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->c:F

    mul-float/2addr v4, v7

    move-object/from16 v0, v16

    invoke-virtual {v0, v6, v4}, Lcom/google/android/apps/gmm/map/offline/c;->a(Ljava/lang/String;F)Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/offline/a;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v7

    const/4 v4, 0x4

    invoke-static {v5, v4}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a(Landroid/graphics/Bitmap;I)Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;

    move-result-object v5

    if-nez v5, :cond_c

    const/4 v4, 0x0

    :cond_b
    :goto_6
    if-nez v4, :cond_17

    const/4 v12, 0x0

    goto/16 :goto_2

    :cond_c
    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-nez v4, :cond_d

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    :goto_7
    invoke-virtual {v13, v4}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-nez v4, :cond_10

    const/4 v4, 0x0

    goto :goto_6

    :cond_d
    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    invoke-static {v8, v9, v6, v7}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->nativeCalculateContentRegion(JII)[I

    move-result-object v8

    if-eqz v8, :cond_e

    array-length v4, v8

    const/4 v9, 0x4

    if-eq v4, v9, :cond_f

    :cond_e
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    goto :goto_7

    :cond_f
    new-instance v4, Landroid/graphics/Rect;

    const/4 v9, 0x0

    aget v9, v8, v9

    const/4 v10, 0x1

    aget v10, v8, v10

    const/4 v11, 0x2

    aget v11, v8, v11

    const/16 v18, 0x3

    aget v8, v8, v18

    invoke-direct {v4, v9, v10, v11, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_7

    :cond_10
    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    const-wide/16 v10, 0x0

    cmp-long v4, v8, v10

    if-nez v4, :cond_12

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object v11, v4

    :goto_8
    if-eqz v11, :cond_11

    invoke-virtual {v11}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_15

    :cond_11
    const/4 v4, 0x0

    goto :goto_6

    :cond_12
    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    invoke-static {v8, v9, v6, v7}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->nativeCalculateStretchedDimensions(JII)[I

    move-result-object v8

    if-eqz v8, :cond_13

    array-length v4, v8

    const/4 v9, 0x2

    if-eq v4, v9, :cond_14

    :cond_13
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    move-object v11, v4

    goto :goto_8

    :cond_14
    new-instance v4, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget v11, v8, v11

    const/16 v18, 0x1

    aget v8, v8, v18

    invoke-direct {v4, v9, v10, v11, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v11, v4

    goto :goto_8

    :cond_15
    iget-wide v4, v5, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->a:J

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/offline/NinepatchWrapper;->nativeRenderNinepatch(JII)[I

    move-result-object v5

    if-nez v5, :cond_16

    const/4 v4, 0x0

    goto/16 :goto_6

    :cond_16
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    const/4 v6, 0x0

    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v11}, Landroid/graphics/Rect;->height()I

    move-result v11

    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    goto/16 :goto_6

    :pswitch_1
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/offline/a;->a:Landroid/graphics/Bitmap;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/offline/a;->c:Lcom/google/maps/b/h;

    iget v5, v5, Lcom/google/maps/b/h;->j:I

    const/16 v6, 0x280

    if-eq v6, v5, :cond_b

    const-wide/high16 v6, 0x4084000000000000L    # 640.0

    int-to-double v8, v5

    div-double/2addr v6, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-double v8, v5

    mul-double/2addr v8, v6

    double-to-int v5, v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    int-to-double v8, v8

    mul-double/2addr v6, v8

    double-to-int v6, v6

    const/4 v7, 0x1

    invoke-static {v4, v5, v6, v7}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v4

    goto/16 :goto_6

    :cond_17
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->e:[I

    array-length v5, v5

    if-le v5, v14, :cond_18

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->e:[I

    aget v5, v5, v14

    const/high16 v6, -0x1000000

    and-int/2addr v6, v5

    if-nez v6, :cond_18

    const v6, 0xffffff

    and-int/2addr v5, v6

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/offline/c;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v4

    :cond_18
    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->f:[I

    array-length v5, v5

    if-le v5, v14, :cond_1b

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->f:[I

    aget v5, v5, v14

    const/high16 v6, -0x1000000

    and-int/2addr v6, v5

    if-eqz v6, :cond_1b

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v6

    if-eqz v6, :cond_19

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v6

    if-nez v6, :cond_1a

    :cond_19
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-static {v4, v6, v7, v8, v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v4, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    :cond_1a
    const/4 v6, 0x1

    const/4 v7, 0x1

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8, v5}, Landroid/graphics/Bitmap;->setPixel(III)V

    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    new-instance v7, Landroid/graphics/Canvas;

    invoke-direct {v7, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v8, Landroid/graphics/PorterDuffXfermode;

    sget-object v9, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v8, v9}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v5, v8}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    new-instance v8, Landroid/graphics/Rect;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-direct {v8, v9, v10, v11, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v9, Landroid/graphics/Rect;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v9, v10, v11, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v7, v6, v8, v9, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1b
    if-nez v4, :cond_1d

    move-object v4, v12

    :cond_1c
    :goto_9
    add-int/lit8 v5, v14, 0x1

    move v14, v5

    move-object v12, v4

    goto/16 :goto_5

    :cond_1d
    if-eqz v12, :cond_1c

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    if-lt v5, v6, :cond_1e

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    if-lt v5, v6, :cond_1e

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v5

    if-eqz v5, :cond_1e

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result v5

    if-eqz v5, :cond_1e

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    new-instance v10, Landroid/graphics/Canvas;

    invoke-direct {v10, v12}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v11, Landroid/graphics/Rect;

    const/16 v18, 0x0

    const/16 v19, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v18, Landroid/graphics/Rect;

    sub-int v19, v5, v7

    sub-int v20, v6, v8

    add-int/2addr v5, v7

    add-int/2addr v6, v8

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, v18

    invoke-virtual {v10, v4, v11, v0, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object v4, v12

    goto :goto_9

    :cond_1e
    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, v5}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/16 v18, 0x0

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v20

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v10, v11, v0, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/Rect;

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    sub-int v18, v6, v18

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    sub-int v19, v7, v19

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    div-int/lit8 v20, v20, 0x2

    add-int v20, v20, v6

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v21

    div-int/lit8 v21, v21, 0x2

    add-int v21, v21, v7

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v8, v12, v10, v11, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    new-instance v10, Landroid/graphics/Rect;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v18

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v19

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-direct {v10, v11, v12, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v11, Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int v12, v6, v12

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    sub-int v18, v7, v18

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    add-int v6, v6, v19

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v19

    div-int/lit8 v19, v19, 0x2

    add-int v7, v7, v19

    move/from16 v0, v18

    invoke-direct {v11, v12, v0, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v8, v4, v10, v11, v9}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object v4, v5

    goto/16 :goto_9

    :cond_1f
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x0

    aget-object v4, v15, v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/offline/a;->c:Lcom/google/maps/b/h;

    iget v4, v4, Lcom/google/maps/b/h;->j:I

    int-to-float v4, v4

    const/high16 v5, 0x43200000    # 160.0f

    div-float v5, v4, v5

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->c:F

    mul-float/2addr v6, v5

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Lcom/google/android/apps/gmm/map/offline/c;->a(Ljava/lang/String;F)Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v13}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_28

    new-instance v4, Landroid/graphics/Rect;

    const/4 v6, 0x0

    const/4 v8, 0x0

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v12}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-direct {v4, v6, v8, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_a
    array-length v6, v15

    add-int/lit8 v6, v6, -0x1

    aget-object v8, v15, v6

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/offline/c;->c:Lcom/google/android/apps/gmm/map/offline/f;

    new-instance v9, Lcom/google/android/apps/gmm/map/offline/e;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/map/offline/e;-><init>()V

    invoke-virtual {v9, v12}, Lcom/google/android/apps/gmm/map/offline/e;->a(Landroid/graphics/Bitmap;)V

    iget-object v6, v9, Lcom/google/android/apps/gmm/map/offline/e;->c:Landroid/graphics/Bitmap;

    if-nez v6, :cond_21

    const/4 v12, 0x0

    :goto_b
    invoke-virtual {v9, v12}, Lcom/google/android/apps/gmm/map/offline/e;->a(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->c:F

    mul-float/2addr v5, v6

    iget-object v6, v9, Lcom/google/android/apps/gmm/map/offline/e;->a:Landroid/graphics/Paint;

    invoke-virtual {v6, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->d:I

    iget-object v6, v9, Lcom/google/android/apps/gmm/map/offline/e;->a:Landroid/graphics/Paint;

    invoke-virtual {v6, v5}, Landroid/graphics/Paint;->setColor(I)V

    iget v5, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v5, v6

    iget v6, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v6

    iget-object v6, v8, Lcom/google/android/apps/gmm/map/offline/a;->d:Ljava/lang/Integer;

    if-eqz v6, :cond_22

    const/4 v6, 0x1

    :goto_c
    if-eqz v6, :cond_27

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/offline/a;->d:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    move v6, v5

    :goto_d
    iget-object v5, v8, Lcom/google/android/apps/gmm/map/offline/a;->e:Ljava/lang/Integer;

    if-eqz v5, :cond_23

    const/4 v5, 0x1

    :goto_e
    if-eqz v5, :cond_20

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/offline/a;->e:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :cond_20
    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v6, v5

    iget v6, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    sub-int/2addr v4, v6

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v6

    add-int/2addr v6, v4

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a:Ljava/lang/String;

    iget-object v4, v9, Lcom/google/android/apps/gmm/map/offline/e;->b:Landroid/graphics/Canvas;

    if-nez v4, :cond_24

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    throw v4

    :cond_21
    iget-object v6, v9, Lcom/google/android/apps/gmm/map/offline/e;->c:Landroid/graphics/Bitmap;

    sget-object v10, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v11, 0x1

    invoke-virtual {v6, v10, v11}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v12

    goto :goto_b

    :cond_22
    const/4 v6, 0x0

    goto :goto_c

    :cond_23
    const/4 v5, 0x0

    goto :goto_e

    :cond_24
    if-eqz v7, :cond_25

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_26

    :cond_25
    const/4 v4, 0x1

    :goto_f
    if-nez v4, :cond_2

    iget-object v4, v9, Lcom/google/android/apps/gmm/map/offline/e;->b:Landroid/graphics/Canvas;

    int-to-float v5, v5

    int-to-float v6, v6

    iget-object v8, v9, Lcom/google/android/apps/gmm/map/offline/e;->a:Landroid/graphics/Paint;

    invoke-virtual {v4, v7, v5, v6, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    :cond_26
    const/4 v4, 0x0

    goto :goto_f

    :cond_27
    move v6, v5

    goto :goto_d

    :cond_28
    move-object v4, v13

    goto/16 :goto_a

    :cond_29
    move-object v4, v12

    goto/16 :goto_9

    :cond_2a
    move-object v6, v4

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a([Landroid/graphics/Bitmap;[Ljava/lang/String;I[I[ILjava/lang/String;FI)Landroid/graphics/Bitmap;
    .locals 10
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 252
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;

    const/4 v3, 0x0

    const/4 v4, 0x0

    int-to-float v7, p3

    move-object v1, p2

    move-object/from16 v2, p6

    move/from16 v5, p7

    move/from16 v6, p8

    move-object v8, p4

    move-object v9, p5

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;-><init>([Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;FIF[I[I)V

    .line 254
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/map/offline/h;->a(Lcom/google/android/apps/gmm/map/internal/d/c/a/f;[Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/maps/b/a;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/offline/h;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/b/a;

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/offline/h;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/offline/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 320
    if-eqz v0, :cond_0

    .line 322
    :try_start_1
    new-instance v1, Ljava/net/URL;

    invoke-direct {v1, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 323
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/offline/h;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    invoke-virtual {v1}, Ljava/net/URL;->getFile()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 325
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/offline/h;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/offline/g;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/offline/g;->a:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/offline/h;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/offline/g;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/offline/g;->a(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 328
    :try_start_2
    invoke-static {v1}, Lcom/google/b/e/h;->a(Ljava/io/InputStream;)[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 330
    :try_start_3
    invoke-static {v1}, Lcom/google/b/e/k;->a(Ljava/io/Closeable;)V
    :try_end_3
    .catch Ljava/net/MalformedURLException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 339
    :goto_0
    monitor-exit p0

    return-object v0

    .line 330
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-static {v1}, Lcom/google/b/e/k;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_4
    .catch Ljava/net/MalformedURLException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 334
    :catch_0
    move-exception v0

    .line 335
    :try_start_5
    const-string v1, "Invalid url for resource fetch"

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 339
    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [B
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 319
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 238
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/a/f;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/d/c/a/f;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/map/offline/h;->a(Lcom/google/android/apps/gmm/map/internal/d/c/a/f;[Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method
