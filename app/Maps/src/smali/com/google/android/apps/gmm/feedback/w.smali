.class Lcom/google/android/apps/gmm/feedback/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/graphics/Bitmap;

.field final synthetic b:Lcom/google/android/apps/gmm/feedback/v;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/feedback/v;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/apps/gmm/feedback/w;->b:Lcom/google/android/apps/gmm/feedback/v;

    iput-object p2, p0, Lcom/google/android/apps/gmm/feedback/w;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/w;->b:Lcom/google/android/apps/gmm/feedback/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/w;->a:Landroid/graphics/Bitmap;

    iget-object v2, v0, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    .line 128
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/w;->b:Lcom/google/android/apps/gmm/feedback/v;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/feedback/v;->b:Z

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/w;->b:Lcom/google/android/apps/gmm/feedback/v;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/feedback/v;->d:Landroid/graphics/Bitmap;

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/w;->b:Lcom/google/android/apps/gmm/feedback/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/v;->e:Lcom/google/android/apps/gmm/feedback/a/g;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/feedback/w;->b:Lcom/google/android/apps/gmm/feedback/v;

    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/v;->e:Lcom/google/android/apps/gmm/feedback/a/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/feedback/w;->b:Lcom/google/android/apps/gmm/feedback/v;

    iget-object v1, v1, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/feedback/a/g;->a(Landroid/graphics/Bitmap;)V

    .line 135
    :cond_0
    return-void

    .line 126
    :cond_1
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, v0, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v3, v0, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->DST_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    if-eqz v1, :cond_2

    sget-object v5, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int v5, v3, v5

    const/4 v6, 0x0

    int-to-float v5, v5

    invoke-virtual {v2, v1, v6, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/feedback/v;->d:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_3

    sget-object v1, Lcom/google/android/apps/gmm/feedback/v;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/apps/gmm/feedback/v;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int v1, v3, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v3, v0, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v5, v0, Lcom/google/android/apps/gmm/feedback/v;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    sub-int/2addr v3, v5

    div-int/lit8 v3, v3, 0x2

    new-instance v5, Landroid/graphics/PorterDuffXfermode;

    sget-object v6, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v5, v6}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iget-object v5, v0, Lcom/google/android/apps/gmm/feedback/v;->d:Landroid/graphics/Bitmap;

    int-to-float v3, v3

    int-to-float v1, v1

    invoke-virtual {v2, v5, v3, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/feedback/v;->c:Landroid/graphics/Bitmap;

    goto :goto_0
.end method
