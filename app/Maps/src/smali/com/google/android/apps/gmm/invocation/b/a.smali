.class public Lcom/google/android/apps/gmm/invocation/b/a;
.super Lcom/google/android/apps/gmm/invocation/b/e;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/invocation/b/e;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 41
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/r/b/a/gb;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/google/r/b/a/gb;->e:Lcom/google/r/b/a/gb;

    return-object v0
.end method

.method public final b(Lcom/google/e/a/a/a/b;)Ljava/lang/Runnable;
    .locals 12

    .prologue
    .line 46
    if-nez p1, :cond_0

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/g;

    const-string v1, "null external invocation response"

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/invocation/b/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_0
    const/4 v0, 0x3

    invoke-static {}, Lcom/google/r/b/a/agd;->d()Lcom/google/r/b/a/agd;

    move-result-object v1

    if-eqz p1, :cond_1

    const/16 v2, 0x19

    invoke-virtual {p1, v0, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/google/r/b/a/agd;

    .line 50
    const/4 v1, 0x7

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v2

    if-eqz p1, :cond_2

    const/16 v3, 0x19

    invoke-virtual {p1, v1, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    :goto_1
    check-cast v1, Lcom/google/r/b/a/agl;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    move-object v10, v1

    .line 51
    :goto_2
    if-nez v0, :cond_4

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/g;

    const-string v1, "null directions request"

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/invocation/b/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 50
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/e;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    move-object v10, v2

    goto :goto_2

    .line 55
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/invocation/b/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 56
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/directions/f/a;->a(Lcom/google/r/b/a/agd;Landroid/content/Context;)Lcom/google/android/apps/gmm/directions/f/a;

    move-result-object v11

    .line 57
    if-eqz v10, :cond_a

    if-eqz v10, :cond_9

    iget-object v0, v10, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget v0, v0, Lcom/google/r/b/a/afb;->g:I

    invoke-static {v0}, Lcom/google/maps/g/a/z;->a(I)Lcom/google/maps/g/a/z;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    :cond_5
    sget-object v1, Lcom/google/maps/g/a/z;->a:Lcom/google/maps/g/a/z;

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_a

    const/4 v0, 0x1

    move v9, v0

    .line 58
    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/invocation/b/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v2

    .line 59
    const/4 v1, 0x0

    iget-object v0, v11, Lcom/google/android/apps/gmm/directions/f/a;->e:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v3

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ap;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    sget-object v4, Lcom/google/android/apps/gmm/map/r/a/ar;->b:Lcom/google/android/apps/gmm/map/r/a/ar;

    if-ne v0, v4, :cond_b

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_e

    iget-object v1, v11, Lcom/google/android/apps/gmm/directions/f/a;->g:Lcom/google/o/b/a/v;

    if-eqz v1, :cond_7

    iget v0, v1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v3, 0x10

    if-ne v0, v3, :cond_c

    const/4 v0, 0x1

    :goto_7
    if-nez v0, :cond_d

    :cond_7
    const/4 v0, 0x1

    move v1, v0

    .line 61
    :goto_8
    const/4 v0, 0x1

    .line 63
    invoke-static {}, Lcom/google/r/b/a/fr;->d()Lcom/google/r/b/a/fr;

    move-result-object v2

    .line 61
    if-eqz p1, :cond_f

    const/16 v3, 0x19

    invoke-virtual {p1, v0, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    :goto_9
    if-eqz v0, :cond_10

    :goto_a
    check-cast v0, Lcom/google/r/b/a/fr;

    .line 64
    iget v0, v0, Lcom/google/r/b/a/fr;->b:I

    invoke-static {v0}, Lcom/google/r/b/a/ft;->a(I)Lcom/google/r/b/a/ft;

    move-result-object v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/google/r/b/a/ft;->a:Lcom/google/r/b/a/ft;

    :cond_8
    sget-object v2, Lcom/google/android/apps/gmm/invocation/b/d;->a:[I

    invoke-virtual {v0}, Lcom/google/r/b/a/ft;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const-string v2, "DirectionsExecutor"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected action type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v2, v0

    .line 66
    :goto_b
    if-eqz v9, :cond_11

    if-nez v1, :cond_11

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/invocation/b/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v0

    .line 69
    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v0

    const/4 v1, 0x1

    .line 70
    invoke-interface {v0, v11, v10, v1}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/android/apps/gmm/map/r/a/e;Z)Lcom/google/android/apps/gmm/directions/a/c;

    move-result-object v1

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/b;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/gmm/invocation/b/b;-><init>(Lcom/google/android/apps/gmm/invocation/b/a;Lcom/google/android/apps/gmm/directions/a/c;Lcom/google/android/apps/gmm/directions/a/g;)V

    .line 80
    :goto_c
    return-object v0

    .line 57
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_3

    :cond_a
    const/4 v0, 0x0

    move v9, v0

    goto/16 :goto_4

    .line 59
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_7

    :cond_d
    if-eqz v2, :cond_e

    iget-object v0, v1, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    const/4 v0, 0x1

    new-array v8, v0, [F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v2

    iget-wide v4, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    const/high16 v1, 0x42480000    # 50.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_e

    const/4 v0, 0x1

    move v1, v0

    goto/16 :goto_8

    :cond_e
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_8

    .line 61
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_9

    :cond_10
    move-object v0, v2

    goto/16 :goto_a

    .line 64
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/directions/a/g;->d:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v2, v0

    goto :goto_b

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/directions/a/g;->b:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v2, v0

    goto :goto_b

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/directions/a/g;->a:Lcom/google/android/apps/gmm/directions/a/g;

    move-object v2, v0

    goto :goto_b

    .line 80
    :cond_11
    new-instance v0, Lcom/google/android/apps/gmm/invocation/b/c;

    invoke-direct {v0, p0, v11, v2}, Lcom/google/android/apps/gmm/invocation/b/c;-><init>(Lcom/google/android/apps/gmm/invocation/b/a;Lcom/google/android/apps/gmm/directions/f/a;Lcom/google/android/apps/gmm/directions/a/g;)V

    goto :goto_c

    :cond_12
    move v0, v1

    goto/16 :goto_6

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
