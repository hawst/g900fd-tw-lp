.class Lcom/google/android/apps/gmm/base/views/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    if-eq p1, v0, :cond_1

    .line 306
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    instance-of v0, p2, Lcom/google/android/apps/gmm/base/views/ArrowViewPager$ArrowViewLeft;

    if-eqz v0, :cond_4

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    if-eq v1, p2, :cond_3

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->removeView(Landroid/view/View;)V

    :cond_2
    iput-object p2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/f;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/base/views/f;-><init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;Landroid/view/View;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->e:Lcom/google/android/apps/gmm/base/views/f;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->c:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->bringChildToFront(Landroid/view/View;)V

    .line 301
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(I)V

    goto :goto_0

    .line 302
    :cond_4
    instance-of v0, p2, Lcom/google/android/apps/gmm/base/views/ArrowViewPager$ArrowViewRight;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    if-eq v1, p2, :cond_6

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->removeView(Landroid/view/View;)V

    :cond_5
    iput-object p2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/base/views/f;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/base/views/f;-><init>(Lcom/google/android/apps/gmm/base/views/ArrowViewPager;Landroid/view/View;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->f:Lcom/google/android/apps/gmm/base/views/f;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->d:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->bringChildToFront(Landroid/view/View;)V

    .line 304
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/b;->a:Lcom/google/android/apps/gmm/base/views/ArrowViewPager;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/ArrowViewPager;->a(I)V

    goto :goto_0
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 309
    return-void
.end method
