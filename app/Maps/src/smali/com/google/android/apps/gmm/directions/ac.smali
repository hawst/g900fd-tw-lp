.class Lcom/google/android/apps/gmm/directions/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/i/a/b;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/aa;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/aa;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 331
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    sget-object v2, Lcom/google/android/apps/gmm/directions/ad;->b:Lcom/google/android/apps/gmm/directions/ad;

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v1, v0

    goto :goto_0

    .line 332
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/directions/aa;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "notifyAllIconsAvailable for: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 335
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    monitor-enter v1

    .line 336
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    sget-object v3, Lcom/google/android/apps/gmm/directions/ad;->c:Lcom/google/android/apps/gmm/directions/ad;

    iput-object v3, v2, Lcom/google/android/apps/gmm/directions/aa;->c:Lcom/google/android/apps/gmm/directions/ad;

    .line 338
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/directions/aa;->f:Z

    if-eqz v2, :cond_3

    .line 340
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/aa;->d:Lcom/google/android/apps/gmm/directions/f/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/f/a;->h:Lcom/google/maps/g/hy;

    if-eqz v2, :cond_2

    iget-boolean v0, v2, Lcom/google/maps/g/hy;->i:Z

    :cond_2
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/aa;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 341
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v2, Lcom/google/r/b/a/tf;->b:Lcom/google/r/b/a/tf;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    .line 342
    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/aa;->e:Ljava/lang/String;

    .line 341
    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/tf;Ljava/lang/String;)V

    .line 345
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/aa;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/directions/b/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ac;->a:Lcom/google/android/apps/gmm/directions/aa;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/directions/b/b;-><init>(Lcom/google/android/apps/gmm/directions/a/c;)V

    .line 347
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 348
    return-void

    .line 345
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
