.class public Lcom/google/android/apps/gmm/v/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/g;


# static fields
.field static b:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field c:Lcom/google/android/apps/gmm/v/be;

.field private d:Lcom/google/android/apps/gmm/v/l;

.field private e:Lcom/google/android/apps/gmm/v/l;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/v/f;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/lang/Object;

.field private h:Lcom/google/android/apps/gmm/v/cm;

.field private final i:Lcom/google/android/apps/gmm/v/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/v/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 120
    new-instance v0, Lcom/google/android/apps/gmm/v/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/j;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/v/ag;)V
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/v/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/i;->d:Lcom/google/android/apps/gmm/v/l;

    .line 98
    new-instance v0, Lcom/google/android/apps/gmm/v/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    .line 107
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/i;->f:Ljava/util/Set;

    .line 113
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/v/cm;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cm;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    .line 141
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/i;->i:Lcom/google/android/apps/gmm/v/ag;

    .line 142
    return-void
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 311
    sget-object v0, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempt to update live data from outside a Behavior"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 314
    :cond_0
    return-void
.end method


# virtual methods
.method final a()V
    .locals 13

    .prologue
    const/4 v7, -0x1

    const/4 v3, 0x0

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 247
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->d:Lcom/google/android/apps/gmm/v/l;

    .line 248
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    iput-object v2, p0, Lcom/google/android/apps/gmm/v/i;->d:Lcom/google/android/apps/gmm/v/l;

    .line 249
    iput-object v0, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    iget-object v2, v0, Lcom/google/android/apps/gmm/v/l;->a:[Ljava/util/List;

    array-length v4, v2

    move v0, v3

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v2, v0

    invoke-interface {v5}, Ljava/util/List;->clear()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/f;

    .line 255
    iget-object v4, p0, Lcom/google/android/apps/gmm/v/i;->d:Lcom/google/android/apps/gmm/v/l;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/v/l;->a(Lcom/google/android/apps/gmm/v/f;)V

    goto :goto_1

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 257
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 258
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v4, v3

    .line 265
    :goto_2
    sget v0, Lcom/google/android/apps/gmm/v/i;->a:I

    if-ge v4, v0, :cond_a

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->d:Lcom/google/android/apps/gmm/v/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/l;->a:[Ljava/util/List;

    aget-object v9, v0, v4

    .line 267
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v10

    move v8, v3

    .line 268
    :goto_3
    if-ge v8, v10, :cond_9

    .line 269
    invoke-interface {v9, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/f;

    .line 270
    sget-boolean v1, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v1, :cond_2

    .line 271
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->c:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/v/be;->a(Lcom/google/android/apps/gmm/v/f;)V

    .line 273
    :cond_2
    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/f;->b()Z

    move-result v1

    .line 274
    if-eqz v1, :cond_8

    .line 277
    :try_start_2
    sget-object v1, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 279
    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/v/f;->b(Lcom/google/android/apps/gmm/v/g;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 282
    sget-object v1, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 287
    iget-object v11, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    monitor-enter v11

    .line 288
    :try_start_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    move v1, v3

    :goto_4
    iget v5, v2, Lcom/google/android/apps/gmm/v/cm;->c:I

    if-ge v1, v5, :cond_4

    iget-object v5, v2, Lcom/google/android/apps/gmm/v/cm;->a:[Lcom/google/android/apps/gmm/v/f;

    aget-object v5, v5, v1

    if-ne v0, v5, :cond_3

    :goto_5
    if-eq v1, v7, :cond_5

    iget-object v5, v2, Lcom/google/android/apps/gmm/v/cm;->a:[Lcom/google/android/apps/gmm/v/f;

    aget-object v5, v5, v1

    if-eqz v5, :cond_5

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/v/cm;->a(I)Ljava/util/List;

    move-result-object v1

    move-object v6, v1

    .line 289
    :goto_6
    if-nez v6, :cond_6

    move v2, v3

    :goto_7
    move v5, v3

    .line 291
    :goto_8
    if-ge v5, v2, :cond_7

    .line 292
    iget-object v12, p0, Lcom/google/android/apps/gmm/v/i;->d:Lcom/google/android/apps/gmm/v/l;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/v/f;

    invoke-virtual {v12, v1}, Lcom/google/android/apps/gmm/v/l;->a(Lcom/google/android/apps/gmm/v/f;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 291
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_8

    .line 281
    :catchall_1
    move-exception v0

    .line 282
    sget-object v1, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0

    .line 288
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    move v1, v7

    goto :goto_5

    :cond_5
    const/4 v1, 0x0

    move-object v6, v1

    goto :goto_6

    .line 290
    :cond_6
    :try_start_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v1

    move v2, v1

    goto :goto_7

    .line 294
    :cond_7
    monitor-exit v11
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 295
    sget-boolean v1, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v1, :cond_8

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->c:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/v/be;->b(Lcom/google/android/apps/gmm/v/f;)V

    .line 268
    :cond_8
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_3

    .line 294
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v11
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 265
    :cond_9
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_2

    .line 304
    :cond_a
    return-void
.end method

.method final a(Lcom/google/android/apps/gmm/v/f;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 208
    sget-object v0, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 211
    :try_start_0
    invoke-interface {p1, p0}, Lcom/google/android/apps/gmm/v/f;->a(Lcom/google/android/apps/gmm/v/g;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    sget-object v0, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 216
    return-void

    .line 213
    :catchall_0
    move-exception v0

    .line 214
    sget-object v1, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    .locals 7

    .prologue
    .line 150
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/v/k;->a:[I

    iget-object v1, p2, Lcom/google/android/apps/gmm/v/cp;->a:Lcom/google/android/apps/gmm/v/cq;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/cq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 171
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unimplemented WakeUpCondition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/v/l;->a(Lcom/google/android/apps/gmm/v/f;)V

    .line 155
    monitor-exit v1

    .line 175
    :goto_0
    return-void

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 156
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 161
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    monitor-enter v1

    .line 164
    :try_start_2
    check-cast p2, Lcom/google/android/apps/gmm/v/cr;

    iget-object v2, p2, Lcom/google/android/apps/gmm/v/cr;->b:Lcom/google/android/apps/gmm/v/f;

    .line 165
    invoke-interface {p1}, Lcom/google/android/apps/gmm/v/f;->a()Lcom/google/android/apps/gmm/v/h;

    move-result-object v0

    iget v0, v0, Lcom/google/android/apps/gmm/v/h;->f:I

    invoke-interface {v2}, Lcom/google/android/apps/gmm/v/f;->a()Lcom/google/android/apps/gmm/v/h;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/gmm/v/h;->f:I

    if-gt v0, v3, :cond_1

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 167
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x48

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Trigger behavior "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " does not have higher priority than triggered behavior "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 169
    :cond_1
    :try_start_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/v/cm;->a(Lcom/google/android/apps/gmm/v/f;)I

    move-result v0

    const/4 v4, -0x1

    if-eq v0, v4, :cond_3

    iget-object v2, v3, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v3, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    aget-object v0, v2, v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    :cond_2
    :goto_1
    monitor-exit v1

    goto/16 :goto_0

    .line 169
    :cond_3
    iget v0, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    const/16 v4, 0x8

    if-ge v0, v4, :cond_4

    const/4 v0, 0x1

    :goto_2
    const-string v4, "Behavior relations pool is full"

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    iget-object v0, v3, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    iget v4, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    aget-object v0, v0, v4

    if-nez v0, :cond_6

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    iget v4, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    aput-object v5, v0, v4

    :goto_3
    iget-object v0, v3, Lcom/google/android/apps/gmm/v/cm;->a:[Lcom/google/android/apps/gmm/v/f;

    iget v4, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    aput-object v2, v0, v4

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    iget v2, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    aget-object v0, v0, v2

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget v0, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    goto :goto_1

    :cond_6
    iget-object v0, v3, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    iget v4, v3, Lcom/google/android/apps/gmm/v/cm;->c:I

    aget-object v0, v0, v4

    invoke-interface {v0}, Ljava/util/List;->clear()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_3

    .line 151
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method final b(Lcom/google/android/apps/gmm/v/f;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 221
    sget-object v0, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 224
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 225
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/l;->a:[Ljava/util/List;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/v/f;->a()Lcom/google/android/apps/gmm/v/h;

    move-result-object v4

    iget v4, v4, Lcom/google/android/apps/gmm/v/h;->f:I

    aget-object v0, v0, v4

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 226
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    :try_start_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    monitor-enter v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 228
    :try_start_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    move v0, v2

    :goto_0
    iget v5, v4, Lcom/google/android/apps/gmm/v/cm;->c:I

    if-ge v0, v5, :cond_2

    iget-object v5, v4, Lcom/google/android/apps/gmm/v/cm;->a:[Lcom/google/android/apps/gmm/v/f;

    aget-object v5, v5, v0

    if-ne p1, v5, :cond_1

    :goto_1
    if-eq v0, v1, :cond_0

    iget-object v1, v4, Lcom/google/android/apps/gmm/v/cm;->a:[Lcom/google/android/apps/gmm/v/f;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/v/cm;->a(I)Ljava/util/List;

    .line 229
    :cond_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    move v1, v2

    :goto_2
    iget v0, v4, Lcom/google/android/apps/gmm/v/cm;->c:I

    if-ge v1, v0, :cond_3

    iget-object v0, v4, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    aget-object v0, v0, v1

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, v4, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    aget-object v0, v0, v1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit8 v0, v1, -0x1

    invoke-virtual {v4, v1}, Lcom/google/android/apps/gmm/v/cm;->a(I)Ljava/util/List;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_3
    add-int/lit8 v1, v0, 0x1

    goto :goto_2

    .line 226
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 232
    :catchall_1
    move-exception v0

    .line 233
    sget-object v1, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    throw v0

    .line 228
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 230
    :cond_3
    :try_start_6
    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 233
    sget-object v0, Lcom/google/android/apps/gmm/v/i;->b:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 235
    return-void

    .line 230
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method public final b(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V
    .locals 7

    .prologue
    .line 179
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 180
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/v/k;->a:[I

    iget-object v1, p2, Lcom/google/android/apps/gmm/v/cp;->a:Lcom/google/android/apps/gmm/v/cq;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/cq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 200
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unimplemented WakeUpCondition"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/l;->a:[Ljava/util/List;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/v/f;->a()Lcom/google/android/apps/gmm/v/h;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/gmm/v/h;->f:I

    aget-object v0, v0, v2

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 184
    monitor-exit v1

    .line 204
    :goto_0
    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 185
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 188
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/i;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 189
    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 190
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    monitor-enter v1

    .line 193
    :try_start_2
    check-cast p2, Lcom/google/android/apps/gmm/v/cr;

    iget-object v0, p2, Lcom/google/android/apps/gmm/v/cr;->b:Lcom/google/android/apps/gmm/v/f;

    .line 194
    invoke-interface {p1}, Lcom/google/android/apps/gmm/v/f;->a()Lcom/google/android/apps/gmm/v/h;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/gmm/v/h;->f:I

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/f;->a()Lcom/google/android/apps/gmm/v/h;

    move-result-object v3

    iget v3, v3, Lcom/google/android/apps/gmm/v/h;->f:I

    if-gt v2, v3, :cond_1

    .line 195
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 196
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x48

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Trigger behavior "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " does not have higher priority than triggered behavior "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 199
    :catchall_2
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    throw v0

    .line 198
    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/i;->h:Lcom/google/android/apps/gmm/v/cm;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/v/cm;->a(Lcom/google/android/apps/gmm/v/f;)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_2

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/cm;->a:[Lcom/google/android/apps/gmm/v/f;

    aget-object v3, v3, v0

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    aget-object v3, v3, v0

    invoke-interface {v3, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/cm;->b:[Ljava/util/List;

    aget-object v3, v3, v0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/v/cm;->a(I)Ljava/util/List;

    .line 199
    :cond_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto/16 :goto_0

    .line 180
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final c()Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 322
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/i;->g:Ljava/lang/Object;

    monitor-enter v3

    .line 326
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/i;->e:Lcom/google/android/apps/gmm/v/l;

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/l;->a:[Ljava/util/List;

    array-length v5, v4

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/i;->i:Lcom/google/android/apps/gmm/v/ag;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/ag;->b()I

    move-result v2

    if-lez v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    monitor-exit v3

    return v0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    move v2, v1

    goto :goto_1

    .line 327
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
