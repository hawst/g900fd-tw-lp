.class Lcom/google/android/apps/gmm/directions/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/h/u;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)V
    .locals 0

    .prologue
    .line 721
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/q;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 724
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/q;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b(Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 735
    :goto_0
    return-void

    .line 728
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/q;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/q;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget-object v4, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->f:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/q;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    iget v5, v0, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->b:I

    .line 729
    if-ltz v5, :cond_1

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v0, v0

    if-gt v0, v5, :cond_3

    :cond_1
    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ao;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    if-eqz p1, :cond_2

    .line 730
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->y:Ljava/lang/String;

    :cond_2
    iput-object v1, v3, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v1, 0x0

    sget-object v4, Lcom/google/b/f/t;->fM:Lcom/google/b/f/t;

    aput-object v4, v0, v1

    .line 731
    iput-object v0, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 732
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 728
    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 734
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/q;->a:Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/directions/DirectionsDetailsPageFragment;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    goto :goto_0

    .line 729
    :cond_3
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v5

    if-nez v0, :cond_4

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v7, Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    invoke-direct {v7, v0, v4}, Lcom/google/android/apps/gmm/map/r/a/ao;-><init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V

    aput-object v7, v6, v5

    :cond_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v0, v0, v5

    goto :goto_1
.end method
