.class public Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;
.super Landroid/app/DialogFragment;
.source "PG"


# instance fields
.field a:Z

.field b:Z

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Activity;ZZI)Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;
    .locals 2

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;-><init>()V

    .line 48
    const-string v1, "photoResultDialog"

    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Landroid/app/DialogFragment;Ljava/lang/String;)Z

    .line 49
    iput-boolean p1, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->a:Z

    .line 50
    iput-boolean p2, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->b:Z

    .line 51
    iput p3, v0, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->c:I

    .line 52
    return-object v0
.end method


# virtual methods
.method public declared-synchronized onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 60
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->a:Z

    if-eqz v1, :cond_0

    .line 61
    sget v1, Lcom/google/android/apps/gmm/l;->jM:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 66
    :goto_0
    sget v1, Lcom/google/android/apps/gmm/l;->ki:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->fW:I

    new-instance v2, Lcom/google/android/apps/gmm/photo/r;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/photo/r;-><init>(Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;)V

    .line 67
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 63
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/j;->E:I

    iget v3, p0, Lcom/google/android/apps/gmm/photo/PhotoUploadResultDialog;->c:I

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    .line 64
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
