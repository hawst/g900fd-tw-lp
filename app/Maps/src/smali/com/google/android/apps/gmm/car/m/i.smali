.class public final Lcom/google/android/apps/gmm/car/m/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/l;


# instance fields
.field public a:Lcom/google/android/apps/gmm/car/m/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:Lcom/google/android/apps/gmm/car/m/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public c:Lcom/google/android/apps/gmm/car/m/o;

.field public final d:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Lcom/google/android/apps/gmm/car/m/h;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/car/m/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/car/m/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/m/j;-><init>(Lcom/google/android/apps/gmm/car/m/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->e:Lcom/google/android/apps/gmm/car/m/g;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;Lcom/google/android/apps/gmm/car/m/f;)Landroid/view/View;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 63
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/m/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    .line 64
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/car/m/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->e:Lcom/google/android/apps/gmm/car/m/g;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v2, p2, Lcom/google/android/apps/gmm/car/m/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 78
    :goto_0
    return-object v0

    .line 72
    :cond_3
    iget v0, p2, Lcom/google/android/apps/gmm/car/m/f;->a:I

    if-lez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/car/m/o;->b:Lcom/google/android/apps/gmm/car/m/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    move-object v0, v1

    .line 76
    goto :goto_0

    .line 72
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 78
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/car/m/h;->a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    if-eqz v0, :cond_1

    .line 84
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    .line 92
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/m/i;->e:Lcom/google/android/apps/gmm/car/m/g;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 87
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/h;->b()V

    goto :goto_0

    .line 92
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/f;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Don\'t have that listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 93
    :cond_3
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    .line 94
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    .line 95
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/car/m/h;)V
    .locals 2

    .prologue
    .line 109
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 111
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/m/i;->e()V

    .line 112
    invoke-interface {p1}, Lcom/google/android/apps/gmm/car/m/h;->a()V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0, p1}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 114
    sget-object v1, Lcom/google/android/apps/gmm/car/m/o;->b:Lcom/google/android/apps/gmm/car/m/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v0, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    if-lez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iput-object v1, p0, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    .line 115
    :cond_1
    :goto_1
    return-void

    .line 114
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/o;)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/car/m/o;)V
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/google/android/apps/gmm/car/m/k;->a(Landroid/view/View;Lcom/google/android/apps/gmm/car/m/o;)V

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/car/m/h;->a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;

    move-result-object v0

    invoke-interface {v1, v0, p1}, Lcom/google/android/apps/gmm/car/m/k;->a(Landroid/view/View;Lcom/google/android/apps/gmm/car/m/o;)V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final c()Lcom/google/android/apps/gmm/car/m/h;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/h;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Tried to pop an empty stack."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 121
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/m/i;->e()V

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    .line 123
    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/h;->c()V

    .line 124
    sget-object v3, Lcom/google/android/apps/gmm/car/m/o;->c:Lcom/google/android/apps/gmm/car/m/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v4, v4, Lcom/google/android/apps/gmm/car/m/f;->a:I

    if-lez v4, :cond_3

    :goto_1
    if-eqz v1, :cond_4

    iput-object v3, p0, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    .line 126
    :cond_2
    :goto_2
    return-object v0

    :cond_3
    move v1, v2

    .line 124
    goto :goto_1

    :cond_4
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/o;)V

    goto :goto_2
.end method

.method public e()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    if-nez v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/m/h;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/m/h;->b()V

    .line 137
    :cond_0
    return-void
.end method
