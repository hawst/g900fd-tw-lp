.class public final enum Lcom/google/android/apps/gmm/map/o/b/f;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/o/b/f;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/o/b/f;

.field public static final enum b:Lcom/google/android/apps/gmm/map/o/b/f;

.field public static final enum c:Lcom/google/android/apps/gmm/map/o/b/f;

.field public static final enum d:Lcom/google/android/apps/gmm/map/o/b/f;

.field public static final enum e:Lcom/google/android/apps/gmm/map/o/b/f;

.field public static final enum f:Lcom/google/android/apps/gmm/map/o/b/f;

.field private static final synthetic g:[Lcom/google/android/apps/gmm/map/o/b/f;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/f;

    const-string v1, "TOPLEFT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/o/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->a:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 28
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/f;

    const-string v1, "TOPRIGHT"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/o/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->b:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/f;

    const-string v1, "BOTTOMLEFT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/o/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->c:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/f;

    const-string v1, "BOTTOMRIGHT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/map/o/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/f;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/map/o/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->e:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/f;

    const-string v1, "RIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/o/b/f;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->f:Lcom/google/android/apps/gmm/map/o/b/f;

    .line 26
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/o/b/f;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/b/f;->a:Lcom/google/android/apps/gmm/map/o/b/f;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/o/b/f;->b:Lcom/google/android/apps/gmm/map/o/b/f;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/o/b/f;->c:Lcom/google/android/apps/gmm/map/o/b/f;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/o/b/f;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/o/b/f;->e:Lcom/google/android/apps/gmm/map/o/b/f;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/o/b/f;->f:Lcom/google/android/apps/gmm/map/o/b/f;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->g:[Lcom/google/android/apps/gmm/map/o/b/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/o/b/f;
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/gmm/map/o/b/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/b/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/o/b/f;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/apps/gmm/map/o/b/f;->g:[Lcom/google/android/apps/gmm/map/o/b/f;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/o/b/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/o/b/f;

    return-object v0
.end method
