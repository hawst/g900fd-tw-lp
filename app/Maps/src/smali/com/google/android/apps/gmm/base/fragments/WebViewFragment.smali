.class public Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/activities/y;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:Ljava/lang/String;

.field c:Landroid/webkit/WebView;

.field d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

.field e:Lcom/google/android/apps/gmm/base/activities/c;

.field private f:Ljava/lang/String;

.field private g:Z

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Z)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;
    .locals 3

    .prologue
    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;-><init>()V

    .line 144
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 145
    const-string v2, "url"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v2, "loadAsResource"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 147
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 148
    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Landroid/text/SpannableString;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 98
    const/4 v0, 0x1

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/p;

    invoke-direct {v1, p3, v0, p4, p0}, Lcom/google/android/apps/gmm/base/fragments/p;-><init>(Ljava/lang/String;ZZLcom/google/android/apps/gmm/base/activities/c;)V

    invoke-virtual {p1}, Landroid/text/SpannableString;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    const/16 v3, 0x21

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 99
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/t;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/fragments/t;-><init>(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;
    .locals 4

    .prologue
    .line 159
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x32

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v1, v2

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "WebViewFragment is created with url="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gaiaService="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    new-instance v0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;-><init>()V

    .line 161
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 162
    const-string v2, "url"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v2, "authenticate"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 164
    const-string v2, "gaiaService"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 166
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->a()V

    .line 319
    return-void
.end method

.method a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 3

    .prologue
    .line 282
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/base/fragments/u;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/base/fragments/u;-><init>(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 300
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 171
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 173
    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->f:Ljava/lang/String;

    .line 174
    const-string v1, "loadAsResource"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->g:Z

    .line 175
    const-string v1, "authenticate"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->m:Z

    .line 176
    const-string v1, "gaiaService"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->b:Ljava/lang/String;

    .line 177
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    .line 178
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 184
    sget v1, Lcom/google/android/apps/gmm/h;->R:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 186
    sget v0, Lcom/google/android/apps/gmm/g;->eE:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->c:Landroid/webkit/WebView;

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->c:Landroid/webkit/WebView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->c:Landroid/webkit/WebView;

    new-instance v2, Lcom/google/android/apps/gmm/base/fragments/q;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/fragments/q;-><init>(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;)V

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->c:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 202
    sget v0, Lcom/google/android/apps/gmm/g;->cq:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->d:Lcom/google/android/apps/gmm/base/views/GmmProgressBar;

    new-array v2, v3, [Landroid/view/View;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->c:Landroid/webkit/WebView;

    aput-object v4, v2, v3

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/views/GmmProgressBar;->b:[Landroid/view/View;

    .line 205
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->g:Z

    if-eqz v0, :cond_1

    .line 206
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "#onCreateView()"

    invoke-virtual {v0, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->f:Ljava/lang/String;

    new-instance v4, Lcom/google/android/apps/gmm/base/fragments/r;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/base/fragments/r;-><init>(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;)V

    invoke-interface {v0, v3, v2, v4}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    .line 213
    :cond_0
    :goto_0
    return-object v1

    .line 207
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->m:Z

    if-eqz v0, :cond_3

    .line 208
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v3

    if-nez v3, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->c:Landroid/webkit/WebView;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->e:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v4, Lcom/google/android/apps/gmm/base/fragments/s;

    invoke-direct {v4, p0, v2, v3}, Lcom/google/android/apps/gmm/base/fragments/s;-><init>(Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;Ljava/lang/String;Landroid/accounts/Account;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v4, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->c:Landroid/webkit/WebView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 304
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 306
    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 307
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->getView()Landroid/view/View;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v3, v4, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 308
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v1, v3, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 309
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/WebViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "fullScreen"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :cond_0
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    const/4 v0, 0x0

    .line 310
    iget-object v3, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 311
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 312
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 314
    return-void
.end method
