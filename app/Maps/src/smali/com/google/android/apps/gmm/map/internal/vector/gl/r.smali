.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/r;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:[Lcom/google/android/apps/gmm/map/internal/vector/gl/q;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;)V
    .locals 10

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const-string v0, "TexturePool.initialize"

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/t;->a(Ljava/lang/String;)V

    .line 86
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a:[Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    .line 87
    const/4 v3, 0x0

    sget v4, Lcom/google/android/apps/gmm/f;->aS:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    const/4 v3, 0x1

    sget v4, Lcom/google/android/apps/gmm/f;->aT:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x2

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    mul-int/2addr v0, v1

    const/high16 v1, 0xc0000

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    const/4 v3, 0x3

    sget v4, Lcom/google/android/apps/gmm/f;->aW:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    const/4 v3, 0x4

    sget v4, Lcom/google/android/apps/gmm/f;->aY:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    const/4 v3, 0x5

    sget v4, Lcom/google/android/apps/gmm/f;->aR:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    .line 88
    :goto_1
    const-string v0, "TexturePool.initialize"

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/t;->b(Ljava/lang/String;)V

    .line 89
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v3, 0x3

    sget v4, Lcom/google/android/apps/gmm/f;->aV:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    const/4 v3, 0x4

    sget v4, Lcom/google/android/apps/gmm/f;->aX:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    const/4 v3, 0x5

    sget v4, Lcom/google/android/apps/gmm/f;->aR:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V

    goto :goto_1
.end method

.method private a(Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Landroid/content/res/Resources;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/s;I)V
    .locals 10

    .prologue
    .line 158
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    move/from16 v0, p9

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;-><init>(I)V

    .line 159
    if-eqz p6, :cond_1

    const/16 v2, 0x2901

    move v3, v2

    :goto_0
    if-eqz p7, :cond_2

    const/16 v2, 0x2901

    :goto_1
    iget-boolean v4, v1, Lcom/google/android/apps/gmm/v/ci;->p:Z

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput v3, v1, Lcom/google/android/apps/gmm/v/ci;->k:I

    iput v2, v1, Lcom/google/android/apps/gmm/v/ci;->l:I

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/v/ci;->m:Z

    .line 163
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->c:Z

    .line 164
    if-eqz p5, :cond_4

    .line 165
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    move-object/from16 v0, p8

    if-ne v0, v2, :cond_3

    .line 166
    invoke-static {p2, p4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v8, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    .line 179
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a:[Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    aput-object v1, v2, p3

    .line 180
    return-void

    .line 159
    :cond_1
    const v2, 0x812f

    move v3, v2

    goto :goto_0

    :cond_2
    const v2, 0x812f

    goto :goto_1

    .line 168
    :cond_3
    invoke-static {p2, p4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-nez v2, :cond_8

    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/o;

    invoke-static {v5, v2, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lcom/google/android/apps/gmm/map/internal/vector/gl/o;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    move-object v9, v2

    :goto_3
    invoke-virtual {v9}, Landroid/graphics/Bitmap;->extractAlpha()Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v8, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    invoke-virtual {v9}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    .line 171
    :cond_4
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->b:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    move-object/from16 v0, p8

    if-ne v0, v2, :cond_5

    .line 172
    invoke-static {p2, p4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    move-object v8, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    goto :goto_2

    .line 173
    :cond_5
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/s;->c:Lcom/google/android/apps/gmm/map/internal/vector/gl/s;

    move-object/from16 v0, p8

    if-ne v0, v2, :cond_6

    .line 174
    invoke-static {p2, p4}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-nez v2, :cond_7

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iget-object v6, p1, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/o;

    invoke-static {v5, v2, v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap$Config;Lcom/google/android/apps/gmm/map/internal/vector/gl/o;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    :goto_4
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v8, p1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/graphics/Bitmap;IIZZZLcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    goto :goto_2

    .line 176
    :cond_6
    invoke-virtual {v1, p2, p4, p1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/q;->a(Landroid/content/res/Resources;ILcom/google/android/apps/gmm/map/internal/vector/gl/a;)V

    goto/16 :goto_2

    :cond_7
    move-object v2, v5

    goto :goto_4

    :cond_8
    move-object v9, v5

    goto :goto_3
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/map/internal/vector/gl/q;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/r;->a:[Lcom/google/android/apps/gmm/map/internal/vector/gl/q;

    aget-object v0, v0, p1

    return-object v0
.end method
