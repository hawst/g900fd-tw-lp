.class public final Lcom/google/android/apps/gmm/car/i/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/car/m/h;


# static fields
.field static final a:Ljava/lang/String;

.field public static final b:I


# instance fields
.field final c:Lcom/google/android/apps/gmm/car/ad;

.field final d:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final e:Lcom/google/android/apps/gmm/car/w;

.field final f:Lcom/google/android/apps/gmm/car/i/l;

.field g:Landroid/view/View;

.field h:Lcom/google/android/gms/car/support/PagedListView;

.field final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/i/v;",
            ">;"
        }
    .end annotation
.end field

.field final j:Lcom/google/android/apps/gmm/car/i/b/e;

.field private final k:Lcom/google/maps/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:I

.field private final m:Lcom/google/android/apps/gmm/z/b/j;

.field private n:Lcom/google/android/apps/gmm/car/i/i;

.field private o:Lcom/google/android/apps/gmm/car/i/x;

.field private final p:Lcom/google/android/apps/gmm/car/i/k;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/car/i/k",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/bm;",
            ">;>;"
        }
    .end annotation
.end field

.field private final q:Lcom/google/android/apps/gmm/car/i/w;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/apps/gmm/car/i/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/i/o;->a:Ljava/lang/String;

    .line 55
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/i/o;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;I)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/car/i/o;-><init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;ILcom/google/maps/a/a;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;ILcom/google/maps/a/a;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/maps/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/z/b/j;

    sget-object v2, Lcom/google/b/f/t;->aj:Lcom/google/b/f/t;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/z/b/j;-><init>(Lcom/google/b/f/t;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->m:Lcom/google/android/apps/gmm/z/b/j;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->i:Ljava/util/List;

    .line 225
    new-instance v0, Lcom/google/android/apps/gmm/car/i/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/q;-><init>(Lcom/google/android/apps/gmm/car/i/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->p:Lcom/google/android/apps/gmm/car/i/k;

    .line 283
    new-instance v0, Lcom/google/android/apps/gmm/car/i/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/s;-><init>(Lcom/google/android/apps/gmm/car/i/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->j:Lcom/google/android/apps/gmm/car/i/b/e;

    .line 324
    new-instance v0, Lcom/google/android/apps/gmm/car/i/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/t;-><init>(Lcom/google/android/apps/gmm/car/i/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->q:Lcom/google/android/apps/gmm/car/i/w;

    .line 125
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/car/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 126
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    .line 127
    if-ltz p3, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 128
    :cond_2
    rsub-int/lit8 v0, p3, 0x3

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/car/i/o;->l:I

    .line 129
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/i/o;->k:Lcom/google/maps/a/a;

    .line 131
    new-instance v0, Lcom/google/android/apps/gmm/car/w;

    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    new-instance v2, Lcom/google/android/apps/gmm/car/z;

    .line 132
    iget-object v3, p1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    .line 133
    iget-object v4, p1, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v5, p1, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v5, v5, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/z;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/car/w;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->e:Lcom/google/android/apps/gmm/car/w;

    .line 134
    new-instance v0, Lcom/google/android/apps/gmm/car/i/l;

    iget-object v1, p1, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget v2, p0, Lcom/google/android/apps/gmm/car/i/o;->l:I

    mul-int/lit8 v2, v2, 0x3

    const/4 v3, 0x3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/i/l;-><init>(Lcom/google/android/libraries/curvular/bd;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    .line 136
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/car/m/k;)Landroid/view/View;
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    :goto_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/car/ao;->j:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    new-instance v1, Lcom/google/android/apps/gmm/car/i/p;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/i/p;-><init>(Lcom/google/android/apps/gmm/car/i/o;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/ao;->b(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->u:Lcom/google/android/apps/gmm/car/q;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/q;->d:Z

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/q;->a(F)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->m:Lcom/google/android/apps/gmm/z/b/j;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 176
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->bJ:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/o;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    return-object v0
.end method

.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/i/u;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->q:Lcom/google/android/apps/gmm/car/i/w;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/car/i/o;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/PagedListView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->h:Lcom/google/android/gms/car/support/PagedListView;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->h:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->a()Lcom/google/android/gms/car/support/PagedRecyclerView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedRecyclerView;->setClipChildren(Z)V

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->h:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->b()V

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->h:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->setAdapter(Landroid/support/v7/widget/bk;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->h:Lcom/google/android/gms/car/support/PagedListView;

    iget v1, p0, Lcom/google/android/apps/gmm/car/i/o;->l:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->setMaxPages(I)V

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    const-class v2, Lcom/google/android/apps/gmm/car/i/a/d;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/apps/gmm/l;->bo:I

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/car/i/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 153
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->l:Lcom/google/android/apps/gmm/car/bg;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 154
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->p:Lcom/google/android/apps/gmm/car/c;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 155
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 156
    iget-object v4, v4, Lcom/google/android/apps/gmm/car/ad;->d:Landroid/view/LayoutInflater;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/car/i/i;-><init>(Lcom/google/android/apps/gmm/car/bg;Lcom/google/android/apps/gmm/car/c;Lcom/google/android/apps/gmm/map/util/b/g;Landroid/view/LayoutInflater;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->n:Lcom/google/android/apps/gmm/car/i/i;

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->n:Lcom/google/android/apps/gmm/car/i/i;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->p:Lcom/google/android/apps/gmm/car/i/k;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 149
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->bn:I

    goto :goto_0

    .line 158
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/i;->a()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/i/i;->f:Lcom/google/android/apps/gmm/car/i/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/i;->b:Lcom/google/android/apps/gmm/car/bg;

    sget-object v1, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    sget-object v2, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/car/bg;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Z)Z

    .line 170
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->e:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/w;->a()V

    .line 171
    return-void

    .line 160
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/car/i/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 161
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->k:Lcom/google/android/apps/gmm/search/ah;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 162
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 163
    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 164
    iget-object v4, v4, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v4

    iget v5, p0, Lcom/google/android/apps/gmm/car/i/o;->l:I

    mul-int/lit8 v5, v5, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/i/x;-><init>(Lcom/google/android/apps/gmm/search/ah;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/p/b/a;Lcom/google/android/apps/gmm/hotels/a/b;I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->o:Lcom/google/android/apps/gmm/car/i/x;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->o:Lcom/google/android/apps/gmm/car/i/x;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/o;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/o;->k:Lcom/google/maps/a/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/o;->p:Lcom/google/android/apps/gmm/car/i/k;

    invoke-virtual {v0, v1, v2, v6, v3}, Lcom/google/android/apps/gmm/car/i/x;->a(Ljava/lang/String;Lcom/google/maps/a/a;[BLcom/google/android/apps/gmm/car/i/k;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->u:Lcom/google/android/apps/gmm/car/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/q;->a()V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->a()V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->q:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->j:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/ao;->b()V

    .line 193
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/i/o;->e()V

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->e:Lcom/google/android/apps/gmm/car/w;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/w;->b()V

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->n:Lcom/google/android/apps/gmm/car/i/i;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->n:Lcom/google/android/apps/gmm/car/i/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/i;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/i/i;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/i/i;->g:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 202
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/i/o;->n:Lcom/google/android/apps/gmm/car/i/i;

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->o:Lcom/google/android/apps/gmm/car/i/x;

    if-eqz v0, :cond_1

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->o:Lcom/google/android/apps/gmm/car/i/x;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/i/x;->a()V

    .line 206
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/i/o;->o:Lcom/google/android/apps/gmm/car/i/x;

    .line 209
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/i/o;->g:Landroid/view/View;

    .line 210
    iput-object v2, p0, Lcom/google/android/apps/gmm/car/i/o;->h:Lcom/google/android/gms/car/support/PagedListView;

    .line 211
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/car/m/e;
    .locals 1

    .prologue
    .line 215
    sget-object v0, Lcom/google/android/apps/gmm/car/m/e;->b:Lcom/google/android/apps/gmm/car/m/e;

    return-object v0
.end method

.method e()V
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/i/v;

    .line 220
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/i/v;->a:Lcom/google/android/apps/gmm/car/i/l;

    goto :goto_0

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/o;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 223
    return-void
.end method
