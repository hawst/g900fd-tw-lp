.class public Lcom/google/android/apps/gmm/navigation/i/q;
.super Lcom/google/android/apps/gmm/navigation/i/i;
.source "PG"


# static fields
.field private static final e:[I


# instance fields
.field private final c:Lcom/google/maps/g/a/fb;

.field private final d:Lcom/google/maps/g/a/fd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 120
    const/16 v0, 0xc

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/apps/gmm/l;->ez:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/apps/gmm/l;->eB:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/apps/gmm/l;->ei:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/apps/gmm/l;->ek:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/apps/gmm/l;->ee:I

    aput v2, v0, v1

    const/4 v1, 0x5

    sget v2, Lcom/google/android/apps/gmm/l;->eg:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lcom/google/android/apps/gmm/l;->eA:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lcom/google/android/apps/gmm/l;->eC:I

    aput v2, v0, v1

    const/16 v1, 0x8

    sget v2, Lcom/google/android/apps/gmm/l;->ej:I

    aput v2, v0, v1

    const/16 v1, 0x9

    sget v2, Lcom/google/android/apps/gmm/l;->el:I

    aput v2, v0, v1

    const/16 v1, 0xa

    sget v2, Lcom/google/android/apps/gmm/l;->ef:I

    aput v2, v0, v1

    const/16 v1, 0xb

    sget v2, Lcom/google/android/apps/gmm/l;->eh:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/i/q;->e:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;",
            "Lcom/google/maps/g/a/fb;",
            "Lcom/google/maps/g/a/fd;",
            ")V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/i/i;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 130
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p3, Lcom/google/maps/g/a/fb;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/q;->c:Lcom/google/maps/g/a/fb;

    .line 131
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p4, Lcom/google/maps/g/a/fd;

    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/i/q;->d:Lcom/google/maps/g/a/fd;

    .line 132
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/g/a/ez;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    return-object v0
.end method

.method public final b()Lcom/google/maps/g/a/fb;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/q;->c:Lcom/google/maps/g/a/fb;

    return-object v0
.end method

.method public final c()Lcom/google/maps/g/a/fd;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/q;->d:Lcom/google/maps/g/a/fd;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 141
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/q;->c:Lcom/google/maps/g/a/fb;

    sget-object v4, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    if-ne v0, v4, :cond_1

    move v0, v1

    .line 149
    :goto_0
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/i/q;->d:Lcom/google/maps/g/a/fd;

    sget-object v5, Lcom/google/maps/g/a/fd;->b:Lcom/google/maps/g/a/fd;

    if-ne v4, v5, :cond_2

    .line 150
    add-int/lit8 v0, v0, 0x2

    .line 157
    :cond_0
    :goto_1
    sget-object v3, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/navigation/i/q;->a(Lcom/google/maps/g/a/af;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v3

    .line 158
    if-eqz v3, :cond_4

    .line 159
    add-int/lit8 v0, v0, 0x6

    .line 160
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/i/q;->a:Landroid/content/Context;

    sget-object v5, Lcom/google/android/apps/gmm/navigation/i/q;->e:[I

    aget v0, v5, v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v4, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 162
    :goto_2
    return-object v0

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/q;->c:Lcom/google/maps/g/a/fb;

    sget-object v4, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    if-eq v0, v4, :cond_5

    move-object v0, v3

    .line 146
    goto :goto_2

    .line 151
    :cond_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/i/q;->d:Lcom/google/maps/g/a/fd;

    sget-object v5, Lcom/google/maps/g/a/fd;->d:Lcom/google/maps/g/a/fd;

    if-ne v4, v5, :cond_3

    .line 152
    add-int/lit8 v0, v0, 0x4

    goto :goto_1

    .line 153
    :cond_3
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/i/q;->d:Lcom/google/maps/g/a/fd;

    sget-object v5, Lcom/google/maps/g/a/fd;->c:Lcom/google/maps/g/a/fd;

    if-eq v4, v5, :cond_0

    move-object v0, v3

    .line 154
    goto :goto_2

    .line 162
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/i/q;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/q;->e:[I

    aget v0, v2, v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_0
.end method
