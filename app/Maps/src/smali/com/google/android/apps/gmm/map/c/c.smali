.class public Lcom/google/android/apps/gmm/map/c/c;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/c/d;",
            "Landroid/graphics/Picture;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/c/d;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(ILcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v1, "SvgPictureCache"

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/c/c;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v1, "SvgDrawableCache"

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/c/c;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 33
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/map/c/c;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;)V

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;IILcom/google/b/a/bw;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "II",
            "Lcom/google/b/a/bw",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)",
            "Landroid/graphics/drawable/Drawable;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v1, Lcom/google/android/apps/gmm/map/c/d;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/c/d;-><init>(Lcom/google/android/apps/gmm/map/c/c;Landroid/content/res/Resources;II)V

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/c/c;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 47
    if-nez v0, :cond_1

    .line 48
    monitor-enter p0

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/c/c;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 50
    if-nez v0, :cond_0

    if-eqz p4, :cond_0

    .line 51
    invoke-interface {p4}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 52
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/c/c;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 54
    :cond_0
    monitor-exit p0

    .line 56
    :cond_1
    return-object v0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Landroid/content/res/Resources;IILcom/google/b/a/bw;)Landroid/graphics/Picture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "II",
            "Lcom/google/b/a/bw",
            "<",
            "Landroid/graphics/Picture;",
            ">;)",
            "Landroid/graphics/Picture;"
        }
    .end annotation

    .prologue
    .line 65
    new-instance v1, Lcom/google/android/apps/gmm/map/c/d;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/c/d;-><init>(Lcom/google/android/apps/gmm/map/c/c;Landroid/content/res/Resources;II)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/c/c;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Picture;

    .line 67
    if-nez v0, :cond_1

    .line 68
    monitor-enter p0

    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/c/c;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Picture;

    .line 70
    if-nez v0, :cond_0

    if-eqz p4, :cond_0

    .line 71
    invoke-interface {p4}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Picture;

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/c/c;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 74
    :cond_0
    monitor-exit p0

    .line 76
    :cond_1
    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
