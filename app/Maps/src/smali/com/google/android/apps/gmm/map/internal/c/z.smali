.class public Lcom/google/android/apps/gmm/map/internal/c/z;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/internal/c/bk;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/aa;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/google/android/apps/gmm/map/internal/c/d;

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bk;

    const/4 v1, 0x0

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/z;->a:Lcom/google/android/apps/gmm/map/internal/c/bk;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/aa;",
            ">;",
            "Lcom/google/android/apps/gmm/map/internal/c/d;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    if-nez p1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Parameter labelElements can not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v6

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_5

    .line 57
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    .line 58
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-eqz v3, :cond_3

    move v3, v1

    :goto_1
    if-eqz v3, :cond_1

    .line 59
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    :cond_1
    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->a:I

    const/16 v3, 0x8

    and-int/2addr v0, v3

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    .line 62
    const/16 v0, 0xa

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 56
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    move v3, v2

    .line 58
    goto :goto_1

    :cond_4
    move v0, v2

    .line 61
    goto :goto_2

    .line 65
    :cond_5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    .line 69
    return-void
.end method

.method public static a(Lcom/google/maps/b/a/al;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;
    .locals 16

    .prologue
    .line 108
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/maps/b/a/al;->a:Lcom/google/maps/b/a/cs;

    iget v2, v1, Lcom/google/maps/b/a/cs;->b:I

    if-ltz v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 109
    new-instance v1, Lcom/google/maps/b/a/an;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/google/maps/b/a/an;-><init>(Lcom/google/maps/b/a/al;)V

    invoke-interface {v1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/b/a/ak;

    .line 110
    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v4, v1, Lcom/google/maps/b/a/ak;->a:Lcom/google/maps/b/a/cr;

    iget-boolean v4, v4, Lcom/google/maps/b/a/cr;->c:Z

    if-eqz v4, :cond_11

    invoke-virtual {v1}, Lcom/google/maps/b/a/ak;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    move-object v13, v2

    :goto_2
    iget-object v2, v1, Lcom/google/maps/b/a/ak;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v2, v2, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/google/maps/b/a/ak;->b:Lcom/google/maps/b/a/cz;

    iget v4, v2, Lcom/google/maps/b/a/cz;->b:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v5

    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bk;

    invoke-direct {v2, v5, v4}, Lcom/google/android/apps/gmm/map/internal/c/bk;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bi;I)V

    move-object v10, v2

    :goto_3
    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v2, :cond_5

    const/4 v2, 0x0

    move-object v11, v2

    :goto_4
    iget-object v2, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_12

    if-eqz v13, :cond_2

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_6
    if-eqz v2, :cond_12

    iget-object v2, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->j:Lcom/google/android/apps/gmm/map/internal/c/bm;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bm;->b:Z

    if-eqz v2, :cond_12

    :cond_2
    iget-object v2, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    move-object v12, v2

    :goto_7
    iget-object v1, v1, Lcom/google/maps/b/a/ak;->c:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    if-eqz v1, :cond_9

    const/4 v1, 0x1

    :goto_8
    if-eqz v1, :cond_3

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/16 v2, 0x8

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/high16 v9, -0x40800000    # -1.0f

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-object v1, v11, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_9
    if-eqz v1, :cond_b

    const/4 v2, 0x1

    :goto_a
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v3, :cond_c

    const/4 v5, 0x0

    :goto_b
    iget-object v6, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget v7, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    iget-object v3, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v3, :cond_e

    const/4 v8, 0x0

    :goto_c
    const/4 v9, 0x0

    move-object v3, v12

    move-object v4, v13

    invoke-direct/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    invoke-interface {v14, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_4
    move-object/from16 v10, p2

    goto :goto_3

    :cond_5
    iget-object v2, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v4

    if-nez v4, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v2

    move-object v11, v2

    goto :goto_4

    :cond_6
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v4, 0x0

    aget-object v2, v2, v4

    move-object v11, v2

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    goto :goto_5

    :cond_8
    const/4 v2, 0x0

    goto :goto_6

    :cond_9
    const/4 v1, 0x0

    goto :goto_8

    :cond_a
    const/4 v1, 0x0

    goto :goto_9

    :cond_b
    const/4 v2, 0x2

    goto :goto_a

    :cond_c
    iget-object v3, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v4

    if-nez v4, :cond_d

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v5

    goto :goto_b

    :cond_d
    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v4, 0x0

    aget-object v5, v3, v4

    goto :goto_b

    :cond_e
    iget-object v3, v10, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v8, v3, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto :goto_c

    .line 113
    :cond_f
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/maps/b/a/al;->b:Lcom/google/maps/b/a/cz;

    iget-boolean v1, v1, Lcom/google/maps/b/a/cz;->c:Z

    if-eqz v1, :cond_10

    .line 115
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/maps/b/a/al;->b:Lcom/google/maps/b/a/cz;

    iget v1, v1, Lcom/google/maps/b/a/cz;->b:I

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/c/d;->a(I)Lcom/google/android/apps/gmm/map/internal/c/d;

    move-result-object v1

    .line 122
    :goto_d
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/z;

    invoke-direct {v2, v14, v1}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    return-object v2

    .line 117
    :cond_10
    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    goto :goto_d

    :cond_11
    move-object v13, v2

    goto/16 :goto_2

    :cond_12
    move-object v12, v3

    goto/16 :goto_7
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Lcom/google/android/apps/gmm/map/internal/c/bk;)Lcom/google/android/apps/gmm/map/internal/c/z;
    .locals 13

    .prologue
    .line 87
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v10

    .line 88
    if-ltz v10, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v10, :cond_1d

    .line 90
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v0, 0x2

    and-int/2addr v0, v1

    if-eqz v0, :cond_6

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_2

    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v3

    :cond_2
    const/4 v0, 0x4

    and-int/2addr v0, v1

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_8

    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/bk;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-result-object v0

    move-object v7, v0

    :goto_4
    const/4 v0, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_d

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_3

    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v0, :cond_e

    const/4 v0, 0x0

    :goto_6
    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-eqz v4, :cond_10

    const/4 v4, 0x1

    :goto_7
    if-eqz v4, :cond_11

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->l:Lcom/google/android/apps/gmm/map/internal/c/p;

    :cond_3
    :goto_8
    const/4 v8, 0x0

    const/16 v0, 0x10

    and-int/2addr v0, v1

    if-eqz v0, :cond_13

    const/4 v0, 0x1

    :goto_9
    if-eqz v0, :cond_4

    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    int-to-float v0, v0

    const/high16 v4, 0x41000000    # 8.0f

    div-float v8, v0, v4

    :cond_4
    const/16 v0, 0x20

    and-int/2addr v0, v1

    if-eqz v0, :cond_14

    const/4 v0, 0x1

    :goto_a
    if-eqz v0, :cond_5

    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    :cond_5
    const/16 v0, 0x8

    and-int/2addr v0, v1

    if-eqz v0, :cond_15

    const/4 v0, 0x1

    :goto_b
    if-eqz v0, :cond_19

    const/16 v0, 0x8

    if-eq v1, v0, :cond_19

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    xor-int/lit8 v1, v1, 0x8

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v4, :cond_16

    const/4 v4, 0x0

    :goto_c
    iget-object v5, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget v6, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    iget-object v12, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v12, :cond_18

    const/4 v7, 0x0

    :goto_d
    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    const/16 v1, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/4 v7, 0x0

    const/high16 v8, -0x40800000    # -1.0f

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    :goto_e
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_1

    .line 90
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :cond_8
    const/4 v0, 0x2

    and-int/2addr v0, v1

    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_f
    if-eqz v0, :cond_b

    const/4 v0, 0x1

    and-int/2addr v0, v1

    if-eqz v0, :cond_a

    const/4 v0, 0x1

    :goto_10
    if-nez v0, :cond_b

    const/4 v0, 0x1

    :goto_11
    if-eqz v0, :cond_c

    move-object v7, p2

    goto/16 :goto_4

    :cond_9
    const/4 v0, 0x0

    goto :goto_f

    :cond_a
    const/4 v0, 0x0

    goto :goto_10

    :cond_b
    const/4 v0, 0x0

    goto :goto_11

    :cond_c
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/z;->a:Lcom/google/android/apps/gmm/map/internal/c/bk;

    move-object v7, v0

    goto/16 :goto_4

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_e
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v4

    if-nez v4, :cond_f

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    goto/16 :goto_6

    :cond_f
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v4, 0x0

    aget-object v0, v0, v4

    goto/16 :goto_6

    :cond_10
    const/4 v4, 0x0

    goto/16 :goto_7

    :cond_11
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bs;->d:Lcom/google/android/apps/gmm/map/internal/c/o;

    if-eqz v4, :cond_12

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bs;->d:Lcom/google/android/apps/gmm/map/internal/c/o;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v4

    if-eqz v4, :cond_12

    const/4 v4, 0x1

    :goto_12
    if-nez v4, :cond_3

    const-string v4, "LabelGroup"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x22

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Style doesn\'t have an icon. style="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v0, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_8

    :cond_12
    const/4 v4, 0x0

    goto :goto_12

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_9

    :cond_14
    const/4 v0, 0x0

    goto/16 :goto_a

    :cond_15
    const/4 v0, 0x0

    goto/16 :goto_b

    :cond_16
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v5, v5

    if-nez v5, :cond_17

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    goto/16 :goto_c

    :cond_17
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    goto/16 :goto_c

    :cond_18
    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto/16 :goto_d

    :cond_19
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v4, :cond_1a

    const/4 v4, 0x0

    :goto_13
    iget-object v5, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget v6, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->b:I

    iget-object v12, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    if-nez v12, :cond_1c

    const/4 v7, 0x0

    :goto_14
    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/internal/c/aa;-><init>(ILcom/google/android/apps/gmm/map/internal/c/p;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/be;Lcom/google/android/apps/gmm/map/internal/c/bi;ILjava/lang/String;F)V

    invoke-interface {v11, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_e

    :cond_1a
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v5, v5

    if-nez v5, :cond_1b

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    goto :goto_13

    :cond_1b
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    goto :goto_13

    :cond_1c
    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bk;->a:Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    goto :goto_14

    .line 95
    :cond_1d
    const/4 v0, 0x1

    if-le v10, v0, :cond_1e

    .line 97
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/internal/c/d;->a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/d;

    move-result-object v0

    .line 102
    :goto_15
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/z;

    invoke-direct {v1, v11, v0}, Lcom/google/android/apps/gmm/map/internal/c/z;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/internal/c/d;)V

    return-object v1

    .line 99
    :cond_1e
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/d;->b:Lcom/google/android/apps/gmm/map/internal/c/d;

    goto :goto_15
.end method


# virtual methods
.method public final a()I
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 181
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    move v5, v3

    move v6, v3

    :goto_0
    if-ge v5, v7, :cond_8

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/aa;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    if-nez v1, :cond_0

    move v1, v3

    :goto_1
    add-int/lit8 v2, v1, 0x30

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->c:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v3

    :goto_2
    add-int/2addr v2, v1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->f:Ljava/lang/String;

    if-nez v1, :cond_6

    move v1, v3

    :goto_3
    add-int/2addr v1, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->d:Lcom/google/android/apps/gmm/map/internal/c/be;

    if-nez v0, :cond_7

    move v0, v3

    :goto_4
    add-int/2addr v0, v1

    add-int v1, v6, v0

    .line 182
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v6, v1

    goto :goto_0

    .line 183
    :cond_0
    iget-object v8, v0, Lcom/google/android/apps/gmm/map/internal/c/aa;->b:Lcom/google/android/apps/gmm/map/internal/c/p;

    iget-object v1, v8, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    if-eqz v1, :cond_2

    move v2, v3

    move v4, v3

    :goto_5
    iget-object v1, v8, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    iget-object v1, v8, Lcom/google/android/apps/gmm/map/internal/c/p;->b:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/q;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/q;->a:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v3

    :goto_6
    add-int/lit8 v1, v1, 0x14

    add-int/2addr v4, v1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x28

    goto :goto_6

    :cond_2
    move v4, v3

    :cond_3
    iget-object v1, v8, Lcom/google/android/apps/gmm/map/internal/c/p;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v3

    :goto_7
    add-int/lit8 v1, v1, 0x14

    add-int/2addr v1, v4

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x28

    goto :goto_7

    :cond_5
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x28

    goto :goto_2

    :cond_6
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v1, v1, 0x28

    goto :goto_3

    :cond_7
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v0

    goto :goto_4

    .line 185
    :cond_8
    add-int/lit8 v0, v6, 0x18

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    if-nez v1, :cond_9

    :goto_8
    add-int/2addr v0, v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    .line 186
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/d;->a()I

    move-result v1

    add-int/2addr v0, v1

    return v0

    .line 185
    :cond_9
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x4

    shl-int/lit8 v1, v1, 0x2

    shl-int/lit8 v1, v1, 0x1

    add-int/lit8 v3, v1, 0x28

    goto :goto_8
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 160
    if-ne p0, p1, :cond_1

    .line 161
    const/4 v0, 0x1

    .line 177
    :cond_0
    :goto_0
    return v0

    .line 163
    :cond_1
    if-eqz p1, :cond_0

    .line 166
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 169
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/z;

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    if-nez v1, :cond_3

    .line 171
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    if-nez v1, :cond_0

    .line 177
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 154
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    return v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/c/d;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 257
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "nameText"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    .line 258
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "labelElements"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->b:Ljava/util/List;

    .line 259
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "alignment"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/z;->c:Lcom/google/android/apps/gmm/map/internal/c/d;

    .line 260
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 261
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
