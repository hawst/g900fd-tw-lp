.class public Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 122
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 113
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 102
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->I:I

    if-ne v0, v1, :cond_1

    .line 103
    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ownerresponse/a/a;->j()Lcom/google/android/libraries/curvular/cf;

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 104
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->U:I

    if-ne v0, v1, :cond_2

    .line 105
    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ownerresponse/a/a;->k()Lcom/google/android/libraries/curvular/cf;

    goto :goto_0

    .line 106
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->cv:I

    if-ne v0, v1, :cond_0

    .line 107
    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ownerresponse/a/a;->l()Lcom/google/android/libraries/curvular/cf;

    goto :goto_0
.end method

.method public onFinishInflate()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/apps/gmm/g;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 48
    sget v0, Lcom/google/android/apps/gmm/g;->r:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 49
    sget v0, Lcom/google/android/apps/gmm/g;->cw:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 50
    sget v0, Lcom/google/android/apps/gmm/g;->cR:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 51
    sget v0, Lcom/google/android/apps/gmm/g;->cf:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 52
    sget v0, Lcom/google/android/apps/gmm/g;->cd:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 53
    sget v0, Lcom/google/android/apps/gmm/g;->cS:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 55
    sget v0, Lcom/google/android/apps/gmm/g;->I:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 56
    sget v0, Lcom/google/android/apps/gmm/g;->U:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 57
    sget v0, Lcom/google/android/apps/gmm/g;->cv:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseView;->findViewById(I)Landroid/view/View;

    .line 58
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/place/ownerresponse/a/a;->a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;

    .line 118
    return-void
.end method
