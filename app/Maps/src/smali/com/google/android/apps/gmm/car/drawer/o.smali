.class public Lcom/google/android/apps/gmm/car/drawer/o;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I


# instance fields
.field b:Landroid/view/View;

.field c:Lcom/google/android/gms/car/support/PagedListView;

.field private final d:Lcom/google/android/libraries/curvular/bd;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/drawer/s;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/car/i/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/google/android/libraries/curvular/cq;->b()I

    move-result v0

    sput v0, Lcom/google/android/apps/gmm/car/drawer/o;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/bd;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/drawer/s;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->d:Lcom/google/android/libraries/curvular/bd;

    .line 70
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/drawer/o;->e:Ljava/util/List;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/car/i/l;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/car/i/l;-><init>(Lcom/google/android/libraries/curvular/bd;II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    .line 73
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->d:Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/car/drawer/q;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->b:Landroid/view/View;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->b:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/car/drawer/p;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/drawer/p;-><init>(Lcom/google/android/apps/gmm/car/drawer/o;)V

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->b:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/car/drawer/o;->a:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/PagedListView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->c:Lcom/google/android/gms/car/support/PagedListView;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->c:Lcom/google/android/gms/car/support/PagedListView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->setAdapter(Landroid/support/v7/widget/bk;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->c:Lcom/google/android/gms/car/support/PagedListView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/PagedListView;->setMaxPages(I)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->c:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->a()Lcom/google/android/gms/car/support/PagedRecyclerView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/support/PagedRecyclerView;->setClipChildren(Z)V

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->c:Lcom/google/android/gms/car/support/PagedListView;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/PagedListView;->a()Lcom/google/android/gms/car/support/PagedRecyclerView;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/support/PagedRecyclerView;->setClipToPadding(Z)V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->f:Lcom/google/android/apps/gmm/car/i/l;

    const-class v1, Lcom/google/android/apps/gmm/car/drawer/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/o;->e:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/o;->b:Landroid/view/View;

    return-object v0
.end method
