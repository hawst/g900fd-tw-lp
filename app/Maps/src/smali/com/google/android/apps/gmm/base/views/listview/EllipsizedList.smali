.class public Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field public b:I

.field public c:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 100
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    iput v2, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b:I

    .line 83
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    .line 89
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->d:I

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/n;->k:[I

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 104
    sget v1, Lcom/google/android/apps/gmm/n;->l:I

    .line 105
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    .line 106
    sget v1, Lcom/google/android/apps/gmm/n;->m:I

    .line 107
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 108
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 110
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b:I

    if-eq v0, v1, :cond_0

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->requestLayout()V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->invalidate()V

    .line 111
    :cond_0
    return-void
.end method

.method private c()Landroid/view/View;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 128
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 129
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 130
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->d:I

    if-ne v3, v4, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v3

    if-ne v3, p0, :cond_0

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_1

    move-object v0, v2

    .line 134
    :goto_2
    return-object v0

    :cond_0
    move v3, v1

    .line 130
    goto :goto_1

    .line 128
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 165
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b:I

    return v0
.end method

.method public a(I)I
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 267
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;Z)Z

    move v0, v1

    move v2, v3

    move v4, v1

    .line 271
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildCount()I

    move-result v5

    if-ge v0, v5, :cond_3

    .line 272
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 273
    invoke-virtual {v6}, Landroid/view/View;->getId()I

    move-result v5

    iget v7, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->d:I

    if-ne v5, v7, :cond_2

    invoke-virtual {v6}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-ne v5, p0, :cond_2

    move v5, v3

    :goto_1
    if-nez v5, :cond_1

    .line 274
    if-nez v2, :cond_0

    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a()I

    move-result v2

    add-int/2addr v4, v2

    .line 279
    :cond_0
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v4, v2

    move v2, v1

    .line 271
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v5, v1

    .line 273
    goto :goto_1

    .line 283
    :cond_3
    return v4
.end method

.method public a(IIII)I
    .locals 0

    .prologue
    .line 295
    return p4
.end method

.method public b()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 363
    move v1, v0

    .line 364
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 365
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 366
    invoke-static {v2}, Lcom/google/android/apps/gmm/util/r;->c(Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 367
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 364
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 370
    :cond_1
    return v1
.end method

.method public final b(I)I
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 305
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c()Landroid/view/View;

    move-result-object v6

    .line 309
    if-eqz v6, :cond_8

    .line 310
    invoke-static {v6, v3}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;Z)Z

    .line 311
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    :goto_0
    move v2, v3

    move v4, v0

    move v0, v1

    .line 315
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildCount()I

    move-result v5

    if-ge v0, v5, :cond_6

    .line 316
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 317
    invoke-virtual {v7}, Landroid/view/View;->getId()I

    move-result v5

    iget v8, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->d:I

    if-ne v5, v8, :cond_1

    invoke-virtual {v7}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    if-ne v5, p0, :cond_1

    move v5, v3

    :goto_2
    if-nez v5, :cond_5

    .line 318
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 326
    if-eqz v2, :cond_2

    .line 327
    if-eqz v6, :cond_7

    .line 328
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a()I

    move-result v2

    add-int/2addr v2, v5

    .line 339
    :goto_3
    add-int v5, v4, v2

    if-le v5, p1, :cond_4

    .line 341
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 342
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 343
    invoke-virtual {v2}, Landroid/view/View;->getId()I

    move-result v5

    iget v6, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->d:I

    if-ne v5, v6, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-ne v2, p0, :cond_3

    move v2, v3

    :goto_5
    if-nez v2, :cond_0

    .line 344
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;Z)Z

    .line 341
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_1
    move v5, v1

    .line 317
    goto :goto_2

    .line 334
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a()I

    move-result v2

    add-int/2addr v2, v5

    goto :goto_3

    :cond_3
    move v2, v1

    .line 343
    goto :goto_5

    .line 350
    :cond_4
    add-int/2addr v4, v2

    move v2, v1

    .line 315
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 354
    :cond_6
    return v4

    :cond_7
    move v2, v5

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_0
.end method

.method public onLayout(ZIIII)V
    .locals 10
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 414
    sub-int v3, p4, p2

    .line 415
    sub-int v0, p5, p3

    .line 416
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingBottom()I

    move-result v1

    sub-int v4, v0, v1

    .line 418
    sget-boolean v0, Lcom/google/android/apps/gmm/map/util/c;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingStart()I

    move-result v0

    .line 419
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 420
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    .line 421
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 422
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingTop()I

    move-result v2

    sub-int v8, v4, v7

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v8, v2

    .line 423
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_1

    sub-int v2, v3, v1

    sub-int/2addr v2, v6

    .line 427
    :goto_2
    add-int v9, v2, v6

    add-int/2addr v7, v8

    invoke-virtual {v0, v2, v8, v9, v7}, Landroid/view/View;->layout(IIII)V

    .line 429
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a()I

    move-result v0

    add-int/2addr v0, v6

    add-int/2addr v0, v1

    move v1, v0

    .line 430
    goto :goto_1

    .line 418
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingLeft()I

    move-result v0

    goto :goto_0

    :cond_1
    move v2, v1

    .line 423
    goto :goto_2

    .line 431
    :cond_2
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 202
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->setupMeasuring()V

    .line 204
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingRight()I

    move-result v1

    add-int v3, v0, v1

    .line 205
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getPaddingBottom()I

    move-result v1

    add-int v4, v0, v1

    .line 209
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 208
    invoke-virtual {p0, v0, p2}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->measureChildren(II)V

    .line 213
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7fffffff

    .line 218
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a(I)I

    move-result v1

    .line 221
    if-le v1, v0, :cond_5

    .line 223
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->a(IIII)I

    move-result v1

    .line 224
    if-le v1, v0, :cond_5

    .line 225
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b(I)I

    move-result v0

    .line 229
    :goto_1
    add-int/2addr v0, v3

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->b()I

    move-result v1

    add-int/2addr v1, v4

    .line 237
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getSuggestedMinimumWidth()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 236
    invoke-static {v0, p1, v2}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->resolveSizeAndState(III)I

    move-result v0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getSuggestedMinimumHeight()I

    move-result v3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 238
    invoke-static {v1, p2, v2}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->resolveSizeAndState(III)I

    move-result v1

    .line 235
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->setMeasuredDimension(II)V

    .line 241
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    move v0, v2

    :goto_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getId()I

    move-result v1

    iget v5, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->d:I

    if-ne v1, v5, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_2

    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_0

    invoke-static {v4}, Lcom/google/android/apps/gmm/util/r;->c(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3, v4}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 215
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    sub-int/2addr v0, v3

    goto :goto_0

    :cond_2
    move v1, v2

    .line 241
    goto :goto_3

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/util/r;->c(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v3, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_4
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->c:Lcom/google/b/c/cv;

    .line 242
    return-void

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public setupMeasuring()V
    .locals 3

    .prologue
    .line 252
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/listview/EllipsizedList;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;Z)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 253
    :cond_0
    return-void
.end method
