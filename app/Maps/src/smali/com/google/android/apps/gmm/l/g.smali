.class public Lcom/google/android/apps/gmm/l/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/ab;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 43
    const-string v1, "geo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "peterparker"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    const/4 v0, 0x0

    .line 46
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 87
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v2, "geo"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "peterparker"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v4

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v5

    goto :goto_0

    .line 88
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v0, v5

    :goto_1
    if-eqz v0, :cond_4

    :goto_2
    return-object v1

    :cond_3
    move v0, v4

    goto :goto_1

    :cond_4
    if-nez v1, :cond_7

    new-instance v0, Lcom/google/android/apps/gmm/l/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/l/h;-><init>()V

    :goto_3
    const-string v3, "?"

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    const/4 v3, -0x1

    if-ne v6, v3, :cond_8

    move-object v3, v2

    move-object v2, v1

    :goto_4
    invoke-static {v3}, Lcom/google/android/apps/gmm/util/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/apps/gmm/l/ac;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v3

    if-eqz v2, :cond_b

    invoke-virtual {v0, v2}, Landroid/net/UrlQuerySanitizer;->parseQuery(Ljava/lang/String;)V

    const-string v2, "z"

    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-static {v0, v2, v6, v7}, Lcom/google/android/apps/gmm/l/ac;->a(Landroid/net/UrlQuerySanitizer;Ljava/lang/String;FF)Ljava/lang/Float;

    move-result-object v2

    const-string v6, "q"

    invoke-virtual {v0, v6}, Landroid/net/UrlQuerySanitizer;->getValue(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/l/ac;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/l/af;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/l/af;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v6, Lcom/google/android/apps/gmm/l/af;->a:Ljava/lang/String;

    :cond_5
    :goto_5
    invoke-static {}, Lcom/google/android/apps/gmm/l/u;->b()Lcom/google/android/apps/gmm/l/z;

    move-result-object v6

    iput-object v2, v6, Lcom/google/android/apps/gmm/l/z;->h:Ljava/lang/Float;

    iput-object v3, v6, Lcom/google/android/apps/gmm/l/z;->f:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_9

    :cond_6
    move v2, v5

    :goto_6
    if-eqz v2, :cond_a

    sget-object v0, Lcom/google/android/apps/gmm/l/y;->f:Lcom/google/android/apps/gmm/l/y;

    iput-object v0, v6, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v1

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3

    :cond_8
    invoke-virtual {v2, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_9
    move v2, v4

    goto :goto_6

    :cond_a
    sget-object v2, Lcom/google/android/apps/gmm/l/y;->c:Lcom/google/android/apps/gmm/l/y;

    iput-object v2, v6, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    iput-object v0, v6, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    iput-object v1, v6, Lcom/google/android/apps/gmm/l/z;->e:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v1

    goto :goto_2

    :cond_b
    move-object v0, v1

    move-object v2, v1

    goto :goto_5
.end method
