.class public Lcom/google/android/apps/gmm/map/internal/c/bb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Z

.field public c:Lcom/google/android/apps/gmm/map/b/a/q;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    .line 38
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bb;->b:Z

    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/internal/c/bb;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bb;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 40
    return-void
.end method

.method private a()Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 109
    :cond_0
    :goto_0
    return-object v0

    .line 100
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 101
    array-length v2, v1

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    aget-object v2, v1, v4

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    aget-object v2, v1, v5

    const-string v3, "0"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    const/4 v2, 0x0

    :try_start_0
    aget-object v2, v1, v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/u;->b(Ljava/lang/String;)I

    move-result v2

    .line 106
    const/4 v3, 0x1

    aget-object v1, v1, v3

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/u;->b(Ljava/lang/String;)I

    move-result v1

    .line 107
    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 109
    :catch_0
    move-exception v1

    goto :goto_0
.end method
