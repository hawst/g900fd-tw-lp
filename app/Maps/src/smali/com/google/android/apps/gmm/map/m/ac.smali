.class public Lcom/google/android/apps/gmm/map/m/ac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/m/l;


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/m/m;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->b:Z

    .line 9
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->c:Z

    .line 10
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->d:Z

    .line 11
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->e:Z

    .line 12
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->f:Z

    .line 13
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->g:Z

    .line 14
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->h:Z

    .line 15
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->i:Z

    .line 16
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->j:Z

    .line 17
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/ac;->k:Z

    .line 18
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/ac;->l:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    .line 106
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/m;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->a:Lcom/google/android/apps/gmm/map/m/m;

    .line 101
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->b:Z

    .line 111
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 140
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->h:Z

    .line 141
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->b:Z

    return v0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 155
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->b:Z

    .line 156
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->c:Z

    .line 157
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->d:Z

    .line 158
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->e:Z

    .line 159
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->f:Z

    .line 160
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->g:Z

    .line 161
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->h:Z

    .line 162
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->i:Z

    .line 163
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/m/ac;->j:Z

    .line 164
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 183
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->c:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 188
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->d:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->e:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->f:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->g:Z

    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 208
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->h:Z

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 213
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->i:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/ac;->j:Z

    return v0
.end method
