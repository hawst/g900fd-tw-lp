.class final enum Lcom/google/android/apps/gmm/map/internal/c/by;
.super Lcom/google/android/apps/gmm/map/internal/c/bv;
.source "PG"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/c/bv;-><init>(Ljava/lang/String;I)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/bu;
    .locals 10

    .prologue
    const/16 v9, 0x15

    const/16 v8, 0xc

    const/16 v6, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 62
    invoke-virtual {p1, v1, v9}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(I)Lcom/google/android/apps/gmm/map/b/a/ai;

    move-result-object v0

    .line 63
    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v0, v3, :cond_8

    .line 64
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/ci;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/internal/c/ci;-><init>()V

    .line 65
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    invoke-virtual {p1, v6}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 67
    const/16 v0, 0x1c

    invoke-virtual {p1, v6, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 66
    iput-object v0, v4, Lcom/google/android/apps/gmm/map/internal/c/ci;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 69
    :cond_1
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v8}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    move v3, v2

    .line 70
    :goto_2
    if-ge v3, v5, :cond_4

    .line 71
    invoke-virtual {p1, v8, v3, v9}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v0, v6

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/internal/c/ci;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    move v0, v2

    .line 65
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    .line 73
    :cond_4
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/ci;->a()Lcom/google/android/apps/gmm/map/internal/c/ch;

    move-result-object v0

    .line 74
    sget-object v3, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v4, Lcom/google/android/apps/gmm/map/b/a/ai;->p:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v3, v4, :cond_6

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/ch;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v3, :cond_5

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/ch;->b:[Ljava/lang/Integer;

    array-length v3, v3

    if-eqz v3, :cond_6

    :cond_5
    :goto_3
    if-eqz v1, :cond_7

    .line 76
    :goto_4
    return-object v0

    :cond_6
    move v1, v2

    .line 74
    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    .line 76
    :cond_8
    const/4 v0, 0x0

    goto :goto_4
.end method
