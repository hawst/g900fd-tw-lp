.class public Lcom/google/android/apps/gmm/navigation/navui/StepCueView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/b/f;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

.field public final b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

.field public c:Lcom/google/android/apps/gmm/map/r/a/ag;

.field public d:Lcom/google/android/apps/gmm/navigation/navui/ay;

.field public e:Z

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:F

.field public k:Z

.field public l:I

.field public m:F

.field public n:F

.field public o:F

.field public p:Lcom/google/android/apps/gmm/navigation/navui/az;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 64
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/h;->aa:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 66
    sget v0, Lcom/google/android/apps/gmm/g;->bS:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    .line 67
    sget v0, Lcom/google/android/apps/gmm/g;->bT:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setMaxLines(I)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setSingleLine()V

    .line 72
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    .line 73
    return-void
.end method

.method public static a(I)I
    .locals 2

    .prologue
    const v0, 0x800005

    const v1, 0x800003

    .line 150
    packed-switch p0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    .line 160
    :goto_0
    :pswitch_1
    return v0

    .line 152
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 156
    goto :goto_0

    .line 150
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;FIILjava/util/Collection;I)Ljava/lang/CharSequence;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;",
            "FII",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;I)",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 345
    new-instance v4, Landroid/text/TextPaint;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    .line 346
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v4, v0}, Landroid/text/TextPaint;->setTextScaleX(F)V

    .line 347
    invoke-virtual {v4, p2}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->d:Lcom/google/android/apps/gmm/navigation/navui/ay;

    iget-boolean v6, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->k:Z

    iget v7, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->l:I

    const/4 v8, 0x1

    iget v9, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->m:F

    iget v10, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->n:F

    iget v11, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->o:F

    move-object/from16 v1, p5

    move/from16 v2, p3

    move/from16 v3, p4

    move/from16 v5, p6

    move-object v12, p0

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;IILandroid/text/TextPaint;IZIZFFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 13

    .prologue
    const/4 v7, 0x2

    const/16 v12, 0x8

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->c:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    .line 209
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->getVisibility()I

    move-result v1

    if-eq v1, v12, :cond_9

    :cond_0
    :goto_1
    invoke-virtual {v0, v8}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setSingleLine(Z)V

    .line 210
    return-void

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->f:I

    int-to-float v0, v0

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->c:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->k:Z

    if-eqz v1, :cond_2

    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v1, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v3, Landroid/text/style/StyleSpan;

    invoke-direct {v3, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, v2, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    const-string v0, "%s"

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;I)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->g:I

    int-to-float v0, v0

    goto :goto_2

    .line 201
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->b:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 202
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v0

    if-ne v0, v8, :cond_5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->j:F

    mul-float/2addr v0, v1

    float-to-int v4, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-static {v0, v9}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;I)V

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->f:I

    int-to-float v2, v0

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    if-eqz v0, :cond_6

    move v3, v7

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget v6, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->c:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a(Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;FIILjava/util/Collection;I)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->j:F

    mul-float/2addr v0, v1

    float-to-int v4, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;I)V

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->g:I

    int-to-float v2, v0

    goto :goto_3

    :cond_6
    move v3, v8

    goto :goto_4

    .line 204
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->e:Z

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->j:F

    mul-float/2addr v0, v1

    float-to-int v4, v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->j:F

    mul-float/2addr v0, v1

    float-to-int v7, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->f:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget v6, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->c:I

    move-object v0, p0

    move v3, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a(Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;FIILjava/util/Collection;I)Ljava/lang/CharSequence;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->h:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->b:Ljava/util/Collection;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget v6, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->d:I

    move-object v0, p0

    move v3, v8

    move v4, v7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a(Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;FIILjava/util/Collection;I)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->f:I

    int-to-float v2, v2

    invoke-virtual {v1, v10}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->h:I

    int-to-float v2, v2

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-static {v0, v9}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->j:F

    mul-float/2addr v0, v1

    float-to-int v4, v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->j:F

    mul-float/2addr v0, v1

    float-to-int v10, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->g:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget v6, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->c:I

    move-object v0, p0

    move v3, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a(Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;FIILjava/util/Collection;I)Ljava/lang/CharSequence;

    move-result-object v11

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->g:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->b:Ljava/util/Collection;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->p:Lcom/google/android/apps/gmm/navigation/navui/az;

    iget v6, v0, Lcom/google/android/apps/gmm/navigation/navui/az;->d:I

    move-object v0, p0

    move v3, v8

    move v4, v10

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a(Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;FIILjava/util/Collection;I)Ljava/lang/CharSequence;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/CharSequence;

    aput-object v11, v1, v9

    const-string v2, " "

    aput-object v2, v1, v8

    aput-object v0, v1, v7

    invoke-static {v1}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->g:I

    int-to-float v2, v2

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setDesiredTextSize(F)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->b:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    invoke-virtual {v0, v12}, Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a:Lcom/google/android/apps/gmm/base/views/SqueezedLabelView;

    iget v0, p0, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->i:I

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;I)V

    goto/16 :goto_0

    :cond_9
    move v8, v9

    .line 209
    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 1

    .prologue
    .line 367
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ax;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/ax;-><init>(Lcom/google/android/apps/gmm/navigation/navui/StepCueView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->post(Ljava/lang/Runnable;)Z

    .line 373
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 377
    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    .line 378
    if-eqz p1, :cond_0

    .line 379
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/StepCueView;->a()V

    .line 381
    :cond_0
    return-void
.end method
