.class public Lcom/google/android/apps/gmm/car/bg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field final b:Lcom/google/android/apps/gmm/startpage/w;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/k/a;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/google/android/apps/gmm/map/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/car/bg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/bg;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/startpage/w;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/bg;->f:Z

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/bg;->c:Ljava/util/List;

    .line 62
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/bg;->a:Lcom/google/android/apps/gmm/base/a;

    .line 63
    iput-object p2, p0, Lcom/google/android/apps/gmm/car/bg;->e:Lcom/google/android/apps/gmm/map/t;

    .line 64
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/bg;->b:Lcom/google/android/apps/gmm/startpage/w;

    .line 65
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/startpage/w;->b()V

    .line 66
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/startpage/w;->d()V

    .line 67
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 68
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;Z)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/gp;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 139
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 140
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    iget v4, v0, Lcom/google/o/h/a/lh;->c:I

    if-eqz p2, :cond_3

    sget-object v6, Lcom/google/o/h/a/lk;->m:Lcom/google/o/h/a/lk;

    iget v6, v6, Lcom/google/o/h/a/lk;->aN:I

    if-ne v4, v6, :cond_3

    move-object v4, v0

    .line 141
    :goto_0
    if-eqz v4, :cond_c

    .line 142
    invoke-virtual {v4}, Lcom/google/o/h/a/lh;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    .line 143
    invoke-virtual {v0}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    .line 144
    iget v1, v0, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_5

    move v1, v2

    :goto_2
    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ir;

    iget v1, v1, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v2, :cond_6

    move v1, v2

    :goto_3
    if-eqz v1, :cond_2

    .line 145
    iget-object v0, v0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 140
    :cond_3
    if-nez p2, :cond_0

    sget-object v6, Lcom/google/o/h/a/lk;->ax:Lcom/google/o/h/a/lk;

    iget v6, v6, Lcom/google/o/h/a/lk;->aN:I

    if-ne v4, v6, :cond_0

    move-object v4, v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_0

    :cond_5
    move v1, v3

    .line 144
    goto :goto_2

    :cond_6
    move v1, v3

    goto :goto_3

    .line 149
    :cond_7
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bg;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/w;->c:Lcom/google/android/apps/gmm/startpage/e;

    iget-wide v8, v4, Lcom/google/o/h/a/lh;->k:J

    invoke-interface {v0, v8, v9}, Lcom/google/android/apps/gmm/startpage/a/c;->c(J)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/o/h/a/iv;

    iget v0, v1, Lcom/google/o/h/a/iv;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_8

    move v0, v2

    :goto_5
    if-eqz v0, :cond_a

    iget-object v0, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget v0, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_9

    move v0, v2

    :goto_6
    if-eqz v0, :cond_a

    iget-object v0, v1, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget-object v0, v0, Lcom/google/o/h/a/ir;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gp;->h()Lcom/google/o/h/a/gp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    move v0, v3

    goto :goto_5

    :cond_9
    move v0, v3

    goto :goto_6

    :cond_a
    sget-object v0, Lcom/google/android/apps/gmm/car/bg;->d:Ljava/lang/String;

    goto :goto_4

    :cond_b
    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 151
    :cond_c
    return-object v5
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 210
    sget-object v0, Lcom/google/android/apps/gmm/car/bg;->d:Ljava/lang/String;

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bg;->b:Lcom/google/android/apps/gmm/startpage/w;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/w;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    sget-object v0, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->b:Lcom/google/android/apps/gmm/startpage/af;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/car/bg;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Z)Z

    .line 214
    :cond_0
    return-void

    .line 211
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/k/c;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 233
    sget-object v0, Lcom/google/android/apps/gmm/car/bg;->d:Ljava/lang/String;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 235
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/k/c;->a:Lcom/google/b/c/cv;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/k/a;

    .line 236
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    sget-object v3, Lcom/google/android/apps/gmm/map/k/b;->b:Lcom/google/android/apps/gmm/map/k/b;

    if-eq v2, v3, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/k/a;->d:Lcom/google/android/apps/gmm/map/k/b;

    sget-object v3, Lcom/google/android/apps/gmm/map/k/b;->c:Lcom/google/android/apps/gmm/map/k/b;

    if-ne v2, v3, :cond_0

    .line 237
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/bg;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 240
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bg;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    .line 243
    sget-object v0, Lcom/google/android/apps/gmm/car/bg;->d:Ljava/lang/String;

    .line 245
    :cond_3
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 220
    if-nez p1, :cond_1

    .line 221
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/bg;->f:Z

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 224
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/bg;->f:Z

    if-nez v0, :cond_0

    .line 225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/bg;->f:Z

    .line 226
    sget-object v0, Lcom/google/o/h/a/dq;->p:Lcom/google/o/h/a/dq;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->b:Lcom/google/android/apps/gmm/startpage/af;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/car/bg;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Z)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Z)Z
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-wide v2, 0x3fa47ae147ae147bL    # 0.04

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bg;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v6, v0

    .line 101
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bg;->e:Lcom/google/android/apps/gmm/map/t;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bg;->e:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    move v1, v8

    :goto_1
    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/bg;->e:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->b(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    move-object v4, v0

    :goto_2
    if-nez v4, :cond_0

    if-eqz v6, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/car/bg;->d:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/r/b/a;->h:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2e

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Constructing viewport from current location ("

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/r;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v4

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v10

    invoke-direct {v1, v4, v5, v10, v11}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/b/a/r;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;DD)V

    move-object v4, v0

    :cond_0
    if-nez v4, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/car/bg;->d:Ljava/lang/String;

    .line 102
    :cond_1
    if-nez v4, :cond_4

    move v0, v7

    .line 121
    :goto_3
    return v0

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bg;->a:Lcom/google/android/apps/gmm/base/a;

    .line 100
    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v1

    move-object v6, v1

    goto :goto_0

    :cond_3
    move v1, v7

    .line 101
    goto :goto_1

    .line 105
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/car/bh;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v6

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/car/bh;-><init>(Lcom/google/android/apps/gmm/car/bg;Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/r/b/a;Z)V

    .line 120
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/bg;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move v0, v8

    .line 121
    goto :goto_3

    :cond_5
    move-object v4, v0

    goto :goto_2
.end method
