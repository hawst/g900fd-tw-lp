.class public Lcom/google/android/apps/gmm/base/views/ah;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/widget/LinearLayout;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field b:I

.field private c:Landroid/content/Context;

.field private d:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 612
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 601
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    .line 603
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/ah;->d:Z

    .line 604
    iput v1, p0, Lcom/google/android/apps/gmm/base/views/ah;->b:I

    .line 610
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/ah;->c:Landroid/content/Context;

    .line 614
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 647
    :cond_0
    return-void

    .line 644
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/ah;->c:Landroid/content/Context;

    invoke-virtual {v0, v2, p1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 644
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method a(Landroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 762
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/ah;->d:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/ah;->b:I

    move v3, v2

    :goto_0
    if-lez v3, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const v2, 0x7fffffff

    move v3, v2

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    if-ne v3, v0, :cond_3

    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setMaxLines(I)V

    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 763
    return-void

    :cond_3
    move v0, v1

    .line 762
    goto :goto_2
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 628
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/ah;->d:Z

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 636
    :cond_0
    return-void

    .line 633
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ah;->a(Landroid/widget/TextView;)V

    .line 633
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 676
    iput p1, p0, Lcom/google/android/apps/gmm/base/views/ah;->b:I

    .line 678
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 684
    :cond_0
    return-void

    .line 681
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ah;->a(Landroid/widget/TextView;)V

    .line 681
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 706
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 722
    :cond_0
    return-void

    .line 717
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 718
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setGravity(I)V

    .line 719
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 720
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ah;->a:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    .line 719
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
