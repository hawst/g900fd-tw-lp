.class Lcom/google/android/apps/gmm/map/n/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/b/a/bw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/b/a/bw",
        "<",
        "Lcom/google/android/apps/gmm/shared/net/r;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/n/g;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/n/g;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/o;->a:Lcom/google/android/apps/gmm/map/n/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/o;->a:Lcom/google/android/apps/gmm/map/n/g;

    sget-object v1, Lcom/google/android/apps/gmm/d/a;->b:Ljava/lang/String;

    const-string v2, "https:"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "MapEnvironment"

    const-string v3, "Gmm server url should start with https."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/n/g;->a:Landroid/content/Context;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/net/s;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/net/ad;Ljava/lang/String;)Lcom/google/android/apps/gmm/shared/net/s;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/el;->cq:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/gk;

    sget-object v3, Lcom/google/r/b/a/gq;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->H:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/xb;

    sget-object v3, Lcom/google/r/b/a/xh;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0xa

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    sget-object v1, Lcom/google/r/b/a/el;->ch:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/vr;

    sget-object v3, Lcom/google/r/b/a/vv;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cP:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/jx;

    sget-object v3, Lcom/google/r/b/a/kd;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cO:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/ki;

    sget-object v3, Lcom/google/r/b/a/kn;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cf:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/ks;

    sget-object v3, Lcom/google/r/b/a/kx;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cN:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/lc;

    sget-object v3, Lcom/google/r/b/a/lh;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cA:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/ni;

    sget-object v3, Lcom/google/r/b/a/nm;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->bM:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/my;

    sget-object v3, Lcom/google/r/b/a/nc;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cm:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/qj;

    sget-object v3, Lcom/google/r/b/a/qn;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cB:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/tr;

    sget-object v3, Lcom/google/r/b/a/tw;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cr:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/gw;

    sget-object v3, Lcom/google/r/b/a/ha;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->co:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/n;

    sget-object v3, Lcom/google/r/b/a/r;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cn:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/v;

    sget-object v3, Lcom/google/r/b/a/z;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cg:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/wf;

    sget-object v3, Lcom/google/r/b/a/wj;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cu:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/abj;

    sget-object v3, Lcom/google/r/b/a/abn;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cl:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/aih;

    sget-object v3, Lcom/google/r/b/a/ail;->PARSER:Lcom/google/n/ax;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;)V

    sget-object v1, Lcom/google/r/b/a/el;->cE:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/aip;

    sget-object v3, Lcom/google/r/b/a/ait;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    const/4 v5, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x3c

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    sget-object v1, Lcom/google/r/b/a/el;->cw:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/ahw;

    sget-object v3, Lcom/google/r/b/a/aia;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cx:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/ahn;

    sget-object v3, Lcom/google/r/b/a/ahr;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cj:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/ajp;

    sget-object v3, Lcom/google/r/b/a/ajt;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->bQ:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/aka;

    sget-object v3, Lcom/google/r/b/a/ake;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cD:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/aor;

    sget-object v3, Lcom/google/r/b/a/aov;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cb:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/sk;

    sget-object v3, Lcom/google/r/b/a/so;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cc:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/ss;

    sget-object v3, Lcom/google/r/b/a/sw;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x1

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->ci:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/alz;

    sget-object v3, Lcom/google/r/b/a/amd;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x5

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const/4 v8, 0x2

    const/4 v9, 0x1

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    sget-object v1, Lcom/google/r/b/a/el;->bU:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/maps/g/xd;

    sget-object v3, Lcom/google/maps/g/xi;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->bI:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/api;

    sget-object v3, Lcom/google/r/b/a/apm;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    const/4 v5, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0x3c

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    sget-object v1, Lcom/google/r/b/a/el;->ct:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/apx;

    sget-object v3, Lcom/google/r/b/a/aqc;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->cv:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/uy;

    sget-object v3, Lcom/google/r/b/a/vc;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;Z)V

    sget-object v1, Lcom/google/r/b/a/el;->bE:Lcom/google/r/b/a/el;

    const-class v2, Lcom/google/r/b/a/aog;

    sget-object v3, Lcom/google/r/b/a/aok;->PARSER:Lcom/google/n/ax;

    const/4 v4, 0x1

    const/4 v5, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v8, 0xf

    invoke-virtual {v6, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-interface/range {v0 .. v9}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/r/b/a/el;Ljava/lang/Class;Lcom/google/n/ax;ZZJIZ)V

    return-object v0
.end method
