.class public Lcom/google/android/apps/gmm/map/b/a/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/map/b/a/j;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/b/a/j;

.field static final serialVersionUID:J = 0x2d172014d70e6166L


# instance fields
.field public final b:J

.field public final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-direct {v0, v2, v3, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/b/a/j;->a:Lcom/google/android/apps/gmm/map/b/a/j;

    return-void
.end method

.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 59
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    .line 60
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    .line 64
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    .line 65
    return-void
.end method

.method public static a(Lcom/google/d/a/a/ds;)Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 6
    .param p0    # Lcom/google/d/a/a/ds;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 250
    if-nez p0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 253
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-wide v2, p0, Lcom/google/d/a/a/ds;->b:J

    iget-wide v4, p0, Lcom/google/d/a/a/ds;->c:J

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    goto :goto_0
.end method

.method public static a(Lcom/google/o/b/a/h;)Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 6
    .param p0    # Lcom/google/o/b/a/h;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 259
    if-nez p0, :cond_0

    .line 260
    const/4 v0, 0x0

    .line 262
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/j;

    iget-wide v2, p0, Lcom/google/o/b/a/h;->b:J

    iget-wide v4, p0, Lcom/google/o/b/a/h;->c:J

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    goto :goto_0
.end method

.method public static a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 5

    .prologue
    .line 74
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v0

    .line 75
    invoke-interface {p0}, Ljava/io/DataInput;->readLong()J

    move-result-wide v2

    .line 76
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    return-object v4
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x2

    .line 95
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 97
    :cond_0
    const-string v0, ":"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 98
    array-length v1, v0

    if-ne v1, v4, :cond_3

    .line 99
    aget-object v1, v0, v3

    const-string v2, "0x"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 100
    :cond_1
    aget-object v1, v0, v5

    const-string v2, "0x"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 102
    :cond_2
    aget-object v1, v0, v3

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->c(Ljava/lang/String;)J

    move-result-wide v2

    .line 103
    aget-object v0, v0, v5

    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c(Ljava/lang/String;)J

    move-result-wide v4

    .line 104
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    .line 117
    :goto_0
    return-object v0

    .line 105
    :cond_3
    const-string v0, "-?[0-9]{1,20}"

    invoke-static {v0, p0}, Ljava/util/regex/Pattern;->matches(Ljava/lang/String;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 109
    const/16 v0, 0x2d

    :try_start_0
    invoke-virtual {p0, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-nez v0, :cond_4

    .line 110
    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 117
    :goto_1
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/j;

    const-wide/16 v4, 0x0

    invoke-direct {v2, v4, v5, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/j;-><init>(JJ)V

    move-object v0, v2

    goto :goto_0

    .line 112
    :cond_4
    :try_start_1
    invoke-static {p0}, Lcom/google/b/h/d;->a(Ljava/lang/String;)J
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-wide v0

    goto :goto_1

    .line 115
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "malformed cdocid "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 119
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "malformed feature id "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/j;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 201
    if-nez p0, :cond_1

    .line 204
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 209
    if-nez p0, :cond_0

    .line 217
    :goto_0
    return-object v0

    .line 213
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 217
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)J
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v4, 0x10

    .line 153
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v4, :cond_0

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v3, 0x7

    if-le v0, v3, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 154
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v0, v0, -0x8

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 157
    :goto_1
    const-wide/high16 v2, -0x8000000000000000L

    invoke-static {v0, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    or-long/2addr v0, v2

    .line 159
    :goto_2
    return-wide v0

    :cond_0
    move v0, v2

    .line 153
    goto :goto_0

    .line 154
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 159
    :cond_2
    invoke-static {p0, v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    goto :goto_2
.end method

.method public static d()I
    .locals 1

    .prologue
    .line 308
    const/16 v0, 0x28

    return v0
.end method

.method public static e()Lcom/google/android/apps/gmm/map/b/a/k;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 413
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Deprecated"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/d/a/a/ds;
    .locals 4

    .prologue
    .line 245
    invoke-static {}, Lcom/google/d/a/a/ds;->newBuilder()Lcom/google/d/a/a/du;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v1, v0, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/d/a/a/du;->a:I

    iput-wide v2, v0, Lcom/google/d/a/a/du;->b:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v1, v0, Lcom/google/d/a/a/du;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/d/a/a/du;->a:I

    iput-wide v2, v0, Lcom/google/d/a/a/du;->c:J

    invoke-virtual {v0}, Lcom/google/d/a/a/du;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ds;

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/j;)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 344
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 345
    :cond_0
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 355
    :cond_1
    :goto_0
    return v0

    .line 347
    :cond_2
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    .line 348
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 350
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    .line 351
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 353
    goto :goto_0

    :cond_4
    move v0, v1

    .line 355
    goto :goto_0
.end method

.method public final b()Lcom/google/o/b/a/h;
    .locals 4

    .prologue
    .line 267
    invoke-static {}, Lcom/google/o/b/a/h;->newBuilder()Lcom/google/o/b/a/j;

    move-result-object v0

    .line 268
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget v1, v0, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/o/b/a/j;->a:I

    iput-wide v2, v0, Lcom/google/o/b/a/j;->b:J

    .line 269
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget v1, v0, Lcom/google/o/b/a/j;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/o/b/a/j;->a:I

    iput-wide v2, v0, Lcom/google/o/b/a/j;->c:J

    .line 270
    invoke-virtual {v0}, Lcom/google/o/b/a/j;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/h;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 5

    .prologue
    .line 301
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ":0x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Lcom/google/android/apps/gmm/map/b/a/j;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 327
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/b/a/j;

    if-nez v1, :cond_1

    .line 331
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/j;

    .line 331
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/16 v4, 0x20

    .line 322
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    xor-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->b:J

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
