.class public Lcom/google/android/apps/gmm/map/t/z;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/bc;


# instance fields
.field final a:F

.field final b:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet",
            "<",
            "Lcom/google/android/apps/gmm/map/t/p;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/map/f/o;

.field final d:Ljava/util/concurrent/Semaphore;

.field public final e:Lcom/google/android/apps/gmm/map/t/ab;

.field final f:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field final g:Lcom/google/android/apps/gmm/map/c/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/n;FLcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/map/util/b/a/a;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/map/t/aa;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/t/aa;-><init>(Lcom/google/android/apps/gmm/map/t/z;)V

    new-instance v1, Ljava/util/TreeSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/t/z;->b:Ljava/util/SortedSet;

    .line 79
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->d:Ljava/util/concurrent/Semaphore;

    .line 97
    const/high16 v0, 0x41700000    # 15.0f

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/z;->a:F

    .line 98
    check-cast p1, Lcom/google/android/apps/gmm/map/f/o;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/t/z;->c:Lcom/google/android/apps/gmm/map/f/o;

    .line 99
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/t/z;->g:Lcom/google/android/apps/gmm/map/c/a/a;

    .line 100
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/t/z;->f:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 102
    new-instance v0, Lcom/google/android/apps/gmm/map/t/ab;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/t/ab;-><init>(Lcom/google/android/apps/gmm/map/t/z;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->e:Lcom/google/android/apps/gmm/map/t/ab;

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->e:Lcom/google/android/apps/gmm/map/t/ab;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t/ab;->start()V

    .line 104
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 129
    const/4 v0, 0x0

    move v1, v0

    .line 133
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    if-eqz v1, :cond_0

    .line 143
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 134
    :cond_0
    return-void

    .line 137
    :catch_0
    move-exception v0

    const/4 v0, 0x1

    move v1, v0

    .line 138
    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 143
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    :cond_1
    throw v0
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 121
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/t/z;->c:Lcom/google/android/apps/gmm/map/f/o;

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    :goto_0
    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/t/z;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 125
    :cond_0
    return-void

    .line 121
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->e:Lcom/google/android/apps/gmm/map/t/ab;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/ab;->a:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ab;->a:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 2

    .prologue
    .line 108
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/t/p;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Current implementation only supports GmmEntity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->b:Ljava/util/SortedSet;

    check-cast p1, Lcom/google/android/apps/gmm/map/t/p;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    .line 112
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 151
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aa;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->b:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 117
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/z;->e:Lcom/google/android/apps/gmm/map/t/ab;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x1

    :try_start_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/ab;->d:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/ab;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
