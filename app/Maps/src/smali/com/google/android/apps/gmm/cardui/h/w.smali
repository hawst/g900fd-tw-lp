.class public Lcom/google/android/apps/gmm/cardui/h/w;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/g/i;


# instance fields
.field private final a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/cardui/g/j;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/google/android/apps/gmm/cardui/g/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/qr;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iget v0, p3, Lcom/google/o/h/a/qr;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    move v0, v6

    :goto_0
    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p3, Lcom/google/o/h/a/qr;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hv;->g()Lcom/google/o/h/a/hv;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hv;

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/h/g;->a(Lcom/google/o/h/a/hv;)Lcom/google/android/apps/gmm/base/views/c/k;

    .line 39
    :cond_0
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v7

    .line 40
    invoke-virtual {p3}, Lcom/google/o/h/a/qr;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/o/h/a/qu;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/cardui/h/x;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/h/x;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/qu;Z)V

    invoke-virtual {v7, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    :cond_1
    move v0, v5

    .line 33
    goto :goto_0

    .line 44
    :cond_2
    invoke-virtual {v7}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/w;->a:Lcom/google/b/c/cv;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/cardui/h/x;

    .line 46
    iget-object v1, p3, Lcom/google/o/h/a/qr;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/qu;->h()Lcom/google/o/h/a/qu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v4

    check-cast v4, Lcom/google/o/h/a/qu;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/h/x;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/util/b/ag;Lcom/google/o/h/a/qr;Lcom/google/o/h/a/qu;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/w;->b:Lcom/google/android/apps/gmm/cardui/g/j;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/cardui/g/j;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/w;->a:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/cardui/g/j;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/h/w;->b:Lcom/google/android/apps/gmm/cardui/g/j;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return-object v0
.end method
