.class public Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final b:[I


# instance fields
.field a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 17
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->b:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->a:Z

    .line 23
    return-void
.end method


# virtual methods
.method public isChecked()Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->a:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->b:[I

    add-int/lit8 v0, p1, 0x1

    .line 64
    invoke-super {p0, v0}, Landroid/widget/LinearLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    sget-object v1, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->b:[I

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->mergeDrawableStates([I[I)[I

    .line 68
    :cond_0
    return-object v0
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setChecked(Z)V

    .line 75
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 76
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 81
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->isChecked()Z

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 83
    const-class v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 84
    return-void
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->setChecked(Z)V

    .line 54
    invoke-super {p0}, Landroid/widget/LinearLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->a:Z

    if-ne v0, p1, :cond_0

    .line 38
    :goto_0
    return-void

    .line 36
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->a:Z

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->refreshDrawableState()V

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->a:Z

    if-nez v0, :cond_0

    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/RadioLinearLayout;->setChecked(Z)V

    .line 49
    :cond_0
    return-void

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
