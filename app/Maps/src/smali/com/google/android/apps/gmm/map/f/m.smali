.class public Lcom/google/android/apps/gmm/map/f/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/animation/TypeEvaluator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/animation/TypeEvaluator",
        "<",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/view/animation/Interpolator;

.field final b:Lcom/google/android/apps/gmm/map/f/f;

.field final c:Lcom/google/android/apps/gmm/map/f/o;

.field final d:Lcom/google/android/apps/gmm/map/f/y;

.field final e:Landroid/view/animation/Interpolator;

.field f:F

.field g:I

.field h:Lcom/google/android/apps/gmm/map/f/a/a;

.field i:Lcom/google/android/apps/gmm/map/f/a/a;

.field j:J

.field k:F

.field l:F


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/f/y;Landroid/view/animation/Interpolator;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/f/f;)V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/map/f/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/f/n;-><init>(Lcom/google/android/apps/gmm/map/f/m;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/m;->a:Landroid/view/animation/Interpolator;

    .line 148
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/f/y;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/m;->d:Lcom/google/android/apps/gmm/map/f/y;

    .line 149
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Landroid/view/animation/Interpolator;

    iput-object p2, p0, Lcom/google/android/apps/gmm/map/f/m;->e:Landroid/view/animation/Interpolator;

    .line 150
    if-nez p4, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p4, Lcom/google/android/apps/gmm/map/f/f;

    iput-object p4, p0, Lcom/google/android/apps/gmm/map/f/m;->b:Lcom/google/android/apps/gmm/map/f/f;

    .line 151
    if-nez p3, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move-object v0, p3

    check-cast v0, Lcom/google/android/apps/gmm/map/f/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/m;->c:Lcom/google/android/apps/gmm/map/f/o;

    .line 152
    iget v0, p3, Lcom/google/android/apps/gmm/v/n;->K:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/m;->f:F

    .line 153
    iget-object v0, p3, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/m;->g:I

    .line 154
    return-void
.end method

.method static a(JJF)J
    .locals 2

    .prologue
    .line 273
    sub-long v0, p2, p0

    long-to-float v0, v0

    mul-float/2addr v0, p4

    float-to-long v0, v0

    add-long/2addr v0, p0

    return-wide v0
.end method


# virtual methods
.method final a(F)F
    .locals 7

    .prologue
    .line 240
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/m;->g:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/m;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/o;->i:F

    div-float/2addr v0, v1

    float-to-int v6, v0

    .line 241
    float-to-double v0, p1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/m;->h:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v4, p0, Lcom/google/android/apps/gmm/map/f/m;->f:F

    float-to-double v4, v4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->a(DDDI)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public synthetic evaluate(FLjava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 19
    cmpg-float v0, v1, p1

    if-gtz v0, :cond_0

    cmpg-float v0, p1, v3

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Cannot accept interpolated time outside of range [0, 1.0]."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    cmpg-float v0, p1, v1

    if-gtz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/m;->h:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    cmpl-float v0, p1, v3

    if-ltz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/m;->i:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/m;->d:Lcom/google/android/apps/gmm/map/f/y;

    iget v2, v0, Lcom/google/android/apps/gmm/map/f/y;->c:F

    cmpl-float v2, v2, v1

    if-nez v2, :cond_4

    move v0, v1

    :goto_2
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/m;->l:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/m;->k:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/map/f/m;->g:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/m;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/o;->i:F

    div-float/2addr v1, v2

    float-to-int v6, v1

    float-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/m;->h:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v4, p0, Lcom/google/android/apps/gmm/map/f/m;->f:F

    float-to-double v4, v4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->b(DDDI)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget v1, v0, Lcom/google/android/apps/gmm/map/f/y;->a:F

    iget v2, v0, Lcom/google/android/apps/gmm/map/f/y;->b:F

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/y;->a:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    float-to-double v4, v1

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/y;->c:F

    float-to-double v0, v0

    div-double v0, v4, v0

    sub-double v0, v2, v0

    double-to-float v0, v0

    goto :goto_2
.end method
