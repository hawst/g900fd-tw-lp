.class public Lcom/google/android/apps/gmm/map/t/f;
.super Lcom/google/android/apps/gmm/v/bo;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:F

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 201
    const-string v0, "precision highp float;\nuniform mat4 uMVPMatrix;\nuniform float heightScale;\nattribute vec4 aPosition;\nattribute vec4 aColor;\nuniform float uAlphaMap;\nuniform float buildingOpacity;\nuniform float zBias;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * vec4(aPosition.xy, aPosition.z * heightScale,\n      aPosition.w);\n  gl_Position.z += zBias;\n  vColor = vec4(aColor.rgb, aColor.a * uAlphaMap * buildingOpacity);\n}\n"

    const-string v1, "precision mediump float;\nvarying vec4 vColor;\nuniform float brightnessScale;\nvoid main() {\n  gl_FragColor = vec4(vColor.rgb * brightnessScale, vColor.a);\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/bo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/f;->c:F

    .line 202
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 206
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/v/bo;->a(I)V

    .line 207
    const-string v0, "uAlphaMap"

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/map/t/f;->a(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/f;->a:I

    .line 209
    const-string v0, "brightnessScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/f;->b:I

    .line 210
    sget-object v0, Lcom/google/android/apps/gmm/map/t/e;->a:Ljava/lang/String;

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/f;->b:I

    if-ne v0, v2, :cond_0

    .line 212
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get brightnessScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    const-string v0, "buildingOpacity"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/f;->d:I

    .line 216
    sget-object v0, Lcom/google/android/apps/gmm/map/t/e;->a:Ljava/lang/String;

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/f;->d:I

    if-ne v0, v2, :cond_1

    .line 218
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get buildingOpacity handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_1
    const-string v0, "heightScale"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/f;->e:I

    .line 222
    sget-object v0, Lcom/google/android/apps/gmm/map/t/e;->a:Ljava/lang/String;

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/f;->e:I

    if-ne v0, v2, :cond_2

    .line 224
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get heightScale handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 227
    :cond_2
    const-string v0, "zBias"

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/f;->f:I

    .line 228
    sget-object v0, Lcom/google/android/apps/gmm/map/t/e;->a:Ljava/lang/String;

    const-string v1, "glGetUniformLocation"

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/v/ad;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget v0, p0, Lcom/google/android/apps/gmm/map/t/f;->f:I

    if-ne v0, v2, :cond_3

    .line 230
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to get zBias handle"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_3
    return-void
.end method
