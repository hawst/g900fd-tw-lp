.class Lcom/google/android/apps/gmm/map/m/v;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/m/u;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private b:F

.field private c:F


# direct methods
.method constructor <init>(Landroid/view/MotionEvent;Landroid/view/VelocityTracker;Ljava/util/EnumSet;Z)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/MotionEvent;",
            "Landroid/view/VelocityTracker;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/apps/gmm/map/m/p;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/m/v;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 53
    const/4 v2, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 55
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    new-array v3, v2, [Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 56
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    new-array v4, v2, [Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 57
    const/4 v2, 0x0

    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_0

    .line 58
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v5

    .line 59
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/ay;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    aput-object v6, v4, v2

    .line 60
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 61
    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v7

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    invoke-direct {v6, v7, v5}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    aput-object v6, v3, v2

    .line 62
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/m/v;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    aget-object v6, v3, v2

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 57
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 64
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/v;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x1

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    long-to-float v5, v6

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v6, v5

    iput v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v5, v6

    iput v5, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 65
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/v;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    array-length v5, v3

    int-to-float v5, v5

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    div-float/2addr v6, v5

    iput v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    div-float v5, v6, v5

    iput v5, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 67
    const/4 v2, 0x0

    aget-object v2, v4, v2

    iget v5, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    const/4 v2, 0x0

    aget-object v2, v4, v2

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v4, v2

    iget v7, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v4, v2

    iget v8, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    const/4 v2, 0x0

    aget-object v2, v3, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float v9, v5, v2

    const/4 v2, 0x0

    aget-object v2, v3, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float v10, v6, v2

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v3, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    add-float v11, v7, v2

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v3, v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    add-float v3, v8, v2

    new-instance v12, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v12}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    array-length v2, v4

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v4, v2

    const/4 v13, 0x0

    aget-object v4, v4, v13

    invoke-static {v2, v4, v12}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v4, v11, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    new-instance v13, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v13, v9, v10}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    invoke-static {v4, v13, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->a(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v4, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v13, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v4, v13

    iget v13, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v14, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v13, v14

    add-float/2addr v4, v13

    float-to-double v14, v4

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    double-to-float v4, v14

    const/4 v13, 0x0

    cmpl-float v13, v4, v13

    if-nez v13, :cond_5

    const/high16 v2, 0x3f800000    # 1.0f

    :goto_1
    invoke-static {v2}, Lcom/google/android/apps/gmm/shared/c/s;->d(F)F

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/v;->b:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/m/v;->b:F

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v14, 0x1

    invoke-virtual {v4, v14, v15}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v14

    long-to-float v4, v14

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/v;->b:F

    sub-float v2, v7, v5

    float-to-double v14, v2

    sub-float v2, v8, v6

    float-to-double v0, v2

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v14

    double-to-float v2, v14

    sub-float v4, v11, v9

    float-to-double v14, v4

    sub-float v4, v3, v10

    float-to-double v0, v4

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v14

    double-to-float v4, v14

    invoke-static {v2, v4}, Lcom/google/android/apps/gmm/map/m/d;->a(FF)F

    move-result v2

    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v14

    double-to-float v2, v14

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/v;->c:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/m/v;->c:F

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v14, 0x1

    invoke-virtual {v4, v14, v15}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v14

    long-to-float v4, v14

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/v;->c:F

    if-eqz p4, :cond_1

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ay;

    sub-float v4, v9, v5

    sub-float v5, v10, v6

    invoke-direct {v2, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/ay;

    sub-float v5, v11, v7

    sub-float/2addr v3, v8

    invoke-direct {v4, v5, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>(FF)V

    iget v3, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v5, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v3, v5

    iget v5, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v6, v4, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v5, v6

    add-float/2addr v3, v5

    iget v5, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v6, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v5, v6

    iget v6, v12, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v7, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    sub-float/2addr v3, v5

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v5, v12, v4}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)F

    move-result v4

    sget-object v5, Lcom/google/android/apps/gmm/map/b/a/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v5, v12, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)F

    move-result v2

    sub-float v2, v4, v2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/apps/gmm/map/m/v;->c:F

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/m/n;->a(FF)F

    move-result v2

    mul-float/2addr v2, v4

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/v;->c:F

    .line 71
    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/map/m/p;->a:Lcom/google/android/apps/gmm/map/m/p;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 72
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/v;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    const/4 v3, 0x0

    const/4 v4, 0x0

    iput v3, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 74
    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/map/m/p;->b:Lcom/google/android/apps/gmm/map/m/p;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 75
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/v;->b:F

    .line 77
    :cond_3
    sget-object v2, Lcom/google/android/apps/gmm/map/m/p;->c:Lcom/google/android/apps/gmm/map/m/p;

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 78
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/m/v;->c:F

    .line 80
    :cond_4
    return-void

    .line 67
    :cond_5
    iget v13, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v14, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v13, v14

    iget v14, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v2, v14

    add-float/2addr v2, v13

    float-to-double v14, v2

    invoke-static {v14, v15}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v14

    double-to-float v2, v14

    div-float/2addr v2, v4

    goto/16 :goto_1
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/b/a/ay;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/v;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    return-object v0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/v;->b:F

    return v0
.end method

.method public final c()F
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/v;->c:F

    return v0
.end method
