.class public Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;
.super Landroid/view/View$BaseSavedState;
.source "PG"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

.field final b:[F

.field final c:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 186
    new-instance v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/s;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 201
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->createFloatArray()[F

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->b:[F

    .line 203
    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->c:[I

    .line 204
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;[F[I)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 173
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 174
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->b:[F

    .line 175
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->c:[I

    .line 176
    return-void
.end method


# virtual methods
.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 180
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->b:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/r;->c:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 184
    return-void
.end method
