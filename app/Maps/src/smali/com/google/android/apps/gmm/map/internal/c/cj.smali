.class public Lcom/google/android/apps/gmm/map/internal/c/cj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/internal/c/m;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/internal/c/m;II)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/cj;->a:Lcom/google/android/apps/gmm/map/internal/c/m;

    .line 41
    return-void
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;)Lcom/google/android/apps/gmm/map/internal/c/cj;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 53
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    .line 54
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v3

    .line 55
    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 57
    invoke-static {p0, p1, v0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bs;Ljava/util/Collection;)V

    .line 58
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-ne v4, v1, :cond_1

    .line 59
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ck;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/m;

    invoke-direct {v1, v3, v0}, Lcom/google/android/apps/gmm/map/internal/c/ck;-><init>(ILcom/google/android/apps/gmm/map/internal/c/m;)V

    move-object v0, v1

    .line 68
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0

    .line 61
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 68
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cl;

    int-to-long v2, v3

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/cl;-><init>(J)V

    goto :goto_1
.end method

.method public static a(Lcom/google/maps/b/a/ca;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/b/a/ca;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/cj;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/maps/b/a/ca;->a:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 75
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/maps/b/a/ca;->b:Lcom/google/maps/b/a/da;

    iget v1, v1, Lcom/google/maps/b/a/da;->b:I

    if-ge v0, v1, :cond_0

    .line 76
    iget-object v1, p0, Lcom/google/maps/b/a/ca;->b:Lcom/google/maps/b/a/da;

    iget-object v1, v1, Lcom/google/maps/b/a/da;->a:[J

    aget-wide v2, v1, v0

    .line 77
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/cl;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/cl;-><init>(J)V

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method
