.class public Lcom/google/android/apps/gmm/map/n/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/c/a/a;
.implements Lcom/google/android/apps/gmm/map/c/a;
.implements Lcom/google/android/apps/gmm/map/internal/d/ac;


# instance fields
.field private final A:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/i/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private final B:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/indoor/a/a;",
            ">;"
        }
    .end annotation
.end field

.field final a:Landroid/content/Context;

.field b:Lcom/google/android/apps/gmm/map/util/b/g;

.field c:Lcom/google/android/apps/gmm/map/internal/c/cw;

.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field final e:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/libraries/memorymonitor/d;",
            ">;"
        }
    .end annotation
.end field

.field final f:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/util/a/b;",
            ">;"
        }
    .end annotation
.end field

.field final g:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/c/a/g;",
            ">;"
        }
    .end annotation
.end field

.field final h:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/c/a/a;",
            ">;"
        }
    .end annotation
.end field

.field final i:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/google/android/apps/gmm/util/replay/a;

.field private final k:Lcom/google/android/apps/gmm/shared/b/a;

.field private final l:Ljava/lang/String;

.field private m:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private n:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private o:Lcom/google/android/apps/gmm/map/internal/c/o;

.field private final p:Z

.field private final q:Lcom/google/android/apps/gmm/map/t/d;

.field private r:Lcom/google/android/apps/gmm/map/e/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final s:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/shared/net/r;",
            ">;"
        }
    .end annotation
.end field

.field private final u:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/b/f;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/d/bd;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/q/b;",
            ">;"
        }
    .end annotation
.end field

.field private final x:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/c/c;",
            ">;"
        }
    .end annotation
.end field

.field private final y:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/h/d;",
            ">;"
        }
    .end annotation
.end field

.field private final z:Lcom/google/b/a/bw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/bw",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .prologue
    .line 79
    invoke-static {}, Lcom/google/android/apps/gmm/map/util/jni/NativeHelper;->initialize()V

    .line 80
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/util/replay/a;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    .line 319
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 126
    new-instance v0, Lcom/google/android/apps/gmm/map/n/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/h;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 127
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->s:Lcom/google/b/a/bw;

    .line 133
    new-instance v0, Lcom/google/android/apps/gmm/map/n/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/o;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_2

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->t:Lcom/google/b/a/bw;

    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/map/n/p;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/p;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 144
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_4

    :goto_2
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->u:Lcom/google/b/a/bw;

    .line 152
    new-instance v0, Lcom/google/android/apps/gmm/map/n/q;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/q;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 153
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_6

    :goto_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->v:Lcom/google/b/a/bw;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/map/n/r;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/r;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 160
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_8

    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->w:Lcom/google/b/a/bw;

    .line 166
    new-instance v0, Lcom/google/android/apps/gmm/map/n/s;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/s;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 167
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_a

    :goto_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->e:Lcom/google/b/a/bw;

    .line 176
    new-instance v0, Lcom/google/android/apps/gmm/map/n/t;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/t;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 177
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_c

    :goto_6
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->f:Lcom/google/b/a/bw;

    .line 185
    new-instance v0, Lcom/google/android/apps/gmm/map/n/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/u;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 186
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_e

    :goto_7
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->x:Lcom/google/b/a/bw;

    .line 192
    new-instance v0, Lcom/google/android/apps/gmm/map/n/v;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/v;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 193
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_10

    :goto_8
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->g:Lcom/google/b/a/bw;

    .line 202
    new-instance v0, Lcom/google/android/apps/gmm/map/n/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/i;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 203
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_12

    :goto_9
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->h:Lcom/google/b/a/bw;

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/map/n/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/j;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 213
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_14

    :goto_a
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->y:Lcom/google/b/a/bw;

    .line 223
    new-instance v0, Lcom/google/android/apps/gmm/map/n/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/k;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 224
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_16

    :goto_b
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->z:Lcom/google/b/a/bw;

    .line 230
    new-instance v0, Lcom/google/android/apps/gmm/map/n/l;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/l;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 231
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_18

    :goto_c
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->A:Lcom/google/b/a/bw;

    .line 237
    new-instance v0, Lcom/google/android/apps/gmm/map/n/m;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/m;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    .line 238
    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_1a

    :goto_d
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->B:Lcom/google/b/a/bw;

    .line 257
    new-instance v0, Lcom/google/android/apps/gmm/map/n/n;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/n/n;-><init>(Lcom/google/android/apps/gmm/map/n/g;)V

    instance-of v1, v0, Lcom/google/b/a/by;

    if-eqz v1, :cond_1c

    :goto_e
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->i:Lcom/google/b/a/bw;

    .line 320
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/n/g;->a:Landroid/content/Context;

    .line 321
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/n/g;->l:Ljava/lang/String;

    .line 322
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/n/g;->k:Lcom/google/android/apps/gmm/shared/b/a;

    .line 323
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/n/g;->j:Lcom/google/android/apps/gmm/util/replay/a;

    .line 324
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->n:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 325
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1e

    move v0, v2

    :goto_f
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/n/g;->p:Z

    .line 326
    new-instance v0, Lcom/google/android/apps/gmm/map/t/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/n/g;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/t/d;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->q:Lcom/google/android/apps/gmm/map/t/d;

    .line 327
    return-void

    .line 127
    :cond_0
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 133
    :cond_2
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_1

    .line 144
    :cond_4
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_2

    .line 153
    :cond_6
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_3

    .line 160
    :cond_8
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_4

    .line 167
    :cond_a
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_5

    .line 177
    :cond_c
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_6

    .line 186
    :cond_e
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_7

    .line 193
    :cond_10
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_8

    .line 203
    :cond_12
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_9

    .line 213
    :cond_14
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_a

    .line 224
    :cond_16
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_b

    .line 231
    :cond_18
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_c

    .line 238
    :cond_1a
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1b
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_d

    .line 257
    :cond_1c
    new-instance v1, Lcom/google/b/a/by;

    if-nez v0, :cond_1d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1d
    check-cast v0, Lcom/google/b/a/bw;

    invoke-direct {v1, v0}, Lcom/google/b/a/by;-><init>(Lcom/google/b/a/bw;)V

    move-object v0, v1

    goto/16 :goto_e

    .line 325
    :cond_1e
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v1, v3, Landroid/util/DisplayMetrics;->xdpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v4, v1

    cmpl-double v1, v4, v6

    if-gtz v1, :cond_1f

    iget v1, v3, Landroid/util/DisplayMetrics;->ydpi:F

    sub-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v1, v0

    float-to-double v4, v1

    cmpl-double v1, v4, v6

    if-lez v1, :cond_20

    :cond_1f
    move v1, v0

    :goto_10
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    div-float v1, v4, v1

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    div-float v0, v3, v0

    mul-float/2addr v1, v1

    mul-float/2addr v0, v0

    add-float/2addr v0, v1

    const/high16 v1, 0x42440000    # 49.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_21

    move v0, v2

    goto/16 :goto_f

    :cond_20
    iget v1, v3, Landroid/util/DisplayMetrics;->xdpi:F

    iget v0, v3, Landroid/util/DisplayMetrics;->ydpi:F

    goto :goto_10

    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_f
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/util/replay/a;)Lcom/google/android/apps/gmm/map/c/a;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 308
    new-instance v3, Lcom/google/android/apps/gmm/map/n/g;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/n/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/util/replay/a;)V

    .line 310
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/d;->a(Ljava/lang/Thread;Lcom/google/android/apps/gmm/map/c/a/a;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const/4 v4, 0x5

    invoke-virtual {v0, v4}, Ljava/lang/Thread;->setPriority(I)V

    invoke-static {v1}, Landroid/os/Process;->setThreadPriority(I)V

    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a/k;

    const/4 v4, 0x4

    const/4 v5, 0x2

    iget-object v6, v3, Lcom/google/android/apps/gmm/map/n/g;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v0, v4, v5, v3, v6}, Lcom/google/android/apps/gmm/shared/c/a/k;-><init>(IILcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/f;)V

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->m:Lcom/google/android/apps/gmm/shared/c/a/j;

    const-string v0, "gmmEventBus"

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->m:Lcom/google/android/apps/gmm/shared/c/a/j;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/n/g;->j:Lcom/google/android/apps/gmm/util/replay/a;

    new-instance v5, Lcom/google/android/apps/gmm/map/util/b/h;

    invoke-direct {v5, v0, v4}, Lcom/google/android/apps/gmm/map/util/b/h;-><init>(Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/util/replay/a;)V

    iput-object v5, v3, Lcom/google/android/apps/gmm/map/n/g;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cw;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/n/g;->n:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/cw;-><init>(Lcom/google/android/apps/gmm/shared/net/a/b;)V

    iput-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->c:Lcom/google/android/apps/gmm/map/internal/c/cw;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/n/g;->c:Lcom/google/android/apps/gmm/map/internal/c/cw;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->t:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->n:Lcom/google/android/apps/gmm/shared/net/a/b;

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/n/g;->k:Lcom/google/android/apps/gmm/shared/b/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/shared/b/a;->c:Lcom/google/android/apps/gmm/m/d;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/net/a/b;->a(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/m/d;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/h;->b(Landroid/content/Context;)Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->mkdirs()Z

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/h;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    new-instance v4, Lcom/google/android/apps/gmm/map/internal/d/w;

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->h:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/a;

    invoke-direct {v4, v3, v0}, Lcom/google/android/apps/gmm/map/internal/d/w;-><init>(Lcom/google/android/apps/gmm/map/internal/d/ac;Lcom/google/android/apps/gmm/map/internal/d/c/a/a;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->n:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->q:Z

    if-nez v0, :cond_0

    iget-object v0, v3, Lcom/google/android/apps/gmm/map/n/g;->k:Lcom/google/android/apps/gmm/shared/b/a;

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->aa:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v5, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/map/internal/d/w;->a(Z)V

    :cond_1
    iput-object v4, v3, Lcom/google/android/apps/gmm/map/n/g;->o:Lcom/google/android/apps/gmm/map/internal/c/o;

    .line 311
    return-object v3

    :cond_2
    move v0, v1

    .line 310
    goto :goto_0
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/gmm/map/c/c;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->x:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/c/c;

    return-object v0
.end method

.method public final A_()Lcom/google/android/apps/gmm/map/internal/b/f;
    .locals 1

    .prologue
    .line 663
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->u:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/b/f;

    return-object v0
.end method

.method public final B()Lcom/google/android/apps/gmm/map/internal/c/o;
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->o:Lcom/google/android/apps/gmm/map/internal/c/o;

    return-object v0
.end method

.method public final B_()Landroid/accounts/Account;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 649
    const/4 v0, 0x0

    return-object v0
.end method

.method public final C()I
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->i:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final C_()Lcom/google/android/apps/gmm/map/internal/c/cw;
    .locals 1

    .prologue
    .line 668
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->c:Lcom/google/android/apps/gmm/map/internal/c/cw;

    return-object v0
.end method

.method public final D_()Lcom/google/android/apps/gmm/map/i/a/a;
    .locals 1

    .prologue
    .line 673
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->A:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/a/a;

    return-object v0
.end method

.method public final E_()Lcom/google/android/apps/gmm/shared/c/a/j;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->m:Lcom/google/android/apps/gmm/shared/c/a/j;

    return-object v0
.end method

.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->a:Landroid/content/Context;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/shared/net/r;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->t:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/r;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/shared/b/a;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->k:Lcom/google/android/apps/gmm/shared/b/a;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/util/b/g;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/shared/c/f;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->d:Lcom/google/android/apps/gmm/shared/c/f;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/internal/d/bd;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->v:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/bd;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/map/indoor/a/a;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->B:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/a/a;

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/map/q/b;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->w:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/q/b;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized q()Lcom/google/android/apps/gmm/map/e/a;
    .locals 1

    .prologue
    .line 504
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->r:Lcom/google/android/apps/gmm/map/e/a;

    .line 505
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->r:Lcom/google/android/apps/gmm/map/e/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final r()Lcom/google/android/apps/gmm/map/c/a/a;
    .locals 0

    .prologue
    .line 570
    return-object p0
.end method

.method public final t()Lcom/google/android/apps/gmm/shared/net/a/b;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->n:Lcom/google/android/apps/gmm/shared/net/a/b;

    return-object v0
.end method

.method public final t_()Lcom/google/android/apps/gmm/map/util/a/b;
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->f:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/a/b;

    return-object v0
.end method

.method public final u()Lcom/google/android/apps/gmm/map/h/d;
    .locals 1

    .prologue
    .line 610
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->y:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/h/d;

    return-object v0
.end method

.method public final u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->g:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    return-object v0
.end method

.method public final v_()Lcom/google/android/apps/gmm/map/legacy/a/a/a;
    .locals 1

    .prologue
    .line 550
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->z:Lcom/google/b/a/bw;

    invoke-interface {v0}, Lcom/google/b/a/bw;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    return-object v0
.end method

.method public final w_()Lcom/google/android/apps/gmm/m/d;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->k:Lcom/google/android/apps/gmm/shared/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->c:Lcom/google/android/apps/gmm/m/d;

    return-object v0
.end method

.method public final x_()Lcom/google/android/apps/gmm/util/replay/a;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->j:Lcom/google/android/apps/gmm/util/replay/a;

    return-object v0
.end method

.method public final y_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 499
    const-string v0, ""

    return-object v0
.end method

.method public final z()Lcom/google/android/apps/gmm/map/t/d;
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/n/g;->q:Lcom/google/android/apps/gmm/map/t/d;

    return-object v0
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 580
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/n/g;->p:Z

    return v0
.end method
