.class public Lcom/google/android/apps/gmm/map/o/c/d;
.super Lcom/google/android/apps/gmm/map/o/c/g;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

.field private final c:Lcom/google/android/apps/gmm/map/o/h;

.field private final d:[Landroid/graphics/Bitmap;

.field private final g:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/google/android/apps/gmm/map/o/c/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/o/c/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;Lcom/google/android/apps/gmm/map/o/h;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/c/g;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    .line 34
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/c/d;->c:Lcom/google/android/apps/gmm/map/o/h;

    .line 35
    invoke-static {}, Lcom/google/android/apps/gmm/map/o/b/f;->values()[Lcom/google/android/apps/gmm/map/o/b/f;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->d:[Landroid/graphics/Bitmap;

    .line 36
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/c/d;->g:Landroid/content/res/Resources;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v0, v0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/v/by;
    .locals 15

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->d:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->d:[Landroid/graphics/Bitmap;

    aget-object v1, v0, v2

    .line 52
    :goto_0
    if-eqz v1, :cond_5

    .line 53
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/c/d;->c:Lcom/google/android/apps/gmm/map/o/h;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x1

    new-instance v0, Lcom/google/android/apps/gmm/map/o/i;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/o/i;-><init>(Landroid/graphics/Bitmap;IIII)V

    iget-object v7, v6, Lcom/google/android/apps/gmm/map/o/h;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v7, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v7

    if-eqz v7, :cond_4

    move-object v0, v7

    .line 55
    :goto_1
    return-object v0

    .line 51
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v0, v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v1, v1

    float-to-int v3, v1

    const/4 v1, 0x0

    if-lez v0, :cond_1

    const/16 v4, 0x800

    if-gt v0, v4, :cond_1

    if-lez v3, :cond_1

    const/16 v4, 0x800

    if-gt v3, v4, :cond_1

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/map/o/c/d;->a:Ljava/lang/String;

    const-string v4, "Bitmap %d, %d creation failed"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v0

    invoke-static {v2, v4, v5}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->g:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v1, v0}, Landroid/graphics/Bitmap;->setDensity(I)V

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    sget-object v5, Lcom/google/android/apps/gmm/map/o/c/e;->a:[I

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->d:Lcom/google/android/apps/gmm/map/o/b/f;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/o/b/f;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/c/d;->g:Landroid/content/res/Resources;

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    if-eqz v0, :cond_3

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v7, v7, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v7, v7, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v7, v7

    float-to-int v7, v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v8, v8, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v8, v8

    float-to-int v8, v8

    invoke-virtual {v0, v5, v6, v7, v8}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(IIII)V

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/graphics/Canvas;->save(I)I

    invoke-virtual {v3, v4}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v3}, Landroid/graphics/Canvas;->restore()V

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->d:[Landroid/graphics/Bitmap;

    aput-object v1, v0, v2

    goto/16 :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/g;->k:I

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/g;->k:I

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v5, v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    goto :goto_2

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v5, v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setScale(FF)V

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_2

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->a:Lcom/google/android/apps/gmm/map/o/b/g;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/g;->l:I

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v6, -0x40800000    # -1.0f

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/i;->a:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v6, v6, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto/16 :goto_2

    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/map/o/c/d;->a:Ljava/lang/String;

    const-string v3, "Callout background resource not found."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_3

    :cond_4
    move-object v7, v0

    move-object v8, v1

    move v9, v2

    move v10, v3

    move v11, v4

    move v12, v5

    .line 53
    invoke-virtual/range {v6 .. v14}, Lcom/google/android/apps/gmm/map/o/h;->a(Lcom/google/android/apps/gmm/map/o/k;Landroid/graphics/Bitmap;IIIIFZ)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    goto/16 :goto_1

    .line 55
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 51
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/c/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a;->f:Lcom/google/android/apps/gmm/map/b/a/i;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/i;->b:I

    int-to-float v0, v0

    return v0
.end method
