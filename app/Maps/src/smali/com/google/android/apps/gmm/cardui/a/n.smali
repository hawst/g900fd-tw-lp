.class public Lcom/google/android/apps/gmm/cardui/a/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 5

    .prologue
    .line 27
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->e()Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v3

    .line 29
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->d()Lcom/google/android/apps/gmm/base/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v4

    .line 30
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->y:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kd;->d()Lcom/google/o/h/a/kd;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kd;

    iget-object v1, v0, Lcom/google/o/h/a/kd;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 31
    :goto_0
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->y:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kd;->d()Lcom/google/o/h/a/kd;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kd;

    iget-object v0, v0, Lcom/google/o/h/a/kd;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ael;->g()Lcom/google/r/b/a/ael;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ael;

    .line 28
    invoke-static {v4, v1, v0}, Lcom/google/android/apps/gmm/place/station/StationPageFragment;->a(Lcom/google/android/apps/gmm/x/a;Ljava/lang/String;Lcom/google/r/b/a/ael;)Lcom/google/android/apps/gmm/place/station/StationPageFragment;

    move-result-object v0

    .line 27
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 32
    return-void

    .line 30
    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v2, v0, Lcom/google/o/h/a/kd;->c:Ljava/lang/Object;

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 22
    sget-object v0, Lcom/google/o/h/a/g;->D:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 23
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    const/high16 v1, 0x800000

    .line 17
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/2addr v0, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
