.class public final Lcom/google/android/apps/gmm/car/e/j;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field final c:Lcom/google/android/apps/gmm/car/e/l;

.field d:Z

.field e:Z

.field public final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/gmm/car/e/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/e/j;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/car/e/l;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    new-instance v0, Lcom/google/android/apps/gmm/car/e/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/e/k;-><init>(Lcom/google/android/apps/gmm/car/e/j;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/j;->f:Ljava/lang/Object;

    .line 60
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/e/j;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 61
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/car/e/l;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/e/j;->c:Lcom/google/android/apps/gmm/car/e/l;

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/j;->f:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 66
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/apps/gmm/car/e/j;->a:Ljava/lang/String;

    .line 113
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/e/j;->d:Z

    if-eqz v0, :cond_0

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/car/e/j;->d:Z

    .line 115
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/e/j;->e:Z

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/j;->c:Lcom/google/android/apps/gmm/car/e/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/e/l;->b()V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/e/j;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    new-instance v1, Lcom/google/android/apps/gmm/car/a/e;

    sget-object v2, Lcom/google/android/apps/gmm/car/a/f;->c:Lcom/google/android/apps/gmm/car/a/f;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/car/a/e;-><init>(Lcom/google/android/apps/gmm/car/a/f;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    .line 120
    return-void
.end method
