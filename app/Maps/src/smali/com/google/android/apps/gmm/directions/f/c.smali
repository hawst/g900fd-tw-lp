.class public Lcom/google/android/apps/gmm/directions/f/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/f/b/o;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lcom/google/android/apps/gmm/directions/f/b/m;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/directions/f/c;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/c;->a:Lcom/google/b/c/cv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c;->a:Lcom/google/b/c/cv;

    .line 51
    iget-object v0, p1, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-static {v0}, Lcom/google/b/c/hj;->b(Ljava/util/Map;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/google/b/c/cv;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/f/b/o;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 35
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/f/c;->a:Lcom/google/b/c/cv;

    .line 36
    const-class v0, Lcom/google/android/apps/gmm/directions/f/b/m;

    invoke-static {v0}, Lcom/google/b/c/hj;->a(Ljava/lang/Class;)Ljava/util/EnumMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    .line 37
    invoke-virtual {p1}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/f/b/o;

    .line 38
    iget-object v2, v0, Lcom/google/android/apps/gmm/directions/f/b/o;->a:[Lcom/google/android/apps/gmm/directions/f/b/l;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 39
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    iget-object v6, v4, Lcom/google/android/apps/gmm/directions/f/b/l;->a:Lcom/google/android/apps/gmm/directions/f/b/m;

    iget v4, v4, Lcom/google/android/apps/gmm/directions/f/b/l;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_2
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 102
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/apps/gmm/directions/f/c;

    if-nez v1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/directions/f/c;

    .line 106
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/c;->a:Lcom/google/b/c/cv;

    iget-object v2, p1, Lcom/google/android/apps/gmm/directions/f/c;->a:Lcom/google/b/c/cv;

    invoke-virtual {v1, v2}, Lcom/google/b/c/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    iget-object v2, p1, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-virtual {v1, v2}, Ljava/util/EnumMap;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
