.class public Lcom/google/android/apps/gmm/place/station/am;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/b/d;
.implements Lcom/google/android/libraries/curvular/cg;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/place/station/b/d;",
        "Lcom/google/android/libraries/curvular/cg",
        "<",
        "Lcom/google/android/apps/gmm/place/station/b/d;",
        ">;",
        "Ljava/io/Serializable;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/Boolean;

.field private final b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final c:Lcom/google/maps/g/vi;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private transient d:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private transient e:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private transient f:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private transient k:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final m:Lcom/google/android/libraries/curvular/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final n:Lcom/google/android/apps/gmm/base/views/c/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final o:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final p:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final q:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final r:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final s:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private transient t:Ljava/lang/CharSequence;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final u:Ljava/util/Calendar;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final v:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final w:Lcom/google/android/apps/gmm/place/station/au;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 107
    .line 108
    invoke-static {}, Lcom/google/maps/g/vf;->g()Lcom/google/maps/g/vf;

    move-result-object v12

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    move-object v11, v2

    .line 107
    invoke-direct/range {v0 .. v12}, Lcom/google/android/apps/gmm/place/station/am;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Ljava/lang/String;Lcom/google/maps/g/vf;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/libraries/curvular/a;Lcom/google/android/apps/gmm/base/views/c/b;Lcom/google/android/apps/gmm/base/views/c/b;Ljava/lang/String;Lcom/google/maps/g/vf;)V
    .locals 7
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Lcom/google/android/libraries/curvular/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p10    # Lcom/google/android/apps/gmm/base/views/c/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/libraries/curvular/a;",
            "Lcom/google/android/apps/gmm/base/views/c/b;",
            "Lcom/google/android/apps/gmm/base/views/c/b;",
            "Ljava/lang/String;",
            "Lcom/google/maps/g/vf;",
            ")V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p4, p0, Lcom/google/android/apps/gmm/place/station/am;->g:Ljava/util/List;

    .line 127
    iput-object p5, p0, Lcom/google/android/apps/gmm/place/station/am;->h:Ljava/lang/String;

    .line 128
    iput-object p6, p0, Lcom/google/android/apps/gmm/place/station/am;->i:Lcom/google/android/libraries/curvular/a;

    .line 129
    iput-object p7, p0, Lcom/google/android/apps/gmm/place/station/am;->j:Lcom/google/android/libraries/curvular/a;

    .line 130
    iput-object p8, p0, Lcom/google/android/apps/gmm/place/station/am;->m:Lcom/google/android/libraries/curvular/a;

    .line 131
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->l:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 132
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->n:Lcom/google/android/apps/gmm/base/views/c/b;

    .line 133
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->o:Ljava/lang/String;

    .line 135
    invoke-static/range {p12 .. p12}, Lcom/google/android/apps/gmm/place/station/v;->a(Lcom/google/maps/g/vf;)Ljava/lang/String;

    move-result-object v2

    .line 136
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    :cond_0
    const/4 v3, 0x1

    :goto_0
    if-eqz v3, :cond_8

    :goto_1
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->p:Ljava/lang/String;

    .line 138
    move-object/from16 v0, p12

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;Lcom/google/maps/g/vf;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->k:Ljava/lang/CharSequence;

    .line 140
    move-object/from16 v0, p12

    iget-object v2, v0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fo;

    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v4, 0x10

    if-ne v2, v4, :cond_9

    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_a

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->f:I

    invoke-static {v2}, Lcom/google/maps/g/vi;->a(I)Lcom/google/maps/g/vi;

    move-result-object v2

    if-nez v2, :cond_1

    sget-object v2, Lcom/google/maps/g/vi;->a:Lcom/google/maps/g/vi;

    :cond_1
    :goto_3
    invoke-static {p1, v3, v2}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/maps/g/vi;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->e:Ljava/lang/CharSequence;

    .line 142
    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x20

    const/16 v3, 0x20

    if-ne v2, v3, :cond_b

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_c

    move-object/from16 v0, p12

    iget-object v2, v0, Lcom/google/maps/g/vf;->h:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fo;

    :goto_5
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;Lcom/google/maps/g/a/fo;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v3, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v4, v3, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    sget-object v3, Lcom/google/android/apps/gmm/place/station/w;->a:[I

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->f:I

    invoke-static {v2}, Lcom/google/maps/g/vi;->a(I)Lcom/google/maps/g/vi;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/vi;->a:Lcom/google/maps/g/vi;

    :cond_2
    invoke-virtual {v2}, Lcom/google/maps/g/vi;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :goto_6
    const-string v2, "%s"

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->f:Ljava/lang/CharSequence;

    .line 144
    move-object/from16 v0, p12

    iget-object v2, v0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_d

    check-cast v2, Ljava/lang/String;

    :goto_7
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->b:Ljava/lang/String;

    .line 145
    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_f

    const/4 v2, 0x1

    :goto_8
    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->b:Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_10

    :cond_3
    const/4 v2, 0x1

    :goto_9
    if-nez v2, :cond_11

    const/4 v2, 0x1

    :goto_a
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->a:Ljava/lang/Boolean;

    .line 147
    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v3, 0x40

    if-ne v2, v3, :cond_12

    const/4 v2, 0x1

    :goto_b
    if-eqz v2, :cond_15

    move-object/from16 v0, p12

    iget-object v2, v0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_13

    check-cast v2, Ljava/lang/String;

    :goto_c
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->v:Ljava/lang/String;

    .line 149
    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_16

    const/4 v2, 0x1

    :goto_d
    if-eqz v2, :cond_17

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->f:I

    invoke-static {v2}, Lcom/google/maps/g/vi;->a(I)Lcom/google/maps/g/vi;

    move-result-object v2

    if-nez v2, :cond_4

    sget-object v2, Lcom/google/maps/g/vi;->a:Lcom/google/maps/g/vi;

    :cond_4
    :goto_e
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->c:Lcom/google/maps/g/vi;

    .line 151
    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x10

    const/16 v3, 0x10

    if-ne v2, v3, :cond_18

    const/4 v2, 0x1

    :goto_f
    if-nez v2, :cond_19

    const/4 v2, 0x0

    :goto_10
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->d:Ljava/lang/CharSequence;

    .line 153
    move-object/from16 v0, p12

    iget-object v2, v0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_1b

    check-cast v2, Ljava/lang/String;

    move-object v3, v2

    :cond_5
    :goto_11
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1c

    :cond_6
    const/4 v2, 0x1

    :goto_12
    if-eqz v2, :cond_1d

    const/4 v2, 0x0

    :goto_13
    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->q:Ljava/lang/String;

    .line 155
    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1e

    const/4 v2, 0x1

    :goto_14
    if-eqz v2, :cond_1f

    .line 156
    move-object/from16 v0, p12

    iget-object v2, v0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fo;

    .line 157
    invoke-static {v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Lcom/google/maps/g/a/fo;)Ljava/util/Calendar;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->u:Ljava/util/Calendar;

    .line 159
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->u:Ljava/util/Calendar;

    const/4 v3, 0x0

    const/16 v4, 0x1a

    invoke-static {p1, v2, v3, v4}, Lcom/google/android/apps/gmm/shared/c/c/b;->a(Landroid/content/Context;Ljava/util/Calendar;ZI)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->r:Ljava/lang/String;

    .line 163
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->u:Ljava/util/Calendar;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/station/am;->c:Lcom/google/maps/g/vi;

    const-string v4, "mm"

    invoke-static {v4, v2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2, v3}, Lcom/google/android/apps/gmm/place/station/v;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/maps/g/vi;)Ljava/lang/CharSequence;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->t:Ljava/lang/CharSequence;

    .line 168
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->u:Ljava/util/Calendar;

    invoke-static {v2}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Ljava/util/Calendar;)J

    move-result-wide v4

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    const/16 v3, 0x4101

    invoke-static {p1, v4, v5, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;JLjava/util/TimeZone;I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->s:Ljava/lang/String;

    .line 178
    :goto_15
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->v:Ljava/lang/String;

    invoke-static {p1, v2, p3, p2, p0}, Lcom/google/android/apps/gmm/place/station/au;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/station/am;)Lcom/google/android/apps/gmm/place/station/au;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->w:Lcom/google/android/apps/gmm/place/station/au;

    .line 180
    return-void

    .line 136
    :cond_7
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_8
    move-object/from16 p11, v2

    goto/16 :goto_1

    .line 140
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 142
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_c
    move-object/from16 v0, p12

    iget-object v2, v0, Lcom/google/maps/g/vf;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/fo;->g()Lcom/google/maps/g/a/fo;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/fo;

    goto/16 :goto_5

    :pswitch_0
    sget v2, Lcom/google/android/apps/gmm/d;->aq:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v2

    iget-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v2, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v3, v2, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v5, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v5}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v2, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    goto/16 :goto_6

    .line 144
    :cond_d
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_e

    move-object/from16 v0, p12

    iput-object v3, v0, Lcom/google/maps/g/vf;->c:Ljava/lang/Object;

    :cond_e
    move-object v2, v3

    goto/16 :goto_7

    .line 145
    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_8

    :cond_10
    const/4 v2, 0x0

    goto/16 :goto_9

    :cond_11
    const/4 v2, 0x0

    goto/16 :goto_a

    .line 147
    :cond_12
    const/4 v2, 0x0

    goto/16 :goto_b

    :cond_13
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_14

    move-object/from16 v0, p12

    iput-object v3, v0, Lcom/google/maps/g/vf;->i:Ljava/lang/Object;

    :cond_14
    move-object v2, v3

    goto/16 :goto_c

    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_c

    .line 149
    :cond_16
    const/4 v2, 0x0

    goto/16 :goto_d

    :cond_17
    const/4 v2, 0x0

    goto/16 :goto_e

    .line 151
    :cond_18
    const/4 v2, 0x0

    goto/16 :goto_f

    :cond_19
    sget-object v3, Lcom/google/android/apps/gmm/place/station/w;->a:[I

    move-object/from16 v0, p12

    iget v2, v0, Lcom/google/maps/g/vf;->f:I

    invoke-static {v2}, Lcom/google/maps/g/vi;->a(I)Lcom/google/maps/g/vi;

    move-result-object v2

    if-nez v2, :cond_1a

    sget-object v2, Lcom/google/maps/g/vi;->a:Lcom/google/maps/g/vi;

    :cond_1a
    invoke-virtual {v2}, Lcom/google/maps/g/vi;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_1

    const/4 v2, 0x0

    goto/16 :goto_10

    :pswitch_1
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/gmm/l;->nO:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    sget v2, Lcom/google/android/apps/gmm/d;->ac:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v2

    iget-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    const-string v2, "%s"

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_10

    :pswitch_2
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/gmm/l;->nN:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    sget v2, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v2

    iget-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    const-string v2, "%s"

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_10

    :pswitch_3
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/apps/gmm/l;->nM:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    sget v2, Lcom/google/android/apps/gmm/d;->ah:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v2

    iget-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v3, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v6, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v6, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v3, v4, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    const-string v2, "%s"

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    goto/16 :goto_10

    .line 153
    :cond_1b
    check-cast v2, Lcom/google/n/f;

    invoke-virtual {v2}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/n/f;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p12

    iput-object v3, v0, Lcom/google/maps/g/vf;->j:Ljava/lang/Object;

    goto/16 :goto_11

    :cond_1c
    const/4 v2, 0x0

    goto/16 :goto_12

    :cond_1d
    sget v2, Lcom/google/android/apps/gmm/l;->nF:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-virtual {p1, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_13

    .line 155
    :cond_1e
    const/4 v2, 0x0

    goto/16 :goto_14

    .line 172
    :cond_1f
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->u:Ljava/util/Calendar;

    .line 173
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->r:Ljava/lang/String;

    .line 174
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->s:Ljava/lang/String;

    .line 175
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/station/am;->t:Ljava/lang/CharSequence;

    goto/16 :goto_15

    .line 142
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 151
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private readObject(Ljava/io/ObjectInputStream;)V
    .locals 4

    .prologue
    .line 88
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->defaultReadObject()V

    .line 90
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readInt()I

    move-result v0

    .line 91
    new-array v1, v0, [B

    .line 92
    invoke-virtual {p1, v1}, Ljava/io/ObjectInputStream;->read([B)I

    .line 93
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v2

    .line 95
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v2, v1, v3, v0}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 96
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->d:Ljava/lang/CharSequence;

    .line 97
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->e:Ljava/lang/CharSequence;

    .line 98
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->f:Ljava/lang/CharSequence;

    .line 99
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->k:Ljava/lang/CharSequence;

    .line 100
    sget-object v0, Landroid/text/TextUtils;->CHAR_SEQUENCE_CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, v2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->t:Ljava/lang/CharSequence;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 103
    return-void

    .line 102
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method private writeObject(Ljava/io/ObjectOutputStream;)V
    .locals 3

    .prologue
    .line 66
    invoke-virtual {p1}, Ljava/io/ObjectOutputStream;->defaultWriteObject()V

    .line 72
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->d:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->e:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->f:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->k:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->t:Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/text/TextUtils;->writeToParcel(Ljava/lang/CharSequence;Landroid/os/Parcel;I)V

    .line 79
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v0

    .line 80
    array-length v2, v0

    invoke-virtual {p1, v2}, Ljava/io/ObjectOutputStream;->writeInt(I)V

    .line 81
    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 83
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 84
    return-void

    .line 83
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/libraries/curvular/ce;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->w:Lcom/google/android/apps/gmm/place/station/au;

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/am;->w:Lcom/google/android/apps/gmm/place/station/au;

    invoke-static {v1}, Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;->a(Lcom/google/android/apps/gmm/place/station/au;)Lcom/google/android/apps/gmm/place/station/TransitTimelinePageFragment;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    :cond_0
    return-void
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/ee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->g:Ljava/util/List;

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->i:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->j:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->m:Lcom/google/android/libraries/curvular/a;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->l:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/views/c/b;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->n:Lcom/google/android/apps/gmm/base/views/c/b;

    return-object v0
.end method

.method public final h()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->k:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->s:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->t:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final n()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->a:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final q()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->d:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->c:Lcom/google/maps/g/vi;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->c:Lcom/google/maps/g/vi;

    sget-object v1, Lcom/google/maps/g/vi;->b:Lcom/google/maps/g/vi;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Ljava/util/Calendar;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->u:Ljava/util/Calendar;

    return-object v0
.end method

.method public final w()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/am;->v:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Lcom/google/android/libraries/curvular/cg;
    .locals 0
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/libraries/curvular/cg",
            "<",
            "Lcom/google/android/apps/gmm/place/station/b/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 340
    return-object p0
.end method
