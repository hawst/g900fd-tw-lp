.class public Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;
.super Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 3

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->og:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 88
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 90
    sget v0, Lcom/google/android/apps/gmm/h;->aw:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;->addPreferencesFromResource(I)V

    .line 92
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/h;->ak:I

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setLayoutResource(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 108
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 109
    sget v1, Lcom/google/android/apps/gmm/l;->Q:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 110
    return-object v0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 3

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 161
    :goto_0
    return v0

    .line 119
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/BaseSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    .line 120
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 122
    const-string v2, "how_to_get_started"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 134
    const-string v0, "https://support.google.com/gmm/?p=maps_android_getstarted"

    .line 135
    sget-object v2, Lcom/google/b/f/t;->eP:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 156
    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/util/q;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 159
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/TutorialsSettingsFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 161
    const/4 v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    const-string v2, "how_to_search_and_manage_contacts"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 138
    const-string v0, "https://support.google.com/gmm/?p=maps_android_contacts"

    .line 139
    sget-object v2, Lcom/google/b/f/t;->eQ:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    goto :goto_1

    .line 141
    :cond_2
    const-string v2, "gestures"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 142
    const-string v0, "https://support.google.com/gmm/?p=maps_android_gestures"

    .line 143
    sget-object v2, Lcom/google/b/f/t;->eN:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    goto :goto_1

    .line 145
    :cond_3
    const-string v2, "tips_and_tricks"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 146
    const-string v0, "https://support.google.com/gmm/?p=maps_android_tips_tricks"

    .line 147
    sget-object v2, Lcom/google/b/f/t;->eR:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    goto :goto_1

    .line 150
    :cond_4
    const-string v0, "https://support.google.com/gmm/?p=maps_android"

    .line 151
    sget-object v2, Lcom/google/b/f/t;->eO:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    goto :goto_1
.end method
