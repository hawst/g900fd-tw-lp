.class public Lcom/google/android/apps/gmm/map/internal/d/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/ak;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final c:Lcom/google/android/apps/gmm/map/internal/d/a/c;

.field final d:Lcom/google/android/apps/gmm/map/internal/d/c;

.field final e:Lcom/google/android/apps/gmm/map/internal/d/n;

.field final f:Z

.field final g:Lcom/google/android/apps/gmm/map/b/a/ai;

.field final h:Lcom/google/b/a/an;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/a/an",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field i:I

.field j:I

.field k:I

.field l:Z

.field volatile m:Z

.field n:Lcom/google/android/apps/gmm/map/internal/d/p;

.field final o:Lcom/google/android/apps/gmm/map/internal/d/p;

.field p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bo;",
            ">;"
        }
    .end annotation
.end field

.field q:Z

.field r:Z

.field s:Z

.field t:Z

.field u:Z


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v6, -0x1

    .line 2624
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/c;->b:Lcom/google/android/apps/gmm/map/internal/d/c;

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/d/n;->c:Lcom/google/android/apps/gmm/map/internal/d/n;

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v7, v6

    move v9, v8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/n;IIIZLcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 2632
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/n;IIIZLcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 2
    .param p10    # Lcom/google/android/apps/gmm/map/internal/d/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2605
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2527
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->l:Z

    .line 2534
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->m:Z

    .line 2540
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 2552
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/d;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->p:Ljava/util/List;

    .line 2558
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->q:Z

    .line 2564
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    .line 2571
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->s:Z

    .line 2576
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    .line 2581
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->u:Z

    .line 2606
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 2607
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 2608
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 2609
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 2610
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->c:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    .line 2611
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 2612
    invoke-static {p4}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    .line 2613
    invoke-static {p4}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->u:Z

    .line 2614
    iput p6, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->i:I

    .line 2615
    iput p7, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->j:I

    .line 2616
    iput p8, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->k:I

    .line 2617
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->e:Lcom/google/android/apps/gmm/map/internal/d/n;

    .line 2618
    iput-boolean p9, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->f:Z

    .line 2619
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;)Lcom/google/b/a/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->h:Lcom/google/b/a/an;

    .line 2620
    iput-object p10, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->o:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 2621
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/n;)V
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v6, -0x1

    .line 2638
    sget-object v4, Lcom/google/android/apps/gmm/map/internal/d/c;->b:Lcom/google/android/apps/gmm/map/internal/d/c;

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    move v7, v6

    move v9, v8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/map/internal/d/p;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;Lcom/google/android/apps/gmm/map/internal/d/c;Lcom/google/android/apps/gmm/map/internal/d/n;IIIZLcom/google/android/apps/gmm/map/internal/d/p;)V

    .line 2646
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/internal/d/p;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 2509
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->c:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->c:Lcom/google/android/apps/gmm/map/internal/d/a/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/d/a/c;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;ILcom/google/android/apps/gmm/map/internal/c/bo;Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z
    .locals 1

    .prologue
    .line 2667
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->e:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 2668
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/c;->c:Lcom/google/android/apps/gmm/map/internal/d/c;

    .line 2669
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/internal/d/p;)V
    .locals 2

    .prologue
    .line 2752
    move-object v0, p0

    .line 2753
    :goto_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    if-eqz v1, :cond_0

    .line 2754
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto :goto_0

    .line 2756
    :cond_0
    iput-object p1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    .line 2759
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->l:Z

    if-eqz v0, :cond_1

    .line 2760
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->l:Z

    .line 2762
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/d/p;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z

    move-result v1

    and-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    .line 2763
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->u:Z

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/d/p;->d:Lcom/google/android/apps/gmm/map/internal/d/c;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/internal/d/aj;->a(Lcom/google/android/apps/gmm/map/internal/d/c;)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->u:Z

    .line 2764
    return-void
.end method

.method protected final a(Z)V
    .locals 1

    .prologue
    .line 2726
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->q:Z

    .line 2727
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    if-eqz v0, :cond_0

    .line 2728
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    :goto_0
    if-eqz v0, :cond_0

    .line 2730
    iput-boolean p1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->q:Z

    .line 2729
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto :goto_0

    .line 2733
    :cond_0
    return-void
.end method

.method protected final a()Z
    .locals 1

    .prologue
    .line 2708
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->t:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->s:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final b(Z)V
    .locals 2

    .prologue
    .line 2739
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    .line 2740
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    if-eqz v0, :cond_0

    .line 2741
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    :goto_0
    if-eqz v0, :cond_0

    .line 2743
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    or-int/2addr v1, p1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->r:Z

    .line 2742
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->n:Lcom/google/android/apps/gmm/map/internal/d/p;

    goto :goto_0

    .line 2746
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 2785
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
