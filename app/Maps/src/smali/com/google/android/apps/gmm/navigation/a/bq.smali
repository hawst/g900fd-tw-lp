.class public Lcom/google/android/apps/gmm/navigation/a/bq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/a/g;


# static fields
.field public static final a:[J

.field public static final b:[J

.field public static final c:[J

.field public static final d:[J

.field public static final e:[J


# instance fields
.field private final f:Landroid/os/Vibrator;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x6

    .line 21
    new-array v0, v1, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->a:[J

    .line 22
    new-array v0, v1, [J

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->b:[J

    .line 23
    new-array v0, v1, [J

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->c:[J

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_3

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->d:[J

    .line 25
    const/4 v0, 0x2

    new-array v0, v0, [J

    fill-array-data v0, :array_4

    sput-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->e:[J

    return-void

    .line 21
    nop

    :array_0
    .array-data 8
        0x0
        0xc8
        0x12c
        0xc8
        0x12c
        0x1f4
    .end array-data

    .line 22
    :array_1
    .array-data 8
        0x0
        0x1f4
        0x12c
        0xc8
        0x12c
        0xc8
    .end array-data

    .line 23
    :array_2
    .array-data 8
        0x0
        0xc8
        0x12c
        0xc8
        0x12c
        0xc8
    .end array-data

    .line 24
    :array_3
    .array-data 8
        0x0
        0x5dc
        0x1f4
        0x5dc
    .end array-data

    .line 25
    :array_4
    .array-data 8
        0x0
        0x1f4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "vibrator"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/bq;->f:Landroid/os/Vibrator;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/navigation/a/b/h;)Lcom/google/android/apps/gmm/navigation/a/a;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 36
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 38
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->d:Lcom/google/android/apps/gmm/map/r/a/am;

    if-eqz v0, :cond_5

    .line 39
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->d:Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 41
    :goto_0
    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->d:Lcom/google/android/apps/gmm/navigation/a/b/j;

    if-ne v2, v3, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->d:[J

    .line 42
    :goto_1
    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/bp;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/bq;->f:Landroid/os/Vibrator;

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/navigation/a/bp;-><init>(Landroid/os/Vibrator;[J)V

    :cond_0
    return-object v1

    .line 41
    :cond_1
    if-eqz v0, :cond_4

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v3, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    if-eq v2, v3, :cond_2

    sget-object v3, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    if-ne v2, v3, :cond_3

    :cond_2
    const/4 v2, 0x1

    :goto_2
    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/br;->a:[I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    invoke-virtual {v0}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->c:[J

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->a:[J

    goto :goto_1

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->b:[J

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/bq;->e:[J

    goto :goto_1

    :cond_5
    move-object v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method
