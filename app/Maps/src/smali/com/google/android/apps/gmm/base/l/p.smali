.class public Lcom/google/android/apps/gmm/base/l/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/o;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/indoor/d/e;

.field b:Z

.field c:Z

.field d:Z

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/apps/gmm/base/l/r;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/indoor/d/e;Lcom/google/android/apps/gmm/base/l/r;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->b:Z

    .line 19
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->c:Z

    .line 20
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->d:Z

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/p;->e:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    .line 31
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/l/p;->f:Lcom/google/android/apps/gmm/base/l/r;

    .line 32
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->b:Z

    if-nez v0, :cond_1

    .line 48
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/p;->f:Lcom/google/android/apps/gmm/base/l/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/indoor/d/e;->d:Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/l/r;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/l/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/e;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/l/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/s/c;

    sget-object v2, Lcom/google/android/apps/gmm/map/s/a;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/s/c;-><init>(Lcom/google/android/apps/gmm/map/s/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 49
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->b:Z

    .line 51
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/indoor/d/e;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 92
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->d:Z

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/p;->e:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->C:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/e;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 94
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->c:Z

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/p;->e:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->B:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/e;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 96
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/l/p;->d:Z

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/p;->e:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->A:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/e;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/p;->e:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->z:I

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/p;->a:Lcom/google/android/apps/gmm/map/indoor/d/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/indoor/d/e;->c:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
