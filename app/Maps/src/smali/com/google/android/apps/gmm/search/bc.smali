.class public Lcom/google/android/apps/gmm/search/bc;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/apps/gmm/search/bc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(I)Lcom/google/e/a/a/a/b;
    .locals 4

    .prologue
    .line 87
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/maps/a/a/a;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 88
    const/4 v1, 0x2

    int-to-long v2, p0

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 89
    const/4 v1, 0x1

    int-to-long v2, p0

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 90
    return-object v0
.end method

.method public static a(Landroid/graphics/Point;Landroid/content/res/Resources;)Lcom/google/e/a/a/a/b;
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    .line 95
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/aw;->f:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 97
    if-eqz p0, :cond_1

    .line 98
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v2, Lcom/google/r/b/a/b/aw;->g:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v2}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 101
    iget v2, p0, Landroid/graphics/Point;->x:I

    iget v3, p0, Landroid/graphics/Point;->y:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 102
    const/4 v3, 0x1

    int-to-long v4, v2

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 103
    if-eqz p1, :cond_0

    .line 104
    sget v2, Lcom/google/android/apps/gmm/e;->aY:I

    .line 105
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 104
    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v6, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 106
    const/4 v2, 0x4

    sget v3, Lcom/google/android/apps/gmm/e;->aZ:I

    .line 107
    invoke-virtual {p1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 106
    int-to-long v4, v3

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v2, v3}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 109
    :cond_0
    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v6, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 111
    :cond_1
    int-to-long v2, v7

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v7, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 114
    return-object v0
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/e/a/a/a/b;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/search/f;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v13, 0x1c

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 62
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 63
    invoke-static {p0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v7

    .line 65
    array-length v8, v7

    move v5, v3

    :goto_0
    if-ge v5, v8, :cond_4

    aget-object v9, v7, v5

    .line 66
    invoke-virtual {v9, v4, v13}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 67
    const/4 v1, 0x2

    invoke-virtual {v9, v1, v13}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 68
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v4

    :goto_1
    if-nez v2, :cond_3

    .line 69
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v10

    .line 70
    new-instance v11, Lcom/google/b/c/cx;

    invoke-direct {v11}, Lcom/google/b/c/cx;-><init>()V

    .line 72
    const/4 v1, 0x6

    invoke-static {v9, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v9

    .line 74
    array-length v12, v9

    move v2, v3

    :goto_2
    if-ge v2, v12, :cond_2

    aget-object v1, v9, v2

    .line 75
    invoke-virtual {v1, v4, v13}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v11, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 74
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_1
    move v2, v3

    .line 68
    goto :goto_1

    .line 78
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/search/f;

    .line 79
    invoke-virtual {v11}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v2

    invoke-direct {v1, v0, v10, v2}, Lcom/google/android/apps/gmm/search/f;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/b/c/cv;)V

    .line 80
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 83
    :cond_4
    return-object v6
.end method

.method public static a(Lcom/google/e/a/a/a/b;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/e/a/a/a/b;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    const/16 v0, 0xb

    .line 50
    invoke-static {}, Lcom/google/r/b/a/ada;->g()Lcom/google/r/b/a/ada;

    move-result-object v1

    .line 48
    invoke-static {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;ILcom/google/n/at;)Lcom/google/b/c/cv;

    .line 51
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->d(Lcom/google/e/a/a/a/b;I)[Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 52
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 53
    new-instance v4, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 54
    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v3

    sget-object v4, Lcom/google/b/f/t;->gb:Lcom/google/b/f/t;

    .line 55
    iput-object v4, v3, Lcom/google/android/apps/gmm/base/g/g;->p:Lcom/google/b/f/cq;

    .line 56
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v3

    .line 53
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    return-void
.end method
