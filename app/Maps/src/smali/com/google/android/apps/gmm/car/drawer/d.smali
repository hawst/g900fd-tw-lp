.class Lcom/google/android/apps/gmm/car/drawer/d;
.super Landroid/support/v4/widget/bn;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;)V
    .locals 0

    .prologue
    .line 745
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-direct {p0}, Landroid/support/v4/widget/bn;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 833
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 837
    :goto_0
    return v0

    .line 836
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 837
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {p2, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 760
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->f:Lcom/google/android/apps/gmm/car/drawer/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/drawer/c;->b()V

    .line 761
    :cond_0
    :goto_0
    return-void

    .line 760
    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    if-nez v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->f:Lcom/google/android/apps/gmm/car/drawer/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/drawer/c;->a()V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 813
    and-int/lit8 v0, p1, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 814
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 824
    :cond_0
    :goto_0
    return-void

    .line 818
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 823
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1, p2}, Landroid/support/v4/widget/bk;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;F)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 787
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 788
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v1

    .line 791
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 796
    cmpl-float v0, p2, v5

    if-lez v0, :cond_0

    move v0, v1

    .line 802
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/widget/bk;->a(II)Z

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    .line 804
    return-void

    :cond_0
    move v0, v2

    .line 796
    goto :goto_0

    .line 799
    :cond_1
    cmpg-float v1, p2, v5

    if-gez v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->getMarginEnd()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    sub-int v2, v0, v1

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final a(Landroid/view/View;II)V
    .locals 4

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v0

    .line 768
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 769
    int-to-float v1, p2

    int-to-float v0, v0

    div-float v0, v1, v0

    move v1, v0

    .line 775
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iget v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    cmpl-float v0, v1, v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    iput v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->b:F

    iget-object v0, v2, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->f:Lcom/google/android/apps/gmm/car/drawer/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/drawer/c;->a(F)V

    .line 777
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->a()V

    .line 778
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b()V

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p3}, Landroid/widget/FrameLayout;->offsetLeftAndRight(I)V

    .line 781
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->c()V

    .line 782
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    .line 783
    return-void

    .line 771
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->getWidth()I

    move-result v1

    .line 772
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    add-int/2addr v2, p2

    .line 773
    sub-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    move v1, v0

    goto :goto_0
.end method

.method public final a(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;

    .line 749
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->g:Landroid/view/ViewGroup;

    if-ne p1, v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v1, :cond_1

    move v1, v2

    .line 750
    :goto_0
    if-eqz v1, :cond_2

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    if-eqz v4, :cond_2

    .line 751
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    .line 755
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v1, v3

    .line 749
    goto :goto_0

    .line 752
    :cond_2
    if-eqz v1, :cond_0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout$LayoutParams;->c:Z

    if-nez v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/drawer/d;->a:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->i:Z

    goto :goto_1
.end method

.method public final c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 828
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 808
    const/4 v0, 0x0

    return v0
.end method

.method public final d(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 842
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    return v0
.end method
