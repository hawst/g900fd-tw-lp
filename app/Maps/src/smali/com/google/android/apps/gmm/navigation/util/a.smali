.class public Lcom/google/android/apps/gmm/navigation/util/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/r/b/a/a/a/b;

.field public final b:Landroid/content/Context;

.field private c:J

.field private d:J

.field private final e:Lcom/google/android/apps/gmm/shared/c/f;

.field private final f:Lcom/google/android/apps/gmm/x/a;

.field private final g:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Z)V
    .locals 8

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->b:Landroid/content/Context;

    .line 95
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->f:Lcom/google/android/apps/gmm/x/a;

    .line 96
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->e:Lcom/google/android/apps/gmm/shared/c/f;

    .line 97
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->g:I

    .line 99
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->f:Lcom/google/android/apps/gmm/x/a;

    sget-object v1, Lcom/google/android/apps/gmm/x/g;->b:Lcom/google/android/apps/gmm/x/g;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/util/a;->g:I

    sget-object v3, Lcom/google/r/b/a/a/a/b;->PARSER:Lcom/google/n/ax;

    new-instance v4, Lcom/google/android/apps/gmm/x/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/g;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v2}, Lcom/google/android/apps/gmm/x/l;-><init>(Ljava/lang/String;I)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/x/a;->d:Lcom/google/android/apps/gmm/x/i;

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/x/i;->a(Lcom/google/android/apps/gmm/x/l;)[B

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a([BLcom/google/n/ax;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    if-nez v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->e:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    const-wide/32 v2, 0x5265c00

    sub-long v4, v0, v2

    .line 107
    if-eqz p2, :cond_2

    .line 110
    const-wide/16 v0, 0x0

    move-wide v2, v0

    .line 117
    :goto_1
    invoke-static {}, Lcom/google/r/b/a/a/a/b;->newBuilder()Lcom/google/r/b/a/a/a/d;

    move-result-object v6

    .line 118
    iget v7, v6, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v2, v6, Lcom/google/r/b/a/a/a/d;->b:J

    .line 119
    iget v2, v6, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v6, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v0, v6, Lcom/google/r/b/a/a/a/d;->c:J

    .line 120
    iget v0, v6, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v6, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v4, v6, Lcom/google/r/b/a/a/a/d;->d:J

    .line 121
    invoke-virtual {v6}, Lcom/google/r/b/a/a/a/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    .line 123
    :cond_0
    return-void

    .line 97
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 113
    :cond_2
    const-wide/16 v2, 0x64

    .line 114
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    .line 115
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->X:I

    int-to-long v0, v0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 129
    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Can\'t start BatteryDrainTracker twice without stopping."

    const-string v1, ""

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 133
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/a;->b(Landroid/content/Context;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->c:J

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->e:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->d:J

    .line 135
    return-void
.end method

.method public final b()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 141
    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->d:J

    cmp-long v0, v0, v10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Can\'t stop BatteryDrainTracker without starting."

    const-string v1, ""

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 145
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->e:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    .line 146
    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/util/a;->d:J

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/util/a;->c:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 148
    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/util/a;->d:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    .line 149
    const-wide/16 v4, 0x12c

    cmp-long v4, v2, v4

    if-ltz v4, :cond_3

    .line 150
    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/util/a;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/h/a;->b(Landroid/content/Context;)I

    move-result v4

    .line 151
    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    .line 152
    iget-wide v6, p0, Lcom/google/android/apps/gmm/navigation/util/a;->c:J

    int-to-long v4, v4

    sub-long v4, v6, v4

    .line 153
    iget-object v6, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    iget-wide v6, v6, Lcom/google/r/b/a/a/a/b;->d:J

    sub-long v6, v0, v6

    long-to-double v6, v6

    const-wide v8, 0x4194997000000000L    # 8.64E7

    div-double/2addr v6, v8

    const-wide v8, 0x3fef5c28f5c28f5cL    # 0.98

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    iget-object v8, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    iget-wide v8, v8, Lcom/google/r/b/a/a/a/b;->b:J

    long-to-double v8, v8

    mul-double/2addr v8, v6

    double-to-long v8, v8

    add-long/2addr v4, v8

    iget-object v8, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    iget-wide v8, v8, Lcom/google/r/b/a/a/a/b;->c:J

    long-to-double v8, v8

    mul-double/2addr v6, v8

    double-to-long v6, v6

    add-long/2addr v2, v6

    invoke-static {}, Lcom/google/r/b/a/a/a/b;->newBuilder()Lcom/google/r/b/a/a/a/d;

    move-result-object v6

    iget v7, v6, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v6, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v4, v6, Lcom/google/r/b/a/a/a/d;->b:J

    iget v4, v6, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v6, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v2, v6, Lcom/google/r/b/a/a/a/d;->c:J

    iget v2, v6, Lcom/google/r/b/a/a/a/d;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v6, Lcom/google/r/b/a/a/a/d;->a:I

    iput-wide v0, v6, Lcom/google/r/b/a/a/a/d;->d:J

    invoke-virtual {v6}, Lcom/google/r/b/a/a/a/d;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    .line 154
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/a;->f:Lcom/google/android/apps/gmm/x/a;

    sget-object v1, Lcom/google/android/apps/gmm/x/g;->b:Lcom/google/android/apps/gmm/x/g;

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/util/a;->g:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/a;->a:Lcom/google/r/b/a/a/a/b;

    new-instance v4, Lcom/google/android/apps/gmm/x/l;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/g;->name()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1, v2}, Lcom/google/android/apps/gmm/x/l;-><init>(Ljava/lang/String;I)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/x/a;->d:Lcom/google/android/apps/gmm/x/i;

    invoke-interface {v3}, Lcom/google/n/at;->l()[B

    move-result-object v1

    invoke-interface {v0, v4, v1}, Lcom/google/android/apps/gmm/x/i;->a(Lcom/google/android/apps/gmm/x/l;[B)V

    .line 159
    :cond_3
    iput-wide v10, p0, Lcom/google/android/apps/gmm/navigation/util/a;->d:J

    .line 160
    return-void
.end method
