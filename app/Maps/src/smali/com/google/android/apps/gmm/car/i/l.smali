.class public final Lcom/google/android/apps/gmm/car/i/l;
.super Landroid/support/v7/widget/bk;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/car/support/ax;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/bk",
        "<",
        "Landroid/support/v7/widget/ce;",
        ">;",
        "Lcom/google/android/gms/car/support/ax;"
    }
.end annotation


# instance fields
.field final c:Lcom/google/android/libraries/curvular/bm;

.field private final d:Lcom/google/android/libraries/curvular/bd;

.field private final e:I

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/curvular/bd;II)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v7/widget/bk;-><init>()V

    .line 50
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->d:Lcom/google/android/libraries/curvular/bd;

    .line 51
    iput p2, p0, Lcom/google/android/apps/gmm/car/i/l;->e:I

    .line 52
    iput p3, p0, Lcom/google/android/apps/gmm/car/i/l;->f:I

    .line 53
    new-instance v0, Lcom/google/android/libraries/curvular/bm;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/curvular/bm;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    .line 54
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)I
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/curvular/bm;->b(I)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/ce;
    .locals 5

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v1, v0, Lcom/google/android/libraries/curvular/bm;->c:Lcom/google/android/libraries/curvular/bd;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/curvular/bm;->c(I)Ljava/lang/Class;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 111
    iget v1, p0, Lcom/google/android/apps/gmm/car/i/l;->f:I

    if-eqz v1, :cond_0

    .line 112
    new-instance v1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    const/4 v2, -0x1

    .line 113
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/car/i/l;->f:I

    div-int/2addr v3, v4

    invoke-direct {v1, v2, v3}, Landroid/support/v7/widget/RecyclerView$LayoutParams;-><init>(II)V

    .line 112
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/car/i/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/car/i/n;-><init>(Lcom/google/android/apps/gmm/car/i/l;Landroid/view/View;)V

    return-object v1
.end method

.method public final a(Landroid/support/v7/widget/ce;I)V
    .locals 2

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v1, p1, Landroid/support/v7/widget/ce;->a:Landroid/view/View;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/curvular/bm;->a(Landroid/view/View;I)Landroid/view/View;

    .line 121
    return-void
.end method

.method public final a(Ljava/lang/Class;I)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<",
            "Lcom/google/android/apps/gmm/car/i/b/b;",
            ">;>;I)V"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->d:Lcom/google/android/libraries/curvular/bd;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 63
    new-instance v2, Lcom/google/android/apps/gmm/car/i/b/f;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/car/i/b/f;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    invoke-virtual {p0, p1, v1}, Lcom/google/android/apps/gmm/car/i/l;->a(Ljava/lang/Class;Ljava/util/Collection;)V

    .line 65
    return-void
.end method

.method public final a(Ljava/lang/Class;Ljava/util/Collection;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/libraries/curvular/ce;",
            ">(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>;",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 78
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    .line 79
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/gmm/car/i/l;->e:I

    if-ge v2, v3, :cond_0

    .line 80
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/libraries/curvular/bm;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 85
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/car/i/l;->f:I

    if-eqz v0, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/car/i/l;->f:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/apps/gmm/car/i/l;->f:I

    div-int/2addr v0, v1

    .line 89
    iget v1, p0, Lcom/google/android/apps/gmm/car/i/l;->f:I

    mul-int/2addr v1, v0

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 91
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/l;->c:Lcom/google/android/libraries/curvular/bm;

    const-class v3, Lcom/google/android/apps/gmm/car/i/a/a;

    new-instance v4, Lcom/google/android/apps/gmm/car/i/m;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/car/i/m;-><init>(Lcom/google/android/apps/gmm/car/i/l;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/bm;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 95
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/i/l;->b()V

    .line 96
    return-void
.end method
