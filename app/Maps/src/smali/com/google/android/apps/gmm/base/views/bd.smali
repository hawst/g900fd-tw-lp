.class Lcom/google/android/apps/gmm/base/views/bd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:F

.field final synthetic b:D

.field final synthetic c:Lcom/google/android/apps/gmm/base/views/bc;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/views/bc;FD)V
    .locals 1

    .prologue
    .line 543
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/bd;->c:Lcom/google/android/apps/gmm/base/views/bc;

    iput p2, p0, Lcom/google/android/apps/gmm/base/views/bd;->a:F

    iput-wide p3, p0, Lcom/google/android/apps/gmm/base/views/bd;->b:D

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 546
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bd;->c:Lcom/google/android/apps/gmm/base/views/bc;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/bc;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget v1, p0, Lcom/google/android/apps/gmm/base/views/bd;->a:F

    iget-wide v2, p0, Lcom/google/android/apps/gmm/base/views/bd;->b:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->n:D

    float-to-double v6, v1

    cmpl-double v4, v4, v6

    if-eqz v4, :cond_0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->setAlpha(F)V

    :cond_0
    float-to-double v4, v1

    iput-wide v4, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->n:D

    iput-wide v2, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->m:D

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getAlpha()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->invalidate()V

    .line 547
    :cond_1
    return-void
.end method
