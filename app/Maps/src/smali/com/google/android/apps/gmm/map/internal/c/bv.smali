.class public abstract enum Lcom/google/android/apps/gmm/map/internal/c/bv;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/internal/c/bv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/internal/c/bv;

.field public static final enum b:Lcom/google/android/apps/gmm/map/internal/c/bv;

.field public static final enum c:Lcom/google/android/apps/gmm/map/internal/c/bv;

.field public static final enum d:Lcom/google/android/apps/gmm/map/internal/c/bv;

.field public static final enum e:Lcom/google/android/apps/gmm/map/internal/c/bv;

.field public static final enum f:Lcom/google/android/apps/gmm/map/internal/c/bv;

.field public static final enum g:Lcom/google/android/apps/gmm/map/internal/c/bv;

.field private static final synthetic h:[Lcom/google/android/apps/gmm/map/internal/c/bv;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 33
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bw;

    const-string v1, "SPOTLIGHT_DIFFTILE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/map/internal/c/bw;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->a:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bx;

    const-string v1, "INDOOR"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/map/internal/c/bx;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/by;

    const-string v1, "TRANSIT"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/map/internal/c/by;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->c:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bz;

    const-string v1, "ALTERNATE_PAINTFE"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/map/internal/c/bz;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->d:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 89
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/ca;

    const-string v1, "ACCOUNT"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/map/internal/c/ca;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->e:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 97
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cb;

    const-string v1, "QSTYLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/cb;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->f:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 125
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cc;

    const-string v1, "MY_MAPS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/c/cc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->g:Lcom/google/android/apps/gmm/map/internal/c/bv;

    .line 31
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/internal/c/bv;

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/bv;->a:Lcom/google/android/apps/gmm/map/internal/c/bv;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/bv;->b:Lcom/google/android/apps/gmm/map/internal/c/bv;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/bv;->c:Lcom/google/android/apps/gmm/map/internal/c/bv;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/bv;->d:Lcom/google/android/apps/gmm/map/internal/c/bv;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/bv;->e:Lcom/google/android/apps/gmm/map/internal/c/bv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/bv;->f:Lcom/google/android/apps/gmm/map/internal/c/bv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/bv;->g:Lcom/google/android/apps/gmm/map/internal/c/bv;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->h:[Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/bv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->h:[Lcom/google/android/apps/gmm/map/internal/c/bv;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/internal/c/bv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/internal/c/bu;
.end method
