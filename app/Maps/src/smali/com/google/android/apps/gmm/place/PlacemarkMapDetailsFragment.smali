.class public Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;
.super Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;
.source "PG"


# instance fields
.field private A:Lcom/google/android/apps/gmm/map/b/a/q;

.field private B:Z

.field n:Landroid/view/View;

.field o:Lcom/google/android/apps/gmm/place/m;

.field public final p:Ljava/lang/Object;

.field public final q:Lcom/google/android/apps/gmm/hotels/a/g;

.field r:Lcom/google/android/apps/gmm/base/fragments/m;

.field s:Lcom/google/android/apps/gmm/base/placelists/p;

.field t:Lcom/google/android/apps/gmm/base/placelists/o;

.field u:Lcom/google/android/apps/gmm/place/bn;

.field private v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

.field private w:Landroid/view/View;

.field private x:Ljava/lang/String;

.field private y:Lcom/google/android/apps/gmm/base/l/aj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;-><init>()V

    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->x:Ljava/lang/String;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/place/bk;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/bk;-><init>(Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->p:Ljava/lang/Object;

    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/place/bl;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/bl;-><init>(Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->q:Lcom/google/android/apps/gmm/hotels/a/g;

    .line 187
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->B:Z

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;
    .locals 4
    .param p2    # Lcom/google/maps/g/hy;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 198
    new-instance v0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;-><init>()V

    .line 199
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 200
    invoke-static {p1}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v2

    .line 201
    const-string v3, "placemark"

    invoke-virtual {p0, v1, v3, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 202
    if-eqz p2, :cond_0

    .line 203
    const-string v2, "loggingParams"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 205
    :cond_0
    const-string v2, "displayFloatingBarText"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    const-string v2, "usePlacemarkCamera"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 208
    return-object v0
.end method


# virtual methods
.method a(Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 352
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 391
    :cond_0
    :goto_0
    return-void

    .line 356
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    .line 357
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_0

    .line 360
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    .line 361
    if-eqz v2, :cond_0

    .line 366
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->A:Lcom/google/android/apps/gmm/map/b/a/q;

    if-nez v2, :cond_2

    .line 367
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->A:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 370
    :cond_2
    if-nez p1, :cond_3

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->B:Z

    if-nez v0, :cond_5

    :cond_3
    const/4 v5, 0x1

    .line 371
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "usePlacemarkCamera"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->s:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->A:Lcom/google/android/apps/gmm/map/b/a/q;

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/placelists/p;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/map/b/a/q;IZZZ)V

    .line 374
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->B:Z

    or-int/2addr v0, v5

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->B:Z

    .line 376
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->T()Lcom/google/b/c/cv;

    move-result-object v2

    .line 377
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/map/indoor/d/f;

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/indoor/d/f;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Lcom/google/android/apps/gmm/map/b/a/l;)Z

    move-result v0

    if-eqz v0, :cond_4

    goto :goto_0

    :cond_5
    move v5, v3

    .line 370
    goto :goto_1

    .line 387
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/indoor/d/f;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a(Lcom/google/android/apps/gmm/map/indoor/d/f;)V

    goto/16 :goto_0
.end method

.method protected final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->g()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->s:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b(I)V

    .line 333
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->r:Lcom/google/android/apps/gmm/base/fragments/m;

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->r:Lcom/google/android/apps/gmm/base/fragments/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/m;->a()Lcom/google/android/apps/gmm/base/fragments/m;

    .line 336
    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->a(Z)V

    .line 337
    return-void
.end method

.method public final e_()Lcom/google/android/apps/gmm/base/fragments/a/a;
    .locals 1

    .prologue
    .line 322
    sget-object v0, Lcom/google/android/apps/gmm/base/fragments/a/a;->b:Lcom/google/android/apps/gmm/base/fragments/a/a;

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 327
    sget-object v0, Lcom/google/b/f/t;->et:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->c:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->t:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/gmm/base/placelists/o;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 397
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "displayFloatingBarText"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->x:Ljava/lang/String;

    .line 217
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/placelists/o;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->t:Lcom/google/android/apps/gmm/base/placelists/o;

    .line 218
    new-instance v0, Lcom/google/android/apps/gmm/place/bn;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/bn;-><init>(Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->u:Lcom/google/android/apps/gmm/place/bn;

    .line 219
    if-eqz p1, :cond_0

    .line 220
    const-string v0, "camera-moved"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->B:Z

    .line 223
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/l/aj;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->y:Lcom/google/android/apps/gmm/base/l/aj;

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 226
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 231
    sget v0, Lcom/google/android/apps/gmm/h;->as:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->u:Lcom/google/android/apps/gmm/place/bn;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->setAdapter(Lcom/google/android/apps/gmm/place/am;)V

    new-instance v0, Lcom/google/android/apps/gmm/place/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->t:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/place/ac;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/app/Fragment;)V

    new-instance v1, Lcom/google/android/apps/gmm/place/bm;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/bm;-><init>(Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/ac;->a:Lcom/google/android/apps/gmm/search/views/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iput-object v0, v1, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->l:Lcom/google/android/apps/gmm/place/an;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/f/b;->a(Lcom/google/android/libraries/curvular/bd;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->w:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->w:Landroid/view/View;

    sget-object v1, Lcom/google/android/apps/gmm/base/f/b;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->n:Landroid/view/View;

    new-instance v0, Lcom/google/android/apps/gmm/place/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/m;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->o:Lcom/google/android/apps/gmm/place/m;

    .line 233
    return-object v3
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 269
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->onDestroy()V

    .line 270
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 304
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->onPause()V

    .line 307
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->s:Lcom/google/android/apps/gmm/base/placelists/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/p;->a()V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->r:Lcom/google/android/apps/gmm/base/fragments/m;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->r:Lcom/google/android/apps/gmm/base/fragments/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/m;->b()V

    .line 310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->r:Lcom/google/android/apps/gmm/base/fragments/m;

    .line 312
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    .line 274
    invoke-super {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->onResume()V

    .line 275
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->s:Lcom/google/android/apps/gmm/base/placelists/p;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/placelists/p;->a:Lcom/google/android/apps/gmm/place/l;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-eq v1, v2, :cond_1

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->v:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->t:Lcom/google/android/apps/gmm/base/placelists/o;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/google/android/apps/gmm/base/placelists/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/place/l;Lcom/google/android/apps/gmm/base/placelists/o;)V

    :cond_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->s:Lcom/google/android/apps/gmm/base/placelists/p;

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->x:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->y:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    .line 282
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->r:Lcom/google/android/apps/gmm/base/fragments/m;

    if-nez v0, :cond_4

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->w:Landroid/view/View;

    sget v3, Lcom/google/android/apps/gmm/g;->aM:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->o:Lcom/google/android/apps/gmm/place/m;

    iget-object v4, v0, Lcom/google/android/apps/gmm/place/m;->a:Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->s:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->y:Lcom/google/android/apps/gmm/base/l/aj;

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/base/fragments/m;->a(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/base/activities/c;Landroid/view/View;ILandroid/view/View;Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;Lcom/google/android/apps/gmm/base/l/aj;)Lcom/google/android/apps/gmm/base/fragments/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->r:Lcom/google/android/apps/gmm/base/fragments/m;

    .line 285
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->a()V

    .line 286
    return-void

    .line 277
    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 316
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 317
    const-string v0, "camera-moved"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkMapDetailsFragment;->B:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 318
    return-void
.end method
