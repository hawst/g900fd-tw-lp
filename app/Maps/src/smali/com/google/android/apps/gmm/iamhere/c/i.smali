.class final Lcom/google/android/apps/gmm/iamhere/c/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/c/h;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 662
    invoke-static {}, Lcom/google/android/apps/gmm/iamhere/c/r;->values()[Lcom/google/android/apps/gmm/iamhere/c/r;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v1, v0, v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v7

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringList(Ljava/util/List;)V

    invoke-static {v0}, Lcom/google/b/c/dn;->a(Ljava/util/Collection;)Lcom/google/b/c/dn;

    move-result-object v9

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v10

    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/h;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/iamhere/c/h;-><init>(Lcom/google/android/apps/gmm/iamhere/c/r;JJLjava/lang/Object;JLcom/google/b/c/dn;J)V

    return-object v0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 662
    new-array v0, p1, [Lcom/google/android/apps/gmm/iamhere/c/h;

    return-object v0
.end method
