.class public Lcom/google/android/apps/gmm/terms/TermsFragment;
.super Lcom/google/android/apps/gmm/base/fragments/OobFragment;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/apps/gmm/terms/b/d;


# static fields
.field static final b:J


# instance fields
.field a:Lcom/google/android/apps/gmm/terms/a/d;

.field c:Lcom/google/android/apps/gmm/shared/net/al;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 60
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x7

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/terms/TermsFragment;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;-><init>()V

    .line 52
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/terms/TermsFragment;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->A()Lcom/google/android/apps/gmm/t/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/t/a/a;->c()V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->a:Lcom/google/android/apps/gmm/terms/a/d;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/terms/a/d;->b:Z

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->a:Lcom/google/android/apps/gmm/terms/a/d;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 266
    return-void

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;I)I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    return v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public static a(Lcom/google/android/apps/gmm/shared/b/a;)Z
    .locals 3

    .prologue
    .line 269
    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->Z:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 270
    :cond_0
    const-string v1, "KR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static i()Lcom/google/android/apps/gmm/terms/TermsFragment;
    .locals 4

    .prologue
    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/terms/TermsFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/terms/TermsFragment;-><init>()V

    .line 76
    const/4 v1, 0x1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "exitsOnBackPressed"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->setArguments(Landroid/os/Bundle;)V

    .line 77
    return-object v0
.end method


# virtual methods
.method protected final a()Landroid/view/View;
    .locals 5

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/apps/gmm/base/f/ca;

    move-object v2, v0

    .line 111
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    const-class v0, Lcom/google/android/apps/gmm/base/f/bz;

    move-object v2, v0

    goto :goto_0

    .line 111
    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 112
    iget-object v1, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    .line 114
    new-instance v2, Lcom/google/android/apps/gmm/terms/d;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/terms/d;-><init>(Lcom/google/android/apps/gmm/terms/TermsFragment;Landroid/view/View;)V

    .line 121
    new-instance v3, Lcom/google/android/apps/gmm/terms/a/d;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-direct {v3, v4, p0, v0}, Lcom/google/android/apps/gmm/terms/a/d;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/terms/b/d;Lcom/google/android/apps/gmm/shared/b/a;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->a:Lcom/google/android/apps/gmm/terms/a/d;

    .line 124
    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 125
    return-object v1
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->ae:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->b(Lcom/google/android/apps/gmm/shared/b/c;I)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/r;->b()V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->i()V

    .line 97
    return-void
.end method

.method public final o()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 191
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    const/4 v0, 0x0

    .line 213
    :goto_0
    return v0

    .line 195
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 197
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/terms/TermsFragment;->a(Z)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/terms/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/terms/e;-><init>(Lcom/google/android/apps/gmm/terms/TermsFragment;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move v0, v1

    .line 213
    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    :goto_0
    return-void

    .line 182
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->a(Z)V

    goto :goto_0
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    packed-switch p2, :pswitch_data_0

    goto :goto_0

    .line 167
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    goto :goto_0

    .line 170
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->o()Z

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 148
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    if-nez p1, :cond_2

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->o()Z

    goto :goto_0

    .line 153
    :cond_2
    if-nez p1, :cond_0

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onCreate(Landroid/os/Bundle;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 103
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 141
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onPause()V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->c:Lcom/google/android/apps/gmm/shared/net/al;

    .line 143
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 130
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/OobFragment;->onResume()V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->c:Lcom/google/android/apps/gmm/shared/net/al;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    sget-wide v2, Lcom/google/android/apps/gmm/terms/TermsFragment;->b:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(J)Lcom/google/android/apps/gmm/shared/net/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->c:Lcom/google/android/apps/gmm/shared/net/al;

    .line 137
    return-void
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 219
    const/4 v0, 0x0

    .line 222
    :goto_0
    return v0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->finish()V

    .line 222
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/terms/TermsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x0

    .line 231
    :goto_0
    return v0

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/terms/TermsFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->H()Lcom/google/android/apps/gmm/settings/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/settings/a/a;->e()V

    .line 231
    const/4 v0, 0x1

    goto :goto_0
.end method
