.class public Lcom/google/android/apps/gmm/navigation/a/b/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/apps/gmm/navigation/a/b/a;

.field public final c:Lcom/google/android/apps/gmm/navigation/a/b/j;

.field public final d:Lcom/google/android/apps/gmm/map/r/a/am;


# direct methods
.method protected constructor <init>(Lcom/google/android/apps/gmm/navigation/a/b/j;Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)V
    .locals 6

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/b/h;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 57
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/b/h;->d:Lcom/google/android/apps/gmm/map/r/a/am;

    .line 59
    instance-of v0, p3, Landroid/text/Spanned;

    if-eqz v0, :cond_0

    .line 60
    check-cast p3, Landroid/text/Spanned;

    invoke-static {p3}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Landroid/text/Spanned;)Ljava/lang/String;

    move-result-object p3

    .line 70
    :goto_0
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    .line 76
    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/a/b/h;->b:Lcom/google/android/apps/gmm/navigation/a/b/a;

    .line 77
    return-void

    .line 61
    :cond_0
    instance-of v0, p3, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 62
    check-cast p3, Ljava/lang/String;

    goto :goto_0

    .line 64
    :cond_1
    invoke-interface {p3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 65
    const-string v1, "SpeechMessage"

    const-string v2, "Can\'t handle CharSequence of type %s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 66
    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    .line 65
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move-object p3, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->f:Lcom/google/android/apps/gmm/navigation/a/b/j;

    .line 82
    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/i;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    invoke-virtual {v2}, Lcom/google/maps/g/a/bx;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 96
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/navigation/a/b/h;

    invoke-direct {v1, v0, p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/a/b/h;-><init>(Lcom/google/android/apps/gmm/navigation/a/b/j;Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)V

    return-object v1

    .line 84
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->b:Lcom/google/android/apps/gmm/navigation/a/b/j;

    goto :goto_0

    .line 87
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    goto :goto_0

    .line 90
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->a:Lcom/google/android/apps/gmm/navigation/a/b/j;

    goto :goto_0

    .line 93
    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/navigation/a/b/j;->c:Lcom/google/android/apps/gmm/navigation/a/b/j;

    goto :goto_0

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;
    .locals 2

    .prologue
    .line 106
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/b/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/google/android/apps/gmm/navigation/a/b/h;-><init>(Lcom/google/android/apps/gmm/navigation/a/b/j;Lcom/google/android/apps/gmm/map/r/a/am;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)V

    return-object v0
.end method

.method private static a(Landroid/text/Spanned;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 131
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 133
    invoke-interface {p0}, Landroid/text/Spanned;->length()I

    move-result v1

    const-class v3, Lcom/google/android/apps/gmm/map/r/a/ai;

    invoke-virtual {v2, v0, v1, v3}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v3

    move v1, v0

    .line 134
    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 135
    aget-object v0, v3, v1

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 136
    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->getSpanStart(Ljava/lang/Object;)I

    move-result v4

    invoke-virtual {v2, v0}, Landroid/text/SpannableStringBuilder;->getSpanEnd(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v5, v0}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 134
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 148
    if-ne p1, p0, :cond_0

    .line 149
    const/4 v0, 0x1

    .line 155
    :goto_0
    return v0

    .line 151
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;

    if-eqz v0, :cond_1

    .line 152
    check-cast p1, Lcom/google/android/apps/gmm/navigation/a/b/h;

    .line 153
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 155
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/b/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
