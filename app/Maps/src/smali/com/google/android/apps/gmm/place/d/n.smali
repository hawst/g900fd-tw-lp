.class public Lcom/google/android/apps/gmm/place/d/n;
.super Lcom/google/android/apps/gmm/place/d/k;
.source "PG"


# instance fields
.field final a:Lcom/google/maps/g/fu;

.field final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/fu;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/d/k;-><init>()V

    .line 136
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/d/n;->a:Lcom/google/maps/g/fu;

    .line 137
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/d/n;->b:Landroid/content/Context;

    .line 138
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/n;->a:Lcom/google/maps/g/fu;

    iget v0, v0, Lcom/google/maps/g/fu;->d:I

    invoke-static {v0}, Lcom/google/maps/g/gb;->a(I)Lcom/google/maps/g/gb;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/gb;->a:Lcom/google/maps/g/gb;

    .line 143
    :cond_0
    sget-object v1, Lcom/google/maps/g/gb;->b:Lcom/google/maps/g/gb;

    if-ne v0, v1, :cond_2

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/n;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    iget-object v1, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lcom/google/maps/g/fc;->h()Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/n;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->jR:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 154
    :goto_1
    return-object v0

    .line 144
    :cond_1
    iget-object v0, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto :goto_0

    .line 148
    :cond_2
    sget-object v1, Lcom/google/maps/g/gb;->a:Lcom/google/maps/g/gb;

    if-ne v0, v1, :cond_4

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/d/n;->a:Lcom/google/maps/g/fu;

    iget-object v0, v0, Lcom/google/maps/g/fu;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fx;->h()Lcom/google/maps/g/fx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fx;

    iget-object v1, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    if-nez v1, :cond_3

    invoke-static {}, Lcom/google/maps/g/fc;->i()Lcom/google/maps/g/fc;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/google/maps/g/fc;->h()Ljava/lang/String;

    move-result-object v0

    .line 150
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 151
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/d/n;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->jP:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 149
    :cond_3
    iget-object v0, v0, Lcom/google/maps/g/fx;->b:Lcom/google/maps/g/fc;

    goto :goto_2

    .line 154
    :cond_4
    const-string v0, ""

    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 159
    sget v0, Lcom/google/android/apps/gmm/f;->dz:I

    return v0
.end method
