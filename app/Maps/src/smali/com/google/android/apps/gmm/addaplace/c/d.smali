.class Lcom/google/android/apps/gmm/addaplace/c/d;
.super Lcom/google/android/apps/gmm/base/l/d;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/addaplace/c/b;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/addaplace/c/b;)V
    .locals 3

    .prologue
    .line 337
    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    .line 338
    iget-object v0, p1, Lcom/google/android/apps/gmm/addaplace/c/b;->c:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->i:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 339
    iget-object v1, p1, Lcom/google/android/apps/gmm/addaplace/c/b;->c:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->lh:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 338
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/base/l/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/app/Activity;Ljava/lang/Runnable;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    .line 348
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 369
    :goto_0
    return-void

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 357
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    invoke-static {v1}, Lcom/google/android/apps/gmm/addaplace/c/b;->a(Lcom/google/android/apps/gmm/addaplace/c/b;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 359
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/c/b;->f:Landroid/app/ProgressDialog;

    if-nez v1, :cond_1

    .line 360
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    new-instance v2, Landroid/app/ProgressDialog;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, v1, Lcom/google/android/apps/gmm/addaplace/c/b;->f:Landroid/app/ProgressDialog;

    .line 361
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/c/b;->f:Landroid/app/ProgressDialog;

    sget v2, Lcom/google/android/apps/gmm/l;->mP:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 363
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/c/b;->f:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    .line 364
    iget-object v1, p0, Lcom/google/android/apps/gmm/addaplace/c/d;->a:Lcom/google/android/apps/gmm/addaplace/c/b;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/c/b;->e:Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/addaplace/fragments/AddAPlaceFragment;->b:Lcom/google/android/apps/gmm/addaplace/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/addaplace/a;->a(Lcom/google/android/apps/gmm/base/activities/c;)V

    goto :goto_0

    .line 366
    :cond_2
    sget v1, Lcom/google/android/apps/gmm/l;->i:I

    sget v2, Lcom/google/android/apps/gmm/l;->aM:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;II)V

    goto :goto_0
.end method
