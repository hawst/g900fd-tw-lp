.class public Lcom/google/android/apps/gmm/navigation/i/v;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/i/x;

.field final b:Lcom/google/android/apps/gmm/shared/net/a/n;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/a/n;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/v;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/x;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/navigation/i/x;-><init>(Lcom/google/android/apps/gmm/shared/net/a/n;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/v;->a:Lcom/google/android/apps/gmm/navigation/i/x;

    .line 48
    return-void
.end method


# virtual methods
.method final a(Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ">;)",
            "Lcom/google/android/apps/gmm/map/b/a/ae;"
        }
    .end annotation

    .prologue
    .line 177
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v1

    add-int/2addr v0, v1

    new-array v3, v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    .line 178
    const/4 v0, 0x0

    .line 179
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/e;

    .line 180
    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v0, v3, v1

    move v1, v2

    .line 181
    goto :goto_0

    .line 182
    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/e;

    .line 183
    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v0, v3, v1

    move v1, v2

    .line 184
    goto :goto_1

    .line 185
    :cond_1
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/ae;->b([Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v1

    .line 186
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v2

    .line 187
    double-to-int v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/v;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v2, v2, Lcom/google/r/b/a/qz;->b:I

    mul-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ae;->b(I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 188
    return-object v0
.end method
