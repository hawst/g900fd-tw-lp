.class public Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;


# static fields
.field static final a:J


# instance fields
.field final b:Lcom/google/android/apps/gmm/map/util/b/g;

.field final c:Landroid/location/LocationManager;

.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field e:Landroid/location/GpsStatus;

.field f:J

.field private final g:Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 29
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->a:J

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/location/LocationManager;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 2

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->e:Landroid/location/GpsStatus;

    .line 82
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->h:Z

    .line 84
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->f:J

    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;-><init>(Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->g:Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 92
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->c:Landroid/location/LocationManager;

    .line 93
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 94
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 7

    .prologue
    .line 98
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 116
    :goto_0
    monitor-exit p0

    return-void

    .line 103
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->c:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    const-string v1, "gps"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->c:Landroid/location/LocationManager;

    const-string v1, "gps"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->g:Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;

    .line 108
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 107
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->c:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    const-string v1, "network"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->c:Landroid/location/LocationManager;

    const-string v1, "network"

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->g:Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;

    .line 113
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v6

    .line 112
    invoke-virtual/range {v0 .. v6}, Landroid/location/LocationManager;->requestLocationUpdates(Ljava/lang/String;JFLandroid/location/LocationListener;Landroid/os/Looper;)V

    .line 115
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 128
    :goto_0
    monitor-exit p0

    return-void

    .line 125
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->c:Landroid/location/LocationManager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->g:Lcom/google/android/apps/gmm/map/location/rawlocationevents/b;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->removeUpdates(Landroid/location/LocationListener;)V

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/a;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x1

    return v0
.end method
