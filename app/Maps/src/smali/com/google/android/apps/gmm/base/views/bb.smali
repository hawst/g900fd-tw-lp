.class Lcom/google/android/apps/gmm/base/views/bb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/views/ScalebarView;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/views/ScalebarView;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/bb;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/j/ab;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 203
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/bb;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->a:Lcom/google/android/apps/gmm/map/j/ac;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/views/ScalebarView;->o:Z

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/base/views/ScalebarView;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/apps/gmm/base/views/ScalebarView;->i:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 204
    :cond_0
    :goto_1
    return-void

    .line 203
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->b:Lcom/google/android/apps/gmm/map/j/ac;

    if-eq v0, v2, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->c:Lcom/google/android/apps/gmm/map/j/ac;

    if-ne v0, v2, :cond_0

    :cond_3
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->a()V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/o/a/e;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/bb;->a:Lcom/google/android/apps/gmm/base/views/ScalebarView;

    sget-object v1, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    iget-object v2, p1, Lcom/google/android/apps/gmm/o/a/e;->a:Ljava/util/EnumSet;

    invoke-virtual {v2, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aK:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aK:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aI:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aI:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->invalidate()V

    .line 196
    return-void

    .line 195
    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aL:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->h:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aL:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aJ:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/ScalebarView;->g:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/ScalebarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/d;->aJ:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0
.end method
