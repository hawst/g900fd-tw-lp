.class public final enum Lcom/google/android/apps/gmm/shared/net/l;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/shared/net/l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum b:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum c:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum d:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum e:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum f:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum g:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum h:Lcom/google/android/apps/gmm/shared/net/l;

.field public static final enum i:Lcom/google/android/apps/gmm/shared/net/l;

.field private static final synthetic j:[Lcom/google/android/apps/gmm/shared/net/l;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "INITIALIZED"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->a:Lcom/google/android/apps/gmm/shared/net/l;

    .line 54
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "QUEUED"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->b:Lcom/google/android/apps/gmm/shared/net/l;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "QUEUE_FAIL"

    invoke-direct {v0, v1, v5}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->c:Lcom/google/android/apps/gmm/shared/net/l;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "SENT"

    invoke-direct {v0, v1, v6}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->d:Lcom/google/android/apps/gmm/shared/net/l;

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "RECEIVED"

    invoke-direct {v0, v1, v7}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->e:Lcom/google/android/apps/gmm/shared/net/l;

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "RESPONSE_PROCESSING"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->f:Lcom/google/android/apps/gmm/shared/net/l;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "COMPLETED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->g:Lcom/google/android/apps/gmm/shared/net/l;

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "FAILED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->h:Lcom/google/android/apps/gmm/shared/net/l;

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/l;

    const-string v1, "CANCELED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/net/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->i:Lcom/google/android/apps/gmm/shared/net/l;

    .line 52
    const/16 v0, 0x9

    new-array v0, v0, [Lcom/google/android/apps/gmm/shared/net/l;

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/l;->a:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/l;->b:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/l;->c:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/l;->d:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/apps/gmm/shared/net/l;->e:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/l;->f:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/l;->g:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/l;->h:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/shared/net/l;->i:Lcom/google/android/apps/gmm/shared/net/l;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/shared/net/l;->j:[Lcom/google/android/apps/gmm/shared/net/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/shared/net/l;
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/android/apps/gmm/shared/net/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/l;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/shared/net/l;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/apps/gmm/shared/net/l;->j:[Lcom/google/android/apps/gmm/shared/net/l;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/shared/net/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/shared/net/l;

    return-object v0
.end method
