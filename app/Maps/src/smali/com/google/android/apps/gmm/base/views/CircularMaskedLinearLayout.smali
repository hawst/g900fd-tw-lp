.class public Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static final c:Landroid/graphics/Paint;

.field private static d:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Landroid/graphics/Bitmap;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:I

.field private e:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    sput-object v0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->c:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 62
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->setLayerType(ILandroid/graphics/Paint;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 65
    sget-object v1, Lcom/google/android/apps/gmm/n;->ac:[I

    invoke-virtual {v0, p2, v1, p3, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 66
    sget v1, Lcom/google/android/apps/gmm/n;->aj:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->b:I

    .line 67
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 68
    return-void
.end method

.method public static a(III)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    .line 113
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 114
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 115
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    .line 116
    invoke-virtual {v2, p0}, Landroid/graphics/Paint;->setColor(I)V

    .line 117
    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 118
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 119
    int-to-float v3, p2

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 120
    div-int/lit8 v3, p1, 0x2

    int-to-float v3, v3

    div-int/lit8 v4, p1, 0x2

    int-to-float v4, v4

    sub-int v5, p1, p2

    div-int/lit8 v5, v5, 0x2

    int-to-double v6, v5

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v6, v8

    double-to-float v5, v6

    invoke-virtual {v1, v3, v4, v5, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 122
    return-object v0
.end method


# virtual methods
.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 94
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->e:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 97
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getHeight()I

    move-result v1

    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingRight()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v1, v5

    invoke-direct {v2, v3, v4, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->e:Landroid/graphics/Rect;

    .line 99
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->aB:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget-object v1, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->d:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/ref/WeakReference;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v0, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/graphics/Paint;-><init>(I)V

    const/16 v4, 0xff

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/graphics/RectF;

    int-to-float v6, v0

    int-to-float v0, v0

    invoke-direct {v5, v7, v7, v6, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v4, v5, v3}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-direct {v1, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v1, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->d:Ljava/lang/ref/WeakReference;

    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->e:Landroid/graphics/Rect;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 100
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->b:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->e:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {p1, v0, v8, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 103
    :cond_3
    return-void
.end method

.method public onSizeChanged(IIII)V
    .locals 5

    .prologue
    .line 89
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingRight()I

    move-result v3

    sub-int v3, p1, v3

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, p2, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/CircularMaskedLinearLayout;->e:Landroid/graphics/Rect;

    .line 90
    return-void
.end method
