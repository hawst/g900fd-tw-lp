.class Lcom/google/android/apps/gmm/map/o/av;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# static fields
.field private static final b:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field volatile a:Z

.field private final c:Lcom/google/android/apps/gmm/map/o/bc;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/o/ax;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/map/f/o;

.field private final g:Lcom/google/android/apps/gmm/map/o/au;

.field private h:Z

.field private final i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/o/d;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/android/apps/gmm/v/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/apps/gmm/map/o/aw;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/aw;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/av;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/v/ad;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->d:Ljava/util/List;

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    .line 65
    sget-object v0, Lcom/google/android/apps/gmm/map/o/av;->b:Ljava/util/Comparator;

    new-instance v1, Ljava/util/TreeSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/util/Comparator;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/av;->i:Ljava/util/Set;

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/av;->f:Lcom/google/android/apps/gmm/map/f/o;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/map/o/au;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/o/au;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->g:Lcom/google/android/apps/gmm/map/o/au;

    .line 76
    new-instance v0, Lcom/google/android/apps/gmm/map/o/bc;

    invoke-direct {v0, p2}, Lcom/google/android/apps/gmm/map/o/bc;-><init>(Lcom/google/android/apps/gmm/v/ad;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->c:Lcom/google/android/apps/gmm/map/o/bc;

    .line 77
    return-void
.end method

.method private declared-synchronized f()V
    .locals 8

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/av;->d:Ljava/util/List;

    .line 105
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 106
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 107
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/ax;

    .line 108
    iget-object v6, v0, Lcom/google/android/apps/gmm/map/o/ax;->a:Lcom/google/android/apps/gmm/map/o/d;

    .line 109
    iget v7, v0, Lcom/google/android/apps/gmm/map/o/ax;->b:I

    packed-switch v7, :pswitch_data_0

    .line 123
    new-instance v1, Ljava/lang/IllegalStateException;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/ax;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x27

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown labeling operation: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 111
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->i:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    const/16 v0, 0x8

    invoke-interface {v6, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->i:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 115
    :cond_0
    const v0, 0x3b5a740e

    invoke-interface {v6, v4, v5, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(JF)V

    .line 106
    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 118
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->i:Ljava/util/Set;

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 119
    const v0, -0x44a58bf2

    invoke-interface {v6, v4, v5, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(JF)V

    goto :goto_1

    .line 127
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 128
    const/16 v3, 0x10

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    goto :goto_2

    .line 130
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 131
    invoke-interface {v2}, Ljava/util/List;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 132
    monitor-exit p0

    return-void

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 3

    .prologue
    .line 176
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->d:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/map/o/ax;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/gmm/map/o/ax;-><init>(Lcom/google/android/apps/gmm/map/o/d;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    monitor-exit p0

    return-void

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 2

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/av;->j:Lcom/google/android/apps/gmm/v/g;

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/av;->d()V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->j:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->j:Lcom/google/android/apps/gmm/v/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/av;->f:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 84
    :cond_0
    return-void
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/o/d;)V
    .locals 3

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/o/d;->a(I)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->d:Ljava/util/List;

    new-instance v1, Lcom/google/android/apps/gmm/map/o/ax;

    const/4 v2, 0x2

    invoke-direct {v1, p1, v2}, Lcom/google/android/apps/gmm/map/o/ax;-><init>(Lcom/google/android/apps/gmm/map/o/d;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    monitor-exit p0

    return-void

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 12

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 136
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/o/av;->f()V

    .line 138
    invoke-static {}, Landroid/view/animation/AnimationUtils;->currentAnimationTimeMillis()J

    move-result-wide v4

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v7

    .line 143
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 144
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 146
    invoke-interface {v0, v4, v5}, Lcom/google/android/apps/gmm/map/o/d;->a(J)Lcom/google/android/apps/gmm/map/o/e;

    move-result-object v2

    .line 147
    sget-object v8, Lcom/google/android/apps/gmm/map/o/e;->e:Lcom/google/android/apps/gmm/map/o/e;

    if-ne v2, v8, :cond_0

    .line 149
    const/16 v2, 0x8

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    .line 150
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 155
    :cond_0
    sget-object v8, Lcom/google/android/apps/gmm/map/o/e;->b:Lcom/google/android/apps/gmm/map/o/e;

    if-eq v2, v8, :cond_1

    sget-object v8, Lcom/google/android/apps/gmm/map/o/e;->c:Lcom/google/android/apps/gmm/map/o/e;

    if-ne v2, v8, :cond_3

    :cond_1
    move v2, v3

    .line 157
    :goto_1
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/o/av;->g:Lcom/google/android/apps/gmm/map/o/au;

    iget-object v9, p0, Lcom/google/android/apps/gmm/map/o/av;->f:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/o/av;->c:Lcom/google/android/apps/gmm/map/o/bc;

    iget-boolean v11, p0, Lcom/google/android/apps/gmm/map/o/av;->a:Z

    invoke-interface {v0, v8, v9, v10, v11}, Lcom/google/android/apps/gmm/map/o/d;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/bc;Z)Z

    move-result v0

    .line 160
    if-nez v2, :cond_2

    if-nez v0, :cond_4

    :cond_2
    move v0, v3

    :goto_2
    or-int/2addr v0, v1

    move v1, v0

    .line 161
    goto :goto_0

    :cond_3
    move v2, v7

    .line 155
    goto :goto_1

    :cond_4
    move v0, v7

    .line 160
    goto :goto_2

    .line 163
    :cond_5
    monitor-enter p0

    .line 164
    if-eqz v1, :cond_7

    .line 165
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/av;->d()V

    .line 169
    :goto_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    iget-object v9, p0, Lcom/google/android/apps/gmm/map/o/av;->c:Lcom/google/android/apps/gmm/map/o/bc;

    move v8, v7

    :goto_4
    sget v0, Lcom/google/android/apps/gmm/map/o/bc;->b:I

    if-ge v8, v0, :cond_c

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/o/bc;->c:[Lcom/google/android/apps/gmm/map/o/bd;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/bd;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/o/bd;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/map/o/be;

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/o/be;->a:Lcom/google/android/apps/gmm/map/o/bg;

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    if-lez v0, :cond_8

    iget-object v11, v6, Lcom/google/android/apps/gmm/map/o/be;->c:Lcom/google/android/apps/gmm/map/t/p;

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/o/be;->a:Lcom/google/android/apps/gmm/map/o/bg;

    new-instance v0, Lcom/google/android/apps/gmm/v/av;

    iget v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    iget v3, v1, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    const/16 v4, 0x11

    const/4 v5, 0x4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/av;-><init>(Lcom/google/android/apps/gmm/v/aw;IIII)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/o/bg;->d:Ljava/nio/ByteBuffer;

    iput-object v3, v1, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/o/bg;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/apps/gmm/map/o/bg;->b:Ljava/nio/FloatBuffer;

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->d:Ljava/nio/ByteBuffer;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/o/bg;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iput v7, v1, Lcom/google/android/apps/gmm/map/o/bg;->e:I

    iput v7, v1, Lcom/google/android/apps/gmm/map/o/bg;->f:I

    invoke-virtual {v11, v0}, Lcom/google/android/apps/gmm/map/t/p;->a(Lcom/google/android/apps/gmm/v/co;)V

    iget-object v0, v6, Lcom/google/android/apps/gmm/map/o/be;->c:Lcom/google/android/apps/gmm/map/t/p;

    const/16 v1, 0xff

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v2, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_6
    int-to-byte v1, v1

    iput-byte v1, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    goto :goto_5

    .line 167
    :cond_7
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/av;->h:Z

    goto :goto_3

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 171
    :cond_8
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/o/be;->b:Lcom/google/android/apps/gmm/v/bg;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/v/bg;->a:Z

    if-eqz v0, :cond_9

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/o/bc;->d:Lcom/google/android/apps/gmm/v/ad;

    iget-object v1, v6, Lcom/google/android/apps/gmm/map/o/be;->c:Lcom/google/android/apps/gmm/map/t/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    invoke-direct {v2, v1, v7}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/aa;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    goto :goto_5

    :cond_9
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/o/be;->c:Lcom/google/android/apps/gmm/map/t/p;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v1, :cond_a

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_a
    int-to-byte v1, v7

    iput-byte v1, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    goto :goto_5

    :cond_b
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_4

    .line 172
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->j:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->j:Lcom/google/android/apps/gmm/v/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/av;->f:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 173
    :cond_d
    return-void
.end method

.method public final declared-synchronized c()V
    .locals 3

    .prologue
    .line 204
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 205
    const/16 v2, 0x10

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 208
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/o/d;

    .line 209
    const/16 v2, 0x8

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/o/d;->b(I)V

    goto :goto_1

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/av;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 215
    monitor-exit p0

    return-void
.end method

.method d()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->j:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 219
    monitor-enter p0

    .line 220
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/av;->h:Z

    .line 221
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/av;->j:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 224
    :cond_0
    return-void

    .line 221
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    .prologue
    .line 238
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/av;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
