.class public Lcom/google/android/apps/gmm/place/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Lcom/google/android/apps/gmm/base/l/a/q;

.field private final c:Lcom/google/android/apps/gmm/place/PlacePageView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/l/a/q;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/PlacePageView;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/r;->b:Lcom/google/android/apps/gmm/base/l/a/q;

    .line 28
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 29
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/r;->c:Lcom/google/android/apps/gmm/place/PlacePageView;

    .line 30
    return-void
.end method

.method private static b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)F
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->j:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    move-result-object v1

    .line 92
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->m:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v1

    aget v1, v2, v1

    sub-int/2addr v1, v0

    .line 93
    int-to-float v0, v0

    int-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    .line 99
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->h:I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 100
    int-to-float v1, v1

    sub-float v0, v1, v0

    return v0
.end method

.method private d()F
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    const/high16 v0, 0x40a00000    # 5.0f

    .line 44
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/r;->c:Lcom/google/android/apps/gmm/place/PlacePageView;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v0, v1, v0

    return v0

    .line 43
    :cond_0
    const/high16 v0, 0x40000000    # 2.0f

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->b:Lcom/google/android/apps/gmm/base/l/a/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/q;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->b:Lcom/google/android/apps/gmm/base/l/a/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/a/q;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->c:Lcom/google/android/apps/gmm/place/PlacePageView;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    instance-of v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 38
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/place/r;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V

    .line 40
    :cond_0
    return-void

    .line 36
    :cond_1
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 178
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)V
    .locals 6

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/r;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/place/r;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)F

    move-result v0

    neg-float v0, v0

    .line 72
    :goto_0
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/place/r;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)F

    move-result v3

    add-float/2addr v3, v0

    neg-float v3, v3

    .line 73
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/r;->e()Z

    move-result v4

    if-eqz v4, :cond_1

    move v1, v2

    .line 75
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/r;->b:Lcom/google/android/apps/gmm/base/l/a/q;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/r;->d()F

    move-result v4

    div-float/2addr v0, v4

    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/base/l/a/q;->c(F)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->b:Lcom/google/android/apps/gmm/base/l/a/q;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/base/l/a/q;->a(F)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->b:Lcom/google/android/apps/gmm/base/l/a/q;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/l/a/q;->b(F)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->b:Lcom/google/android/apps/gmm/base/l/a/q;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 80
    return-void

    .line 70
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/place/s;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Can\'t handle state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    move v0, v1

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/base/h/a;->c:Landroid/view/animation/Interpolator;

    invoke-interface {v0, p3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    neg-float v0, v0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/r;->d()F

    move-result v3

    mul-float/2addr v0, v3

    goto :goto_0

    :pswitch_3
    invoke-static {p1, p2, p3}, Lcom/google/android/apps/gmm/place/r;->b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;F)F

    move-result v0

    neg-float v0, v0

    goto :goto_0

    :pswitch_4
    move v0, v1

    goto :goto_0

    .line 73
    :cond_1
    sget-object v4, Lcom/google/android/apps/gmm/place/s;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_1

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Can\'t handle state: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_5
    sget-object v1, Lcom/google/android/apps/gmm/base/h/a;->b:Landroid/view/animation/Interpolator;

    invoke-interface {v1, p3}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    goto/16 :goto_1

    :pswitch_6
    move v1, v2

    goto/16 :goto_1

    .line 70
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 73
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method public final a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/views/expandingscrollview/e;)V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->c(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/h/f;

    move-result-object v1

    .line 55
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/map/h/f;->e:Z

    if-eqz v0, :cond_1

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/map/h/f;->f:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 56
    :goto_0
    if-nez v0, :cond_0

    .line 58
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne p3, v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->f:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    .line 60
    :goto_1
    invoke-static {p3, v1}, Lcom/google/android/apps/gmm/base/i/f;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/map/h/f;)Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    move-result-object v1

    .line 61
    invoke-virtual {p1, v0, v1}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->setExpandingStateTransition(Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;)V

    .line 63
    :cond_0
    return-void

    .line 55
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 58
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/l;

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/r;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a(Lcom/google/android/apps/gmm/base/views/expandingscrollview/j;)V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/r;->a()V

    .line 188
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V
    .locals 0

    .prologue
    .line 182
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/r;->a()V

    .line 208
    return-void
.end method
