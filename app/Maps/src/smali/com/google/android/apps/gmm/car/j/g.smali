.class Lcom/google/android/apps/gmm/car/j/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/j/e;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/j/e;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/j/g;->a:Lcom/google/android/apps/gmm/car/j/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 155
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/j/g;->a:Lcom/google/android/apps/gmm/car/j/e;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/j/h;->g:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->d:Landroid/view/ViewGroup;

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/car/j/x;->a(Landroid/view/View;I)Z

    :cond_0
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->i:Lcom/google/android/apps/gmm/car/j/l;

    if-eqz v0, :cond_3

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->i:Lcom/google/android/apps/gmm/car/j/l;

    move-object v1, v0

    :goto_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    iget v4, v1, Lcom/google/android/apps/gmm/car/j/l;->b:I

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/j/m;

    sget-object v4, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1e

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "onTick: doing sequence="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ", step="

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v3, Lcom/google/android/apps/gmm/car/j/e;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v5, v3, Lcom/google/android/apps/gmm/car/j/e;->d:Landroid/view/ViewGroup;

    invoke-interface {v0, v4, v5}, Lcom/google/android/apps/gmm/car/j/m;->a(Lcom/google/android/apps/gmm/car/ad;Landroid/view/View;)Lcom/google/android/apps/gmm/car/j/n;

    move-result-object v0

    sget-object v4, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xf

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "onTick: result="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v4, Lcom/google/android/apps/gmm/car/j/n;->b:Lcom/google/android/apps/gmm/car/j/n;

    if-ne v0, v4, :cond_5

    iget v0, v1, Lcom/google/android/apps/gmm/car/j/l;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/android/apps/gmm/car/j/l;->b:I

    iget v0, v1, Lcom/google/android/apps/gmm/car/j/l;->b:I

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/j/l;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->i:Lcom/google/android/apps/gmm/car/j/l;

    iget v0, v3, Lcom/google/android/apps/gmm/car/j/e;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/android/apps/gmm/car/j/e;->j:I

    iget v0, v3, Lcom/google/android/apps/gmm/car/j/e;->j:I

    iget-object v1, v3, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    iput v2, v3, Lcom/google/android/apps/gmm/car/j/e;->j:I

    :cond_1
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    iget v1, v3, Lcom/google/android/apps/gmm/car/j/e;->j:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/j/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/j/l;->a()V

    .line 156
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/j/g;->a:Lcom/google/android/apps/gmm/car/j/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/e;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/j/g;->a:Lcom/google/android/apps/gmm/car/j/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/j/e;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 157
    return-void

    .line 155
    :cond_3
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/h;->b:Ljava/util/ArrayList;

    iget v1, v3, Lcom/google/android/apps/gmm/car/j/e;->j:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/j/l;

    move-object v1, v0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    sget-object v1, Lcom/google/android/apps/gmm/car/j/n;->c:Lcom/google/android/apps/gmm/car/j/n;

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/car/j/e;->b:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->i:Lcom/google/android/apps/gmm/car/j/l;

    if-eqz v0, :cond_6

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "ERROR: step failed in failure recovery sequence."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->e:Lcom/google/android/apps/gmm/car/j/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/h;->d:Lcom/google/android/apps/gmm/car/j/l;

    iput-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->i:Lcom/google/android/apps/gmm/car/j/l;

    iget-object v0, v3, Lcom/google/android/apps/gmm/car/j/e;->i:Lcom/google/android/apps/gmm/car/j/l;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/j/l;->a()V

    goto :goto_2
.end method
