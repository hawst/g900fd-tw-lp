.class public Lcom/google/android/apps/gmm/map/internal/c/bi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final g:[Lcom/google/android/apps/gmm/map/internal/c/be;

.field private static final h:[B

.field private static final j:[Lcom/google/android/apps/gmm/map/internal/c/be;

.field private static final k:Lcom/google/android/apps/gmm/map/internal/c/bi;


# instance fields
.field final a:Ljava/lang/String;

.field public final b:[Lcom/google/android/apps/gmm/map/internal/c/be;

.field public final c:[B

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;

.field public f:[F

.field private i:F


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 24
    new-array v0, v4, [Lcom/google/android/apps/gmm/map/internal/c/be;

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->g:[Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 25
    new-array v0, v4, [B

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->h:[B

    .line 76
    new-array v0, v3, [Lcom/google/android/apps/gmm/map/internal/c/be;

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v1

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->j:[Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 77
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bi;

    const-string v1, ""

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->j:[Lcom/google/android/apps/gmm/map/internal/c/be;

    new-array v3, v3, [B

    aput-byte v4, v3, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/bi;-><init>(Ljava/lang/String;[Lcom/google/android/apps/gmm/map/internal/c/be;[B)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->k:Lcom/google/android/apps/gmm/map/internal/c/bi;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Lcom/google/android/apps/gmm/map/internal/c/be;[B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->i:F

    .line 65
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->e:Ljava/lang/Boolean;

    .line 71
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->f:[F

    .line 85
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    .line 86
    if-nez p2, :cond_0

    sget-object p2, Lcom/google/android/apps/gmm/map/internal/c/bi;->g:[Lcom/google/android/apps/gmm/map/internal/c/be;

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 87
    if-nez p3, :cond_1

    sget-object p3, Lcom/google/android/apps/gmm/map/internal/c/bi;->h:[B

    :cond_1
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    .line 88
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/map/internal/c/bi;
    .locals 1

    .prologue
    .line 179
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->k:Lcom/google/android/apps/gmm/map/internal/c/bi;

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;[B)Lcom/google/android/apps/gmm/map/internal/c/bi;
    .locals 5

    .prologue
    .line 134
    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v1

    .line 135
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    .line 136
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/gmm/map/internal/c/be;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    if-gez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    :goto_0
    aput-object v0, v2, v3

    .line 137
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bi;

    invoke-direct {v0, v1, v2, p2}, Lcom/google/android/apps/gmm/map/internal/c/bi;-><init>(Ljava/lang/String;[Lcom/google/android/apps/gmm/map/internal/c/be;[B)V

    return-object v0

    .line 136
    :cond_1
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/be;

    goto :goto_0
.end method

.method public static b(Ljava/io/DataInput;Lcom/google/android/apps/gmm/map/internal/c/bl;[B)Lcom/google/android/apps/gmm/map/internal/c/bi;
    .locals 5

    .prologue
    .line 152
    invoke-interface {p0}, Ljava/io/DataInput;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object v2

    .line 153
    array-length v0, p2

    new-array v3, v0, [Lcom/google/android/apps/gmm/map/internal/c/be;

    .line 154
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p2

    if-ge v1, v0, :cond_2

    .line 155
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v0

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    if-gez v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v0

    :goto_1
    aput-object v0, v3, v1

    .line 154
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 155
    :cond_1
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bl;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/be;

    goto :goto_1

    .line 157
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bi;

    invoke-direct {v0, v2, v3, p2}, Lcom/google/android/apps/gmm/map/internal/c/bi;-><init>(Ljava/lang/String;[Lcom/google/android/apps/gmm/map/internal/c/be;[B)V

    return-object v0
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/apps/gmm/map/internal/c/bi;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 302
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x2

    move v3, v0

    :goto_0
    if-ltz v3, :cond_7

    .line 303
    if-eqz p1, :cond_6

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v0, v0, v3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_1

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    add-int/lit8 v4, v3, 0x1

    aget-object v2, v2, v4

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v4, :cond_2

    move v2, v1

    :goto_2
    if-ge v0, v2, :cond_3

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v2, v2, v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    add-int/lit8 v5, v3, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/internal/c/be;->a(Lcom/google/android/apps/gmm/map/internal/c/be;)Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v2

    aput-object v2, v0, v3

    .line 302
    :cond_0
    :goto_3
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    .line 309
    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_1

    :cond_2
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto :goto_2

    .line 311
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v0, v0, v3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_4

    move v0, v1

    :goto_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    add-int/lit8 v4, v3, 0x1

    aget-object v2, v2, v4

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v4, :cond_5

    move v2, v1

    :goto_5
    if-le v0, v2, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    add-int/lit8 v2, v3, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    add-int/lit8 v5, v3, 0x1

    aget-object v4, v4, v5

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v5, v5, v3

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/map/internal/c/be;->a(Lcom/google/android/apps/gmm/map/internal/c/be;)Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    aput-object v4, v0, v2

    goto :goto_3

    .line 311
    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    goto :goto_4

    :cond_5
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto :goto_5

    .line 316
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v2, v2, v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    add-int/lit8 v5, v3, 0x1

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/internal/c/be;->a(Lcom/google/android/apps/gmm/map/internal/c/be;)Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v2

    aput-object v2, v0, v3

    goto :goto_3

    .line 319
    :cond_7
    return-object p0
.end method

.method public final b()F
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 197
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->i:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v1, v1, v0

    int-to-float v1, v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v1

    .line 201
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 202
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v2, v2, v0

    int-to-float v2, v2

    .line 203
    invoke-static {v2}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v2

    div-float/2addr v2, v1

    .line 204
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v3, v3, v0

    .line 205
    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->i:F

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/be;->c()F

    move-result v3

    mul-float/2addr v2, v3

    invoke-static {v4, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->i:F

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 208
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->i:F

    return v0
.end method

.method public final c()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v1

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v0, v0, v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v2, :cond_2

    move v3, v1

    .line 337
    :goto_1
    if-eqz v3, :cond_0

    move v0, v1

    .line 341
    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v2, v2, v0

    .line 342
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    if-nez v4, :cond_3

    move v2, v1

    :goto_3
    if-ne v2, v3, :cond_4

    .line 343
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 336
    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v0, v0

    move v3, v0

    goto :goto_1

    .line 342
    :cond_3
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->g:[Lcom/google/android/apps/gmm/map/internal/c/bd;

    array-length v2, v2

    goto :goto_3

    :cond_4
    move v1, v0

    .line 345
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 349
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 350
    const-string v0, "StyleEntry {"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 351
    const-string v0, "globalStyleId="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 353
    const-string v2, "  z"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 355
    :cond_0
    const-string v0, "}\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
