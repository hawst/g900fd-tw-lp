.class public Lcom/google/android/apps/gmm/map/legacy/a/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/util/a/m;


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/internal/c/bp;


# instance fields
.field final b:Lcom/google/android/apps/gmm/shared/c/f;

.field c:I

.field d:I

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/legacy/a/a/e;",
            "Lcom/google/android/apps/gmm/map/legacy/a/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private final f:I

.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/4 v1, -0x1

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/shared/c/f;I)V
    .locals 2

    .prologue
    .line 140
    mul-int/lit8 v0, p3, 0x5

    div-int/lit8 v0, v0, 0x2

    const/16 v1, 0xc0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/map/util/c;->i:I

    if-lez v1, :cond_0

    sget v0, Lcom/google/android/apps/gmm/map/util/c;->i:I

    :cond_0
    div-int/lit8 v0, v0, 0x6

    add-int/lit8 v0, v0, -0x6

    shl-int/lit8 v0, v0, 0xa

    shl-int/lit8 v0, v0, 0xa

    .line 141
    shl-int/lit8 v1, p3, 0xa

    shl-int/lit8 v1, v1, 0xa

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x10

    .line 140
    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/shared/c/f;II)V

    .line 142
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/shared/c/f;II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    .line 82
    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    .line 88
    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    .line 148
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->b:Lcom/google/android/apps/gmm/shared/c/f;

    .line 149
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->f:I

    .line 150
    iput p4, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->g:I

    .line 151
    const-string v0, "GLTileCacheManager"

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/util/a/b;->a:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private declared-synchronized a()Ljava/lang/String;
    .locals 5

    .prologue
    .line 461
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    .line 463
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 462
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 464
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 465
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 466
    const-string v1, " + "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->e()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    .line 469
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 470
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 461
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 473
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_3

    .line 474
    const-string v0, "no"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 476
    :cond_3
    const-string v0, " tiles use "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->f:I

    .line 478
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "M GL, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    .line 479
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->g:I

    .line 480
    invoke-static {v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "M J+N"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 481
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 486
    mul-int/lit8 v0, p0, 0xa

    const/high16 v1, 0x100000

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/s;->b(II)I

    move-result v0

    .line 487
    div-int/lit8 v1, v0, 0xa

    rem-int/lit8 v0, v0, 0xa

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x17

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized a(II)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 274
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    if-gt v0, p1, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt v0, p2, :cond_1

    .line 319
    :cond_0
    monitor-exit p0

    return-void

    .line 280
    :cond_1
    :try_start_1
    new-instance v4, Ljava/util/TreeSet;

    invoke-direct {v4}, Ljava/util/TreeSet;-><init>()V

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 284
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    new-instance v2, Lcom/google/android/apps/gmm/map/util/a/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-direct {v2, v1}, Lcom/google/android/apps/gmm/map/util/a/f;-><init>(La/a/a/a/d/am;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/a/f;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/a/f;->a()Lcom/google/android/apps/gmm/map/util/a/g;

    move-result-object v1

    move-object v2, v1

    .line 285
    :goto_1
    if-eqz v2, :cond_2

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eq v1, v6, :cond_2

    .line 286
    new-instance v6, Lcom/google/android/apps/gmm/map/legacy/a/a/c;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/util/a/g;->b:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    invoke-direct {v6, v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/a/a/c;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/a/b;)V

    invoke-interface {v4, v6}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v2, v3

    .line 284
    goto :goto_1

    .line 291
    :cond_4
    :try_start_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 292
    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/SortedSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    if-gt v0, p1, :cond_6

    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    if-le v0, p2, :cond_9

    .line 297
    :cond_6
    invoke-interface {v4}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/c;

    .line 298
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/c;->a:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    .line 299
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/c;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->g()Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 301
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/c;->a:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 306
    :cond_7
    invoke-interface {v4, v0}, Ljava/util/SortedSet;->remove(Ljava/lang/Object;)Z

    .line 307
    new-instance v5, Lcom/google/android/apps/gmm/map/util/a/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/a/e;->c:La/a/a/a/d/am;

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/map/util/a/f;-><init>(La/a/a/a/d/am;)V

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/util/a/f;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/util/a/f;->a()Lcom/google/android/apps/gmm/map/util/a/g;

    move-result-object v1

    .line 308
    :goto_3
    if-eqz v1, :cond_5

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    if-eq v5, v6, :cond_5

    .line 309
    new-instance v5, Lcom/google/android/apps/gmm/map/legacy/a/a/c;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/c;->a:Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/util/a/g;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/util/a/g;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    invoke-direct {v5, v6, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/c;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/a/b;)V

    invoke-interface {v4, v5}, Ljava/util/SortedSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    move-object v1, v3

    .line 307
    goto :goto_3

    .line 315
    :cond_9
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_4
    if-ge v1, v3, :cond_0

    .line 316
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/e;

    .line 317
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 315
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V
    .locals 9

    .prologue
    .line 435
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    move-object v8, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 436
    if-nez v8, :cond_0

    .line 440
    :goto_0
    monitor-exit p0

    return-void

    .line 439
    :cond_0
    :try_start_1
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    move-object v3, p2

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/map/legacy/a/a/b;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;IIJ)V

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 435
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method private declared-synchronized b(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/a/b;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 340
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    .line 341
    if-nez v0, :cond_3

    .line 342
    if-eqz p3, :cond_0

    .line 343
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/a/a;)V

    .line 344
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v7, v0

    .line 350
    :goto_0
    invoke-virtual {v7, p2}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    .line 351
    if-eqz v0, :cond_1

    .line 352
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    :goto_1
    monitor-exit p0

    return-object v0

    :cond_0
    move-object v0, v6

    .line 346
    goto :goto_1

    .line 356
    :cond_1
    if-eqz p3, :cond_2

    .line 359
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/a/b;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;IIJ)V

    invoke-virtual {v7, p2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    move-object v0, v6

    .line 361
    goto :goto_1

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v7, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(F)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 444
    const-string v0, "GLTileCacheManager"

    .line 445
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "trimToFractionOfMeasuredSize("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/Object;

    .line 444
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 446
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(II)V

    .line 447
    return v4
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    .locals 1

    .prologue
    .line 330
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->b(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    move-result-object v0

    .line 332
    if-eqz v0, :cond_0

    .line 333
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V
    .locals 1

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    .line 160
    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->d()V

    .line 164
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c()V

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 169
    :cond_0
    monitor-exit p0

    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V
    .locals 3

    .prologue
    .line 231
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    .line 232
    if-nez v0, :cond_2

    .line 233
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/a/a;)V

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 237
    :goto_0
    invoke-virtual {v1, p2}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    .line 238
    if-nez v0, :cond_0

    .line 241
    invoke-direct {p0, p1, p3}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(Lcom/google/android/apps/gmm/map/legacy/a/a/e;Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :goto_1
    monitor-exit p0

    return-void

    .line 248
    :cond_0
    :try_start_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    if-eqz v2, :cond_1

    .line 249
    new-instance v2, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/b;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/a/b;)V

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    :cond_1
    iput-object p3, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->a:Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;

    .line 255
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->i()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->b:I

    .line 256
    invoke-interface {p3}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;->j()I

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->c:I

    .line 257
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    iget v2, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->b:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->c:I

    .line 258
    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->c:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->d:I

    .line 262
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->f:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->g:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V
    .locals 1

    .prologue
    .line 175
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    .line 176
    if-eqz v0, :cond_0

    .line 178
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a()V

    .line 180
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :cond_0
    monitor-exit p0

    return-void

    .line 175
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V
    .locals 2

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    .line 193
    if-eqz v0, :cond_0

    .line 194
    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b()V

    .line 198
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c()V

    .line 200
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    :cond_0
    monitor-exit p0

    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Lcom/google/android/apps/gmm/map/legacy/a/a/e;)V
    .locals 8

    .prologue
    .line 420
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    .line 421
    if-nez v0, :cond_1

    .line 422
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/a/a;)V

    .line 423
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v6, v0

    .line 425
    :goto_0
    iget-object v0, v6, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->b:Lcom/google/android/apps/gmm/map/legacy/a/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->b:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    if-nez v0, :cond_0

    sget-object v7, Lcom/google/android/apps/gmm/map/legacy/a/a/a;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/legacy/a/a/b;-><init>(Lcom/google/android/apps/gmm/map/legacy/a/c/b/a/a;IIJ)V

    invoke-virtual {v6, v7, v0}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 426
    :goto_1
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/legacy/a/a/d;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 427
    monitor-exit p0

    return-void

    .line 425
    :cond_0
    :try_start_1
    iput-wide v4, v0, Lcom/google/android/apps/gmm/map/legacy/a/a/b;->d:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 420
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v6, v0

    goto :goto_0
.end method
