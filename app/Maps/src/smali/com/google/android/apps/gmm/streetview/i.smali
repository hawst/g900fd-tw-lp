.class Lcom/google/android/apps/gmm/streetview/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/i;->a:Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/p/c/b;)V
    .locals 24
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 97
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/apps/gmm/p/c/b;->b:F

    .line 98
    const/high16 v5, -0x3b860000    # -1000.0f

    cmpl-float v5, v4, v5

    if-nez v5, :cond_0

    .line 99
    const/high16 v4, 0x42b40000    # 90.0f

    .line 103
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/i;->a:Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget v12, v0, Lcom/google/android/apps/gmm/p/c/b;->a:F

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v5}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/geo/render/mirth/api/e;->c()Lcom/google/geo/render/mirth/api/ax;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/google/geo/render/mirth/api/ax;->a(I)Lcom/google/geo/render/mirth/api/f;

    move-result-object v20

    new-instance v5, Lcom/google/geo/render/mirth/api/f;

    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/google/geo/render/mirth/api/f;->a:J

    move-object/from16 v0, v20

    invoke-static {v6, v7, v0}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->LookFromCamera_getLongitude(JLcom/google/geo/render/mirth/api/f;)D

    move-result-wide v6

    move-object/from16 v0, v20

    iget-wide v8, v0, Lcom/google/geo/render/mirth/api/f;->a:J

    move-object/from16 v0, v20

    invoke-static {v8, v9, v0}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->LookFromCamera_getLatitude(JLcom/google/geo/render/mirth/api/f;)D

    move-result-wide v8

    move-object/from16 v0, v20

    iget-wide v10, v0, Lcom/google/geo/render/mirth/api/f;->a:J

    move-object/from16 v0, v20

    invoke-static {v10, v11, v0}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->LookFromCamera_getAltitude(JLcom/google/geo/render/mirth/api/f;)D

    move-result-wide v10

    float-to-double v12, v12

    float-to-double v14, v4

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/google/geo/render/mirth/api/f;->a:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->LookFromCamera_getRoll(JLcom/google/geo/render/mirth/api/f;)D

    move-result-wide v16

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/google/geo/render/mirth/api/f;->a:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v20}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->LookFromCamera_getFovY(JLcom/google/geo/render/mirth/api/f;)D

    move-result-wide v18

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/google/geo/render/mirth/api/f;->a:J

    move-wide/from16 v22, v0

    move-wide/from16 v0, v22

    move-object/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/google/geo/render/mirth/api/LookFromCameraSwigJNI;->LookFromCamera_getAltitudeReference(JLcom/google/geo/render/mirth/api/f;)I

    move-result v20

    invoke-direct/range {v5 .. v20}, Lcom/google/geo/render/mirth/api/f;-><init>(DDDDDDDI)V

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/geo/render/mirth/api/e;->c()Lcom/google/geo/render/mirth/api/ax;

    move-result-object v4

    const/4 v6, 0x0

    const-wide/16 v7, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Lcom/google/geo/render/mirth/api/ax;->a(Lcom/google/geo/render/mirth/api/f;IDZ)V

    .line 104
    return-void

    .line 101
    :cond_0
    neg-float v4, v4

    goto :goto_0
.end method
