.class public Lcom/google/android/apps/gmm/map/internal/c/bd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final f:[I

.field private static g:Lcom/google/android/apps/gmm/map/internal/c/bd;


# instance fields
.field public final a:I

.field public final b:F

.field public final c:[I

.field public final d:I

.field public final e:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 18
    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->f:[I

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bd;

    const/high16 v2, 0x3f800000    # 1.0f

    new-array v3, v1, [I

    const/4 v5, 0x0

    move v4, v1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/c/bd;-><init>(IF[IIF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->g:Lcom/google/android/apps/gmm/map/internal/c/bd;

    return-void
.end method

.method public constructor <init>(IF[IIF)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    .line 43
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    .line 44
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    .line 45
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    .line 46
    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    .line 47
    return-void
.end method

.method public static a()Lcom/google/android/apps/gmm/map/internal/c/bd;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->g:Lcom/google/android/apps/gmm/map/internal/c/bd;

    return-object v0
.end method

.method public static a(Lcom/google/maps/b/a/cj;)Lcom/google/android/apps/gmm/map/internal/c/bd;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/high16 v6, 0x41000000    # 8.0f

    .line 85
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->a:Lcom/google/maps/b/a/cz;

    iget v1, v0, Lcom/google/maps/b/a/cz;->b:I

    .line 86
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->b:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v0, v0

    div-float v2, v0, v6

    .line 87
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->f:[I

    .line 88
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    iget v0, v0, Lcom/google/maps/b/a/cy;->b:I

    if-lez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    iget v0, v0, Lcom/google/maps/b/a/cy;->b:I

    new-array v3, v0, [I

    move v0, v4

    .line 90
    :goto_0
    iget-object v5, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    iget v5, v5, Lcom/google/maps/b/a/cy;->b:I

    if-ge v0, v5, :cond_0

    .line 91
    iget-object v5, p0, Lcom/google/maps/b/a/cj;->c:Lcom/google/maps/b/a/cy;

    iget-object v5, v5, Lcom/google/maps/b/a/cy;->a:[I

    aget v5, v5, v0

    aput v5, v3, v0

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/maps/b/a/cj;->d:Lcom/google/maps/b/a/cz;

    iget v0, v0, Lcom/google/maps/b/a/cz;->b:I

    int-to-float v0, v0

    div-float v5, v0, v6

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bd;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/c/bd;-><init>(IF[IIF)V

    return-object v0
.end method

.method public static a(Ljava/io/DataInput;)Lcom/google/android/apps/gmm/map/internal/c/bd;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 55
    invoke-interface {p0}, Ljava/io/DataInput;->readInt()I

    move-result v1

    .line 58
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x41000000    # 8.0f

    div-float v2, v0, v2

    .line 61
    invoke-static {p0}, Lcom/google/android/apps/gmm/shared/c/y;->a(Ljava/io/DataInput;)I

    move-result v5

    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bd;->f:[I

    .line 63
    if-lez v5, :cond_0

    .line 64
    new-array v0, v5, [I

    move v4, v3

    .line 65
    :goto_0
    if-ge v4, v5, :cond_0

    .line 66
    invoke-interface {p0}, Ljava/io/DataInput;->readShort()S

    move-result v6

    aput v6, v0, v4

    .line 65
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 71
    :cond_0
    array-length v4, v0

    :goto_1
    if-ge v3, v4, :cond_2

    aget v5, v0, v3

    .line 72
    if-nez v5, :cond_1

    .line 73
    sget-object v3, Lcom/google/android/apps/gmm/map/internal/c/bd;->f:[I

    .line 78
    :goto_2
    invoke-interface {p0}, Ljava/io/DataInput;->readUnsignedByte()I

    move-result v4

    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bd;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/internal/c/bd;-><init>(IF[IIF)V

    return-object v0

    .line 71
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move-object v3, v0

    goto :goto_2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149
    if-ne p0, p1, :cond_1

    .line 171
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 153
    goto :goto_0

    .line 155
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 156
    goto :goto_0

    .line 158
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bd;

    .line 159
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 160
    goto :goto_0

    .line 162
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 163
    goto :goto_0

    .line 165
    :cond_5
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 166
    goto :goto_0

    .line 168
    :cond_6
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 169
    goto :goto_0

    .line 171
    :cond_7
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 137
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    add-int/lit8 v0, v0, 0x1f

    .line 140
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    add-int/2addr v0, v1

    .line 142
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 181
    const-string v1, "Stroke{color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 182
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", width="

    .line 183
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->b:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", offset="

    .line 184
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->e:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", dashes="

    .line 185
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->c:[I

    invoke-static {v2}, Ljava/util/Arrays;->toString([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", endCaps="

    .line 186
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 187
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 188
    const-string v1, "S"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 190
    :cond_0
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/bd;->d:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 191
    const-string v1, "E"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 193
    :cond_1
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 194
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
