.class public Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "intent"
    b = .enum Lcom/google/android/apps/gmm/util/replay/d;->LOW:Lcom/google/android/apps/gmm/util/replay/d;
.end annotation


# instance fields
.field private final intent:Landroid/content/Intent;

.field private final isSynthetic:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->isSynthetic:Ljava/lang/Boolean;

    .line 28
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "action"
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "uri"
        .end annotation
    .end param
    .param p3    # Ljava/lang/Boolean;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "synthetic"
        .end annotation
    .end param

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    .line 35
    if-eqz p1, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 38
    :cond_0
    if-eqz p2, :cond_1

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 41
    :cond_1
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->isSynthetic:Ljava/lang/Boolean;

    .line 42
    return-void
.end method


# virtual methods
.method public getAction()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "action"
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public getSynthetic()Ljava/lang/Boolean;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "synthetic"
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->isSynthetic:Ljava/lang/Boolean;

    return-object v0
.end method

.method public getUriString()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "uri"
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hasAction()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "action"
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSynthetic()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "synthetic"
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->isSynthetic:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasUriString()Z
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/f;
        a = "uri"
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->intent:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 80
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 81
    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/b/a/ak;->b:Z

    const-string v0, "action"

    .line 82
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->getAction()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "uri"

    .line 83
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->getUriString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "synthetic"

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/intents/AndroidIntentEvent;->getSynthetic()Ljava/lang/Boolean;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 85
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
