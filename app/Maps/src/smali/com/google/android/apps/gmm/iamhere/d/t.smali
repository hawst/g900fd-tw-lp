.class Lcom/google/android/apps/gmm/iamhere/d/t;
.super Landroid/content/BroadcastReceiver;
.source "PG"


# static fields
.field static final a:Landroid/content/IntentFilter;


# instance fields
.field final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/gmm/iamhere/d/p;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.apps.gmm.iamhere.notifications.DISMISSED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/d/t;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/iamhere/d/p;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/t;->c:Lcom/google/android/apps/gmm/iamhere/d/p;

    .line 86
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/d/t;->b:Landroid/content/Context;

    .line 87
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/iamhere/c/a;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 76
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.apps.gmm.iamhere.notifications.DISMISSED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 77
    const-string v1, "here_notification"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 78
    return-object v0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/t;->c:Lcom/google/android/apps/gmm/iamhere/d/p;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "here_notification"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/iamhere/d/p;->a(Lcom/google/android/apps/gmm/iamhere/c/a;)V

    .line 92
    return-void
.end method
