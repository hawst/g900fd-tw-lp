.class public Lcom/google/android/apps/gmm/startpage/d/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:Lcom/google/android/apps/gmm/startpage/d/c;


# instance fields
.field public final b:I

.field public final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/c;

    const/4 v1, -0x1

    const-wide/16 v2, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/startpage/d/c;-><init>(IJ)V

    sput-object v0, Lcom/google/android/apps/gmm/startpage/d/c;->a:Lcom/google/android/apps/gmm/startpage/d/c;

    return-void
.end method

.method public constructor <init>(IJ)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    .line 23
    iput-wide p2, p0, Lcom/google/android/apps/gmm/startpage/d/c;->c:J

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 28
    instance-of v1, p1, Lcom/google/android/apps/gmm/startpage/d/c;

    if-eqz v1, :cond_0

    .line 29
    check-cast p1, Lcom/google/android/apps/gmm/startpage/d/c;

    .line 30
    iget v1, p0, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    if-ne v1, v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/startpage/d/c;->c:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/startpage/d/c;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 33
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 38
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/startpage/d/c;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 43
    const-string v0, "<moduleType:%d, moduleSubtype:%d>"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/gmm/startpage/d/c;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/apps/gmm/startpage/d/c;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
