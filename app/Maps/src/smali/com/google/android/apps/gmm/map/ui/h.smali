.class Lcom/google/android/apps/gmm/map/ui/h;
.super Lcom/google/android/apps/gmm/v/e;
.source "PG"


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;

.field private final b:Lcom/google/android/apps/gmm/v/cp;

.field private final c:Lcom/google/android/apps/gmm/map/f/o;

.field private d:F

.field private e:F


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 1

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/ui/h;->a:Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/e;-><init>()V

    .line 324
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/ui/h;->c:Lcom/google/android/apps/gmm/map/f/o;

    .line 325
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/f/o;->z:Lcom/google/android/apps/gmm/v/cp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/ui/h;->b:Lcom/google/android/apps/gmm/v/cp;

    .line 326
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/ui/h;->d:F

    .line 327
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iput v0, p0, Lcom/google/android/apps/gmm/map/ui/h;->e:F

    .line 328
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 365
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->d:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/h;->b:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 333
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 4

    .prologue
    const v3, 0x3c23d70a    # 0.01f

    .line 340
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/ui/h;->c:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 341
    iget v1, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    .line 342
    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    .line 343
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ui/h;->b:Lcom/google/android/apps/gmm/v/cp;

    invoke-interface {p1, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 345
    iget v2, p0, Lcom/google/android/apps/gmm/map/ui/h;->d:F

    sub-float v2, v1, v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/map/ui/h;->e:F

    sub-float v2, v0, v2

    .line 346
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_0

    .line 359
    :goto_0
    return-void

    .line 349
    :cond_0
    iput v1, p0, Lcom/google/android/apps/gmm/map/ui/h;->d:F

    .line 350
    iput v0, p0, Lcom/google/android/apps/gmm/map/ui/h;->e:F

    .line 353
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/ui/h;->a:Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;

    new-instance v3, Lcom/google/android/apps/gmm/map/ui/i;

    invoke-direct {v3, p0, v1, v0}, Lcom/google/android/apps/gmm/map/ui/i;-><init>(Lcom/google/android/apps/gmm/map/ui/h;FF)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/ui/BaseCompassButtonView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
