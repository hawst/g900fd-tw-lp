.class public Lcom/google/android/apps/gmm/map/internal/c/am;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bu;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/e/a/a/a/b;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/gmm/map/internal/c/am;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/am;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/e/a/a/a/b;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/16 v7, 0x1c

    const/4 v6, 0x2

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/e/a/a/a/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->b:Lcom/google/e/a/a/a/b;

    .line 40
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v2

    :goto_0
    const-string v3, "geo_asset_id must be set"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v4, v1

    move v3, v1

    .line 44
    :goto_1
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v6}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-ge v4, v0, :cond_5

    .line 45
    const/16 v0, 0x1a

    invoke-virtual {p1, v6, v4, v0}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 46
    const-string v5, "z_order"

    invoke-virtual {v0, v2, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 47
    invoke-virtual {v0, v6, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 49
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 44
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v3, v0

    goto :goto_1

    .line 51
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/map/internal/c/am;->a:Ljava/lang/String;

    const-string v1, "Invalid z order: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move v0, v3

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    :cond_4
    move v0, v3

    goto :goto_2

    .line 56
    :cond_5
    iput v3, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->d:I

    .line 57
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bv;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/bv;->g:Lcom/google/android/apps/gmm/map/internal/c/bv;

    return-object v0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    .line 115
    const/16 v0, 0x1d

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->b:Lcom/google/e/a/a/a/b;

    iget-object v2, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v2, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 116
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/ai;)Z
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->t:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bu;)Z
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/internal/c/am;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/bu;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/am;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->d:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/am;->d:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 95
    if-ne p0, p1, :cond_1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/internal/c/am;

    if-nez v2, :cond_2

    move v0, v1

    .line 99
    goto :goto_0

    .line 102
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/c/am;

    .line 104
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->d:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/am;->d:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->d:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/am;->b:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0}, Lcom/google/e/a/a/a/b;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
