.class public Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;
.super Lcom/google/android/apps/gmm/cardui/CardUiListFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/login/c/e;


# static fields
.field static final f:Ljava/lang/String;


# instance fields
.field g:Z

.field m:Lcom/google/r/b/a/tw;

.field n:Z

.field o:Z

.field private final p:Ljava/lang/Object;

.field private q:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private r:Landroid/view/View;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;-><init>()V

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/profile/a;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/a;-><init>(Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->p:Ljava/lang/Object;

    .line 82
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->g:Z

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->m:Lcom/google/r/b/a/tw;

    .line 85
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->s:Z

    .line 86
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->n:Z

    .line 87
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->o:Z

    .line 238
    return-void
.end method

.method private a(Z)Landroid/view/View;
    .locals 3

    .prologue
    .line 153
    if-eqz p1, :cond_0

    .line 154
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->b(Z)V

    .line 156
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->r:Landroid/view/View;

    :goto_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->q:Landroid/view/View;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;Lcom/google/r/b/a/tw;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->a(Lcom/google/r/b/a/tw;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->b(Z)V

    return-void
.end method

.method private a(Lcom/google/r/b/a/tw;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 329
    .line 330
    if-eqz p1, :cond_b

    .line 331
    invoke-virtual {p1}, Lcom/google/r/b/a/tw;->d()Ljava/lang/String;

    move-result-object v1

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    .line 333
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/s;->b()V

    .line 334
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->n:Z

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->o:Z

    .line 335
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/r/b/a/tw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/tz;->d()Lcom/google/r/b/a/tz;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/tz;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/tz;

    .line 336
    iget-object v0, v0, Lcom/google/r/b/a/tz;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/be;

    .line 337
    invoke-virtual {p1}, Lcom/google/r/b/a/tw;->d()Ljava/lang/String;

    move-result-object v5

    :try_start_0
    iget-object v6, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-static {}, Lcom/google/o/h/a/br;->newBuilder()Lcom/google/o/h/a/bt;

    move-result-object v7

    invoke-virtual {v0}, Lcom/google/r/b/a/be;->l()[B

    move-result-object v0

    const/4 v8, 0x0

    array-length v9, v0

    invoke-virtual {v7, v0, v8, v9}, Lcom/google/n/b;->a([BII)Lcom/google/n/b;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bt;

    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v5, v7}, Lcom/google/android/apps/gmm/cardui/s;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V
    :try_end_0
    .catch Lcom/google/n/ak; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->f:Ljava/lang/String;

    goto :goto_1

    .line 340
    :cond_2
    iput-boolean v10, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->s:Z

    .line 342
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->n:Z

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->g:Z

    if-nez v0, :cond_6

    .line 343
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a;->a()V

    invoke-static {}, Lcom/google/o/h/a/br;->newBuilder()Lcom/google/o/h/a/bt;

    move-result-object v0

    sget-object v3, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    if-nez v3, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v4, v0, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/o/h/a/bt;->a:I

    iget v3, v3, Lcom/google/o/h/a/bw;->f:I

    iput v3, v0, Lcom/google/o/h/a/bt;->c:I

    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v3

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v4

    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v5

    const-string v6, "Offline"

    if-nez v6, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    invoke-virtual {v5}, Lcom/google/o/h/a/gr;->c()V

    iget-object v7, v5, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v7, v6}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    iget-object v6, v4, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v5

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v6, Lcom/google/n/ao;->d:Z

    iget v5, v4, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/o/h/a/it;->a:I

    iget-object v5, v3, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v4

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v5, Lcom/google/n/ao;->d:Z

    iget v4, v3, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/o/h/a/ix;->a:I

    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v4

    sget-object v5, Lcom/google/o/h/a/ji;->W:Lcom/google/o/h/a/ji;

    if-nez v5, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget v6, v4, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v4, Lcom/google/o/h/a/jh;->a:I

    iget v5, v5, Lcom/google/o/h/a/ji;->ah:I

    iput v5, v4, Lcom/google/o/h/a/jh;->b:I

    invoke-virtual {v3}, Lcom/google/o/h/a/ix;->c()V

    iget-object v5, v3, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-virtual {v4}, Lcom/google/o/h/a/jh;->g()Lcom/google/n/t;

    move-result-object v4

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->c()V

    iget-object v4, v0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-virtual {v3}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v3

    new-instance v5, Lcom/google/n/ao;

    invoke-direct {v5}, Lcom/google/n/ao;-><init>()V

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v5, Lcom/google/n/ao;->d:Z

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/apps/gmm/cardui/s;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    .line 344
    iput-boolean v10, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->n:Z

    .line 346
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->o:Z

    if-nez v0, :cond_a

    invoke-direct {p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->r()Z

    move-result v0

    if-nez v0, :cond_a

    .line 347
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a;->a()V

    invoke-static {}, Lcom/google/o/h/a/br;->newBuilder()Lcom/google/o/h/a/bt;

    move-result-object v0

    sget-object v3, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    if-nez v3, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v4, v0, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/o/h/a/bt;->a:I

    iget v3, v3, Lcom/google/o/h/a/bw;->f:I

    iput v3, v0, Lcom/google/o/h/a/bt;->c:I

    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v3

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v4

    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v5

    const-string v6, "No Network"

    if-nez v6, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    invoke-virtual {v5}, Lcom/google/o/h/a/gr;->c()V

    iget-object v7, v5, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v7, v6}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    iget-object v6, v4, Lcom/google/o/h/a/it;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v5

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v6, Lcom/google/n/ao;->d:Z

    iget v5, v4, Lcom/google/o/h/a/it;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/o/h/a/it;->a:I

    iget-object v5, v3, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v4

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v5, Lcom/google/n/ao;->d:Z

    iget v4, v3, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/o/h/a/ix;->a:I

    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v4

    sget-object v5, Lcom/google/o/h/a/ji;->ag:Lcom/google/o/h/a/ji;

    if-nez v5, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget v6, v4, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v4, Lcom/google/o/h/a/jh;->a:I

    iget v5, v5, Lcom/google/o/h/a/ji;->ah:I

    iput v5, v4, Lcom/google/o/h/a/jh;->b:I

    invoke-virtual {v3}, Lcom/google/o/h/a/ix;->c()V

    iget-object v5, v3, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-virtual {v4}, Lcom/google/o/h/a/jh;->g()Lcom/google/n/t;

    move-result-object v4

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->c()V

    iget-object v4, v0, Lcom/google/o/h/a/bt;->b:Ljava/util/List;

    invoke-virtual {v3}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v3

    new-instance v5, Lcom/google/n/ao;

    invoke-direct {v5}, Lcom/google/n/ao;-><init>()V

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v5, Lcom/google/n/ao;->d:Z

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    iget-object v3, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/apps/gmm/cardui/s;->a(Lcom/google/o/h/a/br;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    .line 348
    iput-boolean v10, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->o:Z

    .line 350
    :cond_a
    return-void

    :cond_b
    move-object v1, v2

    goto/16 :goto_2
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;Z)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 165
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->s:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->g:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->m:Lcom/google/r/b/a/tw;

    if-nez v0, :cond_4

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v0, :cond_3

    move v0, v2

    :goto_0
    if-eqz v0, :cond_4

    move v0, v1

    .line 169
    :goto_1
    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->d:Lcom/google/android/apps/gmm/cardui/h/a;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/cardui/h/a;->b:Z

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 172
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    .line 173
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->v()Lcom/google/android/apps/gmm/mapsactivity/a/f;

    move-result-object v0

    .line 174
    invoke-interface {v0}, Lcom/google/android/apps/gmm/mapsactivity/a/f;->d()Lcom/google/android/apps/gmm/mapsactivity/a/h;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/mapsactivity/profile/b;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/b;-><init>(Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;)V

    .line 175
    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/mapsactivity/a/h;->a(Lcom/google/android/apps/gmm/shared/net/c;)V

    .line 176
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->g:Z

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->m:Lcom/google/r/b/a/tw;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->a(Lcom/google/r/b/a/tw;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/s;->j:Lcom/google/android/apps/gmm/util/b/i;

    invoke-interface {v1, v0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 180
    :cond_2
    return-void

    .line 168
    :cond_3
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public static q()Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;-><init>()V

    return-object v0
.end method

.method private r()Z
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    :goto_0
    return-void

    .line 195
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->a(Z)Landroid/view/View;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected final b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 2

    .prologue
    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->pl:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/views/c/g;->a(Landroid/app/Activity;Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    return-object v0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 206
    sget-object v0, Lcom/google/b/f/t;->fo:Lcom/google/b/f/t;

    return-object v0
.end method

.method protected final i()Lcom/google/android/apps/gmm/cardui/b/b;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/apps/gmm/cardui/b/b;->a:Lcom/google/android/apps/gmm/cardui/b/b;

    return-object v0
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 270
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    .line 273
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 115
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 126
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->r:Landroid/view/View;

    .line 130
    if-eqz p3, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v0, p3}, Lcom/google/android/apps/gmm/cardui/s;->b(Landroid/os/Bundle;)V

    .line 132
    const-string v0, "arg_key_maps_activity_got_profile"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->s:Z

    .line 138
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->s:Z

    goto :goto_0

    .line 138
    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/az;

    invoke-virtual {v0, v1, p2, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 139
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->q:Landroid/view/View;

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->q:Landroid/view/View;

    new-instance v1, Lcom/google/android/apps/gmm/login/c/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/login/c/d;-><init>(Lcom/google/android/apps/gmm/login/c/e;)V

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    sget v1, Lcom/google/android/apps/gmm/l;->pl:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->e()Z

    move-result v0

    .line 142
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->a(Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->p:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onDestroy()V

    .line 121
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 148
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onDestroyView()V

    .line 149
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->n:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->o:Z

    .line 150
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 200
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onPause()V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 202
    return-void
.end method

.method public onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 211
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onResume()V

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 215
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 216
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 217
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 218
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 219
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    .line 221
    iget-object v1, v1, Lcom/google/android/apps/gmm/cardui/c;->b:Lcom/google/android/apps/gmm/z/a;

    .line 220
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/activities/p;->a(Lcom/google/android/apps/gmm/z/a;)Lcom/google/android/apps/gmm/base/activities/y;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 222
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 226
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->o:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->m:Lcom/google/r/b/a/tw;

    if-eqz v0, :cond_2

    .line 227
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->r:Landroid/view/View;

    sget-object v2, Lcom/google/android/apps/gmm/cardui/d/b;->a:Lcom/google/android/libraries/curvular/bk;

    .line 228
    invoke-static {v1, v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0xfa

    .line 227
    invoke-static {v0, v1, v4, v2, v4}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;IIZ)V

    .line 231
    :cond_2
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/cardui/CardUiListFragment;->c:Lcom/google/android/apps/gmm/cardui/s;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/cardui/s;->a(Landroid/os/Bundle;)V

    .line 97
    const-string v0, "arg_key_maps_activity_got_profile"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->s:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 98
    return-void
.end method

.method public final p()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->go:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 279
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mapsactivity/profile/ProfilePageFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/login/LoginDialog;->a(Landroid/app/Activity;)V

    .line 280
    return-void
.end method
