.class public Lcom/google/android/apps/gmm/g/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/h/c;


# static fields
.field private static final e:J

.field private static final f:J


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/a;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/apps/gmm/z/a/b;

.field private final d:Lcom/google/android/apps/gmm/shared/net/r;

.field private final g:I

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Lcom/google/android/apps/gmm/map/h/b;

.field private k:Landroid/content/IntentFilter;

.field private l:Lcom/google/android/apps/gmm/map/h/e;

.field private m:Landroid/content/IntentFilter;

.field private n:I

.field private o:Lcom/google/android/apps/gmm/g/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/g/a;->e:J

    .line 81
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {v0}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/g/a;->f:J

    .line 80
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 2

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    .line 151
    iput-object p1, p0, Lcom/google/android/apps/gmm/g/a;->a:Lcom/google/android/apps/gmm/base/a;

    .line 152
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    .line 153
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->c:Lcom/google/android/apps/gmm/z/a/b;

    .line 154
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->d:Lcom/google/android/apps/gmm/shared/net/r;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v0, p0, Lcom/google/android/apps/gmm/g/a;->g:I

    .line 157
    new-instance v0, Lcom/google/android/apps/gmm/map/h/b;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/h/b;-><init>(Lcom/google/android/apps/gmm/map/h/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->j:Lcom/google/android/apps/gmm/map/h/b;

    .line 158
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->k:Landroid/content/IntentFilter;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/g/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/g/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    .line 163
    new-instance v0, Lcom/google/android/apps/gmm/map/h/e;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/h/e;-><init>(Lcom/google/android/apps/gmm/map/h/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->l:Lcom/google/android/apps/gmm/map/h/e;

    .line 164
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->m:Landroid/content/IntentFilter;

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->m:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_POWER_DISCONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->m:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 167
    return-void
.end method

.method private a(Lcom/google/b/f/b/a/ac;)V
    .locals 4

    .prologue
    .line 170
    invoke-virtual {p1}, Lcom/google/b/f/b/a/ac;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/aa;

    .line 172
    new-instance v1, Lcom/google/android/apps/gmm/z/b/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/g/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/e;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 173
    iget-object v2, v1, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    invoke-virtual {v2, v0}, Lcom/google/b/f/b/a/bc;->a(Lcom/google/b/f/b/a/aa;)Lcom/google/b/f/b/a/bc;

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->c:Lcom/google/android/apps/gmm/z/a/b;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/apps/gmm/z/b/a;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/z/a/b;->a([Lcom/google/android/apps/gmm/z/b/a;)Ljava/util/List;

    .line 175
    return-void
.end method

.method private b(Z)Lcom/google/b/f/b/a/ca;
    .locals 14

    .prologue
    const-wide/16 v12, 0x400

    const-wide/16 v10, 0x0

    const/4 v4, 0x0

    const-wide/16 v0, -0x1

    .line 396
    iget v2, p0, Lcom/google/android/apps/gmm/g/a;->g:I

    invoke-static {v2}, Landroid/net/TrafficStats;->getUidRxBytes(I)J

    move-result-wide v2

    .line 397
    iget v5, p0, Lcom/google/android/apps/gmm/g/a;->g:I

    invoke-static {v5}, Landroid/net/TrafficStats;->getUidTxBytes(I)J

    move-result-wide v8

    .line 400
    cmp-long v5, v2, v0

    if-nez v5, :cond_0

    move-wide v6, v0

    .line 408
    :goto_0
    cmp-long v2, v8, v0

    if-nez v2, :cond_1

    move-wide v2, v0

    .line 417
    :goto_1
    invoke-static {}, Lcom/google/b/f/b/a/by;->newBuilder()Lcom/google/b/f/b/a/ca;

    move-result-object v1

    if-nez p1, :cond_6

    sget-object v0, Lcom/google/r/b/a/pi;->b:Lcom/google/r/b/a/pi;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 405
    :cond_0
    sget-wide v6, Lcom/google/android/apps/gmm/g/a;->e:J

    sub-long/2addr v2, v6

    move-wide v6, v2

    goto :goto_0

    .line 413
    :cond_1
    sget-wide v0, Lcom/google/android/apps/gmm/g/a;->f:J

    sub-long v0, v8, v0

    move-wide v2, v0

    goto :goto_1

    .line 417
    :cond_2
    iget v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    iget v0, v0, Lcom/google/r/b/a/pi;->g:I

    iput v0, v1, Lcom/google/b/f/b/a/ca;->b:I

    :cond_3
    :goto_2
    cmp-long v0, v6, v10

    if-ltz v0, :cond_4

    long-to-int v0, v6

    div-int/lit16 v0, v0, 0x400

    iget v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v4, v4, 0x40

    iput v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/ca;->d:I

    :cond_4
    cmp-long v0, v2, v10

    if-ltz v0, :cond_5

    long-to-int v0, v2

    div-int/lit16 v0, v0, 0x400

    iget v2, v1, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit16 v2, v2, 0x80

    iput v2, v1, Lcom/google/b/f/b/a/ca;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/ca;->e:I

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->d:Lcom/google/android/apps/gmm/shared/net/r;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/r;->c()Lcom/google/android/apps/gmm/shared/net/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/h;->a()J

    move-result-wide v2

    div-long/2addr v2, v12

    long-to-int v2, v2

    iget v3, v1, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit16 v3, v3, 0x100

    iput v3, v1, Lcom/google/b/f/b/a/ca;->a:I

    iput v2, v1, Lcom/google/b/f/b/a/ca;->f:I

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/h;->b()J

    move-result-wide v2

    div-long/2addr v2, v12

    long-to-int v0, v2

    iget v2, v1, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v1, Lcom/google/b/f/b/a/ca;->a:I

    iput v0, v1, Lcom/google/b/f/b/a/ca;->g:I

    move-object v0, v1

    :goto_3
    return-object v0

    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v5

    if-nez v5, :cond_8

    :cond_7
    move-object v5, v4

    :goto_4
    if-nez v5, :cond_9

    move-object v0, v4

    goto :goto_3

    :cond_8
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/b/a/an;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/a/an;

    move-result-object v0

    move-object v5, v0

    goto :goto_4

    :cond_9
    iget-object v0, v5, Lcom/google/b/a/an;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    sget-object v0, Lcom/google/r/b/a/pi;->a:Lcom/google/r/b/a/pi;

    :goto_5
    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :pswitch_1
    sget-object v0, Lcom/google/r/b/a/pi;->c:Lcom/google/r/b/a/pi;

    goto :goto_5

    :pswitch_2
    sget-object v0, Lcom/google/r/b/a/pi;->d:Lcom/google/r/b/a/pi;

    goto :goto_5

    :pswitch_3
    sget-object v0, Lcom/google/r/b/a/pi;->e:Lcom/google/r/b/a/pi;

    goto :goto_5

    :pswitch_4
    sget-object v0, Lcom/google/r/b/a/pi;->f:Lcom/google/r/b/a/pi;

    goto :goto_5

    :cond_a
    iget v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    iget v4, v0, Lcom/google/r/b/a/pi;->g:I

    iput v4, v1, Lcom/google/b/f/b/a/ca;->b:I

    sget-object v4, Lcom/google/r/b/a/pi;->d:Lcom/google/r/b/a/pi;

    if-ne v0, v4, :cond_3

    iget-object v0, v5, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    sget-object v0, Lcom/google/r/b/a/dh;->a:Lcom/google/r/b/a/dh;

    :goto_6
    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :pswitch_5
    sget-object v0, Lcom/google/r/b/a/dh;->b:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_6
    sget-object v0, Lcom/google/r/b/a/dh;->c:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_7
    sget-object v0, Lcom/google/r/b/a/dh;->d:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_8
    sget-object v0, Lcom/google/r/b/a/dh;->e:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_9
    sget-object v0, Lcom/google/r/b/a/dh;->f:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_a
    sget-object v0, Lcom/google/r/b/a/dh;->g:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_b
    sget-object v0, Lcom/google/r/b/a/dh;->h:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_c
    sget-object v0, Lcom/google/r/b/a/dh;->i:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_d
    sget-object v0, Lcom/google/r/b/a/dh;->j:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_e
    sget-object v0, Lcom/google/r/b/a/dh;->k:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_f
    sget-object v0, Lcom/google/r/b/a/dh;->l:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_10
    sget-object v0, Lcom/google/r/b/a/dh;->m:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_11
    sget-object v0, Lcom/google/r/b/a/dh;->n:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_12
    sget-object v0, Lcom/google/r/b/a/dh;->o:Lcom/google/r/b/a/dh;

    goto :goto_6

    :pswitch_13
    sget-object v0, Lcom/google/r/b/a/dh;->p:Lcom/google/r/b/a/dh;

    goto :goto_6

    :cond_b
    iget v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v1, Lcom/google/b/f/b/a/ca;->a:I

    iget v0, v0, Lcom/google/r/b/a/dh;->q:I

    iput v0, v1, Lcom/google/b/f/b/a/ca;->c:I

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_9
        :pswitch_f
        :pswitch_10
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_13
        :pswitch_12
        :pswitch_11
    .end packed-switch
.end method

.method private e()Lcom/google/b/f/b/a/aw;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->a:Lcom/google/android/apps/gmm/base/a;

    .line 494
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->e()Lcom/google/android/apps/gmm/p/b/b;

    move-result-object v0

    invoke-static {}, Lcom/google/b/f/b/a/aw;->newBuilder()Lcom/google/b/f/b/a/ay;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    invoke-static {v3}, Lcom/google/android/apps/gmm/p/b/b;->a(Lcom/google/android/apps/gmm/p/b/d;)Lcom/google/r/b/a/jl;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v4, v2, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/google/b/f/b/a/ay;->a:I

    iget v3, v3, Lcom/google/r/b/a/jl;->f:I

    iput v3, v2, Lcom/google/b/f/b/a/ay;->b:I

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    invoke-static {v3}, Lcom/google/android/apps/gmm/p/b/b;->a(Lcom/google/android/apps/gmm/p/b/d;)Lcom/google/r/b/a/jl;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v4, v2, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v2, Lcom/google/b/f/b/a/ay;->a:I

    iget v3, v3, Lcom/google/r/b/a/jl;->f:I

    iput v3, v2, Lcom/google/b/f/b/a/ay;->c:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->c:Lcom/google/android/apps/gmm/p/b/d;

    invoke-static {v0}, Lcom/google/android/apps/gmm/p/b/b;->a(Lcom/google/android/apps/gmm/p/b/d;)Lcom/google/r/b/a/jl;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v2, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v2, Lcom/google/b/f/b/a/ay;->a:I

    iget v0, v0, Lcom/google/r/b/a/jl;->f:I

    iput v0, v2, Lcom/google/b/f/b/a/ay;->d:I

    invoke-virtual {v2}, Lcom/google/b/f/b/a/ay;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/aw;

    invoke-static {}, Lcom/google/b/f/b/a/aw;->newBuilder()Lcom/google/b/f/b/a/ay;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/b/f/b/a/ay;->a(Lcom/google/b/f/b/a/aw;)Lcom/google/b/f/b/a/ay;

    move-result-object v2

    .line 496
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    iget-object v3, v0, Lcom/google/android/apps/gmm/g/b;->e:Lcom/google/android/apps/gmm/z/b/h;

    iget-object v3, v3, Lcom/google/android/apps/gmm/z/b/h;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/g/b;->f:Lcom/google/android/apps/gmm/z/b/h;

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/h;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_8

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/g/b;->e:Lcom/google/android/apps/gmm/z/b/h;

    invoke-static {v0}, Lcom/google/android/apps/gmm/g/b;->a(Lcom/google/android/apps/gmm/z/b/h;)Lcom/google/b/f/b/a/ak;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 496
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 497
    :cond_4
    iget-object v3, v2, Lcom/google/b/f/b/a/ay;->e:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v2, Lcom/google/b/f/b/a/ay;->a:I

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/g/b;->f:Lcom/google/android/apps/gmm/z/b/h;

    invoke-static {v0}, Lcom/google/android/apps/gmm/g/b;->a(Lcom/google/android/apps/gmm/z/b/h;)Lcom/google/b/f/b/a/ak;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget-object v3, v2, Lcom/google/b/f/b/a/ay;->f:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v0, v0, 0x10

    iput v0, v2, Lcom/google/b/f/b/a/ay;->a:I

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/g/b;->g:Lcom/google/android/apps/gmm/z/b/d;

    invoke-static {}, Lcom/google/b/f/b/a/ao;->newBuilder()Lcom/google/b/f/b/a/aq;

    move-result-object v3

    iget v4, v0, Lcom/google/android/apps/gmm/z/b/d;->a:I

    iget v5, v3, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v3, Lcom/google/b/f/b/a/aq;->a:I

    iput v4, v3, Lcom/google/b/f/b/a/aq;->b:I

    iget v4, v0, Lcom/google/android/apps/gmm/z/b/d;->b:F

    iget v5, v3, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v3, Lcom/google/b/f/b/a/aq;->a:I

    iput v4, v3, Lcom/google/b/f/b/a/aq;->c:F

    iget v0, v0, Lcom/google/android/apps/gmm/z/b/d;->c:F

    iget v4, v3, Lcom/google/b/f/b/a/aq;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v3, Lcom/google/b/f/b/a/aq;->a:I

    iput v0, v3, Lcom/google/b/f/b/a/aq;->d:F

    invoke-virtual {v3}, Lcom/google/b/f/b/a/aq;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ao;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget-object v3, v2, Lcom/google/b/f/b/a/ay;->g:Lcom/google/n/ao;

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v0, v0, 0x20

    iput v0, v2, Lcom/google/b/f/b/a/ay;->a:I

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/g/b;->b:Landroid/location/Location;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/g/b;->c:Landroid/location/Location;

    if-nez v1, :cond_9

    :cond_7
    const/4 v0, 0x0

    :goto_1
    iget v1, v2, Lcom/google/b/f/b/a/ay;->a:I

    or-int/lit8 v1, v1, 0x40

    iput v1, v2, Lcom/google/b/f/b/a/ay;->a:I

    iput v0, v2, Lcom/google/b/f/b/a/ay;->h:F

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/h;-><init>()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/g/b;->e:Lcom/google/android/apps/gmm/z/b/h;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/h;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/h;-><init>()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/g/b;->f:Lcom/google/android/apps/gmm/z/b/h;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/d;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/z/b/d;-><init>()V

    iput-object v1, v0, Lcom/google/android/apps/gmm/g/b;->g:Lcom/google/android/apps/gmm/z/b/d;

    iput-object v6, v0, Lcom/google/android/apps/gmm/g/b;->b:Landroid/location/Location;

    iput-object v6, v0, Lcom/google/android/apps/gmm/g/b;->c:Landroid/location/Location;

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Lcom/google/android/apps/gmm/g/b;->d:J

    .line 503
    :cond_8
    invoke-virtual {v2}, Lcom/google/b/f/b/a/ay;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/aw;

    return-object v0

    .line 500
    :cond_9
    iget-object v1, v0, Lcom/google/android/apps/gmm/g/b;->c:Landroid/location/Location;

    iget-object v0, v0, Lcom/google/android/apps/gmm/g/b;->b:Landroid/location/Location;

    invoke-virtual {v1, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v0

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 5

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 222
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/g/a;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/g/a;->i:Ljava/lang/Boolean;

    .line 223
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v1, v0, :cond_1

    .line 238
    :goto_1
    monitor-exit p0

    return-void

    .line 221
    :cond_0
    :try_start_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    goto :goto_0

    .line 228
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/g/a;->b(Z)Lcom/google/b/f/b/a/ca;

    move-result-object v1

    .line 229
    invoke-static {}, Lcom/google/b/f/b/a/aa;->newBuilder()Lcom/google/b/f/b/a/ac;

    move-result-object v2

    .line 230
    sget-object v3, Lcom/google/r/b/a/av;->m:Lcom/google/r/b/a/av;

    if-nez v3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 230
    :cond_2
    :try_start_2
    iget v4, v2, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v2, Lcom/google/b/f/b/a/ac;->a:I

    iget v3, v3, Lcom/google/r/b/a/av;->v:I

    iput v3, v2, Lcom/google/b/f/b/a/ac;->b:I

    .line 231
    if-eqz v1, :cond_3

    .line 232
    iget-object v3, v2, Lcom/google/b/f/b/a/ac;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/b/f/b/a/ca;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, v2, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v2, Lcom/google/b/f/b/a/ac;->a:I

    .line 235
    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/g/a;->a(Lcom/google/b/f/b/a/ac;)V

    .line 237
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->i:Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final a(Landroid/location/Location;J)V
    .locals 4

    .prologue
    .line 627
    if-eqz p1, :cond_0

    .line 628
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->o:Lcom/google/android/apps/gmm/g/b;

    if-nez p1, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/g/b;->a:Ljava/lang/String;

    const-string v1, "location should not be null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 630
    :cond_0
    :goto_0
    return-void

    .line 628
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/g/b;->b:Landroid/location/Location;

    if-nez v1, :cond_2

    iput-object p1, v0, Lcom/google/android/apps/gmm/g/b;->b:Landroid/location/Location;

    :goto_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/g/b;->e:Lcom/google/android/apps/gmm/z/b/h;

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/z/b/h;->a(Ljava/lang/Comparable;)V

    iput-object p1, v0, Lcom/google/android/apps/gmm/g/b;->c:Landroid/location/Location;

    iput-wide p2, v0, Lcom/google/android/apps/gmm/g/b;->d:J

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/google/android/apps/gmm/g/b;->f:Lcom/google/android/apps/gmm/z/b/h;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/g/b;->d:J

    sub-long v2, p2, v2

    long-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/z/b/h;->a(Ljava/lang/Comparable;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/g/b;->g:Lcom/google/android/apps/gmm/z/b/d;

    iget-object v2, v0, Lcom/google/android/apps/gmm/g/b;->c:Landroid/location/Location;

    invoke-virtual {p1, v2}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/z/b/d;->a(F)V

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/google/r/b/a/av;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 245
    monitor-enter p0

    :try_start_0
    const-string v1, "DeviceStateReporter"

    const-string v2, "Report current device state!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 247
    invoke-static {}, Lcom/google/b/f/b/a/aa;->newBuilder()Lcom/google/b/f/b/a/ac;

    move-result-object v1

    .line 248
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 248
    :cond_0
    :try_start_1
    iget v2, v1, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/b/f/b/a/ac;->a:I

    iget v2, p1, Lcom/google/r/b/a/av;->v:I

    iput v2, v1, Lcom/google/b/f/b/a/ac;->b:I

    .line 250
    iget-object v2, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/content/Context;)Lcom/google/b/f/b/a/w;

    move-result-object v2

    .line 251
    if-eqz v2, :cond_1

    .line 252
    invoke-virtual {v1, v2}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/ac;

    .line 256
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/g/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/h/d;->b:Landroid/net/NetworkInfo;

    if-nez v2, :cond_3

    .line 255
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/g/a;->b(Z)Lcom/google/b/f/b/a/ca;

    move-result-object v0

    .line 257
    if-eqz v0, :cond_2

    .line 258
    iget-object v2, v1, Lcom/google/b/f/b/a/ac;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/b/f/b/a/ca;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v1, Lcom/google/b/f/b/a/ac;->a:I

    .line 261
    :cond_2
    invoke-direct {p0}, Lcom/google/android/apps/gmm/g/a;->e()Lcom/google/b/f/b/a/aw;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/aw;)Lcom/google/b/f/b/a/ac;

    .line 263
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/g/a;->a(Lcom/google/b/f/b/a/ac;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    monitor-exit p0

    return-void

    .line 256
    :cond_3
    :try_start_2
    invoke-virtual {v2}, Landroid/net/NetworkInfo;->isConnected()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    goto :goto_0
.end method

.method public final declared-synchronized a(Z)V
    .locals 4

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->h:Ljava/lang/Boolean;

    .line 189
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-ne v0, p1, :cond_0

    .line 213
    :goto_0
    monitor-exit p0

    return-void

    .line 194
    :cond_0
    if-eqz p1, :cond_1

    .line 195
    :try_start_1
    const-string v0, "DeviceStateReporter"

    const-string v1, "Charging now, will report battery state!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 200
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/z/b/b;->a(Landroid/content/Context;Z)Lcom/google/b/f/b/a/w;

    move-result-object v0

    .line 203
    invoke-static {}, Lcom/google/b/f/b/a/aa;->newBuilder()Lcom/google/b/f/b/a/ac;

    move-result-object v1

    .line 205
    sget-object v2, Lcom/google/r/b/a/av;->l:Lcom/google/r/b/a/av;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 197
    :cond_1
    :try_start_2
    const-string v0, "DeviceStateReporter"

    const-string v1, "Discharging now, will report battery state!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 205
    :cond_2
    iget v3, v1, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/b/f/b/a/ac;->a:I

    iget v2, v2, Lcom/google/r/b/a/av;->v:I

    iput v2, v1, Lcom/google/b/f/b/a/ac;->b:I

    .line 206
    if-eqz v0, :cond_3

    .line 207
    invoke-virtual {v1, v0}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/w;)Lcom/google/b/f/b/a/ac;

    .line 210
    :cond_3
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/g/a;->a(Lcom/google/b/f/b/a/ac;)V

    .line 212
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/g/a;->h:Ljava/lang/Boolean;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    .line 577
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    .line 579
    iget v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    if-gtz v0, :cond_1

    .line 580
    const-string v0, "DeviceStateReporter"

    const-string v1, "unregisterReceivers is called more than registerReceivers"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 593
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 584
    :cond_1
    :try_start_1
    const-string v0, "DeviceStateReporter"

    iget v1, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "registerReceivers: new receiver count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 585
    iget v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    const/4 v1, 0x1

    if-gt v0, v1, :cond_0

    .line 591
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/g/a;->j:Lcom/google/android/apps/gmm/map/h/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/g/a;->k:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 592
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/g/a;->l:Lcom/google/android/apps/gmm/map/h/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/g/a;->m:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 577
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/r/b/a/av;)V
    .locals 3
    .param p1    # Lcom/google/r/b/a/av;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 315
    monitor-enter p0

    :try_start_0
    const-string v0, "DeviceStateReporter"

    const-string v1, "Report current location state!"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 317
    invoke-static {}, Lcom/google/b/f/b/a/aa;->newBuilder()Lcom/google/b/f/b/a/ac;

    move-result-object v0

    .line 318
    if-eqz p1, :cond_1

    .line 319
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 319
    :cond_0
    :try_start_1
    iget v1, v0, Lcom/google/b/f/b/a/ac;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/b/f/b/a/ac;->a:I

    iget v1, p1, Lcom/google/r/b/a/av;->v:I

    iput v1, v0, Lcom/google/b/f/b/a/ac;->b:I

    .line 322
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/g/a;->e()Lcom/google/b/f/b/a/aw;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/b/f/b/a/ac;->a(Lcom/google/b/f/b/a/aw;)Lcom/google/b/f/b/a/ac;

    .line 324
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/g/a;->a(Lcom/google/b/f/b/a/ac;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 325
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    .line 602
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    .line 604
    iget v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    if-gez v0, :cond_1

    .line 605
    const-string v0, "DeviceStateReporter"

    const-string v1, "unregisterReceivers is called when there\'s no receiver"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 618
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 609
    :cond_1
    :try_start_1
    const-string v0, "DeviceStateReporter"

    iget v1, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x36

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unregisterReceivers: new receiver count is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 610
    iget v0, p0, Lcom/google/android/apps/gmm/g/a;->n:I

    if-gtz v0, :cond_0

    .line 616
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/g/a;->j:Lcom/google/android/apps/gmm/map/h/b;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 617
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/g/a;->l:Lcom/google/android/apps/gmm/map/h/e;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 602
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/h/d;
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/google/android/apps/gmm/g/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    return-object v0
.end method
