.class public Lcom/google/android/apps/gmm/x/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field final b:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;",
            "Lcom/google/android/apps/gmm/x/o",
            "<*>;>;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/x/m;

.field public final d:Lcom/google/android/apps/gmm/x/i;

.field public final e:Lcom/google/android/apps/gmm/shared/c/a/j;

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;",
            "Ljava/io/Serializable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/google/android/apps/gmm/x/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/x/i;Lcom/google/android/apps/gmm/shared/c/a/j;)V
    .locals 2

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/google/b/c/fd;

    invoke-direct {v0}, Lcom/google/b/c/fd;-><init>()V

    .line 56
    sget-object v1, Lcom/google/b/c/gr;->c:Lcom/google/b/c/gr;

    invoke-virtual {v0, v1}, Lcom/google/b/c/fd;->b(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/fd;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/x/a;->b:Ljava/util/concurrent/ConcurrentMap;

    .line 66
    new-instance v0, Lcom/google/b/c/fd;

    invoke-direct {v0}, Lcom/google/b/c/fd;-><init>()V

    .line 67
    sget-object v1, Lcom/google/b/c/gr;->c:Lcom/google/b/c/gr;

    invoke-virtual {v0, v1}, Lcom/google/b/c/fd;->b(Lcom/google/b/c/gr;)Lcom/google/b/c/fd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/fd;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/x/a;->f:Ljava/util/Map;

    .line 69
    new-instance v0, Lcom/google/android/apps/gmm/x/m;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/x/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/x/a;->c:Lcom/google/android/apps/gmm/x/m;

    .line 95
    iput-object p1, p0, Lcom/google/android/apps/gmm/x/a;->d:Lcom/google/android/apps/gmm/x/i;

    .line 96
    iput-object p2, p0, Lcom/google/android/apps/gmm/x/a;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    .line 97
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;)Lcom/google/android/apps/gmm/x/a;
    .locals 3

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/x/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/x/j;-><init>(Landroid/content/Context;)V

    .line 88
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    const/4 v2, 0x4

    invoke-static {v1, p1, p2, v2}, Lcom/google/android/apps/gmm/shared/c/a/a;->a(Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;Lcom/google/android/apps/gmm/shared/c/a/j;I)Lcom/google/android/apps/gmm/shared/c/a/a;

    .line 90
    new-instance v1, Lcom/google/android/apps/gmm/x/a;

    invoke-direct {v1, v0, p2}, Lcom/google/android/apps/gmm/x/a;-><init>(Lcom/google/android/apps/gmm/x/i;Lcom/google/android/apps/gmm/shared/c/a/j;)V

    return-object v1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/x/l;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/apps/gmm/x/l",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 328
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 338
    :cond_0
    :goto_0
    return-object v0

    .line 331
    :cond_1
    const-string v1, "-"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 332
    array-length v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 336
    :try_start_0
    new-instance v1, Lcom/google/android/apps/gmm/x/l;

    const/4 v3, 0x0

    aget-object v3, v2, v3

    const/4 v4, 0x1

    aget-object v2, v2, v4

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    invoke-direct {v1, v3, v2}, Lcom/google/android/apps/gmm/x/l;-><init>(Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 338
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/x/l;)Lcom/google/android/apps/gmm/x/o;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;)",
            "Lcom/google/android/apps/gmm/x/o",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 118
    const-string v0, "bundled"

    iget-object v1, p1, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 121
    if-eqz v0, :cond_2

    .line 131
    :cond_1
    :goto_0
    return-object v0

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->c:Lcom/google/android/apps/gmm/x/m;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/x/m;->a(Lcom/google/android/apps/gmm/x/l;)Z

    .line 125
    invoke-static {p1}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/l;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 126
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->b:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    .line 127
    if-nez v0, :cond_1

    .line 130
    iget-object v0, v1, Lcom/google/android/apps/gmm/x/o;->a:Lcom/google/android/apps/gmm/x/l;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v2, Lcom/google/android/apps/gmm/x/c;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/x/c;-><init>(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    move-object v0, v1

    .line 131
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Landroid/os/Bundle;",
            "Ljava/lang/String;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 172
    invoke-virtual {p1, p2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/h;

    .line 173
    if-nez v0, :cond_1

    .line 174
    const/4 v1, 0x0

    .line 185
    :cond_0
    :goto_0
    return-object v1

    .line 176
    :cond_1
    iget-object v1, v0, Lcom/google/android/apps/gmm/x/h;->c:Lcom/google/android/apps/gmm/x/o;

    .line 177
    if-nez v1, :cond_2

    .line 178
    iget-object v1, v0, Lcom/google/android/apps/gmm/x/h;->a:Lcom/google/android/apps/gmm/x/l;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/x/a;->a(Lcom/google/android/apps/gmm/x/l;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v1

    .line 179
    iput-object v1, v0, Lcom/google/android/apps/gmm/x/h;->c:Lcom/google/android/apps/gmm/x/o;

    .line 184
    :cond_2
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/x/h;->b:Z

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    goto :goto_0
.end method

.method public final a(Ljava/io/Serializable;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/io/Serializable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 297
    if-nez p1, :cond_0

    .line 298
    const-string v0, ""

    .line 302
    :goto_0
    return-object v0

    .line 300
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    .line 301
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/a;)V

    .line 302
    iget-object v0, v0, Lcom/google/android/apps/gmm/x/o;->a:Lcom/google/android/apps/gmm/x/l;

    iget-object v1, v0, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/apps/gmm/x/l;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xc

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V
    .locals 2

    .prologue
    .line 152
    instance-of v0, p3, Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_0

    .line 153
    check-cast p3, Lcom/google/android/apps/gmm/x/o;

    .line 154
    const/4 v0, 0x1

    .line 159
    :goto_0
    invoke-virtual {p3, p0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/a;)V

    .line 160
    new-instance v1, Lcom/google/android/apps/gmm/x/h;

    invoke-direct {v1, p3, v0}, Lcom/google/android/apps/gmm/x/h;-><init>(Lcom/google/android/apps/gmm/x/o;Z)V

    .line 161
    invoke-virtual {p1, p2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 162
    return-void

    .line 156
    :cond_0
    invoke-static {p3}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object p3

    .line 157
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/io/Serializable;",
            ">(",
            "Lcom/google/android/apps/gmm/x/l",
            "<TT;>;TT;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 355
    monitor-enter p0

    :try_start_0
    const-string v0, "bundled"

    iget-object v1, p1, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 357
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->c:Lcom/google/android/apps/gmm/x/m;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/x/m;->a(Lcom/google/android/apps/gmm/x/l;)Z

    .line 358
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;

    .line 359
    if-eqz v0, :cond_2

    if-eq v0, p2, :cond_2

    .line 360
    sget-object v1, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "ID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " : replacing from "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 363
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/x/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/gmm/x/b;-><init>(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 364
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b(Lcom/google/android/apps/gmm/x/l;)Ljava/io/Serializable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;)",
            "Ljava/io/Serializable;"
        }
    .end annotation

    .prologue
    .line 424
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->c:Lcom/google/android/apps/gmm/x/m;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/x/m;->a(Lcom/google/android/apps/gmm/x/l;)Z

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(Lcom/google/android/apps/gmm/x/l;Ljava/io/Serializable;)Ljava/io/Serializable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;",
            "Ljava/io/Serializable;",
            ")",
            "Ljava/io/Serializable;"
        }
    .end annotation

    .prologue
    .line 430
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    if-eqz v0, :cond_0

    .line 439
    :goto_0
    monitor-exit p0

    return-object v0

    .line 435
    :cond_0
    if-eqz p2, :cond_1

    .line 437
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    move-object v0, p2

    .line 439
    goto :goto_0

    .line 430
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c(Lcom/google/android/apps/gmm/x/l;)Ljava/io/Serializable;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;)",
            "Ljava/io/Serializable;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 485
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 487
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->d:Lcom/google/android/apps/gmm/x/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/x/i;->a(Lcom/google/android/apps/gmm/x/l;)[B

    move-result-object v0

    .line 488
    if-nez v0, :cond_0

    .line 489
    sget-object v0, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "No item is found for id "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v0, v1

    .line 502
    :goto_0
    return-object v0

    .line 494
    :cond_0
    :try_start_0
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v3, Lcom/google/android/apps/gmm/x/s;

    invoke-direct {v3, v2, p0}, Lcom/google/android/apps/gmm/x/s;-><init>(Ljava/io/InputStream;Lcom/google/android/apps/gmm/x/a;)V

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readUTF()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readByte()B

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v2, Lcom/google/android/apps/gmm/x/n;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/n;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/x/n;->a(Ljava/io/ObjectInputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 495
    :catch_0
    move-exception v0

    .line 496
    sget-object v2, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Failed to load item."

    invoke-direct {v3, v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 501
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->d:Lcom/google/android/apps/gmm/x/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/x/i;->b(Lcom/google/android/apps/gmm/x/l;)Z

    move-object v0, v1

    .line 502
    goto :goto_0

    .line 494
    :cond_1
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/Serializable;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public final declared-synchronized d(Lcom/google/android/apps/gmm/x/l;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/l",
            "<*>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 518
    monitor-enter p0

    :try_start_0
    const-string v0, "bundled"

    iget-object v1, p1, Lcom/google/android/apps/gmm/x/l;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 520
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->c:Lcom/google/android/apps/gmm/x/m;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/x/m;->b(Lcom/google/android/apps/gmm/x/l;)Z

    .line 523
    sget-object v0, Lcom/google/android/apps/gmm/x/a;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Serializable with id \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" removed from memory."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 524
    iget-object v0, p0, Lcom/google/android/apps/gmm/x/a;->e:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v1, Lcom/google/android/apps/gmm/x/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/x/d;-><init>(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/l;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->GMM_STORAGE:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 530
    monitor-exit p0

    return-void
.end method
