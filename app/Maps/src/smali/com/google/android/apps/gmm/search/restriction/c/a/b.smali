.class public Lcom/google/android/apps/gmm/search/restriction/c/a/b;
.super Lcom/google/android/apps/gmm/search/restriction/c/a/a;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 3

    .prologue
    .line 24
    sget v0, Lcom/google/android/apps/gmm/l;->lN:I

    .line 25
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->lN:I

    .line 26
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/libraries/curvular/c;->a(Ljava/lang/Integer;)Lcom/google/android/libraries/curvular/bi;

    move-result-object v1

    sget-object v2, Lcom/google/b/f/t;->E:Lcom/google/b/f/t;

    .line 24
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/apps/gmm/search/restriction/c/a/a;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/bi;Lcom/google/android/libraries/curvular/bi;Lcom/google/b/f/t;)V

    .line 28
    iget v0, p2, Lcom/google/maps/g/qm;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->c:Ljava/lang/Boolean;

    .line 29
    iget-object v0, p3, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sp;->d()Lcom/google/maps/g/sp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sp;

    iget v0, v0, Lcom/google/maps/g/sp;->b:I

    invoke-static {v0}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/qb;->a:Lcom/google/maps/g/qb;

    :cond_0
    sget-object v1, Lcom/google/maps/g/qb;->b:Lcom/google/maps/g/qb;

    invoke-virtual {v0, v1}, Lcom/google/maps/g/qb;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    .line 30
    return-void

    .line 28
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 3

    .prologue
    .line 50
    invoke-static {}, Lcom/google/maps/g/rk;->d()Lcom/google/maps/g/rk;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p1, Lcom/google/maps/g/qo;->d:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/maps/g/qo;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p1, Lcom/google/maps/g/qo;->a:I

    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 3

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/restriction/c/a/b;->ai_()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    invoke-static {}, Lcom/google/maps/g/sp;->newBuilder()Lcom/google/maps/g/sr;

    move-result-object v0

    sget-object v1, Lcom/google/maps/g/qb;->b:Lcom/google/maps/g/qb;

    .line 42
    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/sr;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/sr;->a:I

    iget v1, v1, Lcom/google/maps/g/qb;->d:I

    iput v1, v0, Lcom/google/maps/g/sr;->b:I

    invoke-virtual {v0}, Lcom/google/maps/g/sr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sp;

    .line 40
    invoke-virtual {p1, v0}, Lcom/google/r/b/a/alm;->a(Lcom/google/maps/g/sp;)Lcom/google/r/b/a/alm;

    move-result-object p1

    .line 45
    :cond_1
    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p1, Lcom/google/r/b/a/alh;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/sp;->d()Lcom/google/maps/g/sp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/sp;

    iget v0, v0, Lcom/google/maps/g/sp;->b:I

    invoke-static {v0}, Lcom/google/maps/g/qb;->a(I)Lcom/google/maps/g/qb;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/maps/g/qb;->a:Lcom/google/maps/g/qb;

    :cond_0
    sget-object v1, Lcom/google/maps/g/qb;->b:Lcom/google/maps/g/qb;

    invoke-virtual {v0, v1}, Lcom/google/maps/g/qb;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/w;->b:Ljava/lang/Boolean;

    .line 35
    return-void
.end method
