.class Lcom/google/android/apps/gmm/place/c/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/place/ae;

.field final b:Lcom/google/android/libraries/curvular/bc;

.field final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/place/ae;Lcom/google/android/libraries/curvular/bc;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/place/ae;

    iput-object p1, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    .line 87
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/libraries/curvular/bc;

    iput-object p2, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    .line 88
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/apps/gmm/place/c/a;->c:Landroid/content/Context;

    .line 89
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/apps/gmm/place/c/a;
    .locals 3

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->D()Lcom/google/android/apps/gmm/place/f/b/c;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->D()Lcom/google/android/apps/gmm/place/f/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/f/b/c;->d()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 439
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 440
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/place/f/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ae;->D()Lcom/google/android/apps/gmm/place/f/b/c;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 444
    :cond_2
    return-object p0

    .line 437
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 184
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->j()Lcom/google/android/apps/gmm/place/facepile/a/c;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    .line 185
    invoke-interface {v1}, Lcom/google/android/apps/gmm/place/ae;->j()Lcom/google/android/apps/gmm/place/facepile/a/c;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/place/facepile/a/c;->b(Ljava/lang/Integer;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method a(Ljava/util/List;Ljava/lang/Class;Z)Z
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/google/android/apps/gmm/place/d/i;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<TT;>;>;Z)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 393
    if-nez p1, :cond_1

    .line 406
    :cond_0
    return p3

    :cond_1
    move v1, v2

    .line 396
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 397
    if-eqz p3, :cond_2

    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v3, Lcom/google/android/apps/gmm/place/d/a/i;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 400
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v4, Lcom/google/android/apps/gmm/place/d/a/d;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    move p3, v2

    .line 403
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v3, Lcom/google/android/apps/gmm/place/d/a/h;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 404
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    invoke-virtual {v3, p2, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 396
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/place/c/a;
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->d()Lcom/google/android/apps/gmm/place/i/a;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ae;->d()Lcom/google/android/apps/gmm/place/i/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 193
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->v()Lcom/google/android/apps/gmm/place/cj;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    .line 194
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->v()Lcom/google/android/apps/gmm/place/cj;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/cj;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 198
    :goto_0
    if-eqz v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/place/cf;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ae;->v()Lcom/google/android/apps/gmm/place/cj;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 201
    :cond_1
    return-object p0

    .line 194
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()Z
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->p()Lcom/google/android/apps/gmm/base/l/v;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    .line 270
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->p()Lcom/google/android/apps/gmm/base/l/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/l/v;->k()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/place/c/a;
    .locals 3

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->B()Lcom/google/android/apps/gmm/place/i/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/c/a;->e()Lcom/google/android/apps/gmm/place/c/a;

    .line 412
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/bb;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ae;->B()Lcom/google/android/apps/gmm/place/i/h;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 414
    :cond_0
    return-object p0
.end method

.method public final e()Lcom/google/android/apps/gmm/place/c/a;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 475
    :cond_0
    return-object p0

    .line 472
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    const-class v2, Lcom/google/android/apps/gmm/base/f/v;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-class v2, Lcom/google/android/apps/gmm/base/f/t;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-class v2, Lcom/google/android/apps/gmm/place/d/a/g;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lcom/google/android/apps/gmm/place/c/a;
    .locals 3

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->n()Lcom/google/android/apps/gmm/place/k;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    .line 480
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->n()Lcom/google/android/apps/gmm/place/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/k;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/ba;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/place/ae;->n()Lcom/google/android/apps/gmm/place/k;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 483
    :cond_0
    return-object p0
.end method

.method public final g()Lcom/google/android/apps/gmm/place/c/a;
    .locals 3

    .prologue
    .line 490
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v1, Lcom/google/android/apps/gmm/base/f/cf;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 493
    :cond_0
    return-object p0

    .line 490
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/bc;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/b/a/an;

    iget-object v0, v0, Lcom/google/b/a/an;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Class;

    const-class v1, Lcom/google/android/apps/gmm/base/f/cf;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/apps/gmm/place/c/a;
    .locals 3

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/c/a;->a:Lcom/google/android/apps/gmm/place/ae;

    .line 531
    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/ae;->r()Lcom/google/android/apps/gmm/place/x;

    move-result-object v0

    .line 532
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/x;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 533
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/c/a;->b:Lcom/google/android/libraries/curvular/bc;

    const-class v2, Lcom/google/android/apps/gmm/place/c/m;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    .line 535
    :cond_0
    return-object p0
.end method
