.class public Lcom/google/android/apps/gmm/startpage/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/b/a;
.implements Lcom/google/android/apps/gmm/cardui/g/c;
.implements Lcom/google/android/apps/gmm/startpage/e/c;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field public final c:Lcom/google/android/apps/gmm/startpage/d/d;

.field public final d:Lcom/google/android/apps/gmm/cardui/c;

.field e:Ljava/lang/Runnable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/startpage/f/i;

.field private final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/startpage/d/c;",
            "Lcom/google/android/apps/gmm/cardui/c/b;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 88
    const-class v0, Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/cardui/a/d;Ljava/lang/Runnable;)V
    .locals 2
    .param p4    # Lcom/google/android/apps/gmm/cardui/a/d;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Ljava/lang/Runnable;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    invoke-static {}, Lcom/google/b/c/hj;->c()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->g:Ljava/util/Map;

    .line 121
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->f(Z)V

    .line 123
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 124
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 125
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    .line 126
    iput-object p5, p0, Lcom/google/android/apps/gmm/startpage/m;->e:Ljava/lang/Runnable;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->d(Ljava/lang/String;)V

    .line 130
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/cardui/s;

    sget-object v1, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    invoke-direct {v0, p2, v1, p4, p0}, Lcom/google/android/apps/gmm/cardui/s;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/cardui/b/b;Lcom/google/android/apps/gmm/cardui/a/d;Lcom/google/android/apps/gmm/cardui/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/startpage/f/i;->a(Lcom/google/android/apps/gmm/startpage/e/c;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    new-instance v1, Lcom/google/android/apps/gmm/startpage/n;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/startpage/n;-><init>(Lcom/google/android/apps/gmm/startpage/m;)V

    iput-object v1, v0, Lcom/google/android/apps/gmm/startpage/f/i;->c:Ljava/lang/Runnable;

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/m;->e()V

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->e()V

    .line 143
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;)Lcom/google/android/apps/gmm/startpage/m;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 173
    new-instance v3, Lcom/google/android/apps/gmm/startpage/f/i;

    invoke-direct {v3, p1, p0}, Lcom/google/android/apps/gmm/startpage/f/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/d/d;)V

    .line 175
    new-instance v0, Lcom/google/android/apps/gmm/startpage/m;

    move-object v1, p0

    move-object v2, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/m;-><init>(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/cardui/a/d;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method private a(Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/r/b/a/tf;)Lcom/google/b/c/cv;
    .locals 11
    .param p4    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/startpage/d/e;",
            "Lcom/google/r/b/a/tf;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/cardui/c/b;",
            ">;"
        }
    .end annotation

    .prologue
    .line 646
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    .line 648
    const/4 v2, 0x0

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v4

    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->e()Lcom/google/o/h/a/eq;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/startpage/d/d;->o()Lcom/google/o/h/a/en;

    move-result-object v6

    if-eqz v0, :cond_0

    if-nez v6, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->fM:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->n()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    const/4 v1, 0x1

    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v7

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v6}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    invoke-virtual {v6}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v0

    const/4 v8, 0x0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sx;

    iget-boolean v0, v0, Lcom/google/o/h/a/sx;->e:Z

    if-nez v0, :cond_3

    invoke-virtual {v6}, Lcom/google/o/h/a/en;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sx;

    iget-boolean v0, v0, Lcom/google/o/h/a/sx;->e:Z

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/startpage/r;->b:[I

    invoke-virtual {v1}, Lcom/google/o/h/a/dq;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    invoke-virtual {v7}, Lcom/google/o/h/a/gr;->c()V

    iget-object v8, v7, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v8, v0}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    sget-object v8, Lcom/google/android/apps/gmm/cardui/e/d;->c:Lcom/google/o/h/a/hv;

    invoke-virtual {v7, v8}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/gr;

    move-result-object v7

    invoke-static {}, Lcom/google/maps/g/a/je;->newBuilder()Lcom/google/maps/g/a/jg;

    move-result-object v8

    if-eqz v6, :cond_6

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v9

    if-eqz v9, :cond_6

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v6

    invoke-virtual {v8, v6}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/gy;)Lcom/google/maps/g/a/jg;

    :cond_6
    if-eqz v0, :cond_8

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v6, v8, Lcom/google/maps/g/a/jg;->a:I

    or-int/lit8 v6, v6, 0x10

    iput v6, v8, Lcom/google/maps/g/a/jg;->a:I

    iput-object v0, v8, Lcom/google/maps/g/a/jg;->d:Ljava/lang/Object;

    :cond_8
    sget-object v0, Lcom/google/maps/g/a/jh;->a:Lcom/google/maps/g/a/jh;

    invoke-virtual {v8, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jh;)Lcom/google/maps/g/a/jg;

    sget-object v0, Lcom/google/maps/g/a/jj;->d:Lcom/google/maps/g/a/jj;

    invoke-virtual {v8, v0}, Lcom/google/maps/g/a/jg;->a(Lcom/google/maps/g/a/jj;)Lcom/google/maps/g/a/jg;

    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v0

    invoke-static {}, Lcom/google/o/h/a/se;->newBuilder()Lcom/google/o/h/a/sg;

    move-result-object v6

    iget-object v9, v6, Lcom/google/o/h/a/sg;->b:Lcom/google/n/ao;

    invoke-virtual {v8}, Lcom/google/maps/g/a/jg;->g()Lcom/google/n/t;

    move-result-object v8

    iget-object v10, v9, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v8, v9, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v8, 0x0

    iput-object v8, v9, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v8, 0x1

    iput-boolean v8, v9, Lcom/google/n/ao;->d:Z

    iget v8, v6, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v6, Lcom/google/o/h/a/sg;->a:I

    iget-object v8, v0, Lcom/google/o/h/a/c;->e:Lcom/google/n/ao;

    invoke-virtual {v6}, Lcom/google/o/h/a/sg;->g()Lcom/google/n/t;

    move-result-object v6

    iget-object v9, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v8, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v6, 0x0

    iput-object v6, v8, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v6, 0x1

    iput-boolean v6, v8, Lcom/google/n/ao;->d:Z

    iget v6, v0, Lcom/google/o/h/a/c;->a:I

    const/high16 v8, 0x100000

    or-int/2addr v6, v8

    iput v6, v0, Lcom/google/o/h/a/c;->a:I

    invoke-virtual {v0}, Lcom/google/o/h/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-virtual {v7, v0}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/gr;

    move-result-object v0

    const/4 v6, 0x0

    sget-object v7, Lcom/google/b/f/t;->fT:Lcom/google/b/f/t;

    invoke-static {v2, v6, v7}, Lcom/google/android/apps/gmm/cardui/e/a;->a(IILcom/google/b/f/t;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    iget v6, v0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v6, v6, 0x200

    iput v6, v0, Lcom/google/o/h/a/gr;->a:I

    iput-object v2, v0, Lcom/google/o/h/a/gr;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v2

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/it;

    move-result-object v0

    iget-object v6, v2, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v6, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/o/h/a/ix;->a:I

    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v0

    sget-object v6, Lcom/google/o/h/a/ji;->b:Lcom/google/o/h/a/ji;

    if-nez v6, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_a
    iget v7, v0, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v0, Lcom/google/o/h/a/jh;->a:I

    iget v6, v6, Lcom/google/o/h/a/ji;->ah:I

    iput v6, v0, Lcom/google/o/h/a/jh;->b:I

    invoke-virtual {v2}, Lcom/google/o/h/a/ix;->c()V

    iget-object v6, v2, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/o/h/a/jh;->g()Lcom/google/n/t;

    move-result-object v0

    new-instance v7, Lcom/google/n/ao;

    invoke-direct {v7}, Lcom/google/n/ao;-><init>()V

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/google/n/ao;->d:Z

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    invoke-virtual {v5, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :goto_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/m;->g()Z

    move-result v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->fh:I

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/o/h/a/se;->newBuilder()Lcom/google/o/h/a/sg;

    move-result-object v0

    const/4 v6, 0x1

    iget v7, v0, Lcom/google/o/h/a/sg;->a:I

    or-int/lit8 v7, v7, 0x2

    iput v7, v0, Lcom/google/o/h/a/sg;->a:I

    iput-boolean v6, v0, Lcom/google/o/h/a/sg;->c:Z

    invoke-virtual {v0}, Lcom/google/o/h/a/sg;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/se;

    invoke-static {}, Lcom/google/o/h/a/a;->newBuilder()Lcom/google/o/h/a/c;

    move-result-object v6

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    iget-object v7, v6, Lcom/google/o/h/a/c;->e:Lcom/google/n/ao;

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/google/n/ao;->d:Z

    iget v0, v6, Lcom/google/o/h/a/c;->a:I

    const/high16 v7, 0x100000

    or-int/2addr v0, v7

    iput v0, v6, Lcom/google/o/h/a/c;->a:I

    invoke-virtual {v6}, Lcom/google/o/h/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    invoke-static {}, Lcom/google/o/h/a/gp;->newBuilder()Lcom/google/o/h/a/gr;

    move-result-object v6

    if-nez v2, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    invoke-virtual {v6}, Lcom/google/o/h/a/gr;->c()V

    iget-object v7, v6, Lcom/google/o/h/a/gr;->b:Lcom/google/n/aq;

    invoke-interface {v7, v2}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/apps/gmm/cardui/e/d;->f:Lcom/google/o/h/a/hv;

    invoke-virtual {v6, v2}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/hv;)Lcom/google/o/h/a/gr;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/o/h/a/gr;->a(Lcom/google/o/h/a/a;)Lcom/google/o/h/a/gr;

    move-result-object v0

    const/4 v2, 0x0

    sget-object v6, Lcom/google/b/f/t;->fR:Lcom/google/b/f/t;

    invoke-static {v1, v2, v6}, Lcom/google/android/apps/gmm/cardui/e/a;->a(IILcom/google/b/f/t;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    iget v2, v0, Lcom/google/o/h/a/gr;->a:I

    or-int/lit16 v2, v2, 0x200

    iput v2, v0, Lcom/google/o/h/a/gr;->a:I

    iput-object v1, v0, Lcom/google/o/h/a/gr;->h:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/o/h/a/gr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gp;

    invoke-static {}, Lcom/google/o/h/a/iv;->newBuilder()Lcom/google/o/h/a/ix;

    move-result-object v1

    invoke-static {}, Lcom/google/o/h/a/ir;->newBuilder()Lcom/google/o/h/a/it;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/o/h/a/it;->a(Lcom/google/o/h/a/gp;)Lcom/google/o/h/a/it;

    move-result-object v0

    iget-object v2, v1, Lcom/google/o/h/a/ix;->b:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/o/h/a/it;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v6, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v2, Lcom/google/n/ao;->d:Z

    iget v0, v1, Lcom/google/o/h/a/ix;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/o/h/a/ix;->a:I

    invoke-static {}, Lcom/google/o/h/a/jf;->newBuilder()Lcom/google/o/h/a/jh;

    move-result-object v0

    sget-object v2, Lcom/google/o/h/a/ji;->b:Lcom/google/o/h/a/ji;

    if-nez v2, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    iget v6, v0, Lcom/google/o/h/a/jh;->a:I

    or-int/lit8 v6, v6, 0x1

    iput v6, v0, Lcom/google/o/h/a/jh;->a:I

    iget v2, v2, Lcom/google/o/h/a/ji;->ah:I

    iput v2, v0, Lcom/google/o/h/a/jh;->b:I

    invoke-virtual {v1}, Lcom/google/o/h/a/ix;->c()V

    iget-object v2, v1, Lcom/google/o/h/a/ix;->c:Ljava/util/List;

    invoke-virtual {v0}, Lcom/google/o/h/a/jh;->g()Lcom/google/n/t;

    move-result-object v0

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/o/h/a/ix;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    invoke-virtual {v5, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_f
    invoke-virtual {v5}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v1

    if-lez v1, :cond_11

    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/c;

    sget-object v2, Lcom/google/o/h/a/lk;->Z:Lcom/google/o/h/a/lk;

    iget v2, v2, Lcom/google/o/h/a/lk;->aN:I

    const-wide/16 v6, 0x0

    invoke-direct {v1, v2, v6, v7}, Lcom/google/android/apps/gmm/startpage/d/c;-><init>(IJ)V

    invoke-static {}, Lcom/google/o/h/a/br;->newBuilder()Lcom/google/o/h/a/bt;

    move-result-object v2

    sget-object v5, Lcom/google/o/h/a/bw;->b:Lcom/google/o/h/a/bw;

    if-nez v5, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    iget v6, v2, Lcom/google/o/h/a/bt;->a:I

    or-int/lit8 v6, v6, 0x2

    iput v6, v2, Lcom/google/o/h/a/bt;->a:I

    iget v5, v5, Lcom/google/o/h/a/bw;->f:I

    iput v5, v2, Lcom/google/o/h/a/bt;->c:I

    invoke-virtual {v2, v0}, Lcom/google/o/h/a/bt;->a(Ljava/lang/Iterable;)Lcom/google/o/h/a/bt;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/bt;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/android/apps/gmm/startpage/d/c;Ljava/util/List;)Lcom/google/o/h/a/lh;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_11
    invoke-virtual {v4}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    .line 653
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    monitor-enter v1

    .line 654
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    .line 655
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->e()Lcom/google/o/h/a/eq;

    .line 656
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->b()Z

    .line 657
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 659
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 660
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 661
    invoke-interface {v1, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 662
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    .line 663
    new-instance v2, Lcom/google/android/apps/gmm/startpage/d/b;

    invoke-direct {v2, v0, p2, p4}, Lcom/google/android/apps/gmm/startpage/d/b;-><init>(Lcom/google/o/h/a/lh;Ljava/lang/String;Lcom/google/r/b/a/tf;)V

    .line 665
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, p3, v2}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/android/apps/gmm/startpage/d/b;)V

    .line 666
    invoke-static {v2}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/android/apps/gmm/startpage/d/b;)Lcom/google/android/apps/gmm/cardui/c/b;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_2

    .line 657
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 669
    :cond_12
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0

    :cond_13
    move v1, v2

    goto/16 :goto_1

    .line 648
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lcom/google/b/c/cv;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/cardui/c/b;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 404
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 407
    invoke-virtual {p1}, Lcom/google/b/c/cv;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    :cond_0
    return-void

    .line 411
    :cond_1
    invoke-virtual {p1}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/c/b;

    .line 412
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/apps/gmm/cardui/c/b;)Z

    move-result v1

    .line 413
    if-eqz v1, :cond_2

    .line 415
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    const-string v1, "Pending requests weren\'t sent: %s"

    new-array v5, v2, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/startpage/d/d;->h()Ljava/util/Set;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v0, v1, v5}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/cardui/c/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/b;

    iget-object v6, v0, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    invoke-virtual {v6}, Lcom/google/o/h/a/lh;->i()Ljava/util/List;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/d/b;->a:Lcom/google/o/h/a/lh;

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->i()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_5
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/oh;

    iget-object v1, v0, Lcom/google/o/h/a/oh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/a;

    iget v1, v1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v6, 0x4

    if-ne v1, v6, :cond_7

    move v1, v2

    :goto_3
    if-eqz v1, :cond_6

    iget-object v0, v0, Lcom/google/o/h/a/oh;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/a;->d()Lcom/google/o/h/a/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/a;

    iget-object v0, v0, Lcom/google/o/h/a/a;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/kh;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    const-string v1, "Prefetch request will be sent: requestToken="

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_8

    invoke-virtual {v1, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v1

    iget v6, v0, Lcom/google/o/h/a/kh;->e:I

    invoke-static {v6}, Lcom/google/o/h/a/dq;->a(I)Lcom/google/o/h/a/dq;

    move-result-object v6

    invoke-static {v6}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/gmm/startpage/af;->c:Lcom/google/android/apps/gmm/startpage/af;

    new-instance v8, Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Lcom/google/android/apps/gmm/startpage/d/e;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v1, v6, v7, v8, v0}, Lcom/google/android/apps/gmm/startpage/aa;->a(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)Lcom/google/android/apps/gmm/startpage/ac;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->l()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v1

    iget-object v6, v0, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v1, v6, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->m()Lcom/google/o/b/a/v;

    move-result-object v1

    iget-object v6, v0, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v1, v6, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->o()Lcom/google/o/h/a/en;

    move-result-object v1

    iget-object v6, v0, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v1, v6, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v1

    invoke-interface {v1, v0, v3}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/android/apps/gmm/startpage/a/d;Z)V

    goto/16 :goto_2

    :cond_7
    move v1, v3

    goto/16 :goto_3

    :cond_8
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method

.method private a(Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/cardui/c/a;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 546
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/apps/gmm/cardui/c/a;

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 550
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v7

    .line 551
    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 552
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/startpage/a/e;->c()Lcom/google/android/apps/gmm/startpage/a/c;

    move-result-object v0

    .line 554
    :goto_2
    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/cardui/c/a;->a(Lcom/google/android/apps/gmm/startpage/a/c;)Ljava/util/List;

    move-result-object v2

    .line 558
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->y()I

    move-result v0

    if-le v0, v7, :cond_2

    move v4, v7

    .line 559
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    .line 560
    iget-object v1, v6, Lcom/google/android/apps/gmm/cardui/c/a;->a:Lcom/google/o/h/a/br;

    iget-object v3, v6, Lcom/google/android/apps/gmm/cardui/c/a;->c:Ljava/lang/String;

    .line 561
    iget-object v5, v6, Lcom/google/android/apps/gmm/cardui/c/a;->d:Lcom/google/r/b/a/tf;

    .line 559
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/c;->a(Lcom/google/o/h/a/br;Ljava/util/List;Ljava/lang/String;ZLcom/google/r/b/a/tf;)Lcom/google/android/apps/gmm/cardui/e/c;

    move-result-object v0

    .line 562
    iput-object v0, v6, Lcom/google/android/apps/gmm/cardui/c/a;->e:Lcom/google/android/apps/gmm/cardui/e/c;

    goto :goto_0

    :cond_0
    move v0, v8

    .line 550
    goto :goto_1

    .line 552
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    :cond_2
    move v4, v8

    .line 558
    goto :goto_3

    .line 564
    :cond_3
    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;ZLcom/google/r/b/a/tf;)V
    .locals 7
    .param p7    # Lcom/google/r/b/a/tf;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;",
            "Ljava/lang/String;",
            "J",
            "Lcom/google/android/apps/gmm/startpage/d/e;",
            "Z",
            "Lcom/google/r/b/a/tf;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 344
    sget-object v0, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x59

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "OdelayContentPresenter.handleOdelayResponseProto(), with requestToken="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFromCache="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 346
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v2, :cond_1

    :cond_0
    move v0, v4

    :goto_0
    if-eqz v0, :cond_5

    .line 347
    sget-object v2, Lcom/google/android/apps/gmm/cardui/b/b;->c:Lcom/google/android/apps/gmm/cardui/b/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/apps/gmm/startpage/u;->a(Ljava/util/List;)Lcom/google/o/h/a/nt;

    move-result-object v5

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->h()Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v6

    new-instance v0, Lcom/google/android/apps/gmm/startpage/p;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/p;-><init>(Lcom/google/android/apps/gmm/startpage/m;Lcom/google/android/apps/gmm/cardui/b/b;Ljava/lang/String;Ljava/util/List;Lcom/google/o/h/a/nt;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v6, v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 360
    :goto_1
    return-void

    .line 346
    :cond_1
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    iget v1, v0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v3, 0x100

    if-ne v1, v3, :cond_3

    move v1, v2

    :goto_2
    if-eqz v1, :cond_4

    iget-object v1, v0, Lcom/google/o/h/a/lh;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/nt;->d()Lcom/google/o/h/a/nt;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/nt;

    iget v1, v1, Lcom/google/o/h/a/nt;->b:I

    invoke-static {v1}, Lcom/google/o/h/a/oa;->a(I)Lcom/google/o/h/a/oa;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/o/h/a/oa;->a:Lcom/google/o/h/a/oa;

    :cond_2
    sget-object v3, Lcom/google/o/h/a/oa;->a:Lcom/google/o/h/a/oa;

    if-eq v1, v3, :cond_4

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->h()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_2

    :cond_4
    move v0, v4

    goto :goto_0

    .line 351
    :cond_5
    invoke-direct {p0, p1, p2, p5, p7}, Lcom/google/android/apps/gmm/startpage/m;->a(Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/r/b/a/tf;)Lcom/google/b/c/cv;

    move-result-object v1

    .line 353
    sget-object v0, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    const-string v3, "Adding %d card groups from %s"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/b/c/cv;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v4

    if-eqz p6, :cond_7

    const-string v0, "cache"

    :goto_3
    aput-object v0, v5, v2

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 355
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/b/c/cv;)V

    .line 357
    if-eqz p1, :cond_6

    invoke-static {p1}, Lcom/google/android/apps/gmm/startpage/u;->a(Ljava/util/List;)Lcom/google/o/h/a/nt;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/nt;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iput-object v0, v1, Lcom/google/android/apps/gmm/cardui/c;->f:Lcom/google/o/h/a/nt;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->v()Lcom/google/o/h/a/nt;

    move-result-object v1

    if-eqz v1, :cond_6

    iget v0, v1, Lcom/google/o/h/a/nt;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v3, 0x20

    if-ne v0, v3, :cond_8

    move v0, v2

    :goto_4
    if-eqz v0, :cond_6

    iget-object v0, v1, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 358
    :cond_6
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/gmm/startpage/f/i;->a(Ljava/util/List;Ljava/lang/String;)V

    .line 359
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->D()V

    goto/16 :goto_1

    .line 353
    :cond_7
    const-string v0, "server"

    goto :goto_3

    :cond_8
    move v0, v4

    .line 357
    goto :goto_4

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    const/4 v3, 0x0

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/t;

    iget-object v0, v1, Lcom/google/o/h/a/nt;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/jy;->g()Lcom/google/maps/g/jy;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/jy;

    invoke-virtual {v0}, Lcom/google/maps/g/jy;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/at;

    sget-object v1, Lcom/google/maps/b/b/c;->a:Lcom/google/e/a/a/a/d;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/map/b/a/t;-><init>(Lcom/google/e/a/a/a/b;)V

    invoke-virtual {v2, v3, v5}, Lcom/google/android/apps/gmm/map/t;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/t;)V

    goto :goto_5
.end method

.method private a(Lcom/google/android/apps/gmm/cardui/c/b;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 501
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->a()Ljava/util/List;

    move-result-object v4

    .line 502
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 542
    :goto_0
    return v0

    .line 506
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->b()Lcom/google/android/apps/gmm/startpage/d/c;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/m;->g:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/cardui/c/b;

    .line 507
    if-eqz v0, :cond_7

    .line 508
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->c()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->d()Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v2

    :goto_1
    if-eqz v3, :cond_2

    move v0, v1

    .line 510
    goto :goto_0

    :cond_1
    move v3, v1

    .line 508
    goto :goto_1

    .line 512
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->c()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 513
    sget-object v1, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    .line 514
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/cardui/c/b;->b(Lcom/google/android/apps/gmm/cardui/c/b;)V

    .line 534
    :goto_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/cardui/c/b;->b:Lcom/google/o/h/a/a;

    .line 535
    if-eqz v0, :cond_4

    .line 536
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->i()Lcom/google/o/h/a/a;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 537
    sget-object v1, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    .line 539
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/a;)V

    :cond_4
    move v0, v2

    .line 542
    goto :goto_0

    .line 515
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 516
    sget-object v1, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    .line 517
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/cardui/c/b;->a(Lcom/google/android/apps/gmm/cardui/c/b;)V

    .line 521
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/startpage/m;->a(Ljava/util/List;)V

    .line 523
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->b()Lcom/google/android/apps/gmm/startpage/d/c;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 525
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    move v0, v1

    .line 526
    goto :goto_0

    .line 530
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/cardui/c/b;->b()Lcom/google/android/apps/gmm/startpage/d/c;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 531
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/startpage/m;->a(Ljava/util/List;)V

    goto :goto_2
.end method

.method private f()V
    .locals 2

    .prologue
    .line 589
    const/4 v0, 0x0

    .line 591
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->s()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 592
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->u()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 593
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->t()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 594
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->h()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 595
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->i()Lcom/google/o/h/a/a;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 596
    :cond_1
    const/4 v0, 0x1

    .line 599
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/startpage/f/i;->f:Z

    .line 600
    return-void
.end method

.method private g()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 769
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    monitor-enter v1

    .line 773
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->b()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    sget-object v3, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/e;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 775
    :cond_0
    monitor-exit v1

    .line 784
    :goto_0
    return v0

    .line 777
    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/startpage/r;->b:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/o/h/a/dq;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 784
    monitor-exit v1

    goto :goto_0

    .line 786
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 782
    :pswitch_0
    const/4 v0, 0x1

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 777
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 827
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->m()Lcom/google/o/b/a/v;

    move-result-object v0

    if-nez v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 831
    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/b/a/v;)V

    .line 834
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/m;->e()V

    .line 835
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/startpage/m;->a(Z)V

    .line 836
    return-void

    .line 829
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 830
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    goto :goto_0

    .line 831
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/b/a/v;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 814
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    monitor-enter v1

    .line 815
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->g()Lcom/google/android/apps/gmm/startpage/d/e;

    move-result-object v0

    .line 816
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/a;)V

    .line 817
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->r()V

    .line 818
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->f()V

    .line 819
    if-eqz v0, :cond_0

    .line 820
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 822
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 823
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/m;->h()V

    .line 824
    return-void

    .line 822
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/base/e/c;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 851
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->u()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v1, :cond_1

    move-object v1, v0

    :goto_0
    if-eq v2, v1, :cond_0

    if-eqz v2, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_4

    .line 852
    sget-object v1, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    .line 853
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    if-nez v2, :cond_3

    :goto_2
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->d(Ljava/lang/String;)V

    .line 854
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/m;->a()V

    .line 859
    :goto_3
    return-void

    .line 851
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 853
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/e/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    goto :goto_2

    .line 857
    :cond_4
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/m;->h()V

    goto :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 866
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/b/a/v;)V

    .line 867
    :goto_0
    return-void

    .line 866
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/b/a/v;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/startpage/b/c;)V
    .locals 14
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/c;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/b/c;->e:Lcom/google/o/h/a/dq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 300
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->d()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/c;->i:Ljava/lang/String;

    if-eq v0, v1, :cond_0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 301
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->b()Z

    move-result v0

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/startpage/b/c;->f:Z

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 302
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->e()Lcom/google/o/h/a/eq;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/c;->g:Lcom/google/o/h/a/eq;

    if-eq v0, v1, :cond_3

    .line 338
    :cond_1
    :goto_1
    return-void

    .line 300
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 306
    :cond_3
    iget-object v6, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    iget-object v7, p1, Lcom/google/android/apps/gmm/startpage/b/c;->k:Lcom/google/android/apps/gmm/shared/net/k;

    if-eqz v7, :cond_16

    iget-object v1, v6, Lcom/google/android/apps/gmm/startpage/f/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v6, Lcom/google/android/apps/gmm/startpage/f/i;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v4

    sget-object v0, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    if-eq v4, v0, :cond_5

    sget-object v0, Lcom/google/o/h/a/dq;->c:Lcom/google/o/h/a/dq;

    if-eq v4, v0, :cond_5

    sget-object v0, Lcom/google/o/h/a/dq;->f:Lcom/google/o/h/a/dq;

    if-eq v4, v0, :cond_5

    sget-object v0, Lcom/google/o/h/a/dq;->e:Lcom/google/o/h/a/dq;

    if-eq v4, v0, :cond_5

    const/4 v0, 0x0

    :goto_2
    iput-object v0, v6, Lcom/google/android/apps/gmm/startpage/f/i;->j:Lcom/google/android/apps/gmm/startpage/e/l;

    :goto_3
    iget-object v0, v6, Lcom/google/android/apps/gmm/startpage/f/i;->h:Lcom/google/android/apps/gmm/shared/net/k;

    if-eq v0, v7, :cond_4

    iput-object v7, v6, Lcom/google/android/apps/gmm/startpage/f/i;->h:Lcom/google/android/apps/gmm/shared/net/k;

    if-nez v7, :cond_17

    const/4 v0, 0x0

    iput-object v0, v6, Lcom/google/android/apps/gmm/startpage/f/i;->i:Lcom/google/android/apps/gmm/startpage/e/g;

    .line 308
    :cond_4
    :goto_4
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/b/c;->k:Lcom/google/android/apps/gmm/shared/net/k;

    if-eqz v0, :cond_19

    .line 310
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    iget-object v6, p1, Lcom/google/android/apps/gmm/startpage/b/c;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 311
    iget-boolean v7, p1, Lcom/google/android/apps/gmm/startpage/b/c;->h:Z

    .line 312
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/startpage/b/c;->j:Z

    if-eqz v0, :cond_18

    sget-object v8, Lcom/google/r/b/a/tf;->d:Lcom/google/r/b/a/tf;

    :goto_5
    move-object v1, p0

    .line 310
    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/startpage/m;->a(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;ZLcom/google/r/b/a/tf;)V

    .line 329
    :goto_6
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/m;->f()V

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->e()V

    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_1

    .line 306
    :cond_5
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    invoke-static {v2, v3, v0}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/shared/net/ad;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/shared/b/c;Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/android/apps/gmm/directions/f/c/b;

    move-result-object v5

    if-nez v5, :cond_7

    const/4 v0, 0x0

    goto :goto_2

    :cond_7
    invoke-static {v5, v1}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/directions/f/c/b;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v3

    if-nez v3, :cond_8

    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/startpage/f/u;->a:[I

    invoke-virtual {v4}, Lcom/google/o/h/a/dq;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    const/4 v0, 0x1

    :goto_7
    if-nez v0, :cond_c

    const/4 v0, 0x0

    goto/16 :goto_2

    :pswitch_0
    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v0, v2, :cond_9

    const/4 v0, 0x1

    goto :goto_7

    :cond_9
    const/4 v0, 0x0

    goto :goto_7

    :pswitch_1
    sget-object v2, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v0, v2, :cond_a

    const/4 v0, 0x1

    goto :goto_7

    :cond_a
    const/4 v0, 0x0

    goto :goto_7

    :pswitch_2
    sget-object v2, Lcom/google/maps/g/a/hm;->b:Lcom/google/maps/g/a/hm;

    if-ne v0, v2, :cond_b

    const/4 v0, 0x1

    goto :goto_7

    :cond_b
    const/4 v0, 0x0

    goto :goto_7

    :cond_c
    iget-object v0, v5, Lcom/google/android/apps/gmm/directions/f/c/b;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agd;->d()Lcom/google/r/b/a/agd;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/agd;

    iget-object v0, v5, Lcom/google/android/apps/gmm/directions/f/c/b;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/agl;->d()Lcom/google/r/b/a/agl;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    iget-object v4, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v4, :cond_d

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_8
    iget-object v4, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v4, :cond_e

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    :goto_9
    iget-object v8, v0, Lcom/google/r/b/a/afb;->b:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    const/4 v9, 0x2

    if-lt v4, v9, :cond_f

    const/4 v4, 0x1

    :goto_a
    if-nez v4, :cond_10

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_d
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_8

    :cond_e
    iget-object v0, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto :goto_9

    :cond_f
    const/4 v4, 0x0

    goto :goto_a

    :cond_10
    iget v0, v0, Lcom/google/r/b/a/afb;->d:I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a(Lcom/google/android/apps/gmm/map/r/a/f;Landroid/content/Context;I)Lcom/google/android/apps/gmm/map/r/a/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->a()Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v9

    if-nez v9, :cond_11

    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_11
    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->e()Lcom/google/android/apps/gmm/shared/net/a/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/net/a/l;->a:Lcom/google/r/b/a/ou;

    iget v0, v0, Lcom/google/r/b/a/ou;->aa:I

    int-to-long v10, v0

    invoke-virtual {v4, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v10

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v12

    iget-wide v4, v5, Lcom/google/android/apps/gmm/directions/f/c/b;->e:J

    sub-long v4, v12, v4

    cmp-long v0, v4, v10

    if-lez v0, :cond_12

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/maps/g/a/fw;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/e;

    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-direct {v4, v9}, Lcom/google/android/apps/gmm/map/r/a/ae;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;)V

    invoke-static {v4}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/ae;)Lcom/google/r/b/a/agl;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/r/a/e;-><init>(Lcom/google/r/b/a/agl;)V

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/map/r/a/f;->a(Lcom/google/android/apps/gmm/map/r/a/e;)Lcom/google/android/apps/gmm/map/r/a/f;

    move-result-object v3

    :cond_12
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/jm;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/map/r/a/as;->a(Landroid/content/res/Resources;Lcom/google/maps/g/a/jm;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v5

    if-nez v5, :cond_13

    const/4 v0, 0x0

    :goto_b
    if-nez v0, :cond_15

    :goto_c
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    iget-object v5, v9, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    invoke-static {v1, v0, v5}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Landroid/content/Context;Lcom/google/maps/g/a/fz;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/google/android/apps/gmm/startpage/f/t;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/f/t;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/r/b/a/agd;Lcom/google/android/apps/gmm/map/r/a/f;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :cond_13
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v10

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/b/a;->getLongitude()D

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v8

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/d/c;->a(Lcom/google/android/apps/gmm/shared/net/ad;)I

    move-result v0

    int-to-double v10, v0

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/r/b/a;->getLatitude()D

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v12

    mul-double/2addr v10, v12

    invoke-virtual {v9, v8, v10, v11}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;D)D

    move-result-wide v10

    const-wide/high16 v12, -0x4010000000000000L    # -1.0

    cmpl-double v0, v10, v12

    if-nez v0, :cond_14

    const/4 v0, 0x0

    goto :goto_b

    :cond_14
    double-to-int v0, v10

    sget-object v5, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    new-instance v8, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v8}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    invoke-static {v1, v0, v5, v8}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_b

    :cond_15
    new-instance v5, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v5, v1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    iget-object v8, v9, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v8, v10, v11}, Lcom/google/android/apps/gmm/directions/f/d/h;->a(Lcom/google/maps/g/a/fz;IZ)I

    move-result v8

    sget v10, Lcom/google/android/apps/gmm/l;->lU:I

    new-instance v11, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v12, v5, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v12, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v11, v5, v10}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v12, 0x0

    new-instance v13, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v13, v5, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    invoke-virtual {v13, v8}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(I)Lcom/google/android/apps/gmm/shared/c/c/i;

    move-result-object v0

    aput-object v0, v10, v12

    const/4 v0, 0x1

    aput-object v4, v10, v0

    invoke-virtual {v11, v10}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    const-string v4, "%s"

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    goto/16 :goto_c

    :cond_16
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/google/android/apps/gmm/startpage/f/i;->j:Lcom/google/android/apps/gmm/startpage/e/l;

    goto/16 :goto_3

    :cond_17
    new-instance v1, Lcom/google/android/apps/gmm/startpage/f/l;

    iget-object v2, v6, Lcom/google/android/apps/gmm/startpage/f/i;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget-object v0, Lcom/google/android/apps/gmm/startpage/r;->a:[I

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/shared/net/k;->ordinal()I

    move-result v3

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    sget v0, Lcom/google/android/apps/gmm/l;->cg:I

    :goto_d
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v6, v7, v0}, Lcom/google/android/apps/gmm/startpage/f/l;-><init>(Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/apps/gmm/shared/net/k;Ljava/lang/CharSequence;)V

    iput-object v1, v6, Lcom/google/android/apps/gmm/startpage/f/i;->i:Lcom/google/android/apps/gmm/startpage/e/g;

    goto/16 :goto_4

    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/l;->ci:I

    goto :goto_d

    :pswitch_4
    sget v0, Lcom/google/android/apps/gmm/l;->ch:I

    goto :goto_d

    .line 312
    :cond_18
    const/4 v8, 0x0

    goto/16 :goto_5

    .line 317
    :cond_19
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/startpage/b/c;->h:Z

    if-nez v0, :cond_1a

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/b/c;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->d(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 320
    :cond_1a
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/b/c;->a:Ljava/util/List;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/b/c;->b:Ljava/lang/String;

    iget-wide v4, p1, Lcom/google/android/apps/gmm/startpage/b/c;->c:J

    .line 321
    iget-object v6, p1, Lcom/google/android/apps/gmm/startpage/b/c;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-boolean v7, p1, Lcom/google/android/apps/gmm/startpage/b/c;->h:Z

    .line 322
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/startpage/b/c;->j:Z

    if-eqz v0, :cond_1b

    sget-object v8, Lcom/google/r/b/a/tf;->d:Lcom/google/r/b/a/tf;

    :goto_e
    move-object v1, p0

    .line 320
    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/startpage/m;->a(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;ZLcom/google/r/b/a/tf;)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->h()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    goto/16 :goto_6

    .line 322
    :cond_1b
    const/4 v8, 0x0

    goto :goto_e

    .line 306
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method a(Lcom/google/android/apps/gmm/startpage/d/e;)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/startpage/d/d;->c(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 283
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/startpage/m;->a(Z)V

    .line 284
    return-void
.end method

.method public final a(Lcom/google/android/libraries/curvular/ag;)V
    .locals 2
    .param p1    # Lcom/google/android/libraries/curvular/ag;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/startpage/e/f;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    .line 148
    new-instance v1, Lcom/google/android/apps/gmm/startpage/o;

    invoke-direct {v1, v0, p1}, Lcom/google/android/apps/gmm/startpage/o;-><init>(Lcom/google/android/apps/gmm/startpage/f/i;Lcom/google/android/libraries/curvular/ag;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    check-cast v0, Lcom/google/android/apps/gmm/cardui/s;

    iput-object v1, v0, Lcom/google/android/apps/gmm/cardui/s;->i:Lcom/google/android/libraries/curvular/ag;

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->e()V

    .line 151
    return-void
.end method

.method public final a(Lcom/google/o/h/a/hl;)V
    .locals 3

    .prologue
    .line 901
    iget-object v0, p1, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/o/h/a/kh;->h()Lcom/google/o/h/a/kh;

    move-result-object v0

    .line 902
    :goto_0
    iget v1, v0, Lcom/google/o/h/a/kh;->f:I

    invoke-static {v1}, Lcom/google/o/h/a/kk;->a(I)Lcom/google/o/h/a/kk;

    move-result-object v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/google/o/h/a/kk;->a:Lcom/google/o/h/a/kk;

    :cond_0
    sget-object v1, Lcom/google/o/h/a/kk;->d:Lcom/google/o/h/a/kk;

    .line 904
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    new-instance v2, Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0}, Lcom/google/o/h/a/kh;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/startpage/d/e;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 911
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    sget-object v1, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    .line 912
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/m;->a()V

    .line 913
    return-void

    .line 901
    :cond_1
    iget-object v0, p1, Lcom/google/o/h/a/hl;->d:Lcom/google/o/h/a/kh;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 232
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 233
    sget-object v1, Lcom/google/android/apps/gmm/startpage/m;->a:Ljava/lang/String;

    const-string v2, "Pending requests weren\'t sent: %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/startpage/d/d;->h()Ljava/util/Set;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 266
    :goto_0
    return-void

    .line 236
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->J()Lcom/google/android/apps/gmm/startpage/a/e;

    move-result-object v3

    .line 237
    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    monitor-enter v4

    .line 238
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v1

    if-nez v1, :cond_1

    .line 239
    monitor-exit v4

    goto :goto_0

    .line 266
    :catchall_0
    move-exception v1

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 241
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->h()Ljava/util/Set;

    move-result-object v5

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/d/d;->a()Lcom/google/o/h/a/dq;

    move-result-object v1

    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v6

    .line 243
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/e;

    move-object v2, v0

    .line 244
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 245
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v1

    sget-object v8, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    iget-object v9, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 249
    invoke-static {v9}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v9

    .line 244
    invoke-static {v1, v6, v8, v2, v9}, Lcom/google/android/apps/gmm/startpage/aa;->a(Landroid/accounts/Account;Lcom/google/b/c/cv;Lcom/google/android/apps/gmm/startpage/af;Lcom/google/android/apps/gmm/startpage/d/e;Z)Lcom/google/android/apps/gmm/startpage/ac;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 250
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v8, v1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v2, v8, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 251
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->l()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v2

    iget-object v8, v1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v2, v8, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 252
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->m()Lcom/google/o/b/a/v;

    move-result-object v2

    iget-object v8, v1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v2, v8, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 253
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->e()Lcom/google/o/h/a/eq;

    move-result-object v2

    iget-object v8, v1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v2, v8, Lcom/google/android/apps/gmm/startpage/aa;->f:Lcom/google/o/h/a/eq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 254
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->b()Z

    move-result v2

    iget-object v8, v1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/startpage/aa;->d:Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 255
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->c()Z

    move-result v2

    iget-object v8, v1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/startpage/aa;->e:Z

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 256
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->o()Lcom/google/o/h/a/en;

    move-result-object v2

    iget-object v8, v1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v2, v8, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    .line 257
    const/4 v2, 0x1

    invoke-interface {v3, v1, v2}, Lcom/google/android/apps/gmm/startpage/a/e;->a(Lcom/google/android/apps/gmm/startpage/a/d;Z)V

    goto :goto_1

    .line 260
    :cond_2
    invoke-interface {v5}, Ljava/util/Set;->isEmpty()Z

    .line 262
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/m;->f()V

    .line 263
    if-eqz p1, :cond_3

    .line 264
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/cardui/c;->e()V

    .line 266
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 193
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    iput-object p0, v0, Lcom/google/android/apps/gmm/startpage/f/i;->g:Lcom/google/android/apps/gmm/cardui/g/c;

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->c()V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 197
    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 202
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->i()Lcom/google/o/h/a/a;

    move-result-object v1

    .line 872
    if-eqz v1, :cond_0

    .line 873
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/a;)V

    .line 876
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/startpage/q;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/startpage/q;-><init>(Lcom/google/android/apps/gmm/startpage/m;Lcom/google/o/h/a/a;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 883
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 214
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 216
    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->d()V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->f:Lcom/google/android/apps/gmm/startpage/f/i;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/startpage/f/i;->g:Lcom/google/android/apps/gmm/cardui/g/c;

    .line 222
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->b:Lcom/google/android/apps/gmm/base/activities/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->C()Z

    move-result v0

    if-nez v0, :cond_1

    .line 447
    :goto_0
    return-void

    .line 431
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/cardui/c;->b()V

    .line 432
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 434
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    monitor-enter v1

    .line 435
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 436
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->p()Lcom/google/b/c/cv;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->q()Lcom/google/b/c/cv;

    move-result-object v2

    .line 435
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 444
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 435
    :cond_2
    if-nez v2, :cond_3

    :try_start_1
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Iterable;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v2, v3, v0

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    new-instance v2, Lcom/google/b/c/ec;

    invoke-direct {v2, v0}, Lcom/google/b/c/ec;-><init>(Ljava/lang/Iterable;)V

    .line 437
    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/b;

    .line 438
    invoke-static {v0}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/android/apps/gmm/startpage/d/b;)Lcom/google/android/apps/gmm/cardui/c/b;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/apps/gmm/cardui/c/b;)Z

    goto :goto_1

    .line 441
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->d:Lcom/google/android/apps/gmm/cardui/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/d/d;->v()Lcom/google/o/h/a/nt;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/apps/gmm/cardui/c;->f:Lcom/google/o/h/a/nt;

    .line 443
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->h()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    .line 444
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 445
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/m;->f()V

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/m;->c:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/d/d;->D()V

    goto :goto_0
.end method
