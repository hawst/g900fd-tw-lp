.class public Lcom/google/android/apps/gmm/directions/c/aq;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/directions/h/w;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/directions/c/aq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    .line 78
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 4

    .prologue
    .line 31
    const/4 v0, 0x2

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v2, 0x0

    .line 32
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/directions/h/w;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/directions/h/w;->j()Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->w:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/c/aq;->l()Lcom/google/android/libraries/curvular/am;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->az:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Lcom/google/android/libraries/curvular/am;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    .line 31
    invoke-static {v1}, Lcom/google/android/apps/gmm/base/k/aa;->i([Lcom/google/android/libraries/curvular/cu;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(ILcom/google/android/libraries/curvular/ce;Landroid/content/Context;Lcom/google/android/libraries/curvular/bc;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 25
    check-cast p2, Lcom/google/android/apps/gmm/directions/h/w;

    invoke-interface {p2}, Lcom/google/android/apps/gmm/directions/h/w;->C()Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    const-string v2, "travel mode unavailable"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {p4, v0, p2}, Lcom/google/android/libraries/curvular/bc;->a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V

    :cond_0
    return-void

    :cond_1
    sget-object v2, Lcom/google/android/apps/gmm/directions/c/ar;->a:[I

    invoke-virtual {v1}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    sget-object v2, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1b

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "travel mode not supported: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_0
    invoke-interface {p2}, Lcom/google/android/apps/gmm/directions/h/w;->B()Lcom/google/android/apps/gmm/directions/h/x;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    const-string v1, "displayMode is null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-class v0, Lcom/google/android/apps/gmm/directions/c/u;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/directions/c/ar;->b:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/h/x;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_1

    const-class v0, Lcom/google/android/apps/gmm/directions/c/u;

    goto :goto_0

    :pswitch_1
    const-class v0, Lcom/google/android/apps/gmm/directions/c/r;

    goto :goto_0

    :pswitch_2
    const-class v0, Lcom/google/android/apps/gmm/directions/c/s;

    goto :goto_0

    :pswitch_3
    invoke-interface {p2}, Lcom/google/android/apps/gmm/directions/h/w;->B()Lcom/google/android/apps/gmm/directions/h/x;

    move-result-object v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    const-string v1, "displayMode is null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-class v0, Lcom/google/android/apps/gmm/directions/c/u;

    goto :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/apps/gmm/directions/c/ar;->b:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/h/x;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_2

    const-class v0, Lcom/google/android/apps/gmm/directions/c/u;

    goto :goto_0

    :pswitch_4
    const-class v0, Lcom/google/android/apps/gmm/directions/c/s;

    goto/16 :goto_0

    :pswitch_5
    invoke-interface {p2}, Lcom/google/android/apps/gmm/directions/h/w;->B()Lcom/google/android/apps/gmm/directions/h/x;

    move-result-object v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    const-string v1, "displayMode is null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-class v0, Lcom/google/android/apps/gmm/directions/c/u;

    goto/16 :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/directions/c/ar;->b:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/h/x;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_3

    const-class v0, Lcom/google/android/apps/gmm/directions/c/u;

    goto/16 :goto_0

    :pswitch_6
    const-class v0, Lcom/google/android/apps/gmm/directions/c/s;

    goto/16 :goto_0

    :pswitch_7
    const-class v0, Lcom/google/android/apps/gmm/directions/c/y;

    goto/16 :goto_0

    :pswitch_8
    invoke-interface {p2}, Lcom/google/android/apps/gmm/directions/h/w;->B()Lcom/google/android/apps/gmm/directions/h/x;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    const-string v1, "displayMode is null"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-class v0, Lcom/google/android/apps/gmm/directions/c/am;

    goto/16 :goto_0

    :cond_5
    sget-object v1, Lcom/google/android/apps/gmm/directions/c/ar;->b:[I

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/h/x;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_4

    sget-object v1, Lcom/google/android/apps/gmm/directions/c/aq;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "display mode not supported: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-class v0, Lcom/google/android/apps/gmm/directions/c/am;

    goto/16 :goto_0

    :pswitch_9
    const-class v0, Lcom/google/android/apps/gmm/directions/c/am;

    goto/16 :goto_0

    :pswitch_a
    const-class v0, Lcom/google/android/apps/gmm/directions/c/ak;

    goto/16 :goto_0

    :pswitch_b
    const-class v0, Lcom/google/android/apps/gmm/directions/c/aj;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_7
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x3
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x3
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_b
        :pswitch_b
        :pswitch_a
        :pswitch_a
        :pswitch_9
    .end packed-switch
.end method
