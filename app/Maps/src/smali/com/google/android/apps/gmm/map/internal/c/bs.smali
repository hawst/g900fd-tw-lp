.class public Lcom/google/android/apps/gmm/map/internal/c/bs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:I

.field final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final c:I

.field final d:Lcom/google/android/apps/gmm/map/internal/c/o;

.field public final e:Lcom/google/android/apps/gmm/map/internal/c/ac;

.field public final f:I

.field final g:Lcom/google/android/apps/gmm/map/b/a/aw;

.field private final h:Lcom/google/android/apps/gmm/map/internal/c/bj;

.field private final i:Lcom/google/android/apps/gmm/map/internal/c/bj;

.field private j:Lcom/google/android/apps/gmm/map/internal/c/bj;


# direct methods
.method public constructor <init>(ILcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/bl;Lcom/google/android/apps/gmm/map/internal/c/bj;Lcom/google/android/apps/gmm/map/internal/c/o;II)V
    .locals 4

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->a:I

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 65
    if-nez p3, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ai;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 66
    :cond_0
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->h:Lcom/google/android/apps/gmm/map/internal/c/bj;

    .line 68
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->d:Lcom/google/android/apps/gmm/map/internal/c/o;

    .line 69
    iput p7, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->f:I

    .line 70
    iput p8, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->c:I

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->d:Lcom/google/android/apps/gmm/map/internal/c/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->d:Lcom/google/android/apps/gmm/map/internal/c/o;

    .line 72
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    iget-boolean v0, p3, Lcom/google/android/apps/gmm/map/b/a/ai;->H:Z

    if-eqz v0, :cond_1

    if-lez p7, :cond_1

    .line 75
    invoke-interface {p6}, Lcom/google/android/apps/gmm/map/internal/c/o;->a()Lcom/google/android/apps/gmm/map/internal/c/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-interface {p6, p7, v0}, Lcom/google/android/apps/gmm/map/internal/c/o;->a(ILcom/google/android/apps/gmm/map/internal/c/ac;)Lcom/google/android/apps/gmm/map/internal/c/bj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->i:Lcom/google/android/apps/gmm/map/internal/c/bj;

    .line 82
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/aw;

    iget v1, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    iget v3, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    invoke-direct {v0, v1, v2, v3, p8}, Lcom/google/android/apps/gmm/map/b/a/aw;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->g:Lcom/google/android/apps/gmm/map/b/a/aw;

    .line 84
    return-void

    .line 78
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->i:Lcom/google/android/apps/gmm/map/internal/c/bj;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/bi;
    .locals 6

    .prologue
    .line 167
    const/4 v0, -0x1

    .line 169
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v1, v0

    .line 173
    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->i:Lcom/google/android/apps/gmm/map/internal/c/bj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->i:Lcom/google/android/apps/gmm/map/internal/c/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v0

    .line 175
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 176
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Index ID mismatch index=%d, ID=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 177
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    aput-object v0, v4, v1

    .line 176
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :catch_0
    move-exception v1

    move v1, v0

    goto :goto_0

    .line 181
    :cond_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/bi;->a()Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v0

    :cond_1
    return-object v0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->i:Lcom/google/android/apps/gmm/map/internal/c/bj;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->h:Lcom/google/android/apps/gmm/map/internal/c/bj;

    :goto_0
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->j:Lcom/google/android/apps/gmm/map/internal/c/bj;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/c/bj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->j:Lcom/google/android/apps/gmm/map/internal/c/bj;

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->h:Lcom/google/android/apps/gmm/map/internal/c/bj;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_5

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->h:Lcom/google/android/apps/gmm/map/internal/c/bj;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bj;->a(I)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v3, v3

    if-nez v3, :cond_4

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/map/internal/c/bs;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/internal/c/bi;

    move-result-object v7

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget v3, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    array-length v2, v2

    if-lez v2, :cond_6

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-byte v2, v2, v4

    if-le v2, v3, :cond_6

    iget-object v2, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-byte v2, v2, v4

    :goto_2
    sub-int v4, v2, v3

    add-int/lit8 v4, v4, 0x1

    new-array v8, v4, [Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v8

    new-array v9, v4, [B

    invoke-static {}, Lcom/google/android/apps/gmm/map/internal/c/be;->a()Lcom/google/android/apps/gmm/map/internal/c/be;

    move-result-object v4

    move-object v5, v4

    move v4, v1

    :goto_3
    iget-object v6, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v6, v6

    if-ge v4, v6, :cond_1

    iget-object v6, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v6, v6, v4

    if-ge v6, v3, :cond_1

    iget-object v5, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v5, v5, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_1
    move v6, v1

    move v11, v3

    move v3, v4

    move-object v4, v5

    move v5, v11

    :goto_4
    if-gt v5, v2, :cond_3

    iget-object v10, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v10, v10

    if-ge v3, v10, :cond_2

    iget-object v10, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->c:[B

    aget-byte v10, v10, v3

    if-ne v10, v5, :cond_2

    iget-object v4, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v4, v4, v3

    add-int/lit8 v3, v3, 0x1

    :cond_2
    aput-object v4, v8, v6

    int-to-byte v10, v5

    aput-byte v10, v9, v6

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/c/bi;

    iget-object v3, v7, Lcom/google/android/apps/gmm/map/internal/c/bi;->a:Ljava/lang/String;

    invoke-direct {v2, v3, v8, v9}, Lcom/google/android/apps/gmm/map/internal/c/bi;-><init>(Ljava/lang/String;[Lcom/google/android/apps/gmm/map/internal/c/be;[B)V

    :cond_4
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->j:Lcom/google/android/apps/gmm/map/internal/c/bj;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/c/bj;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bs;->j:Lcom/google/android/apps/gmm/map/internal/c/bj;

    goto/16 :goto_0

    :cond_6
    move v2, v3

    goto :goto_2
.end method
