.class public Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/directions/i/ag;

.field private c:Lcom/google/android/apps/gmm/map/r/a/f;

.field private d:I

.field private e:Lcom/google/android/apps/gmm/directions/bh;

.field private f:Lcom/google/android/apps/gmm/map/r/a/w;

.field private g:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/l;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/l;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 220
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/map/r/a/f;IILcom/google/android/apps/gmm/o/a/c;)Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;
    .locals 3

    .prologue
    .line 72
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 73
    const-string v1, "storageItem"

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 74
    const-string v1, "tripIndex"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 75
    const-string v1, "currentStepIndex"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 76
    const-string v1, "savedLayerState"

    new-instance v2, Lcom/google/android/apps/gmm/directions/bh;

    invoke-direct {v2, p4}, Lcom/google/android/apps/gmm/directions/bh;-><init>(Lcom/google/android/apps/gmm/o/a/c;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 77
    new-instance v1, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;-><init>()V

    .line 78
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 80
    return-object v1
.end method


# virtual methods
.method public final a(IIF)V
    .locals 1

    .prologue
    .line 211
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->a(IIF)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->a:Lcom/google/android/apps/gmm/directions/i/ag;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/i/ag;->j()V

    .line 215
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    if-nez p1, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "storageItem"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 91
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    .line 93
    const-string v0, "tripIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->d:I

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->d:I

    .line 95
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 94
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/f;ILandroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 97
    const-string v0, "savedLayerState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/bh;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->e:Lcom/google/android/apps/gmm/directions/bh;

    .line 98
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 103
    if-nez p3, :cond_0

    .line 104
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p3

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/directions/c/v;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->g:Lcom/google/android/libraries/curvular/ae;

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/directions/c/x;

    invoke-virtual {v0, v1, v6}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->m:Lcom/google/android/libraries/curvular/ae;

    .line 111
    const-string v0, "currentStepIndex"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 112
    new-instance v0, Lcom/google/android/apps/gmm/directions/i/ag;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->g:Lcom/google/android/libraries/curvular/ae;

    .line 113
    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->m:Lcom/google/android/libraries/curvular/ae;

    iget-object v4, v4, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    invoke-static {v1, v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v4

    .line 114
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/directions/i/ag;-><init>(Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;Lcom/google/android/apps/gmm/map/r/a/w;ILjava/util/List;Lcom/google/android/apps/gmm/shared/net/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->a:Lcom/google/android/apps/gmm/directions/i/ag;

    .line 116
    return-object v6
.end method

.method public onPause()V
    .locals 8

    .prologue
    .line 204
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->e:Lcom/google/android/apps/gmm/directions/bh;

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v2

    .line 204
    invoke-static {}, Lcom/google/android/apps/gmm/o/a/a;->values()[Lcom/google/android/apps/gmm/o/a/a;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    iget-object v6, v1, Lcom/google/android/apps/gmm/directions/bh;->a:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    sget-object v7, Lcom/google/android/apps/gmm/directions/bg;->b:[I

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/o/a/a;->ordinal()I

    move-result v5

    aget v5, v7, v5

    packed-switch v5, :pswitch_data_0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    invoke-interface {v2, v6}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    goto :goto_1

    :pswitch_1
    invoke-interface {v2, v6}, Lcom/google/android/apps/gmm/o/a/c;->c(Z)Z

    goto :goto_1

    :pswitch_2
    invoke-interface {v2, v6}, Lcom/google/android/apps/gmm/o/a/c;->b(Z)Z

    goto :goto_1

    :pswitch_3
    invoke-interface {v2, v6}, Lcom/google/android/apps/gmm/o/a/c;->d(Z)Z

    goto :goto_1

    :pswitch_4
    invoke-interface {v2, v6}, Lcom/google/android/apps/gmm/o/a/c;->e(Z)Z

    goto :goto_1

    .line 206
    :cond_0
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 207
    return-void

    .line 204
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 121
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->t()Lcom/google/android/apps/gmm/o/a/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/o/a/f;->d()Lcom/google/android/apps/gmm/o/a/c;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/directions/bg;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/hm;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unexpected travel mode: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->e(Z)Z

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->g:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->a:Lcom/google/android/apps/gmm/directions/i/ag;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->m:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->a:Lcom/google/android/apps/gmm/directions/i/ag;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/x;-><init>()V

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->g:Lcom/google/android/libraries/curvular/ae;

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/activities/x;->a:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/x;->b:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 131
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->g:Lcom/google/android/libraries/curvular/ae;

    .line 132
    iget-object v2, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v3, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->m:Lcom/google/android/libraries/curvular/ae;

    .line 133
    iget-object v2, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->C:Landroid/view/View;

    .line 134
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 135
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v8, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 136
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v8, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v6, v2, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 137
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v7, v2, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/f;->a:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 138
    iget-object v3, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v2, v3, Lcom/google/android/apps/gmm/base/activities/p;->E:Lcom/google/android/apps/gmm/map/b/a/f;

    .line 139
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->F:Lcom/google/android/apps/gmm/base/activities/x;

    .line 140
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 141
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 142
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->b()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    .line 143
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v0, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    new-instance v0, Lcom/google/android/apps/gmm/directions/bf;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/directions/bf;-><init>(Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;)V

    .line 144
    iget-object v2, v1, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 155
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 157
    return-void

    .line 123
    :pswitch_0
    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->c(Z)Z

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->b(Z)Z

    goto/16 :goto_0

    :pswitch_1
    invoke-interface {v1, v6}, Lcom/google/android/apps/gmm/o/a/c;->c(Z)Z

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->b(Z)Z

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    goto/16 :goto_0

    :pswitch_2
    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->c(Z)Z

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->b(Z)Z

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    goto/16 :goto_0

    :pswitch_3
    invoke-interface {v1, v6}, Lcom/google/android/apps/gmm/o/a/c;->b(Z)Z

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->c(Z)Z

    invoke-interface {v1, v5}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 194
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "storageItem"

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->c:Lcom/google/android/apps/gmm/map/r/a/f;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 197
    const-string v0, "tripIndex"

    iget v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 198
    const-string v0, "currentStepIndex"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->a:Lcom/google/android/apps/gmm/directions/i/ag;

    iget v1, v1, Lcom/google/android/apps/gmm/directions/i/ag;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 199
    const-string v0, "savedLayerState"

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/RoutePreviewFragment;->e:Lcom/google/android/apps/gmm/directions/bh;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 200
    return-void
.end method
