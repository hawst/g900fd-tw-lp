.class public Lcom/google/android/apps/gmm/search/al;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/placelists/a/c;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/search/ai;

.field public b:Lcom/google/android/apps/gmm/base/placelists/a/e;

.field public c:Lcom/google/android/apps/gmm/search/ap;

.field public d:Lcom/google/android/apps/gmm/search/am;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/al;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/search/ai;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/ai;-><init>()V

    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V
    .locals 4

    .prologue
    .line 90
    sget-object v0, Lcom/google/r/b/a/el;->bN:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/bd;->d:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/search/ap;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/ap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 93
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/ai;->d()Lcom/google/android/apps/gmm/place/ck;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/place/cl;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/place/cl;-><init>()V

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    const/4 v1, 0x1

    .line 94
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/cl;->c:Ljava/lang/Boolean;

    .line 95
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/cl;->a()Lcom/google/android/apps/gmm/place/ck;

    move-result-object v0

    .line 97
    new-instance v1, Lcom/google/android/apps/gmm/search/aj;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/search/aj;-><init>(Lcom/google/android/apps/gmm/search/ai;)V

    .line 98
    iput-object v0, v1, Lcom/google/android/apps/gmm/search/aj;->l:Lcom/google/android/apps/gmm/place/ck;

    .line 99
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/aj;->a()Lcom/google/android/apps/gmm/search/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    .line 100
    iput-object p2, p0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    .line 101
    return-void

    .line 93
    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/place/ck;->b:Lcom/google/android/apps/gmm/place/cm;

    iput-object v2, v1, Lcom/google/android/apps/gmm/place/cl;->b:Lcom/google/android/apps/gmm/place/cm;

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/ck;->c:Ljava/lang/Boolean;

    iput-object v2, v1, Lcom/google/android/apps/gmm/place/cl;->c:Ljava/lang/Boolean;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/ck;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/apps/gmm/place/cl;->a:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/e/a/a/a/b;)V
    .locals 4

    .prologue
    const/16 v3, 0x1c

    const/4 v2, 0x1

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/search/al;-><init>(Lcom/google/android/apps/gmm/search/ai;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/e/a/a/a/b;)V

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/search/ap;->b:Ljava/lang/String;

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/ap;->a(Ljava/lang/String;)V

    .line 114
    return-void
.end method


# virtual methods
.method protected final M_()J
    .locals 2

    .prologue
    .line 136
    const-wide/16 v0, 0x3a98

    return-wide v0
.end method

.method protected final S_()Lcom/google/b/a/ak;
    .locals 5

    .prologue
    .line 244
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->S_()Lcom/google/b/a/ak;

    move-result-object v2

    const-string v1, "params.getQuery"

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    if-nez v0, :cond_0

    const-string v0, "<NULL>"

    .line 245
    :goto_0
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v2, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    .line 245
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v3, 0x1

    const/16 v4, 0x1c

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    return-object v2
.end method

.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 4

    .prologue
    const/16 v3, 0x1c

    const/4 v2, 0x1

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/search/ap;->a(Lcom/google/e/a/a/a/b;)V

    .line 169
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/search/ap;->b:Ljava/lang/String;

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/search/ap;->a(Ljava/lang/String;)V

    .line 171
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/io/ObjectInputStream;)V
    .locals 2

    .prologue
    .line 190
    :try_start_0
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/ai;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    .line 191
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 192
    invoke-virtual {p1}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    return-void

    .line 193
    :catch_0
    move-exception v0

    .line 194
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Ljava/io/ObjectOutputStream;)V
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-virtual {p1, v0}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    .line 185
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 176
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    .line 177
    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/search/am;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic b()Lcom/google/android/apps/gmm/base/placelists/a/a;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/placelists/a/e;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 213
    invoke-super {p0}, Lcom/google/android/apps/gmm/shared/net/af;->f()V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/search/am;->b(Lcom/google/android/apps/gmm/search/al;)V

    .line 215
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->b:Lcom/google/android/apps/gmm/base/placelists/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/a/e;->i:Ljava/lang/String;

    .line 221
    :goto_1
    return-object v0

    .line 218
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 221
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v2, 0x1c

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_1
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 3

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/4 v1, 0x3

    const/16 v2, 0x15

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    long-to-int v0, v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v1, 0x1f

    const/16 v2, 0x18

    invoke-virtual {v0, v1, v2}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    if-nez v0, :cond_0

    .line 143
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "Ignoring response, no listener set"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/p;->a([Ljava/lang/Object;)V

    .line 152
    :goto_0
    return-void

    .line 147
    :cond_0
    if-nez p1, :cond_1

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/search/am;->a(Lcom/google/android/apps/gmm/search/al;)V

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    invoke-interface {v0, p0, p1}, Lcom/google/android/apps/gmm/search/am;->a(Lcom/google/android/apps/gmm/search/al;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_0
.end method
