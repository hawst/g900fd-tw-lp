.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;
.super Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/a/b;
.implements Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;


# instance fields
.field public j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

.field public k:Lcom/google/android/apps/gmm/map/f/o;

.field private final l:Landroid/content/res/Resources;

.field private final m:Lcom/google/android/apps/gmm/shared/net/ad;

.field private n:Lcom/google/android/apps/gmm/map/m/f;

.field private o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

.field private p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/map/q/b;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 122
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;-><init>(Landroid/content/Context;)V

    .line 123
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->l:Landroid/content/res/Resources;

    .line 124
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->m:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 125
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->k:Lcom/google/android/apps/gmm/map/f/o;

    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->i:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->l:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    iget v2, v0, Landroid/util/DisplayMetrics;->density:F

    iget v3, v0, Landroid/util/DisplayMetrics;->xdpi:F

    iget v0, v0, Landroid/util/DisplayMetrics;->ydpi:F

    invoke-direct {v1, p0, p5, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/m;Lcom/google/android/apps/gmm/map/q/b;F)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    new-instance v0, Lcom/google/android/apps/gmm/map/m/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/m/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->n:Lcom/google/android/apps/gmm/map/m/f;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->n:Lcom/google/android/apps/gmm/map/m/f;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/m/f;->a:Lcom/google/android/apps/gmm/map/m/q;

    new-instance v2, Lcom/google/android/apps/gmm/map/m/n;

    invoke-direct {v2, v1, v0}, Lcom/google/android/apps/gmm/map/m/n;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/m/r;)V

    iput-object v2, v0, Lcom/google/android/apps/gmm/map/m/f;->b:Lcom/google/android/apps/gmm/map/m/n;

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->setFocusable(Z)V

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->setClickable(Z)V

    iget-object v0, p6, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    new-instance v1, Lcom/google/android/apps/gmm/v/a;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/v/a;-><init>()V

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->e:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->h:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    if-eqz v1, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->e:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    if-nez v1, :cond_3

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/w;

    invoke-direct {v1, p0, v4}, Lcom/google/android/apps/gmm/map/internal/vector/w;-><init>(Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;Z)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->e:Landroid/opengl/GLSurfaceView$EGLConfigChooser;

    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->f:Lcom/google/android/apps/gmm/map/internal/vector/q;

    if-nez v1, :cond_4

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/o;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/map/internal/vector/o;-><init>(Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->f:Lcom/google/android/apps/gmm/map/internal/vector/q;

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->g:Lcom/google/android/apps/gmm/map/internal/vector/r;

    if-nez v1, :cond_5

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/p;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/internal/vector/p;-><init>()V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->g:Lcom/google/android/apps/gmm/map/internal/vector/r;

    :cond_5
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->d:Lcom/google/android/apps/gmm/map/internal/vector/k;

    new-instance v1, Lcom/google/android/apps/gmm/map/internal/vector/t;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/vector/k;->e()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;-><init>(Ljava/lang/ref/WeakReference;Lcom/google/android/apps/gmm/map/c/a/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->start()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c:Lcom/google/android/apps/gmm/map/internal/vector/t;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/vector/t;->a(I)V

    .line 126
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->m:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/ab;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->a:Lcom/google/android/apps/gmm/map/j/ac;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/ab;-><init>(Lcom/google/android/apps/gmm/map/j/ac;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 310
    return-void
.end method

.method public final a(FF)V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    if-eqz v0, :cond_0

    .line 273
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 274
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;->b(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 276
    :cond_0
    return-void

    .line 273
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(FFFZ)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;->a(FFFZ)V

    .line 288
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->m:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 181
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->b()V

    .line 182
    return-void
.end method

.method public final b(FF)V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 299
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 301
    :cond_0
    return-void

    .line 298
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->c()V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->m:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 188
    return-void
.end method

.method public final c(FF)Z
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/apps/gmm/map/f/o;->a(FFLcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 240
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    invoke-interface {v1, p0, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;->a(Lcom/google/android/apps/gmm/map/a/b;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 242
    :cond_0
    const/4 v0, 0x0

    return v0

    .line 239
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canScrollHorizontally(I)Z
    .locals 1

    .prologue
    .line 420
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/l;->b()Z

    move-result v0

    return v0
.end method

.method public canScrollVertically(I)Z
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/l;->b()Z

    move-result v0

    return v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/f/o;
    .locals 6

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->k:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v3, v0, Lcom/google/android/apps/gmm/v/bi;->h:Lcom/google/android/apps/gmm/v/bj;

    .line 208
    new-instance v0, Lcom/google/android/apps/gmm/map/f/o;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->k:Lcom/google/android/apps/gmm/map/f/o;

    .line 209
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v3, Lcom/google/android/apps/gmm/v/bj;->a:I

    iget v3, v3, Lcom/google/android/apps/gmm/v/bj;->b:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->l:Landroid/content/res/Resources;

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v4, v4, Landroid/util/DisplayMetrics;->density:F

    sget-object v5, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/o;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;IIFLcom/google/android/apps/gmm/shared/c/a/p;)V

    return-object v0
.end method

.method public final d(FF)V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    if-eqz v0, :cond_0

    .line 260
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 261
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    .line 263
    :cond_0
    return-void
.end method

.method public final e()Lcom/google/android/apps/gmm/map/m/l;
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->p:Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/k;->a:Lcom/google/android/apps/gmm/map/m/ac;

    return-object v0
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->m:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/ab;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->b:Lcom/google/android/apps/gmm/map/j/ac;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/ab;-><init>(Lcom/google/android/apps/gmm/map/j/ac;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 338
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->m:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/j/ab;

    sget-object v2, Lcom/google/android/apps/gmm/map/j/ac;->c:Lcom/google/android/apps/gmm/map/j/ac;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/map/j/ab;-><init>(Lcom/google/android/apps/gmm/map/j/ac;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 346
    return-void
.end method

.method public final h()Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->j:Lcom/google/android/apps/gmm/map/legacy/internal/vector/f;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/map/m/f;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->n:Lcom/google/android/apps/gmm/map/m/f;

    return-object v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x1

    return v0
.end method

.method public onFinishInflate()V
    .locals 0

    .prologue
    .line 130
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->onFinishInflate()V

    .line 131
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->isClickable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 221
    :cond_0
    const/4 v0, 0x0

    .line 224
    :goto_0
    return v0

    .line 223
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->n:Lcom/google/android/apps/gmm/map/m/f;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/m/f;->a(Landroid/view/MotionEvent;)Z

    .line 224
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->n:Lcom/google/android/apps/gmm/map/m/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/m/f;->b:Lcom/google/android/apps/gmm/map/m/n;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/m/n;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setApiOnMapGestureListener(Lcom/google/android/apps/gmm/map/a/c;)V
    .locals 1

    .prologue
    .line 362
    if-nez p1, :cond_0

    .line 363
    const/4 v0, 0x0

    .line 415
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->o:Lcom/google/android/apps/gmm/map/legacy/internal/vector/q;

    .line 416
    return-void

    .line 365
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/p;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/p;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;Lcom/google/android/apps/gmm/map/a/c;)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 2

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->getVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 230
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/internal/vector/GmmGLTextureView;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/VectorMapViewImpl;->k:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/af;

    if-eqz p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t/af;->g:Lcom/google/android/apps/gmm/map/internal/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/a;->a(Z)V

    .line 234
    :cond_0
    return-void

    .line 231
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
