.class public Lcom/google/android/apps/gmm/base/support/GmmToolbarView;
.super Landroid/widget/LinearLayout;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/c/j;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Landroid/widget/ImageButton;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/ImageButton;

.field private final f:Landroid/widget/ImageButton;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private j:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->i:Ljava/util/List;

    .line 76
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a:Landroid/content/Context;

    .line 77
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->setOrientation(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->addView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/support/c;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->g:Landroid/view/View;

    .line 83
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/support/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    .line 84
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/f;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->e:Landroid/widget/ImageButton;

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/f;->b:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->c:Landroid/widget/TextView;

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/f;->c:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->d:Landroid/widget/TextView;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/f;->d:Lcom/google/android/libraries/curvular/bk;

    .line 89
    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->f:Landroid/widget/ImageButton;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/f;->e:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->b:Landroid/widget/ImageButton;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    sget-object v1, Lcom/google/android/apps/gmm/base/support/f;->f:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->h:Landroid/view/View;

    .line 92
    return-void
.end method

.method private static a(Ljava/lang/CharSequence;ILandroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 128
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 129
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 135
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p2, p0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 134
    int-to-float v0, p1

    const/high16 v1, 0x437f0000    # 255.0f

    div-float/2addr v0, v1

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setAlpha(F)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/views/c/g;)V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->i:Lcom/google/android/apps/gmm/base/views/c/j;

    if-ne v0, p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 244
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->setProperties(Lcom/google/android/apps/gmm/base/views/c/g;)V

    .line 245
    return-void
.end method

.method public final setProperties(Lcom/google/android/apps/gmm/base/views/c/g;)V
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/4 v1, 0x1

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 95
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->g:Ljava/util/List;

    const-string v4, "ActionMenuItems are null"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->a:Ljava/lang/CharSequence;

    iget v4, p1, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->c:Landroid/widget/TextView;

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a(Ljava/lang/CharSequence;ILandroid/widget/TextView;)V

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->b:Ljava/lang/CharSequence;

    iget v4, p1, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->d:Landroid/widget/TextView;

    invoke-static {v0, v4, v5}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a(Ljava/lang/CharSequence;ILandroid/widget/TextView;)V

    .line 100
    iget-object v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->c:Lcom/google/android/libraries/curvular/aw;

    .line 101
    iget-object v4, p1, Lcom/google/android/apps/gmm/base/views/c/g;->d:Lcom/google/android/libraries/curvular/bi;

    .line 102
    iget-object v5, p1, Lcom/google/android/apps/gmm/base/views/c/g;->l:Landroid/view/View$OnClickListener;

    .line 103
    iget-object v6, p1, Lcom/google/android/apps/gmm/base/views/c/g;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 100
    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    if-nez v5, :cond_9

    :cond_1
    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v6, "icon should be null"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    if-nez v4, :cond_4

    move v0, v1

    :goto_1
    const-string v4, "contentDescription should be null"

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    if-nez v5, :cond_6

    move v0, v1

    :goto_2
    const-string v4, "clickListener should be null"

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 105
    :cond_8
    :goto_3
    iget-object v4, p1, Lcom/google/android/apps/gmm/base/views/c/g;->g:Ljava/util/List;

    if-nez v4, :cond_a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 100
    :cond_9
    iget-object v7, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->e:Landroid/widget/ImageButton;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a:Landroid/content/Context;

    invoke-interface {v0, v8}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->e:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a:Landroid/content/Context;

    invoke-interface {v4, v7}, Lcom/google/android/libraries/curvular/bi;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->e:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    if-eqz v6, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->e:Landroid/widget/ImageButton;

    invoke-static {v0, v6}, Lcom/google/android/apps/gmm/z/i;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/l;)V

    goto :goto_3

    .line 105
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/c/e;

    iget v0, v0, Lcom/google/android/apps/gmm/base/views/c/e;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_c

    :cond_b
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->f:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    move v1, v2

    :goto_4
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 106
    :goto_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->h:Landroid/view/View;

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_f

    move v0, v2

    :goto_6
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 108
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v4, p1, Lcom/google/android/apps/gmm/base/views/c/g;->f:Lcom/google/android/libraries/curvular/aq;

    invoke-interface {v4, v0}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    if-eq v0, v9, :cond_10

    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    :goto_7
    mul-int/2addr v0, v5

    div-int/lit16 v0, v0, 0xff

    const v5, 0xffffff

    and-int/2addr v4, v5

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v4

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->j:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v1, v9, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->g:Landroid/view/View;

    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    if-eq v0, v9, :cond_11

    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    :goto_8
    int-to-float v0, v0

    const/high16 v4, 0x437f0000    # 255.0f

    div-float/2addr v0, v4

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->g:Landroid/view/View;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/base/views/c/g;->m:Z

    if-eqz v1, :cond_12

    :goto_9
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iput-object p0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->i:Lcom/google/android/apps/gmm/base/views/c/j;

    .line 116
    return-void

    .line 105
    :cond_c
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/c/e;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->f:Landroid/widget/ImageButton;

    if-nez v0, :cond_d

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    :cond_d
    iget-object v6, v0, Lcom/google/android/apps/gmm/base/views/c/e;->c:Lcom/google/android/libraries/curvular/aw;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a:Landroid/content/Context;

    invoke-interface {v6, v7}, Lcom/google/android/libraries/curvular/aw;->d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v6, v0, Lcom/google/android/apps/gmm/base/views/c/e;->b:Ljava/lang/CharSequence;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v6, Lcom/google/android/apps/gmm/base/support/d;

    invoke-direct {v6, p0, v0}, Lcom/google/android/apps/gmm/base/support/d;-><init>(Lcom/google/android/apps/gmm/base/support/GmmToolbarView;Lcom/google/android/apps/gmm/base/views/c/e;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v5, Lcom/google/android/apps/gmm/f;->cA:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->b:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v5, Lcom/google/android/apps/gmm/l;->af:I

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->b:Landroid/widget/ImageButton;

    sget-object v1, Lcom/google/b/f/t;->di:Lcom/google/b/f/t;

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Landroid/view/View;Lcom/google/android/apps/gmm/z/b/l;)V

    invoke-static {v4}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->b:Landroid/widget/ImageButton;

    new-instance v4, Lcom/google/android/apps/gmm/base/support/e;

    invoke-direct {v4, p0, v0}, Lcom/google/android/apps/gmm/base/support/e;-><init>(Lcom/google/android/apps/gmm/base/support/GmmToolbarView;Lcom/google/b/c/cv;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/support/GmmToolbarView;->b:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_5

    :cond_f
    move v0, v3

    .line 106
    goto/16 :goto_6

    .line 108
    :cond_10
    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    goto/16 :goto_7

    .line 112
    :cond_11
    iget v0, p1, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    goto/16 :goto_8

    :cond_12
    move v2, v3

    .line 113
    goto :goto_9
.end method
