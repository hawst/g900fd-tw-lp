.class public Lcom/google/android/apps/gmm/base/k/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/Boolean;

.field public final b:Lcom/google/android/libraries/curvular/aw;

.field public final c:Lcom/google/android/libraries/curvular/aw;

.field public final d:Lcom/google/android/libraries/curvular/aw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/k;->a:Ljava/lang/Boolean;

    .line 98
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/k/k;->b:Lcom/google/android/libraries/curvular/aw;

    .line 99
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/k/k;->c:Lcom/google/android/libraries/curvular/aw;

    .line 100
    iput-object v1, p0, Lcom/google/android/apps/gmm/base/k/k;->d:Lcom/google/android/libraries/curvular/aw;

    .line 101
    return-void
.end method

.method public constructor <init>(Ljava/lang/Boolean;Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;Lcom/google/android/libraries/curvular/aw;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/k;->a:Ljava/lang/Boolean;

    .line 109
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/k;->b:Lcom/google/android/libraries/curvular/aw;

    .line 110
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/k/k;->c:Lcom/google/android/libraries/curvular/aw;

    .line 111
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/k/k;->d:Lcom/google/android/libraries/curvular/aw;

    .line 112
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 116
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/k/k;

    if-nez v2, :cond_1

    .line 125
    :cond_0
    :goto_0
    return v0

    .line 120
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/base/k/k;

    .line 121
    if-eq p0, p1, :cond_6

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->a:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/k;->a:Ljava/lang/Boolean;

    .line 122
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->b:Lcom/google/android/libraries/curvular/aw;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/k;->b:Lcom/google/android/libraries/curvular/aw;

    .line 123
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_8

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->c:Lcom/google/android/libraries/curvular/aw;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/k;->c:Lcom/google/android/libraries/curvular/aw;

    .line 124
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_9

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_4
    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->d:Lcom/google/android/libraries/curvular/aw;

    iget-object v3, p1, Lcom/google/android/apps/gmm/base/k/k;->d:Lcom/google/android/libraries/curvular/aw;

    .line 125
    if-eq v2, v3, :cond_5

    if-eqz v2, :cond_a

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_5
    move v2, v1

    :goto_4
    if-eqz v2, :cond_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v2, v0

    .line 122
    goto :goto_1

    :cond_8
    move v2, v0

    .line 123
    goto :goto_2

    :cond_9
    move v2, v0

    .line 124
    goto :goto_3

    :cond_a
    move v2, v0

    .line 125
    goto :goto_4
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 130
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->a:Ljava/lang/Boolean;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->b:Lcom/google/android/libraries/curvular/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->c:Lcom/google/android/libraries/curvular/aw;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/k;->d:Lcom/google/android/libraries/curvular/aw;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
