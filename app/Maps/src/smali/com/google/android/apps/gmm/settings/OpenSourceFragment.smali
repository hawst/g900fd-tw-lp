.class public Lcom/google/android/apps/gmm/settings/OpenSourceFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"


# static fields
.field private static final c:Landroid/text/style/ForegroundColorSpan;

.field private static final d:Landroid/text/style/ForegroundColorSpan;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    .line 70
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const v1, -0x777778

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->d:Landroid/text/style/ForegroundColorSpan;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 13

    .prologue
    .line 75
    sget v0, Lcom/google/android/apps/gmm/h;->av:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    new-instance v1, Landroid/text/SpannableString;

    const-string v0, "This software is based in part on JSR305: "

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    const/4 v2, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v2, Landroid/text/SpannableString;

    const-string v0, "Copyright \u00a9 2007-2009, JSR305 expert group\n"

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->d:Landroid/text/style/ForegroundColorSpan;

    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x11

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    sget v0, Lcom/google/android/apps/gmm/g;->ba:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    const-string v1, "http://www.opensource.org/licenses/bsd-license.php"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    new-instance v2, Landroid/text/SpannableString;

    const-string v1, "This software is based in part on Guava JDK5: "

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x11

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v3, Landroid/text/SpannableString;

    const-string v1, "Copyright \u00a9 January 2004, Apache License Version 2.0\n"

    invoke-direct {v3, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->d:Landroid/text/style/ForegroundColorSpan;

    const/4 v4, 0x0

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v5

    const/16 v6, 0x11

    invoke-virtual {v3, v1, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    sget v1, Lcom/google/android/apps/gmm/g;->aL:I

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    const-string v2, "http://www.apache.org/licenses/LICENSE-2.0"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    new-instance v3, Landroid/text/SpannableString;

    const-string v2, "This software is based in part on Android SDK: "

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    const/4 v4, 0x0

    invoke-virtual {v3}, Landroid/text/SpannableString;->length()I

    move-result v5

    const/16 v6, 0x11

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v4, Landroid/text/SpannableString;

    const-string v2, "Copyright \u00a9 April 2009, Google Android SDK License\n"

    invoke-direct {v4, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->d:Landroid/text/style/ForegroundColorSpan;

    const/4 v5, 0x0

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v6

    const/16 v8, 0x11

    invoke-virtual {v4, v2, v5, v6, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    sget v2, Lcom/google/android/apps/gmm/g;->k:I

    invoke-virtual {v7, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    const-string v3, "http://developer.android.com/sdk\n"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    const-string v3, "http://code.google.com/android/download.html"

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    new-instance v4, Landroid/text/SpannableString;

    const-string v3, "SVG Android\n"

    invoke-direct {v4, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    const/4 v5, 0x0

    invoke-virtual {v4}, Landroid/text/SpannableString;->length()I

    move-result v6

    const/16 v8, 0x11

    invoke-virtual {v4, v3, v5, v6, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v5, Landroid/text/SpannableString;

    const-string v3, "Copyright 2011 Larva Labs LLC and Google, Inc."

    invoke-direct {v5, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->d:Landroid/text/style/ForegroundColorSpan;

    const/4 v6, 0x0

    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v8

    const/16 v9, 0x11

    invoke-virtual {v5, v3, v6, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    sget v3, Lcom/google/android/apps/gmm/g;->dE:I

    invoke-virtual {v7, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    new-instance v6, Landroid/text/SpannableString;

    const-string v4, "This product includes software developed at The Apache Software Foundation (http://www.apache.org/)."

    invoke-direct {v6, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v4, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    const/4 v5, 0x0

    invoke-virtual {v6}, Landroid/text/SpannableString;->length()I

    move-result v8

    const/16 v9, 0x11

    invoke-virtual {v6, v4, v5, v8, v9}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    sget v4, Lcom/google/android/apps/gmm/g;->l:I

    invoke-virtual {v7, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    new-instance v8, Landroid/text/SpannableString;

    const-string v5, "Apache Batik\n"

    invoke-direct {v8, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v5, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    const/4 v9, 0x0

    invoke-virtual {v8}, Landroid/text/SpannableString;->length()I

    move-result v10

    const/16 v11, 0x11

    invoke-virtual {v8, v5, v9, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v9, Landroid/text/SpannableString;

    const-string v5, "Copyright 1999-2007 The Apache Software Foundation"

    invoke-direct {v9, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v5, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->d:Landroid/text/style/ForegroundColorSpan;

    const/4 v10, 0x0

    invoke-virtual {v9}, Landroid/text/SpannableString;->length()I

    move-result v11

    const/16 v12, 0x11

    invoke-virtual {v9, v5, v10, v11, v12}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    sget v5, Lcom/google/android/apps/gmm/g;->y:I

    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    sget v5, Lcom/google/android/apps/gmm/g;->m:I

    invoke-virtual {v7, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    sget v6, Lcom/google/android/apps/gmm/g;->eC:I

    invoke-virtual {v7, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const-string v8, "This software contains code from the World Wide Web Consortium (W3C) for the Document Object Model API (DOM API) and SVG Document Type Definition (DTD)."

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    sget v6, Lcom/google/android/apps/gmm/g;->aY:I

    invoke-virtual {v7, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    const-string v8, "This software contains code from the International Organisation for Standardization for the definition of character entities used in the software\'s documentation."

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    const/4 v6, 0x1

    invoke-static {v0, v6}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const/4 v0, 0x1

    invoke-static {v1, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const/4 v0, 0x1

    invoke-static {v2, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const/4 v0, 0x1

    invoke-static {v3, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const-string v0, "http://www.apache.org/"

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-static {v4, v1, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    const-string v0, "http://www.apache.org/"

    const/16 v1, 0x10

    invoke-static {v0, v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-static {v5, v1, v0}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;Ljava/util/regex/Pattern;Ljava/lang/String;)V

    new-instance v1, Landroid/text/SpannableString;

    const-string v0, "This software is based in part on GNU libstdc++:\n"

    invoke-direct {v1, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->c:Landroid/text/style/ForegroundColorSpan;

    const/4 v2, 0x0

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    const/16 v4, 0x11

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    new-instance v2, Landroid/text/SpannableString;

    const-string v0, "GCC RUNTIME LIBRARY EXCEPTION\nVersion 3.1, 31 March 2009\nCopyright (C) 2009 Free Software Foundation, Inc.\n"

    invoke-direct {v2, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->d:Landroid/text/style/ForegroundColorSpan;

    const/4 v3, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableString;->length()I

    move-result v4

    const/16 v5, 0x11

    invoke-virtual {v2, v0, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    sget v0, Lcom/google/android/apps/gmm/g;->aJ:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    const-string v1, "http://gcc.gnu.org/onlinedocs/libstdc++/manual/license.html"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    sget v1, Lcom/google/android/apps/gmm/l;->jH:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setTitle(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->a(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 180
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 181
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 182
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 183
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 184
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/settings/OpenSourceFragment;->getView()Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 185
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 187
    return-void
.end method
