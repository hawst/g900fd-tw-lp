.class public Lcom/google/android/apps/gmm/shared/net/a/h;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public volatile a:Z

.field public volatile b:Z

.field public volatile c:Z

.field public volatile d:Z

.field public volatile e:Z

.field public volatile f:Z

.field public volatile g:Z

.field public volatile h:Z

.field public volatile i:Z

.field volatile j:Z

.field public volatile k:Z

.field public volatile l:Z

.field public volatile m:Z

.field volatile n:Z

.field public volatile o:Z

.field volatile p:Z

.field public volatile q:Z

.field public volatile r:Z

.field private volatile s:Z

.field private volatile t:Z

.field private volatile u:Z


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/fi;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/apps/gmm/shared/net/a/h;->a(Lcom/google/r/b/a/fi;Z)V

    .line 63
    return-void
.end method


# virtual methods
.method a(Lcom/google/r/b/a/fi;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->c:Z

    if-eqz p2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    .line 92
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->b:Z

    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->k:Z

    if-eqz p2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->b:Z

    .line 94
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->c:Z

    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->l:Z

    if-eqz p2, :cond_5

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->c:Z

    .line 96
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->d:Z

    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->u:Z

    if-eqz p2, :cond_7

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->d:Z

    .line 98
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->w:Z

    if-eqz p2, :cond_9

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    .line 100
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->j:Z

    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->z:Z

    if-eqz p2, :cond_b

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->j:Z

    .line 102
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->e:Z

    .line 105
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->v:Z

    .line 102
    if-eqz p2, :cond_d

    :goto_6
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->e:Z

    .line 107
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->f:Z

    .line 110
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->A:Z

    .line 107
    if-eqz p2, :cond_f

    move v1, v0

    :cond_0
    :goto_7
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->f:Z

    .line 113
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->D:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->s:Z

    .line 116
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->E:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->g:Z

    .line 119
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->F:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->h:Z

    .line 122
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->G:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->k:Z

    .line 126
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->J:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->l:Z

    .line 128
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->K:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->t:Z

    .line 130
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->L:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->m:Z

    .line 132
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->M:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->n:Z

    .line 134
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->N:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->o:Z

    .line 136
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->O:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->p:Z

    .line 138
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->P:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->q:Z

    .line 140
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->Q:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->r:Z

    .line 142
    iget-boolean v0, p1, Lcom/google/r/b/a/fi;->R:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->u:Z

    .line 145
    return-void

    .line 90
    :cond_1
    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 92
    :cond_3
    if-eqz v0, :cond_4

    if-eqz v3, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    .line 94
    :cond_5
    if-eqz v0, :cond_6

    if-eqz v3, :cond_6

    move v0, v1

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    .line 96
    :cond_7
    if-eqz v0, :cond_8

    if-eqz v3, :cond_8

    move v0, v1

    goto :goto_3

    :cond_8
    move v0, v2

    goto :goto_3

    .line 98
    :cond_9
    if-eqz v0, :cond_a

    if-eqz v3, :cond_a

    move v0, v1

    goto :goto_4

    :cond_a
    move v0, v2

    goto :goto_4

    .line 100
    :cond_b
    if-eqz v0, :cond_c

    if-eqz v3, :cond_c

    move v0, v1

    goto :goto_5

    :cond_c
    move v0, v2

    goto :goto_5

    .line 102
    :cond_d
    if-eqz v0, :cond_e

    if-eqz v3, :cond_e

    move v0, v1

    goto :goto_6

    :cond_e
    move v0, v2

    goto :goto_6

    .line 107
    :cond_f
    if-eqz v0, :cond_10

    if-nez v3, :cond_0

    :cond_10
    move v1, v2

    goto :goto_7
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 149
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "isGaiaLoginEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->a:Z

    .line 150
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isBicyclingEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->b:Z

    .line 151
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isReviewsEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->c:Z

    .line 152
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isPhotoUploadEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->d:Z

    .line 153
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isPanoramaPhotoUploadEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->e:Z

    .line 154
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isLastAvailableConnectionSearchEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->f:Z

    .line 155
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isPertileEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->i:Z

    .line 156
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isOfflineMapsEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->j:Z

    .line 157
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isBuildingExtrusionDisabled"

    .line 158
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isBuildingsForcedOpaque"

    .line 159
    invoke-static {v3}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isSavedOfferNotificationEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->s:Z

    .line 160
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isSpdyEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->g:Z

    .line 161
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isSpotlightPersonalizedSmartmapsEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->h:Z

    .line 162
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isGcoreFeedbackEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->k:Z

    .line 163
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isAndroidSynchronousSilentFeedbackEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->l:Z

    .line 164
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isAndroidGcoreSilentFeedbackEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->t:Z

    .line 165
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isBoxfixStreetViewEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->m:Z

    .line 166
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isPlaceReportPayloadViaAppTagEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->n:Z

    .line 167
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isChrzaszczButtonEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->o:Z

    .line 168
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isIAmHereEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->p:Z

    .line 169
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isUnifiedWireFormatTilesEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->q:Z

    .line 170
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isParticleFilterEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->r:Z

    .line 171
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isPowerSavingsInterstitialEnabled"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/shared/net/a/h;->u:Z

    .line 172
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
