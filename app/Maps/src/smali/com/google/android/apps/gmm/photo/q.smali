.class public Lcom/google/android/apps/gmm/photo/q;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/g/c;

.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/photo/d;",
            ">;"
        }
    .end annotation
.end field

.field final e:I

.field final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/g/c;ILjava/lang/String;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/g/c;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/photo/d;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    sget-object v0, Lcom/google/r/b/a/el;->bm:Lcom/google/r/b/a/el;

    sget-object v1, Lcom/google/r/b/a/b/bj;->b:Lcom/google/e/a/a/a/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 31
    iput-object p1, p0, Lcom/google/android/apps/gmm/photo/q;->a:Lcom/google/android/apps/gmm/base/g/c;

    .line 32
    iput p2, p0, Lcom/google/android/apps/gmm/photo/q;->e:I

    .line 33
    iput-object p3, p0, Lcom/google/android/apps/gmm/photo/q;->f:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Lcom/google/android/apps/gmm/photo/q;->d:Ljava/util/List;

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/shared/net/k;
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method protected final g_()Lcom/google/e/a/a/a/b;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 39
    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/bj;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 40
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/apps/gmm/photo/q;->e:I

    int-to-long v4, v1

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 44
    iget v0, p0, Lcom/google/android/apps/gmm/photo/q;->e:I

    if-nez v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/q;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v0, v1}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v6, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 46
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/q;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/q;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v1, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v7, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 67
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/photo/d;

    .line 68
    const/4 v4, 0x3

    new-instance v5, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/r/b/a/b/bj;->c:Lcom/google/e/a/a/a/d;

    invoke-direct {v5, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/photo/d;->b:Z

    if-eqz v1, :cond_3

    const/4 v1, -0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/photo/d;->b(I)[B

    move-result-object v0

    iget-object v1, v5, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v6, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    invoke-virtual {v2, v4, v5}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_1

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/q;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 54
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/apps/gmm/photo/q;->f:Ljava/lang/String;

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0, v1}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 58
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/photo/q;->d:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/photo/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/photo/d;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a()Lcom/google/e/a/a/a/b;

    move-result-object v0

    iget-object v1, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v1, v7, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 68
    :cond_3
    const v1, 0x124f80

    goto :goto_2

    .line 70
    :cond_4
    const/4 v1, 0x5

    sget-object v0, Lcom/google/e/a/a/a/b;->b:Ljava/lang/Boolean;

    iget-object v3, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 71
    return-object v2
.end method
