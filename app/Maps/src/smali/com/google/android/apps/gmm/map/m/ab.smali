.class public Lcom/google/android/apps/gmm/map/m/ab;
.super Lcom/google/android/apps/gmm/map/m/t;
.source "PG"


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/m/r;FF)V
    .locals 2

    .prologue
    .line 13
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/m/t;-><init>(ILcom/google/android/apps/gmm/map/m/r;FF)V

    .line 16
    const v0, 0x3d4ccccd    # 0.05f

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/ab;->g:F

    .line 19
    const-wide v0, 0x3fd657184ae74487L    # 0.3490658503988659

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/ab;->h:F

    .line 20
    return-void
.end method


# virtual methods
.method protected final a(F)F
    .locals 4

    .prologue
    .line 34
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 35
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method protected final a(Lcom/google/android/apps/gmm/map/m/k;I)F
    .locals 1

    .prologue
    .line 24
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .locals 1

    .prologue
    .line 9
    invoke-super/range {p0 .. p5}, Lcom/google/android/apps/gmm/map/m/t;->a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a()Z
    .locals 1

    .prologue
    .line 9
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/m/t;->a()Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/m/k;I)F
    .locals 1

    .prologue
    .line 29
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v0, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/ab;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->h(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    return v0
.end method

.method protected final d(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/ab;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->i(Lcom/google/android/apps/gmm/map/m/n;)V

    .line 47
    return-void
.end method

.method protected final f(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/ab;->a:Lcom/google/android/apps/gmm/map/m/r;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/r;->g(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    return v0
.end method
