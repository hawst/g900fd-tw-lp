.class final enum Lcom/google/android/apps/gmm/base/views/ag;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/base/views/ag;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/base/views/ag;

.field public static final enum b:Lcom/google/android/apps/gmm/base/views/ag;

.field private static final synthetic c:[Lcom/google/android/apps/gmm/base/views/ag;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ag;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/ag;->a:Lcom/google/android/apps/gmm/base/views/ag;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/base/views/ag;

    const-string v1, "CENTER"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/base/views/ag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/base/views/ag;->b:Lcom/google/android/apps/gmm/base/views/ag;

    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/apps/gmm/base/views/ag;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/ag;->a:Lcom/google/android/apps/gmm/base/views/ag;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/base/views/ag;->b:Lcom/google/android/apps/gmm/base/views/ag;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/base/views/ag;->c:[Lcom/google/android/apps/gmm/base/views/ag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method static a(I)Lcom/google/android/apps/gmm/base/views/ag;
    .locals 1

    .prologue
    .line 69
    packed-switch p0, :pswitch_data_0

    .line 71
    sget-object v0, Lcom/google/android/apps/gmm/base/views/ag;->a:Lcom/google/android/apps/gmm/base/views/ag;

    :goto_0
    return-object v0

    .line 70
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/base/views/ag;->b:Lcom/google/android/apps/gmm/base/views/ag;

    goto :goto_0

    .line 69
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/base/views/ag;
    .locals 1

    .prologue
    .line 62
    const-class v0, Lcom/google/android/apps/gmm/base/views/ag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/ag;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/base/views/ag;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/base/views/ag;->c:[Lcom/google/android/apps/gmm/base/views/ag;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/base/views/ag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/base/views/ag;

    return-object v0
.end method
