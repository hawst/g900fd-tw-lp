.class Lcom/google/android/apps/gmm/streetview/internal/l;
.super Lcom/google/android/apps/gmm/map/util/a/e;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/util/a/e",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/apps/gmm/streetview/internal/k;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(ILcom/google/android/apps/gmm/map/util/a/b;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/util/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 284
    const-string v0, "StreetView http cache"

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    .line 285
    return-void
.end method


# virtual methods
.method protected final synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 282
    check-cast p2, Lcom/google/android/apps/gmm/streetview/internal/k;

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/streetview/internal/k;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/gmm/streetview/internal/k;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/gmm/streetview/internal/k;->a:Ljava/io/File;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to delete cache file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    return-void
.end method
