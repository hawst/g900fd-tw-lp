.class public Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/r;
.implements Lcom/google/android/apps/gmm/prefetchcache/b/k;
.implements Lcom/google/android/apps/gmm/prefetchcache/u;


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/apps/gmm/prefetchcache/z;

.field b:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/y;",
            ">;"
        }
    .end annotation
.end field

.field c:Lcom/google/android/libraries/curvular/ae;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ae",
            "<",
            "Lcom/google/android/apps/gmm/prefetchcache/b/h;",
            ">;"
        }
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/prefetchcache/b/i;

.field private f:Ljava/lang/String;

.field private g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 63
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->m:Z

    return-void
.end method

.method static a(Ljava/lang/String;)Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;
    .locals 3
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;-><init>()V

    .line 72
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 73
    const-string v2, "area_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    return-object v0
.end method

.method private declared-synchronized p()V
    .locals 6

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->isResumed()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 251
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 226
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    if-eqz v0, :cond_2

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/z;->d()V

    .line 230
    :cond_2
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v2

    .line 231
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 233
    :goto_1
    sget-object v3, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->e:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x15

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "new area validation: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 234
    new-instance v3, Lcom/google/android/apps/gmm/prefetchcache/z;

    new-instance v4, Lcom/google/android/apps/gmm/prefetchcache/q;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/prefetchcache/q;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;)V

    invoke-direct {v3, v1, v2, v0, v4}, Lcom/google/android/apps/gmm/prefetchcache/z;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/prefetchcache/aa;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/z;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 221
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 231
    :cond_3
    :try_start_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private q()Z
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    .line 255
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    .line 256
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 163
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->e:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->m:Z

    if-eqz v0, :cond_1

    const-string v0, "off"

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "camera changed: finger "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " screen"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 164
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->m:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 165
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->p()V

    .line 167
    :cond_0
    return-void

    .line 163
    :cond_1
    const-string v0, "on"

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/ab;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 151
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/ab;->a:Lcom/google/android/apps/gmm/map/j/ac;

    .line 152
    sget-object v1, Lcom/google/android/apps/gmm/map/j/ac;->a:Lcom/google/android/apps/gmm/map/j/ac;

    if-ne v0, v1, :cond_1

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->m:Z

    .line 159
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    sget-object v1, Lcom/google/android/apps/gmm/map/j/ac;->b:Lcom/google/android/apps/gmm/map/j/ac;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/map/j/ac;->c:Lcom/google/android/apps/gmm/map/j/ac;

    if-ne v0, v1, :cond_0

    .line 156
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->m:Z

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/prefetchcache/api/g;)V
    .locals 3
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 187
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/prefetchcache/api/g;->a:Lcom/google/android/apps/gmm/prefetchcache/api/h;

    sget-object v1, Lcom/google/android/apps/gmm/prefetchcache/api/h;->c:Lcom/google/android/apps/gmm/prefetchcache/api/h;

    if-ne v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->cj:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/z;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 182
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->dismiss()V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->f:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->g:Lcom/google/android/apps/gmm/prefetchcache/OfflineAreaNameInputDialogFragment;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_0

    .line 184
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/p;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/prefetchcache/p;-><init>(Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 214
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 293
    :goto_0
    return-void

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/prefetchcache/z;->a()Z

    move-result v0

    if-nez v0, :cond_2

    .line 279
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Viewport re-validation is needed: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->p()V

    goto :goto_0

    .line 283
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p()Lcom/google/android/apps/gmm/prefetchcache/api/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/prefetchcache/api/f;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 284
    sget-object v0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->e:Ljava/lang/String;

    const-string v1, "OfflineAreaManager is busy"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 287
    :cond_3
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->f:Ljava/lang/String;

    .line 288
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->a:Lcom/google/android/apps/gmm/prefetchcache/z;

    .line 290
    iget-object v0, v0, Lcom/google/android/apps/gmm/prefetchcache/z;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->f:Ljava/lang/String;

    .line 289
    invoke-static {}, Lcom/google/r/b/a/a/al;->newBuilder()Lcom/google/r/b/a/a/an;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v5, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/q;->d()Lcom/google/r/b/a/hw;

    move-result-object v4

    if-nez v4, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget-object v5, v3, Lcom/google/r/b/a/a/an;->b:Lcom/google/n/ao;

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v5, Lcom/google/n/ao;->d:Z

    iget v4, v3, Lcom/google/r/b/a/a/an;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/r/b/a/a/an;->a:I

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/q;->d()Lcom/google/r/b/a/hw;

    move-result-object v0

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget-object v4, v3, Lcom/google/r/b/a/a/an;->c:Lcom/google/n/ao;

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v4, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/r/b/a/a/an;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Lcom/google/r/b/a/a/an;->a:I

    invoke-static {}, Lcom/google/r/b/a/a/af;->newBuilder()Lcom/google/r/b/a/a/ah;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/r/b/a/a/an;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/al;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_6
    iget-object v3, v4, Lcom/google/r/b/a/a/ah;->b:Lcom/google/n/ao;

    iget-object v5, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v10, v3, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v4, Lcom/google/r/b/a/a/ah;->a:I

    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v0, v4, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lcom/google/r/b/a/a/ah;->a:I

    iput-object v2, v4, Lcom/google/r/b/a/a/ah;->c:Ljava/lang/Object;

    sget-object v0, Lcom/google/r/b/a/a/ai;->c:Lcom/google/r/b/a/a/ai;

    .line 292
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v2, v4, Lcom/google/r/b/a/a/ah;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v4, Lcom/google/r/b/a/a/ah;->a:I

    iget v0, v0, Lcom/google/r/b/a/a/ai;->f:I

    iput v0, v4, Lcom/google/r/b/a/a/ah;->d:I

    invoke-virtual {v4}, Lcom/google/r/b/a/a/ah;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/a/af;

    .line 288
    invoke-static {v0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;->a(Lcom/google/r/b/a/a/af;)Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSaveFragment;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    :goto_0
    return-void

    .line 301
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->b()V

    goto :goto_0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 128
    sget-object v0, Lcom/google/b/f/t;->ci:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 306
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    :goto_0
    return-void

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/api/g;

    sget-object v2, Lcom/google/android/apps/gmm/prefetchcache/api/h;->c:Lcom/google/android/apps/gmm/prefetchcache/api/h;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/prefetchcache/api/g;-><init>(Lcom/google/android/apps/gmm/prefetchcache/api/h;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 315
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    :goto_0
    return-void

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 81
    if-eqz p1, :cond_0

    .line 82
    :goto_0
    const-string v0, "area_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->f:Ljava/lang/String;

    .line 83
    return-void

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 133
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/prefetchcache/api/g;

    sget-object v2, Lcom/google/android/apps/gmm/prefetchcache/api/h;->b:Lcom/google/android/apps/gmm/prefetchcache/api/h;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/prefetchcache/api/g;-><init>(Lcom/google/android/apps/gmm/prefetchcache/api/h;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/o;->b(Lcom/google/android/apps/gmm/map/r;)V

    .line 140
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 91
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->d:Lcom/google/android/apps/gmm/prefetchcache/b/i;

    if-nez v0, :cond_2

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/prefetchcache/b/i;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/apps/gmm/prefetchcache/b/i;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/prefetchcache/b/k;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->d:Lcom/google/android/apps/gmm/prefetchcache/b/i;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/base/f/m;

    .line 97
    invoke-virtual {v0, v1, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->b:Lcom/google/android/libraries/curvular/ae;

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/prefetchcache/a/h;

    .line 99
    invoke-virtual {v0, v1, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->c:Lcom/google/android/libraries/curvular/ae;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->b:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->d:Lcom/google/android/apps/gmm/prefetchcache/b/i;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->c:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->b:Lcom/google/android/libraries/curvular/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->d:Lcom/google/android/apps/gmm/prefetchcache/b/i;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 105
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/o;->a(Lcom/google/android/apps/gmm/map/r;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 108
    invoke-direct {p0}, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->p()V

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->b:Lcom/google/android/libraries/curvular/ae;

    .line 111
    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->x:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    sget-object v2, Lcom/google/android/apps/gmm/base/activities/ac;->a:Lcom/google/android/apps/gmm/base/activities/ac;

    iput-object v2, v1, Lcom/google/android/apps/gmm/base/activities/p;->y:Lcom/google/android/apps/gmm/base/activities/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->c:Lcom/google/android/libraries/curvular/ae;

    .line 112
    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->A:Landroid/view/View;

    .line 113
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v6, v1, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    .line 114
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 115
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 116
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v4, v1, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 117
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->B:I

    .line 118
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v6, v1, Lcom/google/android/apps/gmm/base/activities/p;->D:Z

    .line 119
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v5, v1, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    const-class v1, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;

    .line 120
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    .line 121
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 122
    invoke-static {}, Lcom/google/android/apps/gmm/base/activities/ag;->c()Lcom/google/android/apps/gmm/base/activities/ag;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->n:Lcom/google/android/apps/gmm/base/activities/ag;

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 124
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 145
    const-string v0, "area_name"

    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetchcache/ManualOfflineSelectFragment;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    return-void
.end method
