.class public Lcom/google/android/apps/gmm/reportmapissue/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:Lcom/google/android/apps/gmm/reportmapissue/c;

.field b:Ljava/lang/String;

.field c:Lcom/google/android/apps/gmm/feedback/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/apps/gmm/reportmapissue/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/reportmapissue/a;->d:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/reportmapissue/c;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    .line 89
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/r/a/w;D)Lcom/google/android/apps/gmm/map/b/a/n;
    .locals 5

    .prologue
    .line 155
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    .line 158
    const-wide/16 v0, 0x0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 162
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    invoke-static {v3, v0, v1}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v0

    if-gez v0, :cond_0

    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    move v1, v0

    .line 163
    :goto_0
    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/n;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v4

    .line 164
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v0

    invoke-direct {v3, v4, v0}, Lcom/google/android/apps/gmm/map/b/a/n;-><init>(II)V

    .line 166
    return-object v3

    :cond_0
    move v1, v0

    .line 162
    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;)Lcom/google/android/apps/gmm/reportmapissue/a;
    .locals 10

    .prologue
    .line 60
    new-instance v6, Lcom/google/android/apps/gmm/reportmapissue/c;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/reportmapissue/c;-><init>()V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->d()Z

    move-result v0

    iput-boolean v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->j:Z

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    .line 65
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    iput-object v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 66
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    .line 67
    const/4 v0, -0x1

    iput v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->n:I

    .line 68
    const/4 v0, -0x1

    iput v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->m:I

    .line 69
    const/4 v0, 0x1

    iput v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->l:I

    .line 70
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->b:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput-object v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->k:[B

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    .line 74
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/o;->c()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/o;->K:F

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(FLcom/google/android/apps/gmm/map/b/a/bb;)V

    :goto_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    .line 75
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iput v1, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->e:I

    .line 76
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v2

    const-wide v4, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v2

    const-wide v4, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v0, v2

    sub-int v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->f:I

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/w;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v4

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v2

    invoke-direct {v1, v4, v2}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    .line 80
    invoke-static {v3}, Lcom/google/android/apps/gmm/map/b/a/be;->a(I)Lcom/google/android/apps/gmm/map/b/a/be;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/b/a/w;-><init>(Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/map/b/a/be;IZZ)V

    iput-object v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->d:Lcom/google/android/apps/gmm/map/b/a/w;

    .line 82
    iget-object v1, p2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-eqz v1, :cond_0

    iget v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->z:I

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    :goto_2
    iput-object v0, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->c:Ljava/lang/String;

    .line 84
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/a;

    invoke-direct {v0, v6}, Lcom/google/android/apps/gmm/reportmapissue/a;-><init>(Lcom/google/android/apps/gmm/reportmapissue/c;)V

    return-object v0

    .line 74
    :cond_1
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const v3, 0x3c8efa35

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f000000    # 0.5f

    iget v4, v0, Lcom/google/android/apps/gmm/map/f/o;->h:F

    div-float/2addr v3, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    float-to-double v8, v3

    add-double/2addr v4, v8

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    div-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    double-to-float v3, v4

    sub-float v2, v3, v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const v4, 0x42652ee1

    mul-float/2addr v2, v4

    add-float/2addr v2, v3

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(FLcom/google/android/apps/gmm/map/b/a/bb;)V

    goto/16 :goto_0

    .line 82
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->i:I

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v2, v2, v0

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/n;

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->a()I

    move-result v4

    iget-object v5, p1, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/map/b/a/y;->c()I

    move-result v5

    invoke-direct {v0, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/n;-><init>(II)V

    const-wide v4, 0x408f400000000000L    # 1000.0

    sub-double v4, v2, v4

    invoke-static {v1, v4, v5}, Lcom/google/android/apps/gmm/reportmapissue/a;->a(Lcom/google/android/apps/gmm/map/r/a/w;D)Lcom/google/android/apps/gmm/map/b/a/n;

    move-result-object v4

    const-wide v8, 0x408f400000000000L    # 1000.0

    add-double/2addr v2, v8

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/reportmapissue/a;->a(Lcom/google/android/apps/gmm/map/r/a/w;D)Lcom/google/android/apps/gmm/map/b/a/n;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/apps/gmm/map/b/a/n;

    const/4 v5, 0x0

    aput-object v0, v3, v5

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    invoke-static {v4, v3, v2, v0}, Lcom/google/android/apps/gmm/directions/f/d/b;->a(Lcom/google/android/apps/gmm/map/b/a/n;[Lcom/google/android/apps/gmm/map/b/a/n;Lcom/google/android/apps/gmm/map/b/a/n;Lcom/google/maps/g/a/hm;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/base/activities/c;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 206
    if-gtz p2, :cond_0

    const/16 v1, 0x23

    if-gt p2, v1, :cond_7

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iput p2, v1, Lcom/google/android/apps/gmm/reportmapissue/c;->g:I

    .line 213
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iput-object p3, v1, Lcom/google/android/apps/gmm/reportmapissue/c;->h:Ljava/lang/String;

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    invoke-static {p4}, Lcom/google/b/a/bo;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object p4, v0

    :cond_1
    iput-object p4, v1, Lcom/google/android/apps/gmm/reportmapissue/c;->i:Ljava/lang/String;

    .line 216
    new-instance v4, Lcom/google/android/apps/gmm/reportmapissue/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    new-instance v5, Lcom/google/android/apps/gmm/reportmapissue/b;

    invoke-direct {v5, p0, p1}, Lcom/google/android/apps/gmm/reportmapissue/b;-><init>(Lcom/google/android/apps/gmm/reportmapissue/a;Lcom/google/android/apps/gmm/base/activities/c;)V

    invoke-direct {v4, v1, v5}, Lcom/google/android/apps/gmm/reportmapissue/d;-><init>(Lcom/google/android/apps/gmm/reportmapissue/c;Lcom/google/android/apps/gmm/reportmapissue/e;)V

    .line 229
    if-nez p5, :cond_5

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_8

    :cond_2
    move v1, v3

    :goto_1
    if-eqz v1, :cond_3

    .line 237
    const-string v1, "<event-track></event-track>"

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->b:Ljava/lang/String;

    .line 242
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    if-eqz v1, :cond_4

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->c:Lcom/google/android/apps/gmm/feedback/a/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/feedback/a/f;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 244
    if-eqz v1, :cond_4

    .line 245
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 246
    sget-object v6, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v1, v6, v2, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 247
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .line 251
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->b:Ljava/lang/String;

    iput-object v1, v4, Lcom/google/android/apps/gmm/reportmapissue/d;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/apps/gmm/reportmapissue/d;->b:[B

    .line 255
    :cond_5
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_6

    .line 256
    sget v0, Lcom/google/android/apps/gmm/l;->mQ:I

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 257
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 258
    sget-object v0, Lcom/google/android/apps/gmm/reportmapissue/a;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Filing lane RMI issue: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 261
    :cond_6
    return-void

    .line 210
    :cond_7
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iput v2, v1, Lcom/google/android/apps/gmm/reportmapissue/c;->g:I

    goto/16 :goto_0

    :cond_8
    move v1, v2

    .line 236
    goto :goto_1
.end method
