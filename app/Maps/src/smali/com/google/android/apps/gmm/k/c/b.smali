.class public Lcom/google/android/apps/gmm/k/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/k/c/a;


# instance fields
.field private final a:Lcom/google/r/b/a/zq;

.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;

.field private final d:Lcom/google/android/apps/gmm/z/b/l;

.field private final e:Lcom/google/android/apps/gmm/z/b/l;

.field private final f:Lcom/google/android/apps/gmm/z/b/l;

.field private final g:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/zq;Ljava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/k/c/b;->b:Ljava/lang/Runnable;

    .line 32
    iput-object p3, p0, Lcom/google/android/apps/gmm/k/c/b;->c:Ljava/lang/Runnable;

    .line 33
    iput-object p1, p0, Lcom/google/android/apps/gmm/k/c/b;->a:Lcom/google/r/b/a/zq;

    .line 34
    sget-object v1, Lcom/google/android/apps/gmm/k/c/c;->a:[I

    iget-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->a:Lcom/google/r/b/a/zq;

    iget v0, v0, Lcom/google/r/b/a/zq;->i:I

    invoke-static {v0}, Lcom/google/r/b/a/zt;->a(I)Lcom/google/r/b/a/zt;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/r/b/a/zt;->a:Lcom/google/r/b/a/zt;

    :cond_0
    invoke-virtual {v0}, Lcom/google/r/b/a/zt;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 49
    iput-object v2, p0, Lcom/google/android/apps/gmm/k/c/b;->d:Lcom/google/android/apps/gmm/z/b/l;

    .line 50
    iput-object v2, p0, Lcom/google/android/apps/gmm/k/c/b;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 51
    iput-object v2, p0, Lcom/google/android/apps/gmm/k/c/b;->f:Lcom/google/android/apps/gmm/z/b/l;

    .line 52
    iput-object v2, p0, Lcom/google/android/apps/gmm/k/c/b;->g:Lcom/google/android/apps/gmm/z/b/l;

    .line 55
    :goto_0
    return-void

    .line 36
    :pswitch_0
    sget-object v0, Lcom/google/b/f/t;->fd:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->d:Lcom/google/android/apps/gmm/z/b/l;

    .line 37
    sget-object v0, Lcom/google/b/f/t;->fe:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 38
    sget-object v0, Lcom/google/b/f/t;->ff:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->f:Lcom/google/android/apps/gmm/z/b/l;

    .line 40
    sget-object v0, Lcom/google/b/f/t;->fg:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->g:Lcom/google/android/apps/gmm/z/b/l;

    goto :goto_0

    .line 43
    :pswitch_1
    sget-object v0, Lcom/google/b/f/t;->eZ:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->d:Lcom/google/android/apps/gmm/z/b/l;

    .line 44
    sget-object v0, Lcom/google/b/f/t;->fa:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->e:Lcom/google/android/apps/gmm/z/b/l;

    .line 45
    sget-object v0, Lcom/google/b/f/t;->fb:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->f:Lcom/google/android/apps/gmm/z/b/l;

    .line 46
    sget-object v0, Lcom/google/b/f/t;->fc:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->g:Lcom/google/android/apps/gmm/z/b/l;

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 63
    iget-object v2, p0, Lcom/google/android/apps/gmm/k/c/b;->a:Lcom/google/r/b/a/zq;

    iget-object v0, v2, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/r/b/a/zq;->f:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    iget-object v2, p0, Lcom/google/android/apps/gmm/k/c/b;->a:Lcom/google/r/b/a/zq;

    iget-object v0, v2, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/r/b/a/zq;->g:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 73
    iget-object v2, p0, Lcom/google/android/apps/gmm/k/c/b;->a:Lcom/google/r/b/a/zq;

    iget-object v0, v2, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/r/b/a/zq;->h:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 85
    const/4 v0, 0x0

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->d:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->g:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->e:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/k/c/b;->f:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
