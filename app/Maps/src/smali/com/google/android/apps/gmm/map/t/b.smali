.class public final enum Lcom/google/android/apps/gmm/map/t/b;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/t/b;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum b:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum c:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum d:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum e:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum f:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum g:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum h:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum i:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum j:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum k:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum l:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum m:Lcom/google/android/apps/gmm/map/t/b;

.field public static final enum n:Lcom/google/android/apps/gmm/map/t/b;

.field private static final synthetic r:[Lcom/google/android/apps/gmm/map/t/b;


# instance fields
.field public final o:J

.field public final p:Lcom/google/android/apps/gmm/map/util/a;

.field public final q:Lcom/google/android/apps/gmm/map/internal/c/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    .line 26
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "NORMAL"

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    .line 32
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "HYBRID"

    const/4 v3, 0x1

    sget-wide v4, Lcom/google/android/apps/gmm/map/t/i;->d:J

    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    .line 33
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/a;->a([J)Lcom/google/android/apps/gmm/map/util/a;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    .line 46
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "NIGHT"

    const/4 v3, 0x2

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    .line 51
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "SATELLITE"

    const/4 v3, 0x3

    sget-wide v4, Lcom/google/android/apps/gmm/map/t/i;->b:J

    sget-wide v6, Lcom/google/android/apps/gmm/map/t/i;->c:J

    or-long/2addr v4, v6

    const/4 v0, 0x1

    new-array v0, v0, [J

    const/4 v6, 0x0

    const-wide/16 v8, 0x9

    aput-wide v8, v0, v6

    .line 52
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/a;->a([J)Lcom/google/android/apps/gmm/map/util/a;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->d:Lcom/google/android/apps/gmm/map/t/b;

    .line 57
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "TERRAIN"

    const/4 v3, 0x4

    sget-wide v4, Lcom/google/android/apps/gmm/map/t/i;->b:J

    sget-wide v6, Lcom/google/android/apps/gmm/map/t/i;->c:J

    or-long/2addr v4, v6

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->b:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->e:Lcom/google/android/apps/gmm/map/t/b;

    .line 63
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "NONE"

    const/4 v3, 0x5

    const-wide/16 v4, 0x0

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->b:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->a:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->f:Lcom/google/android/apps/gmm/map/t/b;

    .line 72
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "ROADMAP"

    const/4 v3, 0x6

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->b:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->g:Lcom/google/android/apps/gmm/map/t/b;

    .line 78
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "NAVIGATION"

    const/4 v3, 0x7

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->h:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->h:Lcom/google/android/apps/gmm/map/t/b;

    .line 83
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "NAVIGATION_LOW_LIGHT"

    const/16 v3, 0x8

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->i:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->i:Lcom/google/android/apps/gmm/map/t/b;

    .line 89
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "HYBRID_LEGEND"

    const/16 v3, 0x9

    sget-wide v4, Lcom/google/android/apps/gmm/map/t/i;->d:J

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->c:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->j:Lcom/google/android/apps/gmm/map/t/b;

    .line 95
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "SATELLITE_LEGEND"

    const/16 v3, 0xa

    sget-wide v4, Lcom/google/android/apps/gmm/map/t/i;->b:J

    sget-wide v6, Lcom/google/android/apps/gmm/map/t/i;->c:J

    or-long/2addr v4, v6

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->c:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->k:Lcom/google/android/apps/gmm/map/t/b;

    .line 104
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "TERRAIN_LEGEND"

    const/16 v3, 0xb

    const-wide/16 v4, -0x1

    const/4 v0, 0x4

    new-array v0, v0, [J

    fill-array-data v0, :array_1

    .line 105
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/a;->a([J)Lcom/google/android/apps/gmm/map/util/a;

    move-result-object v6

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->e:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->l:Lcom/google/android/apps/gmm/map/t/b;

    .line 113
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "NON_ROADMAP"

    const/16 v3, 0xc

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->d:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->m:Lcom/google/android/apps/gmm/map/t/b;

    .line 118
    new-instance v1, Lcom/google/android/apps/gmm/map/t/b;

    const-string v2, "ROADMAP_MUTED"

    const/16 v3, 0xd

    const-wide/16 v4, -0x1

    sget-object v6, Lcom/google/android/apps/gmm/map/util/a;->a:Lcom/google/android/apps/gmm/map/util/a;

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/c/ac;->f:Lcom/google/android/apps/gmm/map/internal/c/ac;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/t/b;-><init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V

    sput-object v1, Lcom/google/android/apps/gmm/map/t/b;->n:Lcom/google/android/apps/gmm/map/t/b;

    .line 22
    const/16 v0, 0xe

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/t/b;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->a:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->b:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->c:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->d:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->e:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->f:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->g:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->h:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->i:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->j:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->k:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->l:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->m:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/apps/gmm/map/t/b;->n:Lcom/google/android/apps/gmm/map/t/b;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/map/t/b;->r:[Lcom/google/android/apps/gmm/map/t/b;

    .line 182
    invoke-static {}, Lcom/google/android/apps/gmm/map/t/b;->values()[Lcom/google/android/apps/gmm/map/t/b;

    return-void

    .line 32
    nop

    :array_0
    .array-data 8
        0x2
        0x8
        0x5
        0x9
    .end array-data

    .line 104
    :array_1
    .array-data 8
        0x2
        0x8
        0x5
        0x9
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;IJLcom/google/android/apps/gmm/map/util/a;Lcom/google/android/apps/gmm/map/internal/c/ac;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Lcom/google/android/apps/gmm/map/util/a;",
            "Lcom/google/android/apps/gmm/map/internal/c/ac;",
            ")V"
        }
    .end annotation

    .prologue
    .line 142
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 143
    iput-wide p3, p0, Lcom/google/android/apps/gmm/map/t/b;->o:J

    .line 144
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/t/b;->p:Lcom/google/android/apps/gmm/map/util/a;

    .line 145
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/t/b;->q:Lcom/google/android/apps/gmm/map/internal/c/ac;

    .line 146
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/t/b;
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/map/t/b;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/b;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/t/b;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/apps/gmm/map/t/b;->r:[Lcom/google/android/apps/gmm/map/t/b;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/t/b;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/t/b;

    return-object v0
.end method
