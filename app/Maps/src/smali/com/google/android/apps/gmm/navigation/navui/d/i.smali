.class public Lcom/google/android/apps/gmm/navigation/navui/d/i;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/d/a;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/directions/h/h;

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Lcom/google/android/apps/gmm/directions/views/d;

.field private final f:Z

.field private final g:Ljava/lang/CharSequence;

.field private final h:Ljava/lang/CharSequence;

.field private final i:Ljava/lang/String;

.field private final j:Z

.field private final k:Lcom/google/android/apps/gmm/base/k/m;

.field private final l:Z

.field private final m:Lcom/google/android/apps/gmm/directions/views/d;

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field private final q:Lcom/google/android/apps/gmm/base/views/c/d;

.field private final r:Z

.field private final s:Ljava/lang/CharSequence;

.field private final t:Ljava/lang/CharSequence;

.field private final u:Ljava/lang/CharSequence;

.field private final v:Z

.field private final w:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/d/a;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->a()Lcom/google/android/apps/gmm/directions/h/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->a:Lcom/google/android/apps/gmm/directions/h/h;

    .line 44
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->e()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->b:Z

    .line 45
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->f()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->c:Z

    .line 46
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->g()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->d:Z

    .line 47
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->h()Lcom/google/android/apps/gmm/directions/views/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->e:Lcom/google/android/apps/gmm/directions/views/d;

    .line 48
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->i()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->f:Z

    .line 49
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->j()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->g:Ljava/lang/CharSequence;

    .line 50
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->k()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->h:Ljava/lang/CharSequence;

    .line 51
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->i:Ljava/lang/String;

    .line 52
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->m()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->j:Z

    .line 53
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->n()Lcom/google/android/apps/gmm/base/k/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->k:Lcom/google/android/apps/gmm/base/k/m;

    .line 54
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->o()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->l:Z

    .line 55
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->p()Lcom/google/android/apps/gmm/directions/views/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->m:Lcom/google/android/apps/gmm/directions/views/d;

    .line 56
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->q()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->n:Z

    .line 57
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->r()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->o:Z

    .line 58
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->s()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->p:Z

    .line 59
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->t()Lcom/google/android/apps/gmm/base/views/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->q:Lcom/google/android/apps/gmm/base/views/c/d;

    .line 60
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->u()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->r:Z

    .line 61
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->v()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->s:Ljava/lang/CharSequence;

    .line 62
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->w()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->t:Ljava/lang/CharSequence;

    .line 63
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->x()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->u:Ljava/lang/CharSequence;

    .line 64
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->y()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->v:Z

    .line 65
    invoke-interface {p1}, Lcom/google/android/apps/gmm/navigation/navui/d/a;->z()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->w:Z

    .line 66
    return-void
.end method


# virtual methods
.method public final A()Lcom/google/android/libraries/curvular/ct;
    .locals 1

    .prologue
    .line 219
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Lcom/google/android/apps/gmm/directions/h/h;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->a:Lcom/google/android/apps/gmm/directions/h/h;

    return-object v0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 208
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final b()Lcom/google/android/apps/gmm/navigation/navui/d/a;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/directions/views/d;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->e:Lcom/google/android/apps/gmm/directions/views/d;

    return-object v0
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final j()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->g:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final k()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->h:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->j:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/base/k/m;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->k:Lcom/google/android/apps/gmm/base/k/m;

    return-object v0
.end method

.method public final o()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 138
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/gmm/directions/views/d;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->m:Lcom/google/android/apps/gmm/directions/views/d;

    return-object v0
.end method

.method public final q()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final r()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->o:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final s()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 159
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->p:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final t()Lcom/google/android/apps/gmm/base/views/c/d;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->q:Lcom/google/android/apps/gmm/base/views/c/d;

    return-object v0
.end method

.method public final u()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->r:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final v()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->s:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final w()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->t:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final x()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->u:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final y()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 193
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->v:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final z()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 198
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/i;->w:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
