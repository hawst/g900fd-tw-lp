.class public Lcom/google/android/apps/gmm/place/ownerresponse/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/ownerresponse/a/a;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field final b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

.field final c:Lcom/google/android/apps/gmm/place/ownerresponse/j;

.field final d:Lcom/google/maps/g/pc;

.field e:Ljava/lang/String;

.field f:Landroid/app/Dialog;

.field final g:Lcom/google/android/apps/gmm/place/ownerresponse/j;

.field private final h:Lcom/google/r/b/a/acq;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;Lcom/google/android/apps/gmm/place/ownerresponse/j;Lcom/google/maps/g/pc;Lcom/google/r/b/a/acq;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/place/ownerresponse/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    new-instance v0, Lcom/google/android/apps/gmm/place/ownerresponse/g;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/ownerresponse/g;-><init>(Lcom/google/android/apps/gmm/place/ownerresponse/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->g:Lcom/google/android/apps/gmm/place/ownerresponse/j;

    .line 60
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

    .line 62
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->c:Lcom/google/android/apps/gmm/place/ownerresponse/j;

    .line 63
    iput-object p4, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    .line 64
    iput-object p5, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    .line 65
    iput-object p6, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->e:Ljava/lang/String;

    .line 66
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/place/facepile/a/a;
    .locals 1

    .prologue
    .line 80
    new-instance v0, Lcom/google/android/apps/gmm/place/ownerresponse/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/ownerresponse/c;-><init>(Lcom/google/android/apps/gmm/place/ownerresponse/b;)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 234
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->i:Ljava/lang/String;

    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    invoke-virtual {v0}, Lcom/google/maps/g/pc;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    iget v0, v0, Lcom/google/maps/g/pc;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v1, 0x2000

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    .line 138
    iget-object v0, v0, Lcom/google/r/b/a/acq;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/mk;->g()Lcom/google/maps/g/mk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mk;

    iget-object v1, v0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    .line 137
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 138
    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v2, v0, Lcom/google/maps/g/mk;->c:Ljava/lang/Object;

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method public final g()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    iget v0, v0, Lcom/google/r/b/a/acq;->a:I

    and-int/lit16 v0, v0, 0x2000

    const/16 v3, 0x2000

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    .line 152
    iget-object v0, v0, Lcom/google/r/b/a/acq;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/mk;->g()Lcom/google/maps/g/mk;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mk;

    iget v0, v0, Lcom/google/maps/g/mk;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    .line 153
    iget-object v0, v0, Lcom/google/r/b/a/acq;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/mk;->g()Lcom/google/maps/g/mk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mk;

    iget-object v0, v0, Lcom/google/maps/g/mk;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->i()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 154
    :cond_0
    :goto_2
    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/k;

    sget-object v2, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v3, Lcom/google/android/apps/gmm/f;->cD:I

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    return-object v1

    :cond_1
    move v0, v2

    .line 151
    goto :goto_0

    :cond_2
    move v0, v2

    .line 152
    goto :goto_1

    .line 153
    :cond_3
    const-string v0, ""

    goto :goto_2
.end method

.method public final h()Ljava/lang/String;
    .locals 3

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    iget v0, v0, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v1, 0x800

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    iget-object v0, v0, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ju;->g()Lcom/google/maps/g/ju;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ju;

    iget-object v1, v0, Lcom/google/maps/g/ju;->b:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_1
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v2, v0, Lcom/google/maps/g/ju;->b:Ljava/lang/Object;

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method public final i()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    iget-boolean v0, v0, Lcom/google/r/b/a/acq;->t:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->b:Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->dismiss()V

    .line 198
    :cond_1
    :goto_0
    return-object v3

    .line 180
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/place/ownerresponse/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/ownerresponse/d;-><init>(Lcom/google/android/apps/gmm/place/ownerresponse/b;)V

    .line 190
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->e:Ljava/lang/String;

    .line 191
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/apps/gmm/l;->bW:I

    :goto_2
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->pg:I

    .line 194
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->iX:I

    .line 195
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 196
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->f:Landroid/app/Dialog;

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->f:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 191
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    sget v0, Lcom/google/android/apps/gmm/l;->bX:I

    goto :goto_2
.end method

.method public final k()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 209
    invoke-static {}, Lcom/google/r/b/a/vr;->newBuilder()Lcom/google/r/b/a/vt;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    .line 210
    iget-object v0, v0, Lcom/google/r/b/a/acq;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/mk;->g()Lcom/google/maps/g/mk;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mk;

    invoke-virtual {v0}, Lcom/google/maps/g/mk;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v1, Lcom/google/r/b/a/vt;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v1, Lcom/google/r/b/a/vt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/vt;->c:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    .line 211
    invoke-virtual {v0}, Lcom/google/maps/g/pc;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v1, Lcom/google/r/b/a/vt;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/r/b/a/vt;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/vt;->b:Ljava/lang/Object;

    .line 212
    invoke-virtual {v1}, Lcom/google/r/b/a/vt;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/vr;

    .line 213
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/place/ownerresponse/e;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/place/ownerresponse/e;-><init>(Lcom/google/android/apps/gmm/place/ownerresponse/b;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 229
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Lcom/google/android/libraries/curvular/cf;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->nm:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 268
    :goto_0
    return-object v4

    .line 246
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->i:Ljava/lang/String;

    .line 247
    invoke-static {}, Lcom/google/r/b/a/wf;->newBuilder()Lcom/google/r/b/a/wh;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    .line 248
    iget-object v0, v0, Lcom/google/r/b/a/acq;->u:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/mk;->g()Lcom/google/maps/g/mk;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/mk;

    invoke-virtual {v0}, Lcom/google/maps/g/mk;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v3, v1, Lcom/google/r/b/a/wh;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v1, Lcom/google/r/b/a/wh;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/wh;->d:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    .line 249
    invoke-virtual {v0}, Lcom/google/maps/g/pc;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v1, Lcom/google/r/b/a/wh;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/r/b/a/wh;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/wh;->b:Ljava/lang/Object;

    .line 250
    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v0, v1, Lcom/google/r/b/a/wh;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v1, Lcom/google/r/b/a/wh;->a:I

    iput-object v2, v1, Lcom/google/r/b/a/wh;->c:Ljava/lang/Object;

    .line 251
    invoke-virtual {v1}, Lcom/google/r/b/a/wh;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/wf;

    .line 252
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/place/ownerresponse/f;

    invoke-direct {v3, p0, v2}, Lcom/google/android/apps/gmm/place/ownerresponse/f;-><init>(Lcom/google/android/apps/gmm/place/ownerresponse/b;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v1, v0, v3, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    goto :goto_0
.end method

.method public final m()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->d:Lcom/google/maps/g/pc;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->h:Lcom/google/r/b/a/acq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->e:Ljava/lang/String;

    .line 288
    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->a(Lcom/google/maps/g/pc;Lcom/google/r/b/a/acq;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;

    move-result-object v0

    .line 289
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->g:Lcom/google/android/apps/gmm/place/ownerresponse/j;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/ownerresponse/SubmitOwnerResponseFragment;->d:Lcom/google/android/apps/gmm/place/ownerresponse/j;

    .line 290
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/ownerresponse/b;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 291
    return-object v3
.end method
