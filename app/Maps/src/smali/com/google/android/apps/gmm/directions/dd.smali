.class Lcom/google/android/apps/gmm/directions/dd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/directions/ck;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/cm;

.field final synthetic b:Lcom/google/android/apps/gmm/directions/bt;

.field final synthetic c:Lcom/google/android/apps/gmm/directions/dc;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/dc;Lcom/google/android/apps/gmm/directions/cm;Lcom/google/android/apps/gmm/directions/bt;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/dd;->c:Lcom/google/android/apps/gmm/directions/dc;

    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/dd;->a:Lcom/google/android/apps/gmm/directions/cm;

    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/dd;->b:Lcom/google/android/apps/gmm/directions/bt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 97
    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/dd;->a:Lcom/google/android/apps/gmm/directions/cm;

    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/cm;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/cz;->b()Lcom/google/android/apps/gmm/directions/da;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/directions/da;->c:Lcom/google/android/apps/gmm/directions/da;

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    move v0, v3

    goto :goto_0

    :cond_1
    iget-object v0, v4, Lcom/google/android/apps/gmm/directions/cm;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/cz;

    check-cast v0, Lcom/google/android/apps/gmm/directions/db;

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/directions/db;->q:Z

    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/directions/db;->q:Z

    :goto_2
    iget v1, v0, Lcom/google/android/apps/gmm/directions/db;->m:I

    if-ge v3, v1, :cond_3

    add-int v1, p1, v3

    add-int/lit8 v1, v1, 0x1

    iget-object v5, v4, Lcom/google/android/apps/gmm/directions/cm;->b:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/directions/cz;

    check-cast v1, Lcom/google/android/apps/gmm/directions/cs;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/directions/cs;->c:Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move v2, v3

    goto :goto_1

    .line 98
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dd;->b:Lcom/google/android/apps/gmm/directions/bt;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/bt;->a()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/bt;->notifyDataSetChanged()V

    .line 99
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/dd;->c:Lcom/google/android/apps/gmm/directions/dc;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/dc;->h:Lcom/google/android/apps/gmm/directions/h/u;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/directions/h/u;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 104
    return-void
.end method
