.class public Lcom/google/android/apps/gmm/directions/views/e;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/directions/views/d;

.field private final b:I

.field private final c:Landroid/graphics/Picture;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/directions/views/d;I)V
    .locals 4

    .prologue
    .line 325
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 326
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/views/e;->a:Lcom/google/android/apps/gmm/directions/views/d;

    .line 327
    iput p3, p0, Lcom/google/android/apps/gmm/directions/views/e;->b:I

    .line 328
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 330
    iget v2, p2, Lcom/google/android/apps/gmm/directions/views/d;->b:I

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A()Lcom/google/android/apps/gmm/map/c/c;

    move-result-object v0

    .line 329
    :goto_0
    if-eqz v0, :cond_1

    new-instance v3, Lcom/google/android/apps/gmm/util/m;

    invoke-direct {v3, v1, v2, p3}, Lcom/google/android/apps/gmm/util/m;-><init>(Landroid/content/res/Resources;II)V

    invoke-virtual {v0, v1, v2, p3, v3}, Lcom/google/android/apps/gmm/map/c/c;->b(Landroid/content/res/Resources;IILcom/google/b/a/bw;)Landroid/graphics/Picture;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/views/e;->c:Landroid/graphics/Picture;

    .line 331
    return-void

    .line 330
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 329
    :cond_1
    invoke-static {v1, v2, p3}, Lcom/google/android/apps/gmm/util/l;->a(Landroid/content/res/Resources;II)Lcom/a/a/b;

    move-result-object v0

    iget-object v0, v0, Lcom/a/a/b;->a:Landroid/graphics/Picture;

    goto :goto_1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 335
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/views/e;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 336
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/e;->c:Landroid/graphics/Picture;

    invoke-virtual {v2}, Landroid/graphics/Picture;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 337
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/e;->c:Landroid/graphics/Picture;

    invoke-virtual {v3}, Landroid/graphics/Picture;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 339
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 340
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 341
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/views/e;->a:Lcom/google/android/apps/gmm/directions/views/d;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/directions/views/d;->a:Z

    if-eqz v3, :cond_0

    .line 342
    neg-float v0, v0

    .line 343
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 345
    :cond_0
    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->scale(FF)V

    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/e;->c:Landroid/graphics/Picture;

    invoke-virtual {v0, p1}, Landroid/graphics/Picture;->draw(Landroid/graphics/Canvas;)V

    .line 347
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 348
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 378
    instance-of v1, p1, Lcom/google/android/apps/gmm/directions/views/e;

    if-eqz v1, :cond_0

    .line 379
    check-cast p1, Lcom/google/android/apps/gmm/directions/views/e;

    .line 380
    iget v1, p1, Lcom/google/android/apps/gmm/directions/views/e;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/e;->b:I

    if-ne v1, v2, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/gmm/directions/views/e;->a:Lcom/google/android/apps/gmm/directions/views/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/e;->a:Lcom/google/android/apps/gmm/directions/views/d;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 382
    :cond_0
    return v0
.end method

.method public getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/e;->c:Landroid/graphics/Picture;

    invoke-virtual {v0}, Landroid/graphics/Picture;->getHeight()I

    move-result v0

    return v0
.end method

.method public getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/views/e;->c:Landroid/graphics/Picture;

    invoke-virtual {v0}, Landroid/graphics/Picture;->getWidth()I

    move-result v0

    return v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 373
    const/4 v0, -0x3

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 387
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/views/e;->a:Lcom/google/android/apps/gmm/directions/views/d;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/directions/views/e;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 368
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 363
    return-void
.end method
