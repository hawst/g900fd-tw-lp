.class Lcom/google/android/apps/gmm/myplaces/b/d;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/myplaces/c/f;

.field final synthetic b:Lcom/google/android/apps/gmm/myplaces/a/b;

.field final synthetic c:Lcom/google/android/apps/gmm/shared/c/a/p;

.field final synthetic d:Lcom/google/android/apps/gmm/myplaces/b/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/c/f;Lcom/google/android/apps/gmm/myplaces/a/b;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->d:Lcom/google/android/apps/gmm/myplaces/b/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->a:Lcom/google/android/apps/gmm/myplaces/c/f;

    iput-object p3, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->b:Lcom/google/android/apps/gmm/myplaces/a/b;

    iput-object p4, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->c:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 422
    sget-object v0, Lcom/google/android/apps/gmm/myplaces/c/b;->c:Lcom/google/b/c/dn;

    invoke-virtual {v0}, Lcom/google/b/c/dn;->b()Lcom/google/b/c/lg;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/myplaces/c/b;

    .line 423
    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->d:Lcom/google/android/apps/gmm/myplaces/b/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/myplaces/b/a;->e:Lcom/google/android/apps/gmm/myplaces/b/n;

    iget-object v3, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->a:Lcom/google/android/apps/gmm/myplaces/c/f;

    iget-wide v4, v3, Lcom/google/android/apps/gmm/myplaces/c/f;->g:J

    iget-object v3, v2, Lcom/google/android/apps/gmm/myplaces/b/n;->b:Lcom/google/android/apps/gmm/myplaces/b/ab;

    new-instance v6, Lcom/google/android/apps/gmm/myplaces/b/r;

    invoke-direct {v6, v2, v0, v4, v5}, Lcom/google/android/apps/gmm/myplaces/b/r;-><init>(Lcom/google/android/apps/gmm/myplaces/b/n;Lcom/google/android/apps/gmm/myplaces/c/b;J)V

    invoke-virtual {v3, v6}, Lcom/google/android/apps/gmm/myplaces/b/ab;->a(Lcom/google/android/apps/gmm/myplaces/b/ae;)Ljava/lang/Object;

    goto :goto_0

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->d:Lcom/google/android/apps/gmm/myplaces/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/j;->c:Lcom/google/android/apps/gmm/myplaces/c/j;

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->c:Lcom/google/b/c/dn;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/c/j;Ljava/util/Set;)V

    .line 426
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->d:Lcom/google/android/apps/gmm/myplaces/b/a;

    sget-object v1, Lcom/google/android/apps/gmm/myplaces/c/b;->a:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Lcom/google/android/apps/gmm/myplaces/c/b;)Lcom/google/b/c/cv;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/myplaces/c/b;->b:Lcom/google/android/apps/gmm/myplaces/c/b;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Lcom/google/android/apps/gmm/myplaces/c/b;)Lcom/google/b/c/cv;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/myplaces/b/a;->a(Ljava/util/List;Ljava/util/List;)V

    .line 427
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->d:Lcom/google/android/apps/gmm/myplaces/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->b:Lcom/google/android/apps/gmm/myplaces/a/b;

    iget-object v2, p0, Lcom/google/android/apps/gmm/myplaces/b/d;->c:Lcom/google/android/apps/gmm/shared/c/a/p;

    iget-object v3, v0, Lcom/google/android/apps/gmm/myplaces/b/a;->c:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v4, Lcom/google/android/apps/gmm/myplaces/b/e;

    invoke-direct {v4, v0, v1}, Lcom/google/android/apps/gmm/myplaces/b/e;-><init>(Lcom/google/android/apps/gmm/myplaces/b/a;Lcom/google/android/apps/gmm/myplaces/a/b;)V

    invoke-interface {v3, v4, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 428
    return-void
.end method
