.class public Lcom/google/android/apps/gmm/map/o/m;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/o/k;


# instance fields
.field final a:Ljava/lang/String;

.field final b:F

.field final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;F)V
    .locals 2

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 264
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/m;->a:Ljava/lang/String;

    .line 265
    iput p2, p0, Lcom/google/android/apps/gmm/map/o/m;->b:F

    .line 266
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {p2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    xor-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/m;->c:I

    .line 267
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 276
    if-ne p0, p1, :cond_1

    .line 283
    :cond_0
    :goto_0
    return v0

    .line 279
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/o/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 280
    goto :goto_0

    .line 282
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/o/m;

    .line 283
    iget v2, p0, Lcom/google/android/apps/gmm/map/o/m;->b:F

    iget v3, p1, Lcom/google/android/apps/gmm/map/o/m;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/m;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/o/m;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lcom/google/android/apps/gmm/map/o/m;->c:I

    return v0
.end method
