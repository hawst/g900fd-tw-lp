.class public Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field final a:J

.field b:Lcom/google/android/apps/gmm/navigation/g/b/k;

.field c:Lcom/google/android/apps/gmm/navigation/g/b/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field d:Lcom/google/android/apps/gmm/directions/f/a/a;

.field e:Landroid/view/View;

.field f:Lcom/google/android/apps/gmm/navigation/navui/an;

.field g:Lcom/google/android/libraries/curvular/ae;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field m:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field n:Lcom/google/android/apps/gmm/shared/c/f;

.field o:J

.field p:Z

.field q:Z

.field r:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/g/b/k;I)V
    .locals 2

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    .line 133
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ak;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/ak;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->r:Ljava/lang/Object;

    .line 100
    const-string v0, "closedRouteState"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/navigation/g/b/k;

    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b:Lcom/google/android/apps/gmm/navigation/g/b/k;

    .line 101
    mul-int/lit16 v0, p2, 0x3e8

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->a:J

    .line 102
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/navigation/j/b/a;)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/navigation/j/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 110
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iget-wide v2, p1, Lcom/google/android/apps/gmm/navigation/j/b/a;->a:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    .line 127
    :cond_0
    :goto_1
    return-void

    .line 110
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/aj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/gmm/navigation/navui/aj;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/navigation/j/b/a;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/j/a/c;->a:Lcom/google/android/apps/gmm/navigation/j/a/c;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_1
.end method


# virtual methods
.method a()V
    .locals 5

    .prologue
    .line 304
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->d:Lcom/google/android/apps/gmm/directions/f/a/a;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/e;->aa:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 310
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/2addr v1, v0

    .line 313
    invoke-static {}, Lcom/google/android/apps/gmm/map/b/a/r;->a()Lcom/google/android/apps/gmm/map/b/a/s;

    move-result-object v2

    .line 314
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->b:Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a(Lcom/google/android/apps/gmm/map/b/a/s;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 315
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundTrafficFragment;->a(Lcom/google/android/apps/gmm/map/b/a/s;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    .line 316
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/s;->a()Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v2

    .line 319
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/directions/a/f;->c()Lcom/google/android/apps/gmm/directions/a/a;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/directions/f/a/c;

    invoke-direct {v4, v0, v0, v0, v1}, Lcom/google/android/apps/gmm/directions/f/a/c;-><init>(IIII)V

    .line 320
    invoke-interface {v3, v2, v4}, Lcom/google/android/apps/gmm/directions/a/a;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/f/a/c;)V

    .line 323
    :cond_0
    return-void
.end method

.method public final a(IIF)V
    .locals 1

    .prologue
    .line 345
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->a(IIF)V

    .line 346
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->c:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->a()V

    .line 349
    :cond_0
    return-void
.end method

.method b()V
    .locals 3

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/am;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/navigation/navui/am;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 336
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 209
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 211
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/an;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/navigation/navui/an;-><init>(Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->f:Lcom/google/android/apps/gmm/navigation/navui/an;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    const-class v1, Lcom/google/android/apps/gmm/navigation/navui/a/u;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->g:Lcom/google/android/libraries/curvular/ae;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->g:Lcom/google/android/libraries/curvular/ae;

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->e:Landroid/view/View;

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->e:Landroid/view/View;

    sget v1, Lcom/google/android/apps/gmm/g;->dO:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->m:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->n:Lcom/google/android/apps/gmm/shared/c/f;

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->n:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->a:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->o:J

    .line 221
    if-eqz p1, :cond_1

    const-string v0, "expirationRelativeTimeMs"

    .line 222
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 223
    const-string v0, "expirationRelativeTimeMs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->o:J

    .line 225
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 241
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onPause()V

    .line 243
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->m:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->m:Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->removeAllListeners()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/QuTimeoutButton;->a:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->r:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 253
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 229
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 230
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->p:Z

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/m/l;->c(Z)V

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->r:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 237
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 203
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 204
    const-string v0, "expirationRelativeTimeMs"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/navui/RouteAroundClosureFragment;->o:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 205
    return-void
.end method
