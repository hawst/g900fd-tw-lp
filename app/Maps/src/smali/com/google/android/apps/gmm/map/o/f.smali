.class public Lcom/google/android/apps/gmm/map/o/f;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final b:Lcom/google/android/apps/gmm/map/b/a/az;

.field private static final c:Lcom/google/android/apps/gmm/map/b/a/az;


# instance fields
.field public final a:[Lcom/google/android/apps/gmm/map/o/g;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 11
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/az;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/az;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/f;->b:Lcom/google/android/apps/gmm/map/b/a/az;

    .line 12
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/az;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/az;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/f;->c:Lcom/google/android/apps/gmm/map/b/a/az;

    return-void
.end method

.method private constructor <init>([Lcom/google/android/apps/gmm/map/o/g;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    .line 20
    return-void
.end method

.method public static b(Lcom/google/android/apps/gmm/map/l/b;[FFF)Lcom/google/android/apps/gmm/map/o/f;
    .locals 4

    .prologue
    .line 90
    array-length v0, p1

    add-int/lit8 v1, v0, -0x1

    .line 91
    new-array v2, v1, [Lcom/google/android/apps/gmm/map/o/g;

    .line 92
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 93
    new-instance v3, Lcom/google/android/apps/gmm/map/o/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/map/o/g;-><init>()V

    aput-object v3, v2, v0

    .line 92
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/o/f;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/o/f;-><init>([Lcom/google/android/apps/gmm/map/o/g;)V

    .line 96
    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/o/f;->a(Lcom/google/android/apps/gmm/map/l/b;[FFF)V

    .line 97
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/l/b;[FFF)V
    .locals 12

    .prologue
    .line 35
    array-length v0, p2

    add-int/lit8 v2, v0, -0x1

    .line 36
    iget v0, p1, Lcom/google/android/apps/gmm/map/l/b;->d:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    .line 37
    :goto_0
    aget v1, p2, v2

    mul-float v3, v1, p4

    .line 38
    sub-float v1, v0, v3

    const/high16 v4, 0x3f000000    # 0.5f

    mul-float/2addr v1, v4

    .line 39
    const/high16 v4, 0x3f800000    # 1.0f

    div-float/2addr v4, v0

    .line 46
    sget-object v0, Lcom/google/android/apps/gmm/map/o/f;->c:Lcom/google/android/apps/gmm/map/b/a/az;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/az;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 47
    const/4 v5, 0x0

    aget v5, p2, v5

    mul-float v5, v5, p4

    add-float/2addr v5, v1

    mul-float/2addr v5, v4

    const/4 v6, 0x0

    aget-object v6, v0, v6

    invoke-virtual {p1, v5, v6}, Lcom/google/android/apps/gmm/map/l/b;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    .line 48
    const v5, 0x3eaaaaab

    mul-float/2addr v5, v3

    add-float/2addr v5, v1

    mul-float/2addr v5, v4

    const/4 v6, 0x1

    aget-object v6, v0, v6

    invoke-virtual {p1, v5, v6}, Lcom/google/android/apps/gmm/map/l/b;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    .line 49
    const v5, 0x3f2aaaab

    mul-float/2addr v5, v3

    add-float/2addr v5, v1

    mul-float/2addr v5, v4

    const/4 v6, 0x2

    aget-object v6, v0, v6

    invoke-virtual {p1, v5, v6}, Lcom/google/android/apps/gmm/map/l/b;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    .line 50
    add-float/2addr v1, v3

    mul-float/2addr v1, v4

    const/4 v4, 0x3

    aget-object v4, v0, v4

    invoke-virtual {p1, v1, v4}, Lcom/google/android/apps/gmm/map/l/b;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    .line 51
    sget-object v1, Lcom/google/android/apps/gmm/map/o/f;->b:Lcom/google/android/apps/gmm/map/b/a/az;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/az;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ay;->a([Lcom/google/android/apps/gmm/map/b/a/ay;[Lcom/google/android/apps/gmm/map/b/a/ay;)[Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v1

    .line 54
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    .line 60
    aget v4, p2, v0

    add-int/lit8 v5, v0, 0x1

    aget v5, p2, v5

    add-float/2addr v4, v5

    mul-float v4, v4, p4

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v5, v3

    div-float/2addr v4, v5

    .line 64
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/o/g;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v1, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ay;->b([Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    move-result-object v5

    .line 65
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    aget-object v6, v6, v0

    iget v7, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    float-to-double v8, v7

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    float-to-double v10, v5

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    iput-wide v8, v6, Lcom/google/android/apps/gmm/map/o/g;->c:D

    .line 68
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/o/g;->d:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v1, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/ay;->a([Lcom/google/android/apps/gmm/map/b/a/ay;FLcom/google/android/apps/gmm/map/b/a/ay;)Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 71
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    aget-object v4, v4, v0

    add-int/lit8 v5, v0, 0x1

    aget v5, p2, v5

    aget v6, p2, v0

    sub-float/2addr v5, v6

    iput v5, v4, Lcom/google/android/apps/gmm/map/o/g;->a:F

    .line 72
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/f;->a:[Lcom/google/android/apps/gmm/map/o/g;

    aget-object v4, v4, v0

    iput p3, v4, Lcom/google/android/apps/gmm/map/o/g;->b:F

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 36
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/l/b;->b()V

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/l/b;->b:[F

    iget v1, p1, Lcom/google/android/apps/gmm/map/l/b;->d:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    goto/16 :goto_0

    .line 74
    :cond_1
    return-void
.end method
