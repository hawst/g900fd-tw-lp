.class public Lcom/google/android/apps/gmm/streetview/internal/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:[B

.field final b:[B

.field c:[B

.field d:[Ljava/lang/String;

.field e:[Lcom/google/android/apps/gmm/streetview/internal/f;

.field f:I

.field g:I

.field h:Z

.field private i:[B

.field private j:[Lcom/google/android/apps/gmm/streetview/internal/e;

.field private k:Ljava/nio/ByteBuffer;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->a:[B

    .line 187
    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->b:[B

    .line 188
    return-void
.end method

.method public constructor <init>([B[B)V
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->a:[B

    .line 172
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/d;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 173
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 175
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->b:[B

    .line 176
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/internal/d;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 177
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 179
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->h:Z

    .line 180
    return-void
.end method

.method private a()Z
    .locals 10

    .prologue
    const/16 v5, 0x8

    const/4 v0, 0x0

    .line 350
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->a:[B

    if-nez v1, :cond_0

    .line 403
    :goto_0
    return v0

    .line 354
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->a:[B

    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/m;

    new-instance v3, Ljava/util/zip/InflaterInputStream;

    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v3, v4}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/m;-><init>(Ljava/io/InputStream;)V

    .line 360
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 361
    if-eq v1, v5, :cond_1

    .line 362
    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 366
    :cond_1
    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedShort()I

    move-result v3

    .line 368
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedShort()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    .line 370
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedShort()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    .line 372
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedByte()I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    .line 373
    if-eq v1, v5, :cond_2

    .line 374
    :try_start_3
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 379
    :cond_2
    :try_start_4
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    iget v4, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    mul-int/2addr v1, v4

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->i:[B

    .line 380
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->i:[B

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFully([B)V

    .line 383
    new-array v1, v3, [Lcom/google/android/apps/gmm/streetview/internal/e;

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->j:[Lcom/google/android/apps/gmm/streetview/internal/e;

    move v1, v0

    .line 385
    :goto_1
    if-ge v1, v3, :cond_3

    .line 386
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFloat()F

    move-result v4

    .line 387
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFloat()F

    move-result v5

    .line 388
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFloat()F

    move-result v6

    .line 389
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFloat()F

    move-result v7

    .line 390
    iget-object v8, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->j:[Lcom/google/android/apps/gmm/streetview/internal/e;

    new-instance v9, Lcom/google/android/apps/gmm/streetview/internal/e;

    invoke-direct {v9, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/streetview/internal/e;-><init>(FFFF)V

    aput-object v9, v8, v1
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 385
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 397
    :cond_3
    :try_start_5
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 403
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    :catch_2
    move-exception v1

    .line 393
    :try_start_6
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    :catch_3
    move-exception v1

    goto :goto_0

    .line 396
    :catchall_0
    move-exception v0

    .line 397
    :try_start_7
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 399
    :goto_3
    throw v0

    :catch_4
    move-exception v0

    goto :goto_2

    :catch_5
    move-exception v1

    goto :goto_3
.end method

.method private b()Z
    .locals 9

    .prologue
    const/16 v7, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 412
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->b:[B

    if-nez v2, :cond_0

    .line 477
    :goto_0
    return v0

    .line 416
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->b:[B

    new-instance v3, Lcom/google/android/apps/gmm/streetview/internal/m;

    new-instance v4, Ljava/util/zip/InflaterInputStream;

    new-instance v5, Ljava/io/ByteArrayInputStream;

    invoke-direct {v5, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v4, v5}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/m;-><init>(Ljava/io/InputStream;)V

    .line 422
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedByte()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 423
    if-eq v2, v7, :cond_1

    .line 424
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 428
    :cond_1
    :try_start_2
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedShort()I

    move-result v4

    .line 430
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedShort()I

    move-result v2

    .line 432
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedShort()I

    move-result v5

    .line 433
    iget v6, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    if-ne v6, v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eq v2, v5, :cond_3

    .line 434
    :cond_2
    :try_start_3
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    .line 438
    :cond_3
    :try_start_4
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->readUnsignedByte()I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v2

    .line 439
    if-eq v2, v7, :cond_4

    .line 440
    :try_start_5
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    .line 445
    :cond_4
    :try_start_6
    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    iget v5, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    mul-int/2addr v2, v5

    new-array v2, v2, [B

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->c:[B

    .line 446
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->c:[B

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFully([B)V

    .line 449
    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->d:[Ljava/lang/String;

    .line 450
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->d:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput-object v6, v2, v5

    .line 451
    const/16 v2, 0x16

    new-array v5, v2, [B

    move v2, v1

    .line 452
    :goto_1
    if-ge v2, v4, :cond_5

    .line 453
    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFully([B)V

    .line 454
    iget-object v6, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->d:[Ljava/lang/String;

    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v5}, Ljava/lang/String;-><init>([B)V

    aput-object v7, v6, v2

    .line 452
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 458
    :cond_5
    new-array v2, v4, [Lcom/google/android/apps/gmm/streetview/internal/f;

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->e:[Lcom/google/android/apps/gmm/streetview/internal/f;

    .line 459
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->e:[Lcom/google/android/apps/gmm/streetview/internal/f;

    const/4 v5, 0x0

    const/4 v6, 0x0

    aput-object v6, v2, v5

    move v2, v1

    .line 461
    :goto_2
    if-ge v2, v4, :cond_6

    .line 462
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFloat()F

    move-result v5

    .line 463
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->readFloat()F

    move-result v6

    .line 464
    iget-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->e:[Lcom/google/android/apps/gmm/streetview/internal/f;

    new-instance v8, Lcom/google/android/apps/gmm/streetview/internal/f;

    invoke-direct {v8, v5, v6}, Lcom/google/android/apps/gmm/streetview/internal/f;-><init>(FF)V

    aput-object v8, v7, v2
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 461
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 471
    :cond_6
    :try_start_7
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :goto_3
    move v0, v1

    .line 477
    goto/16 :goto_0

    :catch_3
    move-exception v1

    .line 467
    :try_start_8
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    goto/16 :goto_0

    :catch_4
    move-exception v1

    goto/16 :goto_0

    .line 470
    :catchall_0
    move-exception v0

    .line 471
    :try_start_9
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/streetview/internal/m;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 473
    :goto_4
    throw v0

    :catch_5
    move-exception v0

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_4
.end method


# virtual methods
.method a(F)I
    .locals 2

    .prologue
    .line 315
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    float-to-int v0, v0

    if-lt v0, v1, :cond_1

    sub-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gez v0, :cond_0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final a(FF)Lcom/google/android/apps/gmm/streetview/internal/e;
    .locals 4

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->h:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Depth map must be decompressed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 232
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->h:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Depth map must be decompressed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    float-to-int v0, v0

    if-lt v0, v1, :cond_4

    sub-int/2addr v0, v1

    :cond_2
    :goto_0
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, p2

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    float-to-int v1, v1

    if-lt v1, v2, :cond_5

    sub-int/2addr v1, v2

    :cond_3
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->i:[B

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    mul-int/2addr v1, v3

    add-int/2addr v0, v1

    aget-byte v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    .line 233
    if-eqz v0, :cond_6

    .line 234
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->j:[Lcom/google/android/apps/gmm/streetview/internal/e;

    aget-object v0, v1, v0

    .line 236
    :goto_2
    return-object v0

    .line 232
    :cond_4
    if-gez v0, :cond_2

    add-int/2addr v0, v1

    goto :goto_0

    :cond_5
    if-gez v1, :cond_3

    add-int/2addr v1, v2

    goto :goto_1

    .line 236
    :cond_6
    const/4 v0, 0x0

    goto :goto_2
.end method

.method b(F)I
    .locals 2

    .prologue
    .line 325
    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v1, p1

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    float-to-int v0, v0

    if-lt v0, v1, :cond_1

    sub-int/2addr v0, v1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gez v0, :cond_0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method final c(F)Ljava/nio/ByteBuffer;
    .locals 13

    .prologue
    .line 505
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 506
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->k:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_2

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->k:Ljava/nio/ByteBuffer;

    .line 539
    :goto_1
    return-object v0

    .line 510
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->h:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Depth map must be decompressed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 511
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    mul-int/2addr v0, v1

    new-array v4, v0, [F

    .line 513
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    int-to-float v1, v1

    div-float v5, v0, v1

    .line 514
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    int-to-float v1, v1

    div-float v6, v0, v1

    .line 515
    const/4 v0, 0x3

    new-array v7, v0, [F

    .line 518
    const/4 v1, 0x0

    .line 519
    const/4 v0, 0x0

    move v3, v0

    :goto_2
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    if-ge v3, v0, :cond_7

    .line 520
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_3
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    if-ge v1, v0, :cond_6

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->i:[B

    aget-byte v0, v0, v2

    and-int/lit16 v0, v0, 0xff

    .line 522
    iget-object v8, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->j:[Lcom/google/android/apps/gmm/streetview/internal/e;

    aget-object v0, v8, v0

    .line 523
    int-to-float v8, v1

    mul-float/2addr v8, v5

    const/high16 v9, 0x3f000000    # 0.5f

    int-to-float v10, v3

    mul-float/2addr v10, v6

    sub-float/2addr v9, v10

    const/4 v10, 0x0

    invoke-static {v8, v9, v7, v10}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    .line 524
    const/4 v8, 0x0

    aget v8, v7, v8

    const/4 v9, 0x2

    aget v9, v7, v9

    neg-float v9, v9

    const/4 v10, 0x1

    aget v10, v7, v10

    iget v11, v0, Lcom/google/android/apps/gmm/streetview/internal/e;->d:F

    iget v12, v0, Lcom/google/android/apps/gmm/streetview/internal/e;->a:F

    mul-float/2addr v8, v12

    iget v12, v0, Lcom/google/android/apps/gmm/streetview/internal/e;->b:F

    mul-float/2addr v9, v12

    add-float/2addr v8, v9

    iget v0, v0, Lcom/google/android/apps/gmm/streetview/internal/e;->c:F

    mul-float/2addr v0, v10

    add-float/2addr v0, v8

    div-float v0, v11, v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 525
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v8

    if-nez v8, :cond_4

    const/4 v8, 0x0

    cmpl-float v8, v0, v8

    if-nez v8, :cond_5

    :cond_4
    move v0, p1

    .line 528
    :cond_5
    aput v0, v4, v2

    .line 520
    add-int/lit8 v0, v1, 0x1

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    .line 519
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v1, v2

    goto :goto_2

    .line 533
    :cond_7
    iget v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->k:Ljava/nio/ByteBuffer;

    .line 534
    const/4 v0, 0x0

    :goto_4
    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->g:I

    mul-int/2addr v1, v2

    if-ge v0, v1, :cond_8

    .line 535
    aget v1, v4, v0

    div-float/2addr v1, p1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 536
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->k:Ljava/nio/ByteBuffer;

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    int-to-byte v1, v1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    .line 534
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 538
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->k:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 539
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/d;->k:Ljava/nio/ByteBuffer;

    goto/16 :goto_1
.end method
