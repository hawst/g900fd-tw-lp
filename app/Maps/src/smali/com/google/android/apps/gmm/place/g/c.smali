.class public Lcom/google/android/apps/gmm/place/g/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/g/a/c;


# instance fields
.field private a:Lcom/google/android/apps/gmm/place/an;

.field private b:Ljava/lang/Boolean;

.field private c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/c;->d:Ljava/lang/Float;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Float;)Lcom/google/android/libraries/curvular/cf;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/g/c;->d:Ljava/lang/Float;

    .line 112
    invoke-static {p0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/c;->a:Lcom/google/android/apps/gmm/place/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/g/c;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/place/an;->a(Lcom/google/android/apps/gmm/x/o;F)V

    .line 114
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/c;->b:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 38
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/g/c;->a:Lcom/google/android/apps/gmm/place/an;

    .line 39
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 41
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->O()Lcom/google/maps/g/pc;

    move-result-object v4

    .line 45
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->N()Z

    move-result v1

    if-nez v1, :cond_4

    .line 46
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->P()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v1

    if-gtz v1, :cond_4

    .line 47
    :cond_0
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-eqz v1, :cond_4

    .line 48
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move v1, v2

    :goto_0
    if-nez v1, :cond_4

    .line 49
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 50
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->c:Z

    if-eqz v1, :cond_4

    .line 51
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v1, v1, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v5, 0x40

    if-ne v1, v5, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    move v1, v2

    .line 45
    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/g/c;->b:Ljava/lang/Boolean;

    .line 54
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->s()I

    move-result v1

    if-nez v1, :cond_5

    .line 55
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v5, Lcom/google/android/apps/gmm/l;->kW:I

    new-array v6, v2, [Ljava/lang/Object;

    .line 56
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    .line 55
    invoke-virtual {v1, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v5, Lcom/google/android/apps/gmm/l;->kX:I

    new-array v2, v2, [Ljava/lang/Object;

    .line 58
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 57
    invoke-virtual {v1, v5, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    .line 61
    :goto_3
    iget v0, v4, Lcom/google/maps/g/pc;->h:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/g/c;->d:Ljava/lang/Float;

    .line 66
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/g/c;->c:Lcom/google/android/apps/gmm/x/o;

    .line 67
    return-void

    :cond_2
    move v1, v3

    .line 48
    goto :goto_0

    :cond_3
    move v1, v3

    .line 51
    goto :goto_1

    :cond_4
    move v1, v3

    goto :goto_2

    .line 60
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->kV:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    goto :goto_3
.end method

.method public final b()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/c;->d:Ljava/lang/Float;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/z/b/l;
    .locals 4

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/c;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 120
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 122
    :goto_0
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    .line 123
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->cY:Lcom/google/b/f/t;

    aput-object v3, v0, v2

    .line 124
    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 125
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    return-object v0

    .line 121
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/g/c;->b:Ljava/lang/Boolean;

    return-object v0
.end method
