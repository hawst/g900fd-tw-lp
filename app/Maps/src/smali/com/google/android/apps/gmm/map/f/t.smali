.class public Lcom/google/android/apps/gmm/map/f/t;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/f/b;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/animation/TypeEvaluator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/animation/TypeEvaluator",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private static final m:Landroid/animation/TypeEvaluator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/animation/TypeEvaluator",
            "<",
            "Lcom/google/android/apps/gmm/map/f/a/e;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Lcom/google/android/apps/gmm/shared/c/f;

.field public d:J

.field public volatile e:I

.field public volatile f:Z

.field public final g:Lcom/google/android/apps/gmm/map/f/w;

.field public final h:Lcom/google/android/apps/gmm/map/f/w;

.field public final i:Lcom/google/android/apps/gmm/map/f/w;

.field public final j:Lcom/google/android/apps/gmm/map/f/w;

.field public final k:Lcom/google/android/apps/gmm/map/f/w;

.field final l:[Lcom/google/android/apps/gmm/map/f/w;

.field private n:J

.field private o:Lcom/google/android/apps/gmm/map/f/a/a;

.field private p:Z

.field private q:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/apps/gmm/map/f/t;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/f/t;->a:Ljava/lang/String;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/map/f/u;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/u;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/t;->b:Landroid/animation/TypeEvaluator;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/map/f/v;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/v;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/f/t;->m:Landroid/animation/TypeEvaluator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 201
    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;)V

    .line 203
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Landroid/animation/TimeInterpolator;)V
    .locals 4

    .prologue
    .line 210
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/f/t;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 211
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 212
    invoke-virtual {v3, p2}, Lcom/google/android/apps/gmm/map/f/w;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 214
    :cond_0
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;Lcom/google/android/apps/gmm/map/f/w;)V
    .locals 3

    .prologue
    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->a:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/f/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    .line 225
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/t;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 226
    if-eqz p2, :cond_1

    :goto_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/f/t;->g:Lcom/google/android/apps/gmm/map/f/w;

    .line 227
    if-eqz p3, :cond_2

    :goto_1
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/f/t;->h:Lcom/google/android/apps/gmm/map/f/w;

    .line 228
    if-eqz p4, :cond_3

    :goto_2
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/f/t;->i:Lcom/google/android/apps/gmm/map/f/w;

    .line 229
    if-eqz p5, :cond_4

    :goto_3
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/f/t;->j:Lcom/google/android/apps/gmm/map/f/w;

    .line 230
    if-eqz p6, :cond_5

    :goto_4
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/f/t;->k:Lcom/google/android/apps/gmm/map/f/w;

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->g:Lcom/google/android/apps/gmm/map/f/w;

    aput-object v2, v0, v1

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->b:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->h:Lcom/google/android/apps/gmm/map/f/w;

    aput-object v2, v0, v1

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->i:Lcom/google/android/apps/gmm/map/f/w;

    aput-object v2, v0, v1

    .line 235
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->j:Lcom/google/android/apps/gmm/map/f/w;

    aput-object v2, v0, v1

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->e:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->k:Lcom/google/android/apps/gmm/map/f/w;

    aput-object v2, v0, v1

    .line 237
    return-void

    .line 226
    :cond_1
    new-instance p2, Lcom/google/android/apps/gmm/map/f/w;

    invoke-direct {p2, p0}, Lcom/google/android/apps/gmm/map/f/w;-><init>(Lcom/google/android/apps/gmm/map/f/t;)V

    goto :goto_0

    .line 227
    :cond_2
    new-instance p3, Lcom/google/android/apps/gmm/map/f/w;

    invoke-direct {p3, p0}, Lcom/google/android/apps/gmm/map/f/w;-><init>(Lcom/google/android/apps/gmm/map/f/t;)V

    goto :goto_1

    .line 228
    :cond_3
    new-instance p4, Lcom/google/android/apps/gmm/map/f/w;

    invoke-direct {p4, p0}, Lcom/google/android/apps/gmm/map/f/w;-><init>(Lcom/google/android/apps/gmm/map/f/t;)V

    goto :goto_2

    .line 229
    :cond_4
    new-instance p5, Lcom/google/android/apps/gmm/map/f/w;

    invoke-direct {p5, p0}, Lcom/google/android/apps/gmm/map/f/w;-><init>(Lcom/google/android/apps/gmm/map/f/t;)V

    goto :goto_3

    .line 230
    :cond_5
    new-instance p6, Lcom/google/android/apps/gmm/map/f/w;

    invoke-direct {p6, p0}, Lcom/google/android/apps/gmm/map/f/w;-><init>(Lcom/google/android/apps/gmm/map/f/t;)V

    goto :goto_4
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/map/f/t;)V
    .locals 14

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v4

    const-wide/16 v6, 0x0

    :try_start_0
    iput-wide v6, p0, Lcom/google/android/apps/gmm/map/f/t;->n:J

    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    iget v8, v7, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v8, v1, v8

    and-int/2addr v0, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    iget-wide v8, p0, Lcom/google/android/apps/gmm/map/f/t;->n:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    iget v10, v7, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    aget-object v0, v0, v10

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/w;->getStartDelay()J

    move-result-wide v10

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    iget v7, v7, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/w;->getDuration()J

    move-result-wide v12

    add-long/2addr v10, v12

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/apps/gmm/map/f/t;->n:J

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    monitor-exit v4

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public W_()I
    .locals 1

    .prologue
    .line 396
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    return v0
.end method

.method public a(J)I
    .locals 15

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    if-nez v0, :cond_1

    .line 289
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/t;->q:I

    .line 290
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->q:I

    .line 314
    :goto_0
    return v0

    .line 293
    :cond_1
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/f/t;->d:J

    sub-long v2, p1, v0

    .line 295
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_3

    .line 296
    const-wide/16 v0, 0x0

    .line 297
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/gmm/map/f/t;->q:I

    .line 307
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v4

    .line 308
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v5

    array-length v6, v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_7

    aget-object v7, v5, v3

    .line 309
    iget v2, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    const/4 v8, 0x1

    iget v9, v7, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int/2addr v8, v9

    and-int/2addr v2, v8

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_2

    .line 310
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    iget v7, v7, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    aget-object v2, v2, v7

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/w;->getStartDelay()J

    move-result-wide v8

    sub-long v8, v0, v8

    const-wide/16 v10, 0x0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/f/w;->getDuration()J

    move-result-wide v12

    invoke-static {v8, v9, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    invoke-virtual {v2, v8, v9}, Lcom/google/android/apps/gmm/map/f/w;->setCurrentPlayTime(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 298
    :cond_3
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/f/t;->n:J

    cmp-long v0, v2, v0

    if-lez v0, :cond_4

    .line 299
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/f/t;->n:J

    .line 300
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/gmm/map/f/t;->q:I

    goto :goto_1

    .line 302
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/t;->f:Z

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_4
    iput v0, p0, Lcom/google/android/apps/gmm/map/f/t;->q:I

    move-wide v0, v2

    goto :goto_1

    :cond_5
    const/4 v0, 0x2

    goto :goto_4

    .line 309
    :cond_6
    const/4 v2, 0x0

    goto :goto_3

    .line 313
    :cond_7
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 314
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->q:I

    goto :goto_0

    .line 313
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 328
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v1

    .line 329
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/w;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 246
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/f/t;->p:Z

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->d()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/f/t;->d:J

    .line 248
    iput p1, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    .line 249
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v4

    .line 250
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v5

    array-length v6, v5

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v7, v5, v3

    .line 251
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    iget v8, v7, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v8, v1, v8

    and-int/2addr v0, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    iget v7, v7, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    aget-object v0, v0, v7

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/w;->start()V

    .line 250
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 251
    goto :goto_1

    .line 255
    :cond_2
    monitor-exit v4

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/map/f/a/c;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 319
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_2

    aget-object v6, v4, v3

    .line 320
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    iget v7, v6, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v7, v1, v7

    and-int/2addr v0, v7

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {p0, v6}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v6, v0}, Lcom/google/android/apps/gmm/map/f/a/c;->a(Lcom/google/android/apps/gmm/map/f/a/d;Ljava/lang/Object;)Lcom/google/android/apps/gmm/map/f/a/c;

    .line 319
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 320
    goto :goto_1

    .line 324
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 454
    if-eqz p2, :cond_0

    .line 455
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v1, v2, v1

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    .line 459
    :goto_0
    return-void

    .line 457
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v1, v2, v1

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/f/a/a;Lcom/google/android/apps/gmm/map/f/a/a;)Z
    .locals 9
    .param p1    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/apps/gmm/map/f/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/high16 v8, 0x43b40000    # 360.0f

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 341
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/map/f/t;->p:Z

    .line 342
    iput v7, p0, Lcom/google/android/apps/gmm/map/f/t;->e:I

    .line 344
    if-nez p2, :cond_0

    .line 391
    :goto_0
    return v7

    .line 347
    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    if-nez v0, :cond_1

    .line 349
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/f/t;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 350
    sget-object v0, Lcom/google/android/apps/gmm/map/f/t;->a:Ljava/lang/String;

    goto :goto_0

    .line 354
    :cond_1
    if-nez p1, :cond_2

    .line 355
    iget-object p1, p0, Lcom/google/android/apps/gmm/map/f/t;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 357
    :cond_2
    new-instance v5, Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-direct {v5, p2}, Lcom/google/android/apps/gmm/map/f/a/c;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;)V

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    .line 360
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->j(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 363
    iget v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget v0, p2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    sub-float v3, v0, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sub-float v4, v8, v3

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 365
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v2

    .line 366
    :try_start_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/t;->g:Lcom/google/android/apps/gmm/map/f/w;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v8, p1, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    aput-object v8, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/f/w;->setObjectValues([Ljava/lang/Object;)V

    .line 367
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->g:Lcom/google/android/apps/gmm/map/f/w;

    sget-object v3, Lcom/google/android/apps/gmm/map/f/t;->b:Landroid/animation/TypeEvaluator;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/f/w;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 368
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->g:Lcom/google/android/apps/gmm/map/f/w;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/gmm/map/f/w;->setCurrentPlayTime(J)V

    .line 369
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->h:Lcom/google/android/apps/gmm/map/f/w;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    iget v5, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    aput v5, v3, v4

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/f/w;->setFloatValues([F)V

    .line 370
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->h:Lcom/google/android/apps/gmm/map/f/w;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/gmm/map/f/w;->setCurrentPlayTime(J)V

    .line 371
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->i:Lcom/google/android/apps/gmm/map/f/w;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    iget v5, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    aput v5, v3, v4

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/f/w;->setFloatValues([F)V

    .line 372
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->i:Lcom/google/android/apps/gmm/map/f/w;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v4, v5}, Lcom/google/android/apps/gmm/map/f/w;->setCurrentPlayTime(J)V

    .line 373
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->j:Lcom/google/android/apps/gmm/map/f/w;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    iget v5, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    aput v5, v3, v4

    const/4 v4, 0x1

    aput v0, v3, v4

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/map/f/w;->setFloatValues([F)V

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->j:Lcom/google/android/apps/gmm/map/f/w;

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/f/w;->setCurrentPlayTime(J)V

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->k:Lcom/google/android/apps/gmm/map/f/w;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    iget-object v4, p2, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    aput-object v4, v1, v3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/w;->setObjectValues([Ljava/lang/Object;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->k:Lcom/google/android/apps/gmm/map/f/w;

    sget-object v1, Lcom/google/android/apps/gmm/map/f/t;->m:Landroid/animation/TypeEvaluator;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/f/w;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->k:Lcom/google/android/apps/gmm/map/f/w;

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Lcom/google/android/apps/gmm/map/f/w;->setCurrentPlayTime(J)V

    .line 379
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 381
    const-wide/16 v0, 0x3e8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/f/t;->b(J)Lcom/google/android/apps/gmm/map/f/b;

    .line 384
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    .line 385
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v6

    .line 384
    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 386
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->b:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget v2, p2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_7

    move v0, v6

    :goto_3
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 387
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget v2, p2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_8

    move v0, v6

    :goto_4
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 388
    sget-object v1, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget v2, p2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_9

    move v0, v6

    :goto_5
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 389
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->e:Lcom/google/android/apps/gmm/map/f/a/d;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget-object v2, p2, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 390
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/f/a/e;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move v7, v6

    .line 389
    :cond_3
    invoke-virtual {p0, v0, v7}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    move v7, v6

    .line 391
    goto/16 :goto_0

    .line 363
    :cond_4
    cmpg-float v2, v0, v2

    if-gez v2, :cond_5

    add-float/2addr v0, v8

    goto/16 :goto_1

    :cond_5
    sub-float/2addr v0, v8

    goto/16 :goto_1

    .line 379
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move v0, v7

    .line 385
    goto :goto_2

    :cond_7
    move v0, v7

    .line 386
    goto :goto_3

    :cond_8
    move v0, v7

    .line 387
    goto :goto_4

    :cond_9
    move v0, v7

    .line 388
    goto :goto_5
.end method

.method public a(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)Z
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/f/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 402
    const/4 v0, 0x1

    return v0
.end method

.method public final b(J)Lcom/google/android/apps/gmm/map/f/b;
    .locals 3

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/t;->p:Z

    if-nez v0, :cond_0

    .line 267
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set duration outside of initialization window."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 270
    :cond_0
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/f/t;->n:J

    .line 273
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    monitor-enter v1

    .line 274
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 275
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/t;->l:[Lcom/google/android/apps/gmm/map/f/w;

    aget-object v2, v2, v0

    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/gmm/map/f/w;->a(J)V

    .line 274
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 277
    :cond_1
    monitor-exit v1

    .line 278
    return-object p0

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/t;->o:Lcom/google/android/apps/gmm/map/f/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lcom/google/android/apps/gmm/map/f/b;Lcom/google/android/apps/gmm/map/f/a/d;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/map/f/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 408
    if-eq p1, p0, :cond_0

    .line 409
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Lcom/google/android/apps/gmm/map/f/t;->a(Lcom/google/android/apps/gmm/map/f/a/d;Z)V

    .line 411
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/f/t;->W_()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/f/t;->a(I)V

    .line 242
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 283
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/f/t;->n:J

    return-wide v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 426
    const/4 v0, 0x0

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 431
    const/4 v0, 0x0

    return v0
.end method
