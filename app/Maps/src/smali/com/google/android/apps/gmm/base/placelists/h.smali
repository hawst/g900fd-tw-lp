.class Lcom/google/android/apps/gmm/base/placelists/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/am;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;)V
    .locals 0

    .prologue
    .line 537
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/x/o;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILcom/google/android/apps/gmm/place/PlacePageView;)V
    .locals 4

    .prologue
    .line 618
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 629
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->d(I)Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    .line 623
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    if-eqz p2, :cond_2

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->r:Lcom/google/android/apps/gmm/base/placelists/p;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/base/placelists/p;->d:Z

    invoke-virtual {p2, v2}, Lcom/google/android/apps/gmm/place/PlacePageView;->a(Z)V

    if-eqz v0, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->v:Lcom/google/android/apps/gmm/base/placelists/a;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/apps/gmm/base/placelists/a;->a(Landroid/view/View;Lcom/google/android/apps/gmm/x/o;)V

    .line 625
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    if-nez v1, :cond_3

    const/4 v0, -0x1

    :goto_1
    if-ne p1, v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->f:Lcom/google/android/apps/gmm/place/m;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/af;->A()Lcom/google/android/apps/gmm/base/l/a/ab;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/m;->a()V

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->m:Landroid/view/View;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0

    .line 625
    :cond_3
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->b()I

    move-result v1

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->g:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    sub-int v0, v1, v0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(IZ)V
    .locals 5

    .prologue
    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->g:Z

    .line 554
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    if-eq v0, v1, :cond_1

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    .line 559
    if-eqz p2, :cond_2

    .line 560
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o()V

    .line 561
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->q:Lcom/google/android/apps/gmm/base/placelists/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    .line 562
    iget-object v2, v2, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/base/placelists/a/a;->c(I)Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 561
    invoke-virtual {v1, p1, v2, v3}, Lcom/google/android/apps/gmm/base/placelists/o;->a(ILcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    .line 565
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-static {v1, p1, p2}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;IZ)V

    .line 569
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 570
    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->a:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_3

    .line 572
    sget-object v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->b:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    .line 574
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->a(Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;Lcom/google/android/apps/gmm/base/activities/w;)V

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->e:Lcom/google/android/apps/gmm/place/PlacePageViewPager;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/place/PlacePageViewPager;->a(Ljava/lang/Object;)Lcom/google/android/apps/gmm/place/PlacePageView;

    move-result-object v0

    .line 578
    if-eqz v0, :cond_0

    .line 579
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->f:Lcom/google/android/apps/gmm/place/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/place/af;->A()Lcom/google/android/apps/gmm/base/l/a/ab;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/apps/gmm/place/m;->b:Landroid/view/View;

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/ce;)V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/m;->a()V

    .line 580
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->m:Landroid/view/View;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacePageView;->e()Lcom/google/android/apps/gmm/place/af;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/place/af;)V
    .locals 2

    .prologue
    .line 613
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/place/af;->m()Lcom/google/android/apps/gmm/place/i/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->p:Z

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/i/g;->a(Z)V

    .line 614
    return-void
.end method

.method public final aa_()I
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->b()I

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->o:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/placelists/a/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/placelists/a/c;->b()Lcom/google/android/apps/gmm/base/placelists/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->h()Z

    move-result v0

    return v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->g:Z

    .line 602
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->t:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->s:Lcom/google/android/apps/gmm/base/placelists/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/a/a;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    .line 603
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/placelists/h;->a:Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/placelists/PlaceCollectionMapFragment;->t()V

    .line 606
    :cond_0
    return-void
.end method
