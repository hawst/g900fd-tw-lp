.class public Lcom/google/android/apps/gmm/reportmapissue/d/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/reportmapissue/c/a;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

.field public final c:Lcom/google/android/apps/gmm/navigation/navui/views/g;

.field public d:Lcom/google/android/apps/gmm/base/views/c/d;

.field private final e:Landroid/app/DialogFragment;

.field private final f:Lcom/google/android/apps/gmm/base/activities/c;

.field private final g:Lcom/google/android/apps/gmm/reportmapissue/a;


# direct methods
.method public constructor <init>(Landroid/app/DialogFragment;Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/reportmapissue/a;Ljava/util/List;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/DialogFragment;",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/apps/gmm/reportmapissue/a;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/j;",
            ">;",
            "Lcom/google/android/apps/gmm/navigation/navui/views/d;",
            ")V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->e:Landroid/app/DialogFragment;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->f:Lcom/google/android/apps/gmm/base/activities/c;

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->g:Lcom/google/android/apps/gmm/reportmapissue/a;

    .line 43
    iput-object p4, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->a:Ljava/util/List;

    .line 44
    iput-object p5, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 45
    invoke-static {}, Lcom/google/android/apps/gmm/navigation/navui/views/d;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v4

    invoke-static {}, Lcom/google/android/apps/gmm/navigation/navui/views/d;->c()Lcom/google/android/libraries/curvular/au;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/libraries/curvular/au;->a(Landroid/content/Context;)F

    move-result v5

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/d;->as:I

    sget v2, Lcom/google/android/apps/gmm/d;->an:I

    sget v3, Lcom/google/android/apps/gmm/d;->aq:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/navui/views/f;->a(Landroid/content/res/Resources;IIIFF)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v6

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/d;->ak:I

    sget v2, Lcom/google/android/apps/gmm/d;->ar:I

    sget v3, Lcom/google/android/apps/gmm/d;->ap:I

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/navui/views/f;->a(Landroid/content/res/Resources;IIIFF)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/views/g;

    invoke-direct {v1, v6, v0, v6, v0}, Lcom/google/android/apps/gmm/navigation/navui/views/g;-><init>(Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->c:Lcom/google/android/apps/gmm/navigation/navui/views/g;

    .line 46
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/apps/gmm/base/views/c/d;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->b:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    iget-object v4, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->c:Lcom/google/android/apps/gmm/navigation/navui/views/g;

    const/4 v5, 0x1

    invoke-virtual {v4, v0, v5}, Lcom/google/android/apps/gmm/navigation/navui/views/g;->a(ZZ)Lcom/google/android/apps/gmm/navigation/navui/views/f;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/base/views/c/d;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/f;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->d:Lcom/google/android/apps/gmm/base/views/c/d;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/views/c/d;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->d:Lcom/google/android/apps/gmm/base/views/c/d;

    return-object v0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->f:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->f:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->dh:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 11

    .prologue
    const/4 v6, 0x2

    const/4 v10, 0x0

    const/4 v5, 0x1

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->e:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->e:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->g:Lcom/google/android/apps/gmm/reportmapissue/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->f:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->de:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    aput-object v4, v3, v10

    iget-object v4, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget v2, Lcom/google/android/apps/gmm/l;->dg:I

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v6, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    aput-object v6, v4, v10

    iget-object v6, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v6, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget v4, Lcom/google/android/apps/gmm/l;->dj:I

    new-array v6, v5, [Ljava/lang/Object;

    const-wide v8, 0x409f400000000000L    # 2000.0

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v1, v4, v6}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "\n"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v2, 0xa

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/reportmapissue/a;->a(Lcom/google/android/apps/gmm/base/activities/c;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 70
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 11

    .prologue
    const/4 v6, 0x2

    const/4 v10, 0x0

    const/4 v5, 0x1

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->e:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->e:Landroid/app/DialogFragment;

    invoke-virtual {v0}, Landroid/app/DialogFragment;->dismiss()V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->g:Lcom/google/android/apps/gmm/reportmapissue/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/d/a;->f:Lcom/google/android/apps/gmm/base/activities/c;

    sget v2, Lcom/google/android/apps/gmm/l;->db:I

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    aput-object v4, v3, v10

    iget-object v4, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v4, v4, Lcom/google/android/apps/gmm/reportmapissue/c;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget v2, Lcom/google/android/apps/gmm/l;->dd:I

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v6, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->o:Ljava/lang/String;

    aput-object v6, v4, v10

    iget-object v6, v0, Lcom/google/android/apps/gmm/reportmapissue/a;->a:Lcom/google/android/apps/gmm/reportmapissue/c;

    iget-object v6, v6, Lcom/google/android/apps/gmm/reportmapissue/c;->a:Lcom/google/android/apps/gmm/map/b/a/u;

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v4}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget v4, Lcom/google/android/apps/gmm/l;->dj:I

    new-array v6, v5, [Ljava/lang/Object;

    const-wide v8, 0x409f400000000000L    # 2000.0

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-virtual {v1, v4, v6}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v7, v8

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "\n"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v2, 0xb

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/reportmapissue/a;->a(Lcom/google/android/apps/gmm/base/activities/c;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 79
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
