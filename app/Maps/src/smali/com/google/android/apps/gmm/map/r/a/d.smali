.class abstract Lcom/google/android/apps/gmm/map/r/a/d;
.super Lcom/google/b/c/b;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/b/c/b",
        "<TT;>;"
    }
.end annotation


# instance fields
.field a:I

.field b:I

.field c:Ljava/lang/Integer;

.field d:I

.field e:I

.field final synthetic f:Lcom/google/android/apps/gmm/map/r/a/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/r/a/a;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 162
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->f:Lcom/google/android/apps/gmm/map/r/a/a;

    invoke-direct {p0}, Lcom/google/b/c/b;-><init>()V

    .line 163
    iput v1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->a:I

    .line 164
    iput v1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->b:I

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->f:Lcom/google/android/apps/gmm/map/r/a/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    iget-object v2, v2, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/a;->a()I

    move-result v0

    if-lt v2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->c:Ljava/lang/Integer;

    .line 166
    iput v1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->d:I

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->f:Lcom/google/android/apps/gmm/map/r/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/a;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->e:I

    return-void

    :cond_0
    move v0, v1

    .line 165
    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected final a()Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const-wide v4, 0xd693a400L

    .line 171
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->e:I

    if-ge v0, v1, :cond_3

    .line 172
    iget v1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->a:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->f:Lcom/google/android/apps/gmm/map/r/a/a;

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/d;->d:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    iget-object v0, v0, Lcom/google/r/b/a/aex;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->a:I

    .line 189
    iget v1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->b:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->f:Lcom/google/android/apps/gmm/map/r/a/a;

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/d;->d:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    iget-object v0, v0, Lcom/google/r/b/a/aex;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v2, v1

    int-to-long v0, v0

    add-long/2addr v0, v2

    const-wide/32 v2, -0x6b49d200

    cmp-long v2, v0, v2

    if-gtz v2, :cond_2

    add-long/2addr v0, v4

    :cond_0
    :goto_0
    long-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->b:I

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->f:Lcom/google/android/apps/gmm/map/r/a/a;

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/a/d;->d:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    iget-object v0, v0, Lcom/google/r/b/a/aex;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->c:Ljava/lang/Integer;

    .line 194
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->d:I

    .line 195
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/d;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/r/a/d;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/d;->c:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/map/r/a/d;->a(IILjava/lang/Integer;)Ljava/lang/Object;

    move-result-object v0

    .line 197
    :goto_1
    return-object v0

    .line 189
    :cond_2
    const-wide/32 v2, 0x6b49d200

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    sub-long/2addr v0, v4

    goto :goto_0

    .line 197
    :cond_3
    sget-object v0, Lcom/google/b/c/d;->c:Lcom/google/b/c/d;

    iput-object v0, p0, Lcom/google/b/c/b;->g:Lcom/google/b/c/d;

    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected abstract a(IILjava/lang/Integer;)Ljava/lang/Object;
    .param p3    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/lang/Integer;",
            ")TT;"
        }
    .end annotation
.end method
