.class Lcom/google/android/apps/gmm/map/i/l;
.super Lcom/google/android/apps/gmm/map/legacy/internal/b/c;
.source "PG"


# instance fields
.field private final d:Lcom/google/android/apps/gmm/v/g;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/q;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Z

.field private g:F


# direct methods
.method public constructor <init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/internal/vector/gl/a;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/t/q;",
            ">;",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Lcom/google/android/apps/gmm/map/internal/vector/gl/a;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 359
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/c;-><init>(Lcom/google/android/apps/gmm/map/f/o;)V

    .line 355
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/i/l;->g:F

    .line 360
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/i/l;->e:Ljava/util/List;

    .line 361
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/map/i/l;->f:Z

    .line 362
    iget-object v0, p3, Lcom/google/android/apps/gmm/map/internal/vector/gl/a;->b:Lcom/google/android/apps/gmm/v/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->c:Lcom/google/android/apps/gmm/v/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/i/l;->d:Lcom/google/android/apps/gmm/v/g;

    .line 363
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 3

    .prologue
    .line 367
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/i/l;->f:Z

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/legacy/internal/b/h;->a(FZZ)F

    move-result v0

    .line 369
    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 370
    iget v1, p0, Lcom/google/android/apps/gmm/map/i/l;->g:F

    cmpl-float v1, v0, v1

    if-nez v1, :cond_1

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 373
    :cond_1
    iput v0, p0, Lcom/google/android/apps/gmm/map/i/l;->g:F

    .line 374
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/l;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 375
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/l;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/t/q;

    iget v2, p0, Lcom/google/android/apps/gmm/map/i/l;->g:F

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/t/q;->a(F)V

    .line 374
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 377
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/l;->d:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 378
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/i/l;->d:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_0
.end method
