.class public Lcom/google/android/apps/gmm/directions/i/s;
.super Lcom/google/android/apps/gmm/directions/i/ae;
.source "PG"


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/android/apps/gmm/directions/h/z;)V
    .locals 0
    .param p7    # Lcom/google/android/apps/gmm/directions/h/z;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct/range {p0 .. p7}, Lcom/google/android/apps/gmm/directions/i/ae;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ao;ILcom/google/android/apps/gmm/directions/h/y;Lcom/google/android/apps/gmm/directions/i/bi;ZLcom/google/android/apps/gmm/directions/h/z;)V

    .line 24
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/i/s;->c:Landroid/content/Context;

    .line 25
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/c/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/s;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    .line 45
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/bg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-nez v2, :cond_2

    .line 46
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/bg;->b:Lcom/google/android/apps/gmm/directions/h/x;

    .line 47
    if-eqz v1, :cond_1

    .line 48
    sget-object v2, Lcom/google/android/apps/gmm/directions/i/t;->a:[I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/directions/h/x;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 56
    :cond_1
    :goto_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/i/s;->d:Ljava/lang/CharSequence;

    .line 62
    return-void

    .line 45
    :cond_2
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_0

    .line 50
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/s;->c:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->o:I

    .line 51
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_1

    .line 54
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/i/s;->c:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->n:I

    .line 55
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 54
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto :goto_1

    .line 48
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/directions/h/x;)V
    .locals 0

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/directions/i/ae;->a(Lcom/google/android/apps/gmm/directions/h/x;)V

    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/i/s;->a()V

    .line 33
    return-void
.end method

.method public final j()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/s;->d:Ljava/lang/CharSequence;

    if-nez v0, :cond_0

    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/gmm/directions/i/s;->a()V

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/i/s;->d:Ljava/lang/CharSequence;

    return-object v0
.end method
