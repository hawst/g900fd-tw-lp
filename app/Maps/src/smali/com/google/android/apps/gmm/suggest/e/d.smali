.class public Lcom/google/android/apps/gmm/suggest/e/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/f;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final h:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final j:Ljava/lang/Integer;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final k:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final l:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final m:[B
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final n:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final o:Lcom/google/e/a/a/a/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/suggest/e/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/suggest/e/d;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/b/c/cv;Lcom/google/b/c/cv;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/Integer;Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;[BLjava/lang/String;Lcom/google/e/a/a/a/b;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p10    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p11    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p12    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p13    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p14    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p15    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p16    # [B
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p17    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p18    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            "Lcom/google/android/apps/gmm/map/b/a/q;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/f;",
            ">;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/f;",
            ">;",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            "Lcom/google/e/a/a/a/b;",
            "Lcom/google/e/a/a/a/b;",
            "Lcom/google/e/a/a/a/b;",
            "[B",
            "Ljava/lang/String;",
            "Lcom/google/e/a/a/a/b;",
            ")V"
        }
    .end annotation

    .prologue
    .line 393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394
    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/suggest/e/d;->d:Ljava/lang/String;

    .line 395
    if-nez p2, :cond_1

    const-string p2, ""

    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/suggest/e/d;->e:Ljava/lang/String;

    .line 396
    iput-object p3, p0, Lcom/google/android/apps/gmm/suggest/e/d;->g:Ljava/lang/String;

    .line 397
    iput p4, p0, Lcom/google/android/apps/gmm/suggest/e/d;->b:I

    .line 398
    iput p5, p0, Lcom/google/android/apps/gmm/suggest/e/d;->c:I

    .line 399
    iput-object p8, p0, Lcom/google/android/apps/gmm/suggest/e/d;->f:Lcom/google/b/c/cv;

    .line 402
    iput-object p10, p0, Lcom/google/android/apps/gmm/suggest/e/d;->h:Ljava/lang/Integer;

    .line 404
    iput-object p11, p0, Lcom/google/android/apps/gmm/suggest/e/d;->i:Ljava/lang/String;

    .line 405
    iput-object p12, p0, Lcom/google/android/apps/gmm/suggest/e/d;->j:Ljava/lang/Integer;

    .line 406
    iput-object p13, p0, Lcom/google/android/apps/gmm/suggest/e/d;->k:Lcom/google/e/a/a/a/b;

    .line 407
    iput-object p14, p0, Lcom/google/android/apps/gmm/suggest/e/d;->l:Lcom/google/e/a/a/a/b;

    .line 408
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/d;->m:[B

    .line 410
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/d;->n:Ljava/lang/String;

    .line 411
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/d;->o:Lcom/google/e/a/a/a/b;

    .line 412
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/suggest/e/e;
    .locals 10
    .param p0    # Lcom/google/e/a/a/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x1c

    const/16 v7, 0x15

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 636
    if-nez p0, :cond_0

    .line 637
    const/4 v0, 0x0

    .line 686
    :goto_0
    return-object v0

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v1, Lcom/google/r/b/a/b/ap;->a:Lcom/google/e/a/a/a/d;

    invoke-virtual {v0, v1}, Lcom/google/e/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 641
    :cond_1
    new-instance v1, Lcom/google/android/apps/gmm/suggest/e/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/suggest/e/e;-><init>()V

    .line 642
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_c

    move v0, v3

    :goto_1
    if-nez v0, :cond_2

    invoke-virtual {p0, v3}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d

    :cond_2
    move v0, v3

    :goto_2
    if-eqz v0, :cond_3

    .line 643
    invoke-virtual {p0, v3, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->a:Ljava/lang/String;

    .line 645
    :cond_3
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v9}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_e

    move v0, v3

    :goto_3
    if-nez v0, :cond_4

    invoke-virtual {p0, v9}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_f

    :cond_4
    move v0, v3

    :goto_4
    if-eqz v0, :cond_5

    .line 646
    invoke-virtual {p0, v9, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->b:Ljava/lang/String;

    .line 648
    :cond_5
    const/16 v4, 0xf

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_10

    move v0, v3

    :goto_5
    if-nez v0, :cond_6

    invoke-virtual {p0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_11

    :cond_6
    move v0, v3

    :goto_6
    if-eqz v0, :cond_7

    .line 649
    const/16 v0, 0xf

    invoke-virtual {p0, v0, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->c:Ljava/lang/String;

    .line 651
    :cond_7
    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_12

    move v0, v3

    :goto_7
    if-nez v0, :cond_8

    invoke-virtual {p0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_13

    :cond_8
    move v0, v3

    :goto_8
    if-eqz v0, :cond_9

    .line 652
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    iput v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->d:I

    .line 654
    :cond_9
    const/16 v4, 0x13

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_14

    move v0, v3

    :goto_9
    if-nez v0, :cond_a

    invoke-virtual {p0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_15

    :cond_a
    move v0, v3

    :goto_a
    if-eqz v0, :cond_b

    .line 655
    const/16 v0, 0x13

    invoke-virtual {p0, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    iput v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->e:I

    .line 657
    :cond_b
    iget-object v0, p0, Lcom/google/e/a/a/a/b;->d:Lcom/google/e/a/a/a/d;

    sget-object v4, Lcom/google/r/b/a/b/ap;->a:Lcom/google/e/a/a/a/d;

    invoke-virtual {v0, v4}, Lcom/google/e/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_c
    move v0, v2

    .line 642
    goto/16 :goto_1

    :cond_d
    move v0, v2

    goto/16 :goto_2

    :cond_e
    move v0, v2

    .line 645
    goto/16 :goto_3

    :cond_f
    move v0, v2

    goto/16 :goto_4

    :cond_10
    move v0, v2

    .line 648
    goto/16 :goto_5

    :cond_11
    move v0, v2

    goto :goto_6

    :cond_12
    move v0, v2

    .line 651
    goto :goto_7

    :cond_13
    move v0, v2

    goto :goto_8

    :cond_14
    move v0, v2

    .line 654
    goto :goto_9

    :cond_15
    move v0, v2

    goto :goto_a

    .line 657
    :cond_16
    const/16 v0, 0xb

    iget-object v4, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v5

    move v4, v2

    :goto_b
    if-ge v4, v5, :cond_22

    const/16 v0, 0xb

    const/16 v6, 0x1a

    invoke-virtual {p0, v0, v4, v6}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    const/4 v6, 0x6

    invoke-static {v0, v6, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;IZ)Z

    move-result v6

    if-eqz v6, :cond_21

    move-object v4, v0

    .line 658
    :goto_c
    if-eqz v4, :cond_1a

    .line 659
    const/4 v5, 0x4

    iget-object v0, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_23

    move v0, v3

    :goto_d
    if-nez v0, :cond_17

    invoke-virtual {v4, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_24

    :cond_17
    move v0, v3

    :goto_e
    if-eqz v0, :cond_18

    .line 660
    const/4 v0, 0x4

    .line 661
    invoke-virtual {v4, v0, v8}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 660
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    .line 662
    if-eqz v0, :cond_18

    .line 663
    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->f:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 666
    :cond_18
    const/16 v5, 0x8

    iget-object v0, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_25

    move v0, v3

    :goto_f
    if-nez v0, :cond_19

    invoke-virtual {v4, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_26

    :cond_19
    move v0, v3

    :goto_10
    if-eqz v0, :cond_1a

    .line 667
    const/16 v0, 0x8

    .line 668
    const/16 v5, 0x1a

    invoke-virtual {v4, v0, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 667
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 669
    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 672
    :cond_1a
    const/16 v4, 0x10

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_27

    move v0, v3

    :goto_11
    if-nez v0, :cond_1b

    invoke-virtual {p0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_28

    :cond_1b
    move v0, v3

    :goto_12
    if-eqz v0, :cond_1c

    .line 673
    const/16 v0, 0x10

    .line 674
    const/16 v4, 0x1a

    invoke-virtual {p0, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 675
    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/suggest/e/f;->a(Lcom/google/e/a/a/a/b;I)Lcom/google/b/c/cv;

    move-result-object v4

    iput-object v4, v1, Lcom/google/android/apps/gmm/suggest/e/e;->h:Lcom/google/b/c/cv;

    .line 677
    invoke-static {v0, v9}, Lcom/google/android/apps/gmm/suggest/e/f;->a(Lcom/google/e/a/a/a/b;I)Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->i:Lcom/google/b/c/cv;

    .line 680
    :cond_1c
    const/16 v4, 0x12

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_29

    move v0, v3

    :goto_13
    if-nez v0, :cond_1d

    invoke-virtual {p0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2a

    :cond_1d
    move v0, v3

    :goto_14
    if-eqz v0, :cond_1e

    .line 681
    const/16 v0, 0x12

    invoke-virtual {p0, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->j:Ljava/lang/Integer;

    .line 683
    :cond_1e
    const/16 v4, 0xc

    iget-object v0, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v4}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_2b

    move v0, v3

    :goto_15
    if-nez v0, :cond_1f

    invoke-virtual {p0, v4}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2c

    :cond_1f
    move v0, v3

    :goto_16
    if-eqz v0, :cond_20

    .line 684
    const/16 v0, 0xc

    invoke-virtual {p0, v0, v7}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/apps/gmm/suggest/e/e;->l:Ljava/lang/Integer;

    :cond_20
    move-object v0, v1

    .line 686
    goto/16 :goto_0

    .line 657
    :cond_21
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_b

    :cond_22
    const/4 v0, 0x0

    move-object v4, v0

    goto/16 :goto_c

    :cond_23
    move v0, v2

    .line 659
    goto/16 :goto_d

    :cond_24
    move v0, v2

    goto/16 :goto_e

    :cond_25
    move v0, v2

    .line 666
    goto/16 :goto_f

    :cond_26
    move v0, v2

    goto/16 :goto_10

    :cond_27
    move v0, v2

    .line 672
    goto/16 :goto_11

    :cond_28
    move v0, v2

    goto/16 :goto_12

    :cond_29
    move v0, v2

    .line 680
    goto :goto_13

    :cond_2a
    move v0, v2

    goto :goto_14

    :cond_2b
    move v0, v2

    .line 683
    goto :goto_15

    :cond_2c
    move v0, v2

    goto :goto_16
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Lcom/google/b/c/cv;Landroid/text/style/ForegroundColorSpan;)Ljava/lang/CharSequence;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/text/SpannableStringBuilder;",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/f;",
            ">;",
            "Landroid/text/style/ForegroundColorSpan;",
            ")",
            "Ljava/lang/CharSequence;"
        }
    .end annotation

    .prologue
    .line 588
    if-nez p1, :cond_1

    .line 603
    :cond_0
    return-object p0

    .line 591
    :cond_1
    invoke-virtual {p1}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/suggest/e/f;

    .line 592
    iget-boolean v2, v0, Lcom/google/android/apps/gmm/suggest/e/f;->a:Z

    if-eqz v2, :cond_2

    .line 598
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v2

    iget v3, v0, Lcom/google/android/apps/gmm/suggest/e/f;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 599
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    iget v0, v0, Lcom/google/android/apps/gmm/suggest/e/f;->c:I

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    const/16 v3, 0x21

    .line 593
    invoke-virtual {p0, p2, v2, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method
