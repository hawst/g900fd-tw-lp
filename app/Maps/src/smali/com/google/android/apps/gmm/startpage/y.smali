.class public Lcom/google/android/apps/gmm/startpage/y;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/a;

.field final c:Landroid/content/Context;

.field d:Z

.field final e:Lcom/google/android/apps/gmm/startpage/z;

.field private final f:Lcom/google/android/apps/gmm/map/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/google/android/apps/gmm/startpage/y;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/y;->d:Z

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/startpage/z;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/z;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->e:Lcom/google/android/apps/gmm/startpage/z;

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    .line 75
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/y;->f:Lcom/google/android/apps/gmm/map/t;

    .line 76
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    .line 77
    return-void
.end method


# virtual methods
.method a(Lcom/google/android/apps/gmm/startpage/ac;Lcom/google/android/apps/gmm/startpage/ae;Z)Lcom/google/android/apps/gmm/startpage/aa;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 122
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 123
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/y;->f:Lcom/google/android/apps/gmm/map/t;

    if-nez v1, :cond_0

    .line 135
    :goto_0
    return-object v0

    .line 126
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/y;->f:Lcom/google/android/apps/gmm/map/t;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/w;->b(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v1

    .line 127
    if-nez v1, :cond_1

    .line 128
    sget-object v1, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    goto :goto_0

    .line 133
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v2

    if-nez v2, :cond_3

    :cond_2
    :goto_1
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->i:Lcom/google/o/b/a/v;

    .line 134
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v1, v0, Lcom/google/android/apps/gmm/startpage/aa;->a:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 135
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/startpage/y;->b(Lcom/google/android/apps/gmm/startpage/ac;Lcom/google/android/apps/gmm/startpage/ae;Z)Lcom/google/android/apps/gmm/startpage/aa;

    move-result-object v0

    goto :goto_0

    .line 133
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/apps/gmm/startpage/ac;Lcom/google/android/apps/gmm/startpage/ae;Z)Lcom/google/android/apps/gmm/startpage/aa;
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->o_()Lcom/google/android/apps/gmm/hotels/a/b;

    move-result-object v0

    .line 153
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->a()V

    .line 154
    invoke-interface {v0}, Lcom/google/android/apps/gmm/hotels/a/b;->c()Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 155
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v1, Lcom/google/android/apps/gmm/startpage/aa;->v:Lcom/google/e/a/a/a/b;

    .line 160
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/startpage/ac;->a()Landroid/accounts/Account;

    move-result-object v5

    .line 176
    if-eqz p3, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    .line 178
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->i()Ljava/util/List;

    move-result-object v0

    .line 177
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v1, Lcom/google/android/apps/gmm/startpage/aa;->j:Ljava/util/List;

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v1, Lcom/google/android/apps/gmm/startpage/aa;->x:Landroid/content/res/Resources;

    .line 183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 184
    sget-object v0, Lcom/google/r/b/a/acy;->m:Lcom/google/r/b/a/acy;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    sget-object v0, Lcom/google/r/b/a/acy;->n:Lcom/google/r/b/a/acy;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    sget-object v0, Lcom/google/r/b/a/acy;->s:Lcom/google/r/b/a/acy;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/d/f;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/hm;

    move-result-object v6

    .line 190
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_3

    .line 193
    invoke-static {}, Lcom/google/o/h/a/en;->newBuilder()Lcom/google/o/h/a/ep;

    move-result-object v0

    .line 194
    invoke-static {}, Lcom/google/o/h/a/et;->newBuilder()Lcom/google/o/h/a/ev;

    move-result-object v7

    if-nez v6, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    move v0, v3

    .line 190
    goto :goto_0

    .line 194
    :cond_2
    iget v8, v7, Lcom/google/o/h/a/ev;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v7, Lcom/google/o/h/a/ev;->a:I

    iget v6, v6, Lcom/google/maps/g/a/hm;->h:I

    iput v6, v7, Lcom/google/o/h/a/ev;->b:I

    .line 193
    iget-object v6, v0, Lcom/google/o/h/a/ep;->b:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/o/h/a/ev;->g()Lcom/google/n/t;

    move-result-object v7

    iget-object v8, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v6, Lcom/google/n/ao;->d:Z

    iget v2, v0, Lcom/google/o/h/a/ep;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/o/h/a/ep;->a:I

    .line 194
    invoke-virtual {v0}, Lcom/google/o/h/a/ep;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/en;

    .line 192
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->k:Lcom/google/o/h/a/en;

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    .line 198
    invoke-static {}, Lcom/google/o/h/a/ed;->newBuilder()Lcom/google/o/h/a/eh;

    move-result-object v6

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_7

    sget-object v7, Lcom/google/o/h/a/ei;->b:Lcom/google/o/h/a/ei;

    if-nez v7, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    iget v8, v6, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v6, Lcom/google/o/h/a/eh;->a:I

    iget v7, v7, Lcom/google/o/h/a/ei;->d:I

    iput v7, v6, Lcom/google/o/h/a/eh;->b:I

    :goto_1
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v7, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    if-eqz v7, :cond_5

    iget v7, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-nez v7, :cond_6

    :cond_5
    sget-object v7, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    const-string v8, "Empty width/height are put in display size"

    new-array v9, v3, [Ljava/lang/Object;

    invoke-static {v7, v8, v9}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_6
    iget v7, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v8, v6, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v8, v8, 0x2

    iput v8, v6, Lcom/google/o/h/a/eh;->a:I

    iput v7, v6, Lcom/google/o/h/a/eh;->c:I

    iget v7, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v8, v6, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v8, v8, 0x4

    iput v8, v6, Lcom/google/o/h/a/eh;->a:I

    iput v7, v6, Lcom/google/o/h/a/eh;->d:I

    iget v7, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iget v8, v6, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v8, v8, 0x8

    iput v8, v6, Lcom/google/o/h/a/eh;->a:I

    iput v7, v6, Lcom/google/o/h/a/eh;->e:I

    iget v7, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v8, 0xa0

    if-gt v7, v8, :cond_9

    sget-object v0, Lcom/google/o/h/a/ek;->a:Lcom/google/o/h/a/ek;

    invoke-virtual {v6, v0}, Lcom/google/o/h/a/eh;->a(Lcom/google/o/h/a/ek;)Lcom/google/o/h/a/eh;

    :goto_2
    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->q()Lcom/google/android/apps/gmm/shared/net/a/o;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/net/a/o;->a(Lcom/google/android/apps/gmm/shared/net/ad;)Ljava/util/List;

    move-result-object v7

    move v2, v3

    :goto_3
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_c

    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v6}, Lcom/google/o/h/a/eh;->c()V

    iget-object v8, v6, Lcom/google/o/h/a/eh;->f:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_7
    sget-object v7, Lcom/google/o/h/a/ei;->a:Lcom/google/o/h/a/ei;

    if-nez v7, :cond_8

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_8
    iget v8, v6, Lcom/google/o/h/a/eh;->a:I

    or-int/lit8 v8, v8, 0x1

    iput v8, v6, Lcom/google/o/h/a/eh;->a:I

    iget v7, v7, Lcom/google/o/h/a/ei;->d:I

    iput v7, v6, Lcom/google/o/h/a/eh;->b:I

    goto/16 :goto_1

    :cond_9
    iget v7, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v8, 0xf0

    if-gt v7, v8, :cond_a

    sget-object v0, Lcom/google/o/h/a/ek;->b:Lcom/google/o/h/a/ek;

    invoke-virtual {v6, v0}, Lcom/google/o/h/a/eh;->a(Lcom/google/o/h/a/ek;)Lcom/google/o/h/a/eh;

    goto :goto_2

    :cond_a
    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v7, 0x140

    if-gt v0, v7, :cond_b

    sget-object v0, Lcom/google/o/h/a/ek;->c:Lcom/google/o/h/a/ek;

    invoke-virtual {v6, v0}, Lcom/google/o/h/a/eh;->a(Lcom/google/o/h/a/ek;)Lcom/google/o/h/a/eh;

    goto :goto_2

    :cond_b
    sget-object v0, Lcom/google/o/h/a/ek;->d:Lcom/google/o/h/a/ek;

    invoke-virtual {v6, v0}, Lcom/google/o/h/a/eh;->a(Lcom/google/o/h/a/ek;)Lcom/google/o/h/a/eh;

    goto :goto_2

    :cond_c
    invoke-virtual {v6}, Lcom/google/o/h/a/eh;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ed;

    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->l:Lcom/google/o/h/a/ed;

    .line 201
    invoke-static {}, Lcom/google/android/apps/gmm/cardui/a/a;->b()Lcom/google/o/h/a/d;

    move-result-object v0

    sget-object v2, Lcom/google/o/h/a/a/a;->E:Lcom/google/e/a/a/a/d;

    .line 200
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 199
    iget-object v6, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    invoke-static {}, Lcom/google/o/h/a/d;->d()Lcom/google/o/h/a/d;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    if-eqz v0, :cond_d

    :goto_4
    check-cast v0, Lcom/google/o/h/a/d;

    iput-object v0, v6, Lcom/google/android/apps/gmm/startpage/aa;->m:Lcom/google/o/h/a/d;

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/myplaces/a/a;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->C:Ljava/lang/String;

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->av:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_e

    invoke-static {v2, v5}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    sget-object v2, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    const-string v2, "configToken is loaded:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_f

    invoke-virtual {v2, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_6
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->B:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    .line 205
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/android/apps/gmm/shared/b/a;)Lcom/google/maps/g/a/al;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->w:Lcom/google/maps/g/a/al;

    .line 206
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object p2, v0, Lcom/google/android/apps/gmm/startpage/aa;->E:Lcom/google/android/apps/gmm/startpage/ae;

    .line 207
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/startpage/aa;->n:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->f:Lcom/google/android/apps/gmm/map/t;

    if-nez v0, :cond_10

    move-object v0, v4

    .line 212
    :goto_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    .line 213
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 211
    invoke-static {v0, v2}, Lcom/google/android/apps/gmm/search/bc;->a(Landroid/graphics/Point;Landroid/content/res/Resources;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    .line 210
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->o:Lcom/google/e/a/a/a/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    .line 215
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/e;->bb:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 214
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->t:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->c:Landroid/content/Context;

    .line 217
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/e;->bt:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 216
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iput v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->u:I

    .line 219
    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    if-nez v1, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    move-object v0, v2

    .line 199
    goto/16 :goto_4

    :cond_e
    move-object v0, v4

    .line 204
    goto :goto_5

    :cond_f
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 207
    :cond_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/y;->f:Lcom/google/android/apps/gmm/map/t;

    .line 212
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    goto :goto_7

    :cond_11
    move-object v0, v1

    .line 219
    check-cast v0, Ljava/util/List;

    iput-object v0, v2, Lcom/google/android/apps/gmm/startpage/aa;->D:Ljava/util/List;

    .line 220
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/aa;->l:Lcom/google/o/h/a/ed;

    if-nez v1, :cond_12

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/aa;->m:Lcom/google/o/h/a/d;

    if-nez v1, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/aa;->E:Lcom/google/android/apps/gmm/startpage/ae;

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_14
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/ac;->a:Lcom/google/android/apps/gmm/startpage/aa;

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/y;->e:Lcom/google/android/apps/gmm/startpage/z;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/z;->a(Lcom/google/android/apps/gmm/startpage/aa;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 222
    sget-object v0, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    .line 231
    :goto_8
    return-object v4

    .line 225
    :cond_15
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    move-result v1

    .line 226
    if-nez v1, :cond_16

    .line 227
    sget-object v0, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    goto :goto_8

    .line 230
    :cond_16
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/y;->e:Lcom/google/android/apps/gmm/startpage/z;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/startpage/z;->b(Lcom/google/android/apps/gmm/startpage/aa;)V

    move-object v4, v0

    .line 231
    goto :goto_8
.end method
