.class public Lcom/google/android/apps/gmm/place/bw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/g/a/d;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field private d:Landroid/content/Context;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/apps/gmm/base/views/c/k;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Runnable;

.field private i:Lcom/google/android/apps/gmm/place/ownerresponse/b;

.field private j:Lcom/google/android/apps/gmm/base/l/a/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/apps/gmm/place/bw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/bw;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/maps/g/pc;Z)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 159
    iget-object v0, p0, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    .line 161
    if-nez p1, :cond_1

    iget v2, v0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    :goto_0
    if-eqz v1, :cond_1

    .line 162
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    .line 164
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 169
    :goto_1
    return-object v0

    .line 161
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 166
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/place/bw;->c:Ljava/lang/String;

    const-string v1, "Failed to parse report review link: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 169
    :cond_1
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 166
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->h:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 189
    :cond_0
    const/4 v0, 0x0

    return-object v0

    .line 186
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/maps/g/pc;ZZLcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/g/a/f;)V
    .locals 8
    .param p6    # Lcom/google/android/apps/gmm/place/g/a/f;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/maps/g/pc;",
            "ZZ",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/g/a/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bw;->d:Landroid/content/Context;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->f:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->g:Ljava/lang/String;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->a:Ljava/lang/String;

    .line 66
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->b:Ljava/lang/Integer;

    .line 68
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->h:Ljava/lang/Runnable;

    .line 69
    iget-object v0, p2, Lcom/google/maps/g/pc;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    .line 72
    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/a;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/shared/c/c/a;-><init>(Landroid/content/Context;)V

    .line 75
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    if-eqz p3, :cond_d

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_b

    :cond_0
    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->d:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->pi:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_1
    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_10

    :cond_2
    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_11

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->d:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->ap:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    :goto_4
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_12

    :cond_3
    :goto_5
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->i()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_13

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->i()Ljava/lang/String;

    move-result-object v1

    :goto_6
    new-instance v3, Lcom/google/android/apps/gmm/base/views/c/k;

    sget-object v4, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v5, Lcom/google/android/apps/gmm/f;->cD:I

    invoke-direct {v3, v1, v4, v5}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/place/bw;->f:Lcom/google/android/apps/gmm/base/views/c/k;

    .line 78
    iget v1, p2, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v3, 0x20

    if-ne v1, v3, :cond_14

    const/4 v1, 0x1

    :goto_7
    if-eqz v1, :cond_4

    .line 79
    iget v1, p2, Lcom/google/maps/g/pc;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->b:Ljava/lang/Integer;

    .line 82
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/bw;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget v4, Lcom/google/android/apps/gmm/j;->b:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v1, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 81
    if-eqz v1, :cond_4

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_15

    .line 86
    :cond_4
    :goto_8
    iget v1, p2, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v3, 0x10

    if-ne v1, v3, :cond_16

    const/4 v1, 0x1

    :goto_9
    if-eqz v1, :cond_17

    .line 87
    invoke-virtual {p2}, Lcom/google/maps/g/pc;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->g:Ljava/lang/String;

    .line 95
    :cond_5
    :goto_a
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_19

    :cond_6
    const/4 v1, 0x1

    :goto_b
    if-eqz v1, :cond_7

    .line 96
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->g:Ljava/lang/String;

    if-eqz v1, :cond_7

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_1a

    .line 100
    :cond_7
    :goto_c
    invoke-virtual {p2}, Lcom/google/maps/g/pc;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->a:Ljava/lang/String;

    .line 102
    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->a:Ljava/lang/String;

    if-eqz v1, :cond_8

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-nez v3, :cond_1b

    .line 104
    :cond_8
    :goto_d
    iget-object v1, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->a:Ljava/lang/StringBuffer;

    .line 106
    iget v1, v0, Lcom/google/maps/g/hg;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1c

    const/4 v1, 0x1

    :goto_e
    if-eqz v1, :cond_9

    .line 108
    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    .line 107
    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/base/views/d/b;->a(Landroid/content/Context;Ljava/lang/String;Z)Ljava/lang/Runnable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->h:Ljava/lang/Runnable;

    .line 111
    :cond_9
    invoke-static {p2, p3}, Lcom/google/android/apps/gmm/place/bw;->a(Lcom/google/maps/g/pc;Z)Landroid/content/Intent;

    .line 113
    if-eqz p3, :cond_1e

    .line 116
    if-nez p6, :cond_a

    .line 117
    sget-object v0, Lcom/google/android/apps/gmm/place/bw;->c:Ljava/lang/String;

    const-string v1, "UserSelfReviewViewModel is null."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 119
    :cond_a
    invoke-interface {p6}, Lcom/google/android/apps/gmm/place/g/a/f;->a()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1d

    new-instance v0, Lcom/google/android/apps/gmm/place/g/f;

    invoke-direct {v0, p1, p6, p5}, Lcom/google/android/apps/gmm/place/g/f;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/place/g/a/f;Lcom/google/android/apps/gmm/x/o;)V

    :goto_f
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->j:Lcom/google/android/apps/gmm/base/l/a/u;

    .line 126
    :goto_10
    new-instance v0, Lcom/google/android/apps/gmm/place/ownerresponse/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 131
    invoke-virtual {p5}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v5

    check-cast v5, Lcom/google/r/b/a/acq;

    .line 132
    iget-object v1, p2, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ju;->g()Lcom/google/maps/g/ju;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/ju;

    invoke-virtual {v1}, Lcom/google/maps/g/ju;->d()Ljava/lang/String;

    move-result-object v6

    move-object v1, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/ownerresponse/b;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;Lcom/google/android/apps/gmm/place/ownerresponse/j;Lcom/google/maps/g/pc;Lcom/google/r/b/a/acq;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->i:Lcom/google/android/apps/gmm/place/ownerresponse/b;

    .line 133
    return-void

    .line 75
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->d:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->mO:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_1

    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_f

    :cond_e
    const/4 v1, 0x1

    :goto_11
    if-eqz v1, :cond_1

    const-string v1, ""

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    goto/16 :goto_2

    :cond_f
    const/4 v1, 0x0

    goto :goto_11

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->d:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->aq:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_4

    :cond_12
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_5

    :cond_13
    const-string v1, ""

    goto/16 :goto_6

    .line 78
    :cond_14
    const/4 v1, 0x0

    goto/16 :goto_7

    .line 81
    :cond_15
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_8

    .line 86
    :cond_16
    const/4 v1, 0x0

    goto/16 :goto_9

    .line 88
    :cond_17
    if-eqz p3, :cond_5

    .line 89
    if-eqz p4, :cond_18

    .line 90
    sget v1, Lcom/google/android/apps/gmm/l;->kW:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->g:Ljava/lang/String;

    goto/16 :goto_a

    .line 92
    :cond_18
    sget v1, Lcom/google/android/apps/gmm/l;->kV:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bw;->g:Ljava/lang/String;

    goto/16 :goto_a

    .line 95
    :cond_19
    const/4 v1, 0x0

    goto/16 :goto_b

    .line 96
    :cond_1a
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_c

    .line 103
    :cond_1b
    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/shared/c/c/a;->a(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/shared/c/c/a;->b:Z

    goto/16 :goto_d

    .line 106
    :cond_1c
    const/4 v1, 0x0

    goto/16 :goto_e

    .line 119
    :cond_1d
    const/4 v0, 0x0

    goto/16 :goto_f

    .line 123
    :cond_1e
    new-instance v0, Lcom/google/android/apps/gmm/place/review/c;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/place/review/c;-><init>(Landroid/content/Context;Lcom/google/maps/g/pc;Z)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->j:Lcom/google/android/apps/gmm/base/l/a/u;

    goto/16 :goto_10
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->f:Lcom/google/android/apps/gmm/base/views/c/k;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 224
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bw;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public final i()Lcom/google/android/apps/gmm/place/ownerresponse/a/a;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->i:Lcom/google/android/apps/gmm/place/ownerresponse/b;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/base/l/a/u;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bw;->j:Lcom/google/android/apps/gmm/base/l/a/u;

    return-object v0
.end method
