.class public Lcom/google/android/apps/gmm/base/views/ExpandableTextView;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field public a:Z

.field public b:Z

.field public c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    sget-object v0, Lcom/google/android/apps/gmm/n;->n:[I

    .line 52
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 53
    sget v1, Lcom/google/android/apps/gmm/n;->o:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->c:I

    .line 54
    sget v1, Lcom/google/android/apps/gmm/n;->p:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/base/views/o;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/o;-><init>(Lcom/google/android/apps/gmm/base/views/ExpandableTextView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-void
.end method


# virtual methods
.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    .line 68
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->b:Z

    if-eqz v0, :cond_0

    const v0, 0x7fffffff

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->setMaxLines(I)V

    .line 69
    return-void

    .line 68
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->c:I

    goto :goto_0
.end method

.method public final setExpanded(ZLcom/google/android/apps/gmm/base/views/p;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/base/views/p;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->b:Z

    if-eq p1, v0, :cond_0

    .line 100
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->b:Z

    .line 101
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->b:Z

    if-eqz v0, :cond_1

    const v0, 0x7fffffff

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->setMaxLines(I)V

    .line 102
    if-eqz p2, :cond_0

    .line 103
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->a:Z

    iput-boolean v0, p2, Lcom/google/android/apps/gmm/base/views/p;->b:Z

    .line 104
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->b:Z

    iput-boolean v0, p2, Lcom/google/android/apps/gmm/base/views/p;->a:Z

    .line 107
    :cond_0
    return-void

    .line 101
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/ExpandableTextView;->c:I

    goto :goto_0
.end method
