.class public Lcom/google/android/apps/gmm/map/internal/d/b/c;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/map/internal/d/b/k;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field static final b:Lcom/google/android/apps/gmm/map/internal/d/b/k;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field static final c:Lcom/google/android/apps/gmm/map/internal/d/b/k;

.field static final d:Lcom/google/android/apps/gmm/map/internal/d/b/k;

.field static final e:Lcom/google/android/apps/gmm/map/internal/d/b/k;

.field static final f:Lcom/google/android/apps/gmm/map/internal/d/b/k;

.field static final g:Lcom/google/android/apps/gmm/map/internal/d/b/d;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/e;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 31
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/h;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/h;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->b:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 38
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/g;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->c:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/f;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->d:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/j;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/j;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->e:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/i;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->f:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 64
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/d/b/l;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/d/b/l;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->g:Lcom/google/android/apps/gmm/map/internal/d/b/d;

    return-void
.end method

.method static a(ILcom/google/android/apps/gmm/map/util/f;Lcom/google/android/apps/gmm/map/util/f;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 751
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v0

    .line 752
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v1

    .line 751
    invoke-static {v0, v3, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a([BII)I

    move-result v0

    xor-int/2addr v0, p0

    .line 753
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/util/f;->a()[B

    move-result-object v1

    .line 754
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/util/f;->b()I

    move-result v2

    .line 753
    invoke-static {v1, v3, v2}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a([BII)I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method static a([BI)I
    .locals 1

    .prologue
    .line 758
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/apps/gmm/map/internal/d/b/t;->a([BII)I

    move-result v0

    return v0
.end method

.method static a([B)Lcom/google/android/apps/gmm/map/internal/d/b/k;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/16 v6, 0x26

    const/16 v5, 0x1d

    const/4 v4, 0x4

    .line 128
    array-length v1, p0

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-object v0

    .line 132
    :cond_1
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/google/android/apps/gmm/map/internal/d/b/p;->a([BI)I

    move-result v1

    .line 133
    const/16 v2, 0x18

    if-ne v1, v2, :cond_2

    .line 134
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->a:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_0

    .line 135
    :cond_2
    const/16 v2, 0x19

    if-ne v1, v2, :cond_3

    aget-byte v2, p0, v4

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/c;->b:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 137
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 138
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->b:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_0

    .line 139
    :cond_3
    if-ne v1, v5, :cond_4

    aget-byte v2, p0, v4

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/c;->c:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 141
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 142
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->c:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_0

    .line 143
    :cond_4
    if-ne v1, v6, :cond_5

    aget-byte v2, p0, v4

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/c;->d:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 145
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a()I

    move-result v3

    if-ne v2, v3, :cond_5

    .line 146
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->d:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_0

    .line 147
    :cond_5
    if-ne v1, v5, :cond_6

    aget-byte v2, p0, v4

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/d/b/c;->e:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 149
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a()I

    move-result v3

    if-ne v2, v3, :cond_6

    .line 150
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->e:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_0

    .line 151
    :cond_6
    if-ne v1, v6, :cond_0

    aget-byte v1, p0, v4

    sget-object v2, Lcom/google/android/apps/gmm/map/internal/d/b/c;->f:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    .line 153
    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/b/k;->a()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 154
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->f:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    goto :goto_0
.end method

.method static a(Lcom/google/android/apps/gmm/map/internal/d/b/k;)Z
    .locals 1

    .prologue
    .line 121
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->d:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    if-eq v0, p0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/map/internal/d/b/c;->f:Lcom/google/android/apps/gmm/map/internal/d/b/k;

    if-ne v0, p0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
