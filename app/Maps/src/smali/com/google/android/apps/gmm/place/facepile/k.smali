.class public Lcom/google/android/apps/gmm/place/facepile/k;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/facepile/a/c;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/base/activities/c;

.field private final b:Lcom/google/android/apps/gmm/x/a;

.field private c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/maps/g/fk;

.field private e:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/gmm/place/facepile/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/facepile/k;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 41
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->b:Lcom/google/android/apps/gmm/x/a;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->d:Lcom/google/maps/g/fk;

    iget-object v0, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->d:Lcom/google/maps/g/fk;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fg;->h()Lcom/google/maps/g/fg;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fg;

    invoke-virtual {v0}, Lcom/google/maps/g/fg;->g()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 85
    :goto_1
    if-nez v2, :cond_2

    move-object v0, v1

    :goto_2
    return-object v0

    .line 84
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move-object v2, v1

    goto :goto_1

    .line 85
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/k;

    sget-object v1, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    sget v3, Lcom/google/android/apps/gmm/f;->cD:I

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;I)V

    goto :goto_2
.end method

.method public final a()Lcom/google/android/libraries/curvular/cf;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->aR:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 62
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->c()Ljava/lang/String;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/k;->d:Lcom/google/maps/g/fk;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/facepile/k;->b:Lcom/google/android/apps/gmm/x/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/facepile/k;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;->a(Lcom/google/maps/g/fk;Ljava/lang/String;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/place/facepile/FacepileListViewFragment;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/facepile/k;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x6

    const/4 v3, 0x0

    .line 48
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/facepile/k;->c:Lcom/google/android/apps/gmm/x/o;

    .line 49
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->B:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ki;->i()Lcom/google/maps/g/ki;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ki;

    iget-object v0, v0, Lcom/google/maps/g/ki;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fk;->d()Lcom/google/maps/g/fk;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fk;

    iget-object v1, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-gt v1, v8, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->d:Lcom/google/maps/g/fk;

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->d:Lcom/google/maps/g/fk;

    iget-object v0, v0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v0}, Lcom/google/n/aq;->size()I

    move-result v0

    if-nez v0, :cond_4

    const-string v0, ""

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->e:Ljava/lang/CharSequence;

    .line 51
    return-void

    .line 49
    :cond_0
    invoke-static {}, Lcom/google/maps/g/fk;->newBuilder()Lcom/google/maps/g/fm;

    move-result-object v4

    iget-boolean v1, v0, Lcom/google/maps/g/fk;->d:Z

    iget v2, v4, Lcom/google/maps/g/fm;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v4, Lcom/google/maps/g/fm;->a:I

    iput-boolean v1, v4, Lcom/google/maps/g/fm;->c:Z

    move v2, v3

    :goto_2
    iget-object v1, v0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v1}, Lcom/google/n/aq;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    iget-object v1, v0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v1, v2}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    invoke-virtual {v4}, Lcom/google/maps/g/fm;->d()V

    iget-object v5, v4, Lcom/google/maps/g/fm;->d:Lcom/google/n/aq;

    invoke-interface {v5, v1}, Lcom/google/n/aq;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    :cond_2
    move v2, v3

    :goto_3
    if-ge v2, v8, :cond_3

    iget-object v1, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    invoke-static {}, Lcom/google/maps/g/fg;->newBuilder()Lcom/google/maps/g/fi;

    move-result-object v5

    iget-object v1, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fg;->h()Lcom/google/maps/g/fg;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/fg;

    invoke-virtual {v5, v1}, Lcom/google/maps/g/fi;->a(Lcom/google/maps/g/fg;)Lcom/google/maps/g/fi;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/maps/g/fm;->c()V

    iget-object v5, v4, Lcom/google/maps/g/fm;->b:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/maps/g/fi;->g()Lcom/google/n/t;

    move-result-object v1

    new-instance v6, Lcom/google/n/ao;

    invoke-direct {v6}, Lcom/google/n/ao;-><init>()V

    iget-object v7, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v6, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v6, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v6, Lcom/google/n/ao;->d:Z

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :cond_3
    invoke-virtual {v4}, Lcom/google/maps/g/fm;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/fk;

    goto/16 :goto_0

    .line 50
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->d:Lcom/google/maps/g/fk;

    iget-object v0, v0, Lcom/google/maps/g/fk;->e:Lcom/google/n/aq;

    invoke-interface {v0, v3}, Lcom/google/n/aq;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_1
.end method

.method public final b(Ljava/lang/Integer;)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->d:Lcom/google/maps/g/fk;

    iget-object v0, v0, Lcom/google/maps/g/fk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/facepile/k;->e:Ljava/lang/CharSequence;

    return-object v0
.end method
