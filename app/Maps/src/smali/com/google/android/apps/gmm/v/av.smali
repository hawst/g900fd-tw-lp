.class public Lcom/google/android/apps/gmm/v/av;
.super Lcom/google/android/apps/gmm/v/co;
.source "PG"


# instance fields
.field private final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public e:Ljava/nio/ByteBuffer;

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Lcom/google/android/apps/gmm/v/aw;

.field private final m:Z

.field private final n:Z

.field private final o:Z

.field private final p:Z

.field private final q:Z

.field private final r:Z

.field private final s:Z

.field private t:Ljava/nio/ShortBuffer;

.field private u:[I

.field private v:[I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/aw;IIII)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 369
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/co;-><init>()V

    .line 196
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    .line 201
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    .line 212
    iput v2, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    .line 371
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    .line 372
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    aput v3, v0, v2

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aput v3, v0, v2

    .line 386
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->n:Z

    .line 387
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->m:Z

    .line 388
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->o:Z

    .line 389
    and-int/lit8 v0, p4, 0x8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->q:Z

    .line 390
    and-int/lit8 v0, p4, 0x20

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    .line 391
    and-int/lit8 v0, p4, 0x10

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->p:Z

    .line 392
    and-int/lit8 v0, p4, 0x40

    if-eqz v0, :cond_6

    :goto_6
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->s:Z

    .line 394
    iput p4, p0, Lcom/google/android/apps/gmm/v/av;->d:I

    .line 401
    iput p5, p0, Lcom/google/android/apps/gmm/v/av;->c:I

    .line 402
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/v/av;->a()V

    .line 404
    iput p2, p0, Lcom/google/android/apps/gmm/v/av;->b:I

    .line 405
    iput p3, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    .line 406
    return-void

    :cond_0
    move v0, v2

    .line 386
    goto :goto_0

    :cond_1
    move v0, v2

    .line 387
    goto :goto_1

    :cond_2
    move v0, v2

    .line 388
    goto :goto_2

    :cond_3
    move v0, v2

    .line 389
    goto :goto_3

    :cond_4
    move v0, v2

    .line 390
    goto :goto_4

    :cond_5
    move v0, v2

    .line 391
    goto :goto_5

    :cond_6
    move v1, v2

    .line 392
    goto :goto_6
.end method

.method public constructor <init>(Ljava/nio/ByteBuffer;I[SII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 318
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/co;-><init>()V

    .line 196
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    .line 201
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    .line 212
    iput v2, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 217
    iput-object v4, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    aput v3, v0, v2

    .line 332
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aput v3, v0, v2

    .line 334
    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->n:Z

    .line 335
    and-int/lit8 v0, p4, 0x1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->m:Z

    .line 336
    and-int/lit8 v0, p4, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->o:Z

    .line 337
    and-int/lit8 v0, p4, 0x8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->q:Z

    .line 338
    and-int/lit8 v0, p4, 0x20

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    .line 339
    and-int/lit8 v0, p4, 0x10

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->p:Z

    .line 340
    and-int/lit8 v0, p4, 0x40

    if-eqz v0, :cond_6

    :goto_6
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->s:Z

    .line 342
    iput p4, p0, Lcom/google/android/apps/gmm/v/av;->d:I

    .line 350
    iput p5, p0, Lcom/google/android/apps/gmm/v/av;->c:I

    .line 351
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/v/av;->a()V

    .line 353
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    .line 354
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 355
    iput p2, p0, Lcom/google/android/apps/gmm/v/av;->b:I

    .line 357
    if-eqz p3, :cond_7

    array-length v0, p3

    if-lez v0, :cond_7

    .line 358
    array-length v0, p3

    shl-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 359
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p3}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 361
    array-length v0, p3

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    .line 366
    :goto_7
    return-void

    :cond_0
    move v0, v2

    .line 334
    goto :goto_0

    :cond_1
    move v0, v2

    .line 335
    goto :goto_1

    :cond_2
    move v0, v2

    .line 336
    goto :goto_2

    :cond_3
    move v0, v2

    .line 337
    goto :goto_3

    :cond_4
    move v0, v2

    .line 338
    goto :goto_4

    :cond_5
    move v0, v2

    .line 339
    goto :goto_5

    :cond_6
    move v1, v2

    .line 340
    goto :goto_6

    .line 363
    :cond_7
    iput-object v4, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    .line 364
    iput v2, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    goto :goto_7
.end method

.method public constructor <init>([FII)V
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/apps/gmm/v/av;-><init>([F[SII)V

    .line 248
    return-void
.end method

.method public constructor <init>([F[SII)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 262
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/co;-><init>()V

    .line 196
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    .line 201
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    .line 212
    iput v2, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 217
    iput-object v4, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    aput v3, v0, v2

    .line 276
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aput v3, v0, v2

    .line 278
    and-int/lit8 v0, p3, 0x2

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->n:Z

    .line 279
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->m:Z

    .line 280
    and-int/lit8 v0, p3, 0x4

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->o:Z

    .line 281
    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->q:Z

    .line 282
    and-int/lit8 v0, p3, 0x20

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    .line 283
    and-int/lit8 v0, p3, 0x10

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->p:Z

    .line 284
    and-int/lit8 v0, p3, 0x40

    if-eqz v0, :cond_6

    :goto_6
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->s:Z

    .line 286
    iput p3, p0, Lcom/google/android/apps/gmm/v/av;->d:I

    .line 294
    iput p4, p0, Lcom/google/android/apps/gmm/v/av;->c:I

    .line 295
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    if-eqz v0, :cond_7

    .line 296
    new-instance v0, Ljava/lang/RuntimeException;

    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->d:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x42

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "COLORS_BYTE_MASK can not be used with this constructor "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 278
    goto :goto_0

    :cond_1
    move v0, v2

    .line 279
    goto :goto_1

    :cond_2
    move v0, v2

    .line 280
    goto :goto_2

    :cond_3
    move v0, v2

    .line 281
    goto :goto_3

    :cond_4
    move v0, v2

    .line 282
    goto :goto_4

    :cond_5
    move v0, v2

    .line 283
    goto :goto_5

    :cond_6
    move v1, v2

    .line 284
    goto :goto_6

    .line 299
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/v/av;->a()V

    .line 301
    array-length v0, p1

    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    div-int/lit8 v1, v1, 0x4

    div-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->b:I

    .line 302
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    mul-int/2addr v0, v1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 303
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    .line 304
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 306
    if-eqz p2, :cond_8

    array-length v0, p2

    if-lez v0, :cond_8

    .line 307
    array-length v0, p2

    shl-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 308
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asShortBuffer()Ljava/nio/ShortBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    invoke-virtual {v0, p2}, Ljava/nio/ShortBuffer;->put([S)Ljava/nio/ShortBuffer;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 310
    array-length v0, p2

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    .line 315
    :goto_7
    return-void

    .line 312
    :cond_8
    iput-object v4, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    .line 313
    iput v2, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    goto :goto_7
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 418
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->m:Z

    if-eqz v1, :cond_6

    .line 419
    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/v/av;->s:Z

    if-eqz v2, :cond_5

    :goto_0
    shl-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 424
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->o:Z

    if-eqz v0, :cond_1

    .line 425
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->h:I

    .line 426
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    add-int/lit8 v0, v0, 0xc

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 428
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->q:Z

    if-eqz v0, :cond_2

    .line 429
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->i:I

    .line 430
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    add-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 432
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    if-eqz v0, :cond_3

    .line 433
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->i:I

    .line 434
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    add-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 436
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/av;->p:Z

    if-eqz v0, :cond_4

    .line 437
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->g:I

    .line 438
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    add-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    .line 440
    :cond_4
    return-void

    .line 419
    :cond_5
    const/4 v0, 0x3

    goto :goto_0

    .line 420
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->n:Z

    if-eqz v1, :cond_0

    .line 422
    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/v/av;->s:Z

    if-eqz v2, :cond_7

    :goto_2
    shl-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    goto :goto_1

    :cond_7
    const/4 v0, 0x4

    goto :goto_2
.end method

.method public a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V
    .locals 12

    .prologue
    const/16 v2, 0x1406

    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v9, 0x2

    const/4 v0, 0x0

    .line 511
    instance-of v1, p2, Lcom/google/android/apps/gmm/v/av;

    if-eqz v1, :cond_c

    check-cast p2, Lcom/google/android/apps/gmm/v/av;

    .line 522
    :goto_0
    if-nez p2, :cond_d

    move v1, v0

    move v3, v0

    move v4, v0

    move v5, v0

    .line 534
    :goto_1
    if-nez p2, :cond_0

    .line 535
    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 537
    :cond_0
    iget-boolean v6, p0, Lcom/google/android/apps/gmm/v/av;->p:Z

    if-eqz v6, :cond_1

    if-nez v5, :cond_1

    .line 538
    invoke-static {v11}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 540
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/apps/gmm/v/av;->o:Z

    if-eqz v5, :cond_2

    if-nez v4, :cond_2

    .line 541
    invoke-static {v10}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 543
    :cond_2
    iget-boolean v4, p0, Lcom/google/android/apps/gmm/v/av;->q:Z

    if-eqz v4, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    if-eqz v3, :cond_5

    if-nez v1, :cond_5

    .line 544
    :cond_4
    invoke-static {v9}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 546
    :cond_5
    const v1, 0x8892

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    aget v3, v3, v0

    invoke-static {v1, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 552
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->m:Z

    if-eqz v1, :cond_f

    .line 556
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->s:Z

    if-eqz v1, :cond_e

    move v1, v9

    :goto_2
    iget v4, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    move v3, v0

    move v5, v0

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 564
    :cond_6
    :goto_3
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->o:Z

    if-eqz v1, :cond_7

    .line 568
    const/4 v4, 0x3

    iget v7, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iget v8, p0, Lcom/google/android/apps/gmm/v/av;->h:I

    move v3, v10

    move v5, v2

    move v6, v10

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 570
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->q:Z

    if-eqz v1, :cond_8

    .line 575
    iget v7, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iget v8, p0, Lcom/google/android/apps/gmm/v/av;->i:I

    move v3, v9

    move v4, v11

    move v5, v2

    move v6, v0

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 577
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    if-eqz v1, :cond_9

    .line 582
    const/16 v5, 0x1401

    iget v7, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iget v8, p0, Lcom/google/android/apps/gmm/v/av;->i:I

    move v3, v9

    move v4, v11

    move v6, v10

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 585
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->p:Z

    if-eqz v1, :cond_a

    .line 590
    iget v7, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    iget v8, p0, Lcom/google/android/apps/gmm/v/av;->g:I

    move v3, v11

    move v4, v9

    move v5, v2

    move v6, v10

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 592
    :cond_a
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v1, v1, v0

    const/4 v2, -0x1

    if-eq v1, v2, :cond_b

    .line 597
    const v1, 0x8893

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v0, v2, v0

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 617
    :cond_b
    return-void

    .line 511
    :cond_c
    const/4 p2, 0x0

    goto/16 :goto_0

    .line 528
    :cond_d
    iget-boolean v5, p2, Lcom/google/android/apps/gmm/v/av;->p:Z

    .line 529
    iget-boolean v4, p2, Lcom/google/android/apps/gmm/v/av;->o:Z

    .line 530
    iget-boolean v3, p2, Lcom/google/android/apps/gmm/v/av;->q:Z

    .line 531
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/av;->r:Z

    goto/16 :goto_1

    .line 556
    :cond_e
    const/4 v1, 0x3

    goto :goto_2

    .line 559
    :cond_f
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->n:Z

    if-eqz v1, :cond_6

    .line 560
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->s:Z

    if-eqz v1, :cond_10

    move v4, v9

    :goto_4
    const/16 v5, 0x1402

    iget v7, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    move v3, v0

    move v6, v0

    move v8, v0

    invoke-static/range {v3 .. v8}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    goto :goto_3

    :cond_10
    const/4 v4, 0x3

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 8

    .prologue
    const v7, 0x8892

    const/4 v2, -0x1

    const v6, 0x8893

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 445
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/gmm/v/co;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    move-result v0

    .line 446
    if-eqz v0, :cond_3

    .line 448
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-eqz v1, :cond_4

    .line 449
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/v/aw;->a()Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    .line 451
    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    if-lez v1, :cond_0

    .line 452
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/v/aw;->b()Ljava/nio/ShortBuffer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    .line 455
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    invoke-static {v5, v1, v4}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 456
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    aget v1, v1, v4

    invoke-static {v7, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 457
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 458
    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    const v3, 0x88e4

    invoke-static {v7, v1, v2, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 460
    invoke-static {v7, v4}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 461
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_1

    .line 466
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    invoke-static {v5, v1, v4}, Landroid/opengl/GLES20;->glGenBuffers(I[II)V

    .line 467
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v1, v1, v4

    invoke-static {v6, v1}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 468
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    invoke-virtual {v1, v4}, Ljava/nio/ShortBuffer;->position(I)Ljava/nio/Buffer;

    .line 469
    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    shl-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    const v3, 0x88e4

    invoke-static {v6, v1, v2, v3}, Landroid/opengl/GLES20;->glBufferData(IILjava/nio/Buffer;I)V

    .line 471
    invoke-static {v6, v4}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 472
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    if-eqz v1, :cond_2

    .line 477
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/v/av;->e:Ljava/nio/ByteBuffer;

    .line 478
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    .line 479
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    .line 481
    :cond_2
    sget-boolean v1, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v1, :cond_3

    .line 482
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    iget v2, p0, Lcom/google/android/apps/gmm/v/av;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/v/av;->f:I

    mul-int/2addr v2, v3

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/v/be;->c(I)V

    .line 483
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->t:Ljava/nio/ShortBuffer;

    if-eqz v1, :cond_3

    .line 484
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ad;->e:Lcom/google/android/apps/gmm/v/be;

    iget v2, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    shl-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/v/be;->d(I)V

    .line 506
    :cond_3
    :goto_0
    return v0

    .line 488
    :cond_4
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v1, :cond_5

    .line 489
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    invoke-static {v5, v1, v4}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 491
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    aput v2, v1, v4

    .line 492
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v1, v1, v4

    if-eq v1, v2, :cond_3

    .line 493
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v1, :cond_6

    .line 496
    invoke-static {v6, v4}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 497
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    invoke-static {v5, v1, v4}, Landroid/opengl/GLES20;->glDeleteBuffers(I[II)V

    .line 499
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aput v2, v1, v4

    goto :goto_0
.end method

.method public b(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/co;)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 636
    instance-of v0, p2, Lcom/google/android/apps/gmm/v/av;

    if-eqz v0, :cond_7

    move-object v0, p2

    check-cast v0, Lcom/google/android/apps/gmm/v/av;

    move-object v6, v0

    .line 639
    :goto_0
    if-eqz v6, :cond_8

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/v/av;->p:Z

    if-eqz v0, :cond_8

    move v5, v1

    .line 640
    :goto_1
    if-eqz v6, :cond_9

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/v/av;->o:Z

    if-eqz v0, :cond_9

    move v4, v1

    .line 641
    :goto_2
    if-eqz v6, :cond_a

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/v/av;->q:Z

    if-eqz v0, :cond_a

    move v3, v1

    .line 642
    :goto_3
    if-eqz v6, :cond_b

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/v/av;->r:Z

    if-eqz v0, :cond_b

    move v0, v1

    .line 644
    :goto_4
    if-nez p2, :cond_0

    .line 645
    invoke-static {v2}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 648
    :cond_0
    iget-boolean v7, p0, Lcom/google/android/apps/gmm/v/av;->p:Z

    if-eqz v7, :cond_1

    if-nez v5, :cond_1

    .line 649
    const/4 v5, 0x4

    invoke-static {v5}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 650
    :cond_1
    iget-boolean v5, p0, Lcom/google/android/apps/gmm/v/av;->o:Z

    if-eqz v5, :cond_2

    if-nez v4, :cond_2

    .line 655
    invoke-static {v1}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 656
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->q:Z

    if-eqz v1, :cond_3

    if-eqz v3, :cond_4

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/av;->r:Z

    if-eqz v1, :cond_5

    if-nez v0, :cond_5

    .line 661
    :cond_4
    const/4 v0, 0x2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisableVertexAttribArray(I)V

    .line 662
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v0, v0, v2

    if-eq v0, v8, :cond_6

    .line 668
    if-eqz v6, :cond_6

    iget-object v0, v6, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v0, v0, v2

    if-eq v0, v8, :cond_6

    .line 669
    const v0, 0x8893

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 672
    :cond_6
    return-void

    .line 636
    :cond_7
    const/4 v0, 0x0

    move-object v6, v0

    goto :goto_0

    :cond_8
    move v5, v2

    .line 639
    goto :goto_1

    :cond_9
    move v4, v2

    .line 640
    goto :goto_2

    :cond_a
    move v3, v2

    .line 641
    goto :goto_3

    :cond_b
    move v0, v2

    .line 642
    goto :goto_4
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    if-eqz v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->j:Lcom/google/android/apps/gmm/v/aw;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/v/aw;->c()Z

    move-result v0

    .line 413
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v0, v0, v3

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 622
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->c:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->a:I

    const/16 v2, 0x1403

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glDrawElements(IIII)V

    .line 628
    :goto_0
    return-void

    .line 624
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/v/av;->c:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/av;->b:I

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 746
    instance-of v1, p1, Lcom/google/android/apps/gmm/v/av;

    if-nez v1, :cond_1

    .line 751
    :cond_0
    :goto_0
    return v0

    .line 750
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/v/av;

    .line 751
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->u:[I

    aget v1, v1, v0

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/av;->u:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v1, v1, v0

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/av;->v:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
