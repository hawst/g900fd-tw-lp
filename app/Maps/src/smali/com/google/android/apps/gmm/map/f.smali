.class final Lcom/google/android/apps/gmm/map/f;
.super Lcom/google/android/apps/gmm/map/a;
.source "PG"


# instance fields
.field final synthetic b:Lcom/google/android/apps/gmm/map/b/a/r;

.field final synthetic c:I

.field final synthetic d:I

.field final synthetic e:I

.field final synthetic f:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/r;IIII)V
    .locals 0

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    iput p2, p0, Lcom/google/android/apps/gmm/map/f;->c:I

    iput p3, p0, Lcom/google/android/apps/gmm/map/f;->d:I

    iput p4, p0, Lcom/google/android/apps/gmm/map/f;->e:I

    iput p5, p0, Lcom/google/android/apps/gmm/map/f;->f:I

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b;)V
    .locals 9

    .prologue
    .line 325
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b;->a()I

    move-result v3

    .line 326
    invoke-interface {p1}, Lcom/google/android/apps/gmm/map/b;->b()I

    move-result v4

    .line 327
    if-eqz v3, :cond_0

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Map size should not be 0. Most likely, layout has not yet occured for the map view."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 329
    :cond_1
    iget v1, p0, Lcom/google/android/apps/gmm/map/f;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    iget v5, p0, Lcom/google/android/apps/gmm/map/f;->c:I

    iget v6, p0, Lcom/google/android/apps/gmm/map/f;->d:I

    iget v7, p0, Lcom/google/android/apps/gmm/map/f;->e:I

    iget v8, p0, Lcom/google/android/apps/gmm/map/f;->f:I

    move-object v0, p1

    invoke-static/range {v0 .. v8}, Lcom/google/android/apps/gmm/map/c;->a(Lcom/google/android/apps/gmm/map/b;ILcom/google/android/apps/gmm/map/b/a/r;IIIIII)V

    .line 331
    return-void
.end method
