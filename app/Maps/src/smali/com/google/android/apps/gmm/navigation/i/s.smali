.class public Lcom/google/android/apps/gmm/navigation/i/s;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/i/u;

.field final b:I

.field final c:Lcom/google/maps/g/a/bx;

.field d:Z

.field e:I

.field f:I

.field g:I

.field h:Ljava/lang/String;

.field final i:I


# direct methods
.method constructor <init>(Lcom/google/maps/g/a/bx;Lcom/google/android/apps/gmm/navigation/i/u;IIII)V
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/i/s;->a:Lcom/google/android/apps/gmm/navigation/i/u;

    .line 38
    iput p3, p0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/s;->c:Lcom/google/maps/g/a/bx;

    .line 40
    iput p4, p0, Lcom/google/android/apps/gmm/navigation/i/s;->e:I

    .line 41
    iput p5, p0, Lcom/google/android/apps/gmm/navigation/i/s;->f:I

    .line 42
    iput p6, p0, Lcom/google/android/apps/gmm/navigation/i/s;->g:I

    .line 43
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/i/s;->d:Z

    .line 45
    sget-object v1, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-eq p1, v1, :cond_0

    .line 46
    iget v1, p0, Lcom/google/android/apps/gmm/navigation/i/s;->b:I

    iget-object v2, p2, Lcom/google/android/apps/gmm/navigation/i/u;->a:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v1, v2, v1

    .line 47
    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/t;->c:[I

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    invoke-virtual {v3}, Lcom/google/maps/g/a/ez;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/i/s;->i:I

    .line 51
    :goto_1
    return-void

    .line 47
    :pswitch_0
    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/t;->b:[I

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->e:Lcom/google/maps/g/a/fd;

    invoke-virtual {v3}, Lcom/google/maps/g/a/fd;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/t;->a:[I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    invoke-virtual {v1}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_2

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xb

    goto :goto_0

    :pswitch_2
    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/t;->a:[I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    invoke-virtual {v1}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_4
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_5
    sget-object v2, Lcom/google/android/apps/gmm/navigation/i/t;->a:[I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    invoke-virtual {v1}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_4

    goto :goto_0

    :pswitch_6
    const/16 v0, 0xd

    goto :goto_0

    :pswitch_7
    const/16 v0, 0xe

    goto :goto_0

    :pswitch_8
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_9
    const/16 v0, 0x25

    goto :goto_0

    :pswitch_a
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/t;->a:[I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    invoke-virtual {v1}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_5

    const/16 v0, 0x13

    goto :goto_0

    :pswitch_b
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_c
    const/16 v0, 0x12

    goto :goto_0

    :pswitch_d
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/t;->a:[I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    invoke-virtual {v1}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_6

    const/16 v0, 0x16

    goto :goto_0

    :pswitch_e
    const/16 v0, 0x14

    goto :goto_0

    :pswitch_f
    const/16 v0, 0x15

    goto :goto_0

    :pswitch_10
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/t;->a:[I

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->d:Lcom/google/maps/g/a/fb;

    invoke-virtual {v1}, Lcom/google/maps/g/a/fb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_7

    const/16 v0, 0x19

    goto/16 :goto_0

    :pswitch_11
    const/16 v0, 0x17

    goto/16 :goto_0

    :pswitch_12
    const/16 v0, 0x18

    goto/16 :goto_0

    :pswitch_13
    const/16 v0, 0x22

    goto/16 :goto_0

    .line 49
    :cond_0
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/i/s;->i:I

    goto/16 :goto_1

    .line 47
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_d
        :pswitch_10
        :pswitch_13
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_1
        :pswitch_8
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x1
        :pswitch_e
        :pswitch_f
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x1
        :pswitch_11
        :pswitch_12
    .end packed-switch
.end method
