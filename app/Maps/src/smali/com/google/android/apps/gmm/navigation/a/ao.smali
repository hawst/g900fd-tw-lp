.class public Lcom/google/android/apps/gmm/navigation/a/ao;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/navigation/a/d;

.field public final b:Lcom/google/android/apps/gmm/map/util/b/g;

.field public c:Z

.field private final d:Lcom/google/android/apps/gmm/shared/b/a;

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/b/a;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->c:Z

    .line 50
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->e:Z

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 61
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 62
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->d:Lcom/google/android/apps/gmm/shared/b/a;

    .line 63
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/navigation/g/a/a;)V
    .locals 12
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    .line 116
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/am;

    iget v2, p1, Lcom/google/android/apps/gmm/navigation/g/a/a;->b:I

    .line 115
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x1

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/ax;->b(Lcom/google/android/apps/gmm/map/r/a/am;IZ)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->c:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v2, Lcom/google/android/apps/gmm/navigation/a/az;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/navigation/a/az;-><init>(Lcom/google/android/apps/gmm/map/r/a/am;)V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/a/az;->a()Lcom/google/android/apps/gmm/map/r/a/am;

    const/4 v0, 0x0

    :goto_0
    const/16 v4, 0xa

    if-ge v0, v4, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/a/az;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/navigation/a/az;->a()Lcom/google/android/apps/gmm/map/r/a/am;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->d:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->e:Lcom/google/maps/g/a/al;

    const v6, 0x7fffffff

    invoke-virtual {v1, v4, v6}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v7, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->d:Ljava/util/Set;

    invoke-interface {v7, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v7, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v6, v7, :cond_1

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    iget v7, v6, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    int-to-float v7, v7

    iget v8, v6, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    iget v8, v4, Lcom/google/android/apps/gmm/map/r/a/am;->d:I

    int-to-double v8, v8

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/r/a/ag;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v10

    div-double/2addr v8, v10

    iget v4, v4, Lcom/google/android/apps/gmm/map/r/a/am;->c:I

    int-to-float v4, v4

    mul-float/2addr v4, v7

    float-to-double v6, v4

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    double-to-int v4, v6

    iget-object v6, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->b:Lcom/google/android/apps/gmm/navigation/a/ae;

    invoke-virtual {v6, v4, v5}, Lcom/google/android/apps/gmm/navigation/a/ae;->a(ILcom/google/maps/g/a/al;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 117
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    monitor-enter v1

    :try_start_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/a/d;->l:Lcom/google/android/apps/gmm/navigation/a/av;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v1

    if-nez v2, :cond_3

    const-string v0, "true"

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x33

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "prefetchAlerts("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") tts generator is null: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/a/b/h;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/aa/d/s;->a:Lcom/google/android/apps/gmm/aa/d/s;

    invoke-interface {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/navigation/a/av;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/aw;Lcom/google/android/apps/gmm/aa/d/s;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    const-string v0, "false"

    goto :goto_1

    .line 118
    :cond_4
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/f;)V
    .locals 7
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 156
    .line 157
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    const-string v1, "currentRoute should be non-null after routing around a closure!"

    .line 156
    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    .line 160
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    .line 159
    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->mo:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v6}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 161
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v1, v0, v6, v5}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 162
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/h;)V
    .locals 10
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 71
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/g/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v2, v2, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v2, v3, v2

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 72
    if-eqz v2, :cond_2

    .line 73
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    .line 74
    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 73
    iput-object v4, v3, Lcom/google/android/apps/gmm/navigation/a/ax;->e:Lcom/google/maps/g/a/al;

    .line 75
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/r/a/w;->A:Ljava/util/List;

    if-eqz v4, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_3

    :cond_0
    iput-boolean v1, v3, Lcom/google/android/apps/gmm/navigation/a/d;->p:Z

    .line 76
    :goto_0
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v4, Lcom/google/maps/g/a/hm;->c:Lcom/google/maps/g/a/hm;

    if-ne v2, v4, :cond_1

    move v0, v1

    :cond_1
    iput-boolean v0, v3, Lcom/google/android/apps/gmm/navigation/a/d;->n:Z

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/a/d;->o:Z

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/d;->a()V

    .line 80
    :cond_2
    return-void

    .line 75
    :cond_3
    iget-object v5, v3, Lcom/google/android/apps/gmm/navigation/a/d;->c:Landroid/content/Context;

    invoke-static {v5}, Lcom/google/android/apps/gmm/aa/b/a;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    iput-boolean v6, v3, Lcom/google/android/apps/gmm/navigation/a/d;->p:Z

    const-string v6, "AlertController"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/navigation/a/d;->p:Z

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x40

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Update TTS whitelist: lang:"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " allowed-langs["

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] TtsWhitelisted:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v6, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/a/n;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/a;)V
    .locals 4
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    .line 123
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/j/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/am;

    iget v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/a;->b:I

    .line 122
    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(Lcom/google/android/apps/gmm/map/r/a/am;IZ)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 126
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/d;)V
    .locals 9
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 147
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/d;->b:Lcom/google/android/apps/gmm/navigation/j/b/b;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    .line 149
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/j/a/d;->b:Lcom/google/android/apps/gmm/navigation/j/b/b;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/j/b/b;->b:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/d;->b:Lcom/google/android/apps/gmm/navigation/j/b/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/j/b/b;->c:Ljava/lang/String;

    .line 148
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget-object v4, Lcom/google/android/apps/gmm/shared/c/c/m;->c:Lcom/google/android/apps/gmm/shared/c/c/m;

    invoke-static {v3, v1, v4}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v1

    invoke-static {v2}, Lcom/google/b/a/bo;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->mu:I

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v7}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 150
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v1, v0, v7, v6}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 152
    :cond_0
    return-void

    .line 148
    :cond_1
    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/l;->mv:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    aput-object v2, v5, v8

    invoke-virtual {v0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v7}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/p/b/e;)V
    .locals 5
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/b/e;->a:Z

    if-nez v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->a:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    sget-object v2, Lcom/google/android/apps/gmm/navigation/a/b/j;->d:Lcom/google/android/apps/gmm/navigation/a/b/j;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->dJ:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/gmm/navigation/a/b/f;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/navigation/a/b/f;-><init>(I)V

    invoke-static {v2, v1, v3}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 141
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/b/e;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/ao;->e:Z

    .line 142
    return-void
.end method
