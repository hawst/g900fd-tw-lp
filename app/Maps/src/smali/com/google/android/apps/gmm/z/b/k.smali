.class public Lcom/google/android/apps/gmm/z/b/k;
.super Lcom/google/android/apps/gmm/z/b/a;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 0
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 29
    invoke-direct {p0, p4}, Lcom/google/android/apps/gmm/z/b/a;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 30
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/b/k;->a:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/google/android/apps/gmm/z/b/k;->b:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/google/android/apps/gmm/z/b/k;->h:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/r/b/a/apf;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 37
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/z/b/a;->a(Lcom/google/r/b/a/apf;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/b/k;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 39
    invoke-static {}, Lcom/google/maps/g/hy;->newBuilder()Lcom/google/maps/g/ia;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/k;->h:Ljava/lang/String;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/ia;->a:I

    iput-object v1, v0, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {p1, v0}, Lcom/google/r/b/a/apf;->a(Lcom/google/maps/g/hy;)Lcom/google/r/b/a/apf;

    .line 42
    :cond_1
    invoke-static {}, Lcom/google/maps/g/up;->newBuilder()Lcom/google/maps/g/ur;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/b/k;->b:Ljava/lang/String;

    if-nez v1, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v2, v0, Lcom/google/maps/g/ur;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/ur;->a:I

    iput-object v1, v0, Lcom/google/maps/g/ur;->b:Ljava/lang/Object;

    .line 43
    invoke-static {}, Lcom/google/maps/g/us;->newBuilder()Lcom/google/maps/g/uu;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/b/k;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    iget v3, v1, Lcom/google/maps/g/uu;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v1, Lcom/google/maps/g/uu;->a:I

    iput-object v2, v1, Lcom/google/maps/g/uu;->b:Ljava/lang/Object;

    .line 42
    invoke-virtual {v0}, Lcom/google/maps/g/ur;->c()V

    iget-object v2, v0, Lcom/google/maps/g/ur;->c:Ljava/util/List;

    invoke-virtual {v1}, Lcom/google/maps/g/uu;->g()Lcom/google/n/t;

    move-result-object v1

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    iget-object v1, p1, Lcom/google/r/b/a/apf;->l:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/ur;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/r/b/a/apf;->a:I

    or-int/lit16 v0, v0, 0x1000

    iput v0, p1, Lcom/google/r/b/a/apf;->a:I

    .line 44
    return-void
.end method
