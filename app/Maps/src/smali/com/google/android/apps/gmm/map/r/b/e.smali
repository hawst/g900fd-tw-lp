.class public Lcom/google/android/apps/gmm/map/r/b/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Z

.field public b:Lcom/google/android/apps/gmm/map/internal/c/az;

.field public c:Lcom/google/android/apps/gmm/map/b/a/y;

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/b/b;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "Lcom/google/android/apps/gmm/map/r/a/ad;",
            ">;"
        }
    .end annotation
.end field

.field public f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/maps/g/a/hm;

.field public h:Z

.field public i:J

.field public j:Z

.field public k:Z

.field public l:Lcom/google/android/apps/gmm/map/b/a/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    .line 141
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    .line 185
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/r/b/e;)V
    .locals 2

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    .line 132
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    .line 141
    sget-object v0, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    .line 170
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    .line 171
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 172
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 173
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->d:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->d:Ljava/util/List;

    .line 174
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    .line 175
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 178
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->i:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->i:J

    .line 179
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    .line 180
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->k:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->k:Z

    .line 181
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/b/e;->l:Lcom/google/android/apps/gmm/map/b/a/ab;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/b/e;->l:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 182
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 189
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/r/b/e;

    if-nez v2, :cond_1

    .line 196
    :cond_0
    :goto_0
    return v0

    .line 192
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/r/b/e;

    .line 193
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 194
    if-eq v2, v3, :cond_2

    if-eqz v2, :cond_5

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/b/e;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 195
    if-eq v2, v3, :cond_3

    if-eqz v2, :cond_6

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_3
    move v2, v1

    :goto_2
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/b/e;->e:Ljava/util/Map;

    .line 196
    if-eq v2, v3, :cond_4

    if-eqz v2, :cond_7

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    :cond_4
    move v2, v1

    :goto_3
    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/b/e;->g:Lcom/google/maps/g/a/hm;

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    move v2, v0

    .line 194
    goto :goto_1

    :cond_6
    move v2, v0

    .line 195
    goto :goto_2

    :cond_7
    move v2, v0

    .line 196
    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 203
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "onRoad"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    .line 204
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "onRouteConfidence"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->f:Ljava/util/Map;

    .line 205
    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "isProjected"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->h:Z

    .line 206
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "timeToComputeSnapping"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->i:J

    .line 207
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "jumpedBackwardsAndSpun"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->k:Z

    .line 208
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "jumpedDisconnectedSegments"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->j:Z

    .line 209
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "mostLikelyFuturePath"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/b/e;->l:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 210
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 211
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
