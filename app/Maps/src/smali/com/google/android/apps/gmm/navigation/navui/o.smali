.class public Lcom/google/android/apps/gmm/navigation/navui/o;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/navui/h;


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field final a:Lcom/google/android/apps/gmm/x/a;

.field final b:Lcom/google/android/apps/gmm/navigation/navui/q;

.field final c:I

.field public d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

.field public e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

.field private final g:Lcom/google/android/apps/gmm/map/util/b/g;

.field private h:Z

.field private final i:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/navigation/navui/o;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/navui/o;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/navigation/navui/q;I)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/p;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/navui/p;-><init>(Lcom/google/android/apps/gmm/navigation/navui/o;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->i:Ljava/lang/Object;

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->g:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 69
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->b:Lcom/google/android/apps/gmm/navigation/navui/q;

    .line 70
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->a:Lcom/google/android/apps/gmm/x/a;

    .line 71
    iput p4, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->c:I

    .line 72
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 99
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->h:Z

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->g:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->i:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->b:Lcom/google/android/apps/gmm/navigation/navui/q;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/q;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->c:Z

    .line 115
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 84
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 86
    :goto_0
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b/d;

    if-eqz v1, :cond_1

    .line 87
    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/b/d;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/b/b;-><init>(Lcom/google/android/apps/gmm/navigation/navui/b/d;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    .line 91
    :goto_1
    return-void

    .line 84
    :cond_0
    const-string v0, "navigationFragmentState"

    .line 85
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    goto :goto_0

    .line 89
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/navui/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->b(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/navui/b/b;

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    iput-object p1, v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->g:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/o;->e()V

    .line 229
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/navui/b/c;)V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iput-object p1, v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->k:Lcom/google/android/apps/gmm/navigation/navui/b/c;

    .line 242
    return-void
.end method

.method public final a(Ljava/lang/Float;)V
    .locals 2
    .param p1    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->b(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/navui/b/b;

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    iput-object p1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->b:Ljava/lang/Float;

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/o;->e()V

    .line 215
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->g:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->i:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 123
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->h:Z

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/b;->c:Z

    .line 131
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->b:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->b(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/navui/b/b;

    .line 220
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/o;->e()V

    .line 221
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->b(Lcom/google/android/apps/gmm/navigation/c/a/a;)Lcom/google/android/apps/gmm/navigation/navui/b/b;

    .line 234
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/navui/b/b;->i:Z

    .line 235
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/navui/o;->e()V

    .line 236
    return-void
.end method

.method public e()V
    .locals 6

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->h:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 144
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->d:Lcom/google/android/apps/gmm/navigation/navui/b/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/navigation/navui/b/b;->c()Lcom/google/android/apps/gmm/navigation/navui/b/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    .line 145
    sget-object v1, Lcom/google/android/apps/gmm/navigation/navui/o;->f:Ljava/lang/String;

    const-string v2, "dispatchFragmentStateChange: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->b:Lcom/google/android/apps/gmm/navigation/navui/q;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/o;->e:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/navigation/navui/q;->a(Lcom/google/android/apps/gmm/navigation/navui/b/a;Lcom/google/android/apps/gmm/navigation/navui/b/a;)V

    .line 147
    return-void
.end method
