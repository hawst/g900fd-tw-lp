.class public Lcom/google/android/apps/gmm/place/bx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/g/a/e;
.implements Lcom/google/android/apps/gmm/search/bb;


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/libraries/curvular/ag;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ag",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/e;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lcom/google/android/apps/gmm/base/l/ad;

.field private f:Ljava/lang/Boolean;

.field private g:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/d;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lcom/google/android/apps/gmm/search/ba;

.field private l:[B

.field private m:I

.field private n:Lcom/google/android/apps/gmm/z/b/l;

.field private o:Lcom/google/android/apps/gmm/z/b/l;

.field private p:Lcom/google/android/apps/gmm/z/b/l;

.field private q:Lcom/google/android/apps/gmm/place/g/c;

.field private r:Lcom/google/android/apps/gmm/place/g/d;

.field private s:Lcom/google/android/apps/gmm/place/an;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/android/apps/gmm/place/bx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/bx;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->d:Ljava/util/List;

    .line 52
    new-instance v0, Lcom/google/android/apps/gmm/base/l/ad;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/l/ad;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->e:Lcom/google/android/apps/gmm/base/l/ad;

    .line 53
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->f:Ljava/lang/Boolean;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->j:Ljava/util/List;

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/place/g/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/g/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->q:Lcom/google/android/apps/gmm/place/g/c;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/place/g/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/g/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->r:Lcom/google/android/apps/gmm/place/g/d;

    .line 76
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->d:Ljava/util/List;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 81
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/bx;->c:Landroid/content/Context;

    .line 82
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    .line 83
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/bx;->s:Lcom/google/android/apps/gmm/place/an;

    .line 85
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/gmm/base/g/c;

    .line 86
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->t()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v8

    :goto_0
    if-nez v0, :cond_4

    move v0, v8

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 87
    iget-object v0, v7, Lcom/google/android/apps/gmm/base/g/c;->o:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->R()Ljava/util/List;

    iget-object v0, v7, Lcom/google/android/apps/gmm/base/g/c;->o:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v3}, Ljava/lang/Boolean;-><init>(Z)V

    iput-object v0, v7, Lcom/google/android/apps/gmm/base/g/c;->o:Ljava/lang/Boolean;

    :cond_1
    iget-object v0, v7, Lcom/google/android/apps/gmm/base/g/c;->o:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->f:Ljava/lang/Boolean;

    .line 89
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->C()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->i:Ljava/lang/Boolean;

    .line 90
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->D()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->h:Ljava/lang/Boolean;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 93
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->R()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_2
    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/pc;

    .line 94
    iget v0, v2, Lcom/google/maps/g/pc;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    move v0, v8

    :goto_3
    if-eqz v0, :cond_2

    .line 95
    new-instance v0, Lcom/google/android/apps/gmm/place/bw;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bw;-><init>()V

    .line 98
    const/4 v6, 0x0

    move-object v1, p1

    move v4, v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/bw;->a(Landroid/content/Context;Lcom/google/maps/g/pc;ZZLcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/g/a/f;)V

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_2

    :cond_3
    move v0, v3

    .line 86
    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    :cond_5
    move v0, v3

    .line 94
    goto :goto_3

    .line 105
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->j:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/place/bx;->m:I

    .line 108
    iget-object v0, v7, Lcom/google/android/apps/gmm/base/g/c;->p:[B

    if-nez v0, :cond_7

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->R()Ljava/util/List;

    :cond_7
    iget-object v0, v7, Lcom/google/android/apps/gmm/base/g/c;->p:[B

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->l:[B

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->e:Lcom/google/android/apps/gmm/base/l/ad;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/base/l/ad;->a(Lcom/google/android/apps/gmm/x/o;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->q:Lcom/google/android/apps/gmm/place/g/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/place/g/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->r:Lcom/google/android/apps/gmm/place/g/d;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/apps/gmm/place/g/d;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 116
    if-eqz v7, :cond_8

    iget-boolean v0, v7, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-eqz v0, :cond_8

    .line 118
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v1, v8, [Lcom/google/b/f/cq;

    sget-object v2, Lcom/google/b/f/t;->dv:Lcom/google/b/f/t;

    aput-object v2, v1, v3

    .line 119
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->n:Lcom/google/android/apps/gmm/z/b/l;

    .line 121
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v1, v8, [Lcom/google/b/f/cq;

    sget-object v2, Lcom/google/b/f/t;->dD:Lcom/google/b/f/t;

    aput-object v2, v1, v3

    .line 122
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->o:Lcom/google/android/apps/gmm/z/b/l;

    .line 125
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/base/g/c;->Z()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    new-array v1, v8, [Lcom/google/b/f/cq;

    sget-object v2, Lcom/google/b/f/t;->dm:Lcom/google/b/f/t;

    aput-object v2, v1, v3

    .line 126
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->p:Lcom/google/android/apps/gmm/z/b/l;

    .line 128
    :cond_8
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/place/review/a/a;)V
    .locals 4
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->q:Lcom/google/android/apps/gmm/place/g/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/bx;->s:Lcom/google/android/apps/gmm/place/an;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/g/c;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->r:Lcom/google/android/apps/gmm/place/g/d;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/bx;->s:Lcom/google/android/apps/gmm/place/an;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/place/g/d;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    .line 264
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->a:Lcom/google/android/libraries/curvular/ag;

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->a:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 267
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/search/ba;Ljava/util/List;Z[B)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/search/ba;",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/pc;",
            ">;Z[B)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 215
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/pc;

    .line 216
    new-instance v0, Lcom/google/android/apps/gmm/place/bw;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bw;-><init>()V

    .line 217
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/bw;->a(Landroid/content/Context;Lcom/google/maps/g/pc;ZZLcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/g/a/f;)V

    .line 219
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->j:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    goto :goto_0

    .line 222
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/search/ba;->a:I

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/place/bx;->m:I

    .line 223
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->f:Ljava/lang/Boolean;

    .line 224
    iput-object p4, p0, Lcom/google/android/apps/gmm/place/bx;->l:[B

    .line 225
    iput-object v6, p0, Lcom/google/android/apps/gmm/place/bx;->k:Lcom/google/android/apps/gmm/search/ba;

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->a:Lcom/google/android/libraries/curvular/ag;

    if-eqz v0, :cond_1

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->a:Lcom/google/android/libraries/curvular/ag;

    invoke-interface {v0, p0}, Lcom/google/android/libraries/curvular/ag;->a(Ljava/lang/Object;)V

    .line 230
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 3

    .prologue
    .line 234
    sget-object v0, Lcom/google/android/apps/gmm/place/bx;->b:Ljava/lang/String;

    const-string v0, "TactileReviewsDataRequest failed:%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 235
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->k:Lcom/google/android/apps/gmm/search/ba;

    .line 236
    return-void
.end method

.method public final a(Ljava/lang/Class;Lcom/google/android/libraries/curvular/ce;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<+",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;",
            "Lcom/google/android/libraries/curvular/ce;",
            ")V"
        }
    .end annotation

    .prologue
    .line 196
    const-class v0, Lcom/google/android/apps/gmm/place/c/ab;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->k:Lcom/google/android/apps/gmm/search/ba;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->l:[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->l:[B

    array-length v0, v0

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 199
    new-instance v0, Lcom/google/android/apps/gmm/search/ba;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/place/bx;->m:I

    const/16 v3, 0xa

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/bx;->l:[B

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/search/ba;-><init>(Lcom/google/android/apps/gmm/map/b/a/j;IILcom/google/android/apps/gmm/search/bb;[B)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->k:Lcom/google/android/apps/gmm/search/ba;

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    .line 202
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->k:Lcom/google/android/apps/gmm/search/ba;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 205
    :cond_1
    return-void

    .line 198
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 6

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 142
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v2

    .line 143
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v3, Lcom/google/b/f/t;->dm:Lcom/google/b/f/t;

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 145
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->r()Lcom/google/android/apps/gmm/iamhere/a/b;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    sget-object v4, Lcom/google/j/d/a/w;->h:Lcom/google/j/d/a/w;

    sget-object v5, Lcom/google/b/f/t;->dm:Lcom/google/b/f/t;

    invoke-interface {v3, v0, v4, v5}, Lcom/google/android/apps/gmm/iamhere/a/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/j/d/a/w;Lcom/google/b/f/t;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    .line 149
    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;->a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/place/PlacePageUserReviewsFragment;

    move-result-object v0

    .line 148
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 150
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->n:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->o:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->p:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/v;
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->e:Lcom/google/android/apps/gmm/base/l/ad;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/place/g/a/c;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->q:Lcom/google/android/apps/gmm/place/g/c;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/place/g/a/f;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->r:Lcom/google/android/apps/gmm/place/g/d;

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/g/a/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->j:Ljava/util/List;

    return-object v0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v4

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bx;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 247
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bx;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move v1, v3

    :goto_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    .line 248
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/shared/net/a/h;->c:Z

    if-eqz v1, :cond_3

    .line 249
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget v0, v0, Lcom/google/r/b/a/ads;->a:I

    and-int/lit8 v0, v0, 0x40

    const/16 v1, 0x40

    if-ne v0, v1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 247
    :goto_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    move v1, v2

    goto :goto_0

    :cond_2
    move v0, v2

    .line 249
    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_2
.end method
