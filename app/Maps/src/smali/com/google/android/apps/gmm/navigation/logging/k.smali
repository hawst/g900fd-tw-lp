.class public Lcom/google/android/apps/gmm/navigation/logging/k;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/map/r/a/w;

.field b:Lcom/google/maps/g/a/hm;

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:Z

.field j:I

.field k:I

.field l:I

.field m:I

.field n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/a/b;",
            ">;"
        }
    .end annotation
.end field

.field o:D

.field p:Lcom/google/android/apps/gmm/map/b/a/y;

.field q:Lcom/google/android/apps/gmm/map/r/b/a;

.field r:I

.field s:Lcom/google/b/f/b/a/cu;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->j:I

    .line 39
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->k:I

    .line 40
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->l:I

    .line 41
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->m:I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->n:Ljava/util/List;

    return-void
.end method


# virtual methods
.method final a()I
    .locals 6

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->q:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->p:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/r/b/a;->a(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v0

    .line 197
    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    iget-wide v4, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->o:D

    float-to-double v0, v0

    sub-double v0, v4, v0

    mul-double/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->o:D

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final a(Lcom/google/b/f/b/a/bk;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v0, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->b:Lcom/google/maps/g/a/hm;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iget v0, v0, Lcom/google/maps/g/a/hm;->h:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->c:I

    .line 150
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->c:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->j:I

    .line 151
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->d:I

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->e:I

    sub-int/2addr v0, v1

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->d:I

    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->i:Z

    if-eqz v0, :cond_3

    .line 154
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->d:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->e:I

    .line 156
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->f:I

    if-lez v0, :cond_4

    .line 157
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->f:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->f:I

    .line 160
    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->g:I

    if-lez v0, :cond_5

    .line 161
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->g:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->g:I

    .line 164
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->h:I

    if-lez v0, :cond_6

    .line 165
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->h:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v1, v1, 0x20

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->h:I

    .line 170
    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->j:I

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->l:I

    sub-int/2addr v0, v1

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v1, v1, 0x400

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->m:I

    .line 171
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->k:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v1, v1, 0x800

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->n:I

    .line 172
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->l:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->o:I

    .line 173
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->m:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v1, v1, 0x2000

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->p:I

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/a/b;

    .line 177
    invoke-static {}, Lcom/google/b/f/b/a/bn;->newBuilder()Lcom/google/b/f/b/a/bp;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/a/b;->d:Lcom/google/b/f/b/a/bq;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v3, v2, Lcom/google/b/f/b/a/bp;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/b/f/b/a/bp;->a:I

    iget v0, v0, Lcom/google/b/f/b/a/bq;->d:I

    iput v0, v2, Lcom/google/b/f/b/a/bp;->b:I

    .line 176
    invoke-virtual {p1}, Lcom/google/b/f/b/a/bk;->c()V

    iget-object v0, p1, Lcom/google/b/f/b/a/bk;->q:Ljava/util/List;

    invoke-virtual {v2}, Lcom/google/b/f/b/a/bp;->g()Lcom/google/n/t;

    move-result-object v2

    new-instance v3, Lcom/google/n/ao;

    invoke-direct {v3}, Lcom/google/n/ao;-><init>()V

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v3, Lcom/google/n/ao;->d:Z

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 180
    :cond_8
    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->o:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->q:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_9

    .line 181
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/logging/k;->a()I

    move-result v0

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    const/high16 v2, 0x20000

    or-int/2addr v1, v2

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->t:I

    .line 184
    :cond_9
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->r:I

    if-eqz v0, :cond_a

    .line 185
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->r:I

    iget v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p1, Lcom/google/b/f/b/a/bk;->a:I

    iput v0, p1, Lcom/google/b/f/b/a/bk;->k:I

    .line 189
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->s:Lcom/google/b/f/b/a/cu;

    if-eqz v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->s:Lcom/google/b/f/b/a/cu;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    iget-object v1, p1, Lcom/google/b/f/b/a/bk;->i:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v5, v1, Lcom/google/n/ao;->d:Z

    iget v0, p1, Lcom/google/b/f/b/a/bk;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, p1, Lcom/google/b/f/b/a/bk;->a:I

    goto/16 :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    if-nez v0, :cond_0

    .line 211
    const-string v0, "RouteStats{}"

    .line 253
    :goto_0
    return-object v0

    .line 214
    :cond_0
    new-instance v4, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    .line 215
    const-string v0, "TRAVEL_MODE"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->b:Lcom/google/maps/g/a/hm;

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 216
    const-string v0, "BASE_ESTIMATE_SECONDS"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->d:I

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->e:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 217
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->i:Z

    if-eqz v0, :cond_4

    .line 218
    const-string v0, "TRAFFIC_ESTIMATE_SECONDS"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->d:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 220
    :cond_4
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->f:I

    if-lez v0, :cond_6

    .line 221
    const-string v0, "OPTIMISTIC_TRAFFIC_ESTIMATE_SECONDS"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->f:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 223
    :cond_6
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->g:I

    if-lez v0, :cond_8

    .line 224
    const-string v0, "PESSIMISTIC_TRAFFIC_ESTIMATE_SECONDS"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->g:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 226
    :cond_8
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->h:I

    if-lez v0, :cond_a

    .line 227
    const-string v0, "HISTORICAL_TRAFFIC_ESTIMATE_SECONDS"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->h:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_9
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 229
    :cond_a
    const-string v0, "LENGTH_METERS"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->c:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_b
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 230
    const-string v0, "OFF_ROUTE_REROUTES"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->j:I

    iget v2, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->l:I

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_c
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 231
    const-string v0, "DYNAMIC_REROUTES_FOUND"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->k:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 232
    const-string v0, "DYNAMIC_REROUTES"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->l:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_e
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 233
    const-string v0, "ROAD_CLOSURE_REROUTES_FOUND"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->m:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 235
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_12

    .line 236
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->n:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/a/b;

    .line 237
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, "OFFLINE_REROUTING_STATS"

    new-instance v5, Lcom/google/b/a/ak;

    invoke-direct {v5, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "STATUS"

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/a/b;->d:Lcom/google/b/f/b/a/bq;

    iget v2, v2, Lcom/google/b/f/b/a/bq;->d:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    new-instance v6, Lcom/google/b/a/al;

    invoke-direct {v6}, Lcom/google/b/a/al;-><init>()V

    iget-object v7, v5, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v6, v7, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v6, v5, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v6, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_10
    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/b/a/al;->a:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v5, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v5, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v0, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v1, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 235
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 240
    :cond_12
    iget-wide v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->o:D

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-lez v0, :cond_14

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->q:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_14

    .line 241
    const-string v0, "PROGRESS_PERCENTAGE"

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/navigation/logging/k;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 244
    :cond_14
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->r:I

    if-eqz v0, :cond_16

    .line 245
    const-string v0, "TRAFFIC_ROUTING_EXPERIMENT_BACKEND_CLASS"

    iget v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->r:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 249
    :cond_16
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->s:Lcom/google/b/f/b/a/cu;

    if-eqz v0, :cond_18

    .line 250
    const-string v0, "ROAD_TRAFFIC_EXPERIMENTAL_DATA"

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/logging/k;->s:Lcom/google/b/f/b/a/cu;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/b/a/al;

    invoke-direct {v2}, Lcom/google/b/a/al;-><init>()V

    iget-object v3, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v2, v4, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v1, v2, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 253
    :cond_18
    invoke-virtual {v4}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method
