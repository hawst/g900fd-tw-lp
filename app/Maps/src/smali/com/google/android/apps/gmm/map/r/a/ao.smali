.class public Lcom/google/android/apps/gmm/map/r/a/ao;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/maps/g/a/hu;

.field public final b:Lcom/google/android/apps/gmm/map/r/a/e;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/a/hu;Lcom/google/android/apps/gmm/map/r/a/e;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    .line 30
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->b:Lcom/google/android/apps/gmm/map/r/a/e;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/r/a/a;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 113
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v1, v1, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v2, 0x20

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_2

    .line 116
    :cond_0
    :goto_1
    return-object v0

    .line 113
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 116
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->b:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v2, v2, Lcom/google/maps/g/a/hu;->i:I

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/e;->d:[Lcom/google/android/apps/gmm/map/r/a/a;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->d:[Lcom/google/android/apps/gmm/map/r/a/a;

    aget-object v0, v0, v2

    if-nez v0, :cond_3

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/e;->d:[Lcom/google/android/apps/gmm/map/r/a/a;

    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/a;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v0, v0, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aex;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/r/a/a;-><init>(Lcom/google/r/b/a/aex;)V

    aput-object v4, v3, v2

    :cond_3
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/e;->d:[Lcom/google/android/apps/gmm/map/r/a/a;

    aget-object v0, v0, v2

    goto :goto_1
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 128
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v2, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 3

    .prologue
    .line 136
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v2, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/maps/g/a/hu;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
