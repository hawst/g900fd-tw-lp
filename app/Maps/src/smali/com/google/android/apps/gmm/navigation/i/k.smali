.class public Lcom/google/android/apps/gmm/navigation/i/k;
.super Lcom/google/android/apps/gmm/navigation/i/i;
.source "PG"


# instance fields
.field c:Lcom/google/android/apps/gmm/navigation/i/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/google/android/apps/gmm/navigation/i/l;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;",
            "Lcom/google/android/apps/gmm/navigation/i/l;",
            ")V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/navigation/i/i;-><init>(Landroid/content/Context;Ljava/util/List;)V

    .line 54
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/k;->c:Lcom/google/android/apps/gmm/navigation/i/l;

    .line 55
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/maps/g/a/ez;
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcom/google/maps/g/a/ez;->b:Lcom/google/maps/g/a/ez;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 59
    sget-object v1, Lcom/google/android/apps/gmm/navigation/i/j;->a:[I

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/k;->c:Lcom/google/android/apps/gmm/navigation/i/l;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/i/l;->ordinal()I

    move-result v3

    aget v1, v1, v3

    packed-switch v1, :pswitch_data_0

    move v1, v2

    :goto_0
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/k;->a:Landroid/content/Context;

    invoke-virtual {v3, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 60
    :goto_1
    if-eqz v1, :cond_0

    .line 61
    sget-object v0, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/navigation/i/k;->a(Lcom/google/maps/g/a/af;)Lcom/google/android/apps/gmm/map/r/a/ai;

    move-result-object v0

    .line 62
    if-eqz v0, :cond_2

    .line 63
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/k;->a:Landroid/content/Context;

    sget v4, Lcom/google/android/apps/gmm/l;->eb:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ai;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 68
    :cond_0
    :goto_2
    return-object v0

    .line 59
    :pswitch_0
    sget v1, Lcom/google/android/apps/gmm/l;->cw:I

    goto :goto_0

    :pswitch_1
    sget v1, Lcom/google/android/apps/gmm/l;->cy:I

    goto :goto_0

    :pswitch_2
    sget v1, Lcom/google/android/apps/gmm/l;->cC:I

    goto :goto_0

    :pswitch_3
    sget v1, Lcom/google/android/apps/gmm/l;->cB:I

    goto :goto_0

    :pswitch_4
    sget v1, Lcom/google/android/apps/gmm/l;->cz:I

    goto :goto_0

    :pswitch_5
    sget v1, Lcom/google/android/apps/gmm/l;->cA:I

    goto :goto_0

    :pswitch_6
    sget v1, Lcom/google/android/apps/gmm/l;->cv:I

    goto :goto_0

    :pswitch_7
    sget v1, Lcom/google/android/apps/gmm/l;->cx:I

    goto :goto_0

    :cond_1
    move-object v1, v0

    goto :goto_1

    .line 65
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/k;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->ea:I

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v2

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
