.class public Lcom/google/android/apps/gmm/map/o/bi;
.super Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/b/a/y;

.field private final b:Lcom/google/android/apps/gmm/map/o/b/a;

.field private c:Lcom/google/android/apps/gmm/map/o/c/g;

.field private d:Lcom/google/android/apps/gmm/v/by;

.field private e:Lcom/google/android/apps/gmm/map/g/b;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;-><init>()V

    .line 37
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/map/o/b/a;

    invoke-direct {v0, v1, v1, v1, v1}, Lcom/google/android/apps/gmm/map/o/b/a;-><init>(FFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    .line 57
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/ap;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 102
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->u:Lcom/google/android/apps/gmm/map/o/ap;

    if-ne v1, p1, :cond_0

    .line 120
    :goto_0
    return v0

    .line 105
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->c:Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/o/c/g;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 106
    const/4 v0, 0x0

    goto :goto_0

    .line 109
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 111
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    if-eqz v1, :cond_3

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 113
    :cond_2
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 119
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/bi;->u:Lcom/google/android/apps/gmm/map/o/ap;

    goto :goto_0

    .line 116
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Z)Z
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    .line 125
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    .line 126
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bi;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p2, v2}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    .line 127
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/bi;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p2, v3, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v3

    if-nez v3, :cond_0

    .line 138
    :goto_0
    return v0

    .line 130
    :cond_0
    aget v0, v1, v0

    .line 131
    aget v1, v1, v9

    .line 132
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/o/bi;->c:Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/o/c/g;->a()F

    move-result v3

    mul-float/2addr v3, v8

    mul-float/2addr v3, v2

    .line 133
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/bi;->c:Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/o/c/g;->b()F

    move-result v4

    mul-float/2addr v4, v8

    mul-float/2addr v2, v4

    .line 134
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/o/bi;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    sub-float v5, v0, v3

    sub-float v6, v1, v2

    add-float/2addr v0, v3

    add-float/2addr v1, v2

    invoke-virtual {v4, v5, v6, v0, v1}, Lcom/google/android/apps/gmm/map/o/b/a;->a(FFFF)V

    .line 135
    if-eqz p3, :cond_1

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    iget-object v10, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    monitor-enter v10

    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/o/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/o/b/a;->e:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    const-wide/16 v4, 0x0

    iget v6, v0, Lcom/google/android/apps/gmm/map/o/b/a;->c:F

    iget v7, v0, Lcom/google/android/apps/gmm/map/o/b/a;->a:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v8

    iget v7, v0, Lcom/google/android/apps/gmm/map/o/b/a;->d:F

    iget v0, v0, Lcom/google/android/apps/gmm/map/o/b/a;->b:F

    sub-float v0, v7, v0

    mul-float v7, v0, v8

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lcom/google/android/apps/gmm/map/o/b/i;->a(FFDFFZ)V

    monitor-exit v10

    :cond_1
    move v0, v9

    .line 138
    goto :goto_0

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method final a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/o/c/g;Lcom/google/android/apps/gmm/map/o/ak;FLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/g/b;Ljava/lang/Integer;)Lcom/google/android/apps/gmm/map/o/bi;
    .locals 10
    .param p7    # Lcom/google/android/apps/gmm/map/g/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 75
    const/4 v2, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    sget-object v8, Lcom/google/android/apps/gmm/map/o/af;->a:Lcom/google/android/apps/gmm/map/o/af;

    move-object v1, p0

    move v3, p1

    move-object v4, p4

    move v6, p5

    move-object/from16 v9, p6

    invoke-super/range {v1 .. v9}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->a(Lcom/google/android/apps/gmm/map/internal/c/m;ILcom/google/android/apps/gmm/map/o/ak;Lcom/google/android/apps/gmm/map/internal/c/be;FILcom/google/android/apps/gmm/map/o/af;Lcom/google/android/apps/gmm/map/t/l;)V

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, p2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v2, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 78
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/o/bi;->c:Lcom/google/android/apps/gmm/map/o/c/g;

    .line 79
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->e:Lcom/google/android/apps/gmm/map/g/b;

    .line 80
    if-eqz p8, :cond_0

    .line 81
    invoke-virtual/range {p8 .. p8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    iput v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->f:I

    .line 82
    return-object p0

    .line 81
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/o/c/g;->a()F

    move-result v1

    float-to-int v1, v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 4

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->e:Lcom/google/android/apps/gmm/map/g/b;

    if-eqz v0, :cond_0

    .line 210
    new-instance v0, Lcom/google/android/apps/gmm/map/j/t;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->e:Lcom/google/android/apps/gmm/map/g/b;

    const-string v2, ""

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/map/j/t;-><init>(Lcom/google/android/apps/gmm/map/g/b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2, v0}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 212
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/b;)V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->t:Lcom/google/android/apps/gmm/map/t/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p1, :cond_0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 162
    :goto_0
    return-void

    .line 154
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/bi;->t:Lcom/google/android/apps/gmm/map/t/b;

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->c:Lcom/google/android/apps/gmm/map/o/c/g;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/o/c/g;->a(Lcom/google/android/apps/gmm/map/t/b;)Lcom/google/android/apps/gmm/v/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/o/bc;Z)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x1

    .line 168
    iget-object v1, p2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/o/bi;->n:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_1

    move v0, v8

    .line 193
    :cond_0
    :goto_0
    return v0

    .line 173
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/o/au;->f:[F

    .line 174
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/o/bi;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p2, v2, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v8

    .line 175
    goto :goto_0

    .line 177
    :cond_2
    aget v2, v1, v0

    .line 178
    aget v3, v1, v8

    .line 180
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {p2, v1}, Lcom/google/android/apps/gmm/map/f/o;->b(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v4

    .line 181
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 186
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    iget v5, p0, Lcom/google/android/apps/gmm/map/o/bi;->v:F

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/o/bi;->q:Lcom/google/android/apps/gmm/map/t/l;

    move-object v0, p3

    move-object v7, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/v/by;FFFFLcom/google/android/apps/gmm/map/t/l;Lcom/google/android/apps/gmm/map/f/o;)V

    .line 187
    if-eqz p4, :cond_3

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->r:Lcom/google/android/apps/gmm/map/o/b/i;

    sget-object v2, Lcom/google/android/apps/gmm/map/t/l;->D:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {p3, v0, v2}, Lcom/google/android/apps/gmm/map/o/bc;->a(Lcom/google/android/apps/gmm/map/o/b/b;Lcom/google/android/apps/gmm/map/t/l;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    move v0, v8

    .line 193
    goto :goto_0

    .line 188
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 191
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/o/ap;Lcom/google/android/apps/gmm/map/f/o;Z)Z
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/map/o/bi;->a(Lcom/google/android/apps/gmm/map/o/ap;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/apps/gmm/map/o/bi;->a(Lcom/google/android/apps/gmm/map/o/au;Lcom/google/android/apps/gmm/map/f/o;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/t/ac;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->e:Lcom/google/android/apps/gmm/map/g/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p0, Lcom/google/android/apps/gmm/map/o/bi;->f:I

    int-to-float v1, v1

    .line 204
    invoke-virtual {p1, v0, v1, v2, v2}, Lcom/google/android/apps/gmm/map/t/ac;->a(Lcom/google/android/apps/gmm/map/b/a/y;FFF)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/apps/gmm/map/o/b/b;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->b:Lcom/google/android/apps/gmm/map/o/b/a;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->e:Lcom/google/android/apps/gmm/map/g/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final k()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 87
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/o/bi;->c:Lcom/google/android/apps/gmm/map/o/c/g;

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    if-eqz v0, :cond_1

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/by;->k:Lcom/google/android/apps/gmm/v/bw;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/bw;->a:Lcom/google/android/apps/gmm/v/bz;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/v/bz;->a(Lcom/google/android/apps/gmm/v/by;)V

    .line 90
    :cond_0
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/o/bi;->d:Lcom/google/android/apps/gmm/v/by;

    .line 92
    :cond_1
    iput-object v2, p0, Lcom/google/android/apps/gmm/map/o/bi;->e:Lcom/google/android/apps/gmm/map/g/b;

    .line 93
    invoke-super {p0}, Lcom/google/android/apps/gmm/map/legacy/a/c/b/p;->k()V

    .line 94
    return-void
.end method
