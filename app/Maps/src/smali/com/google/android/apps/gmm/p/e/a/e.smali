.class public Lcom/google/android/apps/gmm/p/e/a/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/p/e/a/g;

.field public final b:F

.field public final c:F

.field public final d:D


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;)V
    .locals 2

    .prologue
    .line 435
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/apps/gmm/p/e/a/e;-><init>(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;FF)V

    .line 436
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/p/e/a/g;Lcom/google/android/apps/gmm/p/e/a/g;FF)V
    .locals 4

    .prologue
    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    iput-object p2, p0, Lcom/google/android/apps/gmm/p/e/a/e;->a:Lcom/google/android/apps/gmm/p/e/a/g;

    .line 425
    iput p3, p0, Lcom/google/android/apps/gmm/p/e/a/e;->b:F

    .line 426
    iput p4, p0, Lcom/google/android/apps/gmm/p/e/a/e;->c:F

    .line 427
    iget v0, p1, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    iget v1, p2, Lcom/google/android/apps/gmm/p/e/a/g;->e:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/high16 v1, 0x43b40000    # 360.0f

    sub-float v0, v1, v0

    :cond_0
    float-to-double v0, v0

    .line 428
    const-wide v2, 0x3fb015bf9217271aL    # 0.06283185307179587

    mul-double/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/apps/gmm/p/e/a/e;->d:D

    .line 429
    return-void
.end method
