.class Lcom/google/android/apps/gmm/navigation/commonui/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/navigation/commonui/c/d;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

.field private final b:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 41
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->dismiss()V

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->z()Lcom/google/android/apps/gmm/navigation/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/f;->X_()Z

    .line 49
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 53
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->dismiss()V

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-eqz v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->H()Lcom/google/android/apps/gmm/settings/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/settings/a/a;->i()V

    .line 58
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/commonui/b;->a:Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/commonui/CommonNavigationMenuFragment;->dismiss()V

    .line 65
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method
