.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/j;
.super Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;
.source "PG"


# static fields
.field private static final o:Ljava/lang/String;

.field private static final p:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 18

    .prologue
    .line 950
    sget-boolean v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-eqz v1, :cond_0

    const-string v1, "#define DASHES_IN_STYLE_TEXTURE\n"

    :goto_0
    sget-object v2, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    aget v2, v2, v3

    sget-object v3, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const/4 v4, 0x1

    aget v3, v3, v4

    sget-object v4, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const/4 v5, 0x0

    aget v4, v4, v5

    sget-object v5, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v6, 0x1

    aget-object v5, v5, v6

    const/4 v6, 0x1

    aget v5, v5, v6

    sget-object v6, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v7, 0x2

    aget-object v6, v6, v7

    const/4 v7, 0x0

    aget v6, v6, v7

    sget-object v7, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v8, 0x2

    aget-object v7, v7, v8

    const/4 v8, 0x1

    aget v7, v7, v8

    sget-object v8, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v9, 0x3

    aget-object v8, v8, v9

    const/4 v9, 0x0

    aget v8, v8, v9

    sget-object v9, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v10, 0x3

    aget-object v9, v9, v10

    const/4 v10, 0x1

    aget v9, v9, v10

    sget-object v10, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v11, 0x4

    aget-object v10, v10, v11

    const/4 v11, 0x0

    aget v10, v10, v11

    sget-object v11, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v12, 0x4

    aget-object v11, v11, v12

    const/4 v12, 0x1

    aget v11, v11, v12

    sget-object v12, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v13, 0x5

    aget-object v12, v12, v13

    const/4 v13, 0x0

    aget v12, v12, v13

    sget-object v13, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v14, 0x5

    aget-object v13, v13, v14

    const/4 v14, 0x1

    aget v13, v13, v14

    sget-object v14, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/4 v15, 0x6

    aget-object v14, v14, v15

    const/4 v15, 0x0

    aget v14, v14, v15

    sget-object v15, Lcom/google/android/apps/gmm/map/internal/vector/gl/GeometryUtil;->l:[[F

    const/16 v16, 0x6

    aget-object v15, v15, v16

    const/16 v16, 0x1

    aget v15, v15, v16

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0x613

    move/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Ljava/lang/StringBuilder;-><init>(I)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "uniform float uRoadAlpha;\nvarying float vRoadAlpha;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "attribute vec4 userData0;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "attribute float userData1;\n#endif\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "varying float strokeIndex;\nvarying float styleIndex;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "varying vec2 vTextureCoord;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "varying float vDistanceAlongRoad;\n#endif\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "void main() {\n  float texCoordIndex = aPosition.z;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "  gl_Position = uMVPMatrix * vec4(aPosition.xy, 0.0, 1.0);\n  styleIndex = (256.0 * userData0.x) + userData0.y;\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "  strokeIndex = userData0.z;\n  if (strokeIndex < 0.5 && uRoadAlpha < 1.0)\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "    vRoadAlpha = 0.0;\n  else\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "    vRoadAlpha = uRoadAlpha;\n  if (texCoordIndex < 1.0) {\n"

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v16, "    vTextureCoord.x = "

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    vTextureCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 2.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    vTextureCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    vTextureCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 3.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    vTextureCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    vTextureCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 4.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    vTextureCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    vTextureCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 5.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    vTextureCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    vTextureCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 6.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    vTextureCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    vTextureCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  } else if (texCoordIndex < 7.0) {\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "    vTextureCoord.x = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n    vTextureCoord.y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";\n  }\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vTextureCoord -= vec2(0.5);\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vDistanceAlongRoad = userData1;\n#endif\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/j;->o:Ljava/lang/String;

    .line 1003
    const-string v1, "precision mediump float;\n"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-boolean v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-eqz v1, :cond_1

    const-string v1, "#define DASHES_IN_STYLE_TEXTURE\n"

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit16 v4, v4, 0xb22

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#define ROAD_EXTRUSION_SCALE 0.5"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nuniform vec3 uTextureInfo;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "varying float styleIndex;\nuniform vec3 uStrokeInfo;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "varying float strokeIndex;\nuniform float reciprocalBitmapWidth;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "varying vec2 vTextureCoord;\nuniform sampler2D sTexture0;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "uniform sampler2D sStyleTexture2;\nuniform vec4 zoomStyleOffsets;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "varying float vRoadAlpha;\nuniform float brightnessScale;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "uniform float minimumTextureMinificationScale;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "uniform float uDashZoomScale;\nvarying float vDistanceAlongRoad;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "float convertBase255Vec4ToFloat(vec4 inVec4) {;\n  float result = inVec4[3];\n  result /= 255.0;\n  result += inVec4[2];\n  result /= 255.0;\n  result += inVec4[1];\n  result /= 255.0;\n  result += inVec4[0];\n  return result;\n}\n#endif\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "void main() {\n  float texHeight = uTextureInfo[0];\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float widthPixel = 4.0;\n  float colorTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float widthTexX = (widthPixel + 0.5) * reciprocalBitmapWidth;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float dashScaleTexX = (strokeIndex + 0.5) * reciprocalBitmapWidth;\n  float row = styleIndex * 5.0;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#else\n  float row = styleIndex;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#endif\n  float texY = (row + 0.5) / texHeight;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vec4 roadWidthVec1 =\n      texture2D(sStyleTexture2, vec2(zoomStyleOffsets.y + widthTexX, texY));\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vec4 roadWidthVec2 =\n      texture2D(sStyleTexture2, vec2(zoomStyleOffsets.z + widthTexX, texY));\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vec4 roadWidthVec =\n      (1.0 - zoomStyleOffsets.x) * roadWidthVec1 + zoomStyleOffsets.x * roadWidthVec2;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#if defined DASHES_IN_STYLE_TEXTURE\n  vec4 dashScaleVec = texture2D(sStyleTexture2, vec2(dashScaleTexX, texY));\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float dashScale = convertBase255Vec4ToFloat(dashScaleVec);\n  dashScale *= uDashZoomScale;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vec2 dashTextureCoord = vec2(vDistanceAlongRoad * dashScale,\n      (row + strokeIndex + 1.5) / texHeight);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "#endif\n  float roadScale;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  if (strokeIndex < 0.5)\n    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidthVec[0]);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  else if (strokeIndex < 1.5)\n    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidthVec[1]);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  else if (strokeIndex < 2.5)\n    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidthVec[2]);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  else if (strokeIndex < 3.5)\n    roadScale = 1.0 / max(minimumTextureMinificationScale, roadWidthVec[3]);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  else\n    roadScale = 0.0;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  vec2 texCoordTmp = (vTextureCoord * roadScale * ROAD_EXTRUSION_SCALE) + vec2(0.5);\n  vec4 vColor1 = texture2D(sStyleTexture2, vec2(zoomStyleOffsets.y"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " + colorTexX, texY));\n  vec4 vColor2 = texture2D(sStyleTexture2, vec2(zoomStyleOffsets.z"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " + colorTexX, texY));\n  vec4 vColor = (1.0 - zoomStyleOffsets.x) * vColor1 + zoomStyleOffsets.x * vColor2;\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  float t = texture2D(sTexture0, texCoordTmp, -0.25"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ").a;\n#if defined DASHES_IN_STYLE_TEXTURE\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  t *= texture2D(sStyleTexture2, dashTextureCoord).a;\n#endif\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  t *= vRoadAlpha * vColor.a;\n  gl_FragColor = vec4(brightnessScale * vColor.rgb, t);\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/j;->p:Ljava/lang/String;

    return-void

    .line 950
    :cond_0
    const-string v1, ""

    goto/16 :goto_0

    .line 1003
    :cond_1
    const-string v1, ""

    goto/16 :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1087
    sget-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/j;->o:Ljava/lang/String;

    sget-object v1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/j;->p:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1088
    return-void
.end method
