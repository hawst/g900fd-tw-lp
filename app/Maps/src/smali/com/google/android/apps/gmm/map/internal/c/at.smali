.class Lcom/google/android/apps/gmm/map/internal/c/at;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    .line 48
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    .line 49
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    .line 50
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    .line 51
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/at;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    .line 55
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    .line 56
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    .line 57
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    .line 58
    return-void
.end method


# virtual methods
.method final a(II)V
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    if-ge p1, v0, :cond_0

    .line 62
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    .line 64
    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    if-ge p2, v0, :cond_1

    .line 65
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    .line 67
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    if-le p1, v0, :cond_2

    .line 68
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    .line 70
    :cond_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    if-le p2, v0, :cond_3

    .line 71
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    .line 73
    :cond_3
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->a:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->b:I

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->c:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/at;->d:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x35

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "[("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "),("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
