.class public Lcom/google/android/apps/gmm/streetview/internal/ar;
.super Lcom/google/android/apps/gmm/map/m/s;
.source "PG"


# instance fields
.field a:Lcom/google/android/apps/gmm/v/bc;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public b:Lcom/google/android/apps/gmm/streetview/internal/at;

.field private final c:Lcom/google/android/apps/gmm/streetview/internal/aq;

.field private final d:Lcom/google/android/apps/gmm/streetview/internal/b;

.field private final e:Lcom/google/android/apps/gmm/streetview/internal/ak;

.field private final f:Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;

.field private g:Z

.field private h:F

.field private i:J

.field private j:Landroid/view/MotionEvent;

.field private k:F

.field private l:F

.field private m:Lcom/google/android/apps/gmm/streetview/internal/as;

.field private volatile n:F

.field private volatile o:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/streetview/internal/aq;Lcom/google/android/apps/gmm/streetview/internal/b;Lcom/google/android/apps/gmm/streetview/internal/ak;Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/s;-><init>()V

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->g:Z

    .line 58
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/as;->a:Lcom/google/android/apps/gmm/streetview/internal/as;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->m:Lcom/google/android/apps/gmm/streetview/internal/as;

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->c:Lcom/google/android/apps/gmm/streetview/internal/aq;

    .line 67
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    .line 68
    iput-object p3, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->e:Lcom/google/android/apps/gmm/streetview/internal/ak;

    .line 69
    iput-object p4, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->f:Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;

    .line 70
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 138
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->g:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    sget-object v3, Lcom/google/android/apps/gmm/streetview/internal/c;->a:Lcom/google/android/apps/gmm/streetview/internal/c;

    if-eq v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    if-nez v0, :cond_1

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v0

    .line 141
    iget v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->c:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v3, v3, Lcom/google/android/apps/gmm/streetview/internal/aq;->e:Landroid/util/DisplayMetrics;

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 142
    neg-float v3, p3

    mul-float/2addr v3, v2

    iput v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->n:F

    .line 143
    neg-float v3, p4

    mul-float/2addr v2, v3

    iput v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->o:F

    .line 144
    iget v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->n:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x43b40000    # 360.0f

    rem-float/2addr v2, v3

    iput v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 145
    iget v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iget v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->o:F

    add-float/2addr v2, v3

    const/high16 v3, -0x3d4c0000    # -90.0f

    const/high16 v4, 0x42b40000    # 90.0f

    invoke-static {v2, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v2

    iput v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    .line 146
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 148
    :cond_1
    return v1

    :cond_2
    move v2, v0

    .line 138
    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/n;Z)Z
    .locals 5

    .prologue
    const/high16 v4, 0x42b40000    # 90.0f

    const/high16 v3, 0x41700000    # 15.0f

    .line 90
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/m/n;->a()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->h:F

    .line 91
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/m/n;->j:J

    iput-wide v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->i:J

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v0

    .line 96
    iget v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iget v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->h:F

    div-float/2addr v1, v2

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v1

    .line 98
    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/streetview/b/a;->a(FFF)F

    move-result v1

    iput v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 101
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    sget-object v3, Lcom/google/android/apps/gmm/streetview/internal/c;->a:Lcom/google/android/apps/gmm/streetview/internal/c;

    iput-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    iput v2, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->d:I

    .line 117
    :cond_0
    return v1

    :cond_1
    move v0, v2

    .line 114
    goto :goto_0
.end method

.method public final b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    const v5, 0x455ac000    # 3500.0f

    const v4, -0x3aa54000    # -3500.0f

    const/4 v1, 0x1

    .line 123
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    sget-object v3, Lcom/google/android/apps/gmm/streetview/internal/c;->a:Lcom/google/android/apps/gmm/streetview/internal/c;

    if-eq v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v2, :cond_0

    move v0, v1

    :cond_0
    if-nez v0, :cond_1

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    .line 125
    invoke-static {p3, v4, v5}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v2

    .line 126
    invoke-static {p4, v4, v5}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v3

    .line 124
    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(FF)V

    .line 128
    :cond_1
    return v1

    :cond_2
    move v2, v0

    .line 123
    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/m/n;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 83
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/b;->e:Lcom/google/android/apps/gmm/streetview/internal/c;

    sget-object v3, Lcom/google/android/apps/gmm/streetview/internal/c;->a:Lcom/google/android/apps/gmm/streetview/internal/c;

    if-eq v2, v3, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    :goto_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->g:Z

    .line 84
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->g:Z

    return v0

    :cond_0
    move v2, v1

    .line 83
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final c(Lcom/google/android/apps/gmm/map/m/n;Z)V
    .locals 4

    .prologue
    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->g:Z

    .line 108
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->h:F

    sub-float/2addr v0, v1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->i:J

    long-to-float v1, v2

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    .line 109
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(F)V

    .line 110
    return-void
.end method

.method public final e(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public final f(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->a:Lcom/google/android/apps/gmm/v/bc;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->a:Lcom/google/android/apps/gmm/v/bc;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/v/bc;->a(FF)V

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->b:Lcom/google/android/apps/gmm/streetview/internal/at;

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->b:Lcom/google/android/apps/gmm/streetview/internal/at;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/streetview/internal/at;->a()Z

    move-result v0

    .line 164
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 172
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/as;->b:Lcom/google/android/apps/gmm/streetview/internal/as;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->m:Lcom/google/android/apps/gmm/streetview/internal/as;

    .line 173
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->j:Landroid/view/MotionEvent;

    .line 174
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->k:F

    .line 175
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->l:F

    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->j:Landroid/view/MotionEvent;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->m:Lcom/google/android/apps/gmm/streetview/internal/as;

    sget-object v1, Lcom/google/android/apps/gmm/streetview/internal/as;->b:Lcom/google/android/apps/gmm/streetview/internal/as;

    if-ne v0, v1, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->e:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v6, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    if-nez v0, :cond_4

    .line 185
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/apps/gmm/streetview/internal/as;->a:Lcom/google/android/apps/gmm/streetview/internal/as;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->m:Lcom/google/android/apps/gmm/streetview/internal/as;

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->j:Landroid/view/MotionEvent;

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_d

    .line 189
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->l:F

    sub-float/2addr v0, v1

    .line 190
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    iget v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->k:F

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->m:Lcom/google/android/apps/gmm/streetview/internal/as;

    sget-object v2, Lcom/google/android/apps/gmm/streetview/internal/as;->b:Lcom/google/android/apps/gmm/streetview/internal/as;

    if-ne v1, v2, :cond_2

    .line 192
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->j:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    .line 193
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->j:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 194
    const/16 v2, 0x14

    if-le v1, v2, :cond_c

    .line 196
    sget-object v1, Lcom/google/android/apps/gmm/streetview/internal/as;->c:Lcom/google/android/apps/gmm/streetview/internal/as;

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->m:Lcom/google/android/apps/gmm/streetview/internal/as;

    .line 201
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->m:Lcom/google/android/apps/gmm/streetview/internal/as;

    sget-object v2, Lcom/google/android/apps/gmm/streetview/internal/as;->c:Lcom/google/android/apps/gmm/streetview/internal/as;

    if-ne v1, v2, :cond_3

    .line 202
    const/high16 v1, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->f:Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->getHeight()I

    move-result v2

    neg-int v2, v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    const/high16 v2, 0x40800000    # 4.0f

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v1

    .line 211
    iget v2, v1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    mul-float/2addr v0, v2

    const/high16 v2, 0x41700000    # 15.0f

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v0

    .line 213
    const/high16 v2, 0x41700000    # 15.0f

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/streetview/b/a;->a(FFF)F

    move-result v0

    iput v0, v1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->a(Lcom/google/android/apps/gmm/streetview/b/a;)V

    .line 217
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->k:F

    .line 218
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->l:F

    .line 219
    const/4 v0, 0x1

    .line 221
    :goto_1
    return v0

    .line 183
    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->c:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v3

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->c:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v2, v2, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v4

    const/16 v2, 0x10

    new-array v5, v2, [F

    const/16 v2, 0x10

    new-array v2, v2, [F

    iget-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->c:Lcom/google/android/apps/gmm/streetview/internal/aq;

    iget-object v7, v7, Lcom/google/android/apps/gmm/streetview/internal/aq;->h:[F

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/16 v10, 0x10

    invoke-static {v7, v8, v5, v9, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v2, v7, v5, v8}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    int-to-float v3, v3

    div-float/2addr v0, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v0, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v0, v3

    int-to-float v3, v4

    div-float/2addr v1, v3

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v1, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v1, v3

    neg-float v1, v1

    const/4 v3, 0x4

    new-array v4, v3, [F

    const/4 v3, 0x0

    aput v0, v4, v3

    const/4 v0, 0x1

    aput v1, v4, v0

    const/4 v0, 0x2

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v4, v0

    const/4 v0, 0x3

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v4, v0

    const/4 v0, 0x4

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    const/4 v1, 0x0

    aget v2, v0, v1

    const/4 v3, 0x3

    aget v3, v0, v3

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    aget v2, v0, v1

    const/4 v3, 0x3

    aget v3, v0, v3

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    aget v2, v0, v1

    const/4 v3, 0x3

    aget v3, v0, v3

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    const/4 v1, 0x2

    aget v1, v0, v1

    neg-float v1, v1

    float-to-double v2, v1

    const/4 v1, 0x0

    aget v1, v0, v1

    float-to-double v4, v1

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    double-to-float v1, v2

    neg-float v1, v1

    const v2, 0x3fc90fdb

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->d:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v2

    float-to-double v4, v1

    iget v1, v2, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    float-to-double v8, v1

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    add-double/2addr v4, v8

    double-to-float v1, v4

    const/4 v3, 0x1

    aget v3, v0, v3

    float-to-double v4, v3

    const/4 v3, 0x2

    aget v0, v0, v3

    neg-float v0, v0

    float-to-double v8, v0

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v0, v4

    float-to-double v4, v0

    iget v0, v2, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    float-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    add-double/2addr v4, v8

    double-to-float v0, v4

    const v3, 0x40c90fdb

    rem-float/2addr v1, v3

    const v3, 0x40c90fdb

    div-float v3, v1, v3

    const v1, 0x3fc90fdb

    add-float/2addr v0, v1

    const v1, 0x40490fdb    # (float)Math.PI

    div-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v4, 0x0

    iget v5, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    invoke-static {v5}, Lcom/google/android/apps/gmm/streetview/internal/ao;->f(F)F

    move-result v5

    sub-float v5, v3, v5

    const/high16 v7, 0x3f000000    # 0.5f

    sub-float/2addr v5, v7

    aput v5, v1, v4

    const/4 v4, 0x1

    invoke-static {v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->e(F)F

    move-result v5

    iget v7, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    invoke-static {v7}, Lcom/google/android/apps/gmm/streetview/internal/ao;->h(F)F

    move-result v7

    sub-float/2addr v5, v7

    float-to-double v8, v5

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v5, v8

    iget v7, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->y:F

    invoke-static {v7}, Lcom/google/android/apps/gmm/streetview/internal/ao;->d(F)F

    move-result v7

    mul-float/2addr v5, v7

    sub-float/2addr v0, v5

    aput v0, v1, v4

    const/4 v0, 0x2

    new-array v4, v0, [F

    iget-object v5, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    const/4 v0, 0x0

    aget v0, v1, v0

    const/4 v7, 0x1

    aget v7, v1, v7

    iget-boolean v8, v5, Lcom/google/android/apps/gmm/streetview/internal/d;->h:Z

    if-nez v8, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Depth map must be decompressed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/streetview/internal/d;->a(F)I

    move-result v8

    invoke-virtual {v5, v7}, Lcom/google/android/apps/gmm/streetview/internal/d;->b(F)I

    move-result v9

    iget-object v10, v5, Lcom/google/android/apps/gmm/streetview/internal/d;->c:[B

    iget v11, v5, Lcom/google/android/apps/gmm/streetview/internal/d;->f:I

    mul-int/2addr v9, v11

    add-int/2addr v8, v9

    aget-byte v8, v10, v8

    and-int/lit16 v8, v8, 0xff

    if-eqz v4, :cond_7

    if-lez v8, :cond_7

    invoke-virtual {v5, v0, v7}, Lcom/google/android/apps/gmm/streetview/internal/d;->a(FF)Lcom/google/android/apps/gmm/streetview/internal/e;

    move-result-object v9

    const/4 v10, 0x3

    new-array v10, v10, [F

    const/4 v11, 0x0

    invoke-static {v0, v7, v10, v11}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FF[FI)V

    const/4 v0, 0x0

    aget v0, v10, v0

    const/4 v7, 0x2

    aget v7, v10, v7

    neg-float v7, v7

    const/4 v11, 0x1

    aget v11, v10, v11

    iget v12, v9, Lcom/google/android/apps/gmm/streetview/internal/e;->d:F

    iget v13, v9, Lcom/google/android/apps/gmm/streetview/internal/e;->a:F

    mul-float/2addr v0, v13

    iget v13, v9, Lcom/google/android/apps/gmm/streetview/internal/e;->b:F

    mul-float/2addr v7, v13

    add-float/2addr v0, v7

    iget v7, v9, Lcom/google/android/apps/gmm/streetview/internal/e;->c:F

    mul-float/2addr v7, v11

    add-float/2addr v0, v7

    div-float v7, v12, v0

    const/4 v0, 0x0

    :goto_2
    const/4 v9, 0x3

    if-ge v0, v9, :cond_6

    aget v9, v10, v0

    mul-float/2addr v9, v7

    aput v9, v10, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    const/4 v0, 0x0

    aget v0, v10, v0

    iget-object v7, v5, Lcom/google/android/apps/gmm/streetview/internal/d;->e:[Lcom/google/android/apps/gmm/streetview/internal/f;

    aget-object v7, v7, v8

    iget v7, v7, Lcom/google/android/apps/gmm/streetview/internal/f;->a:F

    sub-float/2addr v0, v7

    const/4 v7, 0x1

    aget v7, v10, v7

    const/4 v9, 0x2

    aget v9, v10, v9

    iget-object v10, v5, Lcom/google/android/apps/gmm/streetview/internal/d;->e:[Lcom/google/android/apps/gmm/streetview/internal/f;

    aget-object v10, v10, v8

    iget v10, v10, Lcom/google/android/apps/gmm/streetview/internal/f;->b:F

    add-float/2addr v9, v10

    invoke-static {v0, v7, v9, v4}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF[F)V

    :cond_7
    iget-object v0, v5, Lcom/google/android/apps/gmm/streetview/internal/d;->d:[Ljava/lang/String;

    aget-object v5, v0, v8

    const/4 v0, 0x0

    iget-object v7, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->w:Lcom/google/android/apps/gmm/streetview/internal/d;

    const/4 v8, 0x0

    aget v8, v1, v8

    const/4 v9, 0x1

    aget v1, v1, v9

    iget-boolean v9, v7, Lcom/google/android/apps/gmm/streetview/internal/d;->h:Z

    if-nez v9, :cond_8

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Depth map must be decompressed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    invoke-virtual {v7, v8, v1}, Lcom/google/android/apps/gmm/streetview/internal/d;->a(FF)Lcom/google/android/apps/gmm/streetview/internal/e;

    move-result-object v1

    if-nez v1, :cond_9

    const/4 v1, 0x0

    :goto_3
    if-nez v1, :cond_e

    const/4 v0, 0x0

    aget v0, v4, v0

    const/4 v1, 0x1

    aget v1, v4, v1

    const/4 v7, 0x0

    iget v8, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->q:F

    invoke-static {v8}, Lcom/google/android/apps/gmm/streetview/internal/ao;->f(F)F

    move-result v8

    add-float/2addr v0, v8

    const/high16 v8, 0x3f000000    # 0.5f

    add-float/2addr v0, v8

    aput v0, v4, v7

    const/4 v0, 0x1

    const/4 v7, 0x0

    aget v7, v4, v7

    invoke-static {v7}, Lcom/google/android/apps/gmm/streetview/internal/ao;->e(F)F

    move-result v7

    iget v8, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->r:F

    invoke-static {v8}, Lcom/google/android/apps/gmm/streetview/internal/ao;->h(F)F

    move-result v8

    sub-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    double-to-float v7, v8

    iget v8, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->y:F

    invoke-static {v8}, Lcom/google/android/apps/gmm/streetview/internal/ao;->d(F)F

    move-result v8

    mul-float/2addr v7, v8

    add-float/2addr v1, v7

    aput v1, v4, v0

    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    const/4 v1, 0x0

    aget v1, v4, v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(F)F

    move-result v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/streetview/internal/ao;->g(F)F

    move-result v1

    const/4 v7, 0x1

    aget v4, v4, v7

    invoke-static {v4}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(F)F

    move-result v4

    const/high16 v7, 0x42b40000    # 90.0f

    mul-float/2addr v4, v7

    iget v2, v2, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>(FFF)V

    move-object v1, v0

    :goto_4
    if-eqz v5, :cond_0

    iget-object v0, v6, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v2, Lcom/google/android/apps/gmm/streetview/internal/aa;

    invoke-static {v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->g(F)F

    move-result v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v0, v5, v3, v4}, Lcom/google/android/apps/gmm/streetview/internal/aa;-><init>(FLjava/lang/String;ILjava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/ar;->f:Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;

    iget-object v0, v3, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/internal/b;->c:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x1

    :goto_5
    if-nez v0, :cond_0

    iput-object v2, v3, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->i:Lcom/google/android/apps/gmm/streetview/internal/aa;

    iget-object v0, v2, Lcom/google/android/apps/gmm/streetview/internal/aa;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2, v1}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a(Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V

    goto/16 :goto_0

    :cond_9
    iget v1, v1, Lcom/google/android/apps/gmm/streetview/internal/e;->c:F

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v7, 0x3f666666    # 0.9f

    cmpl-float v1, v1, v7

    if-ltz v1, :cond_a

    const/4 v1, 0x1

    goto/16 :goto_3

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_b
    const/4 v0, 0x0

    goto :goto_5

    .line 198
    :cond_c
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 221
    :cond_d
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_e
    move-object v1, v0

    goto :goto_4
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 295
    const/4 v0, 0x0

    return v0
.end method
