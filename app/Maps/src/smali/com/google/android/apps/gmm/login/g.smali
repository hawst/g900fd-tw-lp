.class Lcom/google/android/apps/gmm/login/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/accounts/Account;

.field final synthetic b:Landroid/app/Activity;

.field final synthetic c:Lcom/google/android/apps/gmm/login/a/b;

.field final synthetic d:Lcom/google/android/apps/gmm/login/a;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/login/a;Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/apps/gmm/login/a/b;)V
    .locals 0

    .prologue
    .line 500
    iput-object p1, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/login/g;->a:Landroid/accounts/Account;

    iput-object p3, p0, Lcom/google/android/apps/gmm/login/g;->b:Landroid/app/Activity;

    iput-object p4, p0, Lcom/google/android/apps/gmm/login/g;->c:Lcom/google/android/apps/gmm/login/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v0, 0x0

    .line 503
    iget-object v1, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/login/g;->a:Landroid/accounts/Account;

    const-string v3, "oauth2:https://www.googleapis.com/auth/mobilemaps.firstparty"

    if-nez v2, :cond_1

    move-object v7, v0

    .line 505
    :goto_0
    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/shared/net/p;->d()Ljava/lang/String;

    move-result-object v0

    .line 506
    if-nez v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/login/a;->c:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/g;->a:Landroid/accounts/Account;

    const-string v2, "oauth2:https://www.googleapis.com/auth/mobilemaps.firstparty"

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/login/g;->b:Landroid/app/Activity;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->updateCredentials(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 510
    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    .line 512
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/shared/net/p;->d()Ljava/lang/String;

    move-result-object v0

    .line 514
    :cond_0
    if-eqz v0, :cond_2

    .line 515
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    invoke-virtual {v0, v7}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/shared/net/p;)V

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/g;->c:Lcom/google/android/apps/gmm/login/a/b;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/login/a/b;Z)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 544
    :goto_1
    return-void

    .line 503
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/shared/net/p;

    iget-object v4, v1, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v4

    iget-object v1, v1, Lcom/google/android/apps/gmm/login/a;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/google/android/apps/gmm/shared/net/p;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/f;Landroid/accounts/Account;Ljava/lang/String;)V

    move-object v7, v0

    goto :goto_0

    .line 519
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    .line 520
    iget-object v0, v0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/g;->b:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/gmm/l;->oi:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 519
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 521
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/g;->c:Lcom/google/android/apps/gmm/login/a/b;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/login/a/b;Z)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/g; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 523
    :catch_0
    move-exception v0

    .line 524
    invoke-virtual {v0}, Lcom/google/android/gms/auth/g;->a()Landroid/content/Intent;

    move-result-object v1

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->c:Lcom/google/android/apps/gmm/login/a/b;

    if-eqz v0, :cond_4

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->c:Lcom/google/android/apps/gmm/login/a/b;

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    .line 527
    const-string v0, "callerExtras"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 528
    if-nez v0, :cond_3

    .line 529
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 530
    const-string v3, "callerExtras"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 532
    :cond_3
    const-string v3, "callbackId"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 533
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/login/a;->h:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/login/g;->c:Lcom/google/android/apps/gmm/login/a/b;

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 535
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->b:Landroid/app/Activity;

    sget-object v2, Lcom/google/android/apps/gmm/l/al;->d:Lcom/google/android/apps/gmm/l/al;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/l/al;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    .line 536
    :catch_1
    move-exception v0

    .line 537
    sget-object v1, Lcom/google/android/apps/gmm/login/a;->a:Ljava/lang/String;

    .line 538
    const-class v1, Ljava/lang/RuntimeException;

    if-eqz v0, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    throw v0

    .line 540
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    .line 541
    iget-object v0, v0, Lcom/google/android/apps/gmm/login/a;->b:Lcom/google/android/apps/gmm/base/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/g;->b:Landroid/app/Activity;

    sget v2, Lcom/google/android/apps/gmm/l;->oi:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 540
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/util/r;->a(Lcom/google/android/apps/gmm/map/c/a;Ljava/lang/CharSequence;)V

    .line 542
    iget-object v0, p0, Lcom/google/android/apps/gmm/login/g;->d:Lcom/google/android/apps/gmm/login/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/login/g;->c:Lcom/google/android/apps/gmm/login/a/b;

    invoke-static {v0, v1, v8}, Lcom/google/android/apps/gmm/login/a;->a(Lcom/google/android/apps/gmm/login/a;Lcom/google/android/apps/gmm/login/a/b;Z)V

    goto/16 :goto_1
.end method
