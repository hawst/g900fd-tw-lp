.class final Lcom/google/android/apps/gmm/l/ax;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/gmm/l/ax;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/gmm/l/ax;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 57
    :try_start_0
    new-instance v0, Ljava/net/URL;

    iget-object v3, p0, Lcom/google/android/apps/gmm/l/ax;->a:Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 61
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 62
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 63
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 72
    const-string v3, "Location"

    .line 73
    invoke-virtual {v0, v3}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 74
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    .line 75
    sget-object v0, Lcom/google/android/apps/gmm/l/aw;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ax;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    .line 89
    :cond_1
    :goto_1
    return-void

    .line 65
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/l/aw;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ax;->a:Ljava/lang/String;

    goto :goto_1

    .line 68
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/l/aw;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ax;->a:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ax;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/ax;->b:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v2, Lcom/google/android/apps/gmm/l/az;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/l/az;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/base/activities/c;)V

    new-instance v0, Lcom/google/android/apps/gmm/l/ba;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/l/ba;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/content/DialogInterface$OnClickListener;)V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_2
    move v0, v1

    .line 74
    goto :goto_0

    .line 79
    :cond_3
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    move v0, v2

    :goto_2
    if-nez v0, :cond_1

    .line 80
    invoke-static {v3}, Lcom/google/android/apps/gmm/invocation/a/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/ax;->b:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v4, Lcom/google/android/apps/gmm/l/ay;

    invoke-direct {v4, p0, v3}, Lcom/google/android/apps/gmm/l/ay;-><init>(Lcom/google/android/apps/gmm/l/ax;Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 87
    sget-object v0, Lcom/google/android/apps/gmm/l/aw;->b:Ljava/lang/String;

    const-string v3, "Expansion of shortened url \'%s\' failed"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/l/ax;->a:Ljava/lang/String;

    aput-object v4, v2, v1

    invoke-static {v0, v3, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    move v0, v1

    .line 79
    goto :goto_2
.end method
