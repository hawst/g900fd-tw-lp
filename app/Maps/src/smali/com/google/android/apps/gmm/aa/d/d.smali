.class public Lcom/google/android/apps/gmm/aa/d/d;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field b:J

.field final c:Ljava/io/File;

.field final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final e:J

.field final f:J

.field final g:I

.field final h:I

.field i:I

.field j:J

.field final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final l:Lcom/google/android/apps/gmm/shared/net/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/apps/gmm/aa/d/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Ljava/lang/String;JI)V
    .locals 3

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->b:J

    .line 103
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 104
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->c:Ljava/io/File;

    .line 105
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    .line 106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->k:Ljava/util/Map;

    .line 108
    iput-wide p3, p0, Lcom/google/android/apps/gmm/aa/d/d;->e:J

    .line 110
    const/4 v0, 0x1

    shr-long v0, p3, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->f:J

    .line 112
    iput p5, p0, Lcom/google/android/apps/gmm/aa/d/d;->g:I

    .line 114
    shr-int/lit8 v0, p5, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->h:I

    .line 116
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    .line 117
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    .line 121
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/aa/d/e;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/aa/d/e;-><init>(Lcom/google/android/apps/gmm/aa/d/d;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 127
    return-void
.end method

.method private static a(Ljava/lang/String;J)Ljava/lang/String;
    .locals 5

    .prologue
    .line 163
    const-string v0, "_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/lang/String;)Ljava/util/Map$Entry;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map$Entry",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 169
    const-string v1, "_"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 170
    array-length v1, v2

    const/4 v3, 0x2

    if-eq v1, v3, :cond_0

    .line 177
    :goto_0
    return-object v0

    .line 174
    :cond_0
    const/4 v1, 0x1

    :try_start_0
    aget-object v1, v2, v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 175
    new-instance v1, Ljava/util/AbstractMap$SimpleImmutableEntry;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/AbstractMap$SimpleImmutableEntry;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 177
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 142
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 143
    :cond_0
    const/4 v0, 0x0

    .line 145
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private e(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 216
    if-nez p1, :cond_0

    .line 217
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    .line 231
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 222
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 224
    if-nez v0, :cond_1

    .line 225
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 230
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 231
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 227
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1
.end method

.method private f(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x1

    .line 239
    if-nez p1, :cond_0

    .line 240
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    .line 256
    :goto_0
    return-void

    .line 244
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 245
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 247
    if-nez v0, :cond_2

    .line 248
    sget-object v2, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v2, "Bad releaseUsage call [usageCount=%s, filePath=%s]."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    :cond_1
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 251
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, v3, :cond_3

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 253
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v3, :cond_1

    .line 254
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 136
    if-nez p1, :cond_0

    move-object v0, v1

    .line 137
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/aa/d/d;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/apps/gmm/aa/d/d;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method a()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 378
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 379
    :try_start_0
    iget v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    if-ltz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 380
    :cond_0
    sget-object v2, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v2, "Scanning for the count and total size of files [dir=%s]."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/d/d;->c:Ljava/io/File;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 381
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    .line 382
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    .line 383
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->c:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 386
    if-eqz v2, :cond_2

    .line 387
    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 388
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 389
    const/4 v5, 0x1

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    invoke-virtual {p0, v5, v6, v7}, Lcom/google/android/apps/gmm/aa/d/d;->a(IJ)V

    .line 390
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/apps/gmm/aa/d/d;->b(Ljava/lang/String;)Ljava/util/Map$Entry;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/d/d;->k:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 387
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 395
    :cond_2
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(IJ)V
    .locals 6

    .prologue
    .line 402
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/d/d;->d:Ljava/util/Map;

    monitor-enter v1

    .line 403
    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    .line 404
    iget-wide v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    add-long/2addr v2, p2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    .line 408
    iget v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->i:I

    if-ltz v0, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->j:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_1

    .line 409
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v0, "Bookkeeping has become corrupt [dir=%s]."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/aa/d/d;->c:Ljava/io/File;

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 411
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/io/File;Ljava/lang/String;)Z
    .locals 10
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 332
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/d/d;->a()V

    .line 334
    iget-object v2, p0, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 335
    invoke-static {p2, v2, v3}, Lcom/google/android/apps/gmm/aa/d/d;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/aa/d/d;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 338
    :try_start_0
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/aa/d/d;->e(Ljava/lang/String;)V

    .line 341
    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/d/d;->k:Ljava/util/Map;

    invoke-interface {v5, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 342
    new-instance v5, Ljava/io/File;

    invoke-virtual {p0, p2}, Lcom/google/android/apps/gmm/aa/d/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 343
    const/4 v6, -0x1

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    neg-long v8, v8

    invoke-virtual {p0, v6, v8, v9}, Lcom/google/android/apps/gmm/aa/d/d;->a(IJ)V

    .line 344
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 347
    :cond_0
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 348
    invoke-virtual {p1, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 349
    sget-object v1, Lcom/google/android/apps/gmm/aa/d/d;->a:Ljava/lang/String;

    const-string v1, "Unable to rename %s to %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 350
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    const/4 v3, 0x1

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/aa/d/d;->f(Ljava/lang/String;)V

    .line 368
    :goto_0
    return v0

    .line 354
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->k:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, p2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    invoke-virtual {v5, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 357
    const/4 v0, 0x1

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/aa/d/d;->a(IJ)V

    .line 361
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/aa/d/f;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/aa/d/f;-><init>(Lcom/google/android/apps/gmm/aa/d/d;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 368
    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/aa/d/d;->f(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/aa/d/d;->f(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(Ljava/lang/String;)Z
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 272
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/aa/d/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 273
    if-nez v2, :cond_0

    move v0, v1

    .line 292
    :goto_0
    return v0

    .line 278
    :cond_0
    :try_start_0
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/aa/d/d;->e(Ljava/lang/String;)V

    .line 279
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 281
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Ljava/io/File;->length()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_2

    .line 282
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/aa/d/d;->f(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 286
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->k:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 287
    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    iget-object v8, p0, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/shared/net/ad;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/apps/gmm/shared/net/a/b;->y()Lcom/google/r/b/a/amh;

    move-result-object v8

    iget v8, v8, Lcom/google/r/b/a/amh;->e:I

    int-to-long v8, v8

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v8

    sub-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 288
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/aa/d/d;->f(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    .line 291
    :cond_3
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/d/d;->l:Lcom/google/android/apps/gmm/shared/net/ad;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v0

    invoke-virtual {v3, v0, v1}, Ljava/io/File;->setLastModified(J)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 292
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/aa/d/d;->f(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/aa/d/d;->f(Ljava/lang/String;)V

    throw v0
.end method
