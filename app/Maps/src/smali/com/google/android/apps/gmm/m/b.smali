.class Lcom/google/android/apps/gmm/m/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/m/h;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/m/a;

.field private b:Ljava/io/FileOutputStream;

.field private c:Ljava/nio/channels/FileLock;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/m/a;Ljava/io/FileOutputStream;Ljava/nio/channels/FileLock;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 274
    iput-object p1, p0, Lcom/google/android/apps/gmm/m/b;->a:Lcom/google/android/apps/gmm/m/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    iput-object v0, p0, Lcom/google/android/apps/gmm/m/b;->b:Ljava/io/FileOutputStream;

    .line 270
    iput-object v0, p0, Lcom/google/android/apps/gmm/m/b;->c:Ljava/nio/channels/FileLock;

    .line 275
    iput-object p2, p0, Lcom/google/android/apps/gmm/m/b;->b:Ljava/io/FileOutputStream;

    .line 276
    iput-object p3, p0, Lcom/google/android/apps/gmm/m/b;->c:Ljava/nio/channels/FileLock;

    .line 277
    iput-object p4, p0, Lcom/google/android/apps/gmm/m/b;->d:Ljava/lang/String;

    .line 278
    iget-object v0, p1, Lcom/google/android/apps/gmm/m/a;->a:Ljava/util/Set;

    invoke-interface {v0, p4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 279
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 283
    iget-object v1, p0, Lcom/google/android/apps/gmm/m/b;->a:Lcom/google/android/apps/gmm/m/a;

    iget-object v1, v1, Lcom/google/android/apps/gmm/m/a;->a:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/apps/gmm/m/b;->d:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 285
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/m/b;->c:Ljava/nio/channels/FileLock;

    if-eqz v1, :cond_0

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/gmm/m/b;->c:Ljava/nio/channels/FileLock;

    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/m/b;->b:Ljava/io/FileOutputStream;

    invoke-static {v0}, Lcom/google/android/apps/gmm/m/a;->a(Ljava/io/FileOutputStream;)Z

    move-result v0

    :goto_0
    return v0

    .line 288
    :catch_0
    move-exception v1

    .line 289
    const-string v2, "Failed to release lock: "

    invoke-virtual {v1}, Ljava/nio/channels/ClosedChannelException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 291
    :catch_1
    move-exception v1

    .line 292
    const-string v2, "Failed to release lock: "

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
