.class public Lcom/google/android/apps/gmm/mylocation/l;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/mylocation/b/b;


# static fields
.field private static final c:J

.field private static f:Z


# instance fields
.field a:Z

.field b:Lcom/google/android/apps/gmm/base/activities/c;

.field private g:Lcom/google/android/apps/gmm/mylocation/f;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 25
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1e

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/mylocation/l;->c:J

    .line 31
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/gmm/mylocation/l;->f:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->a:Z

    return-void
.end method

.method private a(ZZZLcom/google/android/apps/gmm/mylocation/b/c;)V
    .locals 2
    .param p4    # Lcom/google/android/apps/gmm/mylocation/b/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 108
    sget-boolean v0, Lcom/google/android/apps/gmm/mylocation/l;->f:Z

    if-nez v0, :cond_0

    .line 109
    sget-object v0, Lcom/google/android/apps/gmm/mylocation/b/d;->b:Lcom/google/android/apps/gmm/mylocation/b/d;

    invoke-interface {p4, v0}, Lcom/google/android/apps/gmm/mylocation/b/c;->a(Lcom/google/android/apps/gmm/mylocation/b/d;)V

    .line 143
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->g:Lcom/google/android/apps/gmm/mylocation/f;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/mylocation/h;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->g:Lcom/google/android/apps/gmm/mylocation/f;

    .line 119
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/m;

    invoke-direct {v0, p0, p4}, Lcom/google/android/apps/gmm/mylocation/m;-><init>(Lcom/google/android/apps/gmm/mylocation/l;Lcom/google/android/apps/gmm/mylocation/b/c;)V

    .line 141
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/l;->g:Lcom/google/android/apps/gmm/mylocation/f;

    invoke-interface {v1, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/mylocation/f;->a(ZZZLcom/google/android/apps/gmm/mylocation/g;)V

    goto :goto_0
.end method

.method private d()Z
    .locals 6

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->aW:Lcom/google/android/apps/gmm/shared/b/c;

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;J)J

    move-result-wide v0

    .line 154
    sget-wide v4, Lcom/google/android/apps/gmm/mylocation/l;->c:J

    add-long/2addr v0, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->g:Lcom/google/android/apps/gmm/mylocation/f;

    instance-of v0, v0, Lcom/google/android/apps/gmm/mylocation/e;

    if-nez v0, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->g:Lcom/google/android/apps/gmm/mylocation/f;

    check-cast v0, Lcom/google/android/apps/gmm/mylocation/e;

    .line 179
    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/mylocation/e;->a:Lcom/google/android/apps/gmm/mylocation/g;

    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/google/android/apps/gmm/mylocation/e;->a:Lcom/google/android/apps/gmm/mylocation/g;

    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/d;->c:Lcom/google/android/apps/gmm/mylocation/b/d;

    :goto_1
    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/mylocation/g;->a(Lcom/google/android/apps/gmm/mylocation/b/d;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/mylocation/e;->a:Lcom/google/android/apps/gmm/mylocation/g;

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/google/android/apps/gmm/mylocation/b/d;->h:Lcom/google/android/apps/gmm/mylocation/b/d;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/mylocation/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 41
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/b/c;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/mylocation/b/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 85
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->a:Z

    .line 87
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/l;->d()Z

    move-result v1

    const/4 v2, 0x1

    .line 85
    invoke-direct {p0, v0, v1, v2, p1}, Lcom/google/android/apps/gmm/mylocation/l;->a(ZZZLcom/google/android/apps/gmm/mylocation/b/c;)V

    .line 90
    return-void
.end method

.method public final a(ZLcom/google/android/apps/gmm/mylocation/b/c;)V
    .locals 3
    .param p2    # Lcom/google/android/apps/gmm/mylocation/b/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 67
    if-eqz p1, :cond_0

    .line 68
    invoke-direct {p0, v2, v2, v2, p2}, Lcom/google/android/apps/gmm/mylocation/l;->a(ZZZLcom/google/android/apps/gmm/mylocation/b/c;)V

    .line 80
    :goto_0
    return-void

    .line 74
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->a:Z

    .line 76
    invoke-direct {p0}, Lcom/google/android/apps/gmm/mylocation/l;->d()Z

    move-result v1

    .line 74
    invoke-direct {p0, v0, v1, v2, p2}, Lcom/google/android/apps/gmm/mylocation/l;->a(ZZZLcom/google/android/apps/gmm/mylocation/b/c;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/mylocation/h;->a(Lcom/google/android/apps/gmm/base/activities/c;)Z

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/l;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 46
    return-void
.end method
