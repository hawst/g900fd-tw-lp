.class public Lcom/google/android/apps/gmm/v/n;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/cl;


# instance fields
.field public E:Lcom/google/android/apps/gmm/v/bi;

.field public volatile F:[F

.field public volatile G:[F

.field public H:[F

.field public I:I

.field J:B

.field public volatile K:F

.field public L:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/v/bf;",
            ">;"
        }
    .end annotation
.end field

.field private a:Ljava/lang/String;

.field private b:Z

.field private final c:F

.field private final d:F

.field private e:Landroid/graphics/PointF;

.field private final f:Lcom/google/android/apps/gmm/v/p;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/v/bi;IFF)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    .line 47
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    .line 52
    new-array v0, v3, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    .line 58
    iput v2, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 72
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    .line 93
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->L:Ljava/util/List;

    .line 189
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 190
    int-to-byte v0, p2

    iput-byte v0, p0, Lcom/google/android/apps/gmm/v/n;->J:B

    .line 191
    iput p3, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    .line 192
    iput p4, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    .line 193
    iput v1, p0, Lcom/google/android/apps/gmm/v/n;->K:F

    .line 194
    sget-object v0, Lcom/google/android/apps/gmm/v/p;->b:Lcom/google/android/apps/gmm/v/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 196
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/bi;IFFF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    .line 47
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    .line 52
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    .line 58
    iput v1, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 72
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    .line 93
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v3, v3}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->L:Ljava/util/List;

    .line 150
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Lcom/google/android/apps/gmm/v/bi;

    iput-object p1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 151
    int-to-byte v0, p2

    iput-byte v0, p0, Lcom/google/android/apps/gmm/v/n;->J:B

    .line 152
    iput p3, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    .line 153
    iput p4, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    .line 154
    sget-object v0, Lcom/google/android/apps/gmm/v/p;->a:Lcom/google/android/apps/gmm/v/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    .line 155
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 156
    invoke-virtual {p0, p5}, Lcom/google/android/apps/gmm/v/n;->a(F)V

    .line 157
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/v/n;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/16 v1, 0x10

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    .line 47
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    .line 52
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    .line 58
    iput v3, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 72
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    .line 93
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    .line 105
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->L:Ljava/util/List;

    .line 115
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    .line 116
    iget v0, p1, Lcom/google/android/apps/gmm/v/n;->d:F

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    .line 117
    iget v0, p1, Lcom/google/android/apps/gmm/v/n;->K:F

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->K:F

    .line 118
    iget v0, p1, Lcom/google/android/apps/gmm/v/n;->c:F

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    .line 119
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/v/n;->a(Lcom/google/android/apps/gmm/v/n;)V

    .line 120
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/bi;)V
    .locals 0

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/v/n;-><init>(Lcom/google/android/apps/gmm/v/n;)V

    .line 124
    iput-object p2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 125
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 527
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->L:Ljava/util/List;

    monitor-enter v1

    .line 528
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->L:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/bf;

    .line 529
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/v/bf;->a(Lcom/google/android/apps/gmm/v/n;)V

    goto :goto_0

    .line 531
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 222
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    if-eqz v0, :cond_0

    .line 223
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 225
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    sget-object v2, Lcom/google/android/apps/gmm/v/p;->a:Lcom/google/android/apps/gmm/v/p;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 227
    :cond_2
    iput p1, p0, Lcom/google/android/apps/gmm/v/n;->K:F

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 229
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    .line 228
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/v/n;->a([FII)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 231
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/bi;II)V
    .locals 10

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/4 v9, 0x0

    .line 331
    sget-object v0, Lcom/google/android/apps/gmm/v/o;->a:[I

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/p;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 343
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unimplemented projection type "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/v/n;->a([FII)V

    .line 345
    :cond_0
    :goto_0
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 346
    invoke-direct {p0}, Lcom/google/android/apps/gmm/v/n;->a()V

    .line 347
    return-void

    .line 336
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    int-to-float v2, p2

    int-to-float v3, p3

    iget v4, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    iget v5, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    cmpl-float v6, v9, v2

    if-eqz v6, :cond_0

    cmpl-float v6, v3, v9

    if-eqz v6, :cond_0

    cmpl-float v6, v4, v5

    if-eqz v6, :cond_0

    sub-float v6, v2, v9

    div-float v6, v8, v6

    aput v6, v0, v1

    const/4 v6, 0x1

    aput v9, v0, v6

    const/4 v6, 0x2

    aput v9, v0, v6

    const/4 v6, 0x3

    aput v9, v0, v6

    const/4 v6, 0x4

    aput v9, v0, v6

    const/4 v6, 0x5

    sub-float v7, v3, v9

    div-float v7, v8, v7

    aput v7, v0, v6

    const/4 v6, 0x6

    aput v9, v0, v6

    const/4 v6, 0x7

    aput v9, v0, v6

    const/16 v6, 0x8

    aput v9, v0, v6

    const/16 v6, 0x9

    aput v9, v0, v6

    const/16 v6, 0xa

    const/high16 v7, -0x40000000    # -2.0f

    sub-float v8, v5, v4

    div-float/2addr v7, v8

    aput v7, v0, v6

    const/16 v6, 0xb

    aput v9, v0, v6

    const/16 v6, 0xc

    add-float v7, v2, v9

    neg-float v7, v7

    sub-float/2addr v2, v9

    div-float v2, v7, v2

    aput v2, v0, v6

    const/16 v2, 0xd

    add-float v6, v3, v9

    neg-float v6, v6

    sub-float/2addr v3, v9

    div-float v3, v6, v3

    aput v3, v0, v2

    const/16 v2, 0xe

    add-float v3, v5, v4

    neg-float v3, v3

    sub-float v4, v5, v4

    div-float/2addr v3, v4

    aput v3, v0, v2

    const/16 v2, 0xf

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v0, v2

    goto/16 :goto_0

    .line 331
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/google/android/apps/gmm/v/cj;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 206
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    if-eqz v0, :cond_0

    .line 207
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 212
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    iget-object v2, p1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v1, v2, v1}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 214
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/n;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    const/4 v2, 0x0

    .line 538
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    iget v1, p1, Lcom/google/android/apps/gmm/v/n;->d:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->K:F

    iget v1, p1, Lcom/google/android/apps/gmm/v/n;->K:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    iget v1, p1, Lcom/google/android/apps/gmm/v/n;->c:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 544
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 546
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->a:Ljava/lang/String;

    .line 547
    iget-byte v0, p1, Lcom/google/android/apps/gmm/v/n;->J:B

    iput-byte v0, p0, Lcom/google/android/apps/gmm/v/n;->J:B

    .line 548
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 549
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 550
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->F:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 551
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 552
    iget v0, p1, Lcom/google/android/apps/gmm/v/n;->I:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 553
    return-void
.end method

.method public final a([F)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    const/16 v2, 0x10

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 257
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 258
    return-void
.end method

.method public final a([FII)V
    .locals 10

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 414
    if-nez p3, :cond_0

    move v0, v1

    .line 424
    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    iget v3, p0, Lcom/google/android/apps/gmm/v/n;->K:F

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->tan(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v2, v3

    .line 425
    neg-float v3, v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v1

    mul-float/2addr v3, v4

    .line 426
    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v4, v1, v4

    mul-float/2addr v4, v2

    .line 427
    neg-float v5, v2

    mul-float/2addr v5, v0

    iget-object v6, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    add-float/2addr v6, v1

    mul-float/2addr v5, v6

    .line 428
    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    .line 431
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    mul-float/2addr v2, v9

    sub-float v6, v0, v5

    div-float/2addr v2, v6

    aput v2, p1, v1

    .line 432
    const/4 v1, 0x1

    aput v8, p1, v1

    .line 433
    const/4 v1, 0x2

    aput v8, p1, v1

    .line 434
    const/4 v1, 0x3

    aput v8, p1, v1

    .line 437
    const/4 v1, 0x4

    aput v8, p1, v1

    .line 438
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    mul-float/2addr v2, v9

    sub-float v6, v4, v3

    div-float/2addr v2, v6

    aput v2, p1, v1

    .line 439
    const/4 v1, 0x6

    aput v8, p1, v1

    .line 440
    const/4 v1, 0x7

    aput v8, p1, v1

    .line 443
    const/16 v1, 0x8

    add-float v2, v0, v5

    sub-float/2addr v0, v5

    div-float v0, v2, v0

    aput v0, p1, v1

    .line 444
    const/16 v0, 0x9

    add-float v1, v4, v3

    sub-float v2, v4, v3

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 445
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    iget v2, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    add-float/2addr v1, v2

    neg-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    iget v3, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 446
    const/16 v0, 0xb

    const/high16 v1, -0x40800000    # -1.0f

    aput v1, p1, v0

    .line 449
    const/16 v0, 0xc

    aput v8, p1, v0

    .line 450
    const/16 v0, 0xd

    aput v8, p1, v0

    .line 451
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    mul-float/2addr v1, v9

    iget v2, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    mul-float/2addr v1, v2

    neg-float v1, v1

    iget v2, p0, Lcom/google/android/apps/gmm/v/n;->d:F

    iget v3, p0, Lcom/google/android/apps/gmm/v/n;->c:F

    sub-float/2addr v2, v3

    div-float/2addr v1, v2

    aput v1, p1, v0

    .line 452
    const/16 v0, 0xf

    aput v8, p1, v0

    .line 453
    return-void

    .line 417
    :cond_0
    int-to-float v0, p2

    int-to-float v2, p3

    div-float/2addr v0, v2

    goto/16 :goto_0
.end method

.method final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 3

    .prologue
    .line 309
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v0, :cond_0

    .line 310
    const/4 v0, 0x0

    .line 323
    :goto_0
    return v0

    .line 313
    :cond_0
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    .line 315
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/v/n;->a(Lcom/google/android/apps/gmm/v/bi;II)V

    .line 322
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/v/bi;->i:Z

    if-ne v1, v2, :cond_2

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v1, :cond_2

    .line 323
    :goto_2
    const/4 v0, 0x1

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    iget-object v1, v0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    monitor-enter v1

    :try_start_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/v/bi;->k:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 322
    :cond_2
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/v/bi;->i:Z

    goto :goto_2
.end method

.method public final b(FF)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 239
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/n;->b:Z

    if-eqz v0, :cond_0

    .line 240
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->f:Lcom/google/android/apps/gmm/v/p;

    sget-object v2, Lcom/google/android/apps/gmm/v/p;->a:Lcom/google/android/apps/gmm/v/p;

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 244
    :cond_2
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, p1, p2}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/n;->e:Landroid/graphics/PointF;

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 246
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    .line 245
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/v/n;->a([FII)V

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 248
    return-void
.end method

.method public final b([F)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 266
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    const/16 v2, 0x10

    invoke-static {p1, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 267
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/n;->H:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/n;->G:[F

    iget-object v4, p0, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    iget v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/v/n;->I:I

    .line 268
    return-void
.end method
