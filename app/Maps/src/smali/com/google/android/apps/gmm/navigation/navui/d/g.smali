.class public Lcom/google/android/apps/gmm/navigation/navui/d/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/views/d;
.implements Lcom/google/android/apps/gmm/navigation/navui/d/b;
.implements Lcom/google/android/apps/gmm/navigation/navui/d/f;


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/a;

.field final b:Z

.field final c:Lcom/google/android/apps/gmm/navigation/navui/ay;

.field final d:Lcom/google/android/apps/gmm/navigation/navui/d/e;

.field final e:Lcom/google/android/apps/gmm/navigation/navui/views/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final f:Lcom/google/android/apps/gmm/navigation/navui/views/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final g:Lcom/google/android/apps/gmm/base/activities/x;

.field h:Lcom/google/android/apps/gmm/navigation/navui/b/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field i:Lcom/google/android/apps/gmm/navigation/g/b/k;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field j:Lcom/google/android/apps/gmm/map/r/a/ag;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

.field l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/d/c;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/navui/d/c;",
            ">;"
        }
    .end annotation
.end field

.field n:Lcom/google/android/apps/gmm/navigation/navui/d/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field o:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field p:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public q:Ljava/lang/Runnable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final r:Lcom/google/android/apps/gmm/navigation/navui/g;

.field private final s:Lcom/google/android/apps/gmm/navigation/navui/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/navui/g;Lcom/google/android/apps/gmm/navigation/navui/h;Lcom/google/android/apps/gmm/base/a;ZLcom/google/android/apps/gmm/navigation/navui/d/e;Lcom/google/android/apps/gmm/navigation/navui/views/d;Lcom/google/android/apps/gmm/navigation/navui/views/g;)V
    .locals 4

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/x;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->g:Lcom/google/android/apps/gmm/base/activities/x;

    .line 68
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    .line 83
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->r:Lcom/google/android/apps/gmm/navigation/navui/g;

    .line 84
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->s:Lcom/google/android/apps/gmm/navigation/navui/h;

    .line 85
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->a:Lcom/google/android/apps/gmm/base/a;

    .line 86
    iput-boolean p4, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->b:Z

    .line 87
    new-instance v0, Lcom/google/android/apps/gmm/navigation/navui/ay;

    .line 88
    invoke-interface {p3}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    .line 89
    invoke-interface {p3}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v2

    .line 90
    invoke-interface {p3}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/navigation/navui/ay;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->c:Lcom/google/android/apps/gmm/navigation/navui/ay;

    .line 91
    iput-object p5, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->d:Lcom/google/android/apps/gmm/navigation/navui/d/e;

    .line 92
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->e:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    .line 93
    iput-object p7, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->f:Lcom/google/android/apps/gmm/navigation/navui/views/g;

    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    invoke-direct {v0, p1, p3}, Lcom/google/android/apps/gmm/navigation/commonui/c/m;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/base/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    .line 95
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/gmm/navigation/navui/d/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    return-object v0
.end method

.method public final a(ILcom/google/android/apps/gmm/base/views/e;)V
    .locals 4

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->r:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/g;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/navui/d/c;->a:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 348
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    .line 350
    sget-object v2, Lcom/google/android/apps/gmm/base/views/e;->a:Lcom/google/android/apps/gmm/base/views/e;

    if-ne p2, v2, :cond_4

    .line 351
    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->f:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    sget-object v3, Lcom/google/b/f/t;->bB:Lcom/google/b/f/t;

    .line 353
    invoke-static {v3}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v3

    .line 351
    invoke-interface {v1, v2, v3}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 362
    :cond_2
    :goto_1
    sget-object v1, Lcom/google/android/apps/gmm/base/views/e;->d:Lcom/google/android/apps/gmm/base/views/e;

    if-eq p2, v1, :cond_3

    .line 363
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->s:Lcom/google/android/apps/gmm/navigation/navui/h;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/h;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 366
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-lt p1, v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->r:Lcom/google/android/apps/gmm/navigation/navui/g;

    sget v1, Lcom/google/android/apps/gmm/l;->nh:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/g;->a(I)V

    goto :goto_0

    .line 355
    :cond_4
    sget-object v2, Lcom/google/android/apps/gmm/base/views/e;->b:Lcom/google/android/apps/gmm/base/views/e;

    if-ne p2, v2, :cond_5

    .line 356
    sget-object v2, Lcom/google/b/f/t;->bD:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    goto :goto_1

    .line 357
    :cond_5
    sget-object v2, Lcom/google/android/apps/gmm/base/views/e;->c:Lcom/google/android/apps/gmm/base/views/e;

    if-ne p2, v2, :cond_2

    .line 358
    sget-object v2, Lcom/google/b/f/t;->bC:Lcom/google/b/f/t;

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 2

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->r:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/g;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/b/f/t;->bB:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    sget-object v1, Lcom/google/android/apps/gmm/navigation/c/a/a;->d:Lcom/google/android/apps/gmm/navigation/c/a/a;

    if-ne v0, v1, :cond_2

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->j:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-ne p1, v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->s:Lcom/google/android/apps/gmm/navigation/navui/h;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/h;->a(Ljava/lang/Float;)V

    goto :goto_0

    .line 318
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->s:Lcom/google/android/apps/gmm/navigation/navui/h;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/navigation/navui/h;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    goto :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/apps/gmm/navigation/navui/d/a;",
            ">;"
        }
    .end annotation

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->m:Ljava/util/List;

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 3

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->r:Lcom/google/android/apps/gmm/navigation/navui/g;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/navui/g;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->e:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    if-nez v0, :cond_1

    .line 328
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->r:Lcom/google/android/apps/gmm/navigation/navui/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->i:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->e:Lcom/google/android/apps/gmm/navigation/navui/views/d;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/navigation/navui/g;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/android/apps/gmm/navigation/g/b/k;Lcom/google/android/apps/gmm/navigation/navui/views/d;)V

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/navigation/navui/d/a;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->n:Lcom/google/android/apps/gmm/navigation/navui/d/c;

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/base/views/d;
    .locals 0

    .prologue
    .line 332
    return-object p0
.end method

.method public final i()Lcom/google/android/apps/gmm/base/activities/x;
    .locals 1

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->g:Lcom/google/android/apps/gmm/base/activities/x;

    return-object v0
.end method

.method public final j()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->g:Z

    goto :goto_0
.end method

.method public final k()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->i:Z

    goto :goto_0
.end method

.method public final bridge synthetic l()Lcom/google/android/apps/gmm/navigation/commonui/c/l;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->k:Lcom/google/android/apps/gmm/navigation/commonui/c/m;

    return-object v0
.end method

.method public final m()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/navui/d/g;->h:Lcom/google/android/apps/gmm/navigation/navui/b/a;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/navui/b/a;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
