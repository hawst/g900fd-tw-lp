.class public final enum Lcom/google/android/apps/gmm/car/n/k;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/cn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/car/n/k;",
        ">;",
        "Lcom/google/android/libraries/curvular/cn;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/car/n/k;

.field private static final synthetic b:[Lcom/google/android/apps/gmm/car/n/k;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 13
    new-instance v0, Lcom/google/android/apps/gmm/car/n/k;

    const-string v1, "DEFAULT_FOCUS"

    const-string v2, "defaultFocus"

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/car/n/k;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/car/n/k;->a:Lcom/google/android/apps/gmm/car/n/k;

    .line 12
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/apps/gmm/car/n/k;

    sget-object v1, Lcom/google/android/apps/gmm/car/n/k;->a:Lcom/google/android/apps/gmm/car/n/k;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/apps/gmm/car/n/k;->b:[Lcom/google/android/apps/gmm/car/n/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/car/n/k;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/google/android/apps/gmm/car/n/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/n/k;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/car/n/k;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/apps/gmm/car/n/k;->b:[Lcom/google/android/apps/gmm/car/n/k;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/car/n/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/car/n/k;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/curvular/co;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/libraries/curvular/co;->a:Lcom/google/android/libraries/curvular/co;

    return-object v0
.end method
