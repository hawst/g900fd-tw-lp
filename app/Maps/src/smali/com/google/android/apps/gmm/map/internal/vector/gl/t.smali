.class public Lcom/google/android/apps/gmm/map/internal/vector/gl/t;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lcom/google/android/apps/gmm/map/internal/vector/gl/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;F[F)V
    .locals 7

    .prologue
    .line 45
    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FZ[F)V

    .line 53
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FZ[F)V
    .locals 4
    .param p3    # Lcom/google/android/apps/gmm/map/b/a/y;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 114
    if-nez p0, :cond_3

    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 116
    sget-object v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a:Lcom/google/android/apps/gmm/map/internal/vector/gl/u;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 131
    :goto_0
    invoke-virtual {p1, v1}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 132
    invoke-static {p2, v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 133
    if-eqz p5, :cond_0

    .line 134
    invoke-virtual {v0, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->i(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 136
    :cond_0
    if-eqz p3, :cond_1

    .line 137
    invoke-static {v0, p3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 139
    :cond_1
    iget v1, p1, Lcom/google/android/apps/gmm/map/f/o;->m:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/o;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p1, Lcom/google/android/apps/gmm/map/f/o;->m:F

    :cond_2
    iget v1, p1, Lcom/google/android/apps/gmm/map/f/o;->m:F

    .line 140
    const/4 v2, 0x0

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    aput v3, p6, v2

    .line 141
    const/4 v2, 0x1

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-float v3, v3

    mul-float/2addr v3, v1

    aput v3, p6, v2

    .line 142
    const/4 v2, 0x2

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    int-to-float v0, v0

    mul-float/2addr v0, v1

    aput v0, p6, v2

    .line 143
    const/4 v0, 0x3

    mul-float/2addr v1, p4

    aput v1, p6, v0

    .line 144
    return-void

    .line 118
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/vector/gl/u;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;F[F)V
    .locals 7

    .prologue
    .line 66
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FZ[F)V

    .line 73
    return-void
.end method

.method public static b(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;F[F)V
    .locals 7

    .prologue
    .line 88
    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/internal/vector/gl/t;->a(Lcom/google/android/apps/gmm/map/internal/vector/gl/u;Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FZ[F)V

    .line 96
    return-void
.end method
