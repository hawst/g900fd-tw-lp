.class public Lcom/google/android/apps/gmm/map/o/af;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lcom/google/android/apps/gmm/map/o/af;


# instance fields
.field final b:Z

.field final c:Z

.field final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 14
    new-instance v0, Lcom/google/android/apps/gmm/map/o/af;

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/apps/gmm/map/o/af;-><init>(ZZZ)V

    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/map/o/af;

    invoke-direct {v0, v1, v2, v1}, Lcom/google/android/apps/gmm/map/o/af;-><init>(ZZZ)V

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/map/o/af;

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/apps/gmm/map/o/af;-><init>(ZZZ)V

    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/map/o/af;

    invoke-direct {v0, v2, v1, v1}, Lcom/google/android/apps/gmm/map/o/af;-><init>(ZZZ)V

    sput-object v0, Lcom/google/android/apps/gmm/map/o/af;->a:Lcom/google/android/apps/gmm/map/o/af;

    return-void
.end method

.method private constructor <init>(ZZZ)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    .line 30
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    .line 31
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    .line 32
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/internal/c/m;)Lcom/google/android/apps/gmm/map/o/af;
    .locals 5
    .param p0    # Lcom/google/android/apps/gmm/map/internal/c/m;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 87
    .line 93
    instance-of v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;

    if-eqz v0, :cond_7

    .line 94
    check-cast p0, Lcom/google/android/apps/gmm/map/internal/c/an;

    .line 95
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v0, v0, 0x800

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v0, v0, 0x1000

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    .line 96
    :goto_2
    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v3, v3, 0x1000

    if-eqz v3, :cond_4

    move v3, v2

    :goto_3
    if-nez v3, :cond_5

    move v3, v2

    .line 97
    :goto_4
    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/c/an;->g:I

    and-int/lit16 v4, v4, 0x2000

    if-eqz v4, :cond_6

    .line 99
    :goto_5
    new-instance v1, Lcom/google/android/apps/gmm/map/o/af;

    invoke-direct {v1, v0, v3, v2}, Lcom/google/android/apps/gmm/map/o/af;-><init>(ZZZ)V

    return-object v1

    :cond_1
    move v0, v1

    .line 95
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v3, v1

    .line 96
    goto :goto_3

    :cond_5
    move v3, v1

    goto :goto_4

    :cond_6
    move v2, v1

    .line 97
    goto :goto_5

    :cond_7
    move v3, v2

    move v0, v1

    goto :goto_5
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 76
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/o/af;

    if-nez v1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 80
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/o/af;

    .line 81
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 67
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/af;->b:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/lit8 v2, v0, 0x0

    .line 69
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/o/af;->c:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_1
    add-int/2addr v0, v2

    .line 70
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/o/af;->d:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x4

    :cond_0
    add-int/2addr v0, v1

    .line 71
    return v0

    :cond_1
    move v0, v1

    .line 67
    goto :goto_0

    :cond_2
    move v0, v1

    .line 69
    goto :goto_1
.end method
