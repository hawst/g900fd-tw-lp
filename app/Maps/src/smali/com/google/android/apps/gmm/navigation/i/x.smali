.class public Lcom/google/android/apps/gmm/navigation/i/x;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public volatile a:Z

.field private final b:Lcom/google/android/apps/gmm/shared/net/a/n;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/a/n;)V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/x;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/i/x;->a:Z

    .line 135
    return-void
.end method

.method private static a(Ljava/util/HashMap;Lcom/google/android/apps/gmm/navigation/i/e;)Lcom/google/android/apps/gmm/navigation/i/aa;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            "Lcom/google/android/apps/gmm/navigation/i/ab;",
            ">;",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ")",
            "Lcom/google/android/apps/gmm/navigation/i/aa;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 271
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 272
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    .line 273
    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/ab;

    .line 274
    :goto_0
    if-eqz v0, :cond_0

    .line 275
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/ab;->f:Lcom/google/android/apps/gmm/navigation/i/ab;

    goto :goto_0

    .line 279
    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/z;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/i/z;->a:Lcom/google/android/apps/gmm/navigation/i/e;

    .line 280
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v6, v0, [Lcom/google/android/apps/gmm/map/u/a/e;

    .line 281
    array-length v0, v6

    new-array v7, v0, [I

    move v2, v1

    .line 283
    :goto_1
    array-length v0, v6

    if-ge v1, v0, :cond_1

    .line 284
    array-length v0, v6

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/i/ab;

    .line 285
    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/i/ab;->b:Lcom/google/android/apps/gmm/map/u/a/e;

    aput-object v3, v6, v1

    .line 286
    iget v3, v0, Lcom/google/android/apps/gmm/navigation/i/ab;->c:I

    .line 287
    sub-int v0, v3, v2

    aput v0, v7, v1

    .line 283
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v2, v3

    goto :goto_1

    .line 290
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/navigation/i/aa;

    invoke-direct {v0, v5, p1, v6, v7}, Lcom/google/android/apps/gmm/navigation/i/aa;-><init>(Lcom/google/android/apps/gmm/navigation/i/e;Lcom/google/android/apps/gmm/navigation/i/e;[Lcom/google/android/apps/gmm/map/u/a/e;[I)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Ljava/util/Collection;Ljava/util/Collection;)Lcom/google/android/apps/gmm/navigation/i/aa;
    .locals 20
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/ae;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/u/a/b;",
            "Lcom/google/android/apps/gmm/map/b/a/ae;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/apps/gmm/navigation/i/e;",
            ">;)",
            "Lcom/google/android/apps/gmm/navigation/i/aa;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v8, Ljava/util/PriorityQueue;

    const/16 v2, 0x64

    new-instance v3, Lcom/google/android/apps/gmm/navigation/i/y;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/navigation/i/y;-><init>()V

    invoke-direct {v8, v2, v3}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    .line 156
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 158
    invoke-interface/range {p3 .. p3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/e;

    .line 159
    new-instance v4, Lcom/google/android/apps/gmm/navigation/i/z;

    iget v5, v2, Lcom/google/android/apps/gmm/navigation/i/e;->h:I

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v2}, Lcom/google/android/apps/gmm/navigation/i/z;-><init>(ILcom/google/android/apps/gmm/navigation/i/ab;Lcom/google/android/apps/gmm/navigation/i/e;)V

    .line 161
    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    invoke-virtual {v9, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-virtual {v8, v4}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_0
    const/4 v4, 0x0

    .line 175
    const v3, 0x7fffffff

    .line 176
    invoke-interface/range {p4 .. p4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/e;

    .line 177
    new-instance v6, Lcom/google/android/apps/gmm/navigation/i/z;

    const v7, 0x7fffffff

    const/4 v10, 0x0

    invoke-direct {v6, v7, v10, v2}, Lcom/google/android/apps/gmm/navigation/i/z;-><init>(ILcom/google/android/apps/gmm/navigation/i/ab;Lcom/google/android/apps/gmm/navigation/i/e;)V

    .line 178
    iget-object v7, v2, Lcom/google/android/apps/gmm/navigation/i/e;->c:Lcom/google/android/apps/gmm/map/u/a/e;

    invoke-virtual {v9, v7, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 179
    iget v6, v2, Lcom/google/android/apps/gmm/navigation/i/e;->h:I

    if-ge v6, v3, :cond_a

    .line 180
    iget-object v3, v2, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 181
    iget v2, v2, Lcom/google/android/apps/gmm/navigation/i/e;->h:I

    :goto_2
    move-object v4, v3

    move v3, v2

    .line 183
    goto :goto_1

    .line 184
    :cond_1
    if-nez v4, :cond_2

    .line 185
    const/4 v2, 0x0

    .line 257
    :goto_3
    return-object v2

    .line 189
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/x;->b:Lcom/google/android/apps/gmm/shared/net/a/n;

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/net/a/n;->a:Lcom/google/r/b/a/qz;

    iget v2, v2, Lcom/google/r/b/a/qz;->m:I

    int-to-double v2, v2

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v6

    mul-double v10, v2, v6

    .line 191
    const/4 v3, 0x0

    .line 197
    const v2, 0x7fffffff

    move v5, v2

    move-object v6, v3

    .line 199
    :cond_3
    invoke-virtual {v8}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 200
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/i/x;->a:Z

    if-nez v2, :cond_5

    .line 203
    const/4 v6, 0x0

    .line 257
    :cond_4
    if-eqz v6, :cond_9

    invoke-static {v9, v6}, Lcom/google/android/apps/gmm/navigation/i/x;->a(Ljava/util/HashMap;Lcom/google/android/apps/gmm/navigation/i/e;)Lcom/google/android/apps/gmm/navigation/i/aa;

    move-result-object v2

    goto :goto_3

    .line 208
    :cond_5
    invoke-virtual {v8}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/navigation/i/ab;

    .line 209
    iget-boolean v2, v3, Lcom/google/android/apps/gmm/navigation/i/ab;->g:Z

    if-nez v2, :cond_3

    .line 210
    const/4 v2, 0x1

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/navigation/i/ab;->g:Z

    .line 214
    instance-of v2, v3, Lcom/google/android/apps/gmm/navigation/i/z;

    if-eqz v2, :cond_6

    move-object v2, v3

    .line 215
    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/z;

    .line 216
    iget-object v7, v2, Lcom/google/android/apps/gmm/navigation/i/z;->a:Lcom/google/android/apps/gmm/navigation/i/e;

    iget-boolean v7, v7, Lcom/google/android/apps/gmm/navigation/i/e;->a:Z

    if-nez v7, :cond_6

    .line 217
    iget v7, v3, Lcom/google/android/apps/gmm/navigation/i/ab;->c:I

    iget-object v12, v2, Lcom/google/android/apps/gmm/navigation/i/z;->a:Lcom/google/android/apps/gmm/navigation/i/e;

    iget v12, v12, Lcom/google/android/apps/gmm/navigation/i/e;->h:I

    add-int/2addr v7, v12

    .line 218
    if-ge v7, v5, :cond_6

    .line 222
    iget-object v6, v2, Lcom/google/android/apps/gmm/navigation/i/z;->a:Lcom/google/android/apps/gmm/navigation/i/e;

    move v5, v7

    .line 228
    :cond_6
    iget-object v12, v3, Lcom/google/android/apps/gmm/navigation/i/ab;->b:Lcom/google/android/apps/gmm/map/u/a/e;

    .line 229
    iget-object v2, v12, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    array-length v13, v2

    .line 230
    const/4 v2, 0x0

    move v7, v2

    :goto_4
    if-ge v7, v13, :cond_3

    .line 231
    iget-object v2, v12, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    aget-object v14, v2, v7

    .line 232
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v14, v0, v1}, Lcom/google/android/apps/gmm/map/u/a/a;->a(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;)Lcom/google/android/apps/gmm/map/u/a/e;

    move-result-object v15

    .line 233
    if-eqz v15, :cond_8

    .line 234
    invoke-virtual {v9, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/navigation/i/ab;

    .line 237
    if-nez v2, :cond_7

    .line 238
    new-instance v2, Lcom/google/android/apps/gmm/navigation/i/ab;

    const v16, 0x7fffffff

    .line 239
    invoke-virtual {v15}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v17

    move/from16 v0, v17

    float-to-double v0, v0

    move-wide/from16 v18, v0

    div-double v18, v18, v10

    move-wide/from16 v0, v18

    double-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-direct {v2, v0, v15, v3, v1}, Lcom/google/android/apps/gmm/navigation/i/ab;-><init>(ILcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/navigation/i/ab;I)V

    .line 240
    invoke-virtual {v9, v15, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 245
    :cond_7
    iget v15, v3, Lcom/google/android/apps/gmm/navigation/i/ab;->c:I

    iget v14, v14, Lcom/google/android/apps/gmm/map/u/a/a;->a:I

    add-int/2addr v14, v15

    .line 246
    iget v15, v2, Lcom/google/android/apps/gmm/navigation/i/ab;->c:I

    if-ge v14, v15, :cond_8

    .line 248
    iput v14, v2, Lcom/google/android/apps/gmm/navigation/i/ab;->c:I

    .line 249
    iget v15, v2, Lcom/google/android/apps/gmm/navigation/i/ab;->d:I

    add-int/2addr v14, v15

    iput v14, v2, Lcom/google/android/apps/gmm/navigation/i/ab;->e:I

    .line 250
    iput-object v3, v2, Lcom/google/android/apps/gmm/navigation/i/ab;->f:Lcom/google/android/apps/gmm/navigation/i/ab;

    .line 251
    iget v14, v2, Lcom/google/android/apps/gmm/navigation/i/ab;->c:I

    if-gt v14, v5, :cond_8

    .line 252
    invoke-virtual {v8, v2}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 230
    :cond_8
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_4

    .line 257
    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_3

    :cond_a
    move v2, v3

    move-object v3, v4

    goto/16 :goto_2
.end method
