.class public Lcom/google/android/apps/gmm/navigation/util/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Ljava/lang/CharSequence;

.field public b:Ljava/lang/CharSequence;

.field public c:Ljava/lang/CharSequence;

.field public d:Ljava/lang/CharSequence;

.field public e:Ljava/lang/CharSequence;

.field public f:Ljava/lang/CharSequence;

.field public g:Ljava/lang/CharSequence;

.field public h:Ljava/lang/CharSequence;

.field public i:Ljava/lang/CharSequence;

.field public j:Ljava/lang/CharSequence;

.field private k:Ljava/lang/CharSequence;

.field private final l:Lcom/google/android/apps/gmm/shared/c/c/c;

.field private final m:Landroid/content/Context;

.field private final n:Lcom/google/android/apps/gmm/shared/c/c/e;

.field private final o:Lcom/google/android/apps/gmm/shared/c/c/j;

.field private final p:Lcom/google/android/apps/gmm/navigation/navui/ay;

.field private final q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    .line 59
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    .line 60
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->l:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 61
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/e;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/shared/c/c/e;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    .line 62
    new-instance v1, Lcom/google/android/apps/gmm/shared/c/c/j;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/shared/c/c/j;-><init>()V

    iget-object v2, v1, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v3, Landroid/text/style/StyleSpan;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->o:Lcom/google/android/apps/gmm/shared/c/c/j;

    .line 63
    new-instance v1, Lcom/google/android/apps/gmm/navigation/navui/ay;

    .line 64
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v2

    .line 65
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i()Lcom/google/android/apps/gmm/shared/c/c/c;

    move-result-object v3

    .line 66
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/c/c;Lcom/google/android/apps/gmm/map/internal/d/c/a/g;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->p:Lcom/google/android/apps/gmm/navigation/navui/ay;

    .line 67
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/d;->E:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->q:I

    .line 68
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/util/b;->a()V

    .line 69
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Ljava/lang/CharSequence;
    .locals 16

    .prologue
    .line 173
    const/4 v1, 0x0

    .line 174
    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Lcom/google/android/apps/gmm/navigation/navui/az;

    move-result-object v15

    .line 175
    iget-object v1, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->n:Landroid/text/Spanned;

    .line 192
    :goto_0
    return-object v1

    .line 178
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/util/b;->p:Lcom/google/android/apps/gmm/navigation/navui/ay;

    iget-object v2, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->a:Ljava/util/Collection;

    const/4 v3, 0x1

    const v4, 0x7fffffff

    const/4 v5, 0x0

    iget v6, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->c:I

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/gmm/navigation/util/b;->q:I

    const/4 v9, 0x0

    const v10, 0x3f19999a    # 0.6f

    const v11, 0x3f19999a    # 0.6f

    const/high16 v12, 0x3f400000    # 0.75f

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v13}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;IILandroid/text/TextPaint;IZIZFFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Ljava/lang/CharSequence;

    move-result-object v14

    .line 182
    if-eqz p2, :cond_1

    move-object v1, v14

    .line 183
    goto :goto_0

    .line 185
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/util/b;->p:Lcom/google/android/apps/gmm/navigation/navui/ay;

    iget-object v2, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->b:Ljava/util/Collection;

    const/4 v3, 0x1

    const v4, 0x7fffffff

    const/4 v5, 0x0

    iget v6, v15, Lcom/google/android/apps/gmm/navigation/navui/az;->d:I

    const/4 v7, 0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/apps/gmm/navigation/util/b;->q:I

    const/4 v9, 0x0

    const v10, 0x3f19999a    # 0.6f

    const v11, 0x3f19999a    # 0.6f

    const/high16 v12, 0x3f400000    # 0.75f

    const/4 v13, 0x0

    invoke-virtual/range {v1 .. v13}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Ljava/util/Collection;IILandroid/text/TextPaint;IZIZFFFLcom/google/android/apps/gmm/map/internal/d/c/b/f;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 189
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v1, v14

    .line 190
    goto :goto_0

    .line 192
    :cond_2
    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object v14, v2, v3

    const/4 v3, 0x1

    const-string v4, " "

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v1, v2, v3

    invoke-static {v2}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 288
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->a:Ljava/lang/CharSequence;

    .line 289
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->b:Ljava/lang/CharSequence;

    .line 290
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->e:Ljava/lang/CharSequence;

    .line 291
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->f:Ljava/lang/CharSequence;

    .line 292
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->k:Ljava/lang/CharSequence;

    .line 293
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->c:Ljava/lang/CharSequence;

    .line 294
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->d:Ljava/lang/CharSequence;

    .line 295
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->i:Ljava/lang/CharSequence;

    .line 296
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->j:Ljava/lang/CharSequence;

    .line 297
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/navigation/g/b/f;)V
    .locals 14

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/util/b;->a()V

    .line 74
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v9, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 75
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget v10, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    .line 76
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    .line 77
    const/4 v0, -0x1

    if-eq v10, v0, :cond_3

    const/4 v0, 0x1

    move v8, v0

    .line 78
    :goto_0
    const/4 v0, -0x1

    if-eq v1, v0, :cond_4

    const/4 v0, 0x1

    move v7, v0

    .line 80
    :goto_1
    if-eqz v8, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    int-to-long v2, v10

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 82
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-virtual {v4, v12, v13}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 81
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    sget v3, Lcom/google/android/apps/gmm/l;->gr:I

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v2, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v2

    const-string v3, "%s"

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/util/b;->f:Ljava/lang/CharSequence;

    .line 88
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    sget v3, Lcom/google/android/apps/gmm/l;->gq:I

    new-instance v4, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v5, v2, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v5, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v2, v3}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    .line 89
    new-instance v6, Lcom/google/android/apps/gmm/shared/c/c/i;

    invoke-direct {v6, v5, v0}, Lcom/google/android/apps/gmm/shared/c/c/i;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/Object;)V

    iget-object v0, v6, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    iget-object v5, v0, Lcom/google/android/apps/gmm/shared/c/c/j;->a:Ljava/util/ArrayList;

    new-instance v11, Landroid/text/style/StyleSpan;

    const/4 v12, 0x1

    invoke-direct {v11, v12}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object v0, v6, Lcom/google/android/apps/gmm/shared/c/c/i;->c:Lcom/google/android/apps/gmm/shared/c/c/j;

    aput-object v6, v2, v3

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 90
    const-string v2, "%s"

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->k:Ljava/lang/CharSequence;

    .line 93
    :cond_0
    if-eqz v7, :cond_1

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->l:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 96
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    const/4 v3, 0x1

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/util/b;->o:Lcom/google/android/apps/gmm/shared/c/c/j;

    const/4 v6, 0x0

    .line 95
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(ILcom/google/maps/g/a/al;ZZLcom/google/android/apps/gmm/shared/c/c/j;Lcom/google/android/apps/gmm/shared/c/c/j;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->b:Ljava/lang/CharSequence;

    .line 103
    :cond_1
    if-eqz v8, :cond_2

    if-eqz v7, :cond_2

    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/c/m;->b:Lcom/google/android/apps/gmm/shared/c/c/m;

    .line 106
    invoke-static {v0, v10, v1}, Lcom/google/android/apps/gmm/shared/c/c/k;->a(Landroid/content/Context;ILcom/google/android/apps/gmm/shared/c/c/m;)Landroid/text/Spanned;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    sget v2, Lcom/google/android/apps/gmm/l;->fY:I

    new-instance v3, Lcom/google/android/apps/gmm/shared/c/c/h;

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/c/c/e;->a:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v1, v2}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object v0, v1, v2

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/util/b;->b:Ljava/lang/CharSequence;

    aput-object v2, v1, v0

    .line 108
    invoke-virtual {v3, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 109
    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->e:Ljava/lang/CharSequence;

    .line 112
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/b;->c:Z

    if-nez v0, :cond_5

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->dn:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    .line 164
    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_10

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    const/4 v2, 0x1

    if-le v0, v2, :cond_10

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v2, 0x1

    aget-object v0, v0, v2

    :goto_3
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ap;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->h:Ljava/lang/CharSequence;

    .line 165
    return-void

    .line 77
    :cond_3
    const/4 v0, 0x0

    move v8, v0

    goto/16 :goto_0

    .line 78
    :cond_4
    const/4 v0, 0x0

    move v7, v0

    goto/16 :goto_1

    .line 114
    :cond_5
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/f;->f:Z

    if-eqz v0, :cond_a

    .line 115
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_6

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_6

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    :goto_4
    if-eqz v0, :cond_7

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_9

    .line 116
    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v0, :cond_8

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_8

    iget-object v0, v9, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    :goto_6
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v0

    .line 117
    :goto_7
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    goto :goto_2

    .line 115
    :cond_6
    const/4 v0, 0x0

    goto :goto_4

    :cond_7
    const/4 v0, 0x0

    goto :goto_5

    .line 116
    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->cq:I

    .line 117
    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 118
    :cond_a
    invoke-static {p1}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->a(Lcom/google/android/apps/gmm/navigation/g/b/f;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/apps/gmm/navigation/navui/c/a;->b(Lcom/google/android/apps/gmm/navigation/g/b/f;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    goto :goto_2

    .line 120
    :cond_b
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-nez v0, :cond_c

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->dk:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    goto/16 :goto_2

    .line 130
    :cond_c
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget-object v3, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 131
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v1, v0

    iget v2, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->d:I

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/util/b;->l:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 133
    iget-object v4, v9, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    const/high16 v5, 0x3f400000    # 0.75f

    .line 132
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/shared/c/c/c;ILcom/google/android/apps/gmm/map/r/a/ag;Lcom/google/maps/g/a/al;F)Landroid/text/Spannable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->m:Landroid/content/Context;

    const/high16 v1, 0x3f400000    # 0.75f

    invoke-static {v0, v3, v1}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/map/r/a/ag;F)Landroid/text/Spannable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->c:Ljava/lang/CharSequence;

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->l:Lcom/google/android/apps/gmm/shared/c/c/c;

    .line 136
    iget-object v1, v9, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 135
    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/navigation/navui/ay;->a(Lcom/google/android/apps/gmm/shared/c/c/c;ILcom/google/maps/g/a/al;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->d:Ljava/lang/CharSequence;

    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/gmm/navigation/util/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->i:Ljava/lang/CharSequence;

    .line 138
    const/4 v0, 0x1

    invoke-direct {p0, v3, v0}, Lcom/google/android/apps/gmm/navigation/util/b;->a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->j:Ljava/lang/CharSequence;

    .line 144
    if-eqz v7, :cond_e

    .line 145
    if-eqz v8, :cond_d

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    const-string v1, "{0}\n\n{1}\n{2}"

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/b;->e:Ljava/lang/CharSequence;

    aput-object v3, v0, v1

    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/b;->k:Ljava/lang/CharSequence;

    aput-object v3, v0, v1

    .line 147
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 148
    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->a:Ljava/lang/CharSequence;

    goto/16 :goto_2

    .line 150
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    const-string v1, "{0}\n\n{1}"

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/b;->b:Ljava/lang/CharSequence;

    aput-object v3, v0, v1

    .line 151
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 152
    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->a:Ljava/lang/CharSequence;

    goto/16 :goto_2

    .line 155
    :cond_e
    if-eqz v8, :cond_f

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->n:Lcom/google/android/apps/gmm/shared/c/c/e;

    const-string v1, "{0}\n\n{1}"

    new-instance v2, Lcom/google/android/apps/gmm/shared/c/c/h;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/h;-><init>(Lcom/google/android/apps/gmm/shared/c/c/e;Ljava/lang/CharSequence;)V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    aput-object v3, v0, v1

    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/util/b;->k:Ljava/lang/CharSequence;

    aput-object v3, v0, v1

    .line 157
    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/shared/c/c/h;->a([Ljava/lang/Object;)Lcom/google/android/apps/gmm/shared/c/c/h;

    move-result-object v0

    .line 158
    const-string v1, "%s"

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/c/i;->a(Ljava/lang/String;)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->a:Ljava/lang/CharSequence;

    goto/16 :goto_2

    .line 160
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->g:Ljava/lang/CharSequence;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/util/b;->a:Ljava/lang/CharSequence;

    goto/16 :goto_2

    .line 164
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_3
.end method
