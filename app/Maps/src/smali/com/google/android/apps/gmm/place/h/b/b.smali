.class public Lcom/google/android/apps/gmm/place/h/b/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/h/b/c;


# instance fields
.field private a:Lcom/google/maps/g/lm;

.field private b:Landroid/content/Context;

.field private c:Lcom/google/android/apps/gmm/z/b/l;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/lm;Lcom/google/android/apps/gmm/z/b/l;)V
    .locals 4

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/h/b/b;->b:Landroid/content/Context;

    .line 27
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/h/b/b;->a:Lcom/google/maps/g/lm;

    .line 28
    invoke-static {p3}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->dC:Lcom/google/b/f/t;

    aput-object v3, v1, v2

    .line 29
    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 30
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/b;->c:Lcom/google/android/apps/gmm/z/b/l;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 35
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/h/b/b;->a:Lcom/google/maps/g/lm;

    iget-object v0, v2, Lcom/google/maps/g/lm;->c:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/maps/g/lm;->c:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 40
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/h/b/b;->a:Lcom/google/maps/g/lm;

    iget-object v0, v2, Lcom/google/maps/g/lm;->b:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/maps/g/lm;->b:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final c()Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/b;->a:Lcom/google/maps/g/lm;

    iget-object v0, v0, Lcom/google/maps/g/lm;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/b;->a:Lcom/google/maps/g/lm;

    iget-object v0, v0, Lcom/google/maps/g/lm;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/h/b/b;->b:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 49
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/h/b/b;->c:Lcom/google/android/apps/gmm/z/b/l;

    return-object v0
.end method
