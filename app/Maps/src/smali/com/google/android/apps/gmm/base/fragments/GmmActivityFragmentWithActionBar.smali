.class public Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

.field public b:Lcom/google/android/apps/gmm/base/l/ao;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/base/views/c/g;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    iput-object p1, v0, Lcom/google/android/apps/gmm/base/l/ao;->a:Lcom/google/android/apps/gmm/base/views/c/g;

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    instance-of v0, v0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    check-cast v0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->b:Landroid/view/View;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;->a:Lcom/google/android/apps/gmm/base/l/a/ab;

    invoke-static {v1, v0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 78
    :cond_0
    return-void
.end method

.method public b()Lcom/google/android/apps/gmm/base/views/c/g;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 67
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    new-instance v1, Lcom/google/android/apps/gmm/base/l/ao;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/l/ao;-><init>(Lcom/google/android/apps/gmm/base/views/c/g;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    .line 53
    new-instance v0, Lcom/google/android/apps/gmm/base/views/QuHeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->b:Lcom/google/android/apps/gmm/base/l/ao;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/QuHeaderView;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/l/a/ab;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    .line 58
    :goto_0
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 59
    return-void

    .line 55
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setFragment(Landroid/app/Fragment;)V

    goto :goto_0
.end method
