.class public Lcom/google/android/apps/gmm/base/k/aq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/libraries/curvular/aw;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/libraries/curvular/aq;

.field private c:Landroid/graphics/drawable/Drawable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private d:I


# direct methods
.method public constructor <init>(ILcom/google/android/libraries/curvular/aq;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->d:I

    .line 54
    iput p1, p0, Lcom/google/android/apps/gmm/base/k/aq;->a:I

    .line 55
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/aq;->b:Lcom/google/android/libraries/curvular/aq;

    .line 56
    return-void
.end method


# virtual methods
.method public final d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->c:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->b:Lcom/google/android/libraries/curvular/aq;

    instance-of v0, v0, Lcom/google/android/libraries/curvular/ax;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->d:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/aq;->b:Lcom/google/android/libraries/curvular/aq;

    .line 74
    invoke-interface {v1, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->b:Lcom/google/android/libraries/curvular/aq;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->d:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/aq;->a:I

    iget v3, p0, Lcom/google/android/apps/gmm/base/k/aq;->d:I

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/c/a;->A()Lcom/google/android/apps/gmm/map/c/c;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    new-instance v4, Lcom/google/android/apps/gmm/util/n;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/apps/gmm/util/n;-><init>(Landroid/content/res/Resources;II)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/apps/gmm/map/c/c;->a(Landroid/content/res/Resources;IILcom/google/b/a/bw;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->c:Landroid/graphics/drawable/Drawable;

    .line 77
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->c:Landroid/graphics/drawable/Drawable;

    return-object v0

    .line 75
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/util/l;->a(Landroid/content/res/Resources;II)Lcom/a/a/b;

    move-result-object v1

    new-instance v0, Landroid/graphics/drawable/PictureDrawable;

    iget-object v1, v1, Lcom/a/a/b;->a:Landroid/graphics/Picture;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/PictureDrawable;-><init>(Landroid/graphics/Picture;)V

    goto :goto_1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 60
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/k/aq;

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/base/k/aq;->a:I

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/k/aq;

    iget v0, v0, Lcom/google/android/apps/gmm/base/k/aq;->a:I

    if-ne v1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/aq;->b:Lcom/google/android/libraries/curvular/aq;

    check-cast p1, Lcom/google/android/apps/gmm/base/k/aq;

    iget-object v1, p1, Lcom/google/android/apps/gmm/base/k/aq;->b:Lcom/google/android/libraries/curvular/aq;

    .line 62
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 67
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/aq;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/aq;->b:Lcom/google/android/libraries/curvular/aq;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
