.class public Lcom/google/android/apps/gmm/base/k/l;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:F

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 288
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/k/l;->a:F

    .line 289
    iput v1, p0, Lcom/google/android/apps/gmm/base/k/l;->b:I

    .line 290
    iput v1, p0, Lcom/google/android/apps/gmm/base/k/l;->c:I

    .line 291
    return-void
.end method

.method public constructor <init>(F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 299
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 300
    :cond_1
    iput p1, p0, Lcom/google/android/apps/gmm/base/k/l;->a:F

    .line 301
    iput v1, p0, Lcom/google/android/apps/gmm/base/k/l;->b:I

    .line 302
    iput v1, p0, Lcom/google/android/apps/gmm/base/k/l;->c:I

    .line 303
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/base/k/l;->a:F

    .line 313
    iput p1, p0, Lcom/google/android/apps/gmm/base/k/l;->b:I

    .line 314
    iput p2, p0, Lcom/google/android/apps/gmm/base/k/l;->c:I

    .line 315
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 326
    if-ne p1, p0, :cond_1

    .line 334
    :cond_0
    :goto_0
    return v0

    .line 328
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/base/k/l;

    if-eqz v2, :cond_3

    .line 329
    check-cast p1, Lcom/google/android/apps/gmm/base/k/l;

    .line 330
    iget v2, p0, Lcom/google/android/apps/gmm/base/k/l;->a:F

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/l;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/l;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/l;->b:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/l;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/base/k/l;->c:I

    if-eq v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 334
    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 339
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/l;->a:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/l;->b:I

    .line 340
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/l;->c:I

    .line 341
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 339
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
