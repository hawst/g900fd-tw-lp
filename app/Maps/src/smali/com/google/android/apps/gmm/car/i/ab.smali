.class public Lcom/google/android/apps/gmm/car/i/ab;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Lcom/google/android/apps/gmm/base/a;

.field public final c:Lcom/google/android/apps/gmm/car/bg;

.field final d:Lcom/google/android/apps/gmm/car/i/h;

.field public final e:Lcom/google/android/apps/gmm/car/w;

.field f:Z

.field public g:Z

.field public h:Z

.field i:Lcom/google/android/apps/gmm/map/r/b/a;

.field public j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/car/bm;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/lang/Object;

.field private final l:Landroid/content/Context;

.field private final m:Lcom/google/android/apps/gmm/shared/c/f;

.field private n:J

.field private final o:Lcom/google/android/apps/gmm/car/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/google/android/apps/gmm/car/i/ab;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/bg;Lcom/google/android/apps/gmm/car/w;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 2

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    new-instance v0, Lcom/google/android/apps/gmm/car/i/ad;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/ad;-><init>(Lcom/google/android/apps/gmm/car/i/ab;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/ab;->o:Lcom/google/android/apps/gmm/car/ac;

    .line 308
    new-instance v0, Lcom/google/android/apps/gmm/car/i/ae;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/i/ae;-><init>(Lcom/google/android/apps/gmm/car/i/ab;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/ab;->k:Ljava/lang/Object;

    .line 122
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    move-object v0, p1

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/ab;->l:Landroid/content/Context;

    .line 123
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/base/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/car/i/ab;->b:Lcom/google/android/apps/gmm/base/a;

    .line 124
    if-nez p3, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/bg;

    iput-object p3, p0, Lcom/google/android/apps/gmm/car/i/ab;->c:Lcom/google/android/apps/gmm/car/bg;

    .line 125
    if-nez p4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p4, Lcom/google/android/apps/gmm/car/w;

    iput-object p4, p0, Lcom/google/android/apps/gmm/car/i/ab;->e:Lcom/google/android/apps/gmm/car/w;

    .line 126
    if-nez p5, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast p5, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p5, p0, Lcom/google/android/apps/gmm/car/i/ab;->m:Lcom/google/android/apps/gmm/shared/c/f;

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/car/i/h;

    new-instance v1, Lcom/google/android/apps/gmm/car/i/ac;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/i/ac;-><init>(Lcom/google/android/apps/gmm/car/i/ab;)V

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/car/i/h;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/car/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/i/ab;->d:Lcom/google/android/apps/gmm/car/i/h;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ab;->d:Lcom/google/android/apps/gmm/car/i/h;

    const v1, 0x44bb8000    # 1500.0f

    iput v1, v0, Lcom/google/android/apps/gmm/car/i/h;->a:F

    .line 135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 112
    new-instance v3, Lcom/google/android/apps/gmm/car/bg;

    new-instance v0, Lcom/google/android/apps/gmm/startpage/w;

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/apps/gmm/startpage/w;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)V

    invoke-direct {v3, p2, v1, v0}, Lcom/google/android/apps/gmm/car/bg;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/startpage/w;)V

    new-instance v4, Lcom/google/android/apps/gmm/car/w;

    new-instance v0, Lcom/google/android/apps/gmm/car/z;

    invoke-direct {v0, p2, v1, p1}, Lcom/google/android/apps/gmm/car/z;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;)V

    invoke-direct {v4, p2, v0}, Lcom/google/android/apps/gmm/car/w;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/y;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/i/ab;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/bg;Lcom/google/android/apps/gmm/car/w;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 116
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/i/ab;)V
    .locals 20

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v4, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v5, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v7, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/car/bm;

    iget-object v8, v2, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    aput-object v8, v4, v3

    iget-object v8, v2, Lcom/google/android/apps/gmm/car/bm;->c:Ljava/lang/String;

    aput-object v8, v5, v3

    const-string v8, "%f,%f"

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v2, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v11, v11, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v12, v11, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, v2, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v11, v11, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v12, v11, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/bm;->f:Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v2, :cond_0

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_0
    sget-object v8, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    const/4 v9, 0x0

    new-instance v10, Lcom/google/android/apps/gmm/l/f;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/l/f;-><init>()V

    const/4 v11, 0x1

    new-array v11, v11, [Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v12, 0x0

    aput-object v2, v11, v12

    iput-object v11, v10, Lcom/google/android/apps/gmm/l/f;->a:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v8, v10, Lcom/google/android/apps/gmm/l/f;->b:Lcom/google/maps/g/a/hm;

    iput-object v9, v10, Lcom/google/android/apps/gmm/l/f;->c:Ljava/lang/String;

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v8, "google.navigation"

    invoke-virtual {v2, v8}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v8, "/"

    invoke-virtual {v2, v8}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v8, ""

    invoke-virtual {v2, v8}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v8, ""

    invoke-virtual {v2, v8}, Landroid/net/Uri$Builder;->fragment(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v8

    const/4 v2, 0x0

    :goto_1
    iget-object v9, v10, Lcom/google/android/apps/gmm/l/f;->a:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v9, v9

    add-int/lit8 v9, v9, -0x1

    if-ge v2, v9, :cond_2

    iget-object v9, v10, Lcom/google/android/apps/gmm/l/f;->a:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v9, v9, v2

    iget-object v9, v9, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v9, :cond_1

    const-string v11, "altvia"

    const-string v12, "%.6f,%.6f"

    const/4 v13, 0x2

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    iget-wide v0, v9, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v15

    aput-object v15, v13, v14

    const/4 v14, 0x1

    iget-wide v0, v9, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    aput-object v9, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v11, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    iget-object v9, v10, Lcom/google/android/apps/gmm/l/f;->a:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v9, v9

    if-ge v2, v9, :cond_6

    iget-object v9, v10, Lcom/google/android/apps/gmm/l/f;->a:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v2, v9, v2

    const-string v9, "q"

    const-string v11, "ll"

    const-string v12, "title"

    const-string v13, "token"

    iget-object v14, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v14, :cond_3

    const-string v15, "%.6f,%.6f"

    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    iget-wide v0, v14, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v18

    aput-object v18, v16, v17

    const/16 v17, 0x1

    iget-wide v0, v14, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v14

    aput-object v14, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v11, v14}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_3
    iget-object v11, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v11, :cond_4

    iget-object v11, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    invoke-virtual {v8, v9, v11}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_4
    iget-object v9, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    if-eqz v9, :cond_5

    invoke-virtual {v8, v12, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_5
    iget-object v9, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    if-eqz v9, :cond_6

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ap;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    invoke-static {v2}, Lcom/google/android/apps/gmm/l/ac;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v13, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_6
    iget-object v2, v10, Lcom/google/android/apps/gmm/l/f;->b:Lcom/google/maps/g/a/hm;

    sget-object v9, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-eq v2, v9, :cond_7

    const-string v2, "mode"

    iget-object v9, v10, Lcom/google/android/apps/gmm/l/f;->b:Lcom/google/maps/g/a/hm;

    invoke-static {v9}, Lcom/google/android/apps/gmm/l/a/a;->a(Lcom/google/maps/g/a/hm;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v2, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_7
    iget-object v2, v10, Lcom/google/android/apps/gmm/l/f;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, v10, Lcom/google/android/apps/gmm/l/f;->c:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_8

    const-string v2, "entry"

    iget-object v9, v10, Lcom/google/android/apps/gmm/l/f;->c:Ljava/lang/String;

    invoke-virtual {v8, v2, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    :cond_8
    invoke-virtual {v8}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v3

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_0

    :cond_9
    new-instance v8, Landroid/content/Intent;

    invoke-direct {v8}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.projection.gearhead"

    invoke-virtual {v8, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.apps.gmm.car.SuggestedDestinations"

    invoke-virtual {v8, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "NumResults"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v8, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "Titles"

    invoke-virtual {v8, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Subtitles"

    invoke-virtual {v8, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Locations"

    invoke-virtual {v8, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "URLs"

    invoke-virtual {v8, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/bm;->a()Lcom/google/android/apps/gmm/car/bn;

    move-result-object v5

    sget-object v6, Lcom/google/android/apps/gmm/car/bn;->b:Lcom/google/android/apps/gmm/car/bn;

    if-eq v5, v6, :cond_a

    sget-object v3, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Route for \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\' is missing."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v2, 0x1

    :goto_2
    if-nez v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v5, v2, [I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v7, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move v3, v2

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/car/bm;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/bm;->b()I

    move-result v4

    if-nez v4, :cond_b

    sget-object v4, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/bm;->b:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x2a

    invoke-direct {v4, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "No trips for \'"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "\', not including route info."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    :cond_b
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/car/bm;->a(I)Lcom/google/android/apps/gmm/map/r/a/ao;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/gmm/directions/f/d/h;->e(Lcom/google/android/apps/gmm/map/r/a/ao;)Lcom/google/maps/g/a/be;

    move-result-object v2

    if-eqz v2, :cond_c

    iget v2, v2, Lcom/google/maps/g/a/be;->b:I

    aput v2, v5, v3

    :cond_c
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v2, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    if-nez v4, :cond_10

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v2

    move-object v4, v2

    :goto_5
    if-eqz v4, :cond_f

    sget-object v10, Lcom/google/android/apps/gmm/car/i/af;->a:[I

    iget v2, v4, Lcom/google/maps/g/a/fw;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v2

    if-nez v2, :cond_d

    sget-object v2, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    :cond_d
    invoke-virtual {v2}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v2

    aget v2, v10, v2

    packed-switch v2, :pswitch_data_0

    sget-object v10, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    iget v2, v4, Lcom/google/maps/g/a/fw;->d:I

    invoke-static {v2}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v2

    if-nez v2, :cond_e

    sget-object v2, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    :cond_e
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x17

    invoke-direct {v4, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Unknown DelayCategory: "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v10, v2, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_f
    :goto_6
    invoke-virtual {v9}, Lcom/google/android/apps/gmm/map/r/a/ao;->a()Lcom/google/android/apps/gmm/map/r/a/a;

    move-result-object v2

    if-eqz v2, :cond_12

    iget-object v10, v2, Lcom/google/android/apps/gmm/map/r/a/a;->b:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    new-array v11, v2, [D

    const/4 v2, 0x0

    move v4, v2

    :goto_7
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_11

    shl-int/lit8 v12, v4, 0x1

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v14, v2

    const-wide v16, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double v14, v14, v16

    const-wide/high16 v16, 0x4000000000000000L    # 2.0

    invoke-static {v14, v15}, Ljava/lang/Math;->exp(D)D

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Math;->atan(D)D

    move-result-wide v14

    const-wide v18, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double v14, v14, v18

    mul-double v14, v14, v16

    const-wide v16, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double v14, v14, v16

    aput-wide v14, v11, v12

    shl-int/lit8 v2, v4, 0x1

    add-int/lit8 v12, v2, 0x1

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->e()D

    move-result-wide v14

    aput-wide v14, v11, v12

    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_7

    :cond_10
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    move-object v4, v2

    goto/16 :goto_5

    :pswitch_0
    const-string v2, "LIGHT"

    aput-object v2, v6, v3

    goto :goto_6

    :pswitch_1
    const-string v2, "MEDIUM"

    aput-object v2, v6, v3

    goto :goto_6

    :pswitch_2
    const-string v2, "HEAVY"

    aput-object v2, v6, v3

    goto :goto_6

    :pswitch_3
    const/4 v2, 0x0

    aput-object v2, v6, v3

    goto :goto_6

    :cond_11
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x14

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Waypoints"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[D)Landroid/content/Intent;

    :cond_12
    iget-object v2, v9, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v4, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v4, :cond_13

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v2

    :goto_8
    invoke-virtual {v2}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v3

    goto/16 :goto_4

    :cond_13
    iget-object v2, v2, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_8

    :cond_14
    const-string v2, "Vias"

    invoke-virtual {v8, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "Times"

    invoke-virtual {v8, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    const-string v2, "Traffic"

    invoke-virtual {v8, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/i/ab;->l:Landroid/content/Context;

    invoke-virtual {v2, v8}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_16
    move v2, v3

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/car/i/ab;Z)Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/ab;->m:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/apps/gmm/car/i/ab;->n:J

    sub-long v4, v2, v4

    if-nez p1, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    const-wide/32 v6, 0xea60

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    sget-object v1, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x42

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Fetched directions "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms ago, not fetching again."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/android/apps/gmm/car/i/ab;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iput-wide v2, p0, Lcom/google/android/apps/gmm/car/i/ab;->n:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/bm;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/bm;->e:Lcom/google/android/apps/gmm/directions/a/c;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/i/ab;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/car/bm;

    iget-object v2, p0, Lcom/google/android/apps/gmm/car/i/ab;->e:Lcom/google/android/apps/gmm/car/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/i/ab;->o:Lcom/google/android/apps/gmm/car/ac;

    invoke-virtual {v2, v0, v3}, Lcom/google/android/apps/gmm/car/w;->a(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ac;)V

    goto :goto_2

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
