.class public Lcom/google/android/apps/gmm/base/views/ay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/l;
.implements Lcom/google/android/apps/gmm/base/views/q;


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/view/View;

.field private c:Ljava/lang/Runnable;

.field private d:Ljava/lang/Runnable;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Runnable;Ljava/lang/Runnable;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 44
    iput-object p6, p0, Lcom/google/android/apps/gmm/base/views/ay;->c:Ljava/lang/Runnable;

    .line 45
    iput-object p7, p0, Lcom/google/android/apps/gmm/base/views/ay;->d:Ljava/lang/Runnable;

    .line 46
    iput-object p2, p0, Lcom/google/android/apps/gmm/base/views/ay;->e:Ljava/lang/String;

    .line 47
    iput-object p3, p0, Lcom/google/android/apps/gmm/base/views/ay;->f:Ljava/lang/String;

    .line 48
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/views/ay;->g:Ljava/lang/String;

    .line 49
    iput-object p5, p0, Lcom/google/android/apps/gmm/base/views/ay;->h:Ljava/lang/String;

    .line 50
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v1, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 51
    const-class v1, Lcom/google/android/apps/gmm/base/f/x;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/apps/gmm/base/views/ay;->a:Landroid/view/View;

    .line 52
    const-class v1, Lcom/google/android/apps/gmm/base/f/y;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    .line 53
    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->b:Landroid/view/View;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->a:Landroid/view/View;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/views/ay;->j:Ljava/lang/String;

    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->i:Z

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->a:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 73
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/ay;->i:Z

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->a:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 85
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 89
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/base/views/ay;->k:Z

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->a:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->b:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 92
    return-void
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 138
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/ay;->i:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/ay;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_3
    move v2, v0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 102
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 118
    const/4 v0, 0x0

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()Landroid/view/View;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->b:Landroid/view/View;

    return-object v0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->a:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->b:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 66
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    .line 77
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->j:Ljava/lang/String;

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/ay;->a:Landroid/view/View;

    invoke-static {v0, p0}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 79
    return-void
.end method
