.class public Lcom/google/android/apps/gmm/map/f/q;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/f;


# instance fields
.field public a:Lcom/google/android/apps/gmm/v/g;

.field final b:Ljava/lang/Object;

.field public c:Lcom/google/android/apps/gmm/map/f/d;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final synthetic d:Lcom/google/android/apps/gmm/map/f/o;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 1

    .prologue
    .line 1958
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1964
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->b:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/v/h;
    .locals 1

    .prologue
    .line 2041
    sget-object v0, Lcom/google/android/apps/gmm/v/h;->c:Lcom/google/android/apps/gmm/v/h;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/v/g;)V
    .locals 3

    .prologue
    .line 1976
    if-nez p1, :cond_0

    .line 1987
    :goto_0
    return-void

    .line 1979
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    .line 1980
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/q;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1981
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->c:Lcom/google/android/apps/gmm/map/f/d;

    if-eqz v0, :cond_1

    .line 1982
    sget-object v0, Lcom/google/android/apps/gmm/v/ct;->b:Lcom/google/android/apps/gmm/v/ct;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1984
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/o;->e:I

    if-eqz v0, :cond_2

    .line 1985
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 1987
    :cond_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final b(Lcom/google/android/apps/gmm/v/g;)V
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2011
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->C:Lcom/google/android/apps/gmm/map/f/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/f/a;->b()I

    move-result v3

    .line 2012
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(I)V

    .line 2013
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v4, v4, Lcom/google/android/apps/gmm/map/f/o;->C:Lcom/google/android/apps/gmm/map/f/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/f/a;->c()Lcom/google/android/apps/gmm/map/f/a/i;

    move-result-object v4

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/f/o;->D:Lcom/google/android/apps/gmm/map/f/a/i;

    .line 2015
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    if-eqz v0, :cond_0

    .line 2016
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->A:Lcom/google/android/apps/gmm/map/f/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/j;->a()V

    .line 2019
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/f/o;->r:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/f/o;->q:Z

    if-eqz v0, :cond_3

    .line 2020
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-boolean v5, v4, Lcom/google/android/apps/gmm/map/f/o;->q:Z

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/f/o;->d()V

    :cond_2
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/f/o;->n:[F

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/f/o;->a([F)V

    .line 2021
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/f/o;->r:Z

    .line 2024
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    if-eqz v0, :cond_4

    .line 2025
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/f/o;->e()V

    .line 2028
    :cond_4
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    iget-boolean v0, v4, Lcom/google/android/apps/gmm/map/f/o;->s:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_0
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    move v0, v2

    goto :goto_0

    :cond_7
    iget-object v0, v4, Lcom/google/android/apps/gmm/map/f/o;->u:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/f/h;

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v5, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/h;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v5, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/f/h;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/f/o;->l:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v6, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v6, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v5, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v5, v1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iget-object v1, v4, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->f()I

    iget-object v1, v4, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/v/bi;->g()I

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/h;->d:[F

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/f/o;->o:[F

    invoke-static {v1, v2, v5, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/h;->e:[F

    iget-object v6, v4, Lcom/google/android/apps/gmm/map/f/o;->p:[F

    invoke-static {v1, v2, v5, v2, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, v4, Lcom/google/android/apps/gmm/map/f/o;->v:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v4, Lcom/google/android/apps/gmm/map/f/o;->w:Lcom/google/android/apps/gmm/map/f/h;

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/f/o;->w:Lcom/google/android/apps/gmm/map/f/h;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/f/h;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/f/h;->f:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 2030
    :cond_8
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/q;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 2031
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->c:Lcom/google/android/apps/gmm/map/f/d;

    if-eqz v0, :cond_a

    .line 2032
    sget-object v0, Lcom/google/android/apps/gmm/v/ct;->b:Lcom/google/android/apps/gmm/v/ct;

    invoke-interface {p1, p0, v0}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 2036
    :cond_9
    :goto_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 2028
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2033
    :cond_a
    if-eqz v3, :cond_9

    .line 2034
    :try_start_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v2, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v2}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    goto :goto_1

    .line 2036
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1992
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    .line 1994
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/q;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 1995
    :try_start_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/q;->c:Lcom/google/android/apps/gmm/map/f/d;

    if-eqz v2, :cond_0

    .line 1996
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/q;->c:Lcom/google/android/apps/gmm/map/f/d;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/d;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v2

    or-int/2addr v0, v2

    .line 1997
    if-nez v0, :cond_0

    .line 2001
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v3, Lcom/google/android/apps/gmm/v/ct;->b:Lcom/google/android/apps/gmm/v/ct;

    invoke-interface {v2, p0, v3}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 2004
    :cond_0
    monitor-exit v1

    .line 2005
    return v0

    .line 2004
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2070
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    if-eqz v0, :cond_0

    .line 2071
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->d:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->y:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2072
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/q;->a:Lcom/google/android/apps/gmm/v/g;

    sget-object v1, Lcom/google/android/apps/gmm/v/cs;->b:Lcom/google/android/apps/gmm/v/cs;

    invoke-interface {v0, p0, v1}, Lcom/google/android/apps/gmm/v/g;->a(Lcom/google/android/apps/gmm/v/f;Lcom/google/android/apps/gmm/v/cp;)V

    .line 2074
    :cond_0
    return-void
.end method
