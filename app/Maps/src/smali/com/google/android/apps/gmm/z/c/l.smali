.class Lcom/google/android/apps/gmm/z/c/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/gmm/z/c/k;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/z/c/k;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/c/l;->c:Lcom/google/android/apps/gmm/z/c/k;

    iput-object p2, p0, Lcom/google/android/apps/gmm/z/c/l;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/google/android/apps/gmm/z/c/l;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v0, 0x1

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/c/l;->c:Lcom/google/android/apps/gmm/z/c/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/c/l;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/apps/gmm/z/c/l;->b:Ljava/lang/String;

    sget-object v4, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/z/c/k;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v4

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/c/k;->a()Lcom/google/android/apps/gmm/z/c/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    :try_start_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/z/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/a;->c()J

    move-result-wide v8

    sub-long v8, v4, v8

    invoke-static {v6, v3, v0, v8, v9}, Lcom/google/android/apps/gmm/z/c/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/android/apps/gmm/z/b/a;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-object v1, v1, Lcom/google/android/apps/gmm/z/c/k;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/c/d;->d()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    :try_start_1
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    iget-object v0, v1, Lcom/google/android/apps/gmm/z/c/k;->c:Lcom/google/android/apps/gmm/z/c/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/c/d;->d()V

    sget-object v0, Lcom/google/android/apps/gmm/z/c/k;->a:Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2c

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Saved "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " failed events for account "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v1, Lcom/google/android/apps/gmm/z/c/k;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/c/k;->a(Lcom/google/android/apps/gmm/login/a/a;)Ljava/lang/String;

    move-result-object v0

    if-ne v3, v0, :cond_2

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, v1, Lcom/google/android/apps/gmm/z/c/k;->f:Z

    iput-object v3, v1, Lcom/google/android/apps/gmm/z/c/k;->g:Ljava/lang/String;

    monitor-exit v1

    .line 190
    :cond_2
    return-void

    .line 189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
