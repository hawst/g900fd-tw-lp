.class public Lcom/google/android/apps/gmm/base/l/ad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/v;


# static fields
.field private static final a:Lcom/google/android/apps/gmm/base/l/a/p;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/l/a/p;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/apps/gmm/base/l/u;

    new-instance v1, Ljava/lang/Integer;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    new-instance v2, Ljava/lang/Float;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/lang/Float;-><init>(F)V

    sget v3, Lcom/google/android/apps/gmm/f;->fa:I

    .line 22
    invoke-static {v3}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/base/l/u;-><init>(Ljava/lang/Integer;Ljava/lang/Float;Lcom/google/android/libraries/curvular/aw;)V

    sput-object v0, Lcom/google/android/apps/gmm/base/l/ad;->a:Lcom/google/android/apps/gmm/base/l/a/p;

    .line 19
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->b:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Integer;)Lcom/google/android/apps/gmm/base/l/a/p;
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->b:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/l/a/p;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/base/l/ad;->a:Lcom/google/android/apps/gmm/base/l/a/p;

    goto :goto_0
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->e:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Lcom/google/android/apps/gmm/x/o;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v12, 0x5

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 30
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 31
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    :goto_0
    move v2, v3

    move v4, v3

    move v5, v3

    .line 34
    :goto_1
    array-length v7, v1

    if-ge v2, v7, :cond_3

    .line 35
    aget v7, v1, v2

    invoke-static {v7, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 36
    aget v7, v1, v2

    add-int/2addr v5, v7

    .line 34
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 31
    :cond_0
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acq;->g()Lcom/google/r/b/a/acq;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/r/b/a/acq;

    iget-object v1, v1, Lcom/google/r/b/a/acq;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/pf;->d()Lcom/google/maps/g/pf;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/pf;

    iget v2, v1, Lcom/google/maps/g/pf;->a:I

    and-int/lit8 v2, v2, 0x2

    if-ne v2, v5, :cond_1

    move v2, v6

    :goto_2
    if-nez v2, :cond_2

    new-array v1, v3, [I

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    goto :goto_0

    :cond_1
    move v2, v3

    goto :goto_2

    :cond_2
    iget-object v1, v1, Lcom/google/maps/g/pf;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/fq;->d()Lcom/google/maps/g/fq;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/fq;

    new-array v2, v12, [I

    iput-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    iget v4, v1, Lcom/google/maps/g/fq;->b:I

    aput v4, v2, v3

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    iget v4, v1, Lcom/google/maps/g/fq;->c:I

    aput v4, v2, v6

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    iget v4, v1, Lcom/google/maps/g/fq;->d:I

    aput v4, v2, v5

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    const/4 v4, 0x3

    iget v5, v1, Lcom/google/maps/g/fq;->e:I

    aput v5, v2, v4

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    const/4 v4, 0x4

    iget v1, v1, Lcom/google/maps/g/fq;->f:I

    aput v1, v2, v4

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->t:[I

    goto :goto_0

    .line 38
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/ad;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    move v7, v3

    .line 39
    :goto_3
    if-ge v7, v12, :cond_5

    .line 41
    array-length v2, v1

    if-ge v7, v2, :cond_4

    if-eqz v5, :cond_4

    .line 42
    aget v2, v1, v7

    int-to-float v2, v2

    int-to-float v8, v4

    div-float/2addr v2, v8

    .line 46
    :goto_4
    iget-object v8, p0, Lcom/google/android/apps/gmm/base/l/ad;->b:Ljava/util/List;

    new-instance v9, Lcom/google/android/apps/gmm/base/l/u;

    add-int/lit8 v10, v7, 0x1

    .line 47
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    sget v11, Lcom/google/android/apps/gmm/f;->fa:I

    invoke-static {v11}, Lcom/google/android/libraries/curvular/c;->c(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v11

    invoke-direct {v9, v10, v2, v11}, Lcom/google/android/apps/gmm/base/l/u;-><init>(Ljava/lang/Integer;Ljava/lang/Float;Lcom/google/android/libraries/curvular/aw;)V

    .line 46
    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 39
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_3

    .line 44
    :cond_4
    const/4 v2, 0x0

    goto :goto_4

    .line 49
    :cond_5
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->v()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/base/l/ad;->c:Ljava/lang/Float;

    .line 50
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->d:Ljava/lang/String;

    .line 51
    array-length v0, v1

    if-lt v0, v12, :cond_6

    move v3, v6

    :cond_6
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->e:Ljava/lang/Boolean;

    .line 52
    return-void
.end method

.method public final b()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->c:Ljava/lang/Float;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/ad;->d:Ljava/lang/String;

    return-object v0
.end method
