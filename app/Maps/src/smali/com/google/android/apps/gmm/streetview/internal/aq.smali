.class public Lcom/google/android/apps/gmm/streetview/internal/aq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final i:Lcom/google/android/apps/gmm/v/cn;

.field private static final j:Lcom/google/android/apps/gmm/v/cn;


# instance fields
.field final a:Lcom/google/android/apps/gmm/v/n;

.field public final b:Lcom/google/android/apps/gmm/v/n;

.field final c:Lcom/google/android/apps/gmm/v/n;

.field final d:Lcom/google/android/apps/gmm/v/cn;

.field final e:Landroid/util/DisplayMetrics;

.field final f:Lcom/google/android/apps/gmm/streetview/b/a;

.field final g:Lcom/google/android/apps/gmm/v/cj;

.field volatile h:[F

.field private final k:Lcom/google/android/apps/gmm/v/cj;

.field private final l:Lcom/google/android/apps/gmm/v/cj;

.field private final m:Lcom/google/android/apps/gmm/v/cj;

.field private final n:Lcom/google/android/apps/gmm/v/cn;

.field private final o:Lcom/google/android/apps/gmm/v/cj;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v1, v2, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/aq;->i:Lcom/google/android/apps/gmm/v/cn;

    .line 41
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v2, v1, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    sput-object v0, Lcom/google/android/apps/gmm/streetview/internal/aq;->j:Lcom/google/android/apps/gmm/v/cn;

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/v/bu;Landroid/util/DisplayMetrics;)V
    .locals 7

    .prologue
    const/16 v6, 0x10

    const/high16 v4, 0x43480000    # 200.0f

    const/high16 v5, 0x42700000    # 60.0f

    const v3, 0x3dcccccd    # 0.1f

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0, v1, v1, v1}, Lcom/google/android/apps/gmm/v/cn;-><init>(FFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->d:Lcom/google/android/apps/gmm/v/cn;

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->f:Lcom/google/android/apps/gmm/streetview/b/a;

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->g:Lcom/google/android/apps/gmm/v/cj;

    .line 66
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->k:Lcom/google/android/apps/gmm/v/cj;

    .line 69
    new-array v0, v6, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->h:[F

    .line 70
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    .line 71
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->m:Lcom/google/android/apps/gmm/v/cj;

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/v/cn;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cn;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->n:Lcom/google/android/apps/gmm/v/cn;

    .line 73
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->o:Lcom/google/android/apps/gmm/v/cj;

    .line 76
    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->e:Landroid/util/DisplayMetrics;

    .line 78
    new-instance v0, Lcom/google/android/apps/gmm/v/n;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/bu;->a:Lcom/google/android/apps/gmm/v/bv;

    const/4 v2, 0x5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/n;-><init>(Lcom/google/android/apps/gmm/v/bi;IFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    .line 82
    iget-object v0, p1, Lcom/google/android/apps/gmm/v/bu;->b:Lcom/google/android/apps/gmm/v/bv;

    if-eqz v0, :cond_0

    .line 83
    new-instance v0, Lcom/google/android/apps/gmm/v/n;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/bu;->b:Lcom/google/android/apps/gmm/v/bv;

    const/16 v2, 0xa

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/n;-><init>(Lcom/google/android/apps/gmm/v/bi;IFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    .line 90
    :goto_0
    new-instance v0, Lcom/google/android/apps/gmm/v/n;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/bu;->a:Lcom/google/android/apps/gmm/v/bv;

    const/high16 v4, 0x40000000    # 2.0f

    move v2, v6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/v/n;-><init>(Lcom/google/android/apps/gmm/v/bi;IFFF)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->c:Lcom/google/android/apps/gmm/v/n;

    .line 92
    return-void

    .line 87
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/v/n;F)V
    .locals 9

    .prologue
    .line 171
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 172
    iget v1, p1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 173
    iget v6, p1, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    .line 175
    iget-object v2, p2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    .line 176
    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/v/n;->a(F)V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->h:[F

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v3

    .line 179
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v2

    .line 178
    invoke-virtual {p2, v0, v3, v2}, Lcom/google/android/apps/gmm/v/n;->a([FII)V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->h:[F

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/v/n;->b([F)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/streetview/internal/aq;->i:Lcom/google/android/apps/gmm/v/cn;

    neg-float v2, v1

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v7, 0x1

    aget v4, v4, v7

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v7, 0x2

    aget v5, v5, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    .line 183
    iget-object v7, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    iget-object v8, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->n:Lcom/google/android/apps/gmm/v/cn;

    sget-object v4, Lcom/google/android/apps/gmm/streetview/internal/aq;->j:Lcom/google/android/apps/gmm/v/cn;

    iget-object v0, v4, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x3

    const/4 v3, 0x0

    aput v3, v0, v1

    iget-object v0, v8, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v1, 0x0

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    iget-object v0, v7, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    iget-object v2, v8, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v3, 0x0

    aget v3, v2, v3

    iget-object v2, v8, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v4, 0x1

    aget v4, v2, v4

    iget-object v2, v8, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v5, 0x2

    aget v5, v2, v5

    move v2, v6

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    .line 184
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->o:Lcom/google/android/apps/gmm/v/cj;

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->o:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->d:Lcom/google/android/apps/gmm/v/cn;

    iget-object v1, v1, Lcom/google/android/apps/gmm/v/cn;->a:[F

    const/4 v2, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v3, 0xc

    const/4 v4, 0x3

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->o:Lcom/google/android/apps/gmm/v/cj;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    invoke-static {v0, v3, p3, v1, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 187
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->o:Lcom/google/android/apps/gmm/v/cj;

    invoke-virtual {p2, v0}, Lcom/google/android/apps/gmm/v/n;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->k:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->h:[F

    const/4 v2, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    const/16 v4, 0x10

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->o:Lcom/google/android/apps/gmm/v/cj;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/cj;->a()Z

    .line 194
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->k:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->g:Lcom/google/android/apps/gmm/v/cj;

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->o:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v1, 0x0

    iget-object v2, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v3, 0x0

    iget-object v4, v4, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 195
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/streetview/b/a;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const v8, -0x40f33333    # -0.55f

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->f:Lcom/google/android/apps/gmm/streetview/b/a;

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iput v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iput v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    iput v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->c:F

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    invoke-direct {p0, p1, v0, v9}, Lcom/google/android/apps/gmm/streetview/internal/aq;->a(Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/v/n;F)V

    .line 155
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/streetview/internal/aq;->j:Lcom/google/android/apps/gmm/v/cn;

    const v2, 0x41b66666    # 22.8f

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v6

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->setRotateM([FIFFFF)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    invoke-static {v0, v1, v9, v8, v8}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 162
    iget v0, p1, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    const/high16 v2, -0x3df40000    # -35.0f

    const/high16 v3, 0x42200000    # 40.0f

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/streetview/internal/ao;->a(FFF)F

    move-result v0

    .line 164
    iget-object v3, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/streetview/internal/aq;->j:Lcom/google/android/apps/gmm/v/cn;

    neg-float v2, v0

    iget-object v0, v3, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v6

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    sget-object v5, Lcom/google/android/apps/gmm/streetview/internal/aq;->i:Lcom/google/android/apps/gmm/v/cn;

    iget v2, p1, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v3, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v3, v3, v1

    iget-object v4, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v4, v4, v6

    iget-object v5, v5, Lcom/google/android/apps/gmm/v/cn;->a:[F

    aget v5, v5, v7

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/cj;->a()Z

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->c:Lcom/google/android/apps/gmm/v/n;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->l:Lcom/google/android/apps/gmm/v/cj;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/v/n;->a(Lcom/google/android/apps/gmm/v/cj;)V

    .line 168
    return-void

    .line 150
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->a:Lcom/google/android/apps/gmm/v/n;

    const v2, -0x42b33333    # -0.05f

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/apps/gmm/streetview/internal/aq;->a(Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/v/n;F)V

    .line 152
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/internal/aq;->b:Lcom/google/android/apps/gmm/v/n;

    const v2, 0x3d4ccccd    # 0.05f

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/apps/gmm/streetview/internal/aq;->a(Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/v/n;F)V

    goto :goto_0
.end method
