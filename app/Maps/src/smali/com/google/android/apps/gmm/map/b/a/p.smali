.class public final Lcom/google/android/apps/gmm/map/b/a/p;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:D


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/map/b/a/p;->a:D

    return-void
.end method

.method public static a(DD)D
    .locals 4

    .prologue
    const-wide v2, 0x4076800000000000L    # 360.0

    .line 143
    sub-double v0, p2, p0

    add-double/2addr v0, v2

    rem-double/2addr v0, v2

    return-wide v0
.end method

.method public static a(DDDI)D
    .locals 12

    .prologue
    .line 313
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 314
    invoke-static {p2, p3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 318
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    div-double v2, v6, v2

    .line 319
    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 320
    invoke-static {v8, v9, p0, p1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    const-wide/high16 v10, 0x4070000000000000L    # 256.0

    mul-double/2addr v8, v10

    div-double/2addr v6, v8

    .line 321
    mul-double/2addr v2, v6

    move/from16 v0, p6

    int-to-double v6, v0

    mul-double/2addr v2, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v6

    .line 325
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    .line 326
    const-wide v6, 0x41584db080000000L    # 6371010.0

    mul-double/2addr v2, v6

    mul-double/2addr v2, v4

    return-wide v2
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)D
    .locals 18

    .prologue
    .line 115
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 116
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 117
    move-object/from16 v0, p1

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    .line 118
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    .line 119
    sub-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 121
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v8

    .line 122
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 123
    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 124
    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    .line 125
    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v12

    .line 127
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v4, v6

    .line 128
    mul-double v14, v2, v10

    mul-double v16, v8, v6

    mul-double v16, v16, v12

    sub-double v14, v14, v16

    .line 129
    mul-double/2addr v8, v10

    mul-double/2addr v2, v6

    mul-double/2addr v2, v12

    add-double/2addr v2, v8

    .line 131
    mul-double/2addr v4, v4

    mul-double v6, v14, v14

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v2

    .line 132
    const-wide v4, 0x41584db080000000L    # 6371010.0

    mul-double/2addr v2, v4

    return-wide v2
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;IID)D
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 278
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x37

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "mapHeightPx must be positive, but "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is given."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 280
    :cond_1
    if-lez p2, :cond_2

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x36

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "mapWidthPx must be positive, but "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is given."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v1, :cond_3

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    move v1, v2

    goto :goto_1

    .line 284
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 286
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v3

    .line 288
    if-gez v0, :cond_4

    .line 289
    const/high16 v3, 0x40000000    # 2.0f

    add-int/2addr v0, v3

    .line 291
    :cond_4
    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v1, v2

    .line 293
    div-int/2addr v0, p2

    int-to-double v2, v0

    .line 294
    div-int v0, v1, p1

    int-to-double v0, v0

    .line 295
    const-wide/high16 v4, 0x4070000000000000L    # 256.0

    mul-double/2addr v4, p3

    .line 296
    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    const-wide/high16 v8, 0x403e000000000000L    # 30.0

    .line 297
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    mul-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lcom/google/android/apps/gmm/map/b/a/p;->a:D

    div-double/2addr v0, v2

    sub-double v0, v8, v0

    .line 296
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/maps/a/a;)D
    .locals 8

    .prologue
    .line 333
    .line 334
    iget-object v0, p0, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/e;

    iget-wide v0, v0, Lcom/google/maps/a/e;->d:D

    .line 335
    iget-object v2, p0, Lcom/google/maps/a/a;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/e;->d()Lcom/google/maps/a/e;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/a/e;

    iget-wide v2, v2, Lcom/google/maps/a/e;->c:D

    .line 336
    iget v4, p0, Lcom/google/maps/a/a;->e:F

    float-to-double v4, v4

    .line 337
    iget-object v6, p0, Lcom/google/maps/a/a;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/a/m;->d()Lcom/google/maps/a/m;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v6

    check-cast v6, Lcom/google/maps/a/m;

    iget v6, v6, Lcom/google/maps/a/m;->c:I

    .line 333
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->b(DDDI)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/r;)Lcom/google/android/apps/gmm/map/b/a/q;
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 63
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 65
    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    add-double/2addr v2, v4

    div-double/2addr v2, v8

    .line 67
    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_0

    .line 68
    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-wide v0, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    add-double/2addr v0, v4

    div-double/2addr v0, v8

    .line 73
    :goto_0
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v4, v2, v3, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    return-object v4

    .line 70
    :cond_0
    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    const-wide v6, 0x4076800000000000L    # 360.0

    add-double/2addr v4, v6

    iget-wide v0, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    add-double/2addr v0, v4

    div-double/2addr v0, v8

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;D)Z
    .locals 6

    .prologue
    .line 96
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4076800000000000L    # 360.0

    sub-double/2addr v4, v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    cmpg-double v0, v0, p2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(DD)D
    .locals 4

    .prologue
    const-wide v2, 0x4076800000000000L    # 360.0

    .line 154
    sub-double v0, p0, p2

    add-double/2addr v0, v2

    rem-double/2addr v0, v2

    return-wide v0
.end method

.method public static b(DDDI)D
    .locals 8

    .prologue
    .line 353
    const-wide v0, -0x3fa9800000000000L    # -90.0

    cmpg-double v0, p2, v0

    if-ltz v0, :cond_0

    const-wide v0, 0x4056800000000000L    # 90.0

    cmpl-double v0, p2, v0

    if-lez v0, :cond_1

    .line 354
    :cond_0
    const-string v0, "GeometryUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid latitude "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 357
    :cond_1
    invoke-static {p4, p5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 358
    invoke-static {p2, p3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 362
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    .line 363
    const-wide v4, 0x41584db080000000L    # 6371010.0

    mul-double/2addr v2, v4

    div-double v2, p0, v2

    .line 367
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->tan(D)D

    move-result-wide v0

    div-double v0, v4, v0

    .line 368
    div-int/lit8 v4, p6, 0x2

    int-to-double v4, v4

    mul-double/2addr v0, v4

    const-wide v4, 0x401921fb54442d18L    # 6.283185307179586

    mul-double/2addr v0, v4

    const-wide/high16 v4, 0x4070000000000000L    # 256.0

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    .line 371
    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    sget-wide v2, Lcom/google/android/apps/gmm/map/b/a/p;->a:D

    div-double/2addr v0, v2

    .line 372
    const-wide/16 v2, 0x0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_2

    const-wide/16 v0, 0x0

    :cond_2
    return-wide v0
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/r;)D
    .locals 4

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method public static b(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)F
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 191
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 210
    :cond_0
    :goto_0
    return v0

    .line 194
    :cond_1
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide v4, 0x4056800000000000L    # 90.0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_2

    .line 195
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_0

    .line 197
    :cond_2
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    const-wide v4, -0x3fa9800000000000L    # -90.0

    cmpl-double v1, v2, v4

    if-eqz v1, :cond_0

    .line 200
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    neg-double v4, v4

    cmpl-double v1, v2, v4

    if-nez v1, :cond_3

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x4066800000000000L    # 180.0

    cmpl-double v1, v2, v4

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    .line 203
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    .line 204
    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    .line 205
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    .line 206
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    .line 207
    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    .line 208
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    .line 209
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 210
    invoke-static {v0}, Lcom/google/android/apps/gmm/shared/c/s;->a(F)F

    move-result v0

    goto :goto_0

    .line 200
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static c(DD)D
    .locals 4

    .prologue
    const-wide v2, 0x4076800000000000L    # 360.0

    .line 179
    sub-double v0, p2, p0

    add-double/2addr v0, v2

    rem-double/2addr v0, v2

    return-wide v0
.end method

.method public static c(Lcom/google/android/apps/gmm/map/b/a/r;)D
    .locals 6

    .prologue
    const-wide v4, 0x4076800000000000L    # 360.0

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    sub-double/2addr v0, v2

    add-double/2addr v0, v4

    rem-double/2addr v0, v4

    return-wide v0
.end method
