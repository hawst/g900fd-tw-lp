.class Lcom/google/android/apps/gmm/map/internal/d/b/ad;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:J

.field final b:I

.field final c:I

.field final d:I

.field final e:I

.field final f:I

.field final g:I

.field final h:I


# direct methods
.method constructor <init>(JIIIIIII)V
    .locals 1

    .prologue
    .line 3206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3207
    iput-wide p1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    .line 3208
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    .line 3209
    iput p6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->c:I

    .line 3210
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    .line 3211
    iput p5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    .line 3212
    iput p7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->f:I

    .line 3213
    iput p8, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    .line 3214
    iput p9, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    .line 3215
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3219
    if-ne p0, p1, :cond_1

    .line 3229
    :cond_0
    :goto_0
    return v0

    .line 3223
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 3224
    goto :goto_0

    .line 3227
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;

    .line 3229
    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 3236
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    .line 3237
    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    add-int/2addr v0, v1

    .line 3238
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .prologue
    .line 3305
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->a:J

    iget v2, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->b:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->d:I

    iget v4, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->e:I

    iget v5, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->f:I

    iget v6, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->g:I

    iget v7, p0, Lcom/google/android/apps/gmm/map/internal/d/b/ad;->h:I

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x8c

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "ID:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Off:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " KeyLen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " DataLen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Checksum:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Shard:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ShardIndex:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
