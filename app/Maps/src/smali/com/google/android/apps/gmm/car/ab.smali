.class Lcom/google/android/apps/gmm/car/ab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/gms/car/as;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/car/aa;

.field private b:I


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/car/aa;)V
    .locals 1

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/ab;->a:Lcom/google/android/apps/gmm/car/aa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/car/ab;->b:I

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    iget-object v0, p1, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    aget-byte v4, v0, v2

    .line 54
    if-eq v4, v6, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Bad assumption"

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    .line 55
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/car/ab;->b:I

    if-eq v4, v0, :cond_4

    .line 57
    iget v0, p0, Lcom/google/android/apps/gmm/car/ab;->b:I

    and-int/lit8 v0, v0, 0xa

    if-eqz v0, :cond_5

    move v3, v1

    .line 60
    :goto_1
    and-int/lit8 v0, v4, 0xa

    if-eqz v0, :cond_6

    move v0, v1

    .line 61
    :goto_2
    iget v5, p0, Lcom/google/android/apps/gmm/car/ab;->b:I

    if-eq v5, v6, :cond_2

    if-eq v3, v0, :cond_3

    :cond_2
    move v2, v1

    .line 64
    :cond_3
    iput v4, p0, Lcom/google/android/apps/gmm/car/ab;->b:I

    .line 65
    if-eqz v2, :cond_4

    .line 66
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ab;->a:Lcom/google/android/apps/gmm/car/aa;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/aa;->c:Lcom/google/android/apps/gmm/map/util/b/g;

    new-instance v2, Lcom/google/android/apps/gmm/car/a/a;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/car/a/a;-><init>(Z)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 69
    :cond_4
    return-void

    :cond_5
    move v3, v2

    .line 57
    goto :goto_1

    :cond_6
    move v0, v2

    .line 60
    goto :goto_2
.end method
