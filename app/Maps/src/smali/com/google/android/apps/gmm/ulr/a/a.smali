.class public Lcom/google/android/apps/gmm/ulr/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/ulr/b/a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/Runnable;

.field private final c:Lcom/google/android/apps/gmm/login/a/a;

.field private final d:Lcom/google/android/apps/gmm/shared/net/a/b;

.field private final e:Lcom/google/android/apps/gmm/ulr/a/b;

.field private f:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Runnable;Lcom/google/android/apps/gmm/login/a/a;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/ulr/a/b;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/ulr/a/a;->f:Ljava/lang/Boolean;

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/ulr/a/a;->a:Landroid/content/Context;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/ulr/a/a;->b:Ljava/lang/Runnable;

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/gmm/ulr/a/a;->c:Lcom/google/android/apps/gmm/login/a/a;

    .line 43
    iput-object p4, p0, Lcom/google/android/apps/gmm/ulr/a/a;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 44
    iput-object p5, p0, Lcom/google/android/apps/gmm/ulr/a/a;->e:Lcom/google/android/apps/gmm/ulr/a/b;

    .line 45
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/a/a;->f:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/gmm/ulr/a/a;->f:Ljava/lang/Boolean;

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/a/a;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 56
    return-void
.end method

.method public final b()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/a/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->hG:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/a/a;->a:Landroid/content/Context;

    .line 61
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 62
    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/a/a;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/apps/gmm/l;->op:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    .line 63
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 64
    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v1, p0, Lcom/google/android/apps/gmm/ulr/a/a;->d:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-static {v1}, Lcom/google/android/apps/gmm/util/q;->c(Lcom/google/android/apps/gmm/shared/net/a/b;)Ljava/lang/String;

    move-result-object v1

    .line 66
    invoke-static {v2, v0, v1, v4}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/text/Spannable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 67
    return-object v2
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/a/a;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/apps/gmm/l;->nd:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/apps/gmm/ulr/a/a;->c:Lcom/google/android/apps/gmm/login/a/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/a/a;->e:Lcom/google/android/apps/gmm/ulr/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/ulr/a/b;->o()V

    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e()Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/ulr/a/a;->e:Lcom/google/android/apps/gmm/ulr/a/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/ulr/a/b;->p()V

    .line 84
    const/4 v0, 0x0

    return-object v0
.end method
