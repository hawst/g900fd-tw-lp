.class public Lcom/google/android/apps/gmm/navigation/a/ab;
.super Lcom/google/android/apps/gmm/navigation/a/a;
.source "PG"


# instance fields
.field final b:[Lcom/google/android/apps/gmm/navigation/a/a;


# direct methods
.method public constructor <init>([Lcom/google/android/apps/gmm/navigation/a/a;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/a/a;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->b:[Lcom/google/android/apps/gmm/navigation/a/a;

    .line 67
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->b:[Lcom/google/android/apps/gmm/navigation/a/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 72
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/a/a;->a()V

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->a:Z

    .line 75
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/b;)V
    .locals 1

    .prologue
    .line 79
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 80
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/navigation/a/ad;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/navigation/a/ad;-><init>(Lcom/google/android/apps/gmm/navigation/a/ab;Lcom/google/android/apps/gmm/navigation/a/b;)V

    .line 81
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/a/ad;->a()Z

    .line 82
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/navigation/a/c;)V
    .locals 4

    .prologue
    .line 100
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->b:[Lcom/google/android/apps/gmm/navigation/a/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 101
    invoke-virtual {v3, p1}, Lcom/google/android/apps/gmm/navigation/a/a;->a(Lcom/google/android/apps/gmm/navigation/a/c;)V

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 86
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->b:[Lcom/google/android/apps/gmm/navigation/a/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 87
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/a/a;->b()V

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 93
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->b:[Lcom/google/android/apps/gmm/navigation/a/a;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 94
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/navigation/a/a;->c()V

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_0
    return-void
.end method

.method public final d()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 121
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/a/ab;->b:[Lcom/google/android/apps/gmm/navigation/a/a;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 122
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/navigation/a/a;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 123
    const/4 v0, 0x1

    .line 126
    :cond_0
    return v0

    .line 121
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
