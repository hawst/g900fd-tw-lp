.class public Lcom/google/android/apps/gmm/map/o/ap;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final o:Lcom/google/android/apps/gmm/map/o/ap;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/o/ar;

.field public final b:Z

.field public final c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field public final d:F

.field public final e:I

.field public final f:I

.field public final g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

.field public final h:F

.field public final i:I

.field public final j:I

.field public final k:F

.field public final l:F

.field public final m:F

.field public final n:F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 100
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/map/o/ap;->b(FZ)Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/map/o/ap;->o:Lcom/google/android/apps/gmm/map/o/ap;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V
    .locals 1

    .prologue
    .line 308
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 309
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/o/ap;->a:Lcom/google/android/apps/gmm/map/o/ar;

    .line 310
    if-eqz p14, :cond_0

    sget-object p2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/o/ap;->c:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 311
    mul-float v0, p3, p13

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/ap;->d:F

    .line 312
    iput p4, p0, Lcom/google/android/apps/gmm/map/o/ap;->e:I

    .line 313
    iput p5, p0, Lcom/google/android/apps/gmm/map/o/ap;->f:I

    .line 314
    if-eqz p14, :cond_1

    sget-object p6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    :cond_1
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/o/ap;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    .line 315
    mul-float v0, p7, p13

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/ap;->h:F

    .line 316
    iput p8, p0, Lcom/google/android/apps/gmm/map/o/ap;->i:I

    .line 317
    iput p9, p0, Lcom/google/android/apps/gmm/map/o/ap;->j:I

    .line 318
    iput p10, p0, Lcom/google/android/apps/gmm/map/o/ap;->k:F

    .line 319
    mul-float v0, p11, p13

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/ap;->l:F

    .line 320
    mul-float v0, p12, p13

    iput v0, p0, Lcom/google/android/apps/gmm/map/o/ap;->m:F

    .line 321
    iput p13, p0, Lcom/google/android/apps/gmm/map/o/ap;->n:F

    .line 322
    iput-boolean p14, p0, Lcom/google/android/apps/gmm/map/o/ap;->b:Z

    .line 323
    return-void
.end method

.method private static a(FZ)Lcom/google/android/apps/gmm/map/o/ap;
    .locals 15

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ap;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->a:Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->h:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p1, :cond_0

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_0
    if-eqz p1, :cond_1

    const/16 v4, 0x8

    :goto_1
    const/16 v5, 0x20

    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->i:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p1, :cond_2

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_2
    const/16 v8, 0x8

    if-eqz p1, :cond_3

    const/16 v9, 0x20

    :goto_3
    if-eqz p1, :cond_4

    const/high16 v10, 0x3f800000    # 1.0f

    :goto_4
    if-eqz p1, :cond_5

    const/high16 v11, 0x3f800000    # 1.0f

    :goto_5
    const/high16 v12, 0x3f800000    # 1.0f

    move v13, p0

    move/from16 v14, p1

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/gmm/map/o/ap;-><init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V

    return-object v0

    :cond_0
    const v3, 0x3f99999a    # 1.2f

    goto :goto_0

    :cond_1
    const/16 v4, 0xe

    goto :goto_1

    :cond_2
    const v7, 0x3f99999a    # 1.2f

    goto :goto_2

    :cond_3
    const/16 v9, 0x10

    goto :goto_3

    :cond_4
    const v10, 0x3feccccd    # 1.85f

    goto :goto_4

    :cond_5
    const/high16 v11, 0x3fc00000    # 1.5f

    goto :goto_5
.end method

.method public static a(Lcom/google/android/apps/gmm/map/o/ar;FZ)Lcom/google/android/apps/gmm/map/o/ap;
    .locals 15

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/apps/gmm/map/o/aq;->a:[I

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/o/ar;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :pswitch_0
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/gmm/map/o/ap;->a(FZ)Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    .line 112
    :pswitch_1
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/gmm/map/o/ap;->a(FZ)Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v13

    new-instance v0, Lcom/google/android/apps/gmm/map/o/ap;

    iget-object v1, v13, Lcom/google/android/apps/gmm/map/o/ap;->a:Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget v3, v13, Lcom/google/android/apps/gmm/map/o/ap;->d:F

    iget v4, v13, Lcom/google/android/apps/gmm/map/o/ap;->e:I

    iget v5, v13, Lcom/google/android/apps/gmm/map/o/ap;->f:I

    iget-object v6, v13, Lcom/google/android/apps/gmm/map/o/ap;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    iget v7, v13, Lcom/google/android/apps/gmm/map/o/ap;->h:F

    iget v8, v13, Lcom/google/android/apps/gmm/map/o/ap;->i:I

    iget v9, v13, Lcom/google/android/apps/gmm/map/o/ap;->j:I

    iget v10, v13, Lcom/google/android/apps/gmm/map/o/ap;->k:F

    iget v11, v13, Lcom/google/android/apps/gmm/map/o/ap;->l:F

    iget v12, v13, Lcom/google/android/apps/gmm/map/o/ap;->m:F

    iget v13, v13, Lcom/google/android/apps/gmm/map/o/ap;->n:F

    move/from16 v14, p2

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/gmm/map/o/ap;-><init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V

    goto :goto_0

    .line 114
    :pswitch_2
    invoke-static/range {p1 .. p2}, Lcom/google/android/apps/gmm/map/o/ap;->b(FZ)Lcom/google/android/apps/gmm/map/o/ap;

    move-result-object v0

    goto :goto_0

    .line 116
    :pswitch_3
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ap;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->d:Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p2, :cond_0

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_1
    if-eqz p2, :cond_1

    const/16 v4, 0x8

    :goto_2
    if-eqz p2, :cond_2

    const/16 v5, 0x20

    :goto_3
    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p2, :cond_3

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_4
    if-eqz p2, :cond_4

    const/16 v8, 0x8

    :goto_5
    if-eqz p2, :cond_5

    const/16 v9, 0x20

    :goto_6
    if-eqz p2, :cond_6

    const/high16 v10, 0x3f800000    # 1.0f

    :goto_7
    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    move/from16 v13, p1

    move/from16 v14, p2

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/gmm/map/o/ap;-><init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V

    goto :goto_0

    :cond_0
    const v3, 0x3f99999a    # 1.2f

    goto :goto_1

    :cond_1
    const/16 v4, 0xa

    goto :goto_2

    :cond_2
    const/16 v5, 0x1c

    goto :goto_3

    :cond_3
    const v7, 0x3f99999a    # 1.2f

    goto :goto_4

    :cond_4
    const/16 v8, 0xa

    goto :goto_5

    :cond_5
    const/16 v9, 0x14

    goto :goto_6

    :cond_6
    const v10, 0x3f99999a    # 1.2f

    goto :goto_7

    .line 118
    :pswitch_4
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ap;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->e:Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p2, :cond_7

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_8
    if-eqz p2, :cond_8

    const/16 v4, 0x8

    :goto_9
    if-eqz p2, :cond_9

    const/16 v5, 0x20

    :goto_a
    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p2, :cond_a

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_b
    if-eqz p2, :cond_b

    const/16 v8, 0x8

    :goto_c
    if-eqz p2, :cond_c

    const/16 v9, 0x20

    :goto_d
    if-eqz p2, :cond_d

    const/high16 v10, 0x3f800000    # 1.0f

    :goto_e
    if-eqz p2, :cond_e

    const/high16 v11, 0x3f800000    # 1.0f

    :goto_f
    if-eqz p2, :cond_f

    const/high16 v12, 0x3f800000    # 1.0f

    :goto_10
    move/from16 v13, p1

    move/from16 v14, p2

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/gmm/map/o/ap;-><init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V

    goto/16 :goto_0

    :cond_7
    const v3, 0x3fa66666    # 1.3f

    goto :goto_8

    :cond_8
    const/16 v4, 0xc

    goto :goto_9

    :cond_9
    const/16 v5, 0x1c

    goto :goto_a

    :cond_a
    const v7, 0x3fb33333    # 1.4f

    goto :goto_b

    :cond_b
    const/16 v8, 0xc

    goto :goto_c

    :cond_c
    const/16 v9, 0x14

    goto :goto_d

    :cond_d
    const v10, 0x3f99999a    # 1.2f

    goto :goto_e

    :cond_e
    const v11, 0x3f99999a    # 1.2f

    goto :goto_f

    :cond_f
    const v12, 0x3fa66666    # 1.3f

    goto :goto_10

    .line 120
    :pswitch_5
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ap;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->a:Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p2, :cond_10

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_11
    if-eqz p2, :cond_11

    const/16 v4, 0x8

    :goto_12
    if-eqz p2, :cond_12

    const/16 v5, 0x20

    :goto_13
    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p2, :cond_13

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_14
    if-eqz p2, :cond_14

    const/16 v8, 0x8

    :goto_15
    if-eqz p2, :cond_15

    const/16 v9, 0x20

    :goto_16
    if-eqz p2, :cond_16

    const/high16 v10, 0x3f800000    # 1.0f

    :goto_17
    if-eqz p2, :cond_17

    const/high16 v11, 0x3f800000    # 1.0f

    :goto_18
    const/high16 v12, 0x3f800000    # 1.0f

    move/from16 v13, p1

    move/from16 v14, p2

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/gmm/map/o/ap;-><init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V

    goto/16 :goto_0

    :cond_10
    const v3, 0x3f4ccccd    # 0.8f

    goto :goto_11

    :cond_11
    const/16 v4, 0xc

    goto :goto_12

    :cond_12
    const/16 v5, 0x1e

    goto :goto_13

    :cond_13
    const v7, 0x3f99999a    # 1.2f

    goto :goto_14

    :cond_14
    const/16 v8, 0xc

    goto :goto_15

    :cond_15
    const/16 v9, 0x1e

    goto :goto_16

    :cond_16
    const v10, 0x3feccccd    # 1.85f

    goto :goto_17

    :cond_17
    const/high16 v11, 0x3fc00000    # 1.5f

    goto :goto_18

    .line 122
    :pswitch_6
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ap;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->c:Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz p2, :cond_18

    const/16 v4, 0x8

    :goto_19
    if-eqz p2, :cond_19

    const/16 v5, 0x20

    :goto_1a
    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    const/high16 v7, 0x3f800000    # 1.0f

    if-eqz p2, :cond_1a

    const/16 v8, 0x8

    :goto_1b
    if-eqz p2, :cond_1b

    const/16 v9, 0x20

    :goto_1c
    if-eqz p2, :cond_1c

    const/high16 v10, 0x3f800000    # 1.0f

    :goto_1d
    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    move/from16 v13, p1

    move/from16 v14, p2

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/gmm/map/o/ap;-><init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V

    goto/16 :goto_0

    :cond_18
    const/16 v4, 0xa

    goto :goto_19

    :cond_19
    const/16 v5, 0x18

    goto :goto_1a

    :cond_1a
    const/16 v8, 0xa

    goto :goto_1b

    :cond_1b
    const/16 v9, 0x1e

    goto :goto_1c

    :cond_1c
    const v10, 0x3f99999a    # 1.2f

    goto :goto_1d

    .line 108
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private static b(FZ)Lcom/google/android/apps/gmm/map/o/ap;
    .locals 15

    .prologue
    .line 150
    new-instance v0, Lcom/google/android/apps/gmm/map/o/ap;

    sget-object v1, Lcom/google/android/apps/gmm/map/o/ar;->c:Lcom/google/android/apps/gmm/map/o/ar;

    sget-object v2, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    const/high16 v3, 0x3f800000    # 1.0f

    const/16 v4, 0x8

    if-eqz p1, :cond_0

    const/16 v5, 0x20

    :goto_0
    sget-object v6, Lcom/google/android/apps/gmm/map/legacy/internal/vector/b;->g:Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;

    if-eqz p1, :cond_1

    const/high16 v7, 0x3f800000    # 1.0f

    :goto_1
    const/16 v8, 0x8

    if-eqz p1, :cond_2

    const/16 v9, 0x20

    :goto_2
    if-eqz p1, :cond_3

    const/high16 v10, 0x3f800000    # 1.0f

    :goto_3
    const/high16 v11, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f800000    # 1.0f

    move v13, p0

    move/from16 v14, p1

    invoke-direct/range {v0 .. v14}, Lcom/google/android/apps/gmm/map/o/ap;-><init>(Lcom/google/android/apps/gmm/map/o/ar;Lcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIILcom/google/android/apps/gmm/map/legacy/internal/vector/c;FIIFFFFZ)V

    return-object v0

    :cond_0
    const/16 v5, 0x18

    goto :goto_0

    :cond_1
    const v7, 0x3f99999a    # 1.2f

    goto :goto_1

    :cond_2
    const/16 v9, 0x10

    goto :goto_2

    :cond_3
    const v10, 0x3f99999a    # 1.2f

    goto :goto_3
.end method
