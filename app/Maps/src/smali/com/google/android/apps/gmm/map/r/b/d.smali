.class public Lcom/google/android/apps/gmm/map/r/b/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Z

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 226
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    .line 227
    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/map/r/b/d;)V
    .locals 1

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    .line 222
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    .line 223
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    .line 224
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 231
    instance-of v1, p1, Lcom/google/android/apps/gmm/map/r/b/d;

    if-nez v1, :cond_1

    .line 235
    :cond_0
    :goto_0
    return v0

    .line 234
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/map/r/b/d;

    .line 235
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 241
    new-instance v0, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v1, "isGpsAccurate"

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/map/r/b/d;->a:Z

    .line 242
    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "numSatInFix"

    iget v2, p0, Lcom/google/android/apps/gmm/map/r/b/d;->b:I

    .line 243
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 244
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
