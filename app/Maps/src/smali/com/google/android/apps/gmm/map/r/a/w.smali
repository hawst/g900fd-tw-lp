.class public Lcom/google/android/apps/gmm/map/r/a/w;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final N:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ad;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public volatile B:Lcom/google/android/apps/gmm/map/r/a/r;

.field public volatile C:Lcom/google/android/apps/gmm/map/r/a/r;

.field public D:Lcom/google/android/apps/gmm/map/r/a/r;

.field public E:Lcom/google/android/apps/gmm/map/r/a/r;

.field public final F:Ljava/lang/Object;

.field public G:Lcom/google/maps/g/a/fz;

.field public final H:I

.field public I:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final J:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final K:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final L:Lcom/google/n/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private volatile M:[Lcom/google/android/apps/gmm/map/r/a/ac;

.field public final a:Lcom/google/r/b/a/agl;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final b:I

.field public final c:Lcom/google/android/apps/gmm/map/r/a/ao;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final d:Lcom/google/maps/g/a/hm;

.field public final e:Lcom/google/maps/g/wq;

.field public final f:Z

.field public final g:[Lcom/google/android/apps/gmm/map/r/a/ag;

.field public final h:Lcom/google/android/apps/gmm/map/b/a/ab;

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Lcom/google/android/apps/gmm/map/internal/c/as;

.field public final k:I

.field public final l:[Lcom/google/android/apps/gmm/map/r/a/ap;

.field public m:Ljava/lang/String;

.field public volatile n:Lcom/google/maps/g/a/fw;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public volatile o:Lcom/google/maps/g/a/gc;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public p:[D

.field public q:[D

.field public final r:I

.field public final s:I

.field public final t:I

.field public final u:I

.field public final v:I

.field public final w:I

.field public final x:Lcom/google/maps/g/a/al;

.field public final y:Lcom/google/r/b/a/afz;

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1408
    new-instance v0, Lcom/google/android/apps/gmm/map/r/a/x;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/r/a/x;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/map/r/a/w;->N:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/r/a/ab;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->F:Ljava/lang/Object;

    .line 355
    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    .line 597
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->a:Lcom/google/android/apps/gmm/map/r/a/ao;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 598
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->b:Lcom/google/maps/g/a/hm;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    .line 599
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->c:Lcom/google/maps/g/wq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->e:Lcom/google/maps/g/wq;

    .line 600
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->e:[Lcom/google/android/apps/gmm/map/r/a/ag;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 601
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 602
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->g:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    .line 603
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->h:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->k:I

    .line 604
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->m:Ljava/lang/String;

    .line 605
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->j:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->t:I

    .line 606
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->k:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->r:I

    .line 607
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->l:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->s:I

    .line 608
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->m:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->u:I

    .line 609
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->n:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->v:I

    .line 610
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->o:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->w:I

    .line 611
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->r:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->f:Z

    .line 612
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->s:Lcom/google/maps/g/a/al;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->x:Lcom/google/maps/g/a/al;

    .line 613
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->t:Lcom/google/r/b/a/afz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->y:Lcom/google/r/b/a/afz;

    .line 614
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->u:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->z:I

    .line 615
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->v:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->A:Ljava/util/List;

    .line 616
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->w:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->H:I

    .line 617
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->y:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->J:Ljava/util/List;

    .line 618
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->z:Lcom/google/r/b/a/agl;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 619
    iget v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->A:I

    iput v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->b:I

    .line 621
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->B:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->L:Lcom/google/n/f;

    .line 622
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->q:Lcom/google/maps/g/a/gc;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->o:Lcom/google/maps/g/a/gc;

    .line 625
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->x:Lcom/google/n/f;

    if-eqz v0, :cond_1

    .line 626
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->x:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->I:Lcom/google/n/f;

    .line 635
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 636
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget v0, v0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    iget-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v0, v0, Lcom/google/maps/g/a/ea;->d:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->K:Lcom/google/n/f;

    .line 642
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/as;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/map/internal/c/as;-><init>(Lcom/google/android/apps/gmm/map/b/a/ab;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->j:Lcom/google/android/apps/gmm/map/internal/c/as;

    .line 643
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/r/a/w;->a()V

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/w;->a([Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 646
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/w;->b([Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 648
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/an;->a([Lcom/google/android/apps/gmm/map/r/a/ag;)Z

    .line 653
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 627
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    .line 628
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget v0, v0, Lcom/google/maps/g/a/ea;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v4, 0x4

    if-ne v0, v4, :cond_2

    move v0, v2

    :goto_3
    if-eqz v0, :cond_3

    .line 629
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->c:Lcom/google/android/apps/gmm/map/r/a/ao;

    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v0, v0, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ea;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    iget-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/p;->a:Lcom/google/maps/g/a/ea;

    iget-object v0, v0, Lcom/google/maps/g/a/ea;->e:Lcom/google/n/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->I:Lcom/google/n/f;

    goto/16 :goto_0

    :cond_2
    move v0, v3

    .line 628
    goto :goto_3

    .line 631
    :cond_3
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->I:Lcom/google/n/f;

    goto/16 :goto_0

    :cond_4
    move v0, v3

    .line 636
    goto/16 :goto_1

    .line 639
    :cond_5
    iput-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->K:Lcom/google/n/f;

    goto :goto_2

    .line 654
    :cond_6
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v0, v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/r/a/as;->a(I)I

    .line 655
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 661
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->d:Lcom/google/maps/g/a/hm;

    sget-object v2, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    if-ne v0, v2, :cond_7

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/r/a/ab;->p:Lcom/google/maps/g/a/fw;

    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/maps/g/a/fw;)V

    .line 662
    return-void

    :cond_7
    move-object v0, v1

    .line 661
    goto :goto_4
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/e;ILandroid/content/Context;Lcom/google/maps/g/wq;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZLcom/google/r/b/a/afz;)Lcom/google/android/apps/gmm/map/r/a/w;
    .locals 13
    .param p0    # Lcom/google/android/apps/gmm/map/r/a/e;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/maps/g/wq;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 684
    if-nez p0, :cond_0

    .line 685
    const/4 v1, 0x0

    .line 695
    :goto_0
    return-object v1

    .line 687
    :cond_0
    if-ltz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    array-length v1, v1

    if-gt v1, p1, :cond_3

    :cond_1
    const/4 v1, 0x0

    move-object v2, v1

    .line 691
    :goto_1
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_4

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    :goto_2
    iget v1, v1, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    :cond_2
    sget-object v3, Lcom/google/maps/g/a/hm;->g:Lcom/google/maps/g/a/hm;

    if-eq v1, v3, :cond_5

    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_6

    .line 692
    const/4 v1, 0x0

    goto :goto_0

    .line 687
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/r/a/e;->a(I)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/e;->c:[Lcom/google/android/apps/gmm/map/r/a/ao;

    aget-object v1, v1, p1

    move-object v2, v1

    goto :goto_1

    .line 691
    :cond_4
    iget-object v1, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_2

    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 697
    :cond_6
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v3, :cond_7

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    :goto_4
    iget v1, v1, Lcom/google/maps/g/a/fk;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_8

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    move-object v3, v1

    .line 704
    :goto_5
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/e;->a:Lcom/google/r/b/a/agl;

    .line 695
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v1, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_9

    const/4 v1, 0x1

    :goto_6
    if-nez v1, :cond_a

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v1

    .line 697
    :cond_7
    iget-object v1, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    goto :goto_4

    :cond_8
    move-object v3, v1

    goto :goto_5

    .line 695
    :cond_9
    const/4 v1, 0x0

    goto :goto_6

    :cond_a
    if-nez v3, :cond_b

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_b
    new-instance v11, Lcom/google/android/apps/gmm/map/r/a/ab;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/map/r/a/ab;-><init>()V

    iput-object v2, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->a:Lcom/google/android/apps/gmm/map/r/a/ao;

    iput-object v3, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->b:Lcom/google/maps/g/a/hm;

    move-object/from16 v0, p3

    iput-object v0, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->c:Lcom/google/maps/g/wq;

    move/from16 v0, p6

    iput-boolean v0, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->r:Z

    move-object/from16 v0, p7

    iput-object v0, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->t:Lcom/google/r/b/a/afz;

    iput-object v4, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->z:Lcom/google/r/b/a/agl;

    iput p1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->A:I

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v5, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v5, v5, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v5, v5, 0x20

    const/16 v6, 0x20

    if-ne v5, v6, :cond_10

    const/4 v5, 0x1

    :goto_7
    if-nez v5, :cond_11

    const-string v5, "RouteDescription"

    const-string v6, "Trip had no compact polyline."

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v6, v7}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_8
    if-eqz v4, :cond_c

    iget-object v5, v4, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v5, v5

    div-int/lit8 v5, v5, 0x3

    if-nez v5, :cond_d

    :cond_c
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v1, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/util/List;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v4

    :cond_d
    iput-object v4, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->f:Lcom/google/android/apps/gmm/map/b/a/ab;

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->g:Ljava/util/List;

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v5, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    if-nez v5, :cond_12

    invoke-static {}, Lcom/google/maps/g/a/fk;->g()Lcom/google/maps/g/a/fk;

    move-result-object v1

    move-object v5, v1

    :goto_9
    iget v1, v5, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v6, 0x8

    if-ne v1, v6, :cond_13

    const/4 v1, 0x1

    :goto_a
    if-eqz v1, :cond_17

    iget-object v1, v5, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v1, :cond_14

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v1

    :goto_b
    iget v1, v1, Lcom/google/maps/g/a/be;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v6, 0x1

    if-ne v1, v6, :cond_15

    const/4 v1, 0x1

    :goto_c
    if-eqz v1, :cond_17

    iget-object v1, v5, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    if-nez v1, :cond_16

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v1

    :goto_d
    iget v1, v1, Lcom/google/maps/g/a/be;->b:I

    move v6, v1

    :goto_e
    iget v1, v5, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit16 v1, v1, 0x100

    const/16 v7, 0x100

    if-ne v1, v7, :cond_18

    const/4 v1, 0x1

    :goto_f
    if-eqz v1, :cond_1a

    iget-object v1, v5, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    if-nez v1, :cond_19

    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v1

    move-object v7, v1

    :goto_10
    iget v1, v7, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v8, 0x1

    if-ne v1, v8, :cond_1b

    const/4 v1, 0x1

    :goto_11
    if-eqz v1, :cond_1c

    iget-object v1, v7, Lcom/google/maps/g/a/au;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/be;

    iget v1, v1, Lcom/google/maps/g/a/be;->b:I

    move v8, v1

    :goto_12
    iget v1, v7, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v9, 0x10

    if-ne v1, v9, :cond_1d

    const/4 v1, 0x1

    :goto_13
    if-eqz v1, :cond_1e

    iget-object v1, v7, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ax;->d()Lcom/google/maps/g/a/ax;

    move-result-object v9

    invoke-virtual {v1, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/ax;

    iget v1, v1, Lcom/google/maps/g/a/ax;->b:I

    move v9, v1

    :goto_14
    iget v1, v7, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v1, v1, 0x10

    const/16 v10, 0x10

    if-ne v1, v10, :cond_1f

    const/4 v1, 0x1

    :goto_15
    if-eqz v1, :cond_20

    iget-object v1, v7, Lcom/google/maps/g/a/au;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ax;->d()Lcom/google/maps/g/a/ax;

    move-result-object v10

    invoke-virtual {v1, v10}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/ax;

    iget v1, v1, Lcom/google/maps/g/a/ax;->c:I

    move v10, v1

    :goto_16
    iget v1, v7, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v1, v1, 0x20

    const/16 v12, 0x20

    if-ne v1, v12, :cond_21

    const/4 v1, 0x1

    :goto_17
    if-eqz v1, :cond_22

    iget-object v1, v7, Lcom/google/maps/g/a/au;->g:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/be;->g()Lcom/google/maps/g/a/be;

    move-result-object v12

    invoke-virtual {v1, v12}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/be;

    iget v1, v1, Lcom/google/maps/g/a/be;->b:I

    :goto_18
    iput v8, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->k:I

    sub-int v6, v8, v6

    iput v6, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->l:I

    iput v9, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->m:I

    iput v10, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->n:I

    iput v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->o:I

    iget v1, v5, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v6, 0x4

    if-ne v1, v6, :cond_23

    const/4 v1, 0x1

    :goto_19
    if-eqz v1, :cond_27

    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v1, :cond_24

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    :goto_1a
    iget v1, v1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v6, 0x1

    if-ne v1, v6, :cond_25

    const/4 v1, 0x1

    :goto_1b
    if-eqz v1, :cond_27

    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v1, :cond_26

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    :goto_1c
    iget v1, v1, Lcom/google/maps/g/a/ai;->b:I

    :goto_1d
    iput v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->j:I

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v1, Lcom/google/maps/g/a/hu;->l:Lcom/google/n/aq;

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->v:Ljava/util/List;

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v1, v1, Lcom/google/maps/g/a/hu;->m:Lcom/google/n/aq;

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->y:Ljava/util/List;

    const/4 v1, 0x0

    new-instance v6, Lcom/google/android/apps/gmm/map/r/a/p;

    iget-object v8, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v8, v8, Lcom/google/maps/g/a/hu;->e:Ljava/util/List;

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/ea;

    invoke-direct {v6, v1}, Lcom/google/android/apps/gmm/map/r/a/p;-><init>(Lcom/google/maps/g/a/ea;)V

    sget-object v1, Lcom/google/maps/g/a/hm;->d:Lcom/google/maps/g/a/hm;

    if-ne v3, v1, :cond_28

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v6, v4, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/r/a/p;Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/r/a/ap;Landroid/content/res/Resources;)[Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->e:[Lcom/google/android/apps/gmm/map/r/a/ag;

    :goto_1e
    iget v1, v5, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_29

    const/4 v1, 0x1

    :goto_1f
    if-eqz v1, :cond_2a

    invoke-virtual {v5}, Lcom/google/maps/g/a/fk;->d()Ljava/lang/String;

    move-result-object v1

    :goto_20
    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->i:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/r/a/ap;

    const/4 v3, 0x0

    aput-object p4, v1, v3

    const/4 v3, 0x1

    aput-object p5, v1, v3

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->d:[Lcom/google/android/apps/gmm/map/r/a/ap;

    iget v1, v5, Lcom/google/maps/g/a/fk;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2b

    const/4 v1, 0x1

    :goto_21
    if-eqz v1, :cond_2f

    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v1, :cond_2c

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    :goto_22
    iget v1, v1, Lcom/google/maps/g/a/ai;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v3, 0x4

    if-ne v1, v3, :cond_2d

    const/4 v1, 0x1

    :goto_23
    if-eqz v1, :cond_2f

    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    if-nez v1, :cond_2e

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    :goto_24
    iget v1, v1, Lcom/google/maps/g/a/ai;->d:I

    invoke-static {v1}, Lcom/google/maps/g/a/al;->a(I)Lcom/google/maps/g/a/al;

    move-result-object v1

    if-nez v1, :cond_e

    sget-object v1, Lcom/google/maps/g/a/al;->d:Lcom/google/maps/g/a/al;

    :cond_e
    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->s:Lcom/google/maps/g/a/al;

    :goto_25
    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v3, v1, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    if-nez v3, :cond_30

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v1

    :goto_26
    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->p:Lcom/google/maps/g/a/fw;

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->b:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v3, v1, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    if-nez v3, :cond_31

    invoke-static {}, Lcom/google/maps/c/a/a;->d()Lcom/google/maps/c/a/a;

    move-result-object v1

    :goto_27
    iget v1, v1, Lcom/google/maps/c/a/a;->b:I

    iput v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->w:I

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget v1, v1, Lcom/google/maps/g/a/hu;->a:I

    const/high16 v3, 0x20000

    and-int/2addr v1, v3

    const/high16 v3, 0x20000

    if-ne v1, v3, :cond_32

    const/4 v1, 0x1

    :goto_28
    if-eqz v1, :cond_34

    iget-object v1, v2, Lcom/google/android/apps/gmm/map/r/a/ao;->a:Lcom/google/maps/g/a/hu;

    iget-object v2, v1, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    if-nez v2, :cond_33

    invoke-static {}, Lcom/google/maps/g/a/gc;->d()Lcom/google/maps/g/a/gc;

    move-result-object v1

    :goto_29
    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->q:Lcom/google/maps/g/a/gc;

    iget v1, v7, Lcom/google/maps/g/a/au;->a:I

    and-int/lit8 v1, v1, 0x40

    const/16 v2, 0x40

    if-ne v1, v2, :cond_35

    const/4 v1, 0x1

    :goto_2a
    if-eqz v1, :cond_f

    iget-object v1, v7, Lcom/google/maps/g/a/au;->h:Lcom/google/n/f;

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->B:Lcom/google/n/f;

    :cond_f
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/w;

    invoke-direct {v1, v11}, Lcom/google/android/apps/gmm/map/r/a/w;-><init>(Lcom/google/android/apps/gmm/map/r/a/ab;)V

    goto/16 :goto_0

    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_7

    :cond_11
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/ao;->a()Lcom/google/android/apps/gmm/map/r/a/a;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/a;->b:Ljava/util/List;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(Ljava/util/List;)Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/ao;->a()Lcom/google/android/apps/gmm/map/r/a/a;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/r/a/a;->a:Lcom/google/r/b/a/aex;

    iget v5, v5, Lcom/google/r/b/a/aex;->e:I

    iput v5, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->h:I

    goto/16 :goto_8

    :cond_12
    iget-object v1, v1, Lcom/google/maps/g/a/hu;->d:Lcom/google/maps/g/a/fk;

    move-object v5, v1

    goto/16 :goto_9

    :cond_13
    const/4 v1, 0x0

    goto/16 :goto_a

    :cond_14
    iget-object v1, v5, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_b

    :cond_15
    const/4 v1, 0x0

    goto/16 :goto_c

    :cond_16
    iget-object v1, v5, Lcom/google/maps/g/a/fk;->e:Lcom/google/maps/g/a/be;

    goto/16 :goto_d

    :cond_17
    const/4 v1, 0x0

    move v6, v1

    goto/16 :goto_e

    :cond_18
    const/4 v1, 0x0

    goto/16 :goto_f

    :cond_19
    iget-object v1, v5, Lcom/google/maps/g/a/fk;->k:Lcom/google/maps/g/a/au;

    move-object v7, v1

    goto/16 :goto_10

    :cond_1a
    invoke-static {}, Lcom/google/maps/g/a/au;->d()Lcom/google/maps/g/a/au;

    move-result-object v1

    move-object v7, v1

    goto/16 :goto_10

    :cond_1b
    const/4 v1, 0x0

    goto/16 :goto_11

    :cond_1c
    move v8, v6

    goto/16 :goto_12

    :cond_1d
    const/4 v1, 0x0

    goto/16 :goto_13

    :cond_1e
    const/4 v1, 0x0

    move v9, v1

    goto/16 :goto_14

    :cond_1f
    const/4 v1, 0x0

    goto/16 :goto_15

    :cond_20
    const/4 v1, 0x0

    move v10, v1

    goto/16 :goto_16

    :cond_21
    const/4 v1, 0x0

    goto/16 :goto_17

    :cond_22
    const/4 v1, 0x0

    goto/16 :goto_18

    :cond_23
    const/4 v1, 0x0

    goto/16 :goto_19

    :cond_24
    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_1a

    :cond_25
    const/4 v1, 0x0

    goto/16 :goto_1b

    :cond_26
    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_1c

    :cond_27
    const/4 v1, 0x0

    goto/16 :goto_1d

    :cond_28
    move-object/from16 v0, p5

    invoke-static {v6, v4, v0}, Lcom/google/android/apps/gmm/map/r/a/q;->a(Lcom/google/android/apps/gmm/map/r/a/p;Lcom/google/android/apps/gmm/map/b/a/ab;Lcom/google/android/apps/gmm/map/r/a/ap;)[Lcom/google/android/apps/gmm/map/r/a/ag;

    move-result-object v1

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->e:[Lcom/google/android/apps/gmm/map/r/a/ag;

    goto/16 :goto_1e

    :cond_29
    const/4 v1, 0x0

    goto/16 :goto_1f

    :cond_2a
    const-string v1, ""

    goto/16 :goto_20

    :cond_2b
    const/4 v1, 0x0

    goto/16 :goto_21

    :cond_2c
    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_22

    :cond_2d
    const/4 v1, 0x0

    goto/16 :goto_23

    :cond_2e
    iget-object v1, v5, Lcom/google/maps/g/a/fk;->d:Lcom/google/maps/g/a/ai;

    goto/16 :goto_24

    :cond_2f
    const-string v1, "RouteDescription"

    const-string v3, "Attempt to create a route from a Trip that doesn\'t have recommended distance units"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    sget-object v1, Lcom/google/maps/g/a/al;->a:Lcom/google/maps/g/a/al;

    iput-object v1, v11, Lcom/google/android/apps/gmm/map/r/a/ab;->s:Lcom/google/maps/g/a/al;

    goto/16 :goto_25

    :cond_30
    iget-object v1, v1, Lcom/google/maps/g/a/hu;->f:Lcom/google/maps/g/a/fw;

    goto/16 :goto_26

    :cond_31
    iget-object v1, v1, Lcom/google/r/b/a/afb;->q:Lcom/google/maps/c/a/a;

    goto/16 :goto_27

    :cond_32
    const/4 v1, 0x0

    goto/16 :goto_28

    :cond_33
    iget-object v1, v1, Lcom/google/maps/g/a/hu;->w:Lcom/google/maps/g/a/gc;

    goto/16 :goto_29

    :cond_34
    const/4 v1, 0x0

    goto/16 :goto_29

    :cond_35
    const/4 v1, 0x0

    goto/16 :goto_2a
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/f;ILandroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/w;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x0

    .line 720
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 721
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 722
    :cond_1
    if-ltz p1, :cond_2

    move v1, v0

    :goto_0
    if-nez v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    move v1, v6

    goto :goto_0

    .line 723
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/e;->b:Lcom/google/r/b/a/afb;

    iget-object v1, v1, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_4

    :goto_1
    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_4
    move v0, v6

    goto :goto_1

    .line 726
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/f;->a:Lcom/google/android/apps/gmm/map/r/a/e;

    const/4 v3, 0x0

    .line 727
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/f;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 728
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/r/a/f;->d:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/r/a/f;->e:Lcom/google/r/b/a/afz;

    move v1, p1

    move-object v2, p2

    .line 725
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/e;ILandroid/content/Context;Lcom/google/maps/g/wq;Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/r/a/ap;ZLcom/google/r/b/a/afz;)Lcom/google/android/apps/gmm/map/r/a/w;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ae;)Lcom/google/r/b/a/agl;
    .locals 15
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 1860
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    if-eq v0, v1, :cond_0

    move v0, v7

    :goto_0
    if-nez v0, :cond_1

    move-object v0, v6

    .line 1936
    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    .line 1860
    goto :goto_0

    .line 1863
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    if-eq v0, v1, :cond_3

    move v0, v7

    :goto_2
    if-eqz v0, :cond_4

    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    move-object v8, v0

    .line 1864
    :goto_3
    if-eqz v8, :cond_2

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    if-eqz v0, :cond_2

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1866
    iget v0, v0, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_5

    move v0, v7

    :goto_4
    if-eqz v0, :cond_2

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1867
    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v1, :cond_6

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_5
    iget v0, v0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_7

    move v0, v7

    :goto_6
    if-nez v0, :cond_8

    :cond_2
    move-object v0, v6

    .line 1868
    goto :goto_1

    :cond_3
    move v0, v3

    .line 1863
    goto :goto_2

    :cond_4
    move-object v8, v6

    goto :goto_3

    :cond_5
    move v0, v3

    .line 1866
    goto :goto_4

    .line 1867
    :cond_6
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_5

    :cond_7
    move v0, v3

    goto :goto_6

    .line 1870
    :cond_8
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1871
    iget-object v1, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v1, :cond_a

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_7
    iget-object v1, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v1, :cond_b

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    .line 1870
    :goto_8
    invoke-static {v0}, Lcom/google/r/b/a/afb;->a(Lcom/google/r/b/a/afb;)Lcom/google/r/b/a/afd;

    move-result-object v9

    .line 1872
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v9, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    iget v0, v9, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v0, v0, -0x3

    iput v0, v9, Lcom/google/r/b/a/afd;->a:I

    .line 1873
    iget v0, v9, Lcom/google/r/b/a/afd;->a:I

    and-int/lit8 v0, v0, -0x9

    iput v0, v9, Lcom/google/r/b/a/afd;->a:I

    iput v3, v9, Lcom/google/r/b/a/afd;->d:I

    .line 1874
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, v9, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    iget v0, v9, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v0, v0, -0x201

    iput v0, v9, Lcom/google/r/b/a/afd;->a:I

    .line 1875
    iget v0, v9, Lcom/google/r/b/a/afd;->a:I

    and-int/lit16 v0, v0, -0x401

    iput v0, v9, Lcom/google/r/b/a/afd;->a:I

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    iget-object v0, v0, Lcom/google/r/b/a/afb;->l:Lcom/google/n/f;

    iput-object v0, v9, Lcom/google/r/b/a/afd;->f:Lcom/google/n/f;

    .line 1878
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    .line 1879
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/r/a/ae;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v2, v3

    move v4, v3

    :cond_9
    :goto_9
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 1880
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1881
    iget v0, v0, Lcom/google/r/b/a/agl;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_c

    move v0, v7

    :goto_a
    if-eqz v0, :cond_9

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1882
    iget-object v5, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v5, :cond_d

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_b
    iget v0, v0, Lcom/google/r/b/a/afn;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v7, :cond_e

    move v0, v7

    :goto_c
    if-eqz v0, :cond_9

    .line 1883
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    iget-object v0, v0, Lcom/google/r/b/a/agl;->d:Ljava/util/List;

    invoke-interface {v10, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1890
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1891
    iget-object v5, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v5, :cond_f

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_d
    iget-object v5, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    if-nez v5, :cond_10

    invoke-static {}, Lcom/google/r/b/a/afb;->d()Lcom/google/r/b/a/afb;

    move-result-object v0

    move-object v5, v0

    .line 1892
    :goto_e
    iget v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->b:I

    iget-object v12, v5, Lcom/google/r/b/a/afb;->c:Ljava/util/List;

    invoke-interface {v12, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    .line 1895
    invoke-static {}, Lcom/google/maps/g/a/hu;->newBuilder()Lcom/google/maps/g/a/hw;

    move-result-object v12

    invoke-virtual {v12, v0}, Lcom/google/maps/g/a/hw;->a(Lcom/google/maps/g/a/hu;)Lcom/google/maps/g/a/hw;

    move-result-object v12

    .line 1896
    iget-object v13, v1, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    if-eqz v13, :cond_12

    .line 1897
    iget-object v13, v1, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    invoke-static {}, Lcom/google/maps/g/a/fw;->d()Lcom/google/maps/g/a/fw;

    move-result-object v14

    if-eq v13, v14, :cond_12

    .line 1898
    iget-object v13, v1, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    if-nez v13, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1871
    :cond_a
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto/16 :goto_7

    :cond_b
    iget-object v0, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    goto/16 :goto_8

    :cond_c
    move v0, v3

    .line 1881
    goto :goto_a

    .line 1882
    :cond_d
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_b

    :cond_e
    move v0, v3

    goto :goto_c

    .line 1891
    :cond_f
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_d

    :cond_10
    iget-object v0, v0, Lcom/google/r/b/a/afn;->b:Lcom/google/r/b/a/afb;

    move-object v5, v0

    goto :goto_e

    .line 1898
    :cond_11
    iput-object v13, v12, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    iget v13, v12, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit8 v13, v13, 0x10

    iput v13, v12, Lcom/google/maps/g/a/hw;->a:I

    .line 1902
    :goto_f
    iget-object v13, v1, Lcom/google/android/apps/gmm/map/r/a/w;->o:Lcom/google/maps/g/a/gc;

    if-eqz v13, :cond_14

    .line 1903
    iget-object v13, v1, Lcom/google/android/apps/gmm/map/r/a/w;->o:Lcom/google/maps/g/a/gc;

    if-nez v13, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1900
    :cond_12
    iput-object v6, v12, Lcom/google/maps/g/a/hw;->b:Lcom/google/maps/g/a/fw;

    iget v13, v12, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit8 v13, v13, -0x11

    iput v13, v12, Lcom/google/maps/g/a/hw;->a:I

    goto :goto_f

    .line 1903
    :cond_13
    iput-object v13, v12, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    iget v13, v12, Lcom/google/maps/g/a/hw;->a:I

    const/high16 v14, 0x200000

    or-int/2addr v13, v14

    iput v13, v12, Lcom/google/maps/g/a/hw;->a:I

    .line 1909
    :goto_10
    iget v0, v0, Lcom/google/maps/g/a/hu;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v13, 0x20

    if-ne v0, v13, :cond_15

    move v0, v7

    :goto_11
    if-eqz v0, :cond_17

    .line 1910
    iget v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->b:I

    iget-object v5, v5, Lcom/google/r/b/a/afb;->k:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/aex;

    .line 1911
    if-eqz v0, :cond_19

    .line 1912
    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1905
    :cond_14
    iput-object v6, v12, Lcom/google/maps/g/a/hw;->d:Lcom/google/maps/g/a/gc;

    iget v13, v12, Lcom/google/maps/g/a/hw;->a:I

    const v14, -0x200001

    and-int/2addr v13, v14

    iput v13, v12, Lcom/google/maps/g/a/hw;->a:I

    goto :goto_10

    :cond_15
    move v0, v3

    .line 1909
    goto :goto_11

    .line 1912
    :cond_16
    invoke-virtual {v9}, Lcom/google/r/b/a/afd;->i()V

    iget-object v5, v9, Lcom/google/r/b/a/afd;->e:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1913
    iget v0, v12, Lcom/google/maps/g/a/hw;->a:I

    or-int/lit16 v0, v0, 0x80

    iput v0, v12, Lcom/google/maps/g/a/hw;->a:I

    iput v4, v12, Lcom/google/maps/g/a/hw;->c:I

    .line 1914
    add-int/lit8 v4, v4, 0x1

    .line 1920
    :cond_17
    :goto_12
    if-ne v1, v8, :cond_18

    .line 1921
    iget v0, v9, Lcom/google/r/b/a/afd;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v9, Lcom/google/r/b/a/afd;->a:I

    iput v2, v9, Lcom/google/r/b/a/afd;->c:I

    .line 1923
    :cond_18
    add-int/lit8 v1, v2, 0x1

    .line 1924
    invoke-virtual {v12}, Lcom/google/maps/g/a/hw;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hu;

    if-nez v0, :cond_1a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1916
    :cond_19
    iget v0, v12, Lcom/google/maps/g/a/hw;->a:I

    and-int/lit16 v0, v0, -0x81

    iput v0, v12, Lcom/google/maps/g/a/hw;->a:I

    iput v3, v12, Lcom/google/maps/g/a/hw;->c:I

    goto :goto_12

    .line 1924
    :cond_1a
    invoke-virtual {v9}, Lcom/google/r/b/a/afd;->d()V

    iget-object v2, v9, Lcom/google/r/b/a/afd;->b:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v2, v1

    .line 1925
    goto/16 :goto_9

    .line 1928
    :cond_1b
    invoke-static {}, Lcom/google/r/b/a/afn;->newBuilder()Lcom/google/r/b/a/afp;

    move-result-object v1

    .line 1929
    invoke-virtual {v9}, Lcom/google/r/b/a/afd;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afb;

    iput-object v0, v1, Lcom/google/r/b/a/afp;->b:Lcom/google/r/b/a/afb;

    iget v0, v1, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/r/b/a/afp;->a:I

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1930
    iget-object v2, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v2, :cond_1c

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_13
    iget-object v2, v0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    if-nez v2, :cond_1d

    invoke-static {}, Lcom/google/maps/a/a;->d()Lcom/google/maps/a/a;

    move-result-object v0

    :goto_14
    if-nez v0, :cond_1e

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1c
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_13

    :cond_1d
    iget-object v0, v0, Lcom/google/r/b/a/afn;->e:Lcom/google/maps/a/a;

    goto :goto_14

    :cond_1e
    iput-object v0, v1, Lcom/google/r/b/a/afp;->c:Lcom/google/maps/a/a;

    iget v0, v1, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/r/b/a/afp;->a:I

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/r/a/w;->a:Lcom/google/r/b/a/agl;

    .line 1931
    iget-object v2, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    if-nez v2, :cond_1f

    invoke-static {}, Lcom/google/r/b/a/afn;->g()Lcom/google/r/b/a/afn;

    move-result-object v0

    :goto_15
    invoke-virtual {v0}, Lcom/google/r/b/a/afn;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_20

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1f
    iget-object v0, v0, Lcom/google/r/b/a/agl;->b:Lcom/google/r/b/a/afn;

    goto :goto_15

    :cond_20
    iget v2, v1, Lcom/google/r/b/a/afp;->a:I

    or-int/lit8 v2, v2, 0x40

    iput v2, v1, Lcom/google/r/b/a/afp;->a:I

    iput-object v0, v1, Lcom/google/r/b/a/afp;->d:Ljava/lang/Object;

    .line 1933
    invoke-static {}, Lcom/google/r/b/a/agl;->newBuilder()Lcom/google/r/b/a/agn;

    move-result-object v2

    .line 1934
    invoke-virtual {v1}, Lcom/google/r/b/a/afp;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afn;

    iput-object v0, v2, Lcom/google/r/b/a/agn;->b:Lcom/google/r/b/a/afn;

    iget v0, v2, Lcom/google/r/b/a/agn;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/r/b/a/agn;->a:I

    .line 1935
    invoke-virtual {v2, v10}, Lcom/google/r/b/a/agn;->a(Ljava/lang/Iterable;)Lcom/google/r/b/a/agn;

    move-result-object v0

    .line 1936
    invoke-virtual {v0}, Lcom/google/r/b/a/agn;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/agl;

    goto/16 :goto_1
.end method

.method private a()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    .line 1220
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    .line 1221
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    .line 1222
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_1

    .line 1237
    :cond_0
    return-void

    .line 1225
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aput-wide v2, v0, v1

    .line 1226
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aput-wide v2, v0, v1

    .line 1230
    const/4 v0, 0x1

    move-wide v4, v2

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1231
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    add-int/lit8 v6, v0, -0x1

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/b/a/ab;->b(I)F

    move-result v1

    float-to-double v6, v1

    .line 1232
    add-double/2addr v4, v6

    .line 1233
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v8, v1

    const-wide v10, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v8, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->atan(D)D

    move-result-wide v8

    const-wide v12, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v8, v12

    mul-double/2addr v8, v10

    const-wide v10, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v8, v10

    invoke-static {v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    add-double/2addr v2, v6

    .line 1234
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    aput-wide v4, v1, v0

    .line 1235
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aput-wide v2, v1, v0

    .line 1230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static a([Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 3

    .prologue
    .line 1696
    const/4 v0, 0x1

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 1697
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p0, v1

    aget-object v2, p0, v0

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 1698
    aget-object v1, p0, v0

    add-int/lit8 v2, v0, -0x1

    aget-object v2, p0, v2

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->C:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 1696
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1700
    :cond_0
    return-void
.end method

.method private static b([Lcom/google/android/apps/gmm/map/r/a/ag;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1704
    move v2, v3

    :goto_0
    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_4

    .line 1705
    aget-object v0, p0, v2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/am;

    .line 1706
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/map/r/a/am;->e:Z

    if-eqz v0, :cond_0

    .line 1707
    add-int/lit8 v0, v2, 0x1

    aget-object v0, p0, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/am;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v7, Lcom/google/maps/g/a/bx;->b:Lcom/google/maps/g/a/bx;

    if-ne v6, v7, :cond_1

    :goto_2
    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/am;->f:Lcom/google/android/apps/gmm/map/r/a/am;

    if-nez v0, :cond_0

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/map/r/a/am;->e:Z

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1704
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1711
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(J)D
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1537
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/ac;

    invoke-direct {v1, v3, p1, p2}, Lcom/google/android/apps/gmm/map/r/a/ac;-><init>(IJ)V

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/z;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/map/r/a/z;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;)V

    .line 1538
    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 1547
    if-ltz v0, :cond_0

    .line 1548
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v0, v1, v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    int-to-double v0, v0

    .line 1574
    :goto_0
    return-wide v0

    .line 1558
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1559
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v0, v0, v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    int-to-double v0, v0

    goto :goto_0

    .line 1563
    :cond_1
    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    .line 1566
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    .line 1567
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    int-to-double v0, v0

    goto :goto_0

    .line 1571
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v1, v1, v0

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->b:J

    long-to-double v2, v2

    .line 1572
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    add-int/lit8 v4, v0, 0x1

    aget-object v1, v1, v4

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->b:J

    long-to-double v4, v4

    .line 1574
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    int-to-double v6, v1

    long-to-double v8, p1

    sub-double v8, v2, v8

    sub-double/2addr v2, v4

    div-double v2, v8, v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    add-int/lit8 v4, v0, 0x1

    aget-object v1, v1, v4

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v0, v4, v0

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    sub-int v0, v1, v0

    int-to-double v0, v0

    mul-double/2addr v0, v2

    add-double/2addr v0, v6

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/ad;)D
    .locals 4

    .prologue
    .line 1023
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->p:[D

    iget v1, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    aget-wide v0, v0, v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 1024
    iget v3, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final a(D)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 11
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 1299
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->t:I

    int-to-double v0, v0

    cmpl-double v0, p1, v0

    if-lez v0, :cond_1

    .line 1300
    :cond_0
    const/4 v0, 0x0

    .line 1323
    :goto_0
    return-object v0

    .line 1303
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    .line 1304
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    invoke-static {v0, p1, p2}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v0

    if-gez v0, :cond_2

    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    move v1, v0

    .line 1305
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v1, v0, :cond_3

    .line 1307
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    :cond_2
    move v1, v0

    .line 1304
    goto :goto_1

    .line 1310
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v4, v0, v1

    .line 1311
    add-int/lit8 v0, v1, 0x1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v6, v3, v0

    .line 1315
    sub-double v8, p1, v4

    sub-double v4, v6, v4

    div-double v4, v8, v4

    .line 1318
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1319
    add-int/lit8 v1, v1, 0x1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1320
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 1321
    double-to-float v3, v4

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object v0, v2

    .line 1323
    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;D)Lcom/google/android/apps/gmm/map/r/a/ad;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1256
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    .line 1257
    if-nez v0, :cond_0

    .line 1258
    const/4 v0, 0x0

    .line 1260
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/a/s;)Lcom/google/android/apps/gmm/map/r/a/r;
    .locals 4

    .prologue
    .line 1102
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/aa;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/r/a/s;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1125
    const-string v0, "RouteDescription"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unsupported TextureType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1126
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1109
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->B:Lcom/google/android/apps/gmm/map/r/a/r;

    goto :goto_0

    .line 1113
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 1114
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->D:Lcom/google/android/apps/gmm/map/r/a/r;

    monitor-exit v1

    goto :goto_0

    .line 1115
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1119
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 1120
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->E:Lcom/google/android/apps/gmm/map/r/a/r;

    monitor-exit v1

    goto :goto_0

    .line 1121
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 1123
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->C:Lcom/google/android/apps/gmm/map/r/a/r;

    goto :goto_0

    .line 1102
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/maps/g/a/fw;)V
    .locals 10
    .param p1    # Lcom/google/maps/g/a/fw;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 1076
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v2, :cond_5

    .line 1077
    :cond_0
    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    .line 1078
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    new-array v3, v0, [I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    new-array v4, v0, [J

    move v0, v1

    move v2, v1

    :goto_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v5, v5, v0

    iget v5, v5, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    add-int/2addr v2, v5

    aput v2, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    move v2, v1

    :goto_1
    if-ltz v0, :cond_2

    int-to-long v6, v2

    aput-wide v6, v4, v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    aget-object v5, v5, v0

    iget v5, v5, Lcom/google/android/apps/gmm/map/r/a/ag;->k:I

    add-int/2addr v2, v5

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/ac;

    :goto_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/ac;

    aget v5, v3, v1

    aget-wide v6, v4, v1

    invoke-direct {v2, v5, v6, v7}, Lcom/google/android/apps/gmm/map/r/a/ac;-><init>(IJ)V

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    .line 1085
    :cond_4
    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->F:Ljava/lang/Object;

    monitor-enter v1

    .line 1087
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->D:Lcom/google/android/apps/gmm/map/r/a/r;

    .line 1088
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->E:Lcom/google/android/apps/gmm/map/r/a/r;

    .line 1090
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    .line 1091
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 1080
    :cond_5
    iget v0, p1, Lcom/google/maps/g/a/fw;->d:I

    invoke-static {v0}, Lcom/google/maps/g/a/fz;->a(I)Lcom/google/maps/g/a/fz;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/maps/g/a/fz;->a:Lcom/google/maps/g/a/fz;

    :cond_6
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    .line 1081
    if-nez p1, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget-object v0, p1, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v0, v2, :cond_8

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_9

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_8
    move v0, v1

    goto :goto_4

    :cond_9
    iget-object v2, p1, Lcom/google/maps/g/a/fw;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Lcom/google/android/apps/gmm/map/r/a/ac;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eo;

    iget-wide v4, v0, Lcom/google/maps/g/a/eo;->c:J

    :goto_5
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    new-instance v6, Lcom/google/android/apps/gmm/map/r/a/ac;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eo;

    iget v7, v0, Lcom/google/maps/g/a/eo;->b:I

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/eo;

    iget-wide v8, v0, Lcom/google/maps/g/a/eo;->c:J

    sub-long v8, v4, v8

    invoke-direct {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/r/a/ac;-><init>(IJ)V

    aput-object v6, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_a
    iput-object v3, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    goto :goto_3

    .line 1091
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;DZ)[Lcom/google/android/apps/gmm/map/r/a/ad;
    .locals 2

    .prologue
    .line 1275
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    .line 1276
    if-nez v0, :cond_1

    .line 1277
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/r/a/ad;

    .line 1283
    :cond_0
    :goto_0
    return-object v0

    .line 1279
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/apps/gmm/map/r/a/ad;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/r/a/ad;

    .line 1280
    if-eqz p4, :cond_0

    .line 1281
    sget-object v1, Lcom/google/android/apps/gmm/map/r/a/w;->N:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method public final b(D)D
    .locals 11

    .prologue
    .line 1485
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/ac;

    double-to-int v2, p1

    const-wide/16 v4, 0x0

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/apps/gmm/map/r/a/ac;-><init>(IJ)V

    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/y;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/map/r/a/y;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;)V

    invoke-static {v0, v1, v2}, Ljava/util/Arrays;->binarySearch([Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)I

    move-result v0

    .line 1494
    if-ltz v0, :cond_0

    .line 1495
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v0, v1, v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/r/a/ac;->b:J

    long-to-double v0, v0

    .line 1520
    :goto_0
    return-wide v0

    .line 1505
    :cond_0
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1506
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/r/a/ac;->b:J

    long-to-double v0, v0

    goto :goto_0

    .line 1510
    :cond_1
    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    .line 1513
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_2

    .line 1514
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 1518
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    int-to-double v2, v1

    .line 1519
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    add-int/lit8 v4, v0, 0x1

    aget-object v1, v1, v4

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->a:I

    int-to-double v4, v1

    .line 1520
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    add-int/lit8 v6, v0, 0x1

    aget-object v1, v1, v6

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->b:J

    long-to-double v6, v6

    sub-double v8, v4, p1

    sub-double v2, v4, v2

    div-double v2, v8, v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    aget-object v1, v1, v0

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/r/a/ac;->b:J

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->M:[Lcom/google/android/apps/gmm/map/r/a/ac;

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v1, v0

    iget-wide v0, v0, Lcom/google/android/apps/gmm/map/r/a/ac;->b:J

    sub-long v0, v4, v0

    long-to-double v0, v0

    mul-double/2addr v0, v2

    add-double/2addr v0, v6

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/y;D)D
    .locals 4

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    .line 1609
    if-nez p1, :cond_0

    move-wide v0, v2

    .line 1616
    :goto_0
    return-wide v0

    .line 1612
    :cond_0
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 1613
    :goto_1
    if-nez v0, :cond_2

    move-wide v0, v2

    .line 1614
    goto :goto_0

    .line 1612
    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_1

    .line 1616
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/r/a/w;->b(Lcom/google/android/apps/gmm/map/r/a/ad;)D

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/map/r/a/w;->b(D)D

    move-result-wide v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/r/a/ad;)D
    .locals 10

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    iget v1, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    aget-wide v0, v0, v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/r/a/w;->h:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 1039
    iget v3, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v2, v2

    .line 1040
    iget-object v4, p1, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v4, v4

    const-wide v6, 0x3e3921fb54442d18L    # 5.8516723170686385E-9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5}, Ljava/lang/Math;->exp(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->atan(D)D

    move-result-wide v4

    const-wide v8, 0x3fe921fb54442d18L    # 0.7853981633974483

    sub-double/2addr v4, v8

    mul-double/2addr v4, v6

    const-wide v6, 0x404ca5dc1a63c1f8L    # 57.29577951308232

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final b(J)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 9

    .prologue
    .line 1586
    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/map/r/a/w;->a(J)D

    move-result-wide v2

    .line 1587
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->binarySearch([DD)I

    move-result v0

    if-gez v0, :cond_0

    add-int/lit8 v0, v0, 0x2

    neg-int v0, v0

    .line 1588
    :cond_0
    add-int/lit8 v1, v0, 0x1

    .line 1589
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    array-length v4, v4

    if-lt v1, v4, :cond_1

    .line 1590
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1597
    :goto_0
    return-object v0

    .line 1592
    :cond_1
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v4, v4, v0

    .line 1593
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/r/a/w;->q:[D

    aget-wide v6, v6, v1

    .line 1594
    sub-double/2addr v2, v4

    sub-double v4, v6, v4

    div-double/2addr v2, v4

    .line 1595
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1596
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1597
    double-to-float v3, v2

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-static {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;FLcom/google/android/apps/gmm/map/b/a/y;)V

    move-object v0, v2

    goto :goto_0
.end method

.method public b(Lcom/google/android/apps/gmm/map/b/a/y;DZ)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "DZ)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1350
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1351
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v2, v2

    .line 1352
    cmpg-double v4, v2, p2

    if-gez v4, :cond_0

    .line 1353
    new-instance v4, Lcom/google/android/apps/gmm/map/r/a/ad;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/r/a/ad;-><init>()V

    .line 1354
    move-object/from16 v0, p0

    iput-object v0, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 1355
    iput-wide v2, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    .line 1356
    const/4 v2, 0x0

    iput v2, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    .line 1357
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->i:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v2, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1358
    const-wide/16 v2, 0x0

    iput-wide v2, v4, Lcom/google/android/apps/gmm/map/r/a/ad;->c:D

    .line 1359
    invoke-static {v4}, Lcom/google/b/c/cv;->a(Ljava/lang/Object;)Lcom/google/b/c/cv;

    move-result-object v2

    .line 1404
    :goto_0
    return-object v2

    .line 1361
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1366
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/w;->j:Lcom/google/android/apps/gmm/map/internal/c/as;

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double v4, v4, p2

    double-to-int v3, v4

    .line 1367
    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    .line 1366
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5, v4}, Lcom/google/android/apps/gmm/map/internal/c/as;->a(Lcom/google/android/apps/gmm/map/b/a/ae;ILjava/util/ArrayList;)V

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/map/internal/c/as;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    .line 1368
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1369
    const/4 v2, 0x0

    goto :goto_0

    .line 1373
    :cond_2
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1374
    new-instance v9, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v9}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 1375
    new-instance v10, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 1376
    new-instance v11, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v11}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 1377
    const/4 v2, 0x0

    move v4, v2

    :goto_1
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_7

    .line 1378
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/ah;

    .line 1379
    iget v2, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->c:I

    iget v6, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    sub-int/2addr v2, v6

    add-int/lit8 v12, v2, -0x1

    .line 1380
    const/4 v2, 0x0

    move v6, v2

    :goto_2
    if-ge v6, v12, :cond_6

    .line 1381
    iget-object v2, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v7, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v7, v6

    invoke-virtual {v2, v7, v9}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1382
    add-int/lit8 v2, v6, 0x1

    iget-object v7, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget v13, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v2, v13

    invoke-virtual {v7, v2, v10}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 1383
    move-object/from16 v0, p1

    invoke-static {v9, v10, v0, v11}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v2

    float-to-double v14, v2

    .line 1384
    cmpg-double v2, v14, p2

    if-gez v2, :cond_4

    .line 1385
    const/4 v7, 0x0

    .line 1386
    if-nez p4, :cond_3

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1387
    :cond_3
    new-instance v2, Lcom/google/android/apps/gmm/map/r/a/ad;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/r/a/ad;-><init>()V

    .line 1388
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1394
    :goto_3
    if-eqz v2, :cond_4

    .line 1395
    move-object/from16 v0, p0

    iput-object v0, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    .line 1396
    iput-wide v14, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    .line 1397
    iget v7, v3, Lcom/google/android/apps/gmm/map/b/a/ah;->b:I

    add-int/2addr v7, v6

    iput v7, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->e:I

    .line 1398
    invoke-static {v11}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v7

    iput-object v7, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 1399
    iget v7, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v9, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v7, v13

    iget v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v14, v9, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v13, v14

    invoke-static {v7, v13}, Lcom/google/android/apps/gmm/map/b/a/z;->a(II)F

    move-result v7

    float-to-double v14, v7

    iput-wide v14, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->c:D

    .line 1380
    :cond_4
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    .line 1389
    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/ad;

    iget-wide v0, v2, Lcom/google/android/apps/gmm/map/r/a/ad;->d:D

    move-wide/from16 v16, v0

    cmpg-double v2, v14, v16

    if-gez v2, :cond_9

    .line 1392
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/r/a/ad;

    goto :goto_3

    .line 1377
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_1

    .line 1404
    :cond_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_8
    move-object v2, v5

    goto/16 :goto_0

    :cond_9
    move-object v2, v7

    goto :goto_3
.end method
