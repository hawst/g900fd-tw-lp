.class public abstract Lcom/google/android/apps/gmm/place/PlacePageView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/l;


# instance fields
.field public a:Lcom/google/android/apps/gmm/place/af;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->b:Z

    .line 29
    return-void
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public abstract a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->b:Z

    .line 46
    return-void
.end method

.method public final ab_()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 105
    .line 106
    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ac_()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->b:Z

    return v0
.end method

.method public e()Lcom/google/android/apps/gmm/place/af;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    if-eqz v0, :cond_0

    .line 91
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/af;->a(Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 93
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onDetachedFromWindow()V

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    if-eqz v0, :cond_0

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacePageView;->a:Lcom/google/android/apps/gmm/place/af;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacePageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/place/af;->b(Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 101
    :cond_0
    return-void
.end method
