.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"


# static fields
.field public static final g:Z

.field public static final h:Z


# instance fields
.field public a:[F

.field public b:F

.field public c:F

.field public final d:F

.field public e:[F

.field public final f:F

.field public i:F

.field public j:F

.field public k:F

.field public l:F

.field public final m:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 124
    sput-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->g:Z

    .line 126
    sput-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 1

    .prologue
    .line 191
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;-><init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/lang/Class;)V

    .line 214
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/lang/Class;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 159
    invoke-direct {p0, p3}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 72
    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a:[F

    .line 81
    iput v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->c:F

    .line 83
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->e:[F

    .line 134
    const v0, 0x3e4ccccd    # 0.2f

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->i:F

    .line 142
    iput v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->j:F

    .line 148
    iput v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->k:F

    .line 153
    iput v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->l:F

    .line 160
    iget v0, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    if-gez v0, :cond_0

    .line 161
    const/4 v0, 0x1

    iget v1, p2, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    neg-int v1, v1

    shl-int/2addr v0, v1

    int-to-float v0, v0

    div-float v0, v3, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->j:F

    .line 164
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->a()I

    move-result v0

    .line 165
    invoke-static {}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->c()I

    move-result v1

    .line 167
    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->f:F

    .line 168
    int-to-float v0, v0

    div-float v0, v3, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->d:F

    .line 172
    iget v0, p1, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->c:I

    int-to-float v0, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->d:F

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->m:F

    .line 173
    return-void
.end method

.method static a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 180
    const-class v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/k;

    return-object v0
.end method


# virtual methods
.method public final a(FII)V
    .locals 5

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->e:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->e:[F

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->m:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->f:F

    int-to-float v4, p2

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->e:[F

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->m:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->f:F

    int-to-float v4, p3

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->e:[F

    const/4 v1, 0x3

    int-to-float v2, p2

    aput v2, v0, v1

    .line 282
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 304
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->d:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->a:[F

    invoke-static {v0, v3, v1, v2}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->g:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->l:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->h:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->c:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 318
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->i:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->d:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->j:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->e:[F

    invoke-static {v0, v3, v1, v2}, Landroid/opengl/GLES20;->glUniform4fv(II[FI)V

    .line 330
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->g:Z

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->e:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->j:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 341
    :goto_0
    sget-boolean v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->h:Z

    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->l:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->b:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->f:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->k:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 354
    return-void

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;

    iget v0, v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/l;->k:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/i;->i:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    goto :goto_0
.end method
