.class public Lcom/google/android/apps/gmm/p/e/a/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/internal/d/as;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/as;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/e/a/b;->a:Lcom/google/android/apps/gmm/map/internal/d/as;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;ZJ)Lcom/google/android/apps/gmm/p/e/a/a;
    .locals 29

    .prologue
    .line 54
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/p/e/a/b;->a:Lcom/google/android/apps/gmm/map/internal/d/as;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v2, v0, v3}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Z)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v2

    .line 57
    if-nez v2, :cond_3

    .line 58
    new-instance v2, Lcom/google/android/apps/gmm/map/internal/d/a/a;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/a/a;-><init>(I)V

    .line 59
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/a/b;->a:Lcom/google/android/apps/gmm/map/internal/d/as;

    move-object/from16 v0, p1

    invoke-interface {v3, v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/as;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/internal/d/a/c;)V

    .line 60
    const-wide/16 v4, 0x0

    cmp-long v3, p3, v4

    if-gez v3, :cond_1

    .line 61
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/a/a;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v3}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 67
    :cond_0
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-gtz v3, :cond_2

    .line 68
    const/4 v2, 0x0

    .line 82
    :goto_0
    return-object v2

    .line 63
    :cond_1
    iget-object v3, v2, Lcom/google/android/apps/gmm/map/internal/d/a/a;->b:Ljava/util/concurrent/CountDownLatch;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 64
    const/4 v2, 0x0

    goto :goto_0

    .line 70
    :cond_2
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/a/a;->a:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/bo;

    .line 74
    :cond_3
    new-instance v25, Lcom/google/android/apps/gmm/p/e/a/a;

    invoke-direct/range {v25 .. v25}, Lcom/google/android/apps/gmm/p/e/a/a;-><init>()V

    .line 78
    instance-of v3, v2, Lcom/google/android/apps/gmm/map/internal/c/cm;

    if-eqz v3, :cond_9

    .line 79
    new-instance v26, Lcom/google/android/apps/gmm/map/b/a/b;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/c/bo;->a()Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/bp;->b()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v3

    move-object/from16 v0, v26

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/b/a/b;-><init>(Lcom/google/android/apps/gmm/map/b/a/af;)V

    .line 80
    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/cm;

    const/4 v3, 0x0

    move/from16 v23, v3

    :goto_1
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/internal/c/cm;->g()I

    move-result v3

    move/from16 v0, v23

    if-ge v0, v3, :cond_9

    move/from16 v0, v23

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(I)Lcom/google/android/apps/gmm/map/internal/c/m;

    move-result-object v21

    new-instance v27, Ljava/util/ArrayList;

    invoke-direct/range {v27 .. v27}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/google/android/apps/gmm/map/internal/c/az;

    if-eqz v3, :cond_4

    move-object/from16 v3, v21

    check-cast v3, Lcom/google/android/apps/gmm/map/internal/c/az;

    iget-boolean v4, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->i:Z

    if-eqz v4, :cond_8

    iget-object v4, v3, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v4, v1}, Lcom/google/android/apps/gmm/map/b/a/b;->a(Lcom/google/android/apps/gmm/map/b/a/ab;Ljava/util/List;)V

    const/4 v4, 0x0

    move v5, v4

    :goto_2
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v4

    if-ge v5, v4, :cond_8

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v25

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/e/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    :cond_4
    if-eqz p2, :cond_8

    move-object/from16 v0, v21

    instance-of v3, v0, Lcom/google/android/apps/gmm/map/internal/c/af;

    if-eqz v3, :cond_8

    move-object/from16 v22, v21

    check-cast v22, Lcom/google/android/apps/gmm/map/internal/c/af;

    move-object/from16 v0, v22

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/c/af;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    const/4 v3, 0x0

    move-object/from16 v0, v26

    iput v3, v0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    const/4 v3, 0x0

    move-object/from16 v0, v26

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/map/b/a/b;->a:Z

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    iget-object v3, v4, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v3, v3

    div-int/lit8 v6, v3, 0x3

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v6, :cond_5

    invoke-virtual {v4, v3, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v7, v5, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/b;->a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V

    add-int/lit8 v7, v3, 0x1

    invoke-virtual {v4, v7, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v26

    invoke-virtual {v0, v7, v5, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/b;->a(ILcom/google/android/apps/gmm/map/b/a/y;IZ)V

    add-int/lit8 v3, v3, 0x2

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    move v4, v3

    :goto_4
    move-object/from16 v0, v26

    iget v3, v0, Lcom/google/android/apps/gmm/map/b/a/b;->c:I

    if-ge v4, v3, :cond_7

    move-object/from16 v0, v26

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/b/a/b;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/ad;

    iget v5, v3, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    const/4 v6, 0x1

    if-le v5, v6, :cond_6

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    const/4 v5, 0x0

    iput v5, v3, Lcom/google/android/apps/gmm/map/b/a/ad;->a:I

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    :cond_7
    const/4 v3, 0x0

    new-array v0, v3, [I

    move-object/from16 v20, v0

    const/4 v3, 0x0

    move/from16 v24, v3

    :goto_5
    invoke-interface/range {v27 .. v27}, Ljava/util/List;->size()I

    move-result v3

    move/from16 v0, v24

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/az;

    const-wide/16 v4, -0x1

    invoke-interface/range {v21 .. v21}, Lcom/google/android/apps/gmm/map/internal/c/m;->c()I

    move-result v6

    invoke-interface/range {v21 .. v21}, Lcom/google/android/apps/gmm/map/internal/c/m;->d()I

    move-result v7

    const/4 v8, 0x0

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/apps/gmm/map/b/a/ab;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/16 v14, 0x10

    move-object/from16 v0, v22

    iget v15, v0, Lcom/google/android/apps/gmm/map/internal/c/af;->c:I

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x1

    invoke-direct/range {v3 .. v20}, Lcom/google/android/apps/gmm/map/internal/c/az;-><init>(JIILcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/ab;[Lcom/google/android/apps/gmm/map/internal/c/z;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/c/bi;IIIIIIZ[I)V

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/b/a/ab;

    move-object/from16 v0, v25

    iget-object v5, v0, Lcom/google/android/apps/gmm/p/e/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v25

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/e/a/a;->b:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v24, 0x1

    move/from16 v24, v3

    goto :goto_5

    :cond_8
    add-int/lit8 v3, v23, 0x1

    move/from16 v23, v3

    goto/16 :goto_1

    :cond_9
    move-object/from16 v2, v25

    .line 82
    goto/16 :goto_0
.end method
