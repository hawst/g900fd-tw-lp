.class public Lcom/google/android/apps/gmm/place/station/au;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/station/b/g;
.implements Ljava/io/Serializable;


# static fields
.field static final e:Landroid/view/View$OnClickListener;

.field private static final f:Ljava/lang/String;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final c:Lcom/google/maps/g/wj;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final d:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/ao;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/google/android/apps/gmm/map/b/a/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final h:Lcom/google/android/apps/gmm/place/station/am;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final i:Ljava/lang/Boolean;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final j:Lcom/google/android/apps/gmm/place/station/ao;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final k:Lcom/google/android/apps/gmm/place/station/ao;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final l:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/place/station/ao;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/i/af;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lcom/google/android/apps/gmm/place/station/aw;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/apps/gmm/place/station/au;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/station/au;->f:Ljava/lang/String;

    .line 96
    new-instance v0, Lcom/google/android/apps/gmm/place/station/av;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/av;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/au;->e:Landroid/view/View$OnClickListener;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/station/am;ZLcom/google/maps/g/wj;Ljava/lang/String;)V
    .locals 10
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/place/station/am;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Lcom/google/maps/g/wj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-direct {v1}, Ljava/lang/NullPointerException;-><init>()V

    throw v1

    :cond_0
    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/google/android/apps/gmm/place/station/au;->a:Ljava/lang/String;

    .line 109
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/station/au;->b:Ljava/lang/String;

    .line 110
    iput-object p4, p0, Lcom/google/android/apps/gmm/place/station/au;->g:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 111
    iput-object p5, p0, Lcom/google/android/apps/gmm/place/station/au;->h:Lcom/google/android/apps/gmm/place/station/am;

    .line 112
    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->i:Ljava/lang/Boolean;

    .line 113
    if-eqz p8, :cond_1

    new-instance v1, Lcom/google/android/apps/gmm/place/station/aw;

    move-object/from16 v0, p8

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/station/aw;-><init>(Ljava/lang/String;)V

    :goto_0
    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->o:Lcom/google/android/apps/gmm/place/station/aw;

    .line 114
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->c:Lcom/google/maps/g/wj;

    .line 115
    move-object/from16 v0, p7

    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/place/station/au;->a(Landroid/content/Context;Lcom/google/maps/g/wj;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->m:Lcom/google/b/c/cv;

    .line 116
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/gmm/place/station/au;->b(Lcom/google/maps/g/wj;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->n:Lcom/google/b/c/cv;

    .line 120
    if-nez p7, :cond_2

    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v1

    .line 121
    :goto_1
    invoke-static {v1, p4}, Lcom/google/android/apps/gmm/place/station/au;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;)I

    move-result v4

    .line 122
    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/gmm/place/station/au;->a(Lcom/google/maps/g/wj;)I

    move-result v5

    .line 124
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v6

    .line 125
    const/4 v2, 0x0

    .line 127
    if-eqz p7, :cond_b

    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/hg;

    iget v1, v1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v1, v1, 0x1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_7

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_b

    .line 128
    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/hg;

    iget-object v1, v1, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/gu;

    .line 129
    const/4 v2, 0x0

    const/4 v3, 0x1

    if-gez v4, :cond_8

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->a:Lcom/google/android/apps/gmm/place/station/ap;

    .line 130
    :goto_3
    new-instance v7, Lcom/google/android/apps/gmm/place/station/ao;

    invoke-direct {v7, p1, v1, v5, v2}, Lcom/google/android/apps/gmm/place/station/ao;-><init>(Landroid/content/Context;Lcom/google/maps/g/a/gu;ILcom/google/android/apps/gmm/place/station/ap;)V

    iput-object v7, p0, Lcom/google/android/apps/gmm/place/station/au;->j:Lcom/google/android/apps/gmm/place/station/ao;

    .line 131
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->j:Lcom/google/android/apps/gmm/place/station/ao;

    invoke-virtual {v6, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    move v2, v3

    .line 136
    :goto_4
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v7

    .line 137
    if-eqz p7, :cond_f

    .line 138
    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/hg;

    invoke-virtual {v1}, Lcom/google/maps/g/a/hg;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/gu;

    .line 139
    add-int/lit8 v3, v2, 0x1

    if-gez v4, :cond_c

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->a:Lcom/google/android/apps/gmm/place/station/ap;

    .line 140
    :goto_6
    new-instance v9, Lcom/google/android/apps/gmm/place/station/ao;

    invoke-direct {v9, p1, v1, v5, v2}, Lcom/google/android/apps/gmm/place/station/ao;-><init>(Landroid/content/Context;Lcom/google/maps/g/a/gu;ILcom/google/android/apps/gmm/place/station/ap;)V

    .line 142
    invoke-virtual {v7, v9}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 143
    invoke-virtual {v6, v9}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    move v2, v3

    .line 144
    goto :goto_5

    .line 113
    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 120
    :cond_2
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v3

    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/hg;

    iget v2, v1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x1

    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    const/4 v2, 0x1

    :goto_7
    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/google/maps/g/a/hg;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/a/gu;

    invoke-virtual {v3, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_3
    invoke-virtual {v1}, Lcom/google/maps/g/a/hg;->d()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Iterable;)Lcom/google/b/c/cx;

    iget v2, v1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v2, v2, 0x2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_6

    const/4 v2, 0x1

    :goto_8
    if-eqz v2, :cond_4

    iget-object v1, v1, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/gu;

    invoke-virtual {v3, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    :cond_4
    invoke-virtual {v3}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_7

    :cond_6
    const/4 v2, 0x0

    goto :goto_8

    .line 127
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 129
    :cond_8
    if-ge v2, v4, :cond_9

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->b:Lcom/google/android/apps/gmm/place/station/ap;

    goto/16 :goto_3

    :cond_9
    if-le v2, v4, :cond_a

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->d:Lcom/google/android/apps/gmm/place/station/ap;

    goto/16 :goto_3

    :cond_a
    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->c:Lcom/google/android/apps/gmm/place/station/ap;

    goto/16 :goto_3

    .line 133
    :cond_b
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->j:Lcom/google/android/apps/gmm/place/station/ao;

    goto/16 :goto_4

    .line 139
    :cond_c
    if-ge v2, v4, :cond_d

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->b:Lcom/google/android/apps/gmm/place/station/ap;

    goto/16 :goto_6

    :cond_d
    if-le v2, v4, :cond_e

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->d:Lcom/google/android/apps/gmm/place/station/ap;

    goto/16 :goto_6

    :cond_e
    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->c:Lcom/google/android/apps/gmm/place/station/ap;

    goto/16 :goto_6

    .line 146
    :cond_f
    invoke-virtual {v7}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->l:Lcom/google/b/c/cv;

    .line 148
    if-eqz p7, :cond_14

    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/hg;

    iget v1, v1, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v1, v1, 0x2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_10

    const/4 v1, 0x1

    :goto_9
    if-eqz v1, :cond_14

    .line 149
    move-object/from16 v0, p7

    iget-object v1, v0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/hg;

    iget-object v1, v1, Lcom/google/maps/g/a/hg;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/gu;->g()Lcom/google/maps/g/a/gu;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/a/gu;

    .line 150
    if-gez v4, :cond_11

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->a:Lcom/google/android/apps/gmm/place/station/ap;

    .line 151
    :goto_a
    new-instance v3, Lcom/google/android/apps/gmm/place/station/ao;

    invoke-direct {v3, p1, v1, v5, v2}, Lcom/google/android/apps/gmm/place/station/ao;-><init>(Landroid/content/Context;Lcom/google/maps/g/a/gu;ILcom/google/android/apps/gmm/place/station/ap;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/place/station/au;->k:Lcom/google/android/apps/gmm/place/station/ao;

    .line 152
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->k:Lcom/google/android/apps/gmm/place/station/ao;

    invoke-virtual {v6, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 157
    :goto_b
    invoke-virtual {v6}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->d:Lcom/google/b/c/cv;

    .line 158
    return-void

    .line 148
    :cond_10
    const/4 v1, 0x0

    goto :goto_9

    .line 150
    :cond_11
    if-ge v2, v4, :cond_12

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->b:Lcom/google/android/apps/gmm/place/station/ap;

    goto :goto_a

    :cond_12
    if-le v2, v4, :cond_13

    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->d:Lcom/google/android/apps/gmm/place/station/ap;

    goto :goto_a

    :cond_13
    sget-object v2, Lcom/google/android/apps/gmm/place/station/ap;->c:Lcom/google/android/apps/gmm/place/station/ap;

    goto :goto_a

    .line 154
    :cond_14
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/station/au;->k:Lcom/google/android/apps/gmm/place/station/ao;

    goto :goto_b
.end method

.method private static a(Lcom/google/maps/g/wj;)I
    .locals 3
    .param p0    # Lcom/google/maps/g/wj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 191
    if-eqz p0, :cond_3

    iget-object v0, p0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hg;

    iget v0, v0, Lcom/google/maps/g/a/hg;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 192
    iget-object v0, p0, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hg;

    iget-object v1, v0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    .line 194
    :goto_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 195
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 200
    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 208
    :goto_2
    return v0

    .line 191
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 192
    :cond_1
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v2, v0, Lcom/google/maps/g/a/hg;->e:Ljava/lang/Object;

    :cond_2
    move-object v0, v2

    goto :goto_1

    .line 205
    :catch_0
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/place/station/au;->f:Ljava/lang/String;

    const-string v1, "Failed to parse color: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 208
    :cond_3
    :goto_3
    const v0, -0x996601

    goto :goto_2

    .line 205
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3
.end method

.method private static a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/j;)I
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/a/gu;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/j;",
            ")I"
        }
    .end annotation

    .prologue
    .line 230
    if-eqz p1, :cond_3

    .line 231
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 232
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gu;

    iget-object v1, v0, Lcom/google/maps/g/a/gu;->j:Ljava/lang/Object;

    instance-of v3, v1, Ljava/lang/String;

    if-eqz v3, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :goto_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->b(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/map/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 237
    :goto_2
    return v2

    .line 232
    :cond_0
    check-cast v1, Lcom/google/n/f;

    invoke-virtual {v1}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/n/f;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    iput-object v3, v0, Lcom/google/maps/g/a/gu;->j:Ljava/lang/Object;

    :cond_1
    move-object v0, v3

    goto :goto_1

    .line 231
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 237
    :cond_3
    const/4 v2, -0x1

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/place/station/au;Lcom/google/maps/g/wj;)Lcom/google/android/apps/gmm/place/station/au;
    .locals 9

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/apps/gmm/place/station/au;

    .line 177
    iget-object v2, p1, Lcom/google/android/apps/gmm/place/station/au;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/place/station/au;->b:Ljava/lang/String;

    .line 178
    iget-object v4, p1, Lcom/google/android/apps/gmm/place/station/au;->g:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v5, p1, Lcom/google/android/apps/gmm/place/station/au;->h:Lcom/google/android/apps/gmm/place/station/am;

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/place/station/au;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/station/am;ZLcom/google/maps/g/wj;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/place/station/au;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/station/au;
    .locals 9

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/apps/gmm/place/station/au;

    .line 185
    iget-object v2, p1, Lcom/google/android/apps/gmm/place/station/au;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/apps/gmm/place/station/au;->b:Ljava/lang/String;

    .line 186
    iget-object v4, p1, Lcom/google/android/apps/gmm/place/station/au;->g:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v5, p1, Lcom/google/android/apps/gmm/place/station/au;->h:Lcom/google/android/apps/gmm/place/station/am;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/place/station/au;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/station/am;ZLcom/google/maps/g/wj;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/station/am;)Lcom/google/android/apps/gmm/place/station/au;
    .locals 9
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/android/apps/gmm/place/station/am;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 165
    if-nez p1, :cond_0

    .line 168
    :goto_0
    return-object v7

    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/place/station/au;

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/gmm/place/station/au;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/place/station/am;ZLcom/google/maps/g/wj;Ljava/lang/String;)V

    move-object v7, v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/maps/g/wj;)Lcom/google/b/c/cv;
    .locals 5
    .param p1    # Lcom/google/maps/g/wj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/maps/g/wj;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 255
    if-eqz p1, :cond_2

    .line 256
    iget-object v0, p1, Lcom/google/maps/g/wj;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/hg;->g()Lcom/google/maps/g/a/hg;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/hg;

    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, v0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, v0, Lcom/google/maps/g/a/hg;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/e;->d()Lcom/google/maps/g/a/e;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/e;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/e;

    .line 257
    iget-object v0, v0, Lcom/google/maps/g/a/e;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/go;->h()Lcom/google/maps/g/a/go;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/go;

    invoke-virtual {v0}, Lcom/google/maps/g/a/go;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/gk;

    .line 258
    invoke-static {p0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/j/b;->m()Lcom/google/android/apps/gmm/directions/a/f;

    move-result-object v4

    .line 259
    invoke-interface {v4, v0, p0}, Lcom/google/android/apps/gmm/directions/a/f;->a(Lcom/google/maps/g/a/gk;Landroid/content/Context;)Lcom/google/android/apps/gmm/directions/h/n;

    move-result-object v0

    .line 258
    invoke-virtual {v1, v0}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 263
    :cond_2
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/google/maps/g/wj;)Lcom/google/b/c/cv;
    .locals 5
    .param p0    # Lcom/google/maps/g/wj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/maps/g/wj;",
            ")",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/directions/i/af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 268
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v1

    .line 269
    if-eqz p0, :cond_1

    .line 270
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/maps/g/wj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/maps/g/wj;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/da;->i()Lcom/google/maps/g/a/da;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/da;

    .line 271
    new-instance v3, Lcom/google/android/apps/gmm/directions/i/af;

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/directions/i/af;-><init>(Lcom/google/maps/g/a/da;)V

    invoke-virtual {v1, v3}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    goto :goto_1

    .line 274
    :cond_1
    invoke-virtual {v1}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/place/station/b/f;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->j:Lcom/google/android/apps/gmm/place/station/ao;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/place/station/b/f;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->k:Lcom/google/android/apps/gmm/place/station/ao;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/station/ao;",
            ">;"
        }
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->l:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/h/n;",
            ">;"
        }
    .end annotation

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->m:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/i/af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->n:Lcom/google/b/c/cv;

    return-object v0
.end method

.method public final f()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->i:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/gmm/base/l/a/k;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->o:Lcom/google/android/apps/gmm/place/station/aw;

    return-object v0
.end method

.method public final bridge synthetic h()Lcom/google/android/apps/gmm/place/station/b/d;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/station/au;->h:Lcom/google/android/apps/gmm/place/station/am;

    return-object v0
.end method
