.class public Lcom/google/android/apps/gmm/map/f/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I


# instance fields
.field public final g:Lcom/google/android/apps/gmm/map/b/a/q;

.field public final h:Lcom/google/android/apps/gmm/map/b/a/y;

.field public final i:F

.field public final j:F

.field public final k:F

.field public final l:Lcom/google/android/apps/gmm/map/f/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 46
    invoke-static {}, Lcom/google/android/apps/gmm/map/f/a/d;->values()[Lcom/google/android/apps/gmm/map/f/a/d;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/google/android/apps/gmm/map/f/a/a;->a:I

    .line 49
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->a:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v0, v1, v0

    sput v0, Lcom/google/android/apps/gmm/map/f/a/a;->b:I

    .line 50
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->b:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v0, v1, v0

    sput v0, Lcom/google/android/apps/gmm/map/f/a/a;->c:I

    .line 51
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->c:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v0, v1, v0

    sput v0, Lcom/google/android/apps/gmm/map/f/a/a;->d:I

    .line 52
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->d:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    shl-int v0, v1, v0

    sput v0, Lcom/google/android/apps/gmm/map/f/a/a;->e:I

    .line 53
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/d;->e:Lcom/google/android/apps/gmm/map/f/a/d;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/d;->f:I

    .line 58
    sget v0, Lcom/google/android/apps/gmm/map/f/a/a;->a:I

    shl-int v0, v1, v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/google/android/apps/gmm/map/f/a/a;->f:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V
    .locals 7

    .prologue
    const/high16 v1, 0x42b40000    # 90.0f

    const/high16 v6, 0x43b40000    # 360.0f

    const/4 v0, 0x0

    .line 347
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348
    const-string v2, "Null camera target"

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_0
    const-string v2, "Null camera lookAhead"

    if-nez p5, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_1
    cmpg-float v2, p3, v0

    if-gtz v2, :cond_4

    move p3, v0

    .line 358
    :cond_2
    :goto_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 359
    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 360
    const/high16 v1, 0x40000000    # 2.0f

    const/high16 v2, 0x41a80000    # 21.0f

    .line 361
    invoke-static {p2, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 360
    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iput v1, p0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 362
    add-float/2addr v0, p3

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    .line 363
    float-to-double v0, p4

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_3

    rem-float v0, p4, v6

    add-float p4, v0, v6

    :cond_3
    rem-float v0, p4, v6

    iput v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    .line 364
    invoke-static {p5}, Lcom/google/android/apps/gmm/map/f/a/e;->a(Lcom/google/android/apps/gmm/map/f/a/e;)Lcom/google/android/apps/gmm/map/f/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 365
    return-void

    .line 354
    :cond_4
    cmpl-float v2, p3, v1

    if-lez v2, :cond_2

    move p3, v1

    .line 355
    goto :goto_0
.end method

.method public static a(F)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 7

    .prologue
    const/high16 v6, 0x47800000    # 65536.0f

    .line 528
    float-to-double v0, p0

    const-wide v2, 0x3f91df46a2529d39L    # 0.017453292519943295

    mul-double/2addr v0, v2

    .line 529
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 530
    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v3, v4

    mul-float/2addr v3, v6

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 531
    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    return-object v2
.end method

.method public static final a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 15

    .prologue
    const/16 v14, 0x1a

    const/16 v6, 0x13

    const/4 v13, 0x1

    const/16 v12, 0x15

    const/4 v11, 0x2

    .line 424
    invoke-virtual {p0, v13, v14}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/e/a/a/a/b;

    .line 425
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v14}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lcom/google/e/a/a/a/b;

    .line 426
    if-eqz v2, :cond_0

    if-nez v3, :cond_1

    .line 427
    :cond_0
    const/4 v0, 0x0

    .line 447
    :goto_0
    return-object v0

    .line 430
    :cond_1
    invoke-virtual {v2, v13, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    .line 431
    const/4 v4, 0x3

    invoke-virtual {v2, v4, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    .line 432
    invoke-virtual {v2, v11, v6}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v8

    .line 433
    const/4 v2, 0x4

    invoke-virtual {p0, v2, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    long-to-int v2, v6

    invoke-static {v2}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v7

    .line 434
    invoke-virtual {v3, v11, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v6, v2

    .line 436
    new-instance v10, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v10, v4, v5, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 438
    iget-wide v2, v10, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    float-to-double v4, v7

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->b(DDDI)D

    move-result-wide v0

    double-to-float v3, v0

    .line 440
    const/4 v2, 0x0

    .line 441
    const/4 v1, 0x0

    .line 442
    invoke-virtual {p0, v11, v14}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 443
    if-eqz v0, :cond_2

    .line 444
    invoke-virtual {v0, v13, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v1, v4

    invoke-static {v1}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v1

    .line 445
    invoke-virtual {v0, v11, v12}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    .line 447
    :goto_1
    new-instance v5, Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/f/a/c;-><init>()V

    iput-object v10, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v10, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v8, v10, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/apps/gmm/map/b/a/y;->a(DD)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    iput-object v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iput v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iput v0, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/map/f/a/c;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget v2, v5, Lcom/google/android/apps/gmm/map/f/a/c;->c:F

    iget v3, v5, Lcom/google/android/apps/gmm/map/f/a/c;->d:F

    iget v4, v5, Lcom/google/android/apps/gmm/map/f/a/c;->e:F

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/c;->f:Lcom/google/android/apps/gmm/map/f/a/e;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/map/f/a/a;-><init>(Lcom/google/android/apps/gmm/map/b/a/q;FFFLcom/google/android/apps/gmm/map/f/a/e;)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public static final a(Lcom/google/maps/a/a;)Lcom/google/android/apps/gmm/map/f/a/a;
    .locals 1

    .prologue
    .line 385
    sget-object v0, Lcom/google/maps/a/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/n/at;Lcom/google/e/a/a/a/d;)Lcom/google/e/a/a/a/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/f/a/a;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/map/f/a/a;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/google/android/apps/gmm/map/f/a/c;
    .locals 1

    .prologue
    .line 369
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/a/c;-><init>()V

    return-object v0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/f/a/a;)Lcom/google/android/apps/gmm/map/f/a/c;
    .locals 1

    .prologue
    .line 374
    new-instance v0, Lcom/google/android/apps/gmm/map/f/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/map/f/a/c;-><init>(Lcom/google/android/apps/gmm/map/f/a/a;)V

    return-object v0
.end method

.method public static final a(Lcom/google/android/apps/gmm/map/f/a/a;FFII)Lcom/google/maps/a/a;
    .locals 10

    .prologue
    .line 393
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 394
    int-to-float v0, p4

    div-float/2addr v0, p1

    float-to-int v6, v0

    .line 395
    int-to-float v0, p3

    div-float/2addr v0, p1

    float-to-int v8, v0

    .line 398
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    float-to-double v0, v0

    iget-wide v2, v7, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    float-to-double v4, p2

    .line 397
    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/b/a/p;->a(DDDI)D

    move-result-wide v0

    .line 400
    invoke-static {}, Lcom/google/maps/a/a;->newBuilder()Lcom/google/maps/a/c;

    move-result-object v2

    .line 401
    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v3

    iget-wide v4, v7, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 402
    iget v9, v3, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v9, v9, 0x2

    iput v9, v3, Lcom/google/maps/a/g;->a:I

    iput-wide v4, v3, Lcom/google/maps/a/g;->c:D

    iget-wide v4, v7, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 403
    iget v7, v3, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v7, v7, 0x1

    iput v7, v3, Lcom/google/maps/a/g;->a:I

    iput-wide v4, v3, Lcom/google/maps/a/g;->b:D

    .line 404
    iget v4, v3, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v3, Lcom/google/maps/a/g;->a:I

    iput-wide v0, v3, Lcom/google/maps/a/g;->d:D

    .line 401
    iget-object v0, v2, Lcom/google/maps/a/c;->b:Lcom/google/n/ao;

    invoke-virtual {v3}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v3, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    .line 405
    invoke-static {}, Lcom/google/maps/a/i;->newBuilder()Lcom/google/maps/a/k;

    move-result-object v0

    .line 406
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget v3, v0, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/maps/a/k;->a:I

    iput v1, v0, Lcom/google/maps/a/k;->b:F

    .line 407
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    iget v3, v0, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v3, v3, 0x2

    iput v3, v0, Lcom/google/maps/a/k;->a:I

    iput v1, v0, Lcom/google/maps/a/k;->c:F

    const/4 v1, 0x0

    .line 408
    iget v3, v0, Lcom/google/maps/a/k;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/maps/a/k;->a:I

    iput v1, v0, Lcom/google/maps/a/k;->d:F

    .line 405
    iget-object v1, v2, Lcom/google/maps/a/c;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/a/k;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    .line 409
    invoke-static {}, Lcom/google/maps/a/m;->newBuilder()Lcom/google/maps/a/o;

    move-result-object v0

    .line 410
    iget v1, v0, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/a/o;->a:I

    iput v8, v0, Lcom/google/maps/a/o;->b:I

    .line 411
    iget v1, v0, Lcom/google/maps/a/o;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/a/o;->a:I

    iput v6, v0, Lcom/google/maps/a/o;->c:I

    .line 409
    iget-object v1, v2, Lcom/google/maps/a/c;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/a/o;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    .line 412
    iget v0, v2, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v2, Lcom/google/maps/a/c;->a:I

    iput p2, v2, Lcom/google/maps/a/c;->e:F

    .line 413
    invoke-virtual {v2}, Lcom/google/maps/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/a/d;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 315
    sget-object v0, Lcom/google/android/apps/gmm/map/f/a/b;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/f/a/d;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 327
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x21

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid camera position property "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 325
    :goto_0
    return-object v0

    .line 319
    :pswitch_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 321
    :pswitch_2
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 323
    :pswitch_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 325
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    goto :goto_0

    .line 315
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 457
    if-ne p0, p1, :cond_1

    .line 467
    :cond_0
    :goto_0
    return v0

    .line 459
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/f/a/a;

    if-nez v2, :cond_2

    move v0, v1

    .line 460
    goto :goto_0

    .line 462
    :cond_2
    check-cast p1, Lcom/google/android/apps/gmm/map/f/a/a;

    .line 463
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 464
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    .line 465
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    .line 466
    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    iget v3, p1, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 467
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/f/a/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 452
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 473
    new-instance v1, Lcom/google/b/a/ak;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/b/a/aj;->a(Ljava/lang/Class;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ak;-><init>(Ljava/lang/String;)V

    const-string v0, "target"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->g:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 474
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "zoom"

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 475
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "tilt"

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    .line 476
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "bearing"

    iget v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    .line 477
    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    const-string v0, "lookAhead"

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    .line 478
    new-instance v3, Lcom/google/b/a/al;

    invoke-direct {v3}, Lcom/google/b/a/al;-><init>()V

    iget-object v4, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v3, v4, Lcom/google/b/a/al;->c:Lcom/google/b/a/al;

    iput-object v3, v1, Lcom/google/b/a/ak;->a:Lcom/google/b/a/al;

    iput-object v2, v3, Lcom/google/b/a/al;->b:Ljava/lang/Object;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_4
    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/b/a/al;->a:Ljava/lang/String;

    .line 479
    invoke-virtual {v1}, Lcom/google/b/a/ak;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
