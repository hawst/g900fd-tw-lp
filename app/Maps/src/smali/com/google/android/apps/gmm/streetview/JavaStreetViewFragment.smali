.class public Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;
.super Lcom/google/android/apps/gmm/streetview/StreetViewFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/l/av;


# instance fields
.field final a:Z

.field b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

.field c:Landroid/widget/ImageView;

.field d:Lcom/google/android/apps/gmm/base/views/HeaderView;

.field e:Lcom/google/android/apps/gmm/util/h;

.field f:Lcom/google/android/apps/gmm/util/h;

.field g:Ljava/lang/String;

.field private m:Landroid/view/ViewGroup;

.field private n:Landroid/widget/ImageView;

.field private o:Ljava/lang/String;

.field private p:Lcom/google/android/apps/gmm/map/b/a/q;

.field private q:Z

.field private r:Landroid/nfc/NfcAdapter;

.field private s:Lcom/google/android/apps/gmm/streetview/internal/af;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;-><init>(Z)V

    .line 119
    return-void
.end method

.method private constructor <init>(Z)V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;-><init>()V

    .line 101
    new-instance v0, Lcom/google/android/apps/gmm/streetview/c;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/streetview/c;-><init>(Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->s:Lcom/google/android/apps/gmm/streetview/internal/af;

    .line 122
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a:Z

    .line 123
    return-void
.end method

.method private c()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 513
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->b()Lcom/google/r/b/a/wx;

    move-result-object v2

    iget-object v0, v2, Lcom/google/r/b/a/wx;->d:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/String;

    .line 514
    :goto_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cb_client"

    const-string v2, "an_mobile"

    .line 515
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "output"

    const-string v2, "report"

    .line 516
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "panoid"

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->o:Ljava/lang/String;

    .line 517
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cbp"

    .line 518
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cbll"

    .line 519
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hl"

    .line 520
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 521
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 513
    :cond_0
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object v1, v2, Lcom/google/r/b/a/wx;->d:Ljava/lang/Object;

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private i()Ljava/lang/String;
    .locals 7

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v0

    .line 533
    iget v1, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    .line 534
    iget v0, v0, Lcom/google/android/apps/gmm/streetview/b/a;->b:F

    neg-float v0, v0

    .line 536
    const/16 v2, 0x2c

    new-instance v3, Lcom/google/b/a/ab;

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    const-string v2, "1"

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, ""

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/high16 v6, 0x40400000    # 3.0f

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v3, v2, v1, v4}, Lcom/google/b/a/ab;->a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 4

    .prologue
    .line 541
    const-string v0, "0,0"

    .line 542
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v1, :cond_0

    .line 543
    const/16 v0, 0x2c

    new-instance v1, Lcom/google/b/a/ab;

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v2, v2, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/b/a/ab;->a(Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 545
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final J_()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 597
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "maps.google.com"

    .line 598
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "/maps"

    .line 599
    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "layer"

    const-string v2, "c"

    .line 600
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "panoid"

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->o:Ljava/lang/String;

    .line 601
    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cbp"

    .line 602
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cbll"

    .line 603
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 604
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 526
    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/streetview/internal/au;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 551
    iget-object v0, p1, Lcom/google/android/apps/gmm/streetview/internal/au;->a:Lcom/google/android/apps/gmm/streetview/internal/t;

    .line 552
    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->m:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->l:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    .line 553
    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->o:Ljava/lang/String;

    .line 555
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 556
    if-eqz v0, :cond_0

    .line 557
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/c;->h:Lcom/google/android/apps/gmm/util/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->d:Lcom/google/android/apps/gmm/base/views/HeaderView;

    new-instance v0, Lcom/google/android/apps/gmm/streetview/h;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/streetview/h;-><init>(Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;)V

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    new-instance v3, Lcom/google/android/apps/gmm/util/a/b;

    invoke-direct {v3, v1, v2, v1, v0}, Lcom/google/android/apps/gmm/util/a/b;-><init>(Lcom/google/android/apps/gmm/util/a/a;Landroid/view/View;Lcom/google/android/apps/gmm/util/a/a;Ljava/lang/Runnable;)V

    sget v0, Lcom/google/android/apps/gmm/g;->eB:I

    invoke-virtual {v2, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/a/d;

    if-nez v0, :cond_1

    invoke-virtual {v1, v3}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/d;)V

    :goto_0
    sget v0, Lcom/google/android/apps/gmm/g;->eB:I

    invoke-virtual {v2, v0, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 564
    :cond_0
    return-void

    .line 557
    :cond_1
    invoke-virtual {v1, v0, v3}, Lcom/google/android/apps/gmm/util/a/a;->a(Lcom/google/android/apps/gmm/util/a/d;Lcom/google/android/apps/gmm/util/a/d;)Lcom/google/android/apps/gmm/util/a/d;

    goto :goto_0
.end method

.method varargs a(Lcom/google/b/f/cq;Lcom/google/android/apps/gmm/z/b/n;[Lcom/google/b/f/cq;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 582
    array-length v0, p3

    add-int/lit8 v0, v0, 0x1

    new-array v1, v0, [Lcom/google/b/f/cq;

    .line 583
    const/4 v0, 0x1

    array-length v2, p3

    invoke-static {p3, v3, v1, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 584
    aput-object p1, v1, v3

    .line 586
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    .line 587
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v2

    iput-object v1, v2, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 586
    invoke-interface {v0, p2, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 588
    return-void
.end method

.method a(Z)V
    .locals 3

    .prologue
    .line 308
    if-eqz p1, :cond_0

    .line 309
    sget v1, Lcom/google/android/apps/gmm/f;->du:I

    .line 310
    sget v0, Lcom/google/android/apps/gmm/l;->w:I

    .line 315
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 316
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 317
    return-void

    .line 312
    :cond_0
    sget v1, Lcom/google/android/apps/gmm/f;->dt:I

    .line 313
    sget v0, Lcom/google/android/apps/gmm/l;->y:I

    goto :goto_0
.end method

.method public final b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 135
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->q:Z

    if-eqz v0, :cond_0

    .line 136
    sget v0, Lcom/google/android/apps/gmm/l;->H:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_0
    new-instance v1, Lcom/google/android/apps/gmm/a/a/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/a/a/a;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    invoke-static {p1}, Lcom/google/android/apps/gmm/a/a/b;->a(Landroid/view/View;)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 149
    return-void

    .line 138
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->P:I

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 141
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->q:Z

    if-eqz v0, :cond_2

    .line 142
    sget v0, Lcom/google/android/apps/gmm/l;->R:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 144
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/l;->nk:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method final b(Z)V
    .locals 5

    .prologue
    .line 322
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 323
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 325
    new-instance v1, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;-><init>(Z)V

    .line 326
    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 328
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 329
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 330
    invoke-virtual {v0, p0}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 331
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 332
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 334
    sget-object v1, Lcom/google/b/f/bc;->i:Lcom/google/b/f/bc;

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/r/b/a/a;->b:Lcom/google/r/b/a/a;

    :goto_0
    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->eX:Lcom/google/b/f/t;

    aput-object v4, v0, v3

    invoke-virtual {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a(Lcom/google/b/f/cq;Lcom/google/android/apps/gmm/z/b/n;[Lcom/google/b/f/cq;)V

    .line 337
    return-void

    .line 334
    :cond_0
    sget-object v0, Lcom/google/r/b/a/a;->c:Lcom/google/r/b/a/a;

    goto :goto_0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 574
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->q:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/b/f/t;->bi:Lcom/google/b/f/t;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/b/f/t;->eX:Lcom/google/b/f/t;

    goto :goto_0
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 1

    .prologue
    .line 592
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->f:Lcom/google/android/apps/gmm/feedback/a/d;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onCreate(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->r:Landroid/nfc/NfcAdapter;

    .line 129
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 1

    .prologue
    .line 471
    sget v0, Lcom/google/android/apps/gmm/i;->b:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 472
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v10, -0x2

    const-wide v6, 0x412e848000000000L    # 1000000.0

    const/4 v1, 0x0

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 154
    .line 157
    if-eqz p3, :cond_3

    .line 160
    :goto_0
    if-eqz p3, :cond_7

    .line 161
    const-string v0, "panoId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->o:Ljava/lang/String;

    .line 162
    const-string v0, "latLng"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_0

    .line 164
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->p:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/u;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v6

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iget-wide v4, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    long-to-int v1, v4

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/b/a/u;-><init>(II)V

    move-object v1, v0

    .line 166
    :cond_0
    const-string v0, "userOrientation"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/streetview/b/a;

    .line 167
    const-string v2, "address"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    .line 168
    if-nez v0, :cond_6

    .line 169
    new-instance v0, Lcom/google/android/apps/gmm/streetview/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/b/a;-><init>()V

    .line 170
    const-string v2, "initialYaw"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    iput v2, v0, Lcom/google/android/apps/gmm/streetview/b/a;->a:F

    move-object v2, v0

    .line 172
    :goto_1
    const-string v0, "placemarkLatLng"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/q;

    .line 173
    const-string v3, "isInnerSpace"

    invoke-virtual {p3, v3, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->q:Z

    move-object v7, v0

    move-object v6, v2

    move-object v5, v1

    .line 176
    :goto_2
    new-instance v0, Lcom/google/android/apps/gmm/base/views/aa;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/views/aa;-><init>(Landroid/content/Context;)V

    .line 177
    iput-object p0, v0, Lcom/google/android/apps/gmm/base/views/aa;->d:Landroid/app/Fragment;

    const v1, 0x103006b

    .line 178
    iput v1, v0, Lcom/google/android/apps/gmm/base/views/aa;->c:I

    sget v1, Lcom/google/android/apps/gmm/h;->aj:I

    .line 179
    iput v1, v0, Lcom/google/android/apps/gmm/base/views/aa;->b:I

    .line 180
    new-instance v1, Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/base/views/HeaderView;-><init>(Lcom/google/android/apps/gmm/base/views/aa;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->d:Lcom/google/android/apps/gmm/base/views/HeaderView;

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->d:Lcom/google/android/apps/gmm/base/views/HeaderView;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/HeaderView;->setTitle(Ljava/lang/CharSequence;)V

    .line 183
    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 185
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->m:Landroid/view/ViewGroup;

    .line 187
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a:Z

    new-instance v0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->o:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/c/a;ZLjava/lang/String;Lcom/google/android/apps/gmm/map/b/a/u;Lcom/google/android/apps/gmm/streetview/b/a;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->m:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->c:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->c:Landroid/widget/ImageView;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    const/high16 v4, -0x1000000

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->m:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/g;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;

    invoke-direct {v0, p0, v1}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment$2;-><init>(Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/e;->bu:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iget-object v4, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v4, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a(Z)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    new-instance v3, Lcom/google/android/apps/gmm/streetview/e;

    invoke-direct {v3, p0}, Lcom/google/android/apps/gmm/streetview/e;-><init>(Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x41800000    # 16.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xc

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    sget v3, Lcom/google/android/apps/gmm/f;->dj:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sget v4, Lcom/google/android/apps/gmm/f;->di:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v4, 0xb2

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    new-instance v4, Landroid/graphics/drawable/LayerDrawable;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/graphics/drawable/Drawable;

    aput-object v3, v5, v8

    const/4 v3, 0x1

    aput-object v0, v5, v3

    invoke-direct {v4, v5}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-lt v0, v3, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setColorFilter(I)V

    const/16 v0, 0xb

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v8, v8, v8, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_5

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->m:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 189
    :cond_1
    if-eqz v7, :cond_2

    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iput-object v7, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->e:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v1, :cond_2

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/b/a/h;->a(Lcom/google/android/apps/gmm/map/b/a/u;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->a(Lcom/google/android/apps/gmm/map/b/a/q;)V

    .line 193
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->m:Landroid/view/ViewGroup;

    return-object v0

    .line 157
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p3

    goto/16 :goto_0

    .line 187
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_5
    invoke-virtual {v2, v8, v8, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    goto :goto_4

    :cond_6
    move-object v2, v0

    goto/16 :goto_1

    :cond_7
    move-object v7, v1

    move-object v6, v1

    move-object v5, v1

    goto/16 :goto_2
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 568
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onDestroyView()V

    .line 569
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    .line 570
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 487
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    move v6, v4

    .line 508
    :goto_0
    return v6

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->e:Lcom/google/android/apps/gmm/util/h;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/util/h;->a(Z)V

    .line 491
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    if-eqz v0, :cond_1

    .line 492
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->a:Z

    if-nez v0, :cond_2

    move v0, v6

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/util/h;->a(Z)V

    .line 494
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->dn:I

    if-ne v0, v1, :cond_3

    .line 495
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->I()Lcom/google/android/apps/gmm/share/a/b;

    move-result-object v0

    .line 496
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->J_()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    new-array v5, v4, [Lcom/google/android/apps/gmm/share/a/a;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/share/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Lcom/google/android/apps/gmm/share/a/a;)V

    goto :goto_0

    :cond_2
    move v0, v4

    .line 492
    goto :goto_1

    .line 499
    :cond_3
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/apps/gmm/g;->cH:I

    if-ne v0, v1, :cond_4

    .line 501
    sget-object v0, Lcom/google/b/f/t;->ey:Lcom/google/b/f/t;

    new-instance v1, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v2, Lcom/google/r/b/a/a;->a:Lcom/google/r/b/a/a;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    new-array v2, v6, [Lcom/google/b/f/cq;

    sget-object v3, Lcom/google/b/f/t;->eX:Lcom/google/b/f/t;

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->a(Lcom/google/b/f/cq;Lcom/google/android/apps/gmm/z/b/n;[Lcom/google/b/f/cq;)V

    .line 504
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 505
    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 508
    :cond_4
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v6

    goto :goto_0
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 369
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onPause()V

    .line 370
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->onPause()V

    .line 373
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->e:Lcom/google/android/apps/gmm/util/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    iput-object v3, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    :cond_1
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 376
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    if-eqz v0, :cond_3

    .line 377
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->cancel()V

    iput-object v3, v0, Lcom/google/android/apps/gmm/util/h;->b:Landroid/animation/AnimatorSet;

    :cond_2
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/h;->a:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 379
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 381
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->c:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->s:Lcom/google/android/apps/gmm/streetview/internal/af;

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/ac;->b:Lcom/google/android/apps/gmm/streetview/internal/af;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iput-object v3, v0, Lcom/google/android/apps/gmm/streetview/internal/ac;->b:Lcom/google/android/apps/gmm/streetview/internal/af;

    .line 386
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 387
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 476
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->e:Lcom/google/android/apps/gmm/util/h;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/h;->a(Z)V

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    if-eqz v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/util/h;->a(Z)V

    .line 483
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 391
    invoke-super {p0}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onResume()V

    .line 392
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->onResume()V

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    sget v1, Lcom/google/android/apps/gmm/l;->S:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 403
    new-instance v0, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 404
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    const/4 v1, 0x0

    .line 405
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->d:Lcom/google/android/apps/gmm/base/views/HeaderView;

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/base/views/HeaderView;->a(Landroid/view/View;Z)Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    const/4 v1, 0x0

    .line 407
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->s:Z

    sget-object v1, Lcom/google/android/apps/gmm/base/activities/z;->a:Lcom/google/android/apps/gmm/base/activities/z;

    .line 408
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->u:Lcom/google/android/apps/gmm/base/activities/z;

    .line 409
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    .line 410
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object p0, v1, Lcom/google/android/apps/gmm/base/activities/p;->O:Lcom/google/android/apps/gmm/base/a/a;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/streetview/f;-><init>(Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;)V

    .line 411
    iget-object v2, v0, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/activities/p;->J:Lcom/google/android/apps/gmm/base/activities/y;

    .line 421
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v0

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 423
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->d:Lcom/google/android/apps/gmm/base/views/HeaderView;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/util/h;->a(Landroid/app/Fragment;Landroid/view/View;)Lcom/google/android/apps/gmm/util/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->e:Lcom/google/android/apps/gmm/util/h;

    .line 424
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->k_()Lcom/google/android/apps/gmm/p/b/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/g;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 425
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->n:Landroid/widget/ImageView;

    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/util/h;->a(Landroid/app/Fragment;Landroid/view/View;)Lcom/google/android/apps/gmm/util/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->f:Lcom/google/android/apps/gmm/util/h;

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    new-instance v1, Lcom/google/android/apps/gmm/streetview/g;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/streetview/g;-><init>(Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->h:Lcom/google/android/apps/gmm/streetview/internal/ar;

    iput-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ar;->b:Lcom/google/android/apps/gmm/streetview/internal/at;

    .line 445
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->c:Lcom/google/android/apps/gmm/streetview/internal/ac;

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->s:Lcom/google/android/apps/gmm/streetview/internal/af;

    iput-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ac;->b:Lcom/google/android/apps/gmm/streetview/internal/af;

    .line 446
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 447
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 349
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    if-eqz v0, :cond_1

    .line 354
    const-string v1, "panoId"

    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v2, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->h:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    const-string v0, "address"

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->g:Lcom/google/android/apps/gmm/streetview/internal/ak;

    iget-object v1, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/ak;->f:Lcom/google/android/apps/gmm/streetview/internal/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/internal/t;->i:Lcom/google/android/apps/gmm/map/b/a/u;

    .line 357
    :goto_1
    if-eqz v0, :cond_0

    .line 358
    const-string v1, "latLng"

    .line 359
    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/u;->a:I

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/u;->b:I

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(II)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 358
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 361
    :cond_0
    const-string v0, "userOrientation"

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;->b:Lcom/google/android/apps/gmm/streetview/GmmStreetViewSurfaceView;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/internal/StreetViewSurfaceView;->f:Lcom/google/android/apps/gmm/streetview/internal/b;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/streetview/internal/b;->c()Lcom/google/android/apps/gmm/streetview/b/a;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 362
    :cond_1
    return-void

    .line 354
    :cond_2
    const-string v0, ""

    goto :goto_0

    .line 356
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
