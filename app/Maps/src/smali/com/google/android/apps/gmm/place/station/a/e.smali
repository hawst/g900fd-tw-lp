.class public Lcom/google/android/apps/gmm/place/station/a/e;
.super Lcom/google/android/libraries/curvular/ay;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/libraries/curvular/ay",
        "<",
        "Lcom/google/android/apps/gmm/place/station/b/d;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Lcom/google/android/libraries/curvular/a;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/libraries/curvular/a;

    const v1, -0xe0e0f

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/a;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/place/station/a/e;->a:Lcom/google/android/libraries/curvular/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/libraries/curvular/ay;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/libraries/curvular/cs;
    .locals 9

    .prologue
    const v8, 0xffffff

    .line 36
    const/16 v0, 0x8

    new-array v1, v0, [Lcom/google/android/libraries/curvular/cu;

    const/4 v0, 0x0

    const/4 v2, -0x1

    .line 37
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->av:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    const/4 v2, -0x2

    .line 38
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->al:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x2

    const-wide/high16 v4, 0x4048000000000000L    # 48.0

    .line 40
    new-instance v3, Lcom/google/android/libraries/curvular/b;

    invoke-static {v4, v5}, Lcom/google/b/g/a;->a(D)Z

    move-result v0

    if-eqz v0, :cond_0

    double-to-int v4, v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Landroid/util/TypedValue;->data:I

    :goto_0
    invoke-direct {v3, v0}, Lcom/google/android/libraries/curvular/b;-><init>(Landroid/util/TypedValue;)V

    sget-object v0, Lcom/google/android/libraries/curvular/g;->aH:Lcom/google/android/libraries/curvular/g;

    invoke-static {v0, v3}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    const/4 v0, 0x3

    .line 42
    invoke-static {}, Lcom/google/android/apps/gmm/base/h/b;->b()Lcom/google/android/libraries/curvular/au;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bl:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x4

    const v2, 0x800013

    .line 44
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->W:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/place/station/a/e;->a:Lcom/google/android/libraries/curvular/a;

    .line 46
    sget-object v3, Lcom/google/android/libraries/curvular/g;->m:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x6

    .line 48
    sget v2, Lcom/google/android/apps/gmm/d;->N:I

    invoke-static {v2}, Lcom/google/android/libraries/curvular/c;->a(I)Lcom/google/android/libraries/curvular/c;

    move-result-object v2

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bK:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v2}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v2, 0x7

    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/curvular/ay;->n:Ljava/lang/Class;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/place/station/b/d;->l()Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/android/libraries/curvular/g;->bG:Lcom/google/android/libraries/curvular/g;

    invoke-static {v3, v0}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v0

    aput-object v0, v1, v2

    .line 36
    new-instance v0, Lcom/google/android/libraries/curvular/cs;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/curvular/cs;-><init>([Lcom/google/android/libraries/curvular/cu;)V

    const-string v1, "android.widget.TextView"

    sget-object v2, Lcom/google/android/libraries/curvular/g;->ah:Lcom/google/android/libraries/curvular/g;

    invoke-static {v2, v1}, Lcom/google/android/libraries/curvular/br;->a(Ljava/lang/Enum;Ljava/lang/Object;)Lcom/google/android/libraries/curvular/ah;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/curvular/cs;->a(Lcom/google/android/libraries/curvular/ah;)Lcom/google/android/libraries/curvular/cs;

    move-result-object v0

    return-object v0

    .line 40
    :cond_0
    const-wide/high16 v6, 0x4060000000000000L    # 128.0

    mul-double/2addr v4, v6

    sget-object v0, Ljava/math/RoundingMode;->HALF_EVEN:Ljava/math/RoundingMode;

    invoke-static {v4, v5, v0}, Lcom/google/b/g/a;->a(DLjava/math/RoundingMode;)I

    move-result v4

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    and-int/2addr v4, v8

    shl-int/lit8 v4, v4, 0x8

    or-int/lit8 v4, v4, 0x11

    iput v4, v0, Landroid/util/TypedValue;->data:I

    goto :goto_0
.end method
