.class Lcom/google/android/apps/gmm/map/internal/d/c/f;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Lcom/google/android/apps/gmm/map/internal/d/c/e;

.field b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/d/c/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/c/a;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/e;)V
    .locals 1

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 112
    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 113
    :cond_1
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->b:Ljava/lang/String;

    .line 114
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->a:Lcom/google/android/apps/gmm/map/internal/d/c/e;

    .line 115
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->b:Ljava/lang/String;

    const-string v1, "data:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 125
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->b:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/c/a;->d:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->a:Lcom/google/android/apps/gmm/map/internal/d/c/e;

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/internal/d/c/e;->a:Landroid/graphics/Bitmap;

    .line 128
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/c/a;->a:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->a:Lcom/google/android/apps/gmm/map/internal/d/c/e;

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 129
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->c:Lcom/google/android/apps/gmm/map/internal/d/c/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/f;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/a;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method
