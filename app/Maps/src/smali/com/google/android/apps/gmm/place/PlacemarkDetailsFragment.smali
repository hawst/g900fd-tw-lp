.class public abstract Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/b/c;


# instance fields
.field public c:Z

.field d:Lcom/google/android/apps/gmm/shared/net/i;

.field e:Lcom/google/maps/g/hy;

.field public f:Lcom/google/android/apps/gmm/mylocation/b/a;

.field public g:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/lang/String;

.field private final n:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    .line 65
    new-instance v0, Lcom/google/android/apps/gmm/place/bg;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/bg;-><init>(Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->n:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c()V

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    new-instance v2, Lcom/google/android/apps/gmm/e/a/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/e/a/a;-><init>(Lcom/google/android/apps/gmm/base/g/c;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 176
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    .line 178
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->e:Lcom/google/maps/g/hy;

    invoke-interface {v1, v0, v2, p0}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/place/b/c;)Lcom/google/android/apps/gmm/shared/net/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    .line 180
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 2

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 215
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/place/bh;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/bh;-><init>(Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 191
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    if-ne p1, v0, :cond_0

    .line 195
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    .line 198
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v5

    iget-object v3, v0, Lcom/google/android/apps/gmm/base/g/c;->c:Ljava/lang/String;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_9

    :cond_2
    move v3, v2

    :goto_1
    if-nez v3, :cond_3

    move v1, v2

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->i()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    :cond_4
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v1, v5, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    :cond_5
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->i:Z

    if-eqz v1, :cond_6

    iput-boolean v2, v5, Lcom/google/android/apps/gmm/base/g/g;->j:Z

    :cond_6
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->h:Z

    if-eqz v1, :cond_7

    iput-boolean v2, v5, Lcom/google/android/apps/gmm/base/g/g;->i:Z

    :cond_7
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_8

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v1, v5, Lcom/google/android/apps/gmm/base/g/g;->c:Lcom/google/android/apps/gmm/base/g/h;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/g/h;->d:Lcom/google/android/apps/gmm/base/g/a;

    :cond_8
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 200
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    .line 201
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c()V

    .line 204
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/base/g/b;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->d()V

    goto :goto_0

    :cond_9
    move v3, v1

    .line 198
    goto :goto_1
.end method

.method protected abstract c()V
.end method

.method public i()V
    .locals 0

    .prologue
    .line 237
    return-void
.end method

.method public final m()Lcom/google/android/apps/gmm/feedback/a/d;
    .locals 2

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->k:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    sget-object v1, Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;->d:Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;

    if-ne v0, v1, :cond_0

    .line 243
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->e:Lcom/google/android/apps/gmm/feedback/a/d;

    .line 245
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/feedback/a/d;->d:Lcom/google/android/apps/gmm/feedback/a/d;

    goto :goto_0
.end method

.method public final n()Lcom/google/android/apps/gmm/base/g/c;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onActivityCreated(Landroid/os/Bundle;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->w()Lcom/google/android/apps/gmm/mylocation/b/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/i;->i()Lcom/google/android/apps/gmm/mylocation/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/b;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a(Lcom/google/android/apps/gmm/base/g/b;)V

    .line 119
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 81
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onCreate(Landroid/os/Bundle;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->m:Ljava/lang/String;

    .line 85
    if-eqz p1, :cond_1

    .line 92
    :goto_0
    const-string v0, "loggingParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    .line 93
    instance-of v1, v0, Lcom/google/maps/g/hy;

    if-eqz v1, :cond_0

    .line 94
    check-cast v0, Lcom/google/maps/g/hy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->e:Lcom/google/maps/g/hy;

    .line 97
    :cond_0
    const-string v0, "loaded"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    .line 98
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemark"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 89
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object p1

    goto :goto_0

    .line 102
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 103
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    if-eqz v0, :cond_4

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    .line 107
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->n:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->a:Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/views/AbstractHeaderView;->setVisibility(I)V

    .line 111
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->n:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 124
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onDestroy()V

    .line 125
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 146
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onPause()V

    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->b()V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->f()V

    .line 153
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->d:Lcom/google/android/apps/gmm/shared/net/i;

    .line 154
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onResume()V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->j:Lcom/google/android/apps/gmm/base/g/e;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->m:Ljava/lang/String;

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v0

    .line 133
    if-eq v1, v0, :cond_0

    if-eqz v1, :cond_3

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 135
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->a()V

    .line 139
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    if-eqz v0, :cond_2

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->f:Lcom/google/android/apps/gmm/mylocation/b/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/mylocation/b/a;->d()V

    .line 142
    :cond_2
    return-void

    .line 133
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragmentWithActionBar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 159
    const-string v0, "loaded"

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    const-string v1, "placemark"

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 161
    return-void
.end method
