.class public final enum Lcom/google/android/apps/gmm/map/i/g;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/map/i/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/map/i/g;

.field public static final enum b:Lcom/google/android/apps/gmm/map/i/g;

.field public static final enum c:Lcom/google/android/apps/gmm/map/i/g;

.field private static final synthetic e:[Lcom/google/android/apps/gmm/map/i/g;


# instance fields
.field d:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 150
    new-instance v0, Lcom/google/android/apps/gmm/map/i/g;

    const-string v1, "TOP"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/apps/gmm/map/i/g;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/i/g;->a:Lcom/google/android/apps/gmm/map/i/g;

    new-instance v0, Lcom/google/android/apps/gmm/map/i/g;

    const-string v1, "CENTER"

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/apps/gmm/map/i/g;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/i/g;->b:Lcom/google/android/apps/gmm/map/i/g;

    new-instance v0, Lcom/google/android/apps/gmm/map/i/g;

    const-string v1, "BOTTOM"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/apps/gmm/map/i/g;-><init>(Ljava/lang/String;IF)V

    sput-object v0, Lcom/google/android/apps/gmm/map/i/g;->c:Lcom/google/android/apps/gmm/map/i/g;

    .line 149
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/i/g;

    sget-object v1, Lcom/google/android/apps/gmm/map/i/g;->a:Lcom/google/android/apps/gmm/map/i/g;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/map/i/g;->b:Lcom/google/android/apps/gmm/map/i/g;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/apps/gmm/map/i/g;->c:Lcom/google/android/apps/gmm/map/i/g;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/apps/gmm/map/i/g;->e:[Lcom/google/android/apps/gmm/map/i/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(F)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 160
    iput p3, p0, Lcom/google/android/apps/gmm/map/i/g;->d:F

    .line 161
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/i/g;
    .locals 1

    .prologue
    .line 149
    const-class v0, Lcom/google/android/apps/gmm/map/i/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/i/g;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/map/i/g;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/apps/gmm/map/i/g;->e:[Lcom/google/android/apps/gmm/map/i/g;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/map/i/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/map/i/g;

    return-object v0
.end method
