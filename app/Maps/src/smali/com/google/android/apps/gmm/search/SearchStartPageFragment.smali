.class public Lcom/google/android/apps/gmm/search/SearchStartPageFragment;
.super Lcom/google/android/apps/gmm/suggest/SuggestFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/suggest/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/gmm/suggest/SuggestFragment;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/search/SearchStartPageFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/android/apps/gmm/search/SearchStartPageFragment;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 39
    new-instance v1, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;-><init>()V

    .line 40
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ap;->a:Ljava/lang/String;

    .line 41
    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/al;->g()Ljava/lang/String;

    move-result-object v0

    .line 42
    :cond_0
    invoke-static {v0, p2, p3, p1}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->a(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/suggest/k;

    move-result-object v0

    .line 51
    invoke-virtual {v1, p0, v0, v2, v2}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)V

    .line 53
    return-object v1
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/apps/gmm/search/SearchStartPageFragment;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;-><init>()V

    .line 59
    invoke-static {p1, p2, p3, v2}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->a(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/suggest/k;

    move-result-object v1

    .line 68
    invoke-virtual {v0, p0, v1, v2, v2}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->b(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/suggest/k;Landroid/app/Fragment;Landroid/app/Fragment;)V

    .line 70
    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/suggest/k;
    .locals 5
    .param p3    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)",
            "Lcom/google/android/apps/gmm/suggest/k;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    .line 78
    new-instance v1, Lcom/google/android/apps/gmm/suggest/k;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/suggest/k;-><init>()V

    .line 79
    sget-object v2, Lcom/google/android/apps/gmm/suggest/e/c;->b:Lcom/google/android/apps/gmm/suggest/e/c;

    iget-object v3, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/suggest/l;->a(Lcom/google/android/apps/gmm/suggest/e/c;)V

    .line 80
    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, p1}, Lcom/google/android/apps/gmm/suggest/l;->b(Ljava/lang/String;)V

    .line 81
    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, p0}, Lcom/google/android/apps/gmm/suggest/l;->a(Ljava/lang/String;)V

    .line 82
    sget-object v2, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-object v3, v1, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/startpage/d/d;->b(Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 83
    sget-object v2, Lcom/google/o/h/a/dq;->b:Lcom/google/o/h/a/dq;

    iget-object v3, v1, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/o/h/a/dq;)V

    .line 84
    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(Z)V

    .line 85
    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Z)V

    .line 86
    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->b:Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v2, v4}, Lcom/google/android/apps/gmm/startpage/d/d;->d(Z)V

    .line 87
    if-eqz p2, :cond_0

    :goto_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->a(I)V

    .line 89
    const v0, 0x12000003

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/suggest/l;->b(I)V

    .line 91
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/suggest/l;->e(Z)V

    .line 92
    sget-object v0, Lcom/google/b/f/t;->cE:Lcom/google/b/f/t;

    iget-object v2, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    iput-object v0, v2, Lcom/google/android/apps/gmm/suggest/l;->c:Lcom/google/b/f/t;

    .line 93
    iget-object v0, v1, Lcom/google/android/apps/gmm/suggest/k;->a:Lcom/google/android/apps/gmm/suggest/l;

    iput-object p3, v0, Lcom/google/android/apps/gmm/suggest/l;->b:Lcom/google/android/apps/gmm/x/o;

    .line 95
    return-object v1

    .line 87
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 8

    .prologue
    const/16 v7, 0x9

    const/4 v6, 0x2

    const/16 v5, 0x1c

    const/4 v2, 0x1

    .line 116
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/suggest/e/d;->l:Lcom/google/e/a/a/a/b;

    .line 123
    if-eqz v1, :cond_4

    .line 124
    new-instance v3, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 125
    invoke-virtual {v1, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    iput-object v0, v4, Lcom/google/android/apps/gmm/base/g/i;->b:Ljava/lang/String;

    .line 127
    invoke-virtual {v1, v6, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    .line 130
    iget-object v0, v1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v7}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    .line 132
    const/16 v0, 0x1a

    invoke-virtual {v1, v7, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 134
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v4

    .line 135
    invoke-virtual {v0, v2, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v4, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 136
    invoke-virtual {v0, v6, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    .line 137
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 138
    iput-object v0, v3, Lcom/google/android/apps/gmm/base/g/g;->s:Lcom/google/android/apps/gmm/z/b/l;

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v0

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 147
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v1

    .line 146
    invoke-interface {v0, v1, p1, p2, p3}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V

    goto :goto_0

    .line 130
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 149
    :cond_4
    new-instance v1, Lcom/google/android/apps/gmm/base/placelists/a/e;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/placelists/a/e;-><init>()V

    .line 150
    iput-boolean v2, v1, Lcom/google/android/apps/gmm/base/placelists/a/e;->j:Z

    .line 151
    const-class v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/google/android/apps/gmm/search/aq;->a(Lcom/google/android/apps/gmm/suggest/e/d;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;Lcom/google/android/apps/gmm/base/placelists/a/e;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 106
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchStartPageFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    move-object v1, p1

    move-object v3, v2

    move-object v4, p2

    move-object v5, p3

    .line 107
    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/cardui/a/a;->a(Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;[BLcom/google/maps/g/hy;Lcom/google/android/apps/gmm/suggest/d/e;)V

    .line 111
    :cond_0
    return-void
.end method

.method protected final c()Lcom/google/android/apps/gmm/suggest/a/a;
    .locals 0

    .prologue
    .line 100
    return-object p0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 158
    sget-object v0, Lcom/google/b/f/t;->eb:Lcom/google/b/f/t;

    return-object v0
.end method
