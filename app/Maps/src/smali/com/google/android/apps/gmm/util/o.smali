.class public Lcom/google/android/apps/gmm/util/o;
.super Landroid/net/UrlQuerySanitizer;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/net/UrlQuerySanitizer;-><init>()V

    .line 28
    return-void
.end method

.method private static a(C)I
    .locals 1

    .prologue
    .line 127
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    .line 128
    add-int/lit8 v0, p0, -0x30

    .line 134
    :goto_0
    return v0

    .line 129
    :cond_0
    const/16 v0, 0x41

    if-lt p0, v0, :cond_1

    const/16 v0, 0x46

    if-gt p0, v0, :cond_1

    .line 130
    add-int/lit8 v0, p0, -0x41

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 131
    :cond_1
    const/16 v0, 0x61

    if-lt p0, v0, :cond_2

    const/16 v0, 0x66

    if-gt p0, v0, :cond_2

    .line 132
    add-int/lit8 v0, p0, -0x61

    add-int/lit8 v0, v0, 0xa

    goto :goto_0

    .line 134
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/o;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    .line 85
    :try_start_0
    const-string v0, "utf-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    .line 86
    array-length v2, v1

    .line 89
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 91
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    .line 92
    aget-byte v4, v1, v0

    and-int/lit16 v4, v4, 0xff

    int-to-char v4, v4

    .line 93
    const/16 v5, 0x2b

    if-ne v4, v5, :cond_0

    .line 94
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 91
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_0
    const/16 v5, 0x25

    if-ne v4, v5, :cond_1

    add-int/lit8 v5, v0, 0x2

    if-ge v5, v2, :cond_1

    .line 97
    add-int/lit8 v5, v0, 0x1

    aget-byte v5, v1, v5

    and-int/lit16 v5, v5, 0xff

    int-to-char v5, v5

    invoke-static {v5}, Lcom/google/android/apps/gmm/util/o;->a(C)I

    move-result v5

    .line 98
    add-int/lit8 v6, v0, 0x2

    aget-byte v6, v1, v6

    and-int/lit16 v6, v6, 0xff

    int-to-char v6, v6

    invoke-static {v6}, Lcom/google/android/apps/gmm/util/o;->a(C)I

    move-result v6

    .line 99
    if-ltz v5, :cond_1

    if-ltz v6, :cond_1

    .line 100
    shl-int/lit8 v4, v5, 0x4

    add-int/2addr v4, v6

    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 101
    add-int/lit8 v0, v0, 0x2

    .line 102
    goto :goto_1

    .line 105
    :cond_1
    invoke-virtual {v3, v4}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_1

    .line 109
    :catch_0
    move-exception v0

    :goto_2
    return-object p0

    .line 107
    :cond_2
    const-string v0, "utf-8"

    invoke-virtual {v3, v0}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_2
.end method


# virtual methods
.method public unescape(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-static {p1}, Lcom/google/android/apps/gmm/util/o;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
