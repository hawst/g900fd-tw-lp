.class public Lcom/google/android/apps/gmm/car/k/a;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Landroid/view/ViewGroup;

.field final c:Landroid/view/View;

.field final d:Lcom/google/android/libraries/curvular/ce;

.field e:Lcom/google/android/apps/gmm/car/k/e;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/libraries/curvular/bd;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    sget-object v0, Lcom/google/android/apps/gmm/car/k/e;->a:Lcom/google/android/apps/gmm/car/k/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    .line 36
    iput-object p1, p0, Lcom/google/android/apps/gmm/car/k/a;->a:Ljava/lang/String;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 37
    :cond_0
    iput-object p3, p0, Lcom/google/android/apps/gmm/car/k/a;->b:Landroid/view/ViewGroup;

    .line 38
    const-class v0, Lcom/google/android/apps/gmm/car/k/f;

    const/4 v1, 0x0

    invoke-virtual {p2, v0, p3, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;Z)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/k/a;->c:Landroid/view/View;

    .line 39
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/k/a;->c:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 40
    new-instance v0, Lcom/google/android/apps/gmm/car/k/b;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/car/k/b;-><init>(Lcom/google/android/apps/gmm/car/k/a;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/k/a;->d:Lcom/google/android/libraries/curvular/ce;

    .line 46
    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/apps/gmm/car/k/e;->b:Lcom/google/android/apps/gmm/car/k/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/k/a;->e:Lcom/google/android/apps/gmm/car/k/e;

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/k/a;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 97
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 98
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/car/k/c;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/k/c;-><init>(Lcom/google/android/apps/gmm/car/k/a;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 110
    return-void
.end method

.method a(Z)V
    .locals 4

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/k/a;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 116
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 117
    if-eqz p1, :cond_0

    .line 118
    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 122
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/car/k/d;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/car/k/d;-><init>(Lcom/google/android/apps/gmm/car/k/a;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 134
    return-void

    .line 120
    :cond_0
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method
