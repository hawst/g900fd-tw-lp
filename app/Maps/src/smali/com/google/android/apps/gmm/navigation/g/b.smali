.class public Lcom/google/android/apps/gmm/navigation/g/b;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->NAVIGATION_INTERNAL:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/g;

.field public b:Z

.field private final d:Lcom/google/android/apps/gmm/shared/net/ad;

.field private final e:Lcom/google/android/apps/gmm/navigation/b/c;

.field private final f:Lcom/google/android/apps/gmm/car/a/g;

.field private final g:Lcom/google/android/apps/gmm/navigation/g/h;

.field private final h:Lcom/google/android/apps/gmm/shared/net/a/l;

.field private final i:Lcom/google/android/apps/gmm/navigation/g/b/e;

.field private final j:Lcom/google/android/apps/gmm/navigation/g/u;

.field private final k:Lcom/google/android/apps/gmm/navigation/g/a;

.field private final l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/navigation/g/g;",
            ">;"
        }
    .end annotation
.end field

.field private m:Z

.field private n:Lcom/google/android/apps/gmm/navigation/g/i;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private o:Lcom/google/android/apps/gmm/navigation/g/g;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/apps/gmm/navigation/g/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/navigation/g/b;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/navigation/b/c;Lcom/google/android/apps/gmm/car/a/g;Lcom/google/android/apps/gmm/navigation/g/h;Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/net/a/l;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/b/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/b/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->l:Ljava/util/List;

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->b:Z

    .line 100
    const-string v0, "netEnvironment"

    if-nez p1, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/ad;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->d:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 101
    const-string v0, "navigationServiceController"

    if-nez p2, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p2, Lcom/google/android/apps/gmm/navigation/b/c;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/g/b;->e:Lcom/google/android/apps/gmm/navigation/b/c;

    .line 103
    const-string v0, "projectedModeController"

    if-nez p3, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast p3, Lcom/google/android/apps/gmm/car/a/g;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/g/b;->f:Lcom/google/android/apps/gmm/car/a/g;

    .line 105
    invoke-interface {p1}, Lcom/google/android/apps/gmm/shared/net/ad;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    const-string v1, "eventBus"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    check-cast v0, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 106
    const-string v0, "locationSimulation"

    if-nez p4, :cond_4

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    check-cast p4, Lcom/google/android/apps/gmm/navigation/g/h;

    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/g/b;->g:Lcom/google/android/apps/gmm/navigation/g/h;

    .line 107
    const-string v0, "navigationParameters"

    .line 108
    if-nez p6, :cond_5

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v0, p6

    check-cast v0, Lcom/google/android/apps/gmm/shared/net/a/l;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    .line 109
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/u;

    invoke-direct {v0, p1, p5, p6}, Lcom/google/android/apps/gmm/navigation/g/u;-><init>(Lcom/google/android/apps/gmm/shared/net/ad;Lcom/google/android/apps/gmm/shared/net/a;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->j:Lcom/google/android/apps/gmm/navigation/g/u;

    .line 110
    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/navigation/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->k:Lcom/google/android/apps/gmm/navigation/g/a;

    .line 111
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 191
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/g;

    .line 193
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_0

    .line 194
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :goto_2
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 193
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 194
    :cond_2
    new-instance v1, Lcom/google/android/apps/gmm/navigation/g/b/h;

    iget-object v4, v0, Lcom/google/android/apps/gmm/navigation/g/g;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/navigation/g/s;->a()Lcom/google/android/apps/gmm/navigation/g/b/k;

    move-result-object v0

    invoke-direct {v1, v4, v0}, Lcom/google/android/apps/gmm/navigation/g/b/h;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/navigation/g/b/k;)V

    move-object v0, v1

    goto :goto_2

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    invoke-static {v2}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/e;->f:Ljava/util/List;

    .line 198
    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->n:Lcom/google/android/apps/gmm/navigation/g/i;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    if-nez v0, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/g;

    .line 233
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    if-eqz v1, :cond_7

    .line 234
    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    iget v1, v1, Lcom/google/android/apps/gmm/navigation/g/s;->j:I

    const/4 v5, 0x2

    if-gt v1, v5, :cond_4

    move v1, v2

    :goto_2
    if-nez v1, :cond_2

    .line 235
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->o:Lcom/google/android/apps/gmm/navigation/g/g;

    .line 236
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->j:Lcom/google/android/apps/gmm/navigation/g/u;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    move v1, v3

    .line 233
    goto :goto_1

    :cond_4
    move v1, v3

    .line 234
    goto :goto_2

    :cond_5
    move v1, v3

    goto :goto_2

    .line 236
    :cond_6
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/s;->f:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b;->f:Lcom/google/android/apps/gmm/car/a/g;

    .line 237
    iget-boolean v3, v3, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    .line 236
    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/r/a/w;Z)Lcom/google/android/apps/gmm/navigation/g/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->n:Lcom/google/android/apps/gmm/navigation/g/i;

    goto :goto_0

    .line 241
    :cond_7
    new-array v2, v2, [Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/g;->a:Lcom/google/android/apps/gmm/map/r/a/ap;

    aput-object v1, v2, v3

    .line 242
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/e;->d:Lcom/google/maps/g/a/hm;

    sget-object v3, Lcom/google/maps/g/a/hr;->c:Lcom/google/maps/g/a/hr;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/b;->d:Lcom/google/android/apps/gmm/shared/net/ad;

    .line 243
    invoke-interface {v4}, Lcom/google/android/apps/gmm/shared/net/ad;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v4

    .line 242
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/maps/g/a/hr;Lcom/google/android/apps/gmm/shared/c/f;Ljava/util/TimeZone;)Lcom/google/r/b/a/afz;

    move-result-object v4

    .line 244
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->o:Lcom/google/android/apps/gmm/navigation/g/g;

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->j:Lcom/google/android/apps/gmm/navigation/g/u;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    .line 247
    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/e;->d:Lcom/google/maps/g/a/hm;

    sget-object v5, Lcom/google/maps/g/wq;->b:Lcom/google/maps/g/wq;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/b;->f:Lcom/google/android/apps/gmm/car/a/g;

    .line 250
    iget-boolean v5, v5, Lcom/google/android/apps/gmm/car/a/g;->a:Z

    .line 245
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/apps/gmm/navigation/g/u;->a(Lcom/google/android/apps/gmm/map/r/b/a;[Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;Z)Lcom/google/android/apps/gmm/navigation/g/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->n:Lcom/google/android/apps/gmm/navigation/g/i;

    goto/16 :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 284
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    new-instance v0, Lcom/google/android/apps/gmm/navigation/g/b/d;

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/navigation/g/b/d;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/e;)V

    new-instance v2, Lcom/google/android/apps/gmm/navigation/j/a/b;

    const/4 v3, 0x0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/d;

    invoke-direct {v2, v3, v0}, Lcom/google/android/apps/gmm/navigation/j/a/b;-><init>(Lcom/google/android/apps/gmm/navigation/g/b/f;Lcom/google/android/apps/gmm/navigation/g/b/d;)V

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 285
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/map/location/a;)V
    .locals 10
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 153
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/location/a;->a:Lcom/google/android/apps/gmm/p/d/f;

    check-cast v0, Lcom/google/android/apps/gmm/map/r/b/a;

    .line 154
    if-nez v0, :cond_1

    .line 188
    :cond_0
    :goto_0
    return-void

    .line 157
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iput-object v0, v3, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 159
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-boolean v3, v3, Lcom/google/android/apps/gmm/map/r/b/e;->a:Z

    if-eqz v3, :cond_4

    move v3, v4

    :goto_1
    if-eqz v3, :cond_9

    .line 160
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    if-eqz v3, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/a;->k:Lcom/google/android/apps/gmm/map/r/b/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/b/e;->b:Lcom/google/android/apps/gmm/map/internal/c/az;

    .line 161
    :cond_2
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    if-eqz v3, :cond_5

    move v3, v4

    :goto_2
    if-lez v3, :cond_8

    .line 162
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v5, v1, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    if-eqz v5, :cond_7

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/az;->c:Ljava/lang/String;

    :goto_3
    iput-object v1, v3, Lcom/google/android/apps/gmm/navigation/g/b/e;->e:Ljava/lang/String;

    .line 173
    :goto_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v3, v2

    :goto_5
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/navigation/g/g;

    .line 174
    iget-object v6, v1, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v6, :cond_3

    iget-object v6, v1, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/b/a;)V

    .line 175
    :cond_3
    iget-object v6, v1, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v6, :cond_a

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/navigation/g/s;->i:Z

    :goto_6
    or-int/2addr v1, v3

    move v3, v1

    .line 176
    goto :goto_5

    :cond_4
    move v3, v2

    .line 159
    goto :goto_1

    .line 161
    :cond_5
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    if-nez v3, :cond_6

    move v3, v2

    goto :goto_2

    :cond_6
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    array-length v3, v3

    goto :goto_2

    .line 162
    :cond_7
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/az;->b:[Lcom/google/android/apps/gmm/map/internal/c/z;

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/z;->d:Ljava/lang/String;

    goto :goto_3

    .line 166
    :cond_8
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget v1, v1, Lcom/google/android/apps/gmm/map/internal/c/az;->e:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x1a

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Road: priority="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/apps/gmm/navigation/g/b/e;->e:Ljava/lang/String;

    goto :goto_4

    .line 169
    :cond_9
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iput-object v1, v3, Lcom/google/android/apps/gmm/navigation/g/b/e;->e:Ljava/lang/String;

    goto :goto_4

    :cond_a
    move v1, v2

    .line 175
    goto :goto_6

    .line 177
    :cond_b
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->a()V

    .line 179
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->k:Lcom/google/android/apps/gmm/navigation/g/a;

    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/g/a;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v5, :cond_c

    iget-object v5, v1, Lcom/google/android/apps/gmm/navigation/g/a;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/map/r/b/a;->distanceTo(Landroid/location/Location;)F

    move-result v5

    const/high16 v6, 0x42480000    # 50.0f

    cmpl-float v5, v5, v6

    if-gtz v5, :cond_c

    if-eqz v3, :cond_f

    iget-boolean v5, v1, Lcom/google/android/apps/gmm/navigation/g/a;->b:Z

    if-nez v5, :cond_f

    :cond_c
    iput-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/a;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/navigation/g/a;->b:Z

    move v0, v2

    :goto_7
    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->m:Z

    if-eqz v0, :cond_d

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->e:Lcom/google/android/apps/gmm/navigation/b/c;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    .line 184
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/e;->d:Lcom/google/maps/g/a/hm;

    if-eqz v0, :cond_e

    move v2, v4

    :cond_e
    if-eqz v2, :cond_0

    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->c()V

    .line 186
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->b()V

    goto/16 :goto_0

    .line 179
    :cond_f
    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/a;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/r/b/a;->j:J

    sub-long/2addr v6, v8

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v3}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    iget-boolean v0, v1, Lcom/google/android/apps/gmm/navigation/g/a;->b:Z

    if-eqz v0, :cond_10

    const-wide/16 v0, 0x3c

    :goto_8
    cmp-long v0, v6, v0

    if-ltz v0, :cond_11

    move v0, v4

    goto :goto_7

    :cond_10
    const-wide/16 v0, 0x258

    goto :goto_8

    :cond_11
    move v0, v2

    goto :goto_7
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/base/a/b;)V
    .locals 6
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->g:Lcom/google/android/apps/gmm/navigation/g/h;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/h;->a:Lcom/google/android/apps/gmm/replay/a/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/replay/a/a;->a()V

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->k:Lcom/google/android/apps/gmm/navigation/g/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/navigation/g/a;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/navigation/g/a;->b:Z

    .line 136
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/base/a/b;->a:Lcom/google/android/apps/gmm/navigation/b/a;

    .line 137
    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/b/a;->a:Lcom/google/maps/g/a/hm;

    iput-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/e;->d:Lcom/google/maps/g/a/hm;

    .line 138
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/b/a;->c:Z

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->m:Z

    .line 142
    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/b/a;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    .line 143
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b;->l:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/navigation/g/g;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/g/b;->a:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v5, p0, Lcom/google/android/apps/gmm/navigation/g/b;->h:Lcom/google/android/apps/gmm/shared/net/a/l;

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/apps/gmm/navigation/g/g;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 145
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->b()V

    .line 147
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->c()V

    .line 148
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/g/l;)V
    .locals 7
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 202
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/g/l;->a:Lcom/google/android/apps/gmm/navigation/g/i;

    .line 203
    iget-object v1, p1, Lcom/google/android/apps/gmm/navigation/g/l;->b:Lcom/google/android/apps/gmm/navigation/g/j;

    .line 205
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/g/b;->n:Lcom/google/android/apps/gmm/navigation/g/i;

    if-ne v0, v2, :cond_4

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->o:Lcom/google/android/apps/gmm/navigation/g/g;

    const-string v2, "pendingGuider"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 207
    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ae;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/apps/gmm/navigation/g/b;->c:Ljava/lang/String;

    const-string v1, "No routes?!"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    :goto_0
    iput-object v6, p0, Lcom/google/android/apps/gmm/navigation/g/b;->n:Lcom/google/android/apps/gmm/navigation/g/i;

    .line 209
    iput-object v6, p0, Lcom/google/android/apps/gmm/navigation/g/b;->o:Lcom/google/android/apps/gmm/navigation/g/g;

    .line 214
    :goto_1
    return-void

    .line 207
    :cond_1
    iget-object v0, v1, Lcom/google/android/apps/gmm/navigation/g/j;->b:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->o:Lcom/google/android/apps/gmm/navigation/g/g;

    new-instance v2, Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v3, v1, Lcom/google/android/apps/gmm/navigation/g/g;->b:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v4, v1, Lcom/google/android/apps/gmm/navigation/g/g;->c:Lcom/google/android/apps/gmm/shared/net/a/l;

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/navigation/g/s;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/shared/net/a/l;)V

    iput-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->o:Lcom/google/android/apps/gmm/navigation/g/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/c;->a:Lcom/google/android/apps/gmm/map/r/b/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/g;->d:Lcom/google/android/apps/gmm/navigation/g/s;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/navigation/g/s;->a(Lcom/google/android/apps/gmm/map/r/b/a;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->g:Lcom/google/android/apps/gmm/navigation/g/h;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->g:Lcom/google/android/apps/gmm/navigation/g/h;

    iput-boolean v5, v0, Lcom/google/android/apps/gmm/navigation/g/h;->b:Z

    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->a()V

    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->c()V

    goto :goto_0

    .line 212
    :cond_4
    sget-object v1, Lcom/google/android/apps/gmm/navigation/g/b;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x19

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Dropping route response: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/p/b/e;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-boolean v1, p1, Lcom/google/android/apps/gmm/p/b/e;->a:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/navigation/g/b/c;->c:Z

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/g/b;->i:Lcom/google/android/apps/gmm/navigation/g/b/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/e;->d:Lcom/google/maps/g/a/hm;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 221
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/g/b;->c()V

    .line 223
    :cond_0
    return-void

    .line 220
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
