.class public final Lcom/google/android/apps/gmm/map/b/a/bb;
.super Lcom/google/android/apps/gmm/map/b/a/bd;
.source "PG"


# instance fields
.field public final a:[Lcom/google/android/apps/gmm/map/b/a/y;

.field public final b:[Lcom/google/android/apps/gmm/map/b/a/y;

.field public final c:Lcom/google/android/apps/gmm/map/b/a/m;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/bc;

.field public final e:Lcom/google/android/apps/gmm/map/b/a/ae;

.field private g:[[Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method public constructor <init>([Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/b/a/bd;-><init>()V

    .line 36
    array-length v0, p1

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    .line 37
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 38
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v2, v1, v0

    .line 39
    aget-object v1, p1, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->i(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/m;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/m;-><init>([Lcom/google/android/apps/gmm/map/b/a/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/m;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    .line 46
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->f:Z

    .line 47
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->f:Z

    if-eqz v0, :cond_1

    .line 48
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/b/a/bb;->e()V

    .line 50
    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const v1, 0x20000001

    const v2, -0x20000001

    .line 114
    iget v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-le v0, v3, :cond_0

    iget v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 116
    :goto_0
    if-lez v0, :cond_1

    move v0, v1

    .line 118
    :goto_1
    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v3

    int-to-double v4, v0

    iget v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v0, v3

    int-to-double v6, v0

    div-double/2addr v4, v6

    iget v0, p2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v0, v3

    int-to-double v6, v0

    mul-double/2addr v4, v6

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    int-to-double v6, v0

    add-double/2addr v4, v6

    double-to-int v0, v4

    .line 119
    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v4, p2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v3, v4, :cond_2

    .line 121
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    aput-object v4, v3, v9

    .line 122
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v2, v2, p3

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3, v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    aput-object v3, v2, v8

    .line 128
    :goto_2
    return-void

    .line 114
    :cond_0
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    goto :goto_0

    :cond_1
    move v0, v2

    .line 116
    goto :goto_1

    .line 125
    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    add-int/lit8 v4, p3, -0x1

    aget-object v3, v3, v4

    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4, v1, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    aput-object v4, v3, v9

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v1, v1, p3

    new-instance v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v3, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    aput-object v3, v1, v8

    goto :goto_2
.end method

.method private e()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    .line 75
    const/4 v0, 0x2

    filled-new-array {v8, v0}, [I

    move-result-object v0

    const-class v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    :cond_0
    move v5, v4

    move v0, v4

    move v2, v4

    .line 80
    :goto_0
    const/4 v1, 0x4

    if-ge v5, v1, :cond_4

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v1, v1, v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v6, v6, v5

    invoke-virtual {v1, v6}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v3

    .line 82
    :goto_1
    if-eq v1, v2, :cond_6

    .line 83
    if-lez v5, :cond_1

    if-ge v0, v7, :cond_1

    .line 84
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    add-int/lit8 v6, v5, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v6, v6, v5

    invoke-direct {p0, v2, v6, v0}, Lcom/google/android/apps/gmm/map/b/a/bb;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V

    .line 86
    add-int/lit8 v0, v0, 0x1

    .line 90
    :cond_1
    :goto_2
    if-lez v5, :cond_2

    .line 91
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    add-int/lit8 v6, v0, -0x1

    aget-object v2, v2, v6

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v6, v6, v5

    aput-object v6, v2, v3

    .line 93
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v2, v2, v0

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v6, v6, v5

    aput-object v6, v2, v4

    .line 94
    add-int/lit8 v2, v0, 0x1

    .line 80
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v2

    move v2, v1

    goto :goto_0

    :cond_3
    move v1, v4

    .line 81
    goto :goto_1

    .line 96
    :cond_4
    if-ge v0, v8, :cond_5

    .line 97
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v2, v2, v4

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/map/b/a/bb;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;I)V

    .line 98
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v1, v1, v4

    aput-object v1, v0, v3

    .line 101
    return-void

    :cond_6
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 56
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 57
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;->i(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    iput-object v1, v0, Lcom/google/android/apps/gmm/map/b/a/m;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/m;->b:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a([Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->e:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/b/a/bc;->f:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->f:Z

    .line 62
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->f:Z

    if-eqz v0, :cond_1

    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/b/a/bb;->e()V

    .line 65
    :cond_1
    return-void
.end method

.method public final a(I[Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 225
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->f:Z

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, p1

    aget-object v0, v0, v1

    aput-object v0, p2, v1

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, p1

    aget-object v0, v0, v2

    aput-object v0, p2, v2

    .line 232
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v0, p1

    aput-object v0, p2, v1

    .line 230
    add-int/lit8 v0, p1, 0x1

    rem-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v0, v1, v0

    aput-object v0, p2, v2

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 292
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->f:Z

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/m;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    .line 301
    :goto_0
    return v0

    :cond_0
    move v3, v2

    move v0, v2

    .line 296
    :goto_1
    const/4 v4, 0x6

    if-ge v3, v4, :cond_2

    .line 297
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v4, v4, v3

    aget-object v4, v4, v2

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->g:[[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v5, v5, v3

    aget-object v5, v5, v1

    invoke-static {v4, v5, p1}, Lcom/google/android/apps/gmm/map/b/a/z;->b(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 298
    add-int/lit8 v0, v0, 0x1

    .line 296
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 301
    :cond_2
    if-ne v0, v1, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/bc;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/af;
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->c:Lcom/google/android/apps/gmm/map/b/a/m;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 220
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 274
    if-ne p0, p1, :cond_0

    .line 275
    const/4 v0, 0x1

    .line 281
    :goto_0
    return v0

    .line 277
    :cond_0
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/b/a/bb;

    if-eqz v0, :cond_1

    .line 278
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/bb;

    .line 279
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 281
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/bb;->a:[Lcom/google/android/apps/gmm/map/b/a/y;

    const/4 v4, 0x3

    aget-object v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x5

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ","

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
