.class public Lcom/google/android/apps/gmm/v/aa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/cl;


# static fields
.field public static final s:I

.field static final synthetic y:Z


# instance fields
.field private final a:[Lcom/google/android/apps/gmm/v/co;

.field public final l:[[Lcom/google/android/apps/gmm/v/ai;

.field public volatile m:Lcom/google/android/apps/gmm/v/cj;

.field public n:Z

.field public o:[Lcom/google/android/apps/gmm/v/cj;

.field public p:[I

.field q:[[F

.field public r:Ljava/lang/String;

.field public t:Z

.field public u:B

.field public v:B

.field public w:B

.field public x:Lcom/google/android/apps/gmm/v/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/google/android/apps/gmm/v/aa;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/apps/gmm/v/aa;->y:Z

    .line 152
    const/4 v0, 0x5

    sput v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    return-void

    .line 18
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->n:Z

    .line 160
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    .line 173
    iput-byte v1, p0, Lcom/google/android/apps/gmm/v/aa;->u:B

    .line 176
    iput-byte v1, p0, Lcom/google/android/apps/gmm/v/aa;->v:B

    .line 183
    const/4 v0, -0x1

    iput-byte v0, p0, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 192
    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/co;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    .line 193
    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    sget v1, Lcom/google/android/apps/gmm/v/ai;->n:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lcom/google/android/apps/gmm/v/ai;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/google/android/apps/gmm/v/ai;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    .line 194
    new-instance v0, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->m:Lcom/google/android/apps/gmm/v/cj;

    .line 195
    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/cj;

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->o:[Lcom/google/android/apps/gmm/v/cj;

    .line 196
    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    new-array v0, v0, [[F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->q:[[F

    .line 197
    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->p:[I

    .line 198
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    sparse-switch p0, :sswitch_data_0

    .line 134
    const-string v0, "Unrecognized Pass Mask"

    :goto_0
    return-object v0

    .line 122
    :sswitch_0
    const-string v0, "Stencil Pass"

    goto :goto_0

    .line 124
    :sswitch_1
    const-string v0, "Frame Buffer Object Pass"

    goto :goto_0

    .line 126
    :sswitch_2
    const-string v0, "Opaque Pass"

    goto :goto_0

    .line 128
    :sswitch_3
    const-string v0, "Transparent Pass"

    goto :goto_0

    .line 130
    :sswitch_4
    const-string v0, "Heads Up Display Pass"

    goto :goto_0

    .line 132
    :sswitch_5
    const-string v0, "Debug Hud Pass"

    goto :goto_0

    .line 120
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 369
    sget-object v1, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    if-ne p1, v1, :cond_0

    move p2, v0

    .line 374
    :cond_0
    const/4 v2, -0x1

    .line 375
    const/4 v1, 0x0

    :goto_0
    sget v3, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v1, v3, :cond_4

    .line 376
    shl-int v3, v0, v1

    and-int/2addr v3, p2

    if-eqz v3, :cond_2

    .line 381
    :goto_1
    if-ltz v1, :cond_1

    shl-int/2addr v0, v1

    if-eq p2, v0, :cond_3

    .line 382
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid passMask: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 375
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 384
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v0, v0, v1

    iget v1, p1, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v0, v0, v1

    return-object v0

    :cond_4
    move v1, v2

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 532
    iget v6, p2, Lcom/google/android/apps/gmm/v/bh;->g:I

    .line 533
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->n:Z

    if-nez v0, :cond_0

    iget v0, p3, Lcom/google/android/apps/gmm/v/n;->I:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->p:[I

    aget v2, v2, v6

    if-eq v0, v2, :cond_1

    .line 534
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->o:[Lcom/google/android/apps/gmm/v/cj;

    aget-object v0, v0, v6

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v2, p3, Lcom/google/android/apps/gmm/v/n;->H:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->q:[[F

    aget-object v0, v0, v6

    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v4, v2, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v2, p3, Lcom/google/android/apps/gmm/v/n;->F:[F

    move v3, v1

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 536
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/v/aa;->n:Z

    .line 537
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->p:[I

    iget v1, p3, Lcom/google/android/apps/gmm/v/n;->I:I

    aput v1, v0, v6

    .line 540
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v0, v0, v6

    invoke-virtual {p1, v0, p0, p2, p3}, Lcom/google/android/apps/gmm/v/ad;->a([Lcom/google/android/apps/gmm/v/ai;Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/bh;Lcom/google/android/apps/gmm/v/n;)V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    aget-object v0, v0, v6

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/v/co;->c()V

    .line 542
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/ai;I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 290
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v1, :cond_0

    .line 291
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 295
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/v/ai;->q:Lcom/google/android/apps/gmm/v/aj;

    sget-object v2, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    if-ne v1, v2, :cond_1

    move p2, v0

    .line 299
    :cond_1
    iget-byte v1, p0, Lcom/google/android/apps/gmm/v/aa;->v:B

    or-int/2addr v1, p2

    int-to-byte v1, v1

    iput-byte v1, p0, Lcom/google/android/apps/gmm/v/aa;->v:B

    .line 302
    const/4 v1, 0x0

    :goto_0
    sget v2, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v1, v2, :cond_4

    .line 304
    shl-int v2, v0, v1

    .line 305
    and-int/2addr v2, p2

    if-eqz v2, :cond_3

    .line 306
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v2, v2, v1

    iget-object v3, p1, Lcom/google/android/apps/gmm/v/ai;->q:Lcom/google/android/apps/gmm/v/aj;

    iget v3, v3, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v2, v2, v3

    .line 307
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v3, v1

    iget-object v4, p1, Lcom/google/android/apps/gmm/v/ai;->q:Lcom/google/android/apps/gmm/v/aj;

    iget v4, v4, Lcom/google/android/apps/gmm/v/aj;->o:I

    aput-object p1, v3, v4

    .line 309
    iget-boolean v3, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v3, :cond_3

    .line 312
    if-eqz v2, :cond_2

    .line 313
    iget v3, v2, Lcom/google/android/apps/gmm/v/ai;->s:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lcom/google/android/apps/gmm/v/ai;->s:I

    .line 314
    iget-object v3, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    sget-object v4, Lcom/google/android/apps/gmm/v/ab;->a:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 316
    :cond_2
    if-eqz p1, :cond_3

    .line 317
    iget v2, p1, Lcom/google/android/apps/gmm/v/ai;->s:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Lcom/google/android/apps/gmm/v/ai;->s:I

    .line 318
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->c:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {p1, v2, v3}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 303
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 323
    :cond_4
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/cj;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 579
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v0, :cond_0

    .line 580
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 582
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->m:Lcom/google/android/apps/gmm/v/cj;

    iget-object v1, p1, Lcom/google/android/apps/gmm/v/cj;->a:[F

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/cj;->a:[F

    const/16 v2, 0x10

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 583
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->n:Z

    .line 584
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/co;I)V
    .locals 4
    .param p1    # Lcom/google/android/apps/gmm/v/co;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 209
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v0, :cond_0

    .line 210
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 214
    :cond_0
    const/4 v0, 0x0

    :goto_0
    sget v1, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v0, v1, :cond_5

    .line 216
    const/4 v1, 0x1

    shl-int/2addr v1, v0

    .line 217
    and-int/2addr v1, p2

    if-eqz v1, :cond_4

    .line 218
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    aget-object v1, v1, v0

    .line 219
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    aput-object p1, v2, v0

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->o:[Lcom/google/android/apps/gmm/v/cj;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->o:[Lcom/google/android/apps/gmm/v/cj;

    new-instance v3, Lcom/google/android/apps/gmm/v/cj;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/v/cj;-><init>()V

    aput-object v3, v2, v0

    .line 224
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->q:[[F

    aget-object v2, v2, v0

    if-nez v2, :cond_2

    .line 225
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->q:[[F

    const/16 v3, 0x10

    new-array v3, v3, [F

    aput-object v3, v2, v0

    .line 227
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v2, :cond_4

    .line 229
    if-eqz v1, :cond_3

    .line 230
    iget v2, v1, Lcom/google/android/apps/gmm/v/co;->l:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, Lcom/google/android/apps/gmm/v/co;->l:I

    .line 231
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    sget-object v3, Lcom/google/android/apps/gmm/v/ab;->a:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/gmm/v/co;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 233
    :cond_3
    if-eqz p1, :cond_4

    .line 234
    iget v1, p1, Lcom/google/android/apps/gmm/v/co;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lcom/google/android/apps/gmm/v/co;->l:I

    .line 235
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    sget-object v2, Lcom/google/android/apps/gmm/v/ab;->c:Lcom/google/android/apps/gmm/v/ab;

    invoke-virtual {p1, v1, v2}, Lcom/google/android/apps/gmm/v/co;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 215
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_5
    if-eqz p1, :cond_7

    iget-byte v0, p0, Lcom/google/android/apps/gmm/v/aa;->u:B

    or-int/2addr v0, p2

    .line 242
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v1, :cond_6

    .line 243
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    iget-byte v2, p0, Lcom/google/android/apps/gmm/v/aa;->u:B

    invoke-virtual {v1, p0, v2, v0}, Lcom/google/android/apps/gmm/v/ad;->a(Lcom/google/android/apps/gmm/v/aa;II)V

    .line 245
    :cond_6
    int-to-byte v0, v0

    iput-byte v0, p0, Lcom/google/android/apps/gmm/v/aa;->u:B

    .line 246
    return-void

    .line 240
    :cond_7
    iget-byte v0, p0, Lcom/google/android/apps/gmm/v/aa;->u:B

    xor-int/lit8 v1, p2, -0x1

    and-int/2addr v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 602
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/aa;->r:Ljava/lang/String;

    .line 603
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 476
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-ne v0, v2, :cond_0

    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v0, :cond_0

    .line 525
    :goto_0
    return v1

    .line 484
    :cond_0
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-eqz v0, :cond_3

    .line 485
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    :goto_1
    move v0, v1

    .line 489
    :goto_2
    sget v2, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v0, v2, :cond_5

    .line 490
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    aget-object v2, v2, v0

    .line 491
    if-eqz v2, :cond_2

    .line 492
    iget-boolean v3, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v3, :cond_1

    .line 493
    iget-boolean v3, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-eqz v3, :cond_4

    .line 494
    iget v3, v2, Lcom/google/android/apps/gmm/v/co;->l:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/android/apps/gmm/v/co;->l:I

    .line 499
    :cond_1
    :goto_3
    invoke-virtual {v2, p1, p2}, Lcom/google/android/apps/gmm/v/co;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 489
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 487
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->x:Lcom/google/android/apps/gmm/v/ad;

    goto :goto_1

    .line 496
    :cond_4
    iget v3, v2, Lcom/google/android/apps/gmm/v/co;->l:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Lcom/google/android/apps/gmm/v/co;->l:I

    goto :goto_3

    :cond_5
    move v0, v1

    .line 502
    :goto_4
    sget v2, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v0, v2, :cond_a

    .line 507
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v2, v0

    array-length v4, v3

    move v2, v1

    :goto_5
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    .line 508
    if-eqz v5, :cond_7

    .line 509
    iget-boolean v6, p2, Lcom/google/android/apps/gmm/v/ab;->f:Z

    if-nez v6, :cond_6

    .line 510
    iget-boolean v6, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    if-eqz v6, :cond_8

    .line 511
    iget v6, v5, Lcom/google/android/apps/gmm/v/ai;->s:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v5, Lcom/google/android/apps/gmm/v/ai;->s:I

    .line 516
    :cond_6
    :goto_6
    invoke-virtual {v5, p1, p2}, Lcom/google/android/apps/gmm/v/ai;->a(Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/v/ab;)Z

    .line 507
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 513
    :cond_8
    iget v6, v5, Lcom/google/android/apps/gmm/v/ai;->s:I

    add-int/lit8 v6, v6, -0x1

    iput v6, v5, Lcom/google/android/apps/gmm/v/ai;->s:I

    goto :goto_6

    .line 506
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 520
    :cond_a
    iget-boolean v0, p2, Lcom/google/android/apps/gmm/v/ab;->e:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    .line 525
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/v/aj;I)Lcom/google/android/apps/gmm/v/ai;
    .locals 3

    .prologue
    .line 398
    sget-object v0, Lcom/google/android/apps/gmm/v/aj;->a:Lcom/google/android/apps/gmm/v/aj;

    if-ne p1, v0, :cond_0

    .line 399
    const/4 p2, 0x0

    .line 402
    :cond_0
    if-ltz p2, :cond_1

    sget v0, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-lt p2, v0, :cond_2

    .line 403
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid passIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v0, v0, p2

    iget v1, p1, Lcom/google/android/apps/gmm/v/aj;->o:I

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 571
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v0, :cond_0

    .line 572
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 574
    :cond_0
    int-to-byte v0, p1

    iput-byte v0, p0, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 575
    return-void
.end method

.method final c(I)Lcom/google/android/apps/gmm/v/co;
    .locals 3

    .prologue
    .line 616
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 617
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x30

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Cannot get the vertex data for pass: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 437
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->a:[Lcom/google/android/apps/gmm/v/co;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 438
    if-eqz v4, :cond_1

    .line 439
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/v/co;->b()Z

    move-result v4

    if-nez v4, :cond_1

    .line 457
    :cond_0
    :goto_1
    return v1

    .line 437
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 447
    :goto_2
    sget v2, Lcom/google/android/apps/gmm/v/aa;->s:I

    if-ge v0, v2, :cond_5

    .line 448
    iget-object v2, p0, Lcom/google/android/apps/gmm/v/aa;->l:[[Lcom/google/android/apps/gmm/v/ai;

    aget-object v3, v2, v0

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 449
    if-eqz v5, :cond_3

    .line 450
    invoke-virtual {v5}, Lcom/google/android/apps/gmm/v/ai;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 448
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 447
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 457
    :cond_5
    const/4 v1, 0x1

    goto :goto_1
.end method
