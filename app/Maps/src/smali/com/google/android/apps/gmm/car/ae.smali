.class public Lcom/google/android/apps/gmm/car/ae;
.super Lcom/google/android/gms/car/support/ae;
.source "PG"


# static fields
.field private static final X:Lcom/google/android/apps/gmm/map/x;

.field private static final w:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/gms/car/support/CarAppLayout;

.field private B:Lcom/google/android/apps/gmm/car/ao;

.field private C:Lcom/google/android/apps/gmm/car/au;

.field private D:Landroid/widget/ImageView;

.field private E:Lcom/google/android/apps/gmm/car/drawer/k;

.field private F:Landroid/view/View;

.field private G:Lcom/google/android/apps/gmm/o/e;

.field private H:Lcom/google/android/apps/gmm/car/ar;

.field private final I:Lcom/google/android/apps/gmm/mylocation/n;

.field private J:Lcom/google/android/apps/gmm/car/bg;

.field private K:Lcom/google/android/apps/gmm/car/e/m;

.field private L:Lcom/google/android/apps/gmm/car/d/a;

.field private M:Lcom/google/android/apps/gmm/car/e/d;

.field private N:Lcom/google/android/apps/gmm/car/m/k;

.field private O:Lcom/google/android/apps/gmm/car/c/a;

.field private P:Lcom/google/android/apps/gmm/car/c/c;

.field private Q:Lcom/google/android/apps/gmm/car/j/e;

.field private R:Lcom/google/android/apps/gmm/car/b/b;

.field private S:Lcom/google/android/apps/gmm/mylocation/c/g;

.field private T:Lcom/google/android/apps/gmm/car/b/a;

.field private final U:Ljava/lang/Object;

.field private final V:Lcom/google/android/apps/gmm/directions/a/e;

.field private final W:Lcom/google/android/apps/gmm/car/e/s;

.field a:Z

.field b:Lcom/google/android/apps/gmm/car/m/i;

.field c:Lcom/google/android/apps/gmm/car/ad;

.field d:Lcom/google/android/apps/gmm/car/d/q;

.field e:Lcom/google/android/apps/gmm/car/m/q;

.field f:Lcom/google/android/apps/gmm/car/e/x;

.field g:Z

.field private x:Lcom/google/android/apps/gmm/q/i;

.field private y:Lcom/google/android/apps/gmm/car/a;

.field private z:Lcom/google/android/libraries/curvular/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 116
    const-class v0, Lcom/google/android/apps/gmm/car/ae;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 832
    new-instance v0, Lcom/google/android/apps/gmm/car/an;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/car/an;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/car/ae;->X:Lcom/google/android/apps/gmm/map/x;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/google/android/gms/car/support/ae;-><init>()V

    .line 141
    new-instance v0, Lcom/google/android/apps/gmm/mylocation/n;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/mylocation/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    .line 757
    new-instance v0, Lcom/google/android/apps/gmm/car/aj;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/aj;-><init>(Lcom/google/android/apps/gmm/car/ae;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->U:Ljava/lang/Object;

    .line 776
    new-instance v0, Lcom/google/android/apps/gmm/car/ak;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/ak;-><init>(Lcom/google/android/apps/gmm/car/ae;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->V:Lcom/google/android/apps/gmm/directions/a/e;

    .line 789
    new-instance v0, Lcom/google/android/apps/gmm/car/al;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/car/al;-><init>(Lcom/google/android/apps/gmm/car/ae;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->W:Lcom/google/android/apps/gmm/car/e/s;

    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 710
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ae;->a:Z

    if-nez v0, :cond_1

    iget-object v11, p0, Lcom/google/android/apps/gmm/car/ae;->P:Lcom/google/android/apps/gmm/car/c/c;

    sget-object v0, Lcom/google/android/apps/gmm/car/c/c;->a:Ljava/lang/String;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/car/c/c;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Received intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 713
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/car/ae;->b(Landroid/content/Intent;)V

    .line 715
    :cond_1
    return-void

    .line 710
    :cond_2
    new-instance v0, Lcom/google/android/apps/gmm/l/c;

    iget-object v1, v11, Lcom/google/android/apps/gmm/car/c/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/l/c;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/l/ab;->b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/car/c/d;->a:[I

    iget-object v1, v2, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/l/y;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/apps/gmm/car/c/c;->a:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Can\'t handle this type of IntentAction: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/car/c/c;->a:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/u;->d:Lcom/google/android/apps/gmm/l/aa;

    sget-object v1, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    if-ne v0, v1, :cond_3

    move v0, v9

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Directions intent, navigating: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    iget-object v1, v2, Lcom/google/android/apps/gmm/l/u;->c:Lcom/google/android/apps/gmm/map/r/a/ap;

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->g:Ljava/lang/String;

    :goto_2
    if-nez v3, :cond_6

    const-string v2, ""

    :goto_3
    new-instance v0, Lcom/google/android/apps/gmm/car/bm;

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    iget-object v1, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/ad;->v:Lcom/google/android/apps/gmm/car/c/f;

    if-eqz v2, :cond_7

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->v:Lcom/google/android/apps/gmm/car/c/f;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/car/c/f;->a(Lcom/google/android/apps/gmm/car/bm;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v9

    :goto_4
    if-eqz v1, :cond_8

    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/j;->a()V

    move v6, v9

    goto/16 :goto_0

    :cond_3
    move v0, v6

    goto :goto_1

    :cond_4
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/ap;->c:Ljava/lang/String;

    goto :goto_2

    :cond_5
    move-object v3, v4

    goto :goto_2

    :cond_6
    move-object v2, v3

    goto :goto_3

    :cond_7
    move v1, v6

    goto :goto_4

    :cond_8
    new-instance v1, Lcom/google/android/apps/gmm/car/f/d;

    iget-object v3, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    sget-object v7, Lcom/google/android/apps/gmm/car/f/o;->c:Lcom/google/android/apps/gmm/car/f/o;

    move-object v2, v0

    move v5, v9

    move v8, v6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/car/f/d;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/w;ZZLcom/google/android/apps/gmm/car/f/o;Z)V

    :goto_5
    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    iget v2, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/android/apps/gmm/car/m/f;->a:I

    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    if-nez v1, :cond_c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/car/c/c;->a:Ljava/lang/String;

    new-instance v10, Lcom/google/android/apps/gmm/car/f/d;

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/u;->e:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v2, v2, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v3

    invoke-static {}, Lcom/google/android/apps/gmm/map/r/a/ap;->d()Lcom/google/android/apps/gmm/map/r/a/aq;

    move-result-object v1

    if-eqz v3, :cond_9

    :goto_6
    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->c:Lcom/google/android/apps/gmm/map/b/a/j;

    iput-boolean v3, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->g:Z

    iput-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v1

    new-instance v0, Lcom/google/android/apps/gmm/car/bm;

    if-eqz v3, :cond_a

    move-object v3, v2

    :goto_7
    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/car/bm;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/base/g/c;)V

    iget-object v3, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    sget-object v7, Lcom/google/android/apps/gmm/car/f/o;->a:Lcom/google/android/apps/gmm/car/f/o;

    move-object v1, v10

    move-object v2, v0

    move v5, v9

    move v8, v6

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/gmm/car/f/d;-><init>(Lcom/google/android/apps/gmm/car/bm;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/w;ZZLcom/google/android/apps/gmm/car/f/o;Z)V

    move-object v1, v10

    goto :goto_5

    :cond_9
    move-object v0, v4

    goto :goto_6

    :cond_a
    move-object v3, v4

    goto :goto_7

    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/car/c/c;->a:Ljava/lang/String;

    const-string v0, "Search intent, query: "

    iget-object v1, v2, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_8
    new-instance v1, Lcom/google/android/apps/gmm/car/i/o;

    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v3, v2, Lcom/google/android/apps/gmm/l/u;->b:Ljava/lang/String;

    iget-object v5, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v5, v5, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    iget-object v7, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v7, v7, Lcom/google/android/apps/gmm/car/ad;->e:Lcom/google/android/libraries/curvular/bd;

    iget-object v7, v7, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v8, v11, Lcom/google/android/apps/gmm/car/c/c;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v8}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v8

    invoke-virtual {v2, v5, v7, v8}, Lcom/google/android/apps/gmm/l/u;->a(Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/p/b/a;)Lcom/google/maps/a/a;

    move-result-object v2

    invoke-direct {v1, v0, v3, v6, v2}, Lcom/google/android/apps/gmm/car/i/o;-><init>(Lcom/google/android/apps/gmm/car/ad;Ljava/lang/String;ILcom/google/maps/a/a;)V

    goto/16 :goto_5

    :cond_b
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    :pswitch_3
    sget-object v0, Lcom/google/android/apps/gmm/car/c/c;->a:Ljava/lang/String;

    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->d:Lcom/google/android/apps/gmm/car/c/a;

    iget-object v1, v2, Lcom/google/android/apps/gmm/l/u;->g:Lcom/google/android/apps/gmm/aa/a/a/a;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/l/u;->h:Z

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/car/c/a;->a(Lcom/google/android/apps/gmm/aa/a/a/a;Z)V

    move v6, v9

    goto/16 :goto_0

    :cond_c
    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->a:Lcom/google/android/apps/gmm/car/m/f;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/f;->a()V

    iget-object v0, v11, Lcom/google/android/apps/gmm/car/c/c;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/j;->a()V

    move v6, v9

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    .line 521
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 523
    invoke-super {p0}, Lcom/google/android/gms/car/support/ae;->a()V

    .line 525
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->y:Lcom/google/android/apps/gmm/car/a;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/a;->c:Z

    .line 526
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->R:Lcom/google/android/apps/gmm/car/b/b;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->R:Lcom/google/android/apps/gmm/car/b/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/b/b;->a:Landroid/view/ViewGroup;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/b/b;->f:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 530
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ae;->i()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/car/ae;->c(Landroid/content/Intent;)V

    .line 532
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 533
    return-void
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 702
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 704
    invoke-super {p0, p1}, Lcom/google/android/gms/car/support/ae;->a(Landroid/content/Intent;)V

    .line 705
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/car/ae;->b(Landroid/content/Intent;)V

    .line 706
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/car/ae;->c(Landroid/content/Intent;)V

    .line 707
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 513
    invoke-super {p0, p1}, Lcom/google/android/gms/car/support/ae;->a(Landroid/content/res/Configuration;)V

    .line 516
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->x:Lcom/google/android/apps/gmm/q/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/q/i;->a:Lcom/google/android/apps/gmm/q/a;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/q/i;->a:Lcom/google/android/apps/gmm/q/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/q/a;->b()V

    .line 517
    :cond_0
    return-void
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 43

    .prologue
    .line 170
    sget-object v2, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    if-nez p1, :cond_3

    const-string v2, "null"

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "onCreate with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " savedInstanceState"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 176
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-super {v0, v2}, Lcom/google/android/gms/car/support/ae;->a(Landroid/os/Bundle;)V

    .line 181
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-string v4, "just_in_case"

    const/4 v5, 0x0

    .line 182
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v6

    const-class v7, Lcom/google/android/apps/gmm/car/CarConnectionService;

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    invoke-virtual {v2, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 184
    const/16 v2, 0x200

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/car/ae;->b(I)V

    .line 186
    const/16 v18, 0x0

    .line 187
    const/4 v2, 0x0

    .line 188
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->l()Lcom/google/android/gms/car/m;

    move-result-object v3

    .line 190
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/car/m;->a(Z)V

    .line 191
    invoke-virtual {v3}, Lcom/google/android/gms/car/m;->c()Lcom/google/android/gms/common/api/o;

    move-result-object v39

    .line 192
    if-eqz v39, :cond_4

    invoke-interface/range {v39 .. v39}, Lcom/google/android/gms/common/api/o;->d()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Lcom/google/android/gms/car/a;->c:Lcom/google/android/gms/car/d;

    move-object/from16 v0, v39

    invoke-interface {v3, v0}, Lcom/google/android/gms/car/d;->a(Lcom/google/android/gms/common/api/o;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 194
    :try_start_0
    sget-object v2, Lcom/google/android/gms/car/a;->d:Lcom/google/android/gms/car/f;

    const-string v3, "car_demo_mode"

    const/4 v4, 0x0

    .line 195
    move-object/from16 v0, v39

    invoke-interface {v2, v0, v3, v4}, Lcom/google/android/gms/car/f;->a(Lcom/google/android/gms/common/api/o;Ljava/lang/String;Z)Z

    move-result v2

    .line 196
    if-eqz v2, :cond_0

    .line 197
    sget-object v3, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 199
    :cond_0
    sget-object v3, Lcom/google/android/gms/car/a;->c:Lcom/google/android/gms/car/d;

    move-object/from16 v0, v39

    invoke-interface {v3, v0}, Lcom/google/android/gms/car/d;->b(Lcom/google/android/gms/common/api/o;)Lcom/google/android/gms/car/CarUiInfo;

    move-result-object v3

    .line 200
    sget-object v4, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "CarUiInfo: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    invoke-virtual {v3}, Lcom/google/android/gms/car/CarUiInfo;->a()Z
    :try_end_0
    .catch Lcom/google/android/gms/car/ao; {:try_start_0 .. :try_end_0} :catch_0

    move-result v18

    move v8, v2

    .line 211
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v25

    check-cast v25, Lcom/google/android/apps/gmm/base/a;

    .line 212
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->ag:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v4, 0x1

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, v2, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v5, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->d:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v4, 0x1

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 217
    :cond_2
    new-instance v2, Lcom/google/android/apps/gmm/q/i;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/q/i;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->x:Lcom/google/android/apps/gmm/q/i;

    .line 218
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->p_()Lcom/google/android/apps/gmm/q/a;

    move-result-object v2

    .line 220
    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/q/a;->b:Z

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/q/a;->b()V

    .line 221
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->x:Lcom/google/android/apps/gmm/q/i;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/q/a;->a(Lcom/google/android/apps/gmm/q/d;)V

    .line 223
    new-instance v2, Lcom/google/android/apps/gmm/car/a;

    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/car/a;-><init>(Lcom/google/android/apps/gmm/z/a/b;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->y:Lcom/google/android/apps/gmm/car/a;

    .line 224
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->y:Lcom/google/android/apps/gmm/car/a;

    if-eqz p1, :cond_5

    const/4 v2, 0x1

    :goto_2
    iput-boolean v2, v3, Lcom/google/android/apps/gmm/car/a;->b:Z

    .line 226
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->j()Landroid/view/LayoutInflater;

    move-result-object v23

    .line 227
    if-nez v23, :cond_6

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 170
    :cond_3
    const-string v2, "non-null"

    goto/16 :goto_0

    .line 202
    :catch_0
    move-exception v2

    .line 204
    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v3

    .line 207
    :cond_4
    sget-object v3, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    move v8, v2

    goto/16 :goto_1

    .line 224
    :cond_5
    const/4 v2, 0x0

    goto :goto_2

    .line 230
    :cond_6
    new-instance v2, Lcom/google/android/libraries/curvular/cd;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/libraries/curvular/cd;-><init>(Landroid/content/Context;)V

    .line 231
    new-instance v3, Lcom/google/android/libraries/curvular/bd;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Lcom/google/android/libraries/curvular/bd;-><init>(Landroid/content/Context;Lcom/google/android/libraries/curvular/cc;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    .line 232
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    new-instance v3, Lcom/google/android/libraries/curvular/an;

    const/4 v4, 0x5

    new-array v4, v4, [Lcom/google/android/libraries/curvular/cp;

    const/4 v5, 0x0

    new-instance v6, Lcom/google/android/apps/gmm/car/n/l;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v6, v7}, Lcom/google/android/apps/gmm/car/n/l;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-instance v6, Lcom/google/android/apps/gmm/car/n/g;

    invoke-direct {v6}, Lcom/google/android/apps/gmm/car/n/g;-><init>()V

    aput-object v6, v4, v5

    const/4 v5, 0x2

    new-instance v6, Lcom/google/android/apps/gmm/base/k/p;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    .line 237
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v9

    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v10

    invoke-direct {v6, v7, v9, v10}, Lcom/google/android/apps/gmm/base/k/p;-><init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/z/a/b;Lcom/google/android/apps/gmm/shared/b/a;)V

    aput-object v6, v4, v5

    const/4 v5, 0x3

    new-instance v6, Lcom/google/android/libraries/curvular/c/b;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v6, v7}, Lcom/google/android/libraries/curvular/c/b;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    aput-object v6, v4, v5

    const/4 v5, 0x4

    new-instance v6, Lcom/google/android/libraries/curvular/j;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    invoke-direct {v6, v7}, Lcom/google/android/libraries/curvular/j;-><init>(Lcom/google/android/libraries/curvular/bd;)V

    aput-object v6, v4, v5

    invoke-direct {v3, v4}, Lcom/google/android/libraries/curvular/an;-><init>([Lcom/google/android/libraries/curvular/cp;)V

    .line 232
    iput-object v3, v2, Lcom/google/android/libraries/curvular/bd;->c:Lcom/google/android/libraries/curvular/cp;

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    iget-object v3, v2, Lcom/google/android/libraries/curvular/bd;->c:Lcom/google/android/libraries/curvular/cp;

    if-nez v3, :cond_7

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_7
    iget-object v3, v2, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    iget-object v2, v2, Lcom/google/android/libraries/curvular/bd;->g:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v3, v2}, Landroid/content/Context;->registerComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    .line 242
    new-instance v2, Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/car/support/CarAppLayout;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    .line 243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/car/ae;->a(Landroid/view/View;)V

    .line 246
    new-instance v5, Lcom/google/android/apps/gmm/car/af;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/google/android/apps/gmm/car/af;-><init>(Lcom/google/android/apps/gmm/car/ae;)V

    .line 254
    new-instance v2, Lcom/google/android/apps/gmm/car/ao;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    .line 255
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->l()Lcom/google/android/gms/car/m;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/car/m;->d()Lcom/google/android/gms/car/a/c;

    move-result-object v4

    .line 256
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/gmm/l;->bI:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 257
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v7

    sget v9, Lcom/google/android/apps/gmm/f;->P:I

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    if-eqz v8, :cond_a

    const/4 v8, 0x0

    .line 258
    :goto_3
    invoke-direct/range {v2 .. v8}, Lcom/google/android/apps/gmm/car/ao;-><init>(Lcom/google/android/gms/car/support/CarAppLayout;Lcom/google/android/gms/car/a/c;Lcom/google/android/gms/car/support/p;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    .line 262
    new-instance v5, Lcom/google/android/apps/gmm/map/t;

    invoke-direct {v5}, Lcom/google/android/apps/gmm/map/t;-><init>()V

    .line 263
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v40

    .line 264
    new-instance v6, Landroid/graphics/Point;

    move-object/from16 v0, v40

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, v40

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v6, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    .line 267
    new-instance v2, Lcom/google/android/apps/gmm/car/au;

    sget-object v7, Lcom/google/android/apps/gmm/car/ae;->X:Lcom/google/android/apps/gmm/map/x;

    move-object/from16 v3, v25

    move-object/from16 v4, v23

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/car/au;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/view/LayoutInflater;Lcom/google/android/apps/gmm/map/t;Landroid/graphics/Point;Lcom/google/android/apps/gmm/map/x;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->C:Lcom/google/android/apps/gmm/car/au;

    .line 271
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->C:Lcom/google/android/apps/gmm/car/au;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/au;->a:Lcom/google/android/apps/gmm/base/a;

    iget-object v6, v2, Lcom/google/android/apps/gmm/car/au;->d:Landroid/graphics/Point;

    iput-object v4, v3, Lcom/google/android/apps/gmm/map/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    iput-object v6, v3, Lcom/google/android/apps/gmm/map/t;->i:Landroid/graphics/Point;

    iget-object v6, v2, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v7, v2, Lcom/google/android/apps/gmm/car/au;->b:Landroid/view/LayoutInflater;

    sget-object v8, Lcom/google/android/apps/gmm/map/o/ar;->c:Lcom/google/android/apps/gmm/map/o/ar;

    const/4 v9, 0x0

    new-instance v10, Lcom/google/android/apps/gmm/car/o;

    new-instance v3, Lcom/google/android/apps/gmm/map/aj;

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/au;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/map/aj;-><init>(Landroid/content/Context;)V

    const/high16 v4, 0x41700000    # 15.0f

    invoke-direct {v10, v3, v4}, Lcom/google/android/apps/gmm/car/o;-><init>(Lcom/google/android/apps/gmm/map/s;F)V

    sget-object v11, Lcom/google/android/apps/gmm/map/b/a/ai;->i:Lcom/google/android/apps/gmm/map/b/a/ai;

    const/4 v12, 0x1

    iget-object v13, v2, Lcom/google/android/apps/gmm/car/au;->e:Lcom/google/android/apps/gmm/map/x;

    invoke-virtual/range {v6 .. v13}, Lcom/google/android/apps/gmm/map/t;->a(Landroid/view/LayoutInflater;Lcom/google/android/apps/gmm/map/o/ar;Landroid/widget/TextView;Lcom/google/android/apps/gmm/map/s;Lcom/google/android/apps/gmm/map/b/a/ai;ZLcom/google/android/apps/gmm/map/x;)Landroid/view/View;

    move-result-object v41

    const/4 v3, 0x0

    move-object/from16 v0, v41

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/au;->f:Lcom/google/android/apps/gmm/v/e;

    iget-object v3, v3, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v6, Lcom/google/android/apps/gmm/v/af;

    const/4 v7, 0x1

    invoke-direct {v6, v4, v7}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v3, v6}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->l()V

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    const/4 v3, 0x0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/o;->g(Z)V

    .line 272
    new-instance v2, Landroid/widget/ImageView;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->D:Landroid/widget/ImageView;

    .line 273
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->D:Landroid/widget/ImageView;

    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 274
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->D:Landroid/widget/ImageView;

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    .line 276
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v6

    sget v7, Lcom/google/android/apps/gmm/e;->j:I

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-direct {v3, v4, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 274
    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    new-instance v10, Lcom/google/android/apps/gmm/car/m/f;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/car/m/f;-><init>()V

    .line 281
    new-instance v42, Landroid/widget/FrameLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v42

    invoke-direct {v0, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 282
    new-instance v8, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, v42

    invoke-direct {v8, v2, v0}, Lcom/google/android/apps/gmm/car/views/PriorityFocusingFrameLayout;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 283
    new-instance v6, Lcom/google/android/apps/gmm/car/drawer/k;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    invoke-direct/range {v6 .. v11}, Lcom/google/android/apps/gmm/car/drawer/k;-><init>(Lcom/google/android/apps/gmm/car/ao;Landroid/view/ViewGroup;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/car/m/f;Lcom/google/android/libraries/curvular/bd;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    .line 287
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    const-class v3, Lcom/google/android/apps/gmm/car/bl;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->F:Landroid/view/View;

    .line 288
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->F:Landroid/view/View;

    new-instance v3, Lcom/google/android/apps/gmm/car/ag;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/apps/gmm/car/ag;-><init>(Lcom/google/android/apps/gmm/car/ae;)V

    invoke-static {v2, v3}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 290
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->U:Ljava/lang/Object;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->g()V

    .line 293
    new-instance v2, Lcom/google/android/apps/gmm/car/ah;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/car/ah;-><init>(Lcom/google/android/apps/gmm/car/ae;Lcom/google/android/apps/gmm/base/a;)V

    .line 299
    new-instance v3, Lcom/google/android/apps/gmm/o/e;

    .line 300
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/apps/gmm/o/e;-><init>(Lcom/google/android/apps/gmm/o/f;Lcom/google/android/apps/gmm/shared/b/a;Lcom/google/android/apps/gmm/map/t;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->G:Lcom/google/android/apps/gmm/o/e;

    .line 302
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->G:Lcom/google/android/apps/gmm/o/e;

    const/4 v3, 0x4

    new-array v3, v3, [Lcom/google/android/apps/gmm/o/a/b;

    const/4 v4, 0x0

    new-instance v6, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v7, Lcom/google/android/apps/gmm/o/a/a;->b:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v9, 0x0

    invoke-direct {v6, v7, v9}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    aput-object v6, v3, v4

    const/4 v4, 0x1

    new-instance v6, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v7, Lcom/google/android/apps/gmm/o/a/a;->c:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v9, 0x0

    invoke-direct {v6, v7, v9}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    aput-object v6, v3, v4

    const/4 v4, 0x2

    new-instance v6, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v7, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v9, 0x0

    invoke-direct {v6, v7, v9}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    aput-object v6, v3, v4

    const/4 v4, 0x3

    new-instance v6, Lcom/google/android/apps/gmm/o/a/b;

    sget-object v7, Lcom/google/android/apps/gmm/o/a/a;->e:Lcom/google/android/apps/gmm/o/a/a;

    const/4 v9, 0x0

    invoke-direct {v6, v7, v9}, Lcom/google/android/apps/gmm/o/a/b;-><init>(Lcom/google/android/apps/gmm/o/a/a;Z)V

    aput-object v6, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/o/e;->a([Lcom/google/android/apps/gmm/o/a/b;)V

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->G:Lcom/google/android/apps/gmm/o/e;

    .line 308
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/shared/b/c;->F:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v6}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v3

    .line 307
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/o/e;->a(Z)Z

    .line 311
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->aa:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v4, 0x1

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v6

    if-eqz v6, :cond_8

    iget-object v2, v2, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 313
    :cond_8
    new-instance v2, Lcom/google/android/apps/gmm/car/ar;

    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    .line 314
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->g()Lcom/google/android/apps/gmm/map/internal/d/bd;

    move-result-object v4

    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v6

    invoke-direct {v2, v5, v3, v4, v6}, Lcom/google/android/apps/gmm/car/ar;-><init>(Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/map/internal/d/bd;Lcom/google/android/apps/gmm/shared/b/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->H:Lcom/google/android/apps/gmm/car/ar;

    .line 316
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x41700000    # 15.0f

    move-object/from16 v0, v25

    invoke-virtual {v2, v0, v5, v3, v4}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;F)V

    .line 318
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    if-eqz p1, :cond_9

    invoke-static {}, Lcom/google/android/apps/gmm/map/s/a;->values()[Lcom/google/android/apps/gmm/map/s/a;

    move-result-object v3

    const-string v4, "MY_LOCATION_AUTOPAN"

    sget-object v6, Lcom/google/android/apps/gmm/mylocation/n;->a:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    aget-object v3, v3, v4

    iput-object v3, v2, Lcom/google/android/apps/gmm/mylocation/n;->j:Lcom/google/android/apps/gmm/map/s/a;

    .line 319
    :cond_9
    new-instance v2, Lcom/google/android/apps/gmm/directions/e;

    .line 320
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/car/ae;->V:Lcom/google/android/apps/gmm/directions/a/e;

    sget-object v7, Lcom/google/android/apps/gmm/car/ae;->X:Lcom/google/android/apps/gmm/map/x;

    move-object/from16 v3, v25

    invoke-direct/range {v2 .. v7}, Lcom/google/android/apps/gmm/directions/e;-><init>(Lcom/google/android/apps/gmm/base/a;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/directions/a/e;Lcom/google/android/apps/gmm/map/x;)V

    .line 322
    new-instance v3, Lcom/google/android/apps/gmm/car/bg;

    new-instance v4, Lcom/google/android/apps/gmm/startpage/w;

    .line 323
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v6

    move-object/from16 v0, v25

    invoke-direct {v4, v6, v0, v5}, Lcom/google/android/apps/gmm/startpage/w;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)V

    move-object/from16 v0, v25

    invoke-direct {v3, v0, v5, v4}, Lcom/google/android/apps/gmm/car/bg;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/startpage/w;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->J:Lcom/google/android/apps/gmm/car/bg;

    .line 324
    new-instance v3, Lcom/google/android/apps/gmm/car/e/m;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->W:Lcom/google/android/apps/gmm/car/e/s;

    move-object/from16 v0, v25

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/car/e/m;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/e/s;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->K:Lcom/google/android/apps/gmm/car/e/m;

    .line 326
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    sget-object v4, Lcom/google/android/apps/gmm/mylocation/b/h;->b:Lcom/google/android/apps/gmm/mylocation/b/h;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/mylocation/b/h;)V

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    sget-object v4, Lcom/google/android/apps/gmm/mylocation/b/g;->a:Lcom/google/android/apps/gmm/mylocation/b/g;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/mylocation/b/g;)V

    .line 335
    new-instance v3, Lcom/google/android/apps/gmm/car/m/i;

    invoke-direct {v3}, Lcom/google/android/apps/gmm/car/m/i;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    .line 336
    new-instance v13, Lcom/google/android/apps/gmm/car/m/m;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    invoke-direct {v13, v3, v10}, Lcom/google/android/apps/gmm/car/m/m;-><init>(Lcom/google/android/apps/gmm/car/m/l;Lcom/google/android/apps/gmm/car/m/f;)V

    .line 339
    new-instance v11, Lcom/google/android/apps/gmm/car/d/a;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    .line 340
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v16

    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v17

    move-object v14, v5

    move-object v15, v8

    invoke-direct/range {v11 .. v18}, Lcom/google/android/apps/gmm/car/d/a;-><init>(Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/car/m/m;Lcom/google/android/apps/gmm/map/t;Landroid/widget/FrameLayout;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/util/b/a/a;Z)V

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/apps/gmm/car/ae;->L:Lcom/google/android/apps/gmm/car/d/a;

    .line 343
    new-instance v3, Landroid/widget/FrameLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 344
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v6, -0x1

    const/4 v7, -0x2

    const/16 v9, 0x50

    invoke-direct {v4, v6, v7, v9}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v4}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 347
    new-instance v35, Lcom/google/android/apps/gmm/car/ai;

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/car/ai;-><init>(Lcom/google/android/apps/gmm/car/ae;)V

    .line 354
    new-instance v37, Lcom/google/android/apps/gmm/car/k/h;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    move-object/from16 v0, v37

    invoke-direct {v0, v3, v4}, Lcom/google/android/apps/gmm/car/k/h;-><init>(Landroid/widget/FrameLayout;Lcom/google/android/libraries/curvular/bd;)V

    .line 356
    new-instance v19, Lcom/google/android/apps/gmm/car/ad;

    new-instance v22, Lcom/google/android/apps/gmm/car/drawer/j;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    move-object/from16 v0, v22

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/car/drawer/j;-><init>(Lcom/google/android/apps/gmm/car/drawer/k;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    move-object/from16 v24, v0

    new-instance v27, Lcom/google/android/apps/gmm/car/at;

    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-direct {v0, v5, v4}, Lcom/google/android/apps/gmm/car/at;-><init>(Lcom/google/android/apps/gmm/map/t;Z)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    move-object/from16 v28, v0

    new-instance v30, Lcom/google/android/apps/gmm/search/ah;

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/search/ah;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->J:Lcom/google/android/apps/gmm/car/bg;

    move-object/from16 v31, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->G:Lcom/google/android/apps/gmm/o/e;

    move-object/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->K:Lcom/google/android/apps/gmm/car/e/m;

    move-object/from16 v33, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    move-object/from16 v36, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ae;->L:Lcom/google/android/apps/gmm/car/d/a;

    move-object/from16 v38, v0

    move-object/from16 v20, v10

    move-object/from16 v21, v13

    move-object/from16 v26, v5

    move-object/from16 v29, v2

    move/from16 v34, v18

    invoke-direct/range {v19 .. v38}, Lcom/google/android/apps/gmm/car/ad;-><init>(Lcom/google/android/apps/gmm/car/m/f;Lcom/google/android/apps/gmm/car/m/m;Lcom/google/android/apps/gmm/car/drawer/j;Landroid/view/LayoutInflater;Lcom/google/android/libraries/curvular/bd;Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/car/at;Lcom/google/android/apps/gmm/mylocation/b/f;Lcom/google/android/apps/gmm/directions/a/a;Lcom/google/android/apps/gmm/search/ah;Lcom/google/android/apps/gmm/car/bg;Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/car/e/m;ZLcom/google/android/apps/gmm/car/c;Lcom/google/android/apps/gmm/car/ao;Lcom/google/android/apps/gmm/car/k/h;Lcom/google/android/apps/gmm/car/d/a;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 378
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    new-instance v4, Lcom/google/android/apps/gmm/car/drawer/t;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    invoke-direct {v4, v6}, Lcom/google/android/apps/gmm/car/drawer/t;-><init>(Lcom/google/android/apps/gmm/car/ad;)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    if-nez v4, :cond_b

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 258
    :cond_a
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v8

    goto/16 :goto_3

    .line 378
    :cond_b
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-interface {v2, v4}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 380
    new-instance v2, Lcom/google/android/apps/gmm/car/d/q;

    .line 381
    iget-object v4, v5, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/o;->o()Lcom/google/android/apps/gmm/map/m/l;

    move-result-object v4

    .line 382
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 383
    iget-object v7, v7, Lcom/google/android/apps/gmm/car/ad;->r:Lcom/google/android/apps/gmm/car/k/h;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 384
    iget-object v9, v9, Lcom/google/android/apps/gmm/car/ad;->s:Lcom/google/android/apps/gmm/car/d/a;

    invoke-direct {v2, v4, v6, v7, v9}, Lcom/google/android/apps/gmm/car/d/q;-><init>(Lcom/google/android/apps/gmm/map/m/l;Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/car/k/h;Lcom/google/android/apps/gmm/car/d/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->d:Lcom/google/android/apps/gmm/car/d/q;

    .line 386
    new-instance v2, Lcom/google/android/apps/gmm/car/m/q;

    invoke-direct {v2, v10}, Lcom/google/android/apps/gmm/car/m/q;-><init>(Lcom/google/android/apps/gmm/car/m/f;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    .line 387
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    if-nez v2, :cond_c

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    :cond_c
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/car/m/i;->e()V

    invoke-interface {v2}, Lcom/google/android/apps/gmm/car/m/h;->a()V

    iget-object v6, v4, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v6, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    sget-object v6, Lcom/google/android/apps/gmm/car/m/o;->b:Lcom/google/android/apps/gmm/car/m/o;

    iget-object v2, v4, Lcom/google/android/apps/gmm/car/m/i;->a:Lcom/google/android/apps/gmm/car/m/k;

    if-eqz v2, :cond_d

    iget-object v2, v4, Lcom/google/android/apps/gmm/car/m/i;->b:Lcom/google/android/apps/gmm/car/m/f;

    iget v2, v2, Lcom/google/android/apps/gmm/car/m/f;->a:I

    if-lez v2, :cond_e

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_f

    iput-object v6, v4, Lcom/google/android/apps/gmm/car/m/i;->c:Lcom/google/android/apps/gmm/car/m/o;

    .line 388
    :cond_d
    :goto_5
    new-instance v2, Lcom/google/android/apps/gmm/car/e/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/car/ae;->d:Lcom/google/android/apps/gmm/car/d/q;

    invoke-direct {v2, v4, v6}, Lcom/google/android/apps/gmm/car/e/d;-><init>(Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/d/q;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->M:Lcom/google/android/apps/gmm/car/e/d;

    .line 389
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    if-nez v4, :cond_10

    new-instance v2, Ljava/lang/NullPointerException;

    invoke-direct {v2}, Ljava/lang/NullPointerException;-><init>()V

    throw v2

    .line 387
    :cond_e
    const/4 v2, 0x0

    goto :goto_4

    :cond_f
    invoke-virtual {v4, v6}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/o;)V

    goto :goto_5

    .line 389
    :cond_10
    iget-object v2, v2, Lcom/google/android/apps/gmm/car/m/q;->a:Lcom/google/android/apps/gmm/car/m/i;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->M:Lcom/google/android/apps/gmm/car/e/d;

    invoke-interface {v2, v4}, Lcom/google/android/apps/gmm/car/m/l;->a(Lcom/google/android/apps/gmm/car/m/h;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->u:Lcom/google/android/apps/gmm/car/q;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/q;->b:Landroid/view/View;

    invoke-virtual {v8, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 392
    move-object/from16 v0, v42

    invoke-virtual {v8, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 393
    new-instance v2, Lcom/google/android/apps/gmm/car/m/p;

    move-object/from16 v0, v42

    invoke-direct {v2, v0}, Lcom/google/android/apps/gmm/car/m/p;-><init>(Landroid/widget/FrameLayout;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->N:Lcom/google/android/apps/gmm/car/m/k;

    .line 394
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->N:Lcom/google/android/apps/gmm/car/m/k;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/car/ae;->N:Lcom/google/android/apps/gmm/car/m/k;

    invoke-virtual {v4, v6, v10}, Lcom/google/android/apps/gmm/car/m/i;->a(Lcom/google/android/apps/gmm/car/m/k;Lcom/google/android/apps/gmm/car/m/f;)Landroid/view/View;

    move-result-object v4

    sget-object v6, Lcom/google/android/apps/gmm/car/m/o;->a:Lcom/google/android/apps/gmm/car/m/o;

    invoke-interface {v2, v4, v6}, Lcom/google/android/apps/gmm/car/m/k;->a(Landroid/view/View;Lcom/google/android/apps/gmm/car/m/o;)V

    .line 397
    const/4 v2, 0x1

    iget-object v4, v5, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v4, v2}, Lcom/google/android/apps/gmm/map/o;->a(Z)V

    .line 399
    new-instance v2, Lcom/google/android/apps/gmm/car/c/a;

    .line 400
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->q_()Lcom/google/android/apps/gmm/aa/a/d;

    move-result-object v4

    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v5

    .line 401
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/car/c/a;-><init>(Lcom/google/android/apps/gmm/aa/a/d;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/car/ad;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->O:Lcom/google/android/apps/gmm/car/c/a;

    .line 402
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->O:Lcom/google/android/apps/gmm/car/c/a;

    iget-object v4, v2, Lcom/google/android/apps/gmm/aa/a/e;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v4, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 404
    new-instance v2, Lcom/google/android/apps/gmm/car/c/c;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/car/ae;->O:Lcom/google/android/apps/gmm/car/c/a;

    move-object/from16 v0, v25

    invoke-direct {v2, v0, v4, v5}, Lcom/google/android/apps/gmm/car/c/c;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/car/ad;Lcom/google/android/apps/gmm/car/c/a;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->P:Lcom/google/android/apps/gmm/car/c/c;

    .line 406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->K:Lcom/google/android/apps/gmm/car/e/m;

    const/4 v4, 0x1

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/car/e/m;->h:Z

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/e/m;->f:Lcom/google/android/apps/gmm/car/e/ap;

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/e/ap;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v4

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/e/ap;->c:Ljava/lang/Object;

    invoke-interface {v4, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 408
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    move-object/from16 v0, v41

    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/support/CarAppLayout;->addView(Landroid/view/View;)V

    .line 409
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->D:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/car/support/CarAppLayout;->addView(Landroid/view/View;)V

    .line 410
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    iget-object v4, v4, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/car/support/CarAppLayout;->addView(Landroid/view/View;)V

    .line 411
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/support/CarAppLayout;->addView(Landroid/view/View;)V

    .line 412
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->F:Landroid/view/View;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/support/CarAppLayout;->addView(Landroid/view/View;)V

    .line 417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/support/CarAppLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->bringToFront()V

    .line 421
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v2

    const-string v3, "location"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/location/LocationManager;

    .line 422
    const-string v3, "gps"

    invoke-virtual {v2, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    .line 423
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/car/ae;->a(Z)V

    .line 426
    :cond_11
    move-object/from16 v0, v25

    move-object/from16 v1, v39

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/car/j/e;->a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/gms/common/api/o;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 427
    new-instance v2, Lcom/google/android/apps/gmm/car/j/e;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/apps/gmm/car/j/e;-><init>(Lcom/google/android/apps/gmm/car/ad;Landroid/view/ViewGroup;Lcom/google/android/apps/gmm/car/j/h;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->Q:Lcom/google/android/apps/gmm/car/j/e;

    .line 439
    :cond_12
    invoke-interface/range {v25 .. v25}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v2

    sget-object v3, Lcom/google/android/apps/gmm/shared/b/c;->D:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 441
    new-instance v2, Lcom/google/android/apps/gmm/car/b/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    move-object/from16 v0, v40

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    move-object/from16 v0, v23

    invoke-direct {v2, v0, v3, v4}, Lcom/google/android/apps/gmm/car/b/b;-><init>(Landroid/view/LayoutInflater;Lcom/google/android/gms/car/support/CarAppLayout;F)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/gmm/car/ae;->R:Lcom/google/android/apps/gmm/car/b/b;

    .line 444
    :cond_13
    sget-object v2, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 509
    return-void
.end method

.method a(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 745
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/car/ae;->a:Z

    .line 746
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ae;->a:Z

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 748
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->F:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 749
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/ao;->a(Z)V

    .line 755
    :goto_0
    return-void

    .line 751
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->F:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 752
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 753
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/car/ao;->a(Z)V

    goto :goto_0
.end method

.method protected final b()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 537
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 539
    invoke-super {p0}, Lcom/google/android/gms/car/support/ae;->b()V

    .line 541
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->x()V

    .line 545
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    iget-object v3, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v3, v3, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    iget-object v4, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v4, v4, Lcom/google/android/apps/gmm/car/ad;->g:Lcom/google/android/apps/gmm/map/t;

    .line 546
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ae;->k()Landroid/content/res/Resources;

    move-result-object v5

    .line 545
    invoke-virtual {v0, v3, v4, v5, v2}, Lcom/google/android/apps/gmm/mylocation/n;->a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/res/Resources;Z)V

    .line 547
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->C:Lcom/google/android/apps/gmm/car/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t;->j:Z

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/o;->b()V

    new-instance v3, Lcom/google/android/apps/gmm/map/u;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/map/o;->p()Landroid/view/View;

    move-result-object v4

    invoke-direct {v3, v0, v4}, Lcom/google/android/apps/gmm/map/u;-><init>(Lcom/google/android/apps/gmm/map/t;Landroid/view/View;)V

    .line 548
    iget-object v3, p0, Lcom/google/android/apps/gmm/car/ae;->y:Lcom/google/android/apps/gmm/car/a;

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/car/a;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/car/a;->b:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/av;->f:Lcom/google/r/b/a/av;

    :goto_0
    iget-object v4, v3, Lcom/google/android/apps/gmm/car/a;->a:Lcom/google/android/apps/gmm/z/a/b;

    invoke-interface {v4, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/av;)Ljava/lang/String;

    :cond_0
    iput-boolean v2, v3, Lcom/google/android/apps/gmm/car/a;->b:Z

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/car/a;->c:Z

    .line 549
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->G:Lcom/google/android/apps/gmm/o/e;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/o/e;->a()V

    .line 550
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    if-eqz v0, :cond_5

    .line 551
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/car/bi;->a:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 548
    :cond_1
    sget-object v0, Lcom/google/r/b/a/av;->e:Lcom/google/r/b/a/av;

    goto :goto_0

    .line 551
    :cond_2
    iget-boolean v0, v3, Lcom/google/android/apps/gmm/car/bi;->c:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iput-boolean v1, v3, Lcom/google/android/apps/gmm/car/bi;->c:Z

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 553
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->M:Lcom/google/android/apps/gmm/car/e/d;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/e/d;->d:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v0, v3, Lcom/google/android/apps/gmm/car/bi;->a:Z

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_6
    iget-boolean v0, v3, Lcom/google/android/apps/gmm/car/bi;->c:Z

    if-nez v0, :cond_7

    move v0, v1

    :goto_2
    if-nez v0, :cond_8

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    iput-boolean v1, v3, Lcom/google/android/apps/gmm/car/bi;->c:Z

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 554
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/car/ae;->g:Z

    .line 557
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->x:Lcom/google/android/apps/gmm/q/i;

    iget-object v1, v0, Lcom/google/android/apps/gmm/q/i;->a:Lcom/google/android/apps/gmm/q/a;

    if-eqz v1, :cond_9

    iget-object v0, v0, Lcom/google/android/apps/gmm/q/i;->a:Lcom/google/android/apps/gmm/q/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/q/a;->b()V

    .line 558
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ae;->g()V

    .line 560
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 561
    return-void
.end method

.method protected final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 565
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 566
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    const-string v1, "MY_LOCATION_AUTOPAN"

    iget-object v0, v0, Lcom/google/android/apps/gmm/mylocation/n;->i:Lcom/google/android/apps/gmm/map/s/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/s/a;->ordinal()I

    move-result v0

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 567
    return-void
.end method

.method protected final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 571
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 573
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 575
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/car/ae;->g:Z

    .line 576
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->M:Lcom/google/android/apps/gmm/car/e/d;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/d;->d:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/bi;->c:Z

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/bi;->c:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 577
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    if-eqz v0, :cond_2

    .line 578
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->f:Lcom/google/android/apps/gmm/car/e/x;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/e/x;->m:Lcom/google/android/apps/gmm/car/bi;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/car/bi;->c:Z

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/bi;->c:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/bi;->d()V

    .line 580
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->C:Lcom/google/android/apps/gmm/car/au;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->c()V

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/t;->j:Z

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/mylocation/n;->a()V

    .line 582
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->y()V

    .line 584
    invoke-super {p0}, Lcom/google/android/gms/car/support/ae;->c()V

    .line 586
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 587
    return-void
.end method

.method protected final d()V
    .locals 4

    .prologue
    .line 591
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 593
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->R:Lcom/google/android/apps/gmm/car/b/b;

    if-eqz v0, :cond_1

    .line 594
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->R:Lcom/google/android/apps/gmm/car/b/b;

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/b/b;->d:Landroid/os/Handler;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/b/b;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, v1, Lcom/google/android/apps/gmm/car/b/b;->a:Landroid/view/ViewGroup;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    :goto_0
    iget-object v2, v1, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getChildCount()I

    move-result v2

    if-eq v0, v2, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/b/b;->e:Ljava/util/ArrayList;

    iget-object v3, v1, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v0}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, v1, Lcom/google/android/apps/gmm/car/b/b;->c:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 596
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->y:Lcom/google/android/apps/gmm/car/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/a;->a:Lcom/google/android/apps/gmm/z/a/b;

    sget-object v1, Lcom/google/r/b/a/av;->g:Lcom/google/r/b/a/av;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/r/b/a/av;)Ljava/lang/String;

    .line 598
    invoke-super {p0}, Lcom/google/android/gms/car/support/ae;->d()V

    .line 600
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 601
    return-void
.end method

.method protected final e()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 605
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 607
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->b()V

    .line 609
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->T:Lcom/google/android/apps/gmm/car/b/a;

    if-eqz v0, :cond_0

    .line 610
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->T:Lcom/google/android/apps/gmm/car/b/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/b/a;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/b/a;->e:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iput-object v5, v0, Lcom/google/android/apps/gmm/car/b/a;->d:Ljava/lang/StringBuilder;

    iput-object v5, v0, Lcom/google/android/apps/gmm/car/b/a;->c:Ljava/util/Hashtable;

    .line 611
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->T:Lcom/google/android/apps/gmm/car/b/a;

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->S:Lcom/google/android/apps/gmm/mylocation/c/g;

    if-eqz v0, :cond_1

    .line 615
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->S:Lcom/google/android/apps/gmm/mylocation/c/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/mylocation/c/g;->a(Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    .line 616
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->S:Lcom/google/android/apps/gmm/mylocation/c/g;

    .line 619
    :cond_1
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->R:Lcom/google/android/apps/gmm/car/b/b;

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->Q:Lcom/google/android/apps/gmm/car/j/e;

    if-eqz v0, :cond_4

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->Q:Lcom/google/android/apps/gmm/car/j/e;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/e;->f:Landroid/os/Handler;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/j/e;->k:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/j/e;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v1

    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->u:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v3, v0, Lcom/google/android/apps/gmm/car/j/e;->g:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v4, v1, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_2
    sget-object v2, Lcom/google/android/apps/gmm/shared/b/c;->x:Lcom/google/android/apps/gmm/shared/b/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/j/e;->h:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 623
    :cond_3
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->Q:Lcom/google/android/apps/gmm/car/j/e;

    .line 626
    :cond_4
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->P:Lcom/google/android/apps/gmm/car/c/c;

    .line 627
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->O:Lcom/google/android/apps/gmm/car/c/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/aa/a/e;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 628
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->O:Lcom/google/android/apps/gmm/car/c/a;

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/i;->a()V

    .line 631
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->N:Lcom/google/android/apps/gmm/car/m/k;

    sget-object v1, Lcom/google/android/apps/gmm/car/m/o;->a:Lcom/google/android/apps/gmm/car/m/o;

    invoke-interface {v0, v5, v1}, Lcom/google/android/apps/gmm/car/m/k;->a(Landroid/view/View;Lcom/google/android/apps/gmm/car/m/o;)V

    .line 632
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->N:Lcom/google/android/apps/gmm/car/m/k;

    .line 634
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->M:Lcom/google/android/apps/gmm/car/e/d;

    .line 635
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->e:Lcom/google/android/apps/gmm/car/m/q;

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->d:Lcom/google/android/apps/gmm/car/d/q;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/d/q;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/d/q;->g:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/d/q;->a:Lcom/google/android/apps/gmm/map/m/l;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/m/l;->a()V

    .line 638
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->d:Lcom/google/android/apps/gmm/car/d/q;

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    .line 641
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    .line 642
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->L:Lcom/google/android/apps/gmm/car/d/a;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/d/a;->e:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v3, v1, Lcom/google/android/apps/gmm/car/d/a;->t:Ljava/lang/Object;

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/d/a;->j:Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/views/CarCompassButtonView;->f()V

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/d/a;->g:Landroid/widget/FrameLayout;

    iget-object v3, v1, Lcom/google/android/apps/gmm/car/d/a;->h:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    iput-object v5, v1, Lcom/google/android/apps/gmm/car/d/a;->l:Lcom/google/android/apps/gmm/car/ax;

    iput-object v5, v1, Lcom/google/android/apps/gmm/car/d/a;->k:Lcom/google/android/apps/gmm/car/d/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/d/a;->i:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 643
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->L:Lcom/google/android/apps/gmm/car/d/a;

    .line 644
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v1}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645
    :cond_5
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->b:Lcom/google/android/apps/gmm/car/m/i;

    .line 647
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->K:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/e/m;->f:Lcom/google/android/apps/gmm/car/e/ap;

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/e/ap;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/e/ap;->c:Ljava/lang/Object;

    invoke-interface {v3, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iput-object v5, v1, Lcom/google/android/apps/gmm/car/e/m;->g:Lcom/google/android/apps/gmm/car/e/as;

    .line 648
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->K:Lcom/google/android/apps/gmm/car/e/m;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/e/m;->e:Lcom/google/android/apps/gmm/car/e/j;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/e/j;->a()V

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/e/j;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v4, v2, Lcom/google/android/apps/gmm/car/e/j;->f:Ljava/lang/Object;

    invoke-interface {v3, v4}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    sget-object v3, Lcom/google/android/apps/gmm/car/e/j;->a:Ljava/lang/String;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/e/j;->b:Lcom/google/android/apps/gmm/map/util/b/a/a;

    new-instance v3, Lcom/google/android/apps/gmm/car/a/e;

    sget-object v4, Lcom/google/android/apps/gmm/car/a/f;->a:Lcom/google/android/apps/gmm/car/a/f;

    invoke-direct {v3, v4}, Lcom/google/android/apps/gmm/car/a/e;-><init>(Lcom/google/android/apps/gmm/car/a/f;)V

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/util/b/a/a;->c(Ljava/lang/Object;)V

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/e/m;->d:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 649
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->K:Lcom/google/android/apps/gmm/car/e/m;

    .line 651
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->J:Lcom/google/android/apps/gmm/car/bg;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/bg;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/bg;->b:Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/startpage/w;->e()V

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/bg;->b:Lcom/google/android/apps/gmm/startpage/w;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/startpage/w;->c()V

    .line 652
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->J:Lcom/google/android/apps/gmm/car/bg;

    .line 653
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->I:Lcom/google/android/apps/gmm/mylocation/n;

    iput-object v5, v1, Lcom/google/android/apps/gmm/mylocation/n;->c:Lcom/google/android/apps/gmm/base/a;

    iput-object v5, v1, Lcom/google/android/apps/gmm/mylocation/n;->d:Lcom/google/android/apps/gmm/map/t;

    .line 654
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->H:Lcom/google/android/apps/gmm/car/ar;

    .line 655
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->H:Lcom/google/android/apps/gmm/car/ar;

    .line 656
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->G:Lcom/google/android/apps/gmm/o/e;

    .line 658
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->U:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 660
    iput-boolean v6, p0, Lcom/google/android/apps/gmm/car/ae;->a:Z

    .line 661
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->F:Landroid/view/View;

    .line 663
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->a:Lcom/google/android/apps/gmm/car/ao;

    invoke-virtual {v1, v5}, Lcom/google/android/apps/gmm/car/ao;->a(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->b:Landroid/support/v4/widget/bk;

    invoke-virtual {v2}, Landroid/support/v4/widget/bk;->d()V

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e:Landroid/os/Handler;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->c:Ljava/lang/Runnable;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-boolean v6, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->d:Lcom/google/android/apps/gmm/car/m/i;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/m/i;->a()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/m/m;->b()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->c:Lcom/google/android/apps/gmm/car/m/a;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/car/m/a;->d:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->d:Lcom/google/android/apps/gmm/car/m/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/i;->d:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 664
    :cond_6
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    .line 665
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->D:Landroid/widget/ImageView;

    .line 666
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->C:Lcom/google/android/apps/gmm/car/au;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iput-boolean v6, v1, Lcom/google/android/apps/gmm/map/t;->e:Z

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/au;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->a()V

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/t;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 667
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->C:Lcom/google/android/apps/gmm/car/au;

    .line 668
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->d:Lcom/google/android/apps/gmm/map/util/b/a/a;

    if-eqz v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/ao;->d:Lcom/google/android/apps/gmm/map/util/b/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ao;->l:Ljava/lang/Object;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 669
    :cond_7
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    .line 670
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->A:Lcom/google/android/gms/car/support/CarAppLayout;

    .line 672
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    iget-object v1, v0, Lcom/google/android/libraries/curvular/bd;->e:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/libraries/curvular/bd;->g:Landroid/content/ComponentCallbacks2;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterComponentCallbacks(Landroid/content/ComponentCallbacks;)V

    iget-object v0, v0, Lcom/google/android/libraries/curvular/bd;->a:Lcom/google/b/c/am;

    invoke-interface {v0}, Lcom/google/b/c/am;->clear()V

    .line 673
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->z:Lcom/google/android/libraries/curvular/bd;

    .line 675
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->y:Lcom/google/android/apps/gmm/car/a;

    .line 676
    iput-object v5, p0, Lcom/google/android/apps/gmm/car/ae;->x:Lcom/google/android/apps/gmm/q/i;

    .line 678
    invoke-super {p0}, Lcom/google/android/gms/car/support/ae;->e()V

    .line 680
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 681
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 685
    sget-object v0, Lcom/google/android/apps/gmm/car/ae;->w:Ljava/lang/String;

    .line 687
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/car/ae;->a:Z

    if-eqz v0, :cond_0

    .line 698
    :goto_0
    return-void

    .line 693
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    if-eqz v0, :cond_1

    .line 694
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    goto :goto_0

    .line 696
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->c:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    goto :goto_0
.end method

.method g()V
    .locals 6

    .prologue
    const v2, -0x50506

    const v1, -0xe8dfda

    .line 724
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v0

    const-string v3, "uimode"

    .line 725
    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 727
    invoke-virtual {v0}, Landroid/app/UiModeManager;->getNightMode()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    .line 728
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->D:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/apps/gmm/f;->A:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 729
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/k;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->b()V

    iget-object v2, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const v3, -0xf47fbd

    iput v3, v2, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->j:I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    move v0, v1

    .line 737
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/ae;->B:Lcom/google/android/apps/gmm/car/ao;

    sget-object v2, Lcom/google/android/apps/gmm/car/n/a;->n:Lcom/google/android/apps/gmm/base/k/am;

    .line 739
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/k/am;->b(Landroid/content/Context;)I

    move-result v2

    sget-object v3, Lcom/google/android/apps/gmm/car/n/a;->l:Lcom/google/android/apps/gmm/base/k/am;

    .line 740
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/base/k/am;->b(Landroid/content/Context;)I

    move-result v3

    sget-object v4, Lcom/google/android/apps/gmm/car/n/a;->n:Lcom/google/android/apps/gmm/base/k/am;

    .line 741
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/car/ae;->h()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/base/k/am;->b(Landroid/content/Context;)I

    move-result v4

    .line 737
    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ao;->a:Lcom/google/android/gms/car/support/CarAppLayout;

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/google/android/gms/car/support/CarAppLayout;->setSearchBoxColors(IIII)V

    .line 742
    return-void

    .line 732
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->D:Landroid/widget/ImageView;

    sget v1, Lcom/google/android/apps/gmm/f;->B:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 733
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/ae;->E:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->g:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->b()V

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    const v3, -0xf062a8

    iput v3, v1, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->j:I

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->invalidate()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    move v0, v2

    .line 734
    goto :goto_0
.end method
