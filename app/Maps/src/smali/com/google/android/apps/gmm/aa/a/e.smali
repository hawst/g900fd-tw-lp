.class public abstract Lcom/google/android/apps/gmm/aa/a/e;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/util/b/a/a;

.field private final b:Lcom/google/android/apps/gmm/aa/a/d;

.field private final c:Lcom/google/android/apps/gmm/navigation/a/d;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/aa/a/d;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/a/a;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    .line 41
    iput-object p2, p0, Lcom/google/android/apps/gmm/aa/a/e;->c:Lcom/google/android/apps/gmm/navigation/a/d;

    .line 42
    iput-object p3, p0, Lcom/google/android/apps/gmm/aa/a/e;->a:Lcom/google/android/apps/gmm/map/util/b/a/a;

    .line 43
    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public final a(Lcom/google/android/apps/gmm/aa/a/a/a;Z)V
    .locals 13
    .param p1    # Lcom/google/android/apps/gmm/aa/a/a/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const v12, -0x21524111

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 55
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 56
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->u:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-ne p1, v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->z:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-eq p1, v0, :cond_0

    .line 62
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->A:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-eq p1, v0, :cond_0

    .line 65
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a/a;->D:Lcom/google/android/apps/gmm/aa/a/a/a;

    if-eq p1, v0, :cond_0

    .line 80
    sget-object v0, Lcom/google/android/apps/gmm/aa/a/f;->a:[I

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/aa/a/a/a;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 168
    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_25

    move v0, v4

    :goto_1
    if-eqz v0, :cond_26

    sget v0, Lcom/google/android/apps/gmm/l;->dT:I

    :goto_2
    iget-object v3, v6, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    invoke-virtual {v3, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v3, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v3, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    .line 174
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x12

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unhandled action: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 177
    :cond_2
    :goto_3
    if-eqz p2, :cond_0

    if-eq v0, v1, :cond_0

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/e;->c:Lcom/google/android/apps/gmm/navigation/a/d;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    .line 180
    iget-object v1, p0, Lcom/google/android/apps/gmm/aa/a/e;->c:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v1, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    goto :goto_0

    .line 82
    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/aa/a/e;->a(Z)V

    move v0, v1

    .line 83
    goto :goto_3

    .line 85
    :pswitch_1
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/aa/a/e;->a(Z)V

    move v0, v1

    .line 86
    goto :goto_3

    .line 88
    :pswitch_2
    invoke-virtual {p0, v3}, Lcom/google/android/apps/gmm/aa/a/e;->b(Z)I

    move-result v0

    goto :goto_3

    .line 91
    :pswitch_3
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/aa/a/e;->b(Z)I

    move-result v0

    goto :goto_3

    :pswitch_4
    move v0, v1

    .line 95
    goto :goto_3

    .line 97
    :pswitch_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->a()I

    move-result v0

    goto :goto_3

    .line 100
    :pswitch_6
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->a()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    goto :goto_3

    :cond_3
    sget v0, Lcom/google/android/apps/gmm/l;->do:I

    goto :goto_3

    .line 103
    :pswitch_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->b()V

    move v0, v1

    .line 104
    goto :goto_3

    .line 106
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v5, v0, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v5, :cond_4

    move v3, v4

    :cond_4
    if-eqz v3, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->s_()Lcom/google/android/apps/gmm/navigation/b/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/navigation/b/c;->a()V

    :cond_5
    move v0, v1

    .line 107
    goto :goto_3

    .line 110
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->c:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->b(Z)V

    sget v0, Lcom/google/android/apps/gmm/l;->dK:I

    goto :goto_3

    .line 113
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->c:Lcom/google/android/apps/gmm/navigation/a/d;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/navigation/a/d;->b(Z)V

    sget v0, Lcom/google/android/apps/gmm/l;->dR:I

    goto :goto_3

    .line 118
    :pswitch_b
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->d()I

    move-result v0

    goto :goto_3

    .line 121
    :pswitch_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/aa/a/d;->a()V

    move v0, v1

    .line 122
    goto :goto_3

    .line 125
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/aa/a/d;->a()V

    move v0, v1

    .line 126
    goto :goto_3

    .line 128
    :pswitch_e
    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_7

    move v0, v4

    :goto_4
    if-eqz v0, :cond_6

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_6

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->f:I

    if-eq v0, v1, :cond_6

    iget-object v6, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v7, v6, Lcom/google/android/apps/gmm/navigation/a/ax;->b:Lcom/google/android/apps/gmm/navigation/a/ae;

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/a/ax;->e:Lcom/google/maps/g/a/al;

    iget-object v8, v7, Lcom/google/android/apps/gmm/navigation/a/ae;->b:Lcom/google/android/apps/gmm/shared/c/c/c;

    invoke-virtual {v8, v6}, Lcom/google/android/apps/gmm/shared/c/c/c;->a(Lcom/google/maps/g/a/al;)Lcom/google/maps/g/a/al;

    move-result-object v6

    invoke-virtual {v7, v6, v3}, Lcom/google/android/apps/gmm/navigation/a/ae;->a(Lcom/google/maps/g/a/al;Z)Lcom/google/android/apps/gmm/navigation/a/ai;

    move-result-object v3

    sget-object v6, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v3, v0}, Lcom/google/android/apps/gmm/navigation/a/ae;->a(Lcom/google/android/apps/gmm/navigation/a/ai;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v3, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    :cond_6
    move v0, v1

    .line 129
    goto/16 :goto_3

    :cond_7
    move v0, v3

    .line 128
    goto :goto_4

    .line 131
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v5, v0, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v5, :cond_8

    move v3, v4

    :cond_8
    if-eqz v3, :cond_9

    iget-object v3, v0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v3, :cond_9

    iget-object v3, v0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    if-eq v3, v1, :cond_9

    iget-object v3, v0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v5, v0, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v5, v5, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    invoke-virtual {v3, v5}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(I)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v5, v3, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    invoke-virtual {v0, v3, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    :cond_9
    move v0, v1

    .line 132
    goto/16 :goto_3

    .line 134
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/aa/a/d;->b()V

    move v0, v1

    .line 135
    goto/16 :goto_3

    .line 137
    :pswitch_11
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->c()V

    move v0, v1

    .line 138
    goto/16 :goto_3

    .line 140
    :pswitch_12
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->g()V

    move v0, v1

    .line 141
    goto/16 :goto_3

    .line 143
    :pswitch_13
    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_c

    move v0, v4

    :goto_5
    if-eqz v0, :cond_b

    invoke-virtual {v5}, Lcom/google/android/apps/gmm/aa/a/d;->d()Ljava/lang/String;

    move-result-object v6

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v0

    iget-object v7, v0, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    if-eqz v6, :cond_a

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_d

    :cond_a
    move v0, v4

    :goto_6
    if-eqz v0, :cond_e

    iget-object v0, v7, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->dA:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_7
    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v3, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v3, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    :cond_b
    move v0, v1

    .line 144
    goto/16 :goto_3

    :cond_c
    move v0, v3

    .line 143
    goto :goto_5

    :cond_d
    move v0, v3

    goto :goto_6

    :cond_e
    iget-object v0, v7, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/gmm/l;->dz:I

    new-array v8, v4, [Ljava/lang/Object;

    aput-object v6, v8, v3

    invoke-virtual {v0, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_7

    .line 146
    :pswitch_14
    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/aa/a/e;->b(Z)I

    move-result v0

    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v5, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v5, :cond_11

    move v5, v4

    :goto_8
    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_f

    move v3, v4

    :cond_f
    if-eqz v3, :cond_10

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v0, :cond_10

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    if-eq v0, v1, :cond_10

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-nez v0, :cond_12

    move-object v0, v2

    :goto_9
    iget-object v3, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    iget-object v6, v3, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v3, v5, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v3, v3, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    invoke-virtual {v6, v3}, Lcom/google/android/apps/gmm/navigation/a/ax;->a(I)Ljava/lang/String;

    move-result-object v3

    if-nez v0, :cond_13

    move-object v0, v2

    :goto_a
    if-eqz v0, :cond_14

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v7, Lcom/google/android/apps/gmm/l;->U:I

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x0

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_b
    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v3, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v3, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    :cond_10
    move v0, v1

    goto/16 :goto_3

    :cond_11
    move v5, v3

    goto :goto_8

    :cond_12
    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->G:Lcom/google/maps/g/a/fz;

    goto :goto_9

    :cond_13
    sget-object v7, Lcom/google/android/apps/gmm/navigation/a/ay;->a:[I

    invoke-virtual {v0}, Lcom/google/maps/g/a/fz;->ordinal()I

    move-result v0

    aget v0, v7, v0

    packed-switch v0, :pswitch_data_1

    move-object v0, v2

    goto :goto_a

    :pswitch_15
    sget v0, Lcom/google/android/apps/gmm/l;->dO:I

    :goto_c
    iget-object v7, v6, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    invoke-virtual {v7, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_a

    :pswitch_16
    sget v0, Lcom/google/android/apps/gmm/l;->dQ:I

    goto :goto_c

    :pswitch_17
    sget v0, Lcom/google/android/apps/gmm/l;->dP:I

    goto :goto_c

    :cond_14
    move-object v0, v3

    goto :goto_b

    .line 149
    :pswitch_18
    iget-object v0, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/aa/a/d;->c()V

    move v0, v1

    .line 150
    goto/16 :goto_3

    .line 152
    :pswitch_19
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->e()I

    move-result v0

    goto/16 :goto_3

    .line 155
    :pswitch_1a
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->f()I

    move-result v0

    goto/16 :goto_3

    .line 158
    :pswitch_1b
    iget-object v5, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_17

    move v0, v4

    :goto_d
    if-eqz v0, :cond_16

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_16

    iget-object v0, v5, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v0, v0, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v0, v6, v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    if-eqz v6, :cond_18

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    array-length v6, v6

    if-le v6, v4, :cond_18

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/w;->l:[Lcom/google/android/apps/gmm/map/r/a/ap;

    aget-object v0, v0, v4

    :goto_e
    if-eqz v0, :cond_16

    iget-object v6, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v6}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v6

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/a/ap;->b()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_15

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_19

    :cond_15
    move v0, v4

    :goto_f
    if-eqz v0, :cond_1a

    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/apps/gmm/l;->dC:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_10
    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->g:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v3, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v3, v5, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    :cond_16
    move v0, v1

    .line 159
    goto/16 :goto_3

    :cond_17
    move v0, v3

    .line 158
    goto :goto_d

    :cond_18
    move-object v0, v2

    goto :goto_e

    :cond_19
    move v0, v3

    goto :goto_f

    :cond_1a
    iget-object v0, v6, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    sget v6, Lcom/google/android/apps/gmm/l;->dB:I

    new-array v8, v4, [Ljava/lang/Object;

    aput-object v7, v8, v3

    invoke-virtual {v0, v6, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_10

    .line 161
    :pswitch_1c
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/aa/a/e;->h()V

    move v0, v1

    .line 162
    goto/16 :goto_3

    .line 164
    :pswitch_1d
    iget-object v6, p0, Lcom/google/android/apps/gmm/aa/a/e;->b:Lcom/google/android/apps/gmm/aa/a/d;

    iget-object v0, v6, Lcom/google/android/apps/gmm/aa/a/d;->b:Lcom/google/android/apps/gmm/aa/a/a;

    iget-object v5, v0, Lcom/google/android/apps/gmm/aa/a/a;->e:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    long-to-double v8, v8

    iget-wide v10, v0, Lcom/google/android/apps/gmm/aa/a/a;->b:D

    cmpl-double v5, v8, v10

    if-lez v5, :cond_1b

    iput v3, v0, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    iget-object v5, v0, Lcom/google/android/apps/gmm/aa/a/a;->e:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v8

    sget-wide v10, Lcom/google/android/apps/gmm/aa/a/a;->a:J

    add-long/2addr v8, v10

    long-to-double v8, v8

    iput-wide v8, v0, Lcom/google/android/apps/gmm/aa/a/a;->b:D

    :cond_1b
    iget v5, v0, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    iget v5, v0, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    const/4 v7, 0x4

    if-le v5, v7, :cond_1c

    const/4 v5, 0x2

    iput v5, v0, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    :cond_1c
    const v0, 0x7fffffff

    iget-object v5, v6, Lcom/google/android/apps/gmm/aa/a/d;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v5, :cond_1f

    move v5, v4

    :goto_11
    if-eqz v5, :cond_1d

    iget-object v5, v6, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    if-eqz v5, :cond_1d

    iget-object v0, v6, Lcom/google/android/apps/gmm/aa/a/d;->d:Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget v0, v0, Lcom/google/android/apps/gmm/navigation/g/b/k;->g:I

    :cond_1d
    iget-object v5, v6, Lcom/google/android/apps/gmm/aa/a/d;->b:Lcom/google/android/apps/gmm/aa/a/a;

    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    iget-object v8, v5, Lcom/google/android/apps/gmm/aa/a/a;->d:Landroid/content/Context;

    invoke-static {v8}, Lcom/google/android/apps/gmm/aa/b/a;->a(Landroid/content/Context;)Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_20

    iget v5, v5, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    if-gt v5, v4, :cond_1e

    const v5, 0x7fffffff

    if-ne v0, v5, :cond_20

    :cond_1e
    move v5, v4

    :goto_12
    if-eqz v5, :cond_24

    iget-object v5, v6, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v5}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/a/d;->g:Lcom/google/android/apps/gmm/navigation/a/ax;

    iget-object v7, v6, Lcom/google/android/apps/gmm/aa/a/d;->b:Lcom/google/android/apps/gmm/aa/a/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/navigation/a/ax;->a:Landroid/content/Context;

    const/16 v8, 0x1e

    if-ge v0, v8, :cond_21

    invoke-virtual {v5, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_13
    sget-object v3, Lcom/google/android/apps/gmm/navigation/a/b/j;->h:Lcom/google/android/apps/gmm/navigation/a/b/j;

    invoke-static {v3, v0, v2}, Lcom/google/android/apps/gmm/navigation/a/b/h;->a(Lcom/google/android/apps/gmm/navigation/a/b/j;Ljava/lang/CharSequence;Lcom/google/android/apps/gmm/navigation/a/b/a;)Lcom/google/android/apps/gmm/navigation/a/b/h;

    move-result-object v0

    iget-object v3, v6, Lcom/google/android/apps/gmm/aa/a/d;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h()Lcom/google/android/apps/gmm/navigation/a/d;

    move-result-object v3

    invoke-virtual {v3, v0, v2, v4}, Lcom/google/android/apps/gmm/navigation/a/d;->a(Lcom/google/android/apps/gmm/navigation/a/b/h;Lcom/google/android/apps/gmm/navigation/a/f;Z)V

    move v0, v1

    goto/16 :goto_3

    :cond_1f
    move v5, v3

    goto :goto_11

    :cond_20
    move v5, v3

    goto :goto_12

    :cond_21
    iget v0, v7, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    if-lez v0, :cond_22

    iget v0, v7, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    const/4 v8, 0x4

    if-ge v0, v8, :cond_22

    invoke-virtual {v5, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_13

    :cond_22
    iget v0, v7, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    const/4 v8, 0x4

    if-eq v0, v8, :cond_23

    sget-object v0, Lcom/google/android/apps/gmm/aa/a/a;->f:Ljava/lang/String;

    iget v7, v7, Lcom/google/android/apps/gmm/aa/a/a;->c:I

    new-instance v8, Ljava/lang/StringBuilder;

    const/16 v9, 0x2c

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "Invalid question count detected: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v7, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_23
    invoke-virtual {v5, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_13

    :cond_24
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/aa/a/d;->b()V

    move v0, v1

    .line 165
    goto/16 :goto_3

    :pswitch_1e
    move v0, v1

    .line 167
    goto/16 :goto_3

    :cond_25
    move v0, v3

    .line 168
    goto/16 :goto_1

    :cond_26
    sget v0, Lcom/google/android/apps/gmm/l;->dS:I

    goto/16 :goto_2

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
    .end packed-switch

    .line 146
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_15
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method public abstract a(Z)V
.end method

.method public abstract b(Z)I
.end method

.method public abstract b()V
.end method

.method public abstract c()V
.end method

.method public abstract d()I
.end method

.method public abstract e()I
.end method

.method public abstract f()I
.end method

.method public abstract g()V
.end method

.method public abstract h()V
.end method
