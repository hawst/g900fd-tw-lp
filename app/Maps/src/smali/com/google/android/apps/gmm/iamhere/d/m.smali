.class public Lcom/google/android/apps/gmm/iamhere/d/m;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/d/m;->a:Landroid/content/Context;

    .line 61
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/iamhere/d/o;Lcom/google/android/apps/gmm/iamhere/c/d;Lcom/google/b/f/t;Z)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 68
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/d/m;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/apps/gmm/iamhere/d/v;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/d/o;->d:Ljava/lang/String;

    .line 69
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p2, Lcom/google/android/apps/gmm/iamhere/c/d;->c:Landroid/net/Uri;

    .line 70
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "geo_ve_type_clicked"

    .line 71
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "card_action_type"

    iget-object v2, p2, Lcom/google/android/apps/gmm/iamhere/c/d;->d:Lcom/google/android/apps/gmm/iamhere/c/g;

    .line 72
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/iamhere/c/g;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "debug_mode"

    .line 73
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method
