.class final Lcom/google/android/apps/gmm/iamhere/c/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/google/android/apps/gmm/iamhere/c/a;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 773
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 773
    invoke-static {}, Lcom/google/android/apps/gmm/iamhere/c/k;->values()[Lcom/google/android/apps/gmm/iamhere/c/k;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v1, v0, v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/c/d;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    new-instance v4, Lcom/google/android/apps/gmm/iamhere/c/a;

    invoke-static {v3}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v3

    invoke-direct {v4, v1, v2, v0, v3}, Lcom/google/android/apps/gmm/iamhere/c/a;-><init>(Lcom/google/android/apps/gmm/iamhere/c/k;Ljava/lang/String;Landroid/net/Uri;Lcom/google/b/c/cv;)V

    return-object v4

    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 773
    new-array v0, p1, [Lcom/google/android/apps/gmm/iamhere/c/a;

    return-object v0
.end method
