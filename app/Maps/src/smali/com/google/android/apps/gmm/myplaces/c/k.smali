.class public Lcom/google/android/apps/gmm/myplaces/c/k;
.super Lcom/google/android/apps/gmm/myplaces/c/f;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field public final d:Lcom/google/android/apps/gmm/map/b/a/j;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public final h:Lcom/google/android/apps/gmm/map/b/a/q;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;)V
    .locals 0
    .param p8    # Lcom/google/android/apps/gmm/map/b/a/j;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 39
    invoke-direct/range {p0 .. p5}, Lcom/google/android/apps/gmm/myplaces/c/f;-><init>(Ljava/lang/String;JJ)V

    .line 40
    iput-object p1, p0, Lcom/google/android/apps/gmm/myplaces/c/k;->a:Ljava/lang/String;

    .line 41
    iput-object p6, p0, Lcom/google/android/apps/gmm/myplaces/c/k;->b:Ljava/lang/String;

    .line 42
    iput-object p7, p0, Lcom/google/android/apps/gmm/myplaces/c/k;->c:Ljava/lang/String;

    .line 43
    iput-object p8, p0, Lcom/google/android/apps/gmm/myplaces/c/k;->d:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 44
    iput-object p9, p0, Lcom/google/android/apps/gmm/myplaces/c/k;->h:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 45
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/g/c;)Lcom/google/android/apps/gmm/myplaces/c/k;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v7, 0x0

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-virtual {p0, v7}, Lcom/google/android/apps/gmm/base/g/c;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/g/c;->n:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/g/c;->n:Ljava/lang/String;

    .line 54
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v6

    .line 55
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v8

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v9

    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/myplaces/c/k;

    .line 58
    invoke-static {v8}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Lcom/google/android/apps/gmm/map/b/a/j;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    move-wide v4, v2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/myplaces/c/k;-><init>(Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/j;Lcom/google/android/apps/gmm/map/b/a/q;)V

    return-object v0

    :cond_1
    move-object v8, v7

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 108
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 109
    const-string v1, "q"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/myplaces/c/k;->b:Ljava/lang/String;

    return-object v0
.end method
