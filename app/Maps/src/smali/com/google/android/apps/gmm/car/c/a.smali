.class public Lcom/google/android/apps/gmm/car/c/a;
.super Lcom/google/android/apps/gmm/aa/a/e;
.source "PG"


# instance fields
.field private final b:Lcom/google/android/apps/gmm/car/ad;

.field private c:Lcom/google/android/apps/gmm/navigation/g/b/f;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/aa/a/d;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/a/a;Lcom/google/android/apps/gmm/car/ad;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/aa/a/e;-><init>(Lcom/google/android/apps/gmm/aa/a/d;Lcom/google/android/apps/gmm/navigation/a/d;Lcom/google/android/apps/gmm/map/util/b/a/a;)V

    .line 32
    iput-object p4, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    .line 33
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->y:Lcom/google/android/apps/gmm/car/c/h;

    .line 67
    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 69
    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/c/h;->a()V

    .line 70
    sget v0, Lcom/google/android/apps/gmm/l;->dM:I

    .line 72
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/apps/gmm/navigation/g/b/f;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    .line 44
    return-void

    .line 43
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public final b(Z)I
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->m:Lcom/google/android/apps/gmm/o/a/c;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/o/a/c;->a(Z)Z

    move-result v0

    .line 54
    if-ne v0, p1, :cond_2

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 56
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->f:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->F:Lcom/google/android/apps/gmm/shared/b/c;

    .line 57
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/shared/b/a;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/shared/b/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 58
    :cond_0
    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/apps/gmm/l;->dN:I

    .line 61
    :goto_0
    return v0

    .line 58
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/l;->dD:I

    goto :goto_0

    .line 61
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->b:Lcom/google/android/apps/gmm/car/m/m;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/m/m;->a:Lcom/google/android/apps/gmm/car/m/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/car/m/n;->a(Lcom/google/android/apps/gmm/car/m/l;)Lcom/google/android/apps/gmm/car/m/e;

    goto :goto_0
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method

.method public final d()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->i:Lcom/google/android/apps/gmm/mylocation/b/f;

    sget-object v1, Lcom/google/android/apps/gmm/map/s/a;->c:Lcom/google/android/apps/gmm/map/s/a;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/mylocation/b/f;->a(Lcom/google/android/apps/gmm/map/s/a;F)V

    .line 97
    sget v0, Lcom/google/android/apps/gmm/l;->dL:I

    .line 105
    :goto_0
    return v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->w:Lcom/google/android/apps/gmm/car/c/e;

    .line 100
    if-eqz v0, :cond_1

    .line 101
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    iput-boolean v2, v1, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v2, v1, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v1, v1, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 102
    invoke-interface {v0}, Lcom/google/android/apps/gmm/car/c/e;->a()V

    .line 103
    sget v0, Lcom/google/android/apps/gmm/l;->dL:I

    goto :goto_0

    .line 105
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final e()I
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->x:Lcom/google/android/apps/gmm/car/c/g;

    .line 111
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/c/a;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 112
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/c/a;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->b:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 113
    if-eqz v1, :cond_0

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v2, :cond_0

    .line 114
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 115
    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/c/g;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 116
    const v0, -0x21524111

    .line 119
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final f()I
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v0, v0, Lcom/google/android/apps/gmm/car/ad;->x:Lcom/google/android/apps/gmm/car/c/g;

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/c/a;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/gmm/car/c/a;->c:Lcom/google/android/apps/gmm/navigation/g/b/f;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/f;->g:Lcom/google/android/apps/gmm/navigation/g/b/i;

    iget-object v2, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->b:[Lcom/google/android/apps/gmm/navigation/g/b/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/i;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    iget v1, v1, Lcom/google/android/apps/gmm/map/r/a/ae;->c:I

    aget-object v1, v2, v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/navigation/g/b/k;->a:Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/r/a/w;->g:[Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 127
    array-length v2, v1

    if-lez v2, :cond_0

    .line 128
    iget-object v2, p0, Lcom/google/android/apps/gmm/car/c/a;->b:Lcom/google/android/apps/gmm/car/ad;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/ad;->c:Lcom/google/android/apps/gmm/car/drawer/j;

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/j;->a:Lcom/google/android/apps/gmm/car/drawer/k;

    const/4 v3, 0x0

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/car/drawer/k;->h:Z

    iget-object v3, v2, Lcom/google/android/apps/gmm/car/drawer/k;->f:Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/car/drawer/CarDrawerLayout;->e()V

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/drawer/k;->a()V

    iget-object v2, v2, Lcom/google/android/apps/gmm/car/drawer/k;->e:Lcom/google/android/apps/gmm/car/m/m;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/car/m/m;->a()V

    .line 129
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/car/c/g;->a(Lcom/google/android/apps/gmm/map/r/a/ag;)V

    .line 130
    const v0, -0x21524111

    .line 133
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method
