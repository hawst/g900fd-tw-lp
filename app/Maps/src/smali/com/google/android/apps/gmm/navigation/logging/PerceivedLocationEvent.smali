.class public Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;
.super Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "perceived-location"
    b = .enum Lcom/google/android/apps/gmm/util/replay/d;->HIGH:Lcom/google/android/apps/gmm/util/replay/d;
.end annotation

.annotation runtime Lcom/google/android/apps/gmm/util/replay/j;
.end annotation


# direct methods
.method private constructor <init>(Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/c;-><init>(Landroid/location/Location;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;DDLjava/lang/Long;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "provider"
        .end annotation
    .end param
    .param p2    # D
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "lat"
        .end annotation
    .end param
    .param p4    # D
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "lng"
        .end annotation
    .end param
    .param p6    # Ljava/lang/Long;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "time"
        .end annotation
    .end param
    .param p7    # Ljava/lang/Double;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "altitude"
        .end annotation
    .end param
    .param p8    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "bearing"
        .end annotation
    .end param
    .param p9    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "speed"
        .end annotation
    .end param
    .param p10    # Ljava/lang/Float;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "accuracy"
        .end annotation
    .end param
    .param p11    # Ljava/lang/Integer;
        .annotation runtime Lb/a/a;
        .end annotation

        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "numSatellites"
        .end annotation
    .end param

    .prologue
    .line 37
    invoke-static/range {p1 .. p11}, Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;->buildLocation(Ljava/lang/String;DDLjava/lang/Long;Ljava/lang/Double;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Float;Ljava/lang/Integer;)Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;-><init>(Landroid/location/Location;)V

    .line 39
    return-void
.end method

.method public static fromLocation(Landroid/location/Location;)Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/logging/PerceivedLocationEvent;-><init>(Landroid/location/Location;)V

    return-object v0
.end method
