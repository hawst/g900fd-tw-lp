.class public Lcom/google/android/apps/gmm/iamhere/l;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/iamhere/j;
.implements Lcom/google/android/apps/gmm/shared/net/c;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/apps/gmm/iamhere/j;",
        "Lcom/google/android/apps/gmm/shared/net/c",
        "<",
        "Lcom/google/r/b/a/amd;",
        ">;"
    }
.end annotation


# static fields
.field private static final c:Ljava/lang/String;

.field private static final d:[B


# instance fields
.field final a:Lcom/google/android/apps/gmm/base/activities/c;

.field b:Lcom/google/android/apps/gmm/iamhere/c/o;

.field private final e:D

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Lcom/google/android/apps/gmm/shared/net/b;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/alz;",
            "Lcom/google/r/b/a/amd;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lcom/google/android/apps/gmm/p/d/f;

.field private k:Z

.field private l:J

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/a/a;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/google/android/apps/gmm/iamhere/l;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/l;->c:Ljava/lang/String;

    .line 75
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/l;->d:[B

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/shared/net/b;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/base/activities/c;",
            "Lcom/google/android/apps/gmm/shared/net/b",
            "<",
            "Lcom/google/r/b/a/alz;",
            "Lcom/google/r/b/a/amd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->m:Ljava/util/List;

    .line 113
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 115
    if-eqz p2, :cond_0

    .line 116
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->BACKGROUND_THREADPOOL:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {p2, p0, v0}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    .line 118
    :cond_0
    iput-object p2, p0, Lcom/google/android/apps/gmm/iamhere/l;->i:Lcom/google/android/apps/gmm/shared/net/b;

    .line 119
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->u()Lcom/google/r/b/a/wt;

    move-result-object v0

    .line 121
    iget v1, v0, Lcom/google/r/b/a/wt;->b:I

    iput v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->g:I

    .line 122
    iget v1, v0, Lcom/google/r/b/a/wt;->e:I

    iput v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->h:I

    .line 123
    iget v1, v0, Lcom/google/r/b/a/wt;->d:I

    iput v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->f:I

    .line 124
    iget v0, v0, Lcom/google/r/b/a/wt;->c:I

    int-to-double v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->e:D

    .line 127
    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->g:I

    neg-int v0, v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->l:J

    .line 128
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 358
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 372
    :goto_1
    return-object p0

    :cond_1
    move v0, v1

    .line 358
    goto :goto_0

    .line 362
    :cond_2
    const-string v0, "http://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 363
    const-string v0, "https://"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 369
    :goto_2
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/URL;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p0

    goto :goto_1

    .line 363
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 364
    :cond_4
    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 365
    const-string v0, "https://"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 371
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/l;->c:Ljava/lang/String;

    const-string v2, "Server icon url is badly formatted."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 372
    const/4 p0, 0x0

    goto :goto_1

    :cond_6
    move-object v0, p0

    goto :goto_2
.end method

.method private declared-synchronized a(Lcom/google/android/apps/gmm/iamhere/c/o;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 377
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v0, v1, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v2, :cond_1

    iget-object v1, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v1, v1, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/x/o;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)D

    move-result-wide v0

    iget v2, p0, Lcom/google/android/apps/gmm/iamhere/l;->f:I

    int-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {p1, v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 378
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/l;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    monitor-exit p0

    return-void

    .line 377
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 19

    .prologue
    .line 262
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/iamhere/l;->k:Z

    .line 263
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/shared/net/d;->a()Lcom/google/android/apps/gmm/p/d/f;

    move-result-object v7

    .line 265
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v8

    .line 267
    if-eqz v7, :cond_0

    .line 268
    move-object/from16 v0, p0

    iput-object v7, v0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;

    .line 269
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/login/a/a;->h()Ljava/lang/String;

    move-result-object v4

    const-string v2, "https://hulk-debug-tool.corp.google.com/?lat="

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/p/d/f;->getLatitude()D

    move-result-wide v10

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/p/d/f;->getLongitude()D

    move-result-wide v12

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/p/d/f;->getAccuracy()F

    move-result v2

    float-to-int v6, v2

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/gmm/iamhere/l;->h:I

    invoke-virtual {v7}, Lcom/google/android/apps/gmm/p/d/f;->getTime()J

    move-result-wide v14

    if-nez v4, :cond_5

    const-string v2, ""

    move-object v3, v2

    :goto_0
    const-string v2, "&stp_env=%2Fbns%2Fvd%2Fborg%2Fvd%2Fbns%2Fwww-pinhole%2Fpinhole.batch.stubby-bastion%2F"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    if-nez v4, :cond_7

    const-string v2, "true"

    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0x88

    move/from16 v17, v0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int v17, v17, v18

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int v17, v17, v18

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->length()I

    move-result v18

    add-int v17, v17, v18

    move/from16 v0, v17

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&lng="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12, v13}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&precision="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&num="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "&timestamp="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&is_anonymous="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 271
    :cond_0
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/shared/net/d;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 272
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/shared/net/d;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 275
    :cond_1
    invoke-interface/range {p2 .. p2}, Lcom/google/android/apps/gmm/shared/net/d;->b()Lcom/google/android/apps/gmm/shared/net/k;

    move-result-object v2

    .line 276
    if-nez v2, :cond_2

    if-eqz p1, :cond_2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-nez v3, :cond_d

    .line 277
    :cond_2
    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v3, v1, v4}, Lcom/google/android/apps/gmm/iamhere/l;->a(ZLcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/iamhere/c/q;)V

    .line 278
    if-nez v2, :cond_8

    sget-object v10, Lcom/google/android/apps/gmm/iamhere/c/o;->c:Lcom/google/android/apps/gmm/iamhere/c/o;

    :goto_2
    if-nez v7, :cond_9

    const/4 v6, 0x0

    .line 280
    :goto_3
    invoke-virtual {v8}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v8

    .line 278
    iget-object v2, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->i:Lcom/google/o/b/a/v;

    if-eq v2, v6, :cond_3

    if-eqz v2, :cond_a

    invoke-virtual {v2, v6}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    :cond_3
    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_c

    iget-object v2, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->j:Ljava/util/List;

    if-eq v2, v8, :cond_4

    if-eqz v2, :cond_b

    invoke-virtual {v2, v8}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_4
    const/4 v2, 0x1

    :goto_5
    if-eqz v2, :cond_c

    move-object v2, v10

    :goto_6
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/iamhere/l;->a(Lcom/google/android/apps/gmm/iamhere/c/o;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 323
    :goto_7
    monitor-exit p0

    return-void

    .line 269
    :cond_5
    :try_start_1
    const-string v3, "&email="

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v16

    if-eqz v16, :cond_6

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_0

    :cond_6
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    move-object v3, v2

    goto/16 :goto_0

    :cond_7
    const-string v2, "0"

    goto/16 :goto_1

    .line 278
    :cond_8
    sget-object v3, Lcom/google/android/apps/gmm/iamhere/o;->b:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/shared/net/k;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/o;->c:Lcom/google/android/apps/gmm/iamhere/c/o;

    :goto_8
    move-object v10, v2

    goto :goto_2

    :pswitch_0
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/o;->d:Lcom/google/android/apps/gmm/iamhere/c/o;

    goto :goto_8

    :pswitch_1
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/o;->e:Lcom/google/android/apps/gmm/iamhere/c/o;

    goto :goto_8

    .line 279
    :cond_9
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/p/d/f;->a()Lcom/google/o/b/a/v;

    move-result-object v6

    goto :goto_3

    .line 278
    :cond_a
    const/4 v2, 0x0

    goto :goto_4

    :cond_b
    const/4 v2, 0x0

    goto :goto_5

    :cond_c
    new-instance v2, Lcom/google/android/apps/gmm/iamhere/c/o;

    iget-object v3, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    iget-object v4, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->g:Ljava/util/List;

    iget-object v5, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->h:Lcom/google/android/apps/gmm/x/o;

    iget-object v7, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->k:Lcom/google/r/b/a/amd;

    iget-boolean v9, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->l:Z

    iget-object v10, v10, Lcom/google/android/apps/gmm/iamhere/c/o;->m:Lcom/google/n/f;

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    .line 262
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 284
    :cond_d
    :try_start_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->u_()Lcom/google/android/apps/gmm/map/internal/d/c/a/g;

    move-result-object v9

    .line 287
    const/4 v2, 0x0

    move v5, v2

    :goto_9
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_1f

    .line 288
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/alu;->d()Lcom/google/r/b/a/alu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/alu;

    iget v3, v2, Lcom/google/r/b/a/alu;->b:I

    const/4 v6, 0x1

    if-ne v3, v6, :cond_10

    iget-object v2, v2, Lcom/google/r/b/a/alu;->c:Ljava/lang/Object;

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/ads;

    move-object v3, v2

    .line 290
    :goto_a
    new-instance v10, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 291
    iget-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v3, v2, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v2, v3, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v2, v10, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    .line 292
    iget v2, v3, Lcom/google/r/b/a/ads;->a:I

    const/high16 v6, 0x10000000

    and-int/2addr v2, v6

    const/high16 v6, 0x10000000

    if-ne v2, v6, :cond_11

    const/4 v2, 0x1

    :goto_b
    if-nez v2, :cond_12

    const/4 v2, 0x0

    :goto_c
    if-nez v2, :cond_14

    const/4 v2, 0x0

    iput-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    const/4 v2, 0x0

    iput-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    :cond_e
    :goto_d
    const/4 v2, 0x1

    .line 293
    iput-boolean v2, v10, Lcom/google/android/apps/gmm/base/g/g;->j:Z

    const/4 v2, 0x0

    .line 294
    iput-boolean v2, v10, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    .line 295
    iget-object v2, v3, Lcom/google/r/b/a/ads;->z:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/acm;->h()Lcom/google/r/b/a/acm;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/acm;

    if-nez v2, :cond_16

    const/4 v2, 0x0

    :goto_e
    iput-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->s:Lcom/google/android/apps/gmm/z/b/l;

    .line 297
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/alu;->d()Lcom/google/r/b/a/alu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/alu;

    iget-object v3, v2, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    instance-of v6, v3, Ljava/lang/String;

    if-eqz v6, :cond_1b

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    :goto_f
    invoke-static {v2}, Lcom/google/android/apps/gmm/iamhere/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 298
    if-eqz v3, :cond_f

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1d

    :cond_f
    const/4 v2, 0x1

    :goto_10
    if-nez v2, :cond_1e

    .line 300
    const-string v2, "IAmHereStateRetrieverImpl#onResponse"

    const/4 v6, 0x0

    invoke-interface {v9, v3, v2, v6}, Lcom/google/android/apps/gmm/map/internal/d/c/a/g;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;)Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    .line 302
    iput-object v3, v10, Lcom/google/android/apps/gmm/base/g/g;->r:Ljava/lang/String;

    .line 307
    :goto_11
    invoke-virtual {v10}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/x/o;->a(Ljava/io/Serializable;)Lcom/google/android/apps/gmm/x/o;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_9

    .line 288
    :cond_10
    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_a

    .line 292
    :cond_11
    const/4 v2, 0x0

    goto :goto_b

    :cond_12
    iget-object v2, v3, Lcom/google/r/b/a/ads;->M:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ap;->d()Lcom/google/maps/g/ap;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/ap;

    iget-object v2, v2, Lcom/google/maps/g/ap;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ai;->d()Lcom/google/maps/g/ai;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/ai;

    iget-object v2, v2, Lcom/google/maps/g/ai;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/al;->d()Lcom/google/maps/g/al;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/maps/g/al;

    iget v2, v2, Lcom/google/maps/g/al;->b:I

    invoke-static {v2}, Lcom/google/maps/g/au;->a(I)Lcom/google/maps/g/au;

    move-result-object v2

    if-nez v2, :cond_13

    sget-object v2, Lcom/google/maps/g/au;->a:Lcom/google/maps/g/au;

    :cond_13
    iget v2, v2, Lcom/google/maps/g/au;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto/16 :goto_c

    :cond_14
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_15

    sget-object v2, Lcom/google/android/apps/gmm/base/g/e;->a:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    sget-object v2, Lcom/google/android/apps/gmm/base/g/g;->l:Ljava/lang/String;

    iput-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    goto/16 :goto_d

    :cond_15
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v6, 0x1

    if-ne v2, v6, :cond_e

    sget-object v2, Lcom/google/android/apps/gmm/base/g/e;->b:Lcom/google/android/apps/gmm/base/g/e;

    iput-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->t:Lcom/google/android/apps/gmm/base/g/e;

    sget-object v2, Lcom/google/android/apps/gmm/base/g/g;->m:Ljava/lang/String;

    iput-object v2, v10, Lcom/google/android/apps/gmm/base/g/g;->n:Ljava/lang/String;

    goto/16 :goto_d

    .line 295
    :cond_16
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v6

    iget v3, v2, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v3, v3, 0x1

    const/4 v11, 0x1

    if-ne v3, v11, :cond_17

    const/4 v3, 0x1

    :goto_12
    if-eqz v3, :cond_18

    invoke-virtual {v2}, Lcom/google/r/b/a/acm;->d()Ljava/lang/String;

    move-result-object v3

    :goto_13
    iput-object v3, v6, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget v3, v2, Lcom/google/r/b/a/acm;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v11, 0x2

    if-ne v3, v11, :cond_19

    const/4 v3, 0x1

    :goto_14
    if-eqz v3, :cond_1a

    invoke-virtual {v2}, Lcom/google/r/b/a/acm;->g()Ljava/lang/String;

    move-result-object v2

    :goto_15
    iput-object v2, v6, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    invoke-virtual {v6}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    goto/16 :goto_e

    :cond_17
    const/4 v3, 0x0

    goto :goto_12

    :cond_18
    const/4 v3, 0x0

    goto :goto_13

    :cond_19
    const/4 v3, 0x0

    goto :goto_14

    :cond_1a
    const/4 v2, 0x0

    goto :goto_15

    .line 297
    :cond_1b
    check-cast v3, Lcom/google/n/f;

    invoke-virtual {v3}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Lcom/google/n/f;->e()Z

    move-result v3

    if-eqz v3, :cond_1c

    iput-object v6, v2, Lcom/google/r/b/a/alu;->f:Ljava/lang/Object;

    :cond_1c
    move-object v2, v6

    goto/16 :goto_f

    .line 298
    :cond_1d
    const/4 v2, 0x0

    goto/16 :goto_10

    .line 304
    :cond_1e
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/l;->c:Ljava/lang/String;

    const-string v3, "Empty parseUrl from server."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v2, v3, v6}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_11

    .line 312
    :cond_1f
    const/4 v2, 0x0

    .line 313
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/r/b/a/amd;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/alu;->d()Lcom/google/r/b/a/alu;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/r/b/a/alu;

    iget v2, v2, Lcom/google/r/b/a/alu;->d:I

    invoke-static {v2}, Lcom/google/maps/g/ta;->a(I)Lcom/google/maps/g/ta;

    move-result-object v2

    if-nez v2, :cond_20

    sget-object v2, Lcom/google/maps/g/ta;->a:Lcom/google/maps/g/ta;

    :cond_20
    invoke-static {v2}, Lcom/google/android/apps/gmm/iamhere/c/q;->a(Lcom/google/maps/g/ta;)Lcom/google/android/apps/gmm/iamhere/c/q;

    move-result-object v3

    .line 315
    move-object/from16 v0, p1

    iget-boolean v9, v0, Lcom/google/r/b/a/amd;->h:Z

    .line 317
    const/4 v2, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/apps/gmm/iamhere/l;->a(ZLcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/iamhere/c/q;)V

    .line 319
    new-instance v2, Lcom/google/android/apps/gmm/iamhere/c/o;

    const/4 v5, 0x0

    if-nez v7, :cond_21

    const/4 v6, 0x0

    .line 322
    :goto_16
    invoke-virtual {v8}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v8

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/r/b/a/amd;->i:Lcom/google/n/f;

    move-object/from16 v7, p1

    invoke-direct/range {v2 .. v10}, Lcom/google/android/apps/gmm/iamhere/c/o;-><init>(Lcom/google/android/apps/gmm/iamhere/c/q;Ljava/util/List;Lcom/google/android/apps/gmm/x/o;Lcom/google/o/b/a/v;Lcom/google/r/b/a/amd;Ljava/util/List;ZLcom/google/n/f;)V

    .line 319
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/gmm/iamhere/l;->a(Lcom/google/android/apps/gmm/iamhere/c/o;)V

    goto/16 :goto_7

    .line 321
    :cond_21
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/p/d/f;->a()Lcom/google/o/b/a/v;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v6

    goto :goto_16

    .line 278
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(ZLcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/iamhere/c/q;)V
    .locals 3

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/gmm/shared/b/c;->K:Lcom/google/android/apps/gmm/shared/b/c;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 456
    :goto_0
    return-void

    .line 443
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/iamhere/m;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/apps/gmm/iamhere/m;-><init>(Lcom/google/android/apps/gmm/iamhere/l;ZLcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/iamhere/c/q;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/p/d/f;)Z
    .locals 6

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;

    .line 204
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/d/f;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/d/f;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 205
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/p/d/f;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/p/d/f;->getLongitude()D

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    .line 203
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/p;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)D

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/l;->e:D

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/Collection;)[B
    .locals 4
    .param p0    # Ljava/util/Collection;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)[B"
        }
    .end annotation

    .prologue
    .line 532
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/l;->d:[B

    .line 546
    :goto_0
    return-object v0

    .line 535
    :cond_1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 536
    new-instance v1, Lcom/google/android/apps/a/a/c;

    const-string v2, "STP"

    const-string v3, ""

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/apps/a/a/c;-><init>(Ljava/io/OutputStream;Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    :try_start_0
    invoke-virtual {v1, p0}, Lcom/google/android/apps/a/a/c;->a(Ljava/lang/Iterable;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 544
    invoke-virtual {v1}, Lcom/google/android/apps/a/a/c;->close()V

    .line 546
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    goto :goto_0

    .line 540
    :catch_0
    move-exception v0

    .line 541
    :try_start_1
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/l;->c:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/shared/c/m;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 542
    const/4 v0, 0x0

    new-array v0, v0, [B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 544
    invoke-virtual {v1}, Lcom/google/android/apps/a/a/c;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/a/a/c;->close()V

    throw v0
.end method

.method private e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 402
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->k:Z

    .line 404
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->i:Lcom/google/android/apps/gmm/shared/net/b;

    if-eqz v1, :cond_0

    .line 405
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->i:Lcom/google/android/apps/gmm/shared/net/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/net/b;->a()Lcom/google/android/apps/gmm/shared/net/b;

    .line 407
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/o;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    iput-object v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 408
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/l;->f()V

    .line 409
    iget-object v1, p0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private declared-synchronized f()V
    .locals 4

    .prologue
    .line 459
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/iamhere/a/a;

    .line 460
    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v3, Lcom/google/android/apps/gmm/iamhere/n;

    invoke-direct {v3, p0, v0}, Lcom/google/android/apps/gmm/iamhere/n;-><init>(Lcom/google/android/apps/gmm/iamhere/l;Lcom/google/android/apps/gmm/iamhere/a/a;)V

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 459
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 467
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/iamhere/c/q;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/x/o;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/iamhere/c/q;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;)",
            "Lcom/google/android/apps/gmm/iamhere/c/o;"
        }
    .end annotation

    .prologue
    .line 416
    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 417
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/iamhere/c/o;->a(Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v0

    .line 418
    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 419
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/l;->f()V

    .line 420
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 417
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 418
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/iamhere/c/o;->a(Lcom/google/android/apps/gmm/iamhere/c/q;)Lcom/google/android/apps/gmm/iamhere/c/o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 416
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 383
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/l;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/k;->a:Lcom/google/android/apps/gmm/iamhere/k;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/iamhere/l;->a(Lcom/google/android/apps/gmm/p/d/f;Lcom/google/android/apps/gmm/iamhere/k;)Z

    .line 385
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 387
    :cond_0
    monitor-exit p0

    return-void

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/iamhere/a/a;)V
    .locals 1

    .prologue
    .line 425
    monitor-enter p0

    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 426
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 427
    monitor-exit p0

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/d;)V
    .locals 0

    .prologue
    .line 62
    check-cast p1, Lcom/google/r/b/a/amd;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/l;->a(Lcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/shared/net/d;)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/p/d/f;Lcom/google/android/apps/gmm/iamhere/k;)Z
    .locals 11

    .prologue
    .line 133
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->k:Z

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x1

    .line 183
    :goto_0
    return v0

    .line 137
    :cond_0
    const/4 v4, 0x0

    .line 140
    const/4 v1, 0x0

    .line 141
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/o;->a:[I

    invoke-virtual {p2}, Lcom/google/android/apps/gmm/iamhere/k;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :cond_1
    move v0, v1

    .line 175
    :goto_1
    if-eqz v4, :cond_18

    .line 176
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/p/d/f;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/p/d/f;->getLongitude()D

    move-result-wide v6

    invoke-direct {v1, v2, v3, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->i:Lcom/google/android/apps/gmm/shared/net/b;

    if-nez v0, :cond_11

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/l;->c:Ljava/lang/String;

    const-string v1, "connection == null, unable to sendNetworkRequest"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_2
    move v0, v4

    .line 183
    goto :goto_0

    .line 144
    :pswitch_0
    const/4 v4, 0x1

    move v0, v1

    .line 145
    goto :goto_1

    .line 150
    :pswitch_1
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_6

    const/4 v0, 0x0

    :goto_4
    if-eqz v0, :cond_3

    .line 151
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/iamhere/l;->a(Lcom/google/android/apps/gmm/p/d/f;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    .line 152
    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->h:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v2, v3, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->c:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v2, v3, :cond_2

    iget-object v2, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v3, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-eq v2, v3, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/iamhere/c/o;->f:Lcom/google/android/apps/gmm/iamhere/c/q;

    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/q;->d:Lcom/google/android/apps/gmm/iamhere/c/q;

    if-ne v0, v2, :cond_7

    :cond_2
    const/4 v0, 0x1

    :goto_5
    if-nez v0, :cond_8

    :cond_3
    const/4 v0, 0x1

    :goto_6
    move v4, v0

    move v0, v1

    .line 153
    goto :goto_1

    .line 150
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/p/d/f;->getTime()J

    move-result-wide v4

    sget-wide v6, Lcom/google/android/apps/gmm/p/d/f;->a:J

    add-long/2addr v4, v6

    cmp-long v0, v4, v2

    if-gez v0, :cond_5

    const/4 v0, 0x1

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    const/4 v0, 0x1

    goto :goto_4

    .line 152
    :cond_7
    const/4 v0, 0x0

    goto :goto_5

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    .line 155
    :pswitch_2
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/p/d/f;->g:Z

    if-nez v0, :cond_9

    const/4 v0, 0x0

    :goto_7
    if-eqz v0, :cond_b

    const/4 v0, 0x0

    :goto_8
    if-nez v0, :cond_c

    .line 156
    const/4 v4, 0x1

    move v0, v1

    goto/16 :goto_1

    .line 155
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/p/d/f;->getTime()J

    move-result-wide v6

    sget-wide v8, Lcom/google/android/apps/gmm/p/d/f;->a:J

    add-long/2addr v6, v8

    cmp-long v0, v6, v2

    if-gez v0, :cond_a

    const/4 v0, 0x1

    goto :goto_7

    :cond_a
    const/4 v0, 0x0

    goto :goto_7

    :cond_b
    const/4 v0, 0x1

    goto :goto_8

    .line 157
    :cond_c
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/iamhere/l;->a(Lcom/google/android/apps/gmm/p/d/f;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-wide v6, p0, Lcom/google/android/apps/gmm/iamhere/l;->l:J

    sub-long/2addr v2, v6

    iget v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->g:I

    int-to-long v6, v0

    cmp-long v0, v2, v6

    if-gez v0, :cond_e

    const/4 v0, 0x1

    :goto_9
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;

    if-nez v0, :cond_f

    .line 164
    :cond_d
    const/4 v4, 0x1

    move v0, v1

    goto/16 :goto_1

    .line 162
    :cond_e
    const/4 v0, 0x0

    goto :goto_9

    .line 165
    :cond_f
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/o;->b()Lcom/google/android/apps/gmm/x/o;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/x/o;->a(Lcom/google/android/apps/gmm/x/o;)Ljava/io/Serializable;

    move-result-object v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_a
    if-eqz v0, :cond_1

    .line 168
    const/4 v0, 0x1

    goto/16 :goto_1

    .line 165
    :cond_10
    const/4 v0, 0x0

    goto :goto_a

    .line 176
    :cond_11
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->k:Z

    iget-object v5, p0, Lcom/google/android/apps/gmm/iamhere/l;->i:Lcom/google/android/apps/gmm/shared/net/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget v6, p0, Lcom/google/android/apps/gmm/iamhere/l;->h:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    const-string v2, "wifi"

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/base/activities/c;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    if-nez v0, :cond_12

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/l;->d:[B

    move-object v2, v0

    :goto_b
    invoke-static {}, Lcom/google/maps/a/a;->newBuilder()Lcom/google/maps/a/c;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/a/e;->newBuilder()Lcom/google/maps/a/g;

    move-result-object v7

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v10, v7, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v10, v10, 0x2

    iput v10, v7, Lcom/google/maps/a/g;->a:I

    iput-wide v8, v7, Lcom/google/maps/a/g;->c:D

    iget-wide v8, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v1, v7, Lcom/google/maps/a/g;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v7, Lcom/google/maps/a/g;->a:I

    iput-wide v8, v7, Lcom/google/maps/a/g;->b:D

    iget-object v1, v0, Lcom/google/maps/a/c;->b:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/maps/a/g;->g()Lcom/google/n/t;

    move-result-object v7

    iget-object v8, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v7, 0x0

    iput-object v7, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v7, 0x1

    iput-boolean v7, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/a/c;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/a/c;->a:I

    invoke-virtual {v0}, Lcom/google/maps/a/c;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/a/a;

    const/4 v1, 0x0

    const/4 v7, 0x0

    invoke-static {v3, v0, v1, v7}, Lcom/google/android/apps/gmm/place/ci;->a(Lcom/google/android/apps/gmm/base/activities/o;Lcom/google/maps/a/a;Lcom/google/android/apps/gmm/place/ck;Lcom/google/e/a/a/a/b;)Lcom/google/e/a/a/a/b;

    move-result-object v1

    invoke-static {}, Lcom/google/r/b/a/ael;->g()Lcom/google/r/b/a/ael;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    if-eqz v1, :cond_13

    :goto_c
    check-cast v1, Lcom/google/r/b/a/ael;

    invoke-static {}, Lcom/google/r/b/a/alz;->newBuilder()Lcom/google/r/b/a/amb;

    move-result-object v3

    if-nez v0, :cond_14

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/iamhere/l;->a(Ljava/util/Collection;)[B

    move-result-object v0

    move-object v2, v0

    goto :goto_b

    :cond_13
    move-object v1, v3

    goto :goto_c

    :cond_14
    iget-object v7, v3, Lcom/google/r/b/a/amb;->b:Lcom/google/n/ao;

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/google/r/b/a/amb;->a:I

    iget-object v0, v1, Lcom/google/r/b/a/ael;->k:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/adi;->d()Lcom/google/r/b/a/adi;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/adi;

    if-nez v0, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_15
    iget-object v7, v3, Lcom/google/r/b/a/amb;->d:Lcom/google/n/ao;

    iget-object v8, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v7, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v7, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v7, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v0, v0, 0x40

    iput v0, v3, Lcom/google/r/b/a/amb;->a:I

    iget-object v0, v1, Lcom/google/r/b/a/ael;->o:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/lh;->d()Lcom/google/maps/g/lh;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/lh;

    if-nez v0, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    iget-object v1, v3, Lcom/google/r/b/a/amb;->e:Lcom/google/n/ao;

    iget-object v7, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/r/b/a/amb;->a:I

    or-int/lit16 v0, v0, 0x100

    iput v0, v3, Lcom/google/r/b/a/amb;->a:I

    iget v0, v3, Lcom/google/r/b/a/amb;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v3, Lcom/google/r/b/a/amb;->a:I

    iput v6, v3, Lcom/google/r/b/a/amb;->c:I

    invoke-static {v2}, Lcom/google/n/f;->a([B)Lcom/google/n/f;

    move-result-object v0

    if-nez v0, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    iget v1, v3, Lcom/google/r/b/a/amb;->a:I

    or-int/lit16 v1, v1, 0x200

    iput v1, v3, Lcom/google/r/b/a/amb;->a:I

    iput-object v0, v3, Lcom/google/r/b/a/amb;->f:Lcom/google/n/f;

    invoke-virtual {v3}, Lcom/google/r/b/a/amb;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/alz;

    invoke-interface {v5, v0}, Lcom/google/android/apps/gmm/shared/net/b;->a(Lcom/google/n/at;)Lcom/google/android/apps/gmm/shared/net/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->l:J

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/iamhere/l;->a(ZLcom/google/r/b/a/amd;Lcom/google/android/apps/gmm/iamhere/c/q;)V

    sget-object v0, Lcom/google/android/apps/gmm/iamhere/l;->c:Ljava/lang/String;

    const-string v1, "new snap-to-place request sent."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 177
    :cond_18
    if-eqz v0, :cond_19

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    sget-object v1, Lcom/google/android/apps/gmm/iamhere/c/q;->b:Lcom/google/android/apps/gmm/iamhere/c/q;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/iamhere/c/o;->a(Lcom/google/android/apps/gmm/iamhere/c/q;)Lcom/google/android/apps/gmm/iamhere/c/o;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/iamhere/l;->a(Lcom/google/android/apps/gmm/iamhere/c/o;)V

    goto/16 :goto_2

    .line 181
    :cond_19
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/l;->f()V

    goto/16 :goto_2

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 391
    invoke-direct {p0}, Lcom/google/android/apps/gmm/iamhere/l;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->j:Lcom/google/android/apps/gmm/p/d/f;

    .line 394
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/apps/gmm/iamhere/a/a;)V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 432
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 435
    return-void
.end method

.method public final d()Lcom/google/android/apps/gmm/iamhere/c/o;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/l;->b:Lcom/google/android/apps/gmm/iamhere/c/o;

    return-object v0
.end method
