.class final Lcom/google/android/apps/gmm/directions/e/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/f/a/a;

.field final synthetic b:Lcom/google/android/apps/gmm/map/c/a;

.field final synthetic c:Lcom/google/android/apps/gmm/map/x;

.field final synthetic d:Lcom/google/android/apps/gmm/shared/c/a/j;

.field final synthetic e:Lcom/google/android/apps/gmm/directions/e/f;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/f/a/a;Lcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/x;Lcom/google/android/apps/gmm/shared/c/a/j;Lcom/google/android/apps/gmm/directions/e/f;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/e/c;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/e/c;->b:Lcom/google/android/apps/gmm/map/c/a;

    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/e/c;->c:Lcom/google/android/apps/gmm/map/x;

    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/e/c;->d:Lcom/google/android/apps/gmm/shared/c/a/j;

    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/e/c;->e:Lcom/google/android/apps/gmm/directions/e/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/c;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->a:Lcom/google/android/apps/gmm/map/r/a/ae;

    const-string v1, "routes"

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v7, v0

    check-cast v7, Lcom/google/android/apps/gmm/map/r/a/ae;

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/c;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    iget-object v11, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->e:[Lcom/google/android/apps/gmm/map/r/a/s;

    .line 354
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    array-length v1, v11

    if-ne v0, v1, :cond_1

    move v0, v8

    :goto_0
    const-string v1, "# routes != # textureTypes"

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v9

    goto :goto_0

    .line 356
    :cond_2
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v1

    if-ltz v1, :cond_3

    move v0, v8

    :goto_1
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_3
    move v0, v9

    goto :goto_1

    :cond_4
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v10, v9

    .line 358
    :goto_2
    invoke-virtual {v7}, Lcom/google/android/apps/gmm/map/r/a/ae;->size()I

    move-result v0

    if-ge v10, v0, :cond_c

    .line 359
    iget-object v0, v7, Lcom/google/android/apps/gmm/map/r/a/ae;->b:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/r/a/w;

    .line 360
    aget-object v2, v11, v10

    .line 363
    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/map/r/a/w;->a(Lcom/google/android/apps/gmm/map/r/a/s;)Lcom/google/android/apps/gmm/map/r/a/r;

    move-result-object v3

    .line 365
    if-nez v3, :cond_b

    .line 366
    iget-object v3, v1, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    .line 369
    if-nez v3, :cond_5

    .line 370
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/s;->a:Lcom/google/android/apps/gmm/map/r/a/s;

    if-ne v2, v0, :cond_6

    .line 371
    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/s;->d:Lcom/google/android/apps/gmm/map/r/a/s;

    .line 378
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/c;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 380
    iget-boolean v4, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->f:Z

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/e/c;->b:Lcom/google/android/apps/gmm/map/c/a;

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/e/c;->c:Lcom/google/android/apps/gmm/map/x;

    .line 379
    new-instance v0, Lcom/google/android/apps/gmm/map/i/p;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/map/i/p;-><init>(Lcom/google/android/apps/gmm/map/r/a/w;Lcom/google/android/apps/gmm/map/r/a/s;Lcom/google/maps/g/a/fw;ZLcom/google/android/apps/gmm/map/c/a;Lcom/google/android/apps/gmm/map/x;)V

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    if-eqz v4, :cond_7

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    if-eqz v4, :cond_7

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/i/p;->k:Ljava/util/Map;

    if-eqz v4, :cond_7

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/i/p;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/i/p;->d:[B

    array-length v5, v5

    if-ne v4, v5, :cond_7

    move v4, v8

    :goto_4
    if-nez v4, :cond_8

    const/4 v0, 0x0

    .line 382
    :goto_5
    sget-object v4, Lcom/google/android/apps/gmm/directions/e/e;->a:[I

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/r/a/s;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 402
    sget-object v1, Lcom/google/android/apps/gmm/directions/e/a;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported TextureType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    :goto_6
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto/16 :goto_2

    .line 372
    :cond_6
    sget-object v0, Lcom/google/android/apps/gmm/map/r/a/s;->b:Lcom/google/android/apps/gmm/map/r/a/s;

    if-ne v2, v0, :cond_5

    .line 373
    sget-object v2, Lcom/google/android/apps/gmm/map/r/a/s;->e:Lcom/google/android/apps/gmm/map/r/a/s;

    goto :goto_3

    :cond_7
    move v4, v9

    .line 379
    goto :goto_4

    :cond_8
    new-instance v4, Lcom/google/android/apps/gmm/map/i/n;

    invoke-direct {v4, v0}, Lcom/google/android/apps/gmm/map/i/n;-><init>(Lcom/google/android/apps/gmm/map/i/p;)V

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/i/p;->i:Lcom/google/android/apps/gmm/map/c/a;

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/map/i/n;->a(Lcom/google/android/apps/gmm/map/c/a;)V

    move-object v0, v4

    goto :goto_5

    .line 390
    :pswitch_0
    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->B:Lcom/google/android/apps/gmm/map/r/a/r;

    goto :goto_6

    .line 393
    :pswitch_1
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/w;->F:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v4, v1, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    if-ne v3, v4, :cond_9

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->D:Lcom/google/android/apps/gmm/map/r/a/r;

    :cond_9
    monitor-exit v2

    goto :goto_6

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 394
    :pswitch_2
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/r/a/w;->F:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v4, v1, Lcom/google/android/apps/gmm/map/r/a/w;->n:Lcom/google/maps/g/a/fw;

    if-ne v3, v4, :cond_a

    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->E:Lcom/google/android/apps/gmm/map/r/a/r;

    :cond_a
    monitor-exit v2

    goto :goto_6

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0

    .line 397
    :pswitch_3
    iput-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/w;->C:Lcom/google/android/apps/gmm/map/r/a/r;

    goto :goto_6

    .line 409
    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/e/c;->b:Lcom/google/android/apps/gmm/map/c/a;

    .line 410
    new-instance v0, Lcom/google/android/apps/gmm/map/i/n;

    invoke-direct {v0, v3}, Lcom/google/android/apps/gmm/map/i/n;-><init>(Lcom/google/android/apps/gmm/map/r/a/r;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/i/n;->a(Lcom/google/android/apps/gmm/map/c/a;)V

    goto :goto_6

    .line 416
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/c;->a:Lcom/google/android/apps/gmm/directions/f/a/a;

    .line 417
    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/a/a;->i:Lcom/google/android/apps/gmm/map/b/a/q;

    new-instance v1, Lcom/google/android/apps/gmm/map/i/d;

    invoke-direct {v1, v12, v0}, Lcom/google/android/apps/gmm/map/i/d;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/q;)V

    .line 419
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/e/c;->d:Lcom/google/android/apps/gmm/shared/c/a/j;

    new-instance v2, Lcom/google/android/apps/gmm/directions/e/d;

    invoke-direct {v2, p0, v1}, Lcom/google/android/apps/gmm/directions/e/d;-><init>(Lcom/google/android/apps/gmm/directions/e/c;Lcom/google/android/apps/gmm/map/i/d;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 426
    return-void

    .line 382
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
