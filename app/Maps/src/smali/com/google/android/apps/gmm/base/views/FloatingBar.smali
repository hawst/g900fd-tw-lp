.class public Lcom/google/android/apps/gmm/base/views/FloatingBar;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field A:Landroid/view/ViewGroup;

.field B:Landroid/widget/TextView;

.field C:Landroid/widget/TextView;

.field D:Landroid/view/ViewGroup;

.field E:Landroid/widget/TextView;

.field F:Lcom/google/android/search/searchplate/SearchPlate;

.field G:Landroid/view/View;

.field H:Landroid/view/View;

.field I:Ljava/lang/CharSequence;

.field public J:Ljava/lang/CharSequence;

.field public K:Ljava/lang/CharSequence;

.field L:Landroid/graphics/drawable/Drawable;

.field M:Landroid/graphics/drawable/Drawable;

.field public N:Landroid/graphics/drawable/Drawable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public O:Landroid/graphics/drawable/Drawable;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public P:F

.field private Q:Ljava/lang/String;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private final R:Landroid/graphics/Rect;

.field private final S:Landroid/graphics/Rect;

.field private final T:Landroid/graphics/Rect;

.field private final U:Landroid/graphics/Rect;

.field private final V:Landroid/graphics/Rect;

.field private final W:Landroid/graphics/Rect;

.field public a:Lcom/google/android/apps/gmm/base/views/w;

.field private final aa:Landroid/graphics/Rect;

.field private final ab:Landroid/graphics/Rect;

.field private final ac:Landroid/graphics/Rect;

.field private final ad:Landroid/graphics/Rect;

.field private final ae:Landroid/graphics/Rect;

.field private af:Z

.field public b:Ljava/lang/String;

.field public c:Z

.field d:Lcom/google/o/h/a/hv;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field e:I

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field public j:Landroid/view/View$OnClickListener;

.field public k:Landroid/view/View$OnClickListener;

.field public l:Landroid/view/View$OnClickListener;

.field m:Landroid/widget/ImageView;

.field n:Landroid/view/View;

.field o:Lcom/google/android/apps/gmm/base/views/WebImageView;

.field p:Landroid/widget/EditText;

.field q:Landroid/widget/TextView;

.field r:Landroid/widget/Button;

.field public s:Landroid/widget/Button;

.field t:Landroid/widget/FrameLayout;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Landroid/view/View;

.field x:Landroid/view/ViewGroup;

.field y:Landroid/widget/TextView;

.field public z:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 396
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 271
    sget-object v0, Lcom/google/android/apps/gmm/base/views/w;->a:Lcom/google/android/apps/gmm/base/views/w;

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    .line 287
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->Q:Ljava/lang/String;

    .line 366
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    .line 367
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    .line 368
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->T:Landroid/graphics/Rect;

    .line 369
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->U:Landroid/graphics/Rect;

    .line 370
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    .line 371
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    .line 372
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    .line 373
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    .line 374
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    .line 375
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    .line 376
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    .line 383
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    .line 387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->af:Z

    .line 397
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/views/v;
    .locals 2

    .prologue
    .line 242
    const/4 v0, 0x0

    new-instance v1, Lcom/google/android/apps/gmm/base/views/v;

    invoke-direct {v1, p0, v0}, Lcom/google/android/apps/gmm/base/views/v;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/FloatingBar;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/FloatingBar;)Lcom/google/android/apps/gmm/base/views/v;
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/base/views/FloatingBar;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 253
    new-instance v0, Lcom/google/android/apps/gmm/base/views/v;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/base/views/v;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/views/FloatingBar;)V

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 1174
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method

.method private a(Landroid/view/View;Landroid/graphics/Rect;Z)V
    .locals 4

    .prologue
    .line 843
    if-nez p3, :cond_0

    .line 844
    iget v0, p2, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iget v2, p2, Landroid/graphics/Rect;->right:I

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 849
    :goto_0
    return-void

    .line 846
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->getMeasuredWidth()I

    move-result v0

    .line 847
    iget v1, p2, Landroid/graphics/Rect;->right:I

    sub-int v1, v0, v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    iget v3, p2, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v3

    iget v3, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 5

    .prologue
    .line 614
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 615
    sget v0, Lcom/google/android/apps/gmm/e;->aJ:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 616
    sget v0, Lcom/google/android/apps/gmm/e;->aQ:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 617
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/w;->b:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v3, v4, :cond_1

    .line 618
    sget v0, Lcom/google/android/apps/gmm/e;->aK:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 619
    sget v0, Lcom/google/android/apps/gmm/e;->aR:I

    .line 620
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 629
    :cond_0
    :goto_0
    add-int/2addr v1, v0

    add-int/2addr v0, v1

    return v0

    .line 621
    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/w;->d:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/w;->e:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/w;->f:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v3, v4, :cond_3

    .line 623
    :cond_2
    sget v1, Lcom/google/android/apps/gmm/e;->aL:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    goto :goto_0

    .line 624
    :cond_3
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v4, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v3, v4, :cond_0

    .line 625
    sget v3, Lcom/google/android/apps/gmm/e;->aL:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    sget v4, Lcom/google/android/apps/gmm/e;->aF:I

    .line 626
    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    goto :goto_0
.end method

.method public a(F)Landroid/animation/Animator;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x2

    .line 883
    iget v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    cmpl-float v1, v1, p1

    if-nez v1, :cond_0

    .line 912
    :goto_0
    return-object v0

    .line 887
    :cond_0
    new-array v1, v4, [F

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    aput v3, v1, v2

    const/4 v2, 0x1

    aput p1, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v1

    .line 888
    new-instance v2, Lcom/google/android/apps/gmm/base/views/t;

    invoke-direct {v2, p0}, Lcom/google/android/apps/gmm/base/views/t;-><init>(Lcom/google/android/apps/gmm/base/views/FloatingBar;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 899
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    invoke-virtual {v2, v4, v0}, Lcom/google/android/apps/gmm/base/views/WebImageView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 900
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->n:Landroid/view/View;

    invoke-virtual {v2, v4, v0}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 901
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    invoke-virtual {v2, v4, v0}, Landroid/widget/Button;->setLayerType(ILandroid/graphics/Paint;)V

    .line 902
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    invoke-virtual {v2, v4, v0}, Landroid/widget/Button;->setLayerType(ILandroid/graphics/Paint;)V

    .line 903
    new-instance v0, Lcom/google/android/apps/gmm/base/views/u;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/base/views/u;-><init>(Lcom/google/android/apps/gmm/base/views/FloatingBar;)V

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    move-object v0, v1

    .line 912
    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 999
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 984
    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 855
    iget v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 857
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->L:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->T:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 858
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->L:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 861
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->M:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->U:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 862
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->M:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 865
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 867
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 868
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_3

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 869
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_4

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 870
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_5

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 871
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->H:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_6

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 873
    :cond_6
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 874
    return-void
.end method

.method public getBaseline()I
    .locals 2

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 917
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    .line 918
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    if-ne p1, v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->j:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_3

    .line 921
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->j:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 931
    :cond_2
    :goto_0
    return-void

    .line 922
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    .line 923
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->k:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_4

    .line 925
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->k:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 926
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    .line 927
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->l:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_2

    .line 929
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->l:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1    # Landroid/widget/TextView;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Landroid/view/KeyEvent;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 935
    const/4 v0, 0x0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 805
    invoke-static {p0}, Lcom/google/android/apps/gmm/util/r;->a(Landroid/view/View;)Z

    move-result v0

    .line 806
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->m:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->T:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 807
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->b:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_0

    .line 808
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 833
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->n:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 834
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 835
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->w:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 836
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 837
    return-void

    .line 809
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->d:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_1

    .line 810
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 811
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    goto :goto_0

    .line 812
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->a:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_2

    .line 814
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 817
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    goto :goto_0

    .line 819
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->e:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v1, v2, :cond_3

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_5

    .line 820
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_4

    .line 821
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->G:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 822
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->H:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 824
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 825
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    goto :goto_0

    .line 826
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->f:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_6

    .line 827
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->D:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 828
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    goto/16 :goto_0

    .line 830
    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 831
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a(Landroid/view/View;Landroid/graphics/Rect;Z)V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 15

    .prologue
    .line 760
    invoke-super/range {p0 .. p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 762
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a()I

    move-result v5

    move/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->resolveSize(II)I

    move-result v1

    move/from16 v0, p2

    invoke-static {v5, v0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->resolveSize(II)I

    move-result v2

    invoke-virtual {p0, v1, v2}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->setMeasuredDimension(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/FloatingBar;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    sget v1, Lcom/google/android/apps/gmm/e;->aJ:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v7

    sget v1, Lcom/google/android/apps/gmm/e;->aO:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x2

    sub-int v8, v4, v1

    sget v1, Lcom/google/android/apps/gmm/e;->aJ:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->b:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v2, v3, :cond_3

    sget v1, Lcom/google/android/apps/gmm/e;->aK:I

    :cond_0
    :goto_0
    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v9

    sget v1, Lcom/google/android/apps/gmm/e;->aH:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    sget v1, Lcom/google/android/apps/gmm/e;->aG:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v10, Lcom/google/android/apps/gmm/base/views/w;->b:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v10, :cond_6

    sget v1, Lcom/google/android/apps/gmm/e;->aR:I

    :goto_1
    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    sget v10, Lcom/google/android/apps/gmm/e;->aM:I

    invoke-virtual {v6, v10}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v10

    sget v11, Lcom/google/android/apps/gmm/e;->aI:I

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v11

    sget v12, Lcom/google/android/apps/gmm/e;->aP:I

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v12

    sget v13, Lcom/google/android/apps/gmm/e;->aF:I

    invoke-virtual {v6, v13}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v13

    iget-object v14, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iput v1, v14, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    sget v14, Lcom/google/android/apps/gmm/e;->aO:I

    invoke-virtual {v6, v14}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v14

    iput v14, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget-object v14, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v14

    iput v9, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget-object v9, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v9

    iput v8, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    iput v8, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iput v8, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iput v8, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v8, Lcom/google/android/apps/gmm/base/views/w;->b:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v1, v8, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v8, Lcom/google/android/apps/gmm/base/views/w;->d:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v1, v8, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v8, Lcom/google/android/apps/gmm/base/views/w;->e:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v1, v8, :cond_1

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v8, Lcom/google/android/apps/gmm/base/views/w;->f:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v8, :cond_14

    :cond_1
    move v1, v3

    :goto_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v1

    shl-int/lit8 v9, v10, 0x1

    add-int/2addr v8, v9

    iput v8, v3, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->top:I

    iput v8, v3, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iput v8, v3, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    iput v8, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    iget-object v8, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v8

    shl-int/lit8 v8, v10, 0x1

    add-int/2addr v1, v8

    iput v1, v3, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iput v3, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iput v3, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v2

    shl-int/lit8 v8, v11, 0x1

    sub-int/2addr v3, v8

    iput v3, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iput v3, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iput v3, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v13

    iput v3, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iput v3, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iput v3, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iput v3, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v2, v3, v2

    shl-int/lit8 v3, v11, 0x1

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    sget v1, Lcom/google/android/apps/gmm/e;->aN:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v13

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v10

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v10

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v7

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iput v3, v2, Landroid/graphics/Rect;->left:I

    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->c:Z

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iput v3, v2, Landroid/graphics/Rect;->left:I

    :goto_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->N:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    :goto_4
    const/high16 v2, 0x3f800000    # 1.0f

    iget v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->P:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->T:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    mul-float/2addr v6, v2

    float-to-int v6, v6

    iput v6, v3, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->T:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    int-to-float v6, v6

    mul-float/2addr v6, v2

    float-to-int v6, v6

    iput v6, v3, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->T:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v6, v5

    int-to-float v6, v6

    mul-float/2addr v6, v2

    float-to-int v6, v6

    add-int/2addr v5, v6

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->T:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v4

    int-to-float v5, v5

    mul-float/2addr v2, v5

    float-to-int v2, v2

    add-int/2addr v2, v4

    iput v2, v3, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->U:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->U:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v2, v3, :cond_c

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->U:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v1

    add-int/2addr v3, v7

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    :goto_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->U:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v3

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 764
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->b:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_d

    .line 765
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->t:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 790
    :goto_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->o:Lcom/google/android/apps/gmm/base/views/WebImageView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 791
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->n:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->S:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 792
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->w:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->aa:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 793
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->s:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 794
    return-void

    .line 762
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->d:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->e:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->f:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v2, v3, :cond_5

    :cond_4
    sget v1, Lcom/google/android/apps/gmm/e;->aL:I

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v3, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v2, v3, :cond_0

    sget v1, Lcom/google/android/apps/gmm/e;->aL:I

    goto/16 :goto_0

    :cond_6
    sget v1, Lcom/google/android/apps/gmm/e;->aQ:I

    goto/16 :goto_1

    :cond_7
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->d:Lcom/google/o/h/a/hv;

    if-nez v2, :cond_8

    iget v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->e:I

    if-eqz v2, :cond_9

    :cond_8
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ac:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto/16 :goto_3

    :cond_9
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v12

    iput v3, v2, Landroid/graphics/Rect;->left:I

    goto/16 :goto_3

    :cond_a
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->O:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ab:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto/16 :goto_4

    :cond_b
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    goto/16 :goto_4

    :cond_c
    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->U:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->R:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v3, v1

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_5

    .line 766
    :cond_d
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->d:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_e

    .line 767
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->x:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 768
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    goto/16 :goto_6

    .line 769
    :cond_e
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->a:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_f

    .line 771
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 774
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    goto/16 :goto_6

    .line 776
    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->e:Lcom/google/android/apps/gmm/base/views/w;

    if-eq v1, v2, :cond_10

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_12

    .line 777
    :cond_10
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->g:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_11

    .line 778
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->G:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ad:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 779
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->H:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->ae:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 781
    :cond_11
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->A:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 782
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    goto/16 :goto_6

    .line 783
    :cond_12
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->a:Lcom/google/android/apps/gmm/base/views/w;

    sget-object v2, Lcom/google/android/apps/gmm/base/views/w;->f:Lcom/google/android/apps/gmm/base/views/w;

    if-ne v1, v2, :cond_13

    .line 784
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->D:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 785
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    goto/16 :goto_6

    .line 787
    :cond_13
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->V:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    .line 788
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->r:Landroid/widget/Button;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->W:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v3, v2}, Landroid/view/View;->measure(II)V

    goto/16 :goto_6

    :cond_14
    move v1, v2

    goto/16 :goto_2
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 988
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 989
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    .line 990
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->Q:Ljava/lang/String;

    .line 994
    return-void
.end method

.method public final setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 1112
    if-nez p1, :cond_0

    const-string p1, ""

    .line 1113
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->q:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1114
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->y:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1115
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->p:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1116
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->B:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1117
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/views/FloatingBar;->E:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1118
    return-void
.end method
