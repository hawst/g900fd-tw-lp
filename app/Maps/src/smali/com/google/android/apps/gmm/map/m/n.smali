.class public Lcom/google/android/apps/gmm/map/m/n;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private A:F

.field private B:F

.field private C:F

.field private D:F

.field private E:F

.field private F:F

.field private G:F

.field private final H:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final I:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private final J:Lcom/google/android/apps/gmm/map/b/a/ay;

.field private K:F

.field private L:F

.field private M:F

.field private N:Z

.field private O:Z

.field private P:Z

.field private final Q:Z

.field private R:F

.field private S:F

.field private T:J

.field private final U:Landroid/view/VelocityTracker;

.field private V:Lcom/google/android/apps/gmm/map/m/v;

.field private final W:Lcom/google/android/apps/gmm/map/m/r;

.field a:Landroid/content/Context;

.field final b:Lcom/google/android/apps/gmm/map/m/d;

.field public final c:Lcom/google/android/apps/gmm/map/m/d;

.field d:F

.field e:F

.field f:F

.field g:F

.field h:F

.field i:F

.field public j:J

.field k:F

.field l:F

.field m:Z

.field public n:Z

.field o:Lcom/google/android/apps/gmm/map/m/g;

.field private p:Landroid/view/MotionEvent;

.field private q:Landroid/view/MotionEvent;

.field private final r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;"
        }
    .end annotation
.end field

.field private final t:Lcom/google/android/apps/gmm/map/m/d;

.field private final u:Lcom/google/android/apps/gmm/map/m/d;

.field private final v:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/map/m/k;",
            ">;"
        }
    .end annotation
.end field

.field private w:J

.field private x:F

.field private y:F

.field private z:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/m/r;)V
    .locals 7

    .prologue
    const/high16 v4, 0x43200000    # 160.0f

    const/4 v1, 0x1

    .line 443
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 322
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    .line 332
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    .line 374
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->H:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 380
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 386
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->J:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 397
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/n;->n:Z

    .line 416
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/m/n;->T:J

    .line 444
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 445
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/m/n;->a:Landroid/content/Context;

    .line 446
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledEdgeSlop()I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/m/n;->K:F

    .line 447
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/m/n;->W:Lcom/google/android/apps/gmm/map/m/r;

    .line 449
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->a:Landroid/content/Context;

    iput v4, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iput v4, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v3, v2, Landroid/util/DisplayMetrics;->xdpi:F

    iput v3, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iget v2, v2, Landroid/util/DisplayMetrics;->ydpi:F

    iput v2, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    .line 454
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/map/m/ae;

    const/4 v4, 0x2

    iget v5, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iget v6, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    invoke-direct {v3, v4, p2, v5, v6}, Lcom/google/android/apps/gmm/map/m/ae;-><init>(ILcom/google/android/apps/gmm/map/m/r;FF)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/m/n;->b:Lcom/google/android/apps/gmm/map/m/d;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 455
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/map/m/ae;

    const/4 v4, 0x3

    iget v5, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iget v6, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    invoke-direct {v3, v4, p2, v5, v6}, Lcom/google/android/apps/gmm/map/m/ae;-><init>(ILcom/google/android/apps/gmm/map/m/r;FF)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/m/n;->u:Lcom/google/android/apps/gmm/map/m/d;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 456
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    new-instance v3, Lcom/google/android/apps/gmm/map/m/af;

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iget v5, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    .line 457
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {v3, p2, v4, v5, v0}, Lcom/google/android/apps/gmm/map/m/af;-><init>(Lcom/google/android/apps/gmm/map/m/r;FFF)V

    .line 456
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 461
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.hardware.touchscreen.multitouch.distinct"

    .line 462
    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->Q:Z

    .line 463
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->Q:Z

    if-eqz v0, :cond_2

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/map/m/ab;

    iget v3, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    invoke-direct {v2, p2, v3, v4}, Lcom/google/android/apps/gmm/map/m/ab;-><init>(Lcom/google/android/apps/gmm/map/m/r;FF)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->c:Lcom/google/android/apps/gmm/map/m/d;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 469
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/map/m/z;

    iget v3, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    invoke-direct {v2, p2, v3, v4}, Lcom/google/android/apps/gmm/map/m/z;-><init>(Lcom/google/android/apps/gmm/map/m/r;FF)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->t:Lcom/google/android/apps/gmm/map/m/d;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 470
    new-instance v0, Lcom/google/android/apps/gmm/map/m/g;

    invoke-direct {v0, p1, p2}, Lcom/google/android/apps/gmm/map/m/g;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/map/m/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->o:Lcom/google/android/apps/gmm/map/m/g;

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->o:Lcom/google/android/apps/gmm/map/m/g;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/m/g;->x:Z

    .line 472
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->o:Lcom/google/android/apps/gmm/map/m/g;

    iput-object p2, v0, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    .line 474
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->U:Landroid/view/VelocityTracker;

    .line 475
    return-void

    .line 462
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 467
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    new-instance v2, Lcom/google/android/apps/gmm/map/m/x;

    iget v3, p0, Lcom/google/android/apps/gmm/map/m/n;->R:F

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/n;->S:F

    invoke-direct {v2, p2, v3, v4}, Lcom/google/android/apps/gmm/map/m/x;-><init>(Lcom/google/android/apps/gmm/map/m/r;FF)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->c:Lcom/google/android/apps/gmm/map/m/d;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static a(FF)F
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 1161
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    div-float/2addr v0, v1

    const/high16 v1, -0x40000000    # -2.0f

    const/high16 v2, 0x40000000    # 2.0f

    sub-float/2addr v0, v2

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    add-double/2addr v0, v4

    div-double v0, v4, v0

    double-to-float v0, v0

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x0

    .line 834
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    .line 838
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 839
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/apps/gmm/map/m/n;->w:J

    .line 843
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    new-instance v2, Lcom/google/android/apps/gmm/map/m/k;

    invoke-direct {v2, p1}, Lcom/google/android/apps/gmm/map/m/k;-><init>(Landroid/view/MotionEvent;)V

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 846
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/16 v2, 0x14

    if-le v0, v2, :cond_1

    .line 847
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    .line 851
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/m/k;->d:J

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    iget-wide v6, v0, Lcom/google/android/apps/gmm/map/m/k;->d:J

    sub-long v2, v6, v2

    const-wide/16 v6, 0xfa

    cmp-long v0, v2, v6

    if-ltz v0, :cond_2

    move v0, v5

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_3

    .line 852
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    iput-object v4, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    goto :goto_0

    :cond_2
    move v0, v8

    .line 851
    goto :goto_1

    .line 857
    :cond_3
    sparse-switch v1, :sswitch_data_0

    :goto_2
    move v5, v8

    .line 873
    :sswitch_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->P:Z

    if-eqz v0, :cond_5

    .line 874
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    move v0, v8

    :goto_3
    if-ge v0, v9, :cond_5

    .line 876
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/m/d;

    .line 879
    iget-boolean v2, v1, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-nez v2, :cond_4

    .line 880
    sget-object v10, Lcom/google/android/apps/gmm/map/m/o;->a:[I

    iget-wide v2, p0, Lcom/google/android/apps/gmm/map/m/n;->w:J

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    move-object v7, p2

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/apps/gmm/map/m/d;->a(JLjava/util/LinkedList;ZLjava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/m/e;->ordinal()I

    move-result v2

    aget v2, v10, v2

    packed-switch v2, :pswitch_data_0

    .line 875
    :cond_4
    :goto_4
    :pswitch_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 868
    :sswitch_1
    iput-boolean v8, p0, Lcom/google/android/apps/gmm/map/m/n;->P:Z

    goto :goto_2

    .line 902
    :pswitch_1
    invoke-virtual {v1, p0}, Lcom/google/android/apps/gmm/map/m/d;->a(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 903
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 913
    :cond_5
    :pswitch_2
    if-eqz v5, :cond_6

    .line 914
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->d()V

    .line 915
    iput-boolean v8, p0, Lcom/google/android/apps/gmm/map/m/n;->P:Z

    .line 917
    :cond_6
    return-void

    .line 857
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x6 -> :sswitch_0
        0x106 -> :sswitch_0
    .end sparse-switch

    .line 880
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private b(Landroid/view/MotionEvent;)V
    .locals 12

    .prologue
    const/high16 v11, -0x40800000    # -1.0f

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 702
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->K:F

    .line 703
    iget v4, p0, Lcom/google/android/apps/gmm/map/m/n;->L:F

    .line 704
    iget v5, p0, Lcom/google/android/apps/gmm/map/m/n;->M:F

    .line 705
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v3

    .line 706
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    .line 707
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    sub-float/2addr v8, v9

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    add-float/2addr v7, v8

    .line 708
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    sub-float/2addr v9, v10

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    add-float/2addr v8, v9

    .line 710
    cmpg-float v9, v3, v0

    if-ltz v9, :cond_0

    cmpg-float v9, v6, v0

    if-ltz v9, :cond_0

    cmpl-float v3, v3, v4

    if-gtz v3, :cond_0

    cmpl-float v3, v6, v5

    if-lez v3, :cond_2

    :cond_0
    move v3, v2

    .line 711
    :goto_0
    cmpg-float v6, v7, v0

    if-ltz v6, :cond_1

    cmpg-float v0, v8, v0

    if-ltz v0, :cond_1

    cmpl-float v0, v7, v4

    if-gtz v0, :cond_1

    cmpl-float v0, v8, v5

    if-lez v0, :cond_3

    :cond_1
    move v0, v2

    .line 713
    :goto_1
    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    .line 714
    iput v11, p0, Lcom/google/android/apps/gmm/map/m/n;->d:F

    .line 715
    iput v11, p0, Lcom/google/android/apps/gmm/map/m/n;->e:F

    .line 716
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    .line 729
    :goto_2
    return-void

    :cond_2
    move v3, v1

    .line 710
    goto :goto_0

    :cond_3
    move v0, v1

    .line 711
    goto :goto_1

    .line 717
    :cond_4
    if-eqz v3, :cond_5

    .line 718
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->d:F

    .line 719
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->e:F

    .line 720
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    goto :goto_2

    .line 721
    :cond_5
    if-eqz v0, :cond_6

    .line 722
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->d:F

    .line 723
    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->e:F

    .line 724
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    goto :goto_2

    .line 726
    :cond_6
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/m/n;->P:Z

    .line 727
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    goto :goto_2
.end method

.method private c()Ljava/util/EnumSet;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/google/android/apps/gmm/map/m/p;",
            ">;"
        }
    .end annotation

    .prologue
    .line 677
    const-class v0, Lcom/google/android/apps/gmm/map/m/p;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    .line 678
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->b:Lcom/google/android/apps/gmm/map/m/d;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->u:Lcom/google/android/apps/gmm/map/m/d;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-eqz v1, :cond_4

    .line 679
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/map/m/p;->d:Lcom/google/android/apps/gmm/map/m/p;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 686
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->t:Lcom/google/android/apps/gmm/map/m/d;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-eqz v1, :cond_2

    .line 687
    sget-object v1, Lcom/google/android/apps/gmm/map/m/p;->b:Lcom/google/android/apps/gmm/map/m/p;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 689
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->c:Lcom/google/android/apps/gmm/map/m/d;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/n;->Q:Z

    if-nez v1, :cond_3

    .line 690
    sget-object v1, Lcom/google/android/apps/gmm/map/m/p;->c:Lcom/google/android/apps/gmm/map/m/p;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 692
    :cond_3
    return-object v0

    .line 680
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/n;->Q:Z

    if-nez v1, :cond_1

    .line 683
    sget-object v1, Lcom/google/android/apps/gmm/map/m/p;->a:Lcom/google/android/apps/gmm/map/m/p;

    invoke-virtual {v0, v1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private c(Landroid/view/MotionEvent;)V
    .locals 14

    .prologue
    .line 942
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    .line 944
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->B:F

    .line 945
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->C:F

    .line 946
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->D:F

    .line 947
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->i:F

    .line 948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->m:Z

    .line 949
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->O:Z

    .line 951
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    .line 953
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    .line 954
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    .line 955
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    .line 956
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    .line 957
    const/4 v5, 0x0

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v5

    .line 958
    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v6

    .line 959
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {p1, v7}, Landroid/view/MotionEvent;->getX(I)F

    move-result v7

    .line 960
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {p1, v8}, Landroid/view/MotionEvent;->getY(I)F

    move-result v8

    .line 962
    sub-float v9, v3, v1

    .line 963
    sub-float v10, v4, v2

    .line 964
    sub-float v11, v7, v5

    .line 965
    sub-float v12, v8, v6

    .line 966
    iput v9, p0, Lcom/google/android/apps/gmm/map/m/n;->x:F

    .line 967
    iput v10, p0, Lcom/google/android/apps/gmm/map/m/n;->y:F

    .line 968
    iput v11, p0, Lcom/google/android/apps/gmm/map/m/n;->z:F

    .line 969
    iput v12, p0, Lcom/google/android/apps/gmm/map/m/n;->A:F

    .line 970
    iput v2, p0, Lcom/google/android/apps/gmm/map/m/n;->g:F

    .line 971
    iput v6, p0, Lcom/google/android/apps/gmm/map/m/n;->h:F

    .line 973
    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v10, v11

    add-float/2addr v10, v5

    iput v10, p0, Lcom/google/android/apps/gmm/map/m/n;->d:F

    .line 974
    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v10, v12

    add-float/2addr v10, v6

    iput v10, p0, Lcom/google/android/apps/gmm/map/m/n;->e:F

    .line 975
    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v9, v10

    add-float/2addr v9, v1

    iput v9, p0, Lcom/google/android/apps/gmm/map/m/n;->f:F

    .line 976
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v10

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    iput-wide v10, p0, Lcom/google/android/apps/gmm/map/m/n;->j:J

    .line 978
    const/4 v9, 0x0

    invoke-virtual {p1, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {p1, v10}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v10

    add-float/2addr v9, v10

    iput v9, p0, Lcom/google/android/apps/gmm/map/m/n;->F:F

    .line 979
    const/4 v9, 0x0

    invoke-virtual {v0, v9}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v9

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    invoke-virtual {v0, v10}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v0

    add-float/2addr v0, v9

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->G:F

    .line 981
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->H:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v9, p0, Lcom/google/android/apps/gmm/map/m/n;->x:F

    iget v10, p0, Lcom/google/android/apps/gmm/map/m/n;->y:F

    iput v9, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v10, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 982
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    sub-float v1, v5, v1

    sub-float v2, v6, v2

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 983
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->J:Lcom/google/android/apps/gmm/map/b/a/ay;

    sub-float v1, v7, v3

    sub-float v2, v8, v4

    iput v1, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iput v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    .line 985
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->H:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->J:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v2, v3

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->H:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget v3, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    iget v4, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->b:F

    mul-float/2addr v3, v4

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/ay;->c:F

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->k:F

    .line 986
    sget-object v0, Lcom/google/android/apps/gmm/map/b/a/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->H:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->J:Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)F

    move-result v0

    sget-object v1, Lcom/google/android/apps/gmm/map/b/a/ay;->a:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->H:Lcom/google/android/apps/gmm/map/b/a/ay;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/m/n;->I:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 987
    invoke-static {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/ay;->b(Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;Lcom/google/android/apps/gmm/map/b/a/ay;)F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->l:F

    .line 988
    return-void
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 825
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    .line 826
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/d;

    .line 827
    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    const-string v5, "Ending inactive gesture: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v2

    if-nez v4, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-static {v5, v6}, Lcom/google/b/a/aq;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 828
    :cond_0
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/m/d;->c(Lcom/google/android/apps/gmm/map/m/n;)V

    .line 825
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 830
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 831
    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 991
    iput-object v5, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    .line 992
    iput-object v5, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    .line 993
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    .line 994
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/m/n;->P:Z

    .line 995
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 996
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/k;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    iput-object v5, v0, Lcom/google/android/apps/gmm/map/m/k;->a:Landroid/view/MotionEvent;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->v:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 997
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_2

    .line 998
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->r:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/d;

    .line 999
    iget-boolean v3, v0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-eqz v3, :cond_1

    .line 1000
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/m/d;->c(Lcom/google/android/apps/gmm/map/m/n;)V

    .line 997
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1003
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()F
    .locals 4

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/high16 v3, -0x40800000    # -1.0f

    .line 1094
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->t:Lcom/google/android/apps/gmm/map/m/d;

    if-eqz v1, :cond_1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_2

    .line 1103
    :cond_0
    :goto_1
    return v0

    .line 1094
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1097
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    invoke-virtual {v2}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1100
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->D:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_5

    .line 1101
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->B:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->z:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/m/n;->A:F

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->B:F

    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->B:F

    iget v1, p0, Lcom/google/android/apps/gmm/map/m/n;->C:F

    cmpl-float v1, v1, v3

    if-nez v1, :cond_4

    iget v1, p0, Lcom/google/android/apps/gmm/map/m/n;->x:F

    iget v2, p0, Lcom/google/android/apps/gmm/map/m/n;->y:F

    mul-float/2addr v1, v1

    mul-float/2addr v2, v2

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/m/n;->C:F

    :cond_4
    iget v1, p0, Lcom/google/android/apps/gmm/map/m/n;->C:F

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->D:F

    .line 1103
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->D:F

    goto :goto_1
.end method

.method public final a(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    const/4 v11, 0x0

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 499
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-nez v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->U:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 508
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->U:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 512
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 526
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 528
    shr-int/lit8 v0, v3, 0x8

    and-int/lit16 v4, v0, 0xff

    .line 530
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->P:Z

    if-nez v0, :cond_e

    .line 531
    const/4 v0, 0x5

    if-eq v3, v0, :cond_1

    const/16 v0, 0x105

    if-eq v3, v0, :cond_1

    if-nez v3, :cond_a

    .line 535
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 536
    iget v3, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/gmm/map/m/n;->K:F

    sub-float/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/gmm/map/m/n;->L:F

    .line 537
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v0, v0

    iget v3, p0, Lcom/google/android/apps/gmm/map/m/n;->K:F

    sub-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->M:F

    .line 540
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->e()V

    .line 542
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    .line 543
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/m/n;->j:J

    .line 545
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/m/n;->c(Landroid/view/MotionEvent;)V

    .line 550
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/m/n;->b(Landroid/view/MotionEvent;)V

    .line 638
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 653
    :goto_2
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    if-ne v0, v12, :cond_3

    .line 654
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/m/n;->T:J

    const-wide/16 v8, 0x64

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-gtz v0, :cond_18

    move v0, v1

    :goto_3
    if-nez v0, :cond_1c

    .line 655
    :cond_3
    iget-object v8, p0, Lcom/google/android/apps/gmm/map/m/n;->o:Lcom/google/android/apps/gmm/map/m/g;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    if-nez v0, :cond_4

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    :cond_4
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    and-int/lit16 v0, v9, 0xff

    const/4 v3, 0x6

    if-ne v0, v3, :cond_19

    move v7, v1

    :goto_4
    if-eqz v7, :cond_1a

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    :goto_5
    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    move v6, v2

    :goto_6
    if-ge v6, v5, :cond_1b

    if-eq v0, v6, :cond_5

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getX(I)F

    move-result v10

    add-float/2addr v4, v10

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v10

    add-float/2addr v3, v10

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_6

    .line 514
    :sswitch_0
    new-instance v3, Lcom/google/android/apps/gmm/map/m/v;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/m/n;->U:Landroid/view/VelocityTracker;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->c()Ljava/util/EnumSet;

    move-result-object v5

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->n:Z

    if-nez v0, :cond_6

    move v0, v1

    :goto_7
    invoke-direct {v3, p1, v4, v5, v0}, Lcom/google/android/apps/gmm/map/m/v;-><init>(Landroid/view/MotionEvent;Landroid/view/VelocityTracker;Ljava/util/EnumSet;Z)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/m/n;->V:Lcom/google/android/apps/gmm/map/m/v;

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_7

    .line 518
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/apps/gmm/map/m/n;->T:J

    const-wide/16 v8, 0x64

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-gtz v0, :cond_8

    move v0, v1

    :goto_8
    if-nez v0, :cond_7

    .line 519
    new-instance v3, Lcom/google/android/apps/gmm/map/m/v;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/m/n;->U:Landroid/view/VelocityTracker;

    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->c()Ljava/util/EnumSet;

    move-result-object v5

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->n:Z

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    invoke-direct {v3, p1, v4, v5, v0}, Lcom/google/android/apps/gmm/map/m/v;-><init>(Landroid/view/MotionEvent;Landroid/view/VelocityTracker;Ljava/util/EnumSet;Z)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/m/n;->V:Lcom/google/android/apps/gmm/map/m/v;

    .line 522
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->W:Lcom/google/android/apps/gmm/map/m/r;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/m/n;->V:Lcom/google/android/apps/gmm/map/m/v;

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/m/r;->a(Lcom/google/android/apps/gmm/map/m/u;)V

    goto/16 :goto_0

    :cond_8
    move v0, v2

    .line 518
    goto :goto_8

    :cond_9
    move v0, v2

    .line 519
    goto :goto_9

    .line 551
    :cond_a
    if-ne v3, v12, :cond_b

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    if-eqz v0, :cond_b

    .line 553
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/m/n;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    .line 554
    :cond_b
    const/4 v0, 0x6

    if-eq v3, v0, :cond_c

    const/16 v0, 0x106

    if-eq v3, v0, :cond_c

    if-ne v3, v1, :cond_2

    :cond_c
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    if-eqz v0, :cond_2

    .line 559
    if-nez v4, :cond_d

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 560
    :goto_a
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iput v3, p0, Lcom/google/android/apps/gmm/map/m/n;->d:F

    .line 561
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->e:F

    goto/16 :goto_1

    :cond_d
    move v0, v2

    .line 559
    goto :goto_a

    .line 563
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    move v0, v1

    :goto_b
    if-nez v0, :cond_11

    .line 566
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    invoke-direct {p0, v0, v11}, Lcom/google/android/apps/gmm/map/m/n;->a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V

    .line 572
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    move v0, v1

    :goto_c
    if-eqz v0, :cond_2

    .line 575
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->F:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/m/n;->G:F

    div-float/2addr v0, v3

    const v3, 0x3f2b851f    # 0.67f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    .line 576
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    goto/16 :goto_1

    :cond_f
    move v0, v2

    .line 563
    goto :goto_b

    :cond_10
    move v0, v2

    .line 572
    goto :goto_c

    .line 583
    :cond_11
    sparse-switch v3, :sswitch_data_1

    goto/16 :goto_1

    .line 588
    :sswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/m/n;->c(Landroid/view/MotionEvent;)V

    .line 591
    if-nez v4, :cond_14

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 592
    :goto_d
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    iput v3, p0, Lcom/google/android/apps/gmm/map/m/n;->d:F

    .line 593
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->e:F

    .line 595
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    if-nez v0, :cond_12

    .line 596
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->d()V

    .line 602
    :cond_12
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->n:Z

    if-nez v0, :cond_13

    .line 603
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v11}, Lcom/google/android/apps/gmm/map/m/n;->a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V

    .line 604
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    if-nez v0, :cond_13

    .line 605
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->d()V

    .line 609
    :cond_13
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->e()V

    goto/16 :goto_1

    :cond_14
    move v0, v2

    .line 591
    goto :goto_d

    .line 613
    :sswitch_3
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->N:Z

    if-nez v0, :cond_15

    .line 614
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->d()V

    .line 617
    :cond_15
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/m/n;->e()V

    goto/16 :goto_1

    .line 621
    :sswitch_4
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/map/m/n;->c(Landroid/view/MotionEvent;)V

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    invoke-direct {p0, v0, v11}, Lcom/google/android/apps/gmm/map/m/n;->a(Landroid/view/MotionEvent;Ljava/lang/StringBuilder;)V

    .line 629
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->F:F

    iget v3, p0, Lcom/google/android/apps/gmm/map/m/n;->G:F

    div-float/2addr v0, v3

    const v3, 0x3f2b851f    # 0.67f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    .line 630
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    move v4, v2

    :goto_e
    if-ge v3, v5, :cond_17

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->s:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/d;

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-nez v6, :cond_16

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_16
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/m/d;->e(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    or-int/2addr v4, v0

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_e

    :cond_17
    if-eqz v4, :cond_2

    .line 631
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    goto/16 :goto_1

    .line 641
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/m/n;->T:J

    goto/16 :goto_2

    .line 646
    :pswitch_2
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/google/android/apps/gmm/map/m/n;->T:J

    goto/16 :goto_2

    :cond_18
    move v0, v2

    .line 654
    goto/16 :goto_3

    :cond_19
    move v7, v2

    .line 655
    goto/16 :goto_4

    :cond_1a
    const/4 v0, -0x1

    goto/16 :goto_5

    :cond_1b
    if-eqz v7, :cond_1d

    add-int/lit8 v0, v5, -0x1

    :goto_f
    int-to-float v6, v0

    div-float/2addr v4, v6

    int-to-float v0, v0

    div-float/2addr v3, v0

    and-int/lit16 v0, v9, 0xff

    packed-switch v0, :pswitch_data_1

    .line 660
    :cond_1c
    :goto_10
    :pswitch_3
    return v1

    :cond_1d
    move v0, v5

    .line 655
    goto :goto_f

    :pswitch_4
    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->t:F

    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->v:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->u:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->w:F

    invoke-virtual {v8, p1}, Lcom/google/android/apps/gmm/map/m/g;->a(Landroid/view/MotionEvent;)V

    goto :goto_10

    :pswitch_5
    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->t:F

    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->v:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->u:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->w:F

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    const/16 v3, 0x3e8

    iget v4, v8, Lcom/google/android/apps/gmm/map/m/g;->f:I

    int-to-float v4, v4

    invoke-virtual {v0, v3, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v4, v3}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v4

    iget-object v6, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v6, v3}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v3

    :goto_11
    if-ge v2, v5, :cond_1c

    if-eq v2, v0, :cond_1e

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    iget-object v7, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v7, v6}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v7

    mul-float/2addr v7, v4

    iget-object v9, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v9, v6}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v6

    mul-float/2addr v6, v3

    add-float/2addr v6, v7

    const/4 v7, 0x0

    cmpg-float v6, v6, v7

    if-gez v6, :cond_1e

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_10

    :cond_1e
    add-int/lit8 v2, v2, 0x1

    goto :goto_11

    :pswitch_6
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    if-eqz v0, :cond_26

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1f

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v5, v13}, Landroid/os/Handler;->removeMessages(I)V

    :cond_1f
    iget-object v5, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    if-eqz v5, :cond_25

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/m/g;->r:Landroid/view/MotionEvent;

    if-eqz v5, :cond_25

    if-eqz v0, :cond_25

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/m/g;->r:Landroid/view/MotionEvent;

    iget-boolean v6, v8, Lcom/google/android/apps/gmm/map/m/g;->p:Z

    if-nez v6, :cond_22

    move v0, v2

    :goto_12
    if-eqz v0, :cond_25

    iput-boolean v1, v8, Lcom/google/android/apps/gmm/map/m/g;->s:Z

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-interface {v0, v5}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTap(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    iget-object v5, v8, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v5, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    or-int/2addr v0, v5

    :goto_13
    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->t:F

    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->v:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->u:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->w:F

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    if-eqz v3, :cond_20

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->recycle()V

    :cond_20
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    iput-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    iput-boolean v1, v8, Lcom/google/android/apps/gmm/map/m/g;->o:Z

    iput-boolean v1, v8, Lcom/google/android/apps/gmm/map/m/g;->p:Z

    iput-boolean v1, v8, Lcom/google/android/apps/gmm/map/m/g;->m:Z

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->n:Z

    iget-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->x:Z

    if-eqz v2, :cond_21

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v2, v12}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sget v3, Lcom/google/android/apps/gmm/map/m/g;->h:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    sget v3, Lcom/google/android/apps/gmm/map/m/g;->g:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v2, v12, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    :cond_21
    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-virtual {v3}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sget v3, Lcom/google/android/apps/gmm/map/m/g;->h:I

    int-to-long v6, v3

    add-long/2addr v4, v6

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    invoke-interface {v2, p1}, Lcom/google/android/apps/gmm/map/m/i;->b(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v0, v2

    goto/16 :goto_10

    :cond_22
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {v5}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v10

    sub-long/2addr v6, v10

    sget v5, Lcom/google/android/apps/gmm/map/m/g;->i:I

    int-to-long v10, v5

    cmp-long v5, v6, v10

    if-lez v5, :cond_23

    move v0, v2

    goto :goto_12

    :cond_23
    invoke-virtual {v0}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v5, v6

    invoke-virtual {v0}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    sub-int/2addr v0, v6

    mul-int/2addr v5, v5

    mul-int/2addr v0, v0

    add-int/2addr v0, v5

    iget v5, v8, Lcom/google/android/apps/gmm/map/m/g;->d:I

    if-ge v0, v5, :cond_24

    move v0, v1

    goto/16 :goto_12

    :cond_24
    move v0, v2

    goto/16 :goto_12

    :cond_25
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    sget v5, Lcom/google/android/apps/gmm/map/m/g;->i:I

    int-to-long v6, v5

    invoke-virtual {v0, v13, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_26
    move v0, v2

    goto/16 :goto_13

    :pswitch_7
    iget v0, v8, Lcom/google/android/apps/gmm/map/m/g;->t:F

    sub-float/2addr v0, v4

    iget v5, v8, Lcom/google/android/apps/gmm/map/m/g;->u:F

    sub-float/2addr v5, v3

    iget-boolean v6, v8, Lcom/google/android/apps/gmm/map/m/g;->s:Z

    if-eqz v6, :cond_27

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    goto/16 :goto_10

    :cond_27
    iget-boolean v6, v8, Lcom/google/android/apps/gmm/map/m/g;->o:Z

    if-eqz v6, :cond_29

    iget v6, v8, Lcom/google/android/apps/gmm/map/m/g;->v:F

    sub-float v6, v4, v6

    float-to-int v6, v6

    iget v7, v8, Lcom/google/android/apps/gmm/map/m/g;->w:F

    sub-float v7, v3, v7

    float-to-int v7, v7

    mul-int/2addr v6, v6

    mul-int/2addr v7, v7

    add-int/2addr v6, v7

    iget v7, v8, Lcom/google/android/apps/gmm/map/m/g;->b:I

    if-le v6, v7, :cond_28

    int-to-double v10, v6

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v7, v10

    iget v9, v8, Lcom/google/android/apps/gmm/map/m/g;->a:I

    int-to-float v9, v9

    sub-float v9, v7, v9

    div-float v7, v9, v7

    iget-object v9, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    iget-object v10, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    mul-float/2addr v0, v7

    mul-float/2addr v5, v7

    invoke-interface {v9, v10, p1, v0, v5}, Lcom/google/android/apps/gmm/map/m/i;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->t:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->u:F

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->o:Z

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v0, v12}, Landroid/os/Handler;->removeMessages(I)V

    :cond_28
    iget v0, v8, Lcom/google/android/apps/gmm/map/m/g;->c:I

    if-le v6, v0, :cond_1c

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->p:Z

    goto/16 :goto_10

    :cond_29
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v6

    if-gez v2, :cond_2a

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v6

    if-ltz v2, :cond_1c

    :cond_2a
    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    iget-object v6, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-interface {v2, v6, p1, v0, v5}, Lcom/google/android/apps/gmm/map/m/i;->a(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    iput v4, v8, Lcom/google/android/apps/gmm/map/m/g;->t:F

    iput v3, v8, Lcom/google/android/apps/gmm/map/m/g;->u:F

    goto/16 :goto_10

    :pswitch_8
    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->m:Z

    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v3

    iget-boolean v0, v8, Lcom/google/android/apps/gmm/map/m/g;->s:Z

    if-eqz v0, :cond_2d

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->l:Landroid/view/GestureDetector$OnDoubleTapListener;

    invoke-interface {v0, p1}, Landroid/view/GestureDetector$OnDoubleTapListener;->onDoubleTapEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    :goto_14
    iget-object v4, v8, Lcom/google/android/apps/gmm/map/m/g;->r:Landroid/view/MotionEvent;

    if-eqz v4, :cond_2b

    iget-object v4, v8, Lcom/google/android/apps/gmm/map/m/g;->r:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    :cond_2b
    iput-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->r:Landroid/view/MotionEvent;

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    if-eqz v3, :cond_2c

    iget-object v3, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v3}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v11, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    :cond_2c
    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->s:Z

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v2, v12}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v2, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    invoke-interface {v2, p1}, Lcom/google/android/apps/gmm/map/m/i;->c(Landroid/view/MotionEvent;)Z

    move-result v2

    or-int/2addr v0, v2

    goto/16 :goto_10

    :cond_2d
    iget-boolean v0, v8, Lcom/google/android/apps/gmm/map/m/g;->n:Z

    if-eqz v0, :cond_2e

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->j:Landroid/os/Handler;

    invoke-virtual {v0, v13}, Landroid/os/Handler;->removeMessages(I)V

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->n:Z

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/i;->f(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_14

    :cond_2e
    iget-boolean v0, v8, Lcom/google/android/apps/gmm/map/m/g;->o:Z

    if-eqz v0, :cond_2f

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/i;->f(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_14

    :cond_2f
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    const/16 v5, 0x3e8

    iget v6, v8, Lcom/google/android/apps/gmm/map/m/g;->f:I

    int-to-float v6, v6

    invoke-virtual {v0, v5, v6}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    invoke-virtual {v0, v4}, Landroid/view/VelocityTracker;->getYVelocity(I)F

    move-result v5

    invoke-virtual {v0, v4}, Landroid/view/VelocityTracker;->getXVelocity(I)F

    move-result v0

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v6, v8, Lcom/google/android/apps/gmm/map/m/g;->e:I

    int-to-float v6, v6

    cmpl-float v4, v4, v6

    if-gtz v4, :cond_30

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v6, v8, Lcom/google/android/apps/gmm/map/m/g;->e:I

    int-to-float v6, v6

    cmpl-float v4, v4, v6

    if-lez v4, :cond_31

    :cond_30
    iget-object v4, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    iget-object v6, v8, Lcom/google/android/apps/gmm/map/m/g;->q:Landroid/view/MotionEvent;

    invoke-interface {v4, v6, p1, v0, v5}, Lcom/google/android/apps/gmm/map/m/i;->b(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    goto :goto_14

    :pswitch_9
    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->k:Lcom/google/android/apps/gmm/map/m/i;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/map/m/i;->d(Landroid/view/MotionEvent;)Z

    invoke-virtual {v8, p1}, Lcom/google/android/apps/gmm/map/m/g;->a(Landroid/view/MotionEvent;)V

    iget-object v0, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    iput-object v11, v8, Lcom/google/android/apps/gmm/map/m/g;->y:Landroid/view/VelocityTracker;

    iput-boolean v2, v8, Lcom/google/android/apps/gmm/map/m/g;->m:Z

    goto/16 :goto_10

    :cond_31
    move v0, v2

    goto/16 :goto_14

    .line 512
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x6 -> :sswitch_0
    .end sparse-switch

    .line 638
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    .line 583
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_4
        0x3 -> :sswitch_3
        0x6 -> :sswitch_2
        0x106 -> :sswitch_2
    .end sparse-switch

    .line 655
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_9
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method b()F
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1177
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->c:Lcom/google/android/apps/gmm/map/m/d;

    if-eqz v1, :cond_1

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    if-nez v1, :cond_2

    .line 1195
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v1, v3

    .line 1177
    goto :goto_0

    .line 1180
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    invoke-virtual {v1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    invoke-virtual {v4}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    if-ne v1, v4, :cond_0

    .line 1183
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/n;->O:Z

    if-nez v0, :cond_3

    .line 1184
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    .line 1185
    invoke-virtual {v0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    .line 1186
    invoke-virtual {v5}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/m/n;->q:Landroid/view/MotionEvent;

    .line 1187
    invoke-virtual {v6}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    .line 1184
    sub-float v0, v4, v0

    float-to-double v6, v0

    sub-float v0, v5, v1

    float-to-double v0, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 1188
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    .line 1189
    invoke-virtual {v1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    invoke-virtual {v4, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    .line 1190
    invoke-virtual {v5}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v4, v5}, Landroid/view/MotionEvent;->getX(I)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/m/n;->p:Landroid/view/MotionEvent;

    .line 1191
    invoke-virtual {v6}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v5, v6}, Landroid/view/MotionEvent;->getY(I)F

    move-result v5

    .line 1188
    sub-float v1, v4, v1

    float-to-double v6, v1

    sub-float v1, v5, v3

    float-to-double v4, v1

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v4

    double-to-float v1, v4

    .line 1192
    invoke-static {v1, v0}, Lcom/google/android/apps/gmm/map/m/d;->a(FF)F

    move-result v0

    const v1, 0x42652ee0

    mul-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/m/n;->E:F

    .line 1193
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/map/m/n;->O:Z

    .line 1195
    :cond_3
    iget v0, p0, Lcom/google/android/apps/gmm/map/m/n;->E:F

    goto/16 :goto_1
.end method
