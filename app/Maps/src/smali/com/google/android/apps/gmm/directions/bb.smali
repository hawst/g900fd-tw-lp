.class Lcom/google/android/apps/gmm/directions/bb;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field final a:Landroid/graphics/Rect;

.field final b:Landroid/graphics/Rect;

.field final c:Landroid/graphics/Paint;

.field final d:Landroid/graphics/Paint;

.field final e:Landroid/graphics/Paint;

.field final f:Landroid/graphics/Paint;

.field final g:Landroid/graphics/Paint;

.field h:F

.field i:Lcom/google/maps/g/a/bi;

.field j:I

.field k:I

.field l:I

.field m:I

.field n:I

.field o:I

.field p:Z

.field private final q:Lcom/google/android/apps/gmm/directions/b;

.field private final r:Lcom/google/android/apps/gmm/directions/b;

.field private final s:Landroid/graphics/Path;

.field private final t:Landroid/graphics/Path;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/directions/b;Lcom/google/android/apps/gmm/directions/b;)V
    .locals 11

    .prologue
    const/4 v10, -0x1

    const/high16 v9, -0x1000000

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x1

    .line 73
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 75
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v4, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    .line 76
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v6, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v6, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 78
    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 79
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v9, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 v10, 0x40a00000    # 5.0f

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 73
    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/directions/bb;-><init>(Lcom/google/android/apps/gmm/directions/b;Lcom/google/android/apps/gmm/directions/b;Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/directions/b;Lcom/google/android/apps/gmm/directions/b;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 11

    .prologue
    .line 88
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    new-instance v5, Landroid/graphics/Path;

    invoke-direct {v5}, Landroid/graphics/Path;-><init>()V

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/apps/gmm/directions/bb;-><init>(Lcom/google/android/apps/gmm/directions/b;Lcom/google/android/apps/gmm/directions/b;Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V

    .line 99
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/directions/b;Lcom/google/android/apps/gmm/directions/b;Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Path;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;Landroid/graphics/Paint;F)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 36
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->a:Landroid/graphics/Rect;

    .line 37
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->b:Landroid/graphics/Rect;

    .line 105
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/bb;->q:Lcom/google/android/apps/gmm/directions/b;

    .line 106
    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    .line 107
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    .line 108
    iput-object p4, p0, Lcom/google/android/apps/gmm/directions/bb;->c:Landroid/graphics/Paint;

    .line 109
    iput-object p5, p0, Lcom/google/android/apps/gmm/directions/bb;->s:Landroid/graphics/Path;

    .line 110
    iput-object p6, p0, Lcom/google/android/apps/gmm/directions/bb;->d:Landroid/graphics/Paint;

    .line 111
    iput-object p7, p0, Lcom/google/android/apps/gmm/directions/bb;->e:Landroid/graphics/Paint;

    .line 112
    iput-object p8, p0, Lcom/google/android/apps/gmm/directions/bb;->f:Landroid/graphics/Paint;

    .line 113
    iput p10, p0, Lcom/google/android/apps/gmm/directions/bb;->h:F

    .line 114
    iput-object p9, p0, Lcom/google/android/apps/gmm/directions/bb;->g:Landroid/graphics/Paint;

    .line 115
    return-void
.end method

.method private a(F)F
    .locals 3

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/apps/gmm/directions/bb;->k:I

    int-to-float v2, v2

    sub-float/2addr v0, v2

    sub-float/2addr v0, p1

    :goto_0
    add-float/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/gmm/directions/bb;->j:I

    int-to-float v0, v0

    add-float/2addr v0, p1

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFLjava/lang/CharSequence;Landroid/graphics/Rect;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 261
    invoke-virtual {p6}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p4, v0

    iget v1, p6, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    sub-float v5, v0, v1

    .line 262
    iget v0, p6, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    sub-float v1, p3, v0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    int-to-float v0, v0

    sub-float v4, v1, v0

    .line 263
    invoke-interface {p5}, Ljava/lang/CharSequence;->length()I

    move-result v3

    move-object v0, p1

    move-object v1, p5

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 264
    return-void

    .line 262
    :cond_0
    invoke-virtual {p6}, Landroid/graphics/Rect;->width()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->m:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iput v3, v0, Lcom/google/android/apps/gmm/directions/b;->a:F

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->q:Lcom/google/android/apps/gmm/directions/b;

    .line 312
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->j:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->k:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->h:F

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/bb;->a:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/bb;->b:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    sub-float/2addr v3, v4

    .line 311
    iput v3, v0, Lcom/google/android/apps/gmm/directions/b;->a:F

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    if-eqz v0, :cond_3

    .line 314
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/bb;->s:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    invoke-virtual {v3}, Landroid/graphics/Path;->reset()V

    iput v2, p0, Lcom/google/android/apps/gmm/directions/bb;->n:I

    iput v0, p0, Lcom/google/android/apps/gmm/directions/bb;->o:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    int-to-float v0, v0

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v3, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->q:Lcom/google/android/apps/gmm/directions/b;

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->n:I

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/bb;->a(F)F

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->s:Landroid/graphics/Path;

    invoke-virtual {v0, v4, v3}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    :goto_0
    invoke-virtual {v5, v0, v3}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    invoke-virtual {v0, v4, v3}, Landroid/graphics/Path;->lineTo(FF)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->i:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->h:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->n:I

    add-int/2addr v0, v4

    iput v0, p0, Lcom/google/android/apps/gmm/directions/bb;->n:I

    iget v0, p0, Lcom/google/android/apps/gmm/directions/bb;->o:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/gmm/directions/bb;->o:I

    iget v0, p0, Lcom/google/android/apps/gmm/directions/bb;->o:I

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/bb;->s:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bb;->q:Lcom/google/android/apps/gmm/directions/b;

    iget v5, p0, Lcom/google/android/apps/gmm/directions/bb;->n:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/directions/bb;->a(F)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    int-to-float v6, v0

    invoke-virtual {v5, v6}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v6

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    add-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bb;->q:Lcom/google/android/apps/gmm/directions/b;

    iget v5, p0, Lcom/google/android/apps/gmm/directions/bb;->n:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v4

    invoke-direct {p0, v4}, Lcom/google/android/apps/gmm/directions/bb;->a(F)F

    move-result v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    int-to-float v0, v0

    invoke-virtual {v5, v0}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v5

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget v6, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    add-float/2addr v0, v5

    invoke-virtual {v3, v4, v0}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    add-float/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v3, v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    :cond_2
    invoke-virtual {v0, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 316
    :cond_3
    return-void

    .line 314
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    goto :goto_3
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 220
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->t:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/bb;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->s:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/bb;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 222
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/directions/bb;->a(F)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v2, v4

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/gmm/directions/bb;->h:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bb;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v2, p0, Lcom/google/android/apps/gmm/directions/bb;->h:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bb;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->q:Lcom/google/android/apps/gmm/directions/b;

    iget v1, p0, Lcom/google/android/apps/gmm/directions/bb;->n:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/directions/bb;->a(F)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    int-to-float v0, v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v2, v4

    int-to-float v2, v2

    add-float/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/gmm/directions/bb;->h:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bb;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v2, p0, Lcom/google/android/apps/gmm/directions/bb;->h:F

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/bb;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/bb;->e:Landroid/graphics/Paint;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/directions/bb;->p:Z

    if-eqz v0, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v1, v4

    int-to-float v1, v1

    add-float v4, v1, v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/bb;->r:Lcom/google/android/apps/gmm/directions/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    iget v0, v0, Lcom/google/maps/g/a/ai;->b:I

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/directions/b;->a(F)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/google/android/apps/gmm/directions/bb;->l:I

    add-int/2addr v1, v5

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/bb;->a:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    add-float v7, v0, v1

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->e:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ai;->d()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/bb;->i:Lcom/google/maps/g/a/bi;

    iget-object v0, v0, Lcom/google/maps/g/a/bi;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/a/ai;->g()Lcom/google/maps/g/a/ai;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/a/ai;

    invoke-virtual {v0}, Lcom/google/maps/g/a/ai;->d()Ljava/lang/String;

    move-result-object v8

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/bb;->b:Landroid/graphics/Rect;

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/bb;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFLjava/lang/CharSequence;Landroid/graphics/Rect;)V

    iget-object v6, p0, Lcom/google/android/apps/gmm/directions/bb;->a:Landroid/graphics/Rect;

    move-object v0, p0

    move-object v1, p1

    move v4, v7

    move-object v5, v8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/directions/bb;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;FFLjava/lang/CharSequence;Landroid/graphics/Rect;)V

    .line 224
    return-void

    .line 223
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v3, v0

    goto/16 :goto_0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 351
    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 343
    return-void
.end method

.method public setBounds(IIII)V
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 269
    iget v1, v0, Landroid/graphics/Rect;->left:I

    if-ne v1, p1, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->right:I

    if-ne v1, p3, :cond_0

    iget v1, v0, Landroid/graphics/Rect;->top:I

    if-ne v1, p2, :cond_0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    if-eq v0, p4, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 272
    :goto_0
    invoke-super {p0, p1, p2, p3, p4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 273
    if-eqz v0, :cond_1

    .line 274
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->a()V

    .line 276
    :cond_1
    return-void

    .line 269
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBounds(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Rect;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 281
    :goto_0
    invoke-super {p0, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 282
    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/bb;->a()V

    .line 285
    :cond_0
    return-void

    .line 280
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 347
    return-void
.end method
