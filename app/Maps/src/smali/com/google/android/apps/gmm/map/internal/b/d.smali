.class public Lcom/google/android/apps/gmm/map/internal/b/d;
.super Lcom/google/android/apps/gmm/map/internal/b/h;
.source "PG"


# instance fields
.field private final g:Ljava/util/LinkedHashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashSet",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/google/android/apps/gmm/map/b/a/bc;

.field private i:F

.field private j:I

.field private final k:Lcom/google/android/apps/gmm/map/b/a/ae;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/ce;Lcom/google/android/apps/gmm/map/internal/c/cw;)V
    .locals 3

    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/internal/b/h;-><init>(Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/map/internal/c/ce;Lcom/google/android/apps/gmm/map/internal/c/cw;)V

    .line 36
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/bc;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/b/a/bc;-><init>(Lcom/google/android/apps/gmm/map/b/a/ae;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/map/internal/b/d;->h:Lcom/google/android/apps/gmm/map/b/a/bc;

    .line 59
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/b/d;->i:F

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/b/d;->j:I

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/b/d;->k:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/f/o;Ljava/util/List;)J
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/f/o;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 97
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->clear()V

    .line 102
    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/google/android/apps/gmm/map/f/o;->t:J

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    .line 104
    :goto_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->f:Lcom/google/android/apps/gmm/map/internal/c/cw;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/f/o;->f:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v5}, Lcom/google/android/apps/gmm/map/b/a/y;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->a:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/map/internal/c/cw;->a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/ai;)Lcom/google/android/apps/gmm/map/internal/c/cv;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/map/internal/c/cv;->a(F)I

    move-result v3

    move v6, v3

    .line 106
    :goto_1
    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->e:J

    cmp-long v3, v8, v4

    if-nez v3, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->j:I

    if-ne v6, v3, :cond_2

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->c:J

    .line 141
    :goto_2
    return-wide v2

    .line 103
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->b:Lcom/google/android/apps/gmm/map/internal/c/ce;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/ce;->a()Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/cd;->equals(Ljava/lang/Object;)Z

    move-result v2

    goto :goto_0

    .line 104
    :cond_1
    float-to-int v3, v3

    move v6, v3

    goto :goto_1

    .line 110
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/map/f/o;->b()Lcom/google/android/apps/gmm/map/b/a/bb;

    move-result-object v7

    .line 111
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    const/4 v3, 0x1

    .line 112
    :goto_3
    if-eqz v3, :cond_6

    .line 116
    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->i:F

    .line 117
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->j:I

    if-ne v2, v6, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->h:Lcom/google/android/apps/gmm/map/b/a/bc;

    const/4 v3, 0x0

    .line 119
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v4, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->h:Lcom/google/android/apps/gmm/map/b/a/bc;

    const/4 v3, 0x2

    .line 120
    iget-object v4, v7, Lcom/google/android/apps/gmm/map/b/a/bb;->b:[Lcom/google/android/apps/gmm/map/b/a/y;

    aget-object v3, v4, v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/bc;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->c:J

    goto :goto_2

    .line 111
    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    .line 123
    :cond_4
    iget-object v2, v7, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->b:Lcom/google/android/apps/gmm/map/internal/c/ce;

    .line 124
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/ce;->a()Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->h:Lcom/google/android/apps/gmm/map/b/a/bc;

    .line 123
    move-object/from16 v0, p2

    invoke-static {v2, v6, v3, v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;ILcom/google/android/apps/gmm/map/internal/c/cd;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/bc;)V

    .line 125
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->i:F

    .line 137
    :goto_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/map/internal/b/d;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 138
    const/4 v2, 0x0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v3

    :goto_5
    if-ge v2, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_9

    const/4 v2, 0x0

    :goto_6
    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->c:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->c:J

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    invoke-virtual {v2}, Ljava/util/LinkedHashSet;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->g:Ljava/util/LinkedHashSet;

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    .line 139
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->e:J

    .line 140
    move-object/from16 v0, p0

    iput v6, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->j:I

    .line 141
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->c:J

    goto/16 :goto_2

    .line 127
    :cond_6
    iget-object v2, v7, Lcom/google/android/apps/gmm/map/b/a/bb;->d:Lcom/google/android/apps/gmm/map/b/a/bc;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->b:Lcom/google/android/apps/gmm/map/internal/c/ce;

    .line 128
    invoke-interface {v3}, Lcom/google/android/apps/gmm/map/internal/c/ce;->a()Lcom/google/android/apps/gmm/map/internal/c/cd;

    move-result-object v3

    const/4 v4, 0x0

    .line 127
    move-object/from16 v0, p2

    invoke-static {v2, v6, v3, v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(Lcom/google/android/apps/gmm/map/b/a/bc;ILcom/google/android/apps/gmm/map/internal/c/cd;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/bc;)V

    .line 129
    const/4 v4, 0x0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    const/4 v2, 0x0

    move v5, v2

    :goto_7
    if-ge v5, v10, :cond_7

    move-object/from16 v0, p2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/internal/c/bp;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->k:Lcom/google/android/apps/gmm/map/b/a/ae;

    const/high16 v11, 0x40000000    # 2.0f

    iget v12, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int/2addr v11, v12

    iget v12, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    iget v13, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    iget v14, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->e:I

    add-int/2addr v14, v11

    iget v15, v2, Lcom/google/android/apps/gmm/map/internal/c/bp;->f:I

    add-int/2addr v11, v15

    invoke-virtual {v3, v12, v13, v14, v11}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(IIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->k:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v7, v3}, Lcom/google/android/apps/gmm/map/b/a/bb;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v3

    if-eqz v3, :cond_b

    add-int/lit8 v3, v4, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v4, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move v2, v3

    :goto_8
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move v4, v2

    goto :goto_7

    :cond_7
    add-int/lit8 v2, v10, -0x1

    :goto_9
    if-lt v2, v4, :cond_8

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v2, v2, -0x1

    goto :goto_9

    .line 133
    :cond_8
    const/high16 v2, -0x40800000    # -1.0f

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/apps/gmm/map/internal/b/d;->i:F

    goto/16 :goto_4

    .line 138
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_5

    :cond_a
    const/4 v2, 0x1

    goto/16 :goto_6

    :cond_b
    move v2, v4

    goto :goto_8
.end method
