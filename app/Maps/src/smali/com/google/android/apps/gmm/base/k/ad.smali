.class public Lcom/google/android/apps/gmm/base/k/ad;
.super Lcom/google/android/apps/gmm/base/k/ab;
.source "PG"


# instance fields
.field private final d:Lcom/google/android/apps/gmm/base/k/ag;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/base/k/ag;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    sget-object v0, Lcom/google/android/libraries/curvular/f;->c:Lcom/google/android/libraries/curvular/f;

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/apps/gmm/base/k/ab;-><init>(Lcom/google/android/libraries/curvular/f;II)V

    .line 44
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/ad;->d:Lcom/google/android/apps/gmm/base/k/ag;

    .line 45
    return-void
.end method


# virtual methods
.method public final d_(Landroid/content/Context;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 50
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/ad;->d:Lcom/google/android/apps/gmm/base/k/ag;

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->p_()Lcom/google/android/apps/gmm/q/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/q/a;->a()Z

    move-result v0

    invoke-interface {v1, p1, v0}, Lcom/google/android/apps/gmm/base/k/ag;->a(Landroid/content/Context;Z)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 55
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/k/ad;

    if-eqz v0, :cond_0

    .line 56
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/k/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/base/k/ad;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/k/ad;->d:Lcom/google/android/apps/gmm/base/k/ag;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/ad;->d:Lcom/google/android/apps/gmm/base/k/ag;

    .line 57
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 62
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/apps/gmm/base/k/ab;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ad;->d:Lcom/google/android/apps/gmm/base/k/ag;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
