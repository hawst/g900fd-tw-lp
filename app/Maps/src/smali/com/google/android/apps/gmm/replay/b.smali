.class public Lcom/google/android/apps/gmm/replay/b;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field static final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<*>;>;"
        }
    .end annotation
.end field


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TT;>;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/util/replay/d;

.field d:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<TT;>;"
        }
    .end annotation
.end field

.field e:[Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/apps/gmm/replay/c;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    .line 52
    const-class v0, Ljava/lang/String;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    const-class v2, Ljava/lang/Boolean;

    sget-object v3, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    const-class v4, Ljava/lang/Character;

    sget-object v5, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    const/16 v6, 0xb

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Ljava/lang/Byte;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    sget-object v8, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-class v8, Ljava/lang/Short;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-class v8, Ljava/lang/Integer;

    aput-object v8, v6, v7

    const/4 v7, 0x5

    sget-object v8, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/4 v7, 0x6

    const-class v8, Ljava/lang/Long;

    aput-object v8, v6, v7

    const/4 v7, 0x7

    sget-object v8, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/16 v7, 0x8

    const-class v8, Ljava/lang/Float;

    aput-object v8, v6, v7

    const/16 v7, 0x9

    sget-object v8, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    aput-object v8, v6, v7

    const/16 v7, 0xa

    const-class v8, Ljava/lang/Double;

    aput-object v8, v6, v7

    invoke-static/range {v0 .. v6}, Lcom/google/b/c/dn;->a(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/replay/b;->f:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/gmm/util/replay/d;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;",
            "Lcom/google/android/apps/gmm/util/replay/d;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->h:Ljava/util/Map;

    .line 68
    iput-object p1, p0, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lcom/google/android/apps/gmm/replay/b;->b:Ljava/lang/Class;

    .line 70
    iput-object p3, p0, Lcom/google/android/apps/gmm/replay/b;->c:Lcom/google/android/apps/gmm/util/replay/d;

    .line 71
    iput-object p4, p0, Lcom/google/android/apps/gmm/replay/b;->g:Ljava/lang/String;

    .line 72
    return-void
.end method

.method public static a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/replay/b;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)",
            "Lcom/google/android/apps/gmm/replay/b",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 471
    :try_start_0
    const-class v1, Lcom/google/android/apps/gmm/util/replay/c;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/util/replay/c;
    :try_end_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_0

    .line 480
    if-nez v1, :cond_0

    .line 481
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Class %s needs @ReplayableEvent annotation"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 482
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 481
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 474
    :catch_0
    move-exception v1

    .line 477
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x20

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Failed to get annotation for <"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 485
    :cond_0
    invoke-interface {v1}, Lcom/google/android/apps/gmm/util/replay/c;->a()Ljava/lang/String;

    move-result-object v2

    .line 486
    invoke-interface {v1}, Lcom/google/android/apps/gmm/util/replay/c;->b()Lcom/google/android/apps/gmm/util/replay/d;

    move-result-object v3

    .line 487
    invoke-interface {v1}, Lcom/google/android/apps/gmm/util/replay/c;->c()Ljava/lang/String;

    move-result-object v1

    .line 488
    new-instance v6, Lcom/google/android/apps/gmm/replay/b;

    .line 489
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->isEnum()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_0
    move-object/from16 v0, p0

    invoke-direct {v6, v2, v0, v3, v1}, Lcom/google/android/apps/gmm/replay/b;-><init>(Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/apps/gmm/util/replay/d;Ljava/lang/String;)V

    .line 491
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v7

    array-length v8, v7

    const/4 v1, 0x0

    move v5, v1

    :goto_1
    if-ge v5, v8, :cond_a

    aget-object v9, v7, v5

    .line 496
    invoke-virtual {v9}, Ljava/lang/reflect/Constructor;->getParameterAnnotations()[[Ljava/lang/annotation/Annotation;

    move-result-object v10

    .line 498
    array-length v1, v10

    if-nez v1, :cond_3

    .line 499
    iget-object v1, v6, Lcom/google/android/apps/gmm/replay/b;->d:Ljava/lang/reflect/Constructor;

    if-nez v1, :cond_1

    .line 500
    const-string v1, "Setting no argument constructor for %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 501
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 500
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 502
    iput-object v9, v6, Lcom/google/android/apps/gmm/replay/b;->d:Ljava/lang/reflect/Constructor;

    .line 491
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 489
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 508
    :cond_3
    array-length v1, v10

    new-array v11, v1, [Lcom/google/android/apps/gmm/util/replay/g;

    .line 509
    array-length v1, v10

    new-array v12, v1, [Z

    .line 510
    const/4 v2, 0x0

    .line 511
    const/4 v1, 0x0

    move/from16 v16, v1

    move v1, v2

    move/from16 v2, v16

    :goto_2
    array-length v3, v10

    if-ge v2, v3, :cond_7

    .line 512
    aget-object v13, v10, v2

    array-length v14, v13

    const/4 v3, 0x0

    move v4, v3

    move v3, v1

    :goto_3
    if-ge v4, v14, :cond_6

    aget-object v1, v13, v4

    .line 513
    instance-of v15, v1, Lcom/google/android/apps/gmm/util/replay/g;

    if-eqz v15, :cond_5

    .line 514
    check-cast v1, Lcom/google/android/apps/gmm/util/replay/g;

    aput-object v1, v11, v2

    .line 515
    add-int/lit8 v3, v3, 0x1

    .line 512
    :cond_4
    :goto_4
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    .line 516
    :cond_5
    instance-of v1, v1, Lb/a/a;

    if-eqz v1, :cond_4

    .line 517
    const/4 v1, 0x1

    aput-boolean v1, v12, v2

    goto :goto_4

    .line 511
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_2

    .line 521
    :cond_7
    if-lez v1, :cond_1

    .line 522
    array-length v2, v10

    if-eq v1, v2, :cond_8

    .line 523
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Not all parameters for %s constructor are annotated with @ReplayableProperty"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 525
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 523
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 527
    :cond_8
    iget-object v2, v6, Lcom/google/android/apps/gmm/replay/b;->e:[Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 528
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "%s has multiple constructors with parameters annotated with @ReplayableProperty"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 530
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 528
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 532
    :cond_9
    const-string v2, "Setting constructor for %s."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 533
    iput-object v9, v6, Lcom/google/android/apps/gmm/replay/b;->d:Ljava/lang/reflect/Constructor;

    .line 534
    new-array v2, v1, [Ljava/lang/String;

    iput-object v2, v6, Lcom/google/android/apps/gmm/replay/b;->e:[Ljava/lang/String;

    .line 535
    invoke-virtual {v9}, Ljava/lang/reflect/Constructor;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v3

    .line 536
    const/4 v2, 0x0

    :goto_5
    if-ge v2, v1, :cond_1

    .line 537
    aget-object v4, v11, v2

    invoke-interface {v4}, Lcom/google/android/apps/gmm/util/replay/g;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v6, v4}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/replay/c;

    move-result-object v4

    .line 538
    aget-object v9, v3, v2

    invoke-virtual {v4, v9}, Lcom/google/android/apps/gmm/replay/c;->a(Ljava/lang/Class;)V

    .line 539
    const/4 v9, 0x1

    iput-boolean v9, v4, Lcom/google/android/apps/gmm/replay/c;->g:Z

    .line 540
    aget-boolean v9, v12, v2

    iput-boolean v9, v4, Lcom/google/android/apps/gmm/replay/c;->h:Z

    .line 541
    iget-object v9, v6, Lcom/google/android/apps/gmm/replay/b;->e:[Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/apps/gmm/replay/c;->a:Ljava/lang/String;

    aput-object v4, v9, v2

    .line 536
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 545
    :cond_a
    iget-object v1, v6, Lcom/google/android/apps/gmm/replay/b;->d:Ljava/lang/reflect/Constructor;

    if-nez v1, :cond_b

    iget-object v1, v6, Lcom/google/android/apps/gmm/replay/b;->g:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 546
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "%s has no usable constructor"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 547
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 546
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 550
    :cond_b
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_6
    if-ge v2, v4, :cond_e

    aget-object v5, v3, v2

    .line 551
    const-class v1, Lcom/google/android/apps/gmm/util/replay/g;

    invoke-virtual {v5, v1}, Ljava/lang/reflect/Field;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/util/replay/g;

    .line 552
    if-eqz v1, :cond_d

    .line 553
    invoke-interface {v1}, Lcom/google/android/apps/gmm/util/replay/g;->a()Ljava/lang/String;

    move-result-object v1

    .line 554
    invoke-virtual {v5}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v7

    .line 555
    invoke-direct {v6, v1}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/replay/c;

    move-result-object v8

    .line 556
    iget-object v9, v8, Lcom/google/android/apps/gmm/replay/c;->c:Ljava/lang/reflect/Field;

    if-eqz v9, :cond_c

    .line 557
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Event class %s has duplicate @ReplayProperty(\"%s\") annotations"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 559
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    .line 557
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 561
    :cond_c
    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/replay/c;->a(Ljava/lang/Class;)V

    .line 562
    iput-object v5, v8, Lcom/google/android/apps/gmm/replay/c;->c:Ljava/lang/reflect/Field;

    .line 550
    :cond_d
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_6

    .line 566
    :cond_e
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getMethods()[Ljava/lang/reflect/Method;

    move-result-object v3

    array-length v4, v3

    const/4 v1, 0x0

    move v2, v1

    :goto_7
    if-ge v2, v4, :cond_1a

    aget-object v5, v3, v2

    .line 567
    const-class v1, Lcom/google/android/apps/gmm/util/replay/h;

    invoke-virtual {v5, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/util/replay/h;

    if-nez v1, :cond_10

    const/4 v1, 0x0

    :goto_8
    if-nez v1, :cond_f

    const-class v1, Lcom/google/android/apps/gmm/util/replay/e;

    invoke-virtual {v5, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/util/replay/e;

    if-nez v1, :cond_14

    const/4 v1, 0x0

    :goto_9
    if-nez v1, :cond_f

    .line 568
    const-class v1, Lcom/google/android/apps/gmm/util/replay/f;

    invoke-virtual {v5, v1}, Ljava/lang/reflect/Method;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/util/replay/f;

    if-nez v1, :cond_17

    .line 566
    :cond_f
    :goto_a
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_7

    .line 567
    :cond_10
    invoke-interface {v1}, Lcom/google/android/apps/gmm/util/replay/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getParameterTypes()[Ljava/lang/Class;

    move-result-object v7

    array-length v8, v7

    const/4 v9, 0x1

    if-eq v8, v9, :cond_11

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Setter %s of %s must take one parameter."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    invoke-direct {v6, v1}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/replay/c;

    move-result-object v8

    iget-object v9, v8, Lcom/google/android/apps/gmm/replay/c;->c:Ljava/lang/reflect/Field;

    if-nez v9, :cond_12

    iget-object v9, v8, Lcom/google/android/apps/gmm/replay/c;->d:Ljava/lang/reflect/Method;

    if-nez v9, :cond_12

    iget-boolean v9, v8, Lcom/google/android/apps/gmm/replay/c;->g:Z

    if-eqz v9, :cond_13

    :cond_12
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Event class %s has redundant @ReplaySetter(\"%s\") annotation"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v6, Lcom/google/android/apps/gmm/replay/b;->b:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_13
    const/4 v1, 0x0

    aget-object v1, v7, v1

    invoke-virtual {v8, v1}, Lcom/google/android/apps/gmm/replay/c;->a(Ljava/lang/Class;)V

    iput-object v5, v8, Lcom/google/android/apps/gmm/replay/c;->d:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    goto :goto_8

    :cond_14
    invoke-interface {v1}, Lcom/google/android/apps/gmm/util/replay/e;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v7

    invoke-direct {v6, v1}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/replay/c;

    move-result-object v8

    iget-object v9, v8, Lcom/google/android/apps/gmm/replay/c;->e:Ljava/lang/reflect/Method;

    if-nez v9, :cond_15

    iget-object v9, v8, Lcom/google/android/apps/gmm/replay/c;->c:Ljava/lang/reflect/Field;

    if-eqz v9, :cond_16

    :cond_15
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Event class %s has redundant @ReplayGetter(\"%s\") annotation"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v6, Lcom/google/android/apps/gmm/replay/b;->b:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_16
    invoke-virtual {v8, v7}, Lcom/google/android/apps/gmm/replay/c;->a(Ljava/lang/Class;)V

    iput-object v5, v8, Lcom/google/android/apps/gmm/replay/c;->e:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    goto/16 :goto_9

    .line 568
    :cond_17
    invoke-interface {v1}, Lcom/google/android/apps/gmm/util/replay/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v6, v1}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/replay/c;

    move-result-object v7

    iget-object v8, v7, Lcom/google/android/apps/gmm/replay/c;->f:Ljava/lang/reflect/Method;

    if-eqz v8, :cond_18

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Event class %s has duplicate @ReplayHasProperty(\"%s\") annotations"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v6, Lcom/google/android/apps/gmm/replay/b;->b:Ljava/lang/Class;

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_18
    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getReturnType()Ljava/lang/Class;

    move-result-object v8

    sget-object v9, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq v8, v9, :cond_19

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "%s of %s must return a boolean"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v5}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_19
    iput-object v5, v7, Lcom/google/android/apps/gmm/replay/c;->f:Ljava/lang/reflect/Method;

    goto/16 :goto_a

    .line 572
    :cond_1a
    iget-object v1, v6, Lcom/google/android/apps/gmm/replay/b;->h:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/replay/c;

    .line 573
    iget-object v3, v1, Lcom/google/android/apps/gmm/replay/c;->c:Ljava/lang/reflect/Field;

    if-nez v3, :cond_1d

    .line 574
    iget-object v3, v1, Lcom/google/android/apps/gmm/replay/c;->d:Ljava/lang/reflect/Method;

    if-nez v3, :cond_1c

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/replay/c;->g:Z

    if-nez v3, :cond_1c

    .line 575
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Must define means to set attribute %s for event class %s "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/replay/c;->a:Ljava/lang/String;

    aput-object v1, v4, v5

    const/4 v1, 0x1

    .line 577
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 575
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 579
    :cond_1c
    iget-object v3, v1, Lcom/google/android/apps/gmm/replay/c;->e:Ljava/lang/reflect/Method;

    if-nez v3, :cond_1d

    .line 580
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Must define means to get attribute %s for event class %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/replay/c;->a:Ljava/lang/String;

    aput-object v1, v4, v5

    const/4 v1, 0x1

    .line 582
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 580
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 585
    :cond_1d
    iget-boolean v3, v1, Lcom/google/android/apps/gmm/replay/c;->g:Z

    if-eqz v3, :cond_1b

    iget-boolean v3, v1, Lcom/google/android/apps/gmm/replay/c;->h:Z

    if-nez v3, :cond_1b

    iget-object v3, v1, Lcom/google/android/apps/gmm/replay/c;->f:Ljava/lang/reflect/Method;

    if-eqz v3, :cond_1b

    .line 586
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Attribute %s in class %s can\'t have @ReplayHasProperty method if it is required in constructor."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v1, v1, Lcom/google/android/apps/gmm/replay/c;->a:Ljava/lang/String;

    aput-object v1, v4, v5

    const/4 v1, 0x1

    .line 588
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    .line 586
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 592
    :cond_1e
    return-object v6
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/apps/gmm/replay/c;
    .locals 2

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/replay/c;

    .line 372
    if-nez v0, :cond_0

    .line 373
    new-instance v0, Lcom/google/android/apps/gmm/replay/c;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/replay/c;-><init>(Ljava/lang/String;)V

    .line 374
    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/b;->h:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 376
    :cond_0
    return-object v0
.end method

.method static a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 105
    const-class v0, Ljava/lang/String;

    if-ne p1, v0, :cond_0

    .line 127
    :goto_0
    return-object p0

    .line 107
    :cond_0
    sget-object v0, Ljava/lang/Character;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_1

    const-class v0, Ljava/lang/Character;

    if-ne p1, v0, :cond_3

    .line 108
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 109
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Illegal character value [%s]"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 111
    :cond_2
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object p0

    goto :goto_0

    .line 112
    :cond_3
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_4

    const-class v0, Ljava/lang/Boolean;

    if-ne p1, v0, :cond_5

    .line 113
    :cond_4
    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p0

    goto :goto_0

    .line 114
    :cond_5
    sget-object v0, Ljava/lang/Byte;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_6

    const-class v0, Ljava/lang/Byte;

    if-ne p1, v0, :cond_7

    .line 115
    :cond_6
    invoke-static {p0}, Ljava/lang/Byte;->decode(Ljava/lang/String;)Ljava/lang/Byte;

    move-result-object p0

    goto :goto_0

    .line 116
    :cond_7
    sget-object v0, Ljava/lang/Short;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_8

    const-class v0, Ljava/lang/Short;

    if-ne p1, v0, :cond_9

    .line 117
    :cond_8
    invoke-static {p0}, Ljava/lang/Short;->decode(Ljava/lang/String;)Ljava/lang/Short;

    move-result-object p0

    goto :goto_0

    .line 118
    :cond_9
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_a

    const-class v0, Ljava/lang/Integer;

    if-ne p1, v0, :cond_b

    .line 119
    :cond_a
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object p0

    goto :goto_0

    .line 120
    :cond_b
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_c

    const-class v0, Ljava/lang/Long;

    if-ne p1, v0, :cond_d

    .line 121
    :cond_c
    invoke-static {p0}, Ljava/lang/Long;->decode(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object p0

    goto :goto_0

    .line 122
    :cond_d
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_e

    const-class v0, Ljava/lang/Float;

    if-ne p1, v0, :cond_f

    .line 123
    :cond_e
    invoke-static {p0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object p0

    goto :goto_0

    .line 124
    :cond_f
    sget-object v0, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    if-eq p1, v0, :cond_10

    const-class v0, Ljava/lang/Double;

    if-ne p1, v0, :cond_11

    .line 125
    :cond_10
    invoke-static {p0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object p0

    goto/16 :goto_0

    .line 126
    :cond_11
    invoke-virtual {p1}, Ljava/lang/Class;->isEnum()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 127
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/replay/b;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    goto/16 :goto_0

    .line 130
    :cond_12
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Invalid attribute type [%s]"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 142
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "-"

    const-string v3, "_"

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    .line 143
    invoke-virtual {p1}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v3

    array-length v4, v3

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    .line 144
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 145
    return-object v5

    .line 143
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v3, "Invalid value [%s]"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static c(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 162
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/replay/b;->b(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/Map;)Ljava/lang/Object;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)TT;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->g:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 271
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->g:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 272
    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Map;->size()I

    move-result v2

    if-eq v2, v5, :cond_1

    .line 273
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Invalid attributes for %s: %s"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    aput-object v4, v3, v1

    aput-object p1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 276
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/b;->b:Ljava/lang/Class;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/replay/b;->c(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    .line 316
    :cond_2
    return-object v2

    .line 278
    :cond_3
    new-instance v3, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->d:Ljava/lang/reflect/Constructor;

    if-nez v0, :cond_4

    .line 281
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "No constructor found for %s(%s)"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    aput-object v4, v3, v1

    aput-object p1, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 285
    :cond_4
    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->e:[Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 286
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->e:[Ljava/lang/String;

    array-length v0, v0

    new-array v4, v0, [Ljava/lang/Object;

    move v2, v1

    .line 287
    :goto_0
    array-length v0, v4

    if-ge v2, v0, :cond_7

    .line 288
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->h:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/gmm/replay/b;->e:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/replay/c;

    .line 289
    iget-object v1, v0, Lcom/google/android/apps/gmm/replay/c;->a:Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 290
    if-nez v1, :cond_5

    .line 291
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/replay/c;->h:Z

    if-nez v0, :cond_6

    .line 292
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Missing %s attribute in %s tag."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/gmm/replay/b;->e:[Ljava/lang/String;

    aget-object v2, v5, v2

    aput-object v2, v3, v4

    const/4 v2, 0x1

    iget-object v4, p0, Lcom/google/android/apps/gmm/replay/b;->a:Ljava/lang/String;

    aput-object v4, v3, v2

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 304
    :catch_0
    move-exception v0

    .line 305
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 296
    :cond_5
    :try_start_1
    iget-object v5, v0, Lcom/google/android/apps/gmm/replay/c;->b:Ljava/lang/Class;

    invoke-static {v1, v5}, Lcom/google/android/apps/gmm/replay/b;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    aput-object v1, v4, v2

    .line 297
    iget-object v0, v0, Lcom/google/android/apps/gmm/replay/c;->a:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 287
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 300
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->d:Ljava/lang/reflect/Constructor;

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    move-object v2, v0

    .line 313
    :goto_1
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 314
    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/replay/b;->h:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/replay/c;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/apps/gmm/replay/c;->a(Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_2

    .line 302
    :cond_8
    :try_start_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/b;->d:Ljava/lang/reflect/Constructor;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    move-object v2, v0

    .line 310
    goto :goto_1

    .line 306
    :catch_1
    move-exception v0

    .line 307
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 308
    :catch_2
    move-exception v0

    .line 309
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
