.class abstract Lcom/google/android/apps/gmm/suggest/o;
.super Lcom/google/android/apps/gmm/shared/net/af;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/suggest/m;


# instance fields
.field final a:Lcom/google/android/apps/gmm/suggest/e/a;

.field final b:Lcom/google/android/apps/gmm/suggest/d/g;

.field final c:Lcom/google/android/apps/gmm/shared/c/f;

.field final d:Lcom/google/android/apps/gmm/map/r/b/a;

.field final e:Lcom/google/android/apps/gmm/map/b/a/r;

.field public f:Lcom/google/b/c/cv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;"
        }
    .end annotation
.end field

.field public h:I

.field private final i:Lcom/google/android/apps/gmm/suggest/e/c;

.field private final j:Lcom/google/android/apps/gmm/suggest/n;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;Lcom/google/android/apps/gmm/suggest/e/c;Lcom/google/android/apps/gmm/suggest/e/a;Lcom/google/android/apps/gmm/suggest/n;Lcom/google/android/apps/gmm/suggest/d/g;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/map/r/b/a;Lcom/google/android/apps/gmm/map/b/a/r;)V
    .locals 1
    .param p8    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p9    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/shared/net/af;-><init>(Lcom/google/r/b/a/el;Lcom/google/e/a/a/a/d;)V

    .line 52
    invoke-static {}, Lcom/google/b/c/cv;->g()Lcom/google/b/c/cv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->f:Lcom/google/b/c/cv;

    .line 53
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/suggest/o;->h:I

    .line 61
    if-nez p3, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p3, Lcom/google/android/apps/gmm/suggest/e/c;

    iput-object p3, p0, Lcom/google/android/apps/gmm/suggest/o;->i:Lcom/google/android/apps/gmm/suggest/e/c;

    .line 62
    if-nez p4, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p4, Lcom/google/android/apps/gmm/suggest/e/a;

    iput-object p4, p0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    .line 63
    iput-object p5, p0, Lcom/google/android/apps/gmm/suggest/o;->j:Lcom/google/android/apps/gmm/suggest/n;

    .line 64
    if-nez p6, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast p6, Lcom/google/android/apps/gmm/suggest/d/g;

    iput-object p6, p0, Lcom/google/android/apps/gmm/suggest/o;->b:Lcom/google/android/apps/gmm/suggest/d/g;

    .line 65
    if-nez p7, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_3
    check-cast p7, Lcom/google/android/apps/gmm/shared/c/f;

    iput-object p7, p0, Lcom/google/android/apps/gmm/suggest/o;->c:Lcom/google/android/apps/gmm/shared/c/f;

    .line 66
    iput-object p8, p0, Lcom/google/android/apps/gmm/suggest/o;->d:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 67
    iput-object p9, p0, Lcom/google/android/apps/gmm/suggest/o;->e:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 68
    return-void
.end method


# virtual methods
.method protected final a(Ljava/io/DataOutput;)V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->b:Lcom/google/android/apps/gmm/suggest/d/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/o;->c:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/suggest/d/g;->a(J)V

    .line 148
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/shared/net/af;->a(Ljava/io/DataOutput;)V

    .line 149
    return-void
.end method

.method protected final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 114
    const/4 v0, 0x0

    return v0
.end method

.method public final aj_()Lcom/google/android/apps/gmm/suggest/d/g;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->b:Lcom/google/android/apps/gmm/suggest/d/g;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/suggest/e/c;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->i:Lcom/google/android/apps/gmm/suggest/e/c;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->a:Lcom/google/android/apps/gmm/suggest/e/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/suggest/e/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final declared-synchronized d()Lcom/google/b/c/cv;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/b/c/cv",
            "<",
            "Lcom/google/android/apps/gmm/suggest/e/d;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->f:Lcom/google/b/c/cv;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()I
    .locals 1

    .prologue
    .line 109
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/suggest/o;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onComplete(Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 1
    .param p1    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->j:Lcom/google/android/apps/gmm/suggest/n;

    if-eqz v0, :cond_0

    .line 137
    if-nez p1, :cond_1

    .line 138
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->j:Lcom/google/android/apps/gmm/suggest/n;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/suggest/n;->a(Lcom/google/android/apps/gmm/suggest/m;)V

    .line 143
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/o;->j:Lcom/google/android/apps/gmm/suggest/n;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/suggest/n;->b(Lcom/google/android/apps/gmm/suggest/m;)V

    goto :goto_0
.end method
