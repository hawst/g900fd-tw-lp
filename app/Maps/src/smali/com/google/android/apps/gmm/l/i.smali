.class public Lcom/google/android/apps/gmm/l/i;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/activities/c;

.field private final c:Lcom/google/android/apps/gmm/l/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/google/android/apps/gmm/l/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 55
    new-instance v1, Lcom/google/android/apps/gmm/l/c;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/l/c;-><init>(Lcom/google/android/apps/gmm/base/a;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/l/i;->c:Lcom/google/android/apps/gmm/l/c;

    .line 56
    return-void
.end method

.method public static a(Landroid/content/Intent;)Ljava/lang/String;
    .locals 3
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 66
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 67
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getDataString()Ljava/lang/String;

    move-result-object v0

    .line 74
    :goto_0
    return-object v0

    .line 68
    :cond_1
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 70
    :goto_1
    if-eqz v0, :cond_3

    .line 71
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 69
    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 74
    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 81
    const-string v0, "noconfirm"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private d(Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 154
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 155
    if-nez v1, :cond_1

    .line 224
    :cond_0
    :goto_0
    return-void

    .line 159
    :cond_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x14

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Parsing Intent URI: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 161
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/i;->c:Lcom/google/android/apps/gmm/l/c;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/l/c;->b(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    .line 162
    const-string v3, "Parsed intent action: "

    if-nez v2, :cond_2

    const-string v0, "null"

    .line 163
    :goto_1
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 162
    :goto_2
    if-eqz v2, :cond_4

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    sget-object v3, Lcom/google/android/apps/gmm/l/y;->l:Lcom/google/android/apps/gmm/l/y;

    if-ne v0, v3, :cond_4

    .line 166
    sget-object v0, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0xc

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "action type="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v0

    new-instance v3, Lcom/google/android/apps/gmm/l/k;

    invoke-direct {v3, p0, v1, v2}, Lcom/google/android/apps/gmm/l/k;-><init>(Lcom/google/android/apps/gmm/l/i;Landroid/net/Uri;Lcom/google/android/apps/gmm/l/u;)V

    sget-object v1, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v0, v3, v1}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    goto :goto_0

    .line 163
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/l/u;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 183
    :cond_4
    if-nez v2, :cond_6

    .line 184
    invoke-static {v1}, Lcom/google/android/apps/gmm/l/ao;->a(Landroid/net/Uri;)Lcom/google/android/apps/gmm/l/ap;

    move-result-object v0

    .line 185
    :goto_3
    if-nez v2, :cond_5

    if-eqz v0, :cond_0

    .line 190
    :cond_5
    sget-object v3, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    .line 191
    new-instance v3, Lcom/google/android/apps/gmm/l/l;

    invoke-direct {v3, p0, v2, v1, v0}, Lcom/google/android/apps/gmm/l/l;-><init>(Lcom/google/android/apps/gmm/l/i;Lcom/google/android/apps/gmm/l/u;Landroid/net/Uri;Lcom/google/android/apps/gmm/l/ap;)V

    .line 211
    if-eqz v2, :cond_7

    iget-object v0, v2, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    sget-object v1, Lcom/google/android/apps/gmm/l/y;->h:Lcom/google/android/apps/gmm/l/y;

    if-ne v0, v1, :cond_7

    .line 213
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    .line 222
    :goto_4
    sget-object v0, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    goto/16 :goto_0

    .line 184
    :cond_6
    const/4 v0, 0x0

    goto :goto_3

    .line 214
    :cond_7
    const-string v0, "noconfirm"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 215
    sget-object v0, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    .line 216
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a()V

    .line 217
    invoke-interface {v3}, Ljava/lang/Runnable;->run()V

    goto :goto_4

    .line 219
    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Runnable;)V

    goto :goto_4
.end method


# virtual methods
.method final a(Ljava/lang/String;Lcom/google/android/apps/gmm/l/u;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 290
    if-nez p2, :cond_0

    sget-object v2, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    .line 293
    :goto_0
    if-eqz p2, :cond_4

    iget-object v0, p2, Lcom/google/android/apps/gmm/l/u;->f:Lcom/google/android/apps/gmm/l/ae;

    if-eqz v0, :cond_4

    .line 294
    iget-object v0, p2, Lcom/google/android/apps/gmm/l/u;->f:Lcom/google/android/apps/gmm/l/ae;

    move-object v1, v0

    .line 297
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    sget-object v3, Lcom/google/r/b/a/av;->u:Lcom/google/r/b/a/av;

    if-nez v1, :cond_3

    :goto_2
    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/apps/gmm/z/a/b;->a(Ljava/lang/String;Lcom/google/r/b/a/gb;Lcom/google/r/b/a/av;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 291
    :cond_0
    sget-object v0, Lcom/google/android/apps/gmm/l/x;->a:[I

    iget-object v1, p2, Lcom/google/android/apps/gmm/l/u;->a:Lcom/google/android/apps/gmm/l/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/l/y;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    sget-object v2, Lcom/google/r/b/a/gb;->a:Lcom/google/r/b/a/gb;

    goto :goto_0

    :pswitch_0
    iget-object v0, p2, Lcom/google/android/apps/gmm/l/u;->d:Lcom/google/android/apps/gmm/l/aa;

    sget-object v1, Lcom/google/android/apps/gmm/l/aa;->c:Lcom/google/android/apps/gmm/l/aa;

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_2

    sget-object v2, Lcom/google/r/b/a/gb;->f:Lcom/google/r/b/a/gb;

    goto :goto_0

    :cond_1
    move v0, v5

    goto :goto_3

    :cond_2
    sget-object v2, Lcom/google/r/b/a/gb;->e:Lcom/google/r/b/a/gb;

    goto :goto_0

    :pswitch_1
    sget-object v2, Lcom/google/r/b/a/gb;->f:Lcom/google/r/b/a/gb;

    goto :goto_0

    :pswitch_2
    sget-object v2, Lcom/google/r/b/a/gb;->b:Lcom/google/r/b/a/gb;

    goto :goto_0

    :pswitch_3
    sget-object v2, Lcom/google/r/b/a/gb;->c:Lcom/google/r/b/a/gb;

    goto :goto_0

    :pswitch_4
    sget-object v2, Lcom/google/r/b/a/gb;->d:Lcom/google/r/b/a/gb;

    goto :goto_0

    .line 299
    :cond_3
    iget-object v4, v1, Lcom/google/android/apps/gmm/l/ae;->g:Ljava/lang/String;

    goto :goto_2

    :cond_4
    move-object v1, v4

    goto :goto_1

    .line 291
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final c(Landroid/content/Intent;)Lcom/google/android/apps/gmm/l/u;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    .line 115
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 117
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 118
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 119
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/i;->d(Landroid/content/Intent;)V

    .line 120
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    .line 144
    :goto_0
    return-object v0

    .line 121
    :cond_0
    const-string v2, "android.intent.action.MANAGE_NETWORK_USAGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 122
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/l/j;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/l/j;-><init>(Lcom/google/android/apps/gmm/l/i;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Runnable;)V

    .line 130
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto :goto_0

    .line 131
    :cond_1
    const-string v2, "android.intent.action.SEND"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->a()Lcom/google/android/apps/gmm/shared/net/a/h;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/shared/net/a/h;->e:Z

    if-nez v0, :cond_3

    .line 135
    :cond_2
    :goto_1
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto :goto_0

    .line 132
    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    const-string v2, "android.intent.extra.STREAM"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    move-object v2, v0

    :goto_2
    if-eqz v2, :cond_2

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "latitude"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "longitude"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "latitude"

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v4

    const-string v3, "longitude"

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v6

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_d

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-direct {v0, v4, v5, v6, v7}, Lcom/google/android/apps/gmm/map/b/a/q;-><init>(DD)V

    :goto_3
    iget-object v1, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->C()Lcom/google/android/apps/gmm/photo/a/a;

    move-result-object v1

    invoke-interface {v1, v2, v0}, Lcom/google/android/apps/gmm/photo/a/a;->a(Landroid/net/Uri;Lcom/google/android/apps/gmm/map/b/a/q;)Z

    goto :goto_1

    :cond_4
    move-object v2, v1

    goto :goto_2

    .line 136
    :cond_5
    const-string v1, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 138
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/l/i;->d(Landroid/content/Intent;)V

    .line 139
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto/16 :goto_0

    .line 140
    :cond_6
    const-string v1, "com.google.android.gms.actions.SEARCH_ACTION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 141
    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_9

    :cond_7
    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/apps/gmm/l/z;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/l/z;-><init>()V

    sget-object v2, Lcom/google/android/apps/gmm/l/y;->c:Lcom/google/android/apps/gmm/l/y;

    iput-object v2, v0, Lcom/google/android/apps/gmm/l/z;->a:Lcom/google/android/apps/gmm/l/y;

    iput-object v1, v0, Lcom/google/android/apps/gmm/l/z;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/l/z;->a()Lcom/google/android/apps/gmm/l/u;

    move-result-object v2

    const-string v3, "Parsed app search intent: "

    if-nez v2, :cond_a

    const-string v0, "null"

    :goto_5
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_b

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_6
    new-instance v0, Lcom/google/android/apps/gmm/l/m;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/apps/gmm/l/m;-><init>(Lcom/google/android/apps/gmm/l/i;Ljava/lang/String;Lcom/google/android/apps/gmm/l/u;)V

    sget-object v1, Lcom/google/android/apps/gmm/l/i;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/apps/gmm/l/i;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Runnable;)V

    .line 142
    :cond_8
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto/16 :goto_0

    .line 141
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/l/u;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_b
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_6

    .line 144
    :cond_c
    sget-object v0, Lcom/google/android/apps/gmm/l/u;->i:Lcom/google/android/apps/gmm/l/u;

    goto/16 :goto_0

    :cond_d
    move-object v0, v1

    goto :goto_3
.end method
