.class public Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;
.super Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;
.source "PG"


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field private d:Lcom/google/android/apps/gmm/startpage/m;

.field private e:Lcom/google/android/apps/gmm/startpage/d/d;

.field private f:Lcom/google/o/h/a/mr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/d/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 51
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/o/h/a/mr;)Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;-><init>()V

    .line 56
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 57
    const-string v2, "odelay_list_fragment_odelay_state"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 58
    const-string v2, "odelay_list_fragment_omnibox_style"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 59
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    return-object v0
.end method


# virtual methods
.method protected final a()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/b/f/t;->er:Lcom/google/b/f/t;

    return-object v0
.end method

.method protected final c()Lcom/google/o/h/a/mr;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->f:Lcom/google/o/h/a/mr;

    if-nez v0, :cond_0

    .line 39
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;->c()Lcom/google/o/h/a/mr;

    move-result-object v0

    .line 41
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->f:Lcom/google/o/h/a/mr;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-nez v2, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->c:Ljava/lang/String;

    .line 69
    :cond_0
    return-void

    .line 66
    :cond_1
    const-string v0, "odelay_list_fragment_odelay_state"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v3, v0, Lcom/google/android/apps/gmm/startpage/d/d;

    if-nez v3, :cond_2

    sget-object v2, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Illegal odelay state is loaded: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    check-cast v0, Lcom/google/android/apps/gmm/startpage/d/d;

    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/startpage/d/d;->a(Lcom/google/android/apps/gmm/startpage/d/d;)V

    const-string v0, "odelay_list_fragment_omnibox_style"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    instance-of v2, v0, Lcom/google/o/h/a/mr;

    if-nez v2, :cond_3

    sget-object v2, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Illegal omnibox style is loaded: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v2, v0, v3}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast v0, Lcom/google/o/h/a/mr;

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->f:Lcom/google/o/h/a/mr;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 101
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;->onPause()V

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->d()V

    .line 103
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 92
    invoke-super {p0}, Lcom/google/android/apps/gmm/cardui/CardUiLoadingFragment;->onResume()V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->e:Lcom/google/android/apps/gmm/startpage/d/d;

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 94
    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/startpage/m;->a(Lcom/google/android/apps/gmm/startpage/d/d;Lcom/google/android/apps/gmm/base/activities/c;)Lcom/google/android/apps/gmm/startpage/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/OdelayLoadingFragment;->d:Lcom/google/android/apps/gmm/startpage/m;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/startpage/m;->b()V

    .line 97
    return-void
.end method
