.class Lcom/google/android/apps/gmm/base/k/ae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/k/ag;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ai;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ak;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ai;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/k/ak;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    check-cast p1, Ljava/util/List;

    iput-object p1, p0, Lcom/google/android/apps/gmm/base/k/ae;->a:Ljava/util/List;

    .line 113
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    check-cast p2, Ljava/util/List;

    iput-object p2, p0, Lcom/google/android/apps/gmm/base/k/ae;->b:Ljava/util/List;

    .line 114
    return-void
.end method


# virtual methods
.method public final synthetic a(Landroid/content/Context;Z)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    .line 106
    new-instance v1, Landroid/graphics/drawable/AnimatedStateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/AnimatedStateListDrawable;-><init>()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/ae;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/k/ai;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/k/ai;->b:Z

    if-ne v3, p2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, v0, Lcom/google/android/apps/gmm/base/k/ai;->c:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    if-eqz v4, :cond_1

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/k/ai;->d:Lcom/google/android/libraries/curvular/aq;

    invoke-interface {v4, p1}, Lcom/google/android/libraries/curvular/aq;->b(Landroid/content/Context;)I

    move-result v4

    invoke-static {v4}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setTintList(Landroid/content/res/ColorStateList;)V

    iget-object v4, v0, Lcom/google/android/apps/gmm/base/k/ai;->e:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/Drawable;->setTintMode(Landroid/graphics/PorterDuff$Mode;)V

    :cond_1
    iget-object v4, v0, Lcom/google/android/apps/gmm/base/k/ai;->a:[I

    iget v0, v0, Lcom/google/android/apps/gmm/base/k/ai;->g:I

    invoke-virtual {v1, v4, v3, v0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/k/ae;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/k/ak;

    iget-boolean v3, v0, Lcom/google/android/apps/gmm/base/k/ak;->e:Z

    if-ne v3, p2, :cond_3

    iget v3, v0, Lcom/google/android/apps/gmm/base/k/ak;->a:I

    iget v4, v0, Lcom/google/android/apps/gmm/base/k/ak;->b:I

    iget-object v5, v0, Lcom/google/android/apps/gmm/base/k/ak;->c:Lcom/google/android/apps/gmm/base/k/a;

    invoke-virtual {v5, p1}, Lcom/google/android/apps/gmm/base/k/a;->a(Landroid/content/Context;)Landroid/graphics/drawable/AnimationDrawable;

    move-result-object v5

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/base/k/ak;->f:Z

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/graphics/drawable/AnimatedStateListDrawable;->addTransition(IILandroid/graphics/drawable/Drawable;Z)V

    goto :goto_1

    :cond_4
    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 155
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/k/ae;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/google/android/apps/gmm/base/k/ae;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/k/ae;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/ae;->a:Ljava/util/List;

    .line 156
    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/android/apps/gmm/base/k/ae;

    iget-object v0, p1, Lcom/google/android/apps/gmm/base/k/ae;->b:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/ae;->b:Ljava/util/List;

    .line 157
    invoke-interface {v0, v1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 162
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ae;->a:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ae;->b:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
