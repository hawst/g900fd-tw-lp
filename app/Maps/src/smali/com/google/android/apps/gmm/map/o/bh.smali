.class public Lcom/google/android/apps/gmm/map/o/bh;
.super Lcom/google/android/apps/gmm/map/t/an;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 501
    const-string v0, "uniform mat4 uMVPMatrix;\nuniform mat3 uTextureMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nvoid main() {\n  gl_Position = uMVPMatrix * vec4(aPosition.xy, 0, aPosition.w);\n  vTextureCoord = (uTextureMatrix * vec3(aTextureCoord, 1.0)).xy;\n  vColor = vec4(aPosition.z, aPosition.z, aPosition.z, aPosition.z);\n}\n"

    const-string v1, "precision mediump float;\nvarying vec2 vTextureCoord;\nvarying vec4 vColor;\nuniform sampler2D sTexture0;\nuniform float brightnessScale;\nvoid main() {\n  gl_FragColor = vColor * texture2D(sTexture0, vTextureCoord);\n  gl_FragColor.rgb *= brightnessScale;\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/t/an;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    return-void
.end method
