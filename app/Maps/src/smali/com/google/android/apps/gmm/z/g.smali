.class public Lcom/google/android/apps/gmm/z/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/z/g;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/z/b/l;

.field public final b:J

.field public final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/z/b/l;JI)V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    .line 127
    iput-wide p2, p0, Lcom/google/android/apps/gmm/z/g;->b:J

    .line 128
    iput p4, p0, Lcom/google/android/apps/gmm/z/g;->c:I

    .line 129
    return-void
.end method


# virtual methods
.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 119
    check-cast p1, Lcom/google/android/apps/gmm/z/g;

    iget-object v0, p0, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    iget-object v1, p1, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/z/b/l;->b(Lcom/google/android/apps/gmm/z/b/l;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 147
    instance-of v1, p1, Lcom/google/android/apps/gmm/z/g;

    if-eqz v1, :cond_0

    .line 148
    check-cast p1, Lcom/google/android/apps/gmm/z/g;

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    iget-object v2, p1, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/z/b/l;->b(Lcom/google/android/apps/gmm/z/b/l;)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 151
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/g;->a:Lcom/google/android/apps/gmm/z/b/l;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/b/l;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/b/l;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/b/l;->c:[Lcom/google/b/f/cq;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/l;->e:Lcom/google/r/b/a/tf;

    aput-object v0, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
