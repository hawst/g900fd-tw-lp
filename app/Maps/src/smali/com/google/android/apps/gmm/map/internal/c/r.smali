.class public Lcom/google/android/apps/gmm/map/internal/c/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/c/bo;


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field public final b:Lcom/google/android/apps/gmm/map/b/a/ai;

.field public final c:[B

.field public d:[Ljava/lang/String;

.field public e:I

.field public final f:Lcom/google/android/apps/gmm/map/internal/c/bt;

.field private final g:I

.field private h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;III[BLcom/google/android/apps/gmm/map/b/a/ai;)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v0, 0x1

    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput v1, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->e:I

    .line 34
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 35
    iput-object p6, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 36
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->g:I

    .line 37
    new-instance v3, Lcom/google/android/apps/gmm/map/internal/c/bt;

    invoke-direct {v3, p1, p6}, Lcom/google/android/apps/gmm/map/internal/c/bt;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;)V

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->f:Lcom/google/android/apps/gmm/map/internal/c/bt;

    .line 40
    array-length v3, p5

    .line 44
    if-eqz p5, :cond_2

    if-eqz v3, :cond_2

    .line 45
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/ah;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/internal/c/ah;-><init>()V

    .line 46
    invoke-virtual {v4, p5, v3}, Lcom/google/android/apps/gmm/map/internal/c/ah;->a([BI)[B

    move-result-object p5

    .line 47
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/ah;->b()[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->d:[Ljava/lang/String;

    .line 48
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/ah;->c()[Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->h:[Ljava/lang/String;

    .line 49
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/internal/c/ah;->a()Lcom/google/e/a/a/a/b;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v3, v4, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v5}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v3

    if-lez v3, :cond_5

    move v3, v0

    :goto_0
    if-nez v3, :cond_0

    invoke-virtual {v4, v5}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_6

    :cond_0
    :goto_1
    if-eqz v0, :cond_7

    const/16 v0, 0x15

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    if-nez v0, :cond_1

    move v0, v1

    :cond_1
    :goto_2
    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->e:I

    .line 50
    if-eqz p5, :cond_2

    aget-byte v0, p5, v2

    const/16 v1, 0x43

    if-ne v0, v1, :cond_2

    .line 56
    :try_start_0
    invoke-static {p5}, Lcom/google/h/a/a/c;->a([B)[B
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p5

    .line 62
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->d:[Ljava/lang/String;

    if-nez v0, :cond_3

    .line 63
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->d:[Ljava/lang/String;

    .line 65
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->h:[Ljava/lang/String;

    if-nez v0, :cond_4

    .line 66
    new-array v0, v2, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->h:[Ljava/lang/String;

    .line 68
    :cond_4
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->c:[B

    .line 69
    return-void

    :cond_5
    move v3, v2

    .line 49
    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    .line 58
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Input image is not Compact JPEG"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    move v0, v1

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    return-object v0
.end method

.method public final b()Lcom/google/android/apps/gmm/map/b/a/ai;
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->b:Lcom/google/android/apps/gmm/map/b/a/ai;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->g:I

    return v0
.end method

.method public final d()Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/r;->f:Lcom/google/android/apps/gmm/map/internal/c/bt;

    return-object v0
.end method
