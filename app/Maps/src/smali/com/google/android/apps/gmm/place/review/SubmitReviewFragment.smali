.class public Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/review/c/e;


# instance fields
.field c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field d:Landroid/view/View;

.field e:Lcom/google/android/apps/gmm/z/a/b;

.field f:Lcom/google/android/apps/gmm/place/review/c/c;

.field g:Landroid/app/ProgressDialog;

.field private h:Lcom/google/android/apps/gmm/map/b/a/j;

.field private i:Ljava/lang/String;

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Landroid/app/Dialog;

.field private final m:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;-><init>()V

    .line 78
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->i:Ljava/lang/String;

    .line 80
    const-string v0, "AndroidGMM"

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->k:Ljava/lang/String;

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/place/review/d;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/review/d;-><init>(Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->m:Ljava/lang/Object;

    .line 115
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;
    .locals 1

    .prologue
    .line 199
    new-instance v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;-><init>()V

    .line 200
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 201
    return-object v0
.end method

.method public static b(Lcom/google/android/apps/gmm/base/activities/c;)Lcom/google/android/apps/gmm/place/review/j;
    .locals 1

    .prologue
    .line 195
    new-instance v0, Lcom/google/android/apps/gmm/place/review/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/review/j;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 393
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 426
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 397
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->e:Lcom/google/android/apps/gmm/z/a/b;

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/b/f/cq;

    sget-object v5, Lcom/google/b/f/t;->fC:Lcom/google/b/f/t;

    aput-object v5, v4, v6

    .line 398
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    if-nez v0, :cond_2

    move-object v0, v1

    .line 399
    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 400
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 397
    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 403
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    .line 404
    :goto_3
    sget-object v2, Lcom/google/b/a/w;->a:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    array-length v2, v2

    .line 405
    iget v3, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->j:I

    if-le v2, v3, :cond_5

    .line 406
    sget v1, Lcom/google/android/apps/gmm/l;->lZ:I

    sget v2, Lcom/google/android/apps/gmm/l;->lY:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;II)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 396
    goto :goto_1

    .line 399
    :cond_2
    iget-object v4, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v4, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_2

    .line 403
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_3

    .line 413
    :cond_5
    if-nez p3, :cond_6

    .line 414
    sget v1, Lcom/google/android/apps/gmm/l;->ih:I

    sget v2, Lcom/google/android/apps/gmm/l;->ig:I

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/content/Context;II)V

    goto :goto_0

    .line 420
    :cond_6
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, v0, v6}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->g:Landroid/app/ProgressDialog;

    .line 421
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->g:Landroid/app/ProgressDialog;

    sget v3, Lcom/google/android/apps/gmm/l;->mP:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 422
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 424
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_7

    :goto_4
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    .line 425
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->b(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/review/l;

    move-result-object v1

    .line 424
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_4
.end method

.method protected final b(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/gmm/place/review/l;
    .locals 10

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v2, 0x0

    .line 434
    :goto_0
    new-instance v7, Lcom/google/android/apps/gmm/place/review/l;

    iget-object v8, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->h:Lcom/google/android/apps/gmm/map/b/a/j;

    iget-object v9, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->k:Ljava/lang/String;

    new-instance v0, Lcom/google/android/apps/gmm/place/review/h;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/place/review/h;-><init>(Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    move-object v1, v7

    move-object v2, v8

    move-object v3, p1

    move v4, p2

    move-object v5, v9

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/place/review/l;-><init>(Lcom/google/android/apps/gmm/map/b/a/j;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/apps/gmm/place/review/m;)V

    return-object v7

    .line 433
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v2

    goto :goto_0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 373
    sget-object v0, Lcom/google/b/f/t;->fy:Lcom/google/b/f/t;

    return-object v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->e:Lcom/google/android/apps/gmm/z/a/b;

    sget-object v1, Lcom/google/b/f/t;->fA:Lcom/google/b/f/t;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    .line 314
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->j()V

    .line 316
    :cond_0
    return-void
.end method

.method j()V
    .locals 3

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->f:Lcom/google/android/apps/gmm/place/review/c/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/c/c;->c:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 325
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->cancel()V

    .line 352
    :goto_0
    return-void

    .line 330
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/place/review/g;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/review/g;-><init>(Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;)V

    .line 344
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->i:Ljava/lang/String;

    .line 345
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/apps/gmm/l;->bY:I

    :goto_2
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->pg:I

    .line 348
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->iX:I

    .line 349
    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 350
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->l:Landroid/app/Dialog;

    .line 351
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->l:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    .line 345
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    sget v0, Lcom/google/android/apps/gmm/l;->bZ:I

    goto :goto_2
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 364
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 365
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/place/review/a/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    sget-object v3, Lcom/google/android/apps/gmm/place/review/a/b;->c:Lcom/google/android/apps/gmm/place/review/a/b;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/place/review/a/a;-><init>(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/review/a/b;)V

    .line 367
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 369
    :cond_0
    return-void

    .line 366
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 214
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 217
    const-string v0, "featureid"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 218
    const-string v0, "featureid"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/j;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->h:Lcom/google/android/apps/gmm/map/b/a/j;

    .line 224
    :goto_0
    const-string v0, "reviewsource"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const-string v0, "reviewsource"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->k:Ljava/lang/String;

    .line 228
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    :goto_1
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    .line 229
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->o()Lcom/google/r/b/a/apq;

    move-result-object v0

    iget v0, v0, Lcom/google/r/b/a/apq;->b:I

    iput v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->j:I

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    move-object v4, v0

    .line 233
    :goto_2
    if-nez v4, :cond_7

    .line 234
    :goto_3
    if-eqz v1, :cond_1

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->N()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 235
    invoke-virtual {v1}, Lcom/google/maps/g/pc;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 236
    invoke-virtual {v1}, Lcom/google/maps/g/pc;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->i:Ljava/lang/String;

    .line 240
    :cond_1
    if-eqz v4, :cond_9

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_8

    :cond_2
    move v0, v3

    :goto_4
    if-nez v0, :cond_9

    .line 241
    sget v0, Lcom/google/android/apps/gmm/l;->pe:I

    new-array v1, v3, [Ljava/lang/Object;

    .line 242
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->g()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 241
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 246
    :goto_5
    new-instance v1, Lcom/google/android/apps/gmm/place/review/c/c;

    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "fivestarrating"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->i:Ljava/lang/String;

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/google/android/apps/gmm/place/review/c/c;-><init>(Lcom/google/android/apps/gmm/place/review/c/e;Ljava/lang/String;Ljava/lang/Float;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->f:Lcom/google/android/apps/gmm/place/review/c/c;

    .line 251
    return-void

    .line 221
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_4

    move-object v0, v1

    :goto_6
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string v6, "placemarkref"

    invoke-virtual {v0, v5, v6}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->y()Lcom/google/android/apps/gmm/map/b/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->h:Lcom/google/android/apps/gmm/map/b/a/j;

    goto/16 :goto_0

    .line 221
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_6

    .line 228
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move-object v4, v1

    .line 232
    goto/16 :goto_2

    .line 233
    :cond_7
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/g/c;->O()Lcom/google/maps/g/pc;

    move-result-object v1

    goto/16 :goto_3

    :cond_8
    move v0, v2

    .line 240
    goto :goto_4

    .line 244
    :cond_9
    sget v0, Lcom/google/android/apps/gmm/l;->pd:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 292
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 293
    new-instance v1, Lcom/google/android/apps/gmm/place/review/f;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/place/review/f;-><init>(Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 307
    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 283
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->r:Lcom/google/android/libraries/curvular/bd;

    const-string v2, "GmmActivity.onCreate() has not been run, or onDestroy() has been run."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    check-cast v0, Lcom/google/android/libraries/curvular/bd;

    .line 284
    const-class v2, Lcom/google/android/apps/gmm/place/review/b/b;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/curvular/bd;->a(Ljava/lang/Class;Landroid/view/ViewGroup;)Lcom/google/android/libraries/curvular/ae;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/libraries/curvular/ae;->a:Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->d:Landroid/view/View;

    .line 285
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->f:Lcom/google/android/apps/gmm/place/review/c/c;

    invoke-static {v0, v1}, Lcom/google/android/libraries/curvular/cq;->a(Landroid/view/View;Lcom/google/android/libraries/curvular/ce;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->d:Landroid/view/View;

    return-object v0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 276
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onPause()V

    .line 277
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->m:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 278
    return-void

    .line 277
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 255
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->onResume()V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getView()Landroid/view/View;

    move-result-object v0

    sget-object v2, Lcom/google/android/apps/gmm/base/f/bn;->a:Lcom/google/android/libraries/curvular/bk;

    invoke-static {v0, v2}, Lcom/google/android/libraries/curvular/cq;->b(Landroid/view/View;Lcom/google/android/libraries/curvular/bk;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 260
    new-instance v2, Lcom/google/android/apps/gmm/place/review/e;

    invoke-direct {v2, p0, v0}, Lcom/google/android/apps/gmm/place/review/e;-><init>(Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->post(Ljava/lang/Runnable;)Z

    .line 270
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->e:Lcom/google/android/apps/gmm/z/a/b;

    .line 271
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->m:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 272
    return-void

    .line 270
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    goto :goto_0

    .line 271
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    goto :goto_1
.end method
