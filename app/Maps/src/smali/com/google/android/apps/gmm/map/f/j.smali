.class public Lcom/google/android/apps/gmm/map/f/j;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/f/k;

.field public b:F

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:Z

.field private final h:[F

.field private final i:Lcom/google/android/apps/gmm/shared/c/a;

.field private final j:Lcom/google/android/apps/gmm/map/f/o;

.field private k:Lcom/google/android/apps/gmm/map/f/k;

.field private l:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/map/f/j;-><init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/a;)V

    .line 87
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/f/o;Lcom/google/android/apps/gmm/shared/c/a;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/16 v0, 0x8

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->h:[F

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/map/f/k;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/map/f/k;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/f/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    .line 91
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/f/j;->j:Lcom/google/android/apps/gmm/map/f/o;

    .line 92
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/f/j;->i:Lcom/google/android/apps/gmm/shared/c/a;

    .line 93
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const v11, 0x3f333333    # 0.7f

    const v10, 0x3e99999a    # 0.3f

    const/4 v9, 0x0

    const/4 v1, 0x0

    const v8, 0x3a83126f    # 0.001f

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    .line 103
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    .line 104
    iput-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/j;->j:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/j;->i:Lcom/google/android/apps/gmm/shared/c/a;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/c/a;->b()J

    move-result-wide v4

    iget-object v3, v2, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iput-object v3, v0, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->f()I

    move-result v3

    iput v3, v0, Lcom/google/android/apps/gmm/map/f/k;->b:I

    iget-object v3, v2, Lcom/google/android/apps/gmm/v/n;->E:Lcom/google/android/apps/gmm/v/bi;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/v/bi;->g()I

    move-result v3

    iput v3, v0, Lcom/google/android/apps/gmm/map/f/k;->c:I

    iget-wide v6, v2, Lcom/google/android/apps/gmm/map/f/o;->t:J

    iput-wide v6, v0, Lcom/google/android/apps/gmm/map/f/k;->d:J

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/o;->e:I

    iput v2, v0, Lcom/google/android/apps/gmm/map/f/k;->e:I

    iput-wide v4, v0, Lcom/google/android/apps/gmm/map/f/k;->f:J

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    if-nez v0, :cond_1

    .line 108
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/f/j;->g:Z

    .line 146
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/k;->b:I

    iget v4, v2, Lcom/google/android/apps/gmm/map/f/k;->b:I

    if-ne v3, v4, :cond_2

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/k;->c:I

    iget v4, v2, Lcom/google/android/apps/gmm/map/f/k;->c:I

    if-eq v3, v4, :cond_4

    :cond_2
    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/f/j;->g:Z

    .line 114
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->j:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/j;->h:[F

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v0

    .line 115
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/j;->h:[F

    aget v2, v2, v1

    .line 116
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/f/j;->h:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    .line 118
    iget-object v4, p0, Lcom/google/android/apps/gmm/map/f/j;->j:Lcom/google/android/apps/gmm/map/f/o;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v5, v5, Lcom/google/android/apps/gmm/map/f/a/a;->h:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/f/j;->h:[F

    invoke-virtual {v4, v5, v6}, Lcom/google/android/apps/gmm/map/f/o;->a(Lcom/google/android/apps/gmm/map/b/a/y;[F)Z

    move-result v4

    .line 119
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/f/j;->h:[F

    aget v1, v5, v1

    .line 120
    iget-object v5, p0, Lcom/google/android/apps/gmm/map/f/j;->h:[F

    const/4 v6, 0x1

    aget v5, v5, v6

    .line 122
    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/f/j;->b:F

    .line 123
    sub-float v1, v5, v3

    iput v1, p0, Lcom/google/android/apps/gmm/map/f/j;->c:F

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v1, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    sub-float/2addr v1, v2

    iput v1, p0, Lcom/google/android/apps/gmm/map/f/j;->l:F

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iget-wide v2, v1, Lcom/google/android/apps/gmm/map/f/k;->f:J

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/f/k;->f:J

    sub-long/2addr v2, v6

    .line 126
    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->k:Lcom/google/android/apps/gmm/map/f/k;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/k;->e:I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/f/j;->a:Lcom/google/android/apps/gmm/map/f/k;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/k;->e:I

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gtz v0, :cond_6

    .line 132
    :cond_3
    iput v9, p0, Lcom/google/android/apps/gmm/map/f/j;->f:F

    iput v9, p0, Lcom/google/android/apps/gmm/map/f/j;->e:F

    iput v9, p0, Lcom/google/android/apps/gmm/map/f/j;->d:F

    goto :goto_0

    .line 111
    :cond_4
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/k;->a:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    cmpg-float v3, v3, v8

    if-gez v3, :cond_5

    iget v3, v2, Lcom/google/android/apps/gmm/map/f/a/a;->j:F

    cmpg-float v3, v3, v8

    if-gez v3, :cond_5

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    iget v4, v2, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v8

    if-gez v3, :cond_5

    iget v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    iget v4, v2, Lcom/google/android/apps/gmm/map/f/a/a;->k:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v8

    if-gez v3, :cond_5

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v3, v3, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    iget-object v4, v2, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v4, v4, Lcom/google/android/apps/gmm/map/f/a/e;->b:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v8

    if-gez v3, :cond_5

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/f/a/a;->l:Lcom/google/android/apps/gmm/map/f/a/e;

    iget v2, v2, Lcom/google/android/apps/gmm/map/f/a/e;->c:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v8

    if-gez v0, :cond_5

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_5
    move v0, v1

    goto/16 :goto_1

    .line 135
    :cond_6
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    div-long/2addr v0, v2

    long-to-float v2, v0

    .line 136
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/j;->b:F

    mul-float/2addr v0, v2

    .line 137
    iget v1, p0, Lcom/google/android/apps/gmm/map/f/j;->c:F

    mul-float/2addr v1, v2

    .line 138
    iget v3, p0, Lcom/google/android/apps/gmm/map/f/j;->l:F

    mul-float/2addr v2, v3

    .line 142
    iget v3, p0, Lcom/google/android/apps/gmm/map/f/j;->d:F

    cmpl-float v4, v3, v9

    if-nez v4, :cond_7

    :goto_2
    iput v0, p0, Lcom/google/android/apps/gmm/map/f/j;->d:F

    .line 143
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/j;->e:F

    cmpl-float v3, v0, v9

    if-nez v3, :cond_8

    move v0, v1

    :goto_3
    iput v0, p0, Lcom/google/android/apps/gmm/map/f/j;->e:F

    .line 144
    iget v0, p0, Lcom/google/android/apps/gmm/map/f/j;->f:F

    cmpl-float v1, v0, v9

    if-nez v1, :cond_9

    move v0, v2

    :goto_4
    iput v0, p0, Lcom/google/android/apps/gmm/map/f/j;->f:F

    goto/16 :goto_0

    .line 142
    :cond_7
    mul-float/2addr v0, v10

    mul-float/2addr v3, v11

    add-float/2addr v0, v3

    goto :goto_2

    .line 143
    :cond_8
    mul-float/2addr v1, v10

    mul-float/2addr v0, v11

    add-float/2addr v0, v1

    goto :goto_3

    .line 144
    :cond_9
    mul-float v1, v10, v2

    mul-float/2addr v0, v11

    add-float/2addr v0, v1

    goto :goto_4
.end method
