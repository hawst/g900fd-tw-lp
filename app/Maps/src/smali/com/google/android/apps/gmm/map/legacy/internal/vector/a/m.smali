.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final b:[I


# instance fields
.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bi;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public final j:Z

.field public final k:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bi;",
            ">;"
        }
    .end annotation
.end field

.field public final l:[F

.field public final m:Z

.field public final n:Z

.field final o:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

.field p:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->b:[I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/v/ao;Lcom/google/android/apps/gmm/map/internal/vector/gl/e;FLcom/google/android/apps/gmm/map/internal/c/bp;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 453
    const/4 v2, 0x4

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->c:I

    .line 464
    const/16 v2, 0x20

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d:I

    .line 467
    iget v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->d:I

    iget v3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->c:I

    add-int/lit8 v3, v3, 0x18

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/shared/c/s;->e(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->e:I

    .line 475
    const/4 v2, 0x5

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->f:I

    .line 482
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->g:Ljava/util/Map;

    .line 488
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    .line 493
    iput v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->i:I

    .line 507
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->k:Ljava/util/Set;

    .line 512
    const/4 v2, 0x3

    new-array v2, v2, [F

    iput-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->l:[F

    .line 559
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->j:Z

    .line 560
    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->d:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/google/android/apps/gmm/map/util/c;->e:Z

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->n:Z

    .line 561
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->m:Z

    .line 562
    const/high16 v0, 0x40000000    # 2.0f

    iget v1, p4, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    shr-int/2addr v0, v1

    .line 563
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->o:Lcom/google/android/apps/gmm/map/internal/vector/gl/e;

    .line 564
    iput p3, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->p:F

    .line 565
    return-void
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 595
    const/4 v0, 0x6

    return v0
.end method

.method public static d()Z
    .locals 1

    .prologue
    .line 817
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 587
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->e:I

    return v0
.end method

.method protected a(BI)I
    .locals 2

    .prologue
    .line 813
    and-int/lit16 v0, p1, 0xff

    and-int/lit16 v1, p2, 0xff

    mul-int/2addr v0, v1

    div-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bi;)I
    .locals 3

    .prologue
    .line 635
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bi;->d:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bi;->d:Ljava/util/List;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bi;->d:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/google/android/apps/gmm/map/internal/c/be;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bi;->d:Ljava/util/List;

    .line 636
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->g:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 637
    if-nez v0, :cond_2

    .line 638
    iget v0, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->i:I

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 639
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->g:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 640
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 641
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 643
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/m;->h:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1, v2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 645
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x4

    return v0
.end method
