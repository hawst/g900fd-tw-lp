.class final enum Lcom/google/android/apps/gmm/v/cq;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/v/cq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/v/cq;

.field public static final enum b:Lcom/google/android/apps/gmm/v/cq;

.field public static final enum c:Lcom/google/android/apps/gmm/v/cq;

.field private static final synthetic d:[Lcom/google/android/apps/gmm/v/cq;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/google/android/apps/gmm/v/cq;

    const-string v1, "NEXT_FRAME"

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/v/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/v/cq;->a:Lcom/google/android/apps/gmm/v/cq;

    .line 17
    new-instance v0, Lcom/google/android/apps/gmm/v/cq;

    const-string v1, "NEXT_FRAME_PASSIVE"

    invoke-direct {v0, v1, v3}, Lcom/google/android/apps/gmm/v/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/v/cq;->b:Lcom/google/android/apps/gmm/v/cq;

    .line 18
    new-instance v0, Lcom/google/android/apps/gmm/v/cq;

    const-string v1, "BEHAVIOR_RUN"

    invoke-direct {v0, v1, v4}, Lcom/google/android/apps/gmm/v/cq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/apps/gmm/v/cq;->c:Lcom/google/android/apps/gmm/v/cq;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/apps/gmm/v/cq;

    sget-object v1, Lcom/google/android/apps/gmm/v/cq;->a:Lcom/google/android/apps/gmm/v/cq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/apps/gmm/v/cq;->b:Lcom/google/android/apps/gmm/v/cq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/apps/gmm/v/cq;->c:Lcom/google/android/apps/gmm/v/cq;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/apps/gmm/v/cq;->d:[Lcom/google/android/apps/gmm/v/cq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/v/cq;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/apps/gmm/v/cq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/v/cq;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/v/cq;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/apps/gmm/v/cq;->d:[Lcom/google/android/apps/gmm/v/cq;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/v/cq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/v/cq;

    return-object v0
.end method
