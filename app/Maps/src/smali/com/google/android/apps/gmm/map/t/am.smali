.class public Lcom/google/android/apps/gmm/map/t/am;
.super Lcom/google/android/apps/gmm/v/cd;
.source "PG"


# instance fields
.field public b:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/gmm/map/t/aq;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/v/cd;-><init>(Ljava/lang/Class;I)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 28
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/t/am;-><init>(III)V

    .line 36
    return-void
.end method

.method private constructor <init>(III)V
    .locals 4

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 49
    const/4 v0, 0x4

    new-array v0, v0, [F

    const/4 v1, 0x0

    .line 50
    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x1

    .line 51
    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x2

    .line 52
    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    .line 53
    invoke-static {p3}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 49
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/map/t/am;-><init>(II[F)V

    .line 55
    return-void
.end method

.method private constructor <init>(II[F)V
    .locals 3

    .prologue
    .line 58
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid blend mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-class v0, Lcom/google/android/apps/gmm/map/t/ap;

    :goto_0
    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/apps/gmm/v/cd;-><init>(Ljava/lang/Class;I[F)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 59
    return-void

    .line 58
    :pswitch_1
    const-class v0, Lcom/google/android/apps/gmm/map/t/aq;

    goto :goto_0

    :pswitch_2
    const-class v0, Lcom/google/android/apps/gmm/map/t/ao;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/v/cd;-><init>(Ljava/lang/Class;I)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;II)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/v/cd;-><init>(Ljava/lang/Class;II)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 41
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;I[F)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;I[F)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/v/cd;-><init>(Ljava/lang/Class;I[F)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    .line 46
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/cd;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/t/am;->t:Lcom/google/android/apps/gmm/v/bo;

    check-cast v0, Lcom/google/android/apps/gmm/map/t/an;

    iget v0, v0, Lcom/google/android/apps/gmm/map/t/an;->b:I

    iget v1, p0, Lcom/google/android/apps/gmm/map/t/am;->b:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 98
    return-void
.end method
