.class Lcom/google/android/apps/gmm/streetview/j;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/geo/render/mirth/api/aw;

.field final synthetic b:Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;Lcom/google/geo/render/mirth/api/aw;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/apps/gmm/streetview/j;->b:Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;

    iput-object p2, p0, Lcom/google/android/apps/gmm/streetview/j;->a:Lcom/google/geo/render/mirth/api/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 141
    new-instance v0, Lcom/google/geo/render/mirth/api/as;

    invoke-direct {v0}, Lcom/google/geo/render/mirth/api/as;-><init>()V

    .line 142
    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/as;->a:J

    invoke-static {v2, v3, v0, v4}, Lcom/google/geo/render/mirth/api/PhotoModeOptionsSwigJNI;->PhotoModeOptions_transitions_enabled_set(JLcom/google/geo/render/mirth/api/as;Z)V

    .line 143
    const/4 v1, 0x0

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/as;->a:J

    invoke-static {v2, v3, v0, v1}, Lcom/google/geo/render/mirth/api/PhotoModeOptionsSwigJNI;->PhotoModeOptions_transition_hints_visible_set(JLcom/google/geo/render/mirth/api/as;Z)V

    .line 144
    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/as;->a:J

    invoke-static {v2, v3, v0, v4}, Lcom/google/geo/render/mirth/api/PhotoModeOptionsSwigJNI;->PhotoModeOptions_text_overlays_visible_set(JLcom/google/geo/render/mirth/api/as;Z)V

    .line 146
    const-string v1, "ground_disk_enabled"

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/as;->a:J

    invoke-static {v2, v3, v0, v1}, Lcom/google/geo/render/mirth/api/PhotoModeOptionsSwigJNI;->PhotoModeOptions_enableExperiment(JLcom/google/geo/render/mirth/api/as;Ljava/lang/String;)V

    .line 147
    const-string v1, "transition_via_swipe_enabled"

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/as;->a:J

    invoke-static {v2, v3, v0, v1}, Lcom/google/geo/render/mirth/api/PhotoModeOptionsSwigJNI;->PhotoModeOptions_enableExperiment(JLcom/google/geo/render/mirth/api/as;Ljava/lang/String;)V

    .line 148
    const-string v1, "swipe_gesture_on_ground"

    iget-wide v2, v0, Lcom/google/geo/render/mirth/api/as;->a:J

    invoke-static {v2, v3, v0, v1}, Lcom/google/geo/render/mirth/api/PhotoModeOptionsSwigJNI;->PhotoModeOptions_enableExperiment(JLcom/google/geo/render/mirth/api/as;Ljava/lang/String;)V

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/j;->b:Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v1}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/geo/render/mirth/api/e;->b()Lcom/google/geo/render/mirth/api/av;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/geo/render/mirth/api/av;->a(Lcom/google/geo/render/mirth/api/as;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/streetview/j;->b:Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/streetview/MirthStreetViewFragment;->a:Lcom/google/geo/render/mirth/api/MirthSurfaceView;

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/MirthSurfaceView;->a()Lcom/google/geo/render/mirth/api/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/geo/render/mirth/api/e;->b()Lcom/google/geo/render/mirth/api/av;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/streetview/j;->a:Lcom/google/geo/render/mirth/api/aw;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/geo/render/mirth/api/av;->a(Lcom/google/geo/render/mirth/api/aw;D)V

    .line 151
    return-void
.end method
