.class public Lcom/google/android/apps/gmm/z/f;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/gmm/shared/c/f;

.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Lcom/google/android/apps/gmm/z/g;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/gmm/z/f;->b:I

    .line 51
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/z/f;->d:Ljava/util/TreeSet;

    .line 54
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/f;->a:Lcom/google/android/apps/gmm/shared/c/f;

    .line 55
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/g;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 64
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/f;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->size()I

    move-result v1

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/z/g;

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/f;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    invoke-direct {v0, p1, v2, v3, v1}, Lcom/google/android/apps/gmm/z/g;-><init>(Lcom/google/android/apps/gmm/z/b/l;JI)V

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/f;->d:Ljava/util/TreeSet;

    invoke-virtual {v1, v0}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 73
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/z/f;->c:Ljava/lang/String;

    .line 83
    iput p2, p0, Lcom/google/android/apps/gmm/z/f;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    monitor-exit p0

    return-void

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 78
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/z/f;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()I
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/apps/gmm/z/f;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/google/android/apps/gmm/z/m;
    .locals 4
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/z/f;->d:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 108
    :goto_0
    monitor-exit p0

    return-object v0

    .line 96
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/apps/gmm/z/m;

    iget-object v1, p0, Lcom/google/android/apps/gmm/z/f;->d:Ljava/util/TreeSet;

    .line 97
    invoke-static {v1}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/z/f;->a:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/z/m;-><init>(Ljava/util/List;Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/z/f;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 100
    iget v1, p0, Lcom/google/android/apps/gmm/z/f;->b:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/z/b/e;->b:Lcom/google/b/f/b/a/bc;

    iget v3, v2, Lcom/google/b/f/b/a/bc;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v2, Lcom/google/b/f/b/a/bc;->a:I

    iput v1, v2, Lcom/google/b/f/b/a/bc;->b:I

    .line 101
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/f;->c:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/apps/gmm/z/f;->b:I

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/z/b/t;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/apps/gmm/z/b/a;->f:Ljava/lang/String;

    .line 105
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/z/f;->d:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->clear()V

    .line 106
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/gmm/z/f;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
