.class public Lcom/google/android/apps/gmm/base/i/g;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/apps/gmm/base/i/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/base/i/g;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/base/l/a/ab;IIIZ)V
    .locals 4

    .prologue
    const/16 v1, 0xff

    const/4 v0, 0x0

    .line 131
    if-ne p2, p3, :cond_3

    if-ge p1, p2, :cond_2

    .line 133
    :goto_0
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/l/a/ab;->c()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v1

    iput v0, v1, Lcom/google/android/apps/gmm/base/views/c/g;->k:I

    .line 134
    if-eqz p4, :cond_0

    .line 135
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/l/a/ab;->c()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v1

    iput v0, v1, Lcom/google/android/apps/gmm/base/views/c/g;->j:I

    .line 137
    :cond_0
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/l/a/ab;->c()Lcom/google/android/apps/gmm/base/views/c/g;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/g;->i:Lcom/google/android/apps/gmm/base/views/c/j;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/views/c/g;->i:Lcom/google/android/apps/gmm/base/views/c/j;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/views/c/j;->a(Lcom/google/android/apps/gmm/base/views/c/g;)V

    .line 138
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 131
    goto :goto_0

    :cond_3
    sub-int v2, p1, p2

    int-to-float v2, v2

    sub-int v3, p3, p2

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x437f0000    # 255.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/support/v7/widget/RecyclerView;IIZ)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->b()Landroid/support/v7/widget/bs;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/ai;

    .line 99
    invoke-virtual {v0}, Landroid/support/v7/widget/ai;->f()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 113
    :goto_0
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    .line 112
    invoke-static {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;IIIZ)V

    .line 115
    return-void

    .line 102
    :cond_0
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ai;->a(I)Landroid/view/View;

    move-result-object v0

    .line 103
    if-nez v0, :cond_1

    .line 107
    const v0, 0x7fffffff

    goto :goto_0

    .line 109
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;IIZ)V

    .line 38
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/view/View;IIZ)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 56
    if-gt p2, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    move v0, v1

    goto :goto_0

    .line 57
    :cond_1
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_2

    .line 58
    check-cast p1, Landroid/support/v7/widget/RecyclerView;

    .line 59
    new-instance v0, Lcom/google/android/apps/gmm/base/i/h;

    invoke-direct {v0, p0, p2, p3, p4}, Lcom/google/android/apps/gmm/base/i/h;-><init>(Lcom/google/android/apps/gmm/base/l/a/ab;IIZ)V

    invoke-virtual {p1, v0}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(Landroid/support/v7/widget/bu;)V

    .line 69
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;Landroid/support/v7/widget/RecyclerView;IIZ)V

    .line 91
    :goto_1
    return-void

    .line 72
    :cond_2
    instance-of v0, p1, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    if-eqz v0, :cond_3

    move-object v2, p1

    .line 73
    check-cast v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;

    .line 75
    new-instance v0, Lcom/google/android/apps/gmm/base/i/i;

    move-object v1, p0

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/i/i;-><init>(Lcom/google/android/apps/gmm/base/l/a/ab;Lcom/google/android/apps/gmm/base/views/MultiColumnListView;IIZ)V

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->j:Lcom/google/android/apps/gmm/base/views/av;

    .line 85
    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getScrollY()I

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getContext()Landroid/content/Context;

    invoke-static {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;IIIZ)V

    goto :goto_1

    .line 89
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/base/i/g;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unsupported listView="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method static a(Lcom/google/android/apps/gmm/base/l/a/ab;Lcom/google/android/apps/gmm/base/views/MultiColumnListView;IIZ)V
    .locals 1

    .prologue
    .line 121
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getScrollY()I

    move-result v0

    .line 123
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/views/MultiColumnListView;->getContext()Landroid/content/Context;

    .line 122
    invoke-static {p0, v0, p2, p3, p4}, Lcom/google/android/apps/gmm/base/i/g;->a(Lcom/google/android/apps/gmm/base/l/a/ab;IIIZ)V

    .line 125
    return-void
.end method
