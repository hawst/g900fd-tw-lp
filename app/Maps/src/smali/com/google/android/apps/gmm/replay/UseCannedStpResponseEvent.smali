.class public Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation runtime Lcom/google/android/apps/gmm/util/replay/c;
    a = "stpdebug"
    b = .enum Lcom/google/android/apps/gmm/util/replay/d;->MEDIUM:Lcom/google/android/apps/gmm/util/replay/d;
.end annotation


# static fields
.field public static final CANNED_RESPONSE_DISABLED:Ljava/lang/String; = "Turn off canned response"

.field public static final DEFAULT_PLACE_COUNT:I = 0xa


# instance fields
.field private final placeCount:I

.field private final stateType:Lcom/google/android/apps/gmm/iamhere/c/q;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/iamhere/c/q;I)V
    .locals 0
    .param p1    # Lcom/google/android/apps/gmm/iamhere/c/q;
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "stateType"
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/google/android/apps/gmm/util/replay/g;
            a = "placeCount"
        .end annotation
    .end param

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;->stateType:Lcom/google/android/apps/gmm/iamhere/c/q;

    .line 29
    iput p2, p0, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;->placeCount:I

    .line 30
    return-void
.end method


# virtual methods
.method public getPlaceCount()I
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "placeCount"
    .end annotation

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;->placeCount:I

    return v0
.end method

.method public getStateType()Lcom/google/android/apps/gmm/iamhere/c/q;
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/util/replay/e;
        a = "stateType"
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/gmm/replay/UseCannedStpResponseEvent;->stateType:Lcom/google/android/apps/gmm/iamhere/c/q;

    return-object v0
.end method
