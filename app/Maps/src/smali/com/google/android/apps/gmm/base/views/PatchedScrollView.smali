.class public Lcom/google/android/apps/gmm/base/views/PatchedScrollView;
.super Landroid/widget/ScrollView;
.source "PG"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/base/views/PatchedScrollView;->getScrollY()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v4}, Lcom/google/android/apps/gmm/base/views/PatchedScrollView;->canScrollVertically(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 61
    :goto_0
    return v0

    .line 46
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_4

    .line 47
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    .line 48
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    .line 49
    invoke-static {p0, v0, v4, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;ZIII)Z

    move-result v3

    if-nez v3, :cond_1

    .line 50
    invoke-static {p0, v0, v5, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->a(Landroid/view/View;ZIII)Z

    move-result v3

    if-nez v3, :cond_1

    .line 51
    invoke-static {p0, v0, v4, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->b(Landroid/view/View;ZIII)Z

    move-result v3

    if-nez v3, :cond_1

    .line 52
    invoke-static {p0, v0, v5, v1, v2}, Lcom/google/android/apps/gmm/base/views/d/g;->b(Landroid/view/View;ZIII)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 53
    :cond_1
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/base/views/PatchedScrollView;->a:Z

    goto :goto_0

    .line 56
    :cond_2
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/base/views/PatchedScrollView;->a:Z

    .line 61
    :cond_3
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 57
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/views/PatchedScrollView;->a:Z

    if-eqz v1, :cond_3

    goto :goto_0
.end method
