.class Lcom/google/android/apps/gmm/cardui/p;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/util/b/w;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    return-void
.end method

.method private static a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/r;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 68
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/r;

    .line 69
    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/r;->b:Lcom/google/android/libraries/curvular/ce;

    instance-of v1, v1, Lcom/google/android/apps/gmm/util/b/u;

    if-eqz v1, :cond_0

    .line 70
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/r;->b:Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/util/b/u;

    .line 71
    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/u;->a()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 73
    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/util/b/r;->e:Z

    return v0
.end method

.method private static b(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/util/b/r;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 77
    :goto_0
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/util/b/r;

    .line 78
    iget-object v1, v0, Lcom/google/android/apps/gmm/util/b/r;->b:Lcom/google/android/libraries/curvular/ce;

    instance-of v1, v1, Lcom/google/android/apps/gmm/util/b/u;

    if-eqz v1, :cond_0

    .line 79
    iget-object v0, v0, Lcom/google/android/apps/gmm/util/b/r;->b:Lcom/google/android/libraries/curvular/ce;

    check-cast v0, Lcom/google/android/apps/gmm/util/b/u;

    .line 80
    invoke-interface {v0}, Lcom/google/android/apps/gmm/util/b/u;->a()Ljava/util/List;

    move-result-object p0

    goto :goto_0

    .line 82
    :cond_0
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/util/b/r;->e:Z

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/util/b/q;Lcom/google/android/apps/gmm/util/b/q;)Ljava/lang/Class;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/util/b/q;",
            "Lcom/google/android/apps/gmm/util/b/q;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 37
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 63
    :cond_0
    :goto_0
    return-object v0

    .line 42
    :cond_1
    invoke-interface {p1}, Lcom/google/android/apps/gmm/util/b/q;->b()Lcom/google/o/h/a/bw;

    move-result-object v1

    .line 43
    invoke-interface {p2}, Lcom/google/android/apps/gmm/util/b/q;->b()Lcom/google/o/h/a/bw;

    move-result-object v2

    .line 45
    sget-object v3, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    if-ne v1, v3, :cond_0

    sget-object v1, Lcom/google/o/h/a/bw;->d:Lcom/google/o/h/a/bw;

    if-ne v2, v1, :cond_0

    .line 50
    invoke-interface {p1}, Lcom/google/android/apps/gmm/util/b/q;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/cardui/p;->a(Ljava/util/List;)Z

    move-result v1

    .line 51
    invoke-interface {p2}, Lcom/google/android/apps/gmm/util/b/q;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/apps/gmm/cardui/p;->b(Ljava/util/List;)Z

    move-result v2

    .line 52
    if-eqz v1, :cond_2

    if-nez v2, :cond_0

    .line 57
    :cond_2
    if-nez v1, :cond_3

    if-nez v2, :cond_3

    .line 59
    const-class v0, Lcom/google/android/apps/gmm/cardui/q;

    goto :goto_0

    .line 63
    :cond_3
    const-class v0, Lcom/google/android/apps/gmm/cardui/r;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/util/b/r;Lcom/google/android/apps/gmm/util/b/r;)Ljava/lang/Class;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/util/b/r;",
            "Lcom/google/android/apps/gmm/util/b/r;",
            ")",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/libraries/curvular/ay",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 116
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 136
    :cond_0
    :goto_0
    return-object v0

    .line 121
    :cond_1
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/util/b/r;->d:Z

    if-nez v1, :cond_0

    .line 126
    iget-boolean v1, p2, Lcom/google/android/apps/gmm/util/b/r;->c:Z

    if-nez v1, :cond_0

    .line 131
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/util/b/r;->e:Z

    if-nez v1, :cond_0

    iget-boolean v1, p2, Lcom/google/android/apps/gmm/util/b/r;->e:Z

    if-nez v1, :cond_0

    .line 136
    const-class v0, Lcom/google/android/apps/gmm/base/f/v;

    goto :goto_0
.end method
