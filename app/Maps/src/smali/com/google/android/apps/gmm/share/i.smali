.class public Lcom/google/android/apps/gmm/share/i;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/share/a/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    return-void
.end method

.method private varargs a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ[Lcom/google/android/apps/gmm/share/a/a;)V
    .locals 8
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/j/c;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 90
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->nc:I

    move-object v2, p2

    move-object v3, p1

    move-object v4, p3

    move v5, p4

    move v6, p5

    move-object v7, p6

    .line 89
    invoke-static/range {v0 .. v7}, Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ[Lcom/google/android/apps/gmm/share/a/a;)Lcom/google/android/apps/gmm/share/UrlShareAppChooserBottomSheetDialogFragment;

    move-result-object v1

    .line 92
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    .line 93
    return-void
.end method


# virtual methods
.method public final varargs a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[Lcom/google/android/apps/gmm/share/a/a;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 110
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/share/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ[Lcom/google/android/apps/gmm/share/a/a;)V

    .line 111
    return-void
.end method

.method public final varargs a(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;[Lcom/google/android/apps/gmm/share/a/a;)V
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "[",
            "Lcom/google/android/apps/gmm/share/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 38
    if-nez p2, :cond_1

    :cond_0
    :goto_0
    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v5, v4

    move-object v6, p4

    .line 40
    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/gmm/share/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ[Lcom/google/android/apps/gmm/share/a/a;)V

    .line 42
    return-void

    .line 38
    :cond_1
    const-string v0, "\n"

    .line 39
    new-instance v1, Lcom/google/b/a/ab;

    invoke-direct {v1, v0}, Lcom/google/b/a/ab;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/b/a/ab;->a()Lcom/google/b/a/ab;

    move-result-object v0

    invoke-interface {p2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3, v1}, Lcom/google/b/a/ab;->a(Ljava/lang/StringBuilder;Ljava/util/Iterator;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_0

    move-object v2, v0

    goto :goto_0

    :cond_3
    move v1, v4

    goto :goto_1
.end method
