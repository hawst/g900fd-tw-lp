.class Lcom/google/android/apps/gmm/place/review/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/review/m;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/base/activities/c;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:I

.field final synthetic f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;Lcom/google/android/apps/gmm/base/activities/c;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 435
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iput-object p2, p0, Lcom/google/android/apps/gmm/place/review/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object p3, p0, Lcom/google/android/apps/gmm/place/review/h;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/apps/gmm/place/review/h;->c:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/apps/gmm/place/review/h;->d:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/apps/gmm/place/review/h;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 477
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->g:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 478
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 479
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->g:Landroid/app/ProgressDialog;

    .line 481
    :cond_0
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 484
    new-instance v0, Lcom/google/android/apps/gmm/place/review/i;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/place/review/i;-><init>(Lcom/google/android/apps/gmm/place/review/h;)V

    .line 501
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->d:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/apps/gmm/l;->no:I

    .line 502
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->pg:I

    .line 503
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->iX:I

    .line 504
    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 506
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->e:Lcom/google/android/apps/gmm/z/a/b;

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    const/4 v2, 0x0

    .line 507
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/b/f/t;->fD:Lcom/google/b/f/t;

    aput-object v4, v2, v3

    .line 508
    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 509
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    .line 506
    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    .line 510
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 472
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/review/h;->b()V

    .line 473
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/review/h;->c()V

    .line 474
    return-void
.end method

.method public final a(Lcom/google/e/a/a/a/b;Lcom/google/e/a/a/a/b;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v9, 0x3

    const/4 v10, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 438
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/review/h;->b()V

    .line 440
    invoke-static {p2, v2, v1}, Lcom/google/android/apps/gmm/shared/c/b/a;->a(Lcom/google/e/a/a/a/b;II)I

    move-result v0

    .line 443
    if-eqz v0, :cond_0

    .line 444
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/review/h;->c()V

    .line 468
    :goto_0
    return-void

    .line 448
    :cond_0
    const/4 v0, 0x4

    invoke-static {p2, v0}, Lcom/google/android/apps/gmm/shared/c/b/a;->b(Lcom/google/e/a/a/a/b;I)Ljava/lang/String;

    move-result-object v3

    .line 450
    if-eqz v3, :cond_1

    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j_()Lcom/google/android/apps/gmm/myplaces/a/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/myplaces/a/a;->a(Ljava/lang/String;)V

    .line 455
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "showthanksonsubmit"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->nq:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 460
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/review/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v3, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    if-eqz v0, :cond_15

    .line 464
    iget-object v3, p0, Lcom/google/android/apps/gmm/place/review/h;->f:Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/review/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/review/h;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/apps/gmm/place/review/h;->c:Ljava/lang/String;

    invoke-static {}, Lcom/google/maps/g/pc;->newBuilder()Lcom/google/maps/g/pe;

    move-result-object v7

    sget v0, Lcom/google/android/apps/gmm/l;->nz:I

    invoke-virtual {v4, v0}, Lcom/google/android/apps/gmm/base/activities/c;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 458
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    sget v3, Lcom/google/android/apps/gmm/l;->np:I

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/base/activities/c;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_1

    .line 464
    :cond_3
    iget v8, v7, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v8, v8, 0x10

    iput v8, v7, Lcom/google/maps/g/pe;->a:I

    iput-object v0, v7, Lcom/google/maps/g/pe;->c:Ljava/lang/Object;

    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v9}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_7

    move v0, v2

    :goto_2
    if-nez v0, :cond_4

    invoke-virtual {p1, v9}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_8

    :cond_4
    move v0, v2

    :goto_3
    if-eqz v0, :cond_5

    const/16 v0, 0x15

    invoke-virtual {p1, v9, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v0, v8

    iget v8, v7, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v8, v8, 0x40

    iput v8, v7, Lcom/google/maps/g/pe;->a:I

    iput v0, v7, Lcom/google/maps/g/pe;->d:I

    :cond_5
    iget-object v0, p1, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v0, v10}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_9

    move v0, v2

    :goto_4
    if-nez v0, :cond_6

    invoke-virtual {p1, v10}, Lcom/google/e/a/a/a/b;->b(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_a

    :cond_6
    move v0, v2

    :goto_5
    if-eqz v0, :cond_c

    const/16 v0, 0x1c

    invoke-virtual {p1, v10, v0}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    move v0, v1

    goto :goto_2

    :cond_8
    move v0, v1

    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_4

    :cond_a
    move v0, v1

    goto :goto_5

    :cond_b
    iget v8, v7, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v8, v8, 0x80

    iput v8, v7, Lcom/google/maps/g/pe;->a:I

    iput-object v0, v7, Lcom/google/maps/g/pe;->e:Ljava/lang/Object;

    :cond_c
    invoke-static {}, Lcom/google/maps/g/hg;->newBuilder()Lcom/google/maps/g/hi;

    move-result-object v0

    if-eqz v5, :cond_e

    if-nez v5, :cond_d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_d
    iget v8, v0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v8, v8, 0x8

    iput v8, v0, Lcom/google/maps/g/hi;->a:I

    iput-object v5, v0, Lcom/google/maps/g/hi;->c:Ljava/lang/Object;

    :cond_e
    if-eqz v6, :cond_10

    if-nez v6, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_f
    iget v5, v0, Lcom/google/maps/g/hi;->a:I

    or-int/lit8 v5, v5, 0x10

    iput v5, v0, Lcom/google/maps/g/hi;->a:I

    iput-object v6, v0, Lcom/google/maps/g/hi;->d:Ljava/lang/Object;

    :cond_10
    invoke-virtual {v0}, Lcom/google/maps/g/hi;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_11
    iget-object v5, v7, Lcom/google/maps/g/pe;->b:Lcom/google/n/ao;

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v5, Lcom/google/n/ao;->d:Z

    iget v0, v7, Lcom/google/maps/g/pe;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v7, Lcom/google/maps/g/pe;->a:I

    iget-object v0, v3, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->O()Lcom/google/maps/g/pc;

    move-result-object v5

    if-eqz v5, :cond_14

    iget v6, v5, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v6, v6, 0x800

    const/16 v8, 0x800

    if-ne v6, v8, :cond_12

    move v1, v2

    :cond_12
    if-eqz v1, :cond_14

    iget-object v1, v5, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ju;->g()Lcom/google/maps/g/ju;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/ju;

    if-nez v1, :cond_13

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_13
    iget-object v5, v7, Lcom/google/maps/g/pe;->f:Lcom/google/n/ao;

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v11, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v2, v5, Lcom/google/n/ao;->d:Z

    iget v1, v7, Lcom/google/maps/g/pe;->a:I

    or-int/lit16 v1, v1, 0x1000

    iput v1, v7, Lcom/google/maps/g/pe;->a:I

    :cond_14
    invoke-virtual {v7}, Lcom/google/maps/g/pe;->g()Lcom/google/n/t;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/pc;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/g/c;->a(Lcom/google/maps/g/pc;)V

    iget-object v1, v3, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/place/review/a/a;

    iget-object v2, v3, Lcom/google/android/apps/gmm/place/review/SubmitReviewFragment;->c:Lcom/google/android/apps/gmm/x/o;

    sget-object v3, Lcom/google/android/apps/gmm/place/review/a/b;->a:Lcom/google/android/apps/gmm/place/review/a/b;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/place/review/a/a;-><init>(Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/review/a/b;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 467
    :cond_15
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/h;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    goto/16 :goto_0
.end method
