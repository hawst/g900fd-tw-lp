.class public Lcom/google/android/apps/gmm/w/a;
.super Lcom/google/android/apps/gmm/base/j/c;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/w/a/a;


# instance fields
.field private a:Lcom/google/android/apps/gmm/base/activities/c;

.field private b:Z

.field private c:Lcom/google/android/apps/gmm/w/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/j/c;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/w/a;->b:Z

    .line 255
    return-void
.end method


# virtual methods
.method public final Y_()V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/google/android/apps/gmm/w/b;

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/w/b;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    .line 74
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x1

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 75
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 64
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/af;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 226
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/w/a;->b:Z

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v0

    .line 228
    instance-of v1, v0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    if-eqz v1, :cond_0

    .line 229
    check-cast v0, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    .line 230
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->i()V

    .line 233
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/f;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 207
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/f;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/f;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/j/g;

    .line 209
    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v1

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/j/g;->a:Ljava/lang/String;

    .line 210
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/apps/gmm/map/j/g;->b:Ljava/lang/String;

    .line 211
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/b/f/cq;

    const/4 v5, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/j/g;->c:Lcom/google/b/f/t;

    aput-object v0, v4, v5

    .line 212
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 213
    invoke-virtual {v3}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 209
    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    goto :goto_0

    .line 221
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/h;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 186
    .line 187
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/h;->a:Lcom/google/android/apps/gmm/map/j/i;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/j/i;->c:Lcom/google/r/b/a/a;

    if-eqz v0, :cond_1

    .line 188
    new-instance v0, Lcom/google/android/apps/gmm/z/b/n;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/h;->a:Lcom/google/android/apps/gmm/map/j/i;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/j/i;->c:Lcom/google/r/b/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    move-object v1, v0

    .line 190
    :goto_0
    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 193
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    .line 194
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/b/f/cq;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/j/h;->d:Lcom/google/b/f/cq;

    aput-object v4, v2, v3

    .line 195
    iput-object v2, v0, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 196
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v2

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    move-result-object v0

    .line 199
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/map/j/h;->e:Z

    if-eqz v1, :cond_0

    .line 200
    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    new-instance v2, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/w/b;->a(Lcom/google/maps/g/hy;)V

    .line 202
    :cond_0
    return-void

    .line 189
    :cond_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/h;->a:Lcom/google/android/apps/gmm/map/j/i;

    move-object v1, v2

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/t;)V
    .locals 6
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 99
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    instance-of v0, v0, Lcom/google/android/apps/gmm/map/g/a;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->a:Lcom/google/android/apps/gmm/map/g/b;

    check-cast v0, Lcom/google/android/apps/gmm/map/g/a;

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/g/a;->f:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/w/a;->b:Z

    if-eqz v1, :cond_6

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/g/a;->f:Z

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->a:Lcom/google/android/apps/gmm/map/internal/c/c;

    if-eqz v1, :cond_5

    move v1, v4

    :goto_0
    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->i()Lcom/google/android/apps/gmm/b/a/a;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/b/a/a;->a(Lcom/google/android/apps/gmm/map/g/a;)V

    .line 100
    :cond_0
    :goto_1
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_e

    :cond_1
    move v0, v4

    :goto_2
    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/t;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_f

    :cond_2
    move v0, v4

    :goto_3
    if-nez v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/j/t;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/j/t;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/apps/gmm/z/b/m;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/z/a/b;->b(Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 101
    :cond_4
    return-void

    :cond_5
    move v1, v3

    .line 99
    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->g()Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v2

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/g/a;->g:Z

    if-nez v1, :cond_7

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    :cond_7
    sget-object v1, Lcom/google/b/f/t;->gd:Lcom/google/b/f/t;

    :goto_4
    invoke-static {v2, v1}, Lcom/google/android/apps/gmm/z/i;->a(Lcom/google/android/apps/gmm/z/a/b;Lcom/google/b/f/t;)Ljava/lang/String;

    :cond_8
    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->b:Lcom/google/android/apps/gmm/map/internal/c/bb;

    if-eqz v1, :cond_a

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/g/a;->b:Lcom/google/android/apps/gmm/map/internal/c/bb;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/c/bb;->a:Ljava/lang/String;

    :goto_5
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/w/a;->b:Z

    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/android/apps/gmm/map/g/a;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v2

    iput-boolean v3, v2, Lcom/google/android/apps/gmm/base/g/g;->f:Z

    iput-object v1, v2, Lcom/google/android/apps/gmm/base/g/g;->o:Ljava/lang/String;

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-boolean v4, v1, Lcom/google/android/apps/gmm/base/g/i;->h:Z

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/map/g/a;->g:Z

    iget-object v5, v2, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v5, Lcom/google/android/apps/gmm/base/g/i;->e:Ljava/lang/Boolean;

    iput-boolean v4, v2, Lcom/google/android/apps/gmm/base/g/g;->i:Z

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v5

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->d()Landroid/app/Fragment;

    move-result-object v1

    instance-of v2, v1, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    if-eqz v2, :cond_b

    check-cast v1, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;

    iget-object v2, v1, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->g:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v5, v2}, Lcom/google/android/apps/gmm/base/g/c;->b(Lcom/google/android/apps/gmm/base/g/c;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/place/PlacemarkDetailsFragment;->i()V

    goto/16 :goto_1

    :cond_9
    sget-object v1, Lcom/google/b/f/t;->ga:Lcom/google/b/f/t;

    goto :goto_4

    :cond_a
    const-string v1, ""

    goto :goto_5

    :cond_b
    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/j/b;->D()Lcom/google/android/apps/gmm/place/b/b;

    move-result-object v1

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/map/g/a;->g:Z

    if-nez v2, :cond_c

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/g/a;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    :cond_c
    sget-object v0, Lcom/google/b/f/t;->gd:Lcom/google/b/f/t;

    :goto_6
    const/4 v2, 0x0

    invoke-interface {v1, v5, v3, v0, v2}, Lcom/google/android/apps/gmm/place/b/b;->a(Lcom/google/android/apps/gmm/base/g/c;ZLcom/google/b/f/t;Lcom/google/android/apps/gmm/base/views/expandingscrollview/k;)V

    goto/16 :goto_1

    :cond_d
    sget-object v0, Lcom/google/b/f/t;->ga:Lcom/google/b/f/t;

    goto :goto_6

    :cond_e
    move v0, v3

    .line 100
    goto/16 :goto_2

    :cond_f
    move v0, v3

    goto/16 :goto_3
.end method

.method public a(Lcom/google/android/apps/gmm/map/j/y;)V
    .locals 5
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 177
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/j/y;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/j/y;->a:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v3, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 177
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 178
    :cond_2
    iget v4, v3, Lcom/google/maps/g/ia;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v3, Lcom/google/maps/g/ia;->a:I

    iput-object v2, v3, Lcom/google/maps/g/ia;->c:Ljava/lang/Object;

    :cond_3
    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/w/b;->a(Lcom/google/maps/g/hy;)V

    .line 180
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/j/y;->b:Lcom/google/android/apps/gmm/map/b/a/ag;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/w/b;->a(Lcom/google/android/apps/gmm/map/b/a/ag;)V

    .line 181
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    new-instance v0, Lcom/google/android/apps/gmm/z/b/f;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/z/b/f;-><init>()V

    .line 171
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/z/b/f;->c(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/z/b/f;->a:Lcom/google/maps/g/ia;

    invoke-virtual {v0}, Lcom/google/maps/g/ia;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hy;

    .line 170
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/w/b;->a(Lcom/google/maps/g/hy;)V

    .line 172
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 237
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/w/a;->b:Z

    .line 238
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->w()Lcom/google/android/apps/gmm/v/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    iget-object v0, v0, Lcom/google/android/apps/gmm/v/ad;->b:Lcom/google/android/apps/gmm/v/ag;

    new-instance v2, Lcom/google/android/apps/gmm/v/af;

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/gmm/v/af;-><init>(Lcom/google/android/apps/gmm/v/f;Z)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/v/ag;->a(Lcom/google/android/apps/gmm/v/af;)V

    .line 88
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/a;->c:Lcom/google/android/apps/gmm/w/b;

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/w/a;->a:Lcom/google/android/apps/gmm/base/activities/c;

    .line 94
    return-void
.end method
