.class public Lcom/google/android/apps/gmm/place/review/c;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/base/l/a/u;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/gmm/place/review/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/place/review/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/pc;Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/apps/gmm/place/review/c;->b:Landroid/content/Context;

    .line 32
    invoke-virtual {p2}, Lcom/google/maps/g/pc;->h()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-nez v0, :cond_4

    if-nez p3, :cond_4

    .line 33
    iget v0, p2, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x200

    const/16 v3, 0x200

    if-ne v0, v3, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    .line 34
    iget-object v0, p2, Lcom/google/maps/g/pc;->l:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/c;->c:Ljava/lang/String;

    .line 39
    :goto_2
    iget v0, p2, Lcom/google/maps/g/pc;->a:I

    and-int/lit16 v0, v0, 0x800

    const/16 v3, 0x800

    if-ne v0, v3, :cond_5

    move v0, v2

    :goto_3
    if-eqz v0, :cond_7

    .line 40
    iget-object v0, p2, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ju;->g()Lcom/google/maps/g/ju;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ju;

    invoke-virtual {v0}, Lcom/google/maps/g/ju;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_6

    :cond_1
    move v0, v2

    :goto_4
    if-nez v0, :cond_7

    .line 41
    iget-object v0, p2, Lcom/google/maps/g/pc;->n:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/ju;->g()Lcom/google/maps/g/ju;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ju;

    iget-object v0, v0, Lcom/google/maps/g/ju;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/hg;->j()Lcom/google/maps/g/hg;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/hg;

    invoke-virtual {v0}, Lcom/google/maps/g/hg;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/review/c;->d:Ljava/lang/String;

    .line 45
    :goto_5
    return-void

    :cond_2
    move v0, v1

    .line 32
    goto :goto_0

    :cond_3
    move v0, v1

    .line 33
    goto :goto_1

    .line 36
    :cond_4
    iput-object v4, p0, Lcom/google/android/apps/gmm/place/review/c;->c:Ljava/lang/String;

    goto :goto_2

    :cond_5
    move v0, v1

    .line 39
    goto :goto_3

    :cond_6
    move v0, v1

    .line 40
    goto :goto_4

    .line 43
    :cond_7
    iput-object v4, p0, Lcom/google/android/apps/gmm/place/review/c;->d:Ljava/lang/String;

    goto :goto_5
.end method


# virtual methods
.method public final a(I)Lcom/google/android/libraries/curvular/cf;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 62
    sget v0, Lcom/google/android/apps/gmm/l;->le:I

    if-ne p1, v0, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c;->c:Ljava/lang/String;

    .line 72
    :goto_0
    const/4 v1, 0x1

    :try_start_0
    invoke-static {v0, v1}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 79
    :try_start_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/c;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 83
    :cond_0
    :goto_1
    return-object v3

    .line 64
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/l;->li:I

    if-ne p1, v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/review/c;->d:Ljava/lang/String;

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/apps/gmm/place/review/c;->a:Ljava/lang/String;

    const-string v0, "Failed to parse report review link: "

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/review/c;->c:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 81
    :catch_1
    move-exception v1

    sget-object v1, Lcom/google/android/apps/gmm/place/review/c;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to resolve intent: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 49
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 50
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    :cond_0
    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 51
    sget v2, Lcom/google/android/apps/gmm/l;->le:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 53
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/review/c;->d:Ljava/lang/String;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    move v0, v1

    :cond_3
    if-nez v0, :cond_4

    .line 54
    sget v0, Lcom/google/android/apps/gmm/l;->li:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    :cond_4
    return-object v3

    :cond_5
    move v2, v0

    .line 50
    goto :goto_0
.end method
