.class public Lcom/google/android/apps/gmm/cardui/e/a;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/o/h/a/od;)Lcom/google/android/apps/gmm/base/g/c;
    .locals 3

    .prologue
    .line 198
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    .line 199
    iget-object v0, p0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    iget-object v2, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v2, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    .line 200
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/o/h/a/hv;I)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 6
    .param p0    # Lcom/google/o/h/a/hv;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 280
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    if-eqz p0, :cond_3

    iget v4, p0, Lcom/google/o/h/a/hv;->a:I

    and-int/lit8 v4, v4, 0x2

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    move v4, v3

    :goto_0
    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/google/o/h/a/hv;->d()Ljava/lang/String;

    move-result-object v1

    iget v0, p0, Lcom/google/o/h/a/hv;->d:I

    invoke-static {v0}, Lcom/google/o/h/a/ia;->a(I)Lcom/google/o/h/a/ia;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/o/h/a/ia;->a:Lcom/google/o/h/a/ia;

    :cond_0
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/o/h/a/ia;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    :cond_1
    :goto_1
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/k;

    const/16 v3, 0xfa

    invoke-direct {v2, v1, v0, p1, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    return-object v2

    :cond_2
    move v4, v2

    goto :goto_0

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-nez v4, :cond_5

    :cond_4
    move v2, v3

    :cond_5
    if-nez v2, :cond_1

    goto :goto_1
.end method

.method public static a(Lcom/google/r/b/a/aje;I)Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 4
    .param p0    # Lcom/google/r/b/a/aje;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 311
    new-instance v2, Lcom/google/android/apps/gmm/base/views/c/k;

    .line 312
    if-eqz p0, :cond_0

    iget v0, p0, Lcom/google/r/b/a/aje;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_3

    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    .line 313
    :goto_1
    if-eqz p0, :cond_4

    iget v0, p0, Lcom/google/r/b/a/aje;->h:I

    invoke-static {v0}, Lcom/google/r/b/a/ajh;->a(I)Lcom/google/r/b/a/ajh;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/r/b/a/ajh;->a:Lcom/google/r/b/a/ajh;

    :cond_1
    invoke-static {v0}, Lcom/google/android/apps/gmm/base/views/b/a;->a(Lcom/google/r/b/a/ajh;)Lcom/google/android/apps/gmm/util/webimageview/b;

    move-result-object v0

    :goto_2
    const/16 v3, 0xfa

    invoke-direct {v2, v1, v0, p1, v3}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;II)V

    return-object v2

    .line 312
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/r/b/a/aje;->g()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 313
    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/util/webimageview/b;->a:Lcom/google/android/apps/gmm/util/webimageview/b;

    goto :goto_2
.end method

.method public static a(Ljava/lang/CharSequence;Lcom/google/o/h/a/qj;Landroid/content/res/Resources;)Ljava/lang/CharSequence;
    .locals 6
    .param p0    # Ljava/lang/CharSequence;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/o/h/a/qj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 100
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 101
    iget-object v0, p1, Lcom/google/o/h/a/qj;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-object p0

    .line 105
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 106
    invoke-virtual {p1}, Lcom/google/o/h/a/qj;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/qm;

    .line 107
    iget v1, v0, Lcom/google/o/h/a/qm;->b:I

    if-ltz v1, :cond_2

    iget v1, v0, Lcom/google/o/h/a/qm;->b:I

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 108
    iget v1, v0, Lcom/google/o/h/a/qm;->c:I

    iget v4, v0, Lcom/google/o/h/a/qm;->b:I

    if-lt v1, v4, :cond_2

    iget v1, v0, Lcom/google/o/h/a/qm;->c:I

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v4

    if-gt v1, v4, :cond_2

    .line 113
    iget v1, v0, Lcom/google/o/h/a/qm;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v4, 0x4

    if-ne v1, v4, :cond_3

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_2

    .line 126
    iget-object v1, v0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    if-nez v1, :cond_4

    invoke-static {}, Lcom/google/o/h/a/dt;->d()Lcom/google/o/h/a/dt;

    move-result-object v1

    :goto_3
    invoke-static {p2, v1}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Landroid/content/res/Resources;Lcom/google/o/h/a/dt;)Ljava/lang/Integer;

    move-result-object v1

    .line 129
    if-eqz v1, :cond_2

    .line 130
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {v4, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 133
    iget v1, v0, Lcom/google/o/h/a/qm;->b:I

    iget v0, v0, Lcom/google/o/h/a/qm;->c:I

    const/16 v5, 0x11

    .line 132
    invoke-virtual {v2, v4, v1, v0, v5}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    .line 113
    :cond_3
    const/4 v1, 0x0

    goto :goto_2

    .line 126
    :cond_4
    iget-object v1, v0, Lcom/google/o/h/a/qm;->d:Lcom/google/o/h/a/dt;

    goto :goto_3

    :cond_5
    move-object p0, v2

    .line 135
    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Lcom/google/o/h/a/dt;)Ljava/lang/Integer;
    .locals 5
    .param p1    # Lcom/google/o/h/a/dt;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 140
    if-nez p1, :cond_0

    move-object v0, v1

    .line 160
    :goto_0
    return-object v0

    .line 143
    :cond_0
    iget v0, p1, Lcom/google/o/h/a/dt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    .line 144
    sget-object v4, Lcom/google/android/apps/gmm/cardui/e/b;->a:[I

    iget v0, p1, Lcom/google/o/h/a/dt;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/dw;->a(I)Lcom/google/o/h/a/dw;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/o/h/a/dw;->a:Lcom/google/o/h/a/dw;

    :cond_1
    invoke-virtual {v0}, Lcom/google/o/h/a/dw;->ordinal()I

    move-result v0

    aget v0, v4, v0

    packed-switch v0, :pswitch_data_0

    .line 157
    :cond_2
    iget v0, p1, Lcom/google/o/h/a/dt;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v4, 0x2

    if-ne v0, v4, :cond_4

    move v0, v2

    :goto_2
    if-eqz v0, :cond_5

    .line 158
    iget v0, p1, Lcom/google/o/h/a/dt;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_3
    move v0, v3

    .line 143
    goto :goto_1

    .line 146
    :pswitch_0
    sget v0, Lcom/google/android/apps/gmm/d;->Q:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 148
    :pswitch_1
    sget v0, Lcom/google/android/apps/gmm/d;->N:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 150
    :pswitch_2
    sget v0, Lcom/google/android/apps/gmm/d;->M:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 152
    :pswitch_3
    sget v0, Lcom/google/android/apps/gmm/d;->X:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v3

    .line 157
    goto :goto_2

    :cond_5
    move-object v0, v1

    .line 160
    goto :goto_0

    .line 144
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(IILcom/google/b/f/t;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 231
    new-instance v0, Lcom/google/e/a/a/a/b;

    sget-object v1, Lcom/google/b/f/a/a;->a:Lcom/google/e/a/a/a/d;

    invoke-direct {v0, v1}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 233
    const/4 v1, 0x7

    const/4 v2, 0x0

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 234
    const/4 v1, 0x1

    int-to-long v2, p0

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 235
    const/4 v1, 0x5

    int-to-long v2, p1

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 236
    const/4 v1, 0x2

    iget v2, p2, Lcom/google/b/f/t;->gy:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v1, v2}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 237
    invoke-static {v0}, Lcom/google/android/apps/gmm/cardui/e/a;->a(Lcom/google/e/a/a/a/b;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/e/a/a/a/b;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 249
    :try_start_0
    const-string v0, "0"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 250
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/e/a/a/a/b;->b(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const/16 v2, 0xb

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 255
    :goto_0
    return-object v0

    .line 250
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 252
    :catch_0
    move-exception v0

    .line 253
    const-string v1, "CardUiUtil"

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Invalid clickTrackingCgi is generated"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 255
    const-string v0, ""

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/br;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 177
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 178
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/br;

    .line 179
    invoke-virtual {v0}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 180
    invoke-virtual {v0}, Lcom/google/o/h/a/br;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/iv;

    iget-object v0, v0, Lcom/google/o/h/a/iv;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ir;->d()Lcom/google/o/h/a/ir;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ir;

    iget v1, v0, Lcom/google/o/h/a/ir;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v5, 0x4

    if-ne v1, v5, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    iget-object v0, v0, Lcom/google/o/h/a/ir;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/od;->d()Lcom/google/o/h/a/od;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/od;

    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    iget-object v0, v0, Lcom/google/o/h/a/od;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/ads;->i()Lcom/google/r/b/a/ads;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ads;

    iget-object v5, v1, Lcom/google/android/apps/gmm/base/g/g;->b:Lcom/google/android/apps/gmm/base/g/i;

    iput-object v0, v5, Lcom/google/android/apps/gmm/base/g/i;->g:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->v:Z

    iput-boolean v0, v1, Lcom/google/android/apps/gmm/base/g/g;->e:Z

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    .line 181
    :goto_2
    if-eqz v0, :cond_0

    .line 182
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move v1, v2

    .line 180
    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 186
    :cond_3
    return-object v3
.end method
