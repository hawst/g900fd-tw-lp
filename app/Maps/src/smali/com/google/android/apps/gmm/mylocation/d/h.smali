.class public Lcom/google/android/apps/gmm/mylocation/d/h;
.super Lcom/google/android/apps/gmm/mylocation/d/f;
.source "PG"


# instance fields
.field private d:F

.field private e:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private f:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private g:Lcom/google/android/apps/gmm/map/t/q;

.field private h:Lcom/google/android/apps/gmm/map/t/q;

.field private i:Lcom/google/android/apps/gmm/mylocation/d/d;

.field private j:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;ZZ)V
    .locals 1

    .prologue
    .line 78
    invoke-direct/range {p0 .. p6}, Lcom/google/android/apps/gmm/mylocation/d/f;-><init>(Lcom/google/android/apps/gmm/map/util/b/g;Landroid/content/res/Resources;Lcom/google/android/apps/gmm/v/ad;Lcom/google/android/apps/gmm/map/internal/vector/gl/v;ZZ)V

    .line 49
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->d:F

    .line 79
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/mylocation/d/h;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/b;

    .line 208
    check-cast v0, Lcom/google/android/apps/gmm/map/b/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/b/f;->a()Lcom/google/android/apps/gmm/v/aa;

    move-result-object v0

    iget-boolean v2, v0, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    int-to-byte v2, p1

    iput-byte v2, v0, Lcom/google/android/apps/gmm/v/aa;->w:B

    goto :goto_0

    .line 210
    :cond_1
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/res/Resources;Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/b;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/high16 v7, 0x42b80000    # 92.0f

    const/4 v6, 0x4

    const/4 v1, 0x1

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->c:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(Z)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->g:Lcom/google/android/apps/gmm/map/t/q;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->c:Z

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/mylocation/d/e;->b(Z)Lcom/google/android/apps/gmm/map/t/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->h:Lcom/google/android/apps/gmm/map/t/q;

    .line 91
    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    if-eqz p2, :cond_1

    sget v0, Lcom/google/android/apps/gmm/f;->ay:I

    :goto_0
    const-string v4, "Navigation chevron"

    sget-object v5, Lcom/google/android/apps/gmm/map/t/l;->F:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v3, v0, v4, v6, v5}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 92
    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->a:Lcom/google/android/apps/gmm/mylocation/d/e;

    if-eqz p2, :cond_2

    sget v0, Lcom/google/android/apps/gmm/f;->aA:I

    :goto_1
    const-string v4, "Navigation chevron disc"

    sget-object v5, Lcom/google/android/apps/gmm/map/t/l;->E:Lcom/google/android/apps/gmm/map/t/l;

    invoke-virtual {v3, v0, v4, v6, v5}, Lcom/google/android/apps/gmm/mylocation/d/e;->a(ILjava/lang/String;ILcom/google/android/apps/gmm/map/t/k;)Lcom/google/android/apps/gmm/mylocation/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    .line 93
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->d:F

    .line 94
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 98
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, v7

    iget-object v3, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget v3, v3, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->j:F

    .line 102
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    sget-object v3, Lcom/google/android/apps/gmm/map/t/r;->d:Lcom/google/android/apps/gmm/map/t/r;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v4, :cond_0

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_0
    iput-object v3, v0, Lcom/google/android/apps/gmm/map/t/q;->h:Lcom/google/android/apps/gmm/map/t/r;

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 104
    const/4 v0, 0x5

    new-array v3, v0, [Lcom/google/android/apps/gmm/map/b/b;

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->g:Lcom/google/android/apps/gmm/map/t/q;

    aput-object v0, v3, v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->h:Lcom/google/android/apps/gmm/map/t/q;

    aput-object v0, v3, v1

    const/4 v0, 0x2

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    aput-object v4, v3, v0

    const/4 v0, 0x3

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->i:Lcom/google/android/apps/gmm/mylocation/d/d;

    aput-object v4, v3, v0

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    aput-object v0, v3, v6

    .line 118
    if-nez v3, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 91
    :cond_1
    sget v0, Lcom/google/android/apps/gmm/f;->ax:I

    goto :goto_0

    .line 92
    :cond_2
    sget v0, Lcom/google/android/apps/gmm/f;->az:I

    goto :goto_1

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget v0, v0, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v0, v0

    mul-float/2addr v0, v7

    iput v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->j:F

    goto :goto_2

    .line 118
    :cond_4
    array-length v4, v3

    if-ltz v4, :cond_5

    move v0, v1

    :goto_3
    if-nez v0, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    const-wide/16 v0, 0x5

    int-to-long v6, v4

    add-long/2addr v0, v6

    div-int/lit8 v2, v4, 0xa

    int-to-long v4, v2

    add-long/2addr v0, v4

    const-wide/32 v4, 0x7fffffff

    cmp-long v2, v0, v4

    if-lez v2, :cond_7

    const v0, 0x7fffffff

    :goto_4
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static {v1, v3}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 119
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 124
    return-object v1

    .line 118
    :cond_7
    const-wide/32 v4, -0x80000000

    cmp-long v2, v0, v4

    if-gez v2, :cond_8

    const/high16 v0, -0x80000000

    goto :goto_4

    :cond_8
    long-to-int v0, v0

    goto :goto_4
.end method

.method public final a(Lcom/google/android/apps/gmm/mylocation/g/a;Lcom/google/android/apps/gmm/map/f/o;)V
    .locals 10

    .prologue
    const/16 v3, 0xff

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 139
    iget-object v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_7

    move v0, v1

    :goto_0
    if-eqz v0, :cond_8

    .line 140
    invoke-direct {p0, v3}, Lcom/google/android/apps/gmm/mylocation/d/h;->a(I)V

    .line 146
    iget-object v4, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 147
    iget v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->e:I

    int-to-double v6, v0

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v8

    mul-double/2addr v6, v8

    double-to-float v5, v6

    .line 148
    iget v6, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->j:F

    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    if-nez v0, :cond_9

    const/4 v0, 0x0

    :goto_1
    mul-float/2addr v0, v6

    .line 150
    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 151
    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget v7, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->d:F

    mul-float/2addr v7, v0

    invoke-virtual {v6, v7}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(F)V

    .line 153
    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    if-eqz v6, :cond_0

    .line 154
    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v6, v4}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 155
    iget-object v6, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->f:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget v7, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->d:F

    mul-float/2addr v0, v7

    invoke-virtual {v6, v0}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(F)V

    .line 158
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->g:Z

    if-eqz v0, :cond_2

    .line 159
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    iget v6, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->d:F

    neg-float v6, v6

    iget-boolean v7, v0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v7, :cond_1

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_1
    iput v6, v0, Lcom/google/android/apps/gmm/map/t/q;->g:F

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->g:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->g:Lcom/google/android/apps/gmm/map/t/q;

    iget-boolean v6, v0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v6, :cond_3

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_3
    iput v5, v0, Lcom/google/android/apps/gmm/map/t/q;->d:F

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 196
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->h:Lcom/google/android/apps/gmm/map/t/q;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->h:Lcom/google/android/apps/gmm/map/t/q;

    iget-boolean v4, v0, Lcom/google/android/apps/gmm/map/t/q;->t:Z

    if-eqz v4, :cond_4

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_4
    iput v5, v0, Lcom/google/android/apps/gmm/map/t/q;->d:F

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/t/q;->f:Z

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/map/t/q;->n:Z

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->g:Lcom/google/android/apps/gmm/map/t/q;

    .line 201
    invoke-virtual {v0, p2}, Lcom/google/android/apps/gmm/map/t/q;->a(Lcom/google/android/apps/gmm/map/f/o;)F

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->e:Lcom/google/android/apps/gmm/mylocation/d/d;

    invoke-virtual {v4, p2}, Lcom/google/android/apps/gmm/mylocation/d/d;->a(Lcom/google/android/apps/gmm/map/f/o;)F

    move-result v4

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_a

    move v0, v1

    .line 202
    :goto_2
    iget-object v4, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->g:Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v0, :cond_b

    move v1, v2

    :goto_3
    iget-boolean v5, v4, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v5, :cond_5

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_5
    int-to-byte v1, v1

    iput-byte v1, v4, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 203
    iget-object v1, p0, Lcom/google/android/apps/gmm/mylocation/d/h;->h:Lcom/google/android/apps/gmm/map/t/q;

    if-eqz v0, :cond_c

    :goto_4
    iget-boolean v0, v1, Lcom/google/android/apps/gmm/v/aa;->t:Z

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    :cond_6
    int-to-byte v0, v2

    iput-byte v0, v1, Lcom/google/android/apps/gmm/v/aa;->w:B

    .line 204
    :goto_5
    return-void

    :cond_7
    move v0, v2

    .line 139
    goto/16 :goto_0

    .line 143
    :cond_8
    invoke-direct {p0, v2}, Lcom/google/android/apps/gmm/mylocation/d/h;->a(I)V

    goto :goto_5

    .line 148
    :cond_9
    iget v0, v0, Lcom/google/android/apps/gmm/mylocation/d/d;->k:I

    int-to-float v0, v0

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v0, v7

    iget v7, p1, Lcom/google/android/apps/gmm/mylocation/g/a;->j:F

    mul-float/2addr v0, v7

    goto/16 :goto_1

    :cond_a
    move v0, v2

    .line 201
    goto :goto_2

    :cond_b
    move v1, v3

    .line 202
    goto :goto_3

    :cond_c
    move v2, v3

    .line 203
    goto :goto_4
.end method
