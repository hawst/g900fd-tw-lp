.class public abstract Lcom/google/android/apps/gmm/navigation/i/i;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ai;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 370
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/i;->a:Landroid/content/Context;

    .line 371
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/i/i;->b:Ljava/util/List;

    .line 372
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/a/af;)Lcom/google/android/apps/gmm/map/r/a/ai;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 411
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/i;->b:Ljava/util/List;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 419
    :goto_0
    return-object v0

    .line 414
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/i;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ai;

    .line 415
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/r/a/ai;->a:Lcom/google/maps/g/a/ac;

    iget v2, v2, Lcom/google/maps/g/a/ac;->b:I

    invoke-static {v2}, Lcom/google/maps/g/a/af;->a(I)Lcom/google/maps/g/a/af;

    move-result-object v2

    if-nez v2, :cond_2

    sget-object v2, Lcom/google/maps/g/a/af;->a:Lcom/google/maps/g/a/af;

    :cond_2
    if-ne v2, p1, :cond_1

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 419
    goto :goto_0
.end method

.method public abstract a()Lcom/google/maps/g/a/ez;
.end method

.method public b()Lcom/google/maps/g/a/fb;
    .locals 1

    .prologue
    .line 383
    sget-object v0, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    return-object v0
.end method

.method public c()Lcom/google/maps/g/a/fd;
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    return-object v0
.end method

.method public abstract d()Ljava/lang/String;
.end method
