.class final Lcom/google/android/apps/gmm/map/c/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/c/c;Landroid/content/res/Resources;II)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/c/d;->a:Landroid/content/res/Resources;

    .line 96
    iput p3, p0, Lcom/google/android/apps/gmm/map/c/d;->b:I

    .line 97
    iput p4, p0, Lcom/google/android/apps/gmm/map/c/d;->c:I

    .line 98
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 102
    if-nez p1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 106
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 107
    goto :goto_0

    .line 110
    :cond_2
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/c/d;

    if-eqz v2, :cond_0

    .line 114
    check-cast p1, Lcom/google/android/apps/gmm/map/c/d;

    .line 115
    iget v2, p0, Lcom/google/android/apps/gmm/map/c/d;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/c/d;->b:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/apps/gmm/map/c/d;->c:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/c/d;->c:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/c/d;->a:Landroid/content/res/Resources;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/c/d;->a:Landroid/content/res/Resources;

    .line 117
    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 122
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/c/d;->a:Landroid/content/res/Resources;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/map/c/d;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/apps/gmm/map/c/d;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
