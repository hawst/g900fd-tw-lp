.class public Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/legacy/a/c/b/o;


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field final b:[F

.field c:[[F

.field d:Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/g;

.field e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

.field f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

.field g:Lcom/google/android/apps/gmm/v/ci;

.field h:[Lcom/google/android/apps/gmm/map/b/a/y;

.field i:Lcom/google/android/apps/gmm/map/l/b;

.field j:Lcom/google/android/apps/gmm/map/b/a/ay;

.field k:Lcom/google/android/apps/gmm/map/b/a/ay;

.field l:Lcom/google/android/apps/gmm/map/b/a/ay;

.field m:Lcom/google/android/apps/gmm/map/b/a/ay;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;II)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/high16 v4, 0x437f0000    # 255.0f

    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    const/4 v0, 0x4

    new-array v0, v0, [[F

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    aput-object v1, v0, v5

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    aput-object v1, v0, v3

    new-array v1, v2, [F

    fill-array-data v1, :array_2

    aput-object v1, v0, v2

    new-array v1, v2, [F

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->c:[[F

    .line 163
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 168
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 173
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 178
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 183
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v2

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->h:[Lcom/google/android/apps/gmm/map/b/a/y;

    .line 189
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/b/a/y;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v3

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v2

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    aput-object v1, v0, v6

    .line 195
    new-instance v0, Lcom/google/android/apps/gmm/map/l/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/l/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->i:Lcom/google/android/apps/gmm/map/l/b;

    .line 200
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->j:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 206
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->k:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->l:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 218
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/map/b/a/ay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->m:Lcom/google/android/apps/gmm/map/b/a/ay;

    .line 229
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 230
    const/16 v0, 0x812

    .line 233
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->g:I

    .line 230
    invoke-static {p3, v0, v3, v1}, Lcom/google/android/apps/gmm/map/internal/vector/gl/k;->a(IIZI)Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->e:Lcom/google/android/apps/gmm/map/internal/vector/gl/k;

    .line 235
    const/4 v0, 0x4

    new-array v0, v0, [F

    .line 236
    ushr-int/lit8 v1, p2, 0x10

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v5

    ushr-int/lit8 v1, p2, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v3

    .line 237
    and-int/lit16 v1, p2, 0xff

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v2

    ushr-int/lit8 v1, p2, 0x18

    int-to-float v1, v1

    div-float/2addr v1, v4

    aput v1, v0, v6

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->b:[F

    .line 238
    return-void

    .line 132
    nop

    :array_0
    .array-data 4
        0x3e800000    # 0.25f
        0x3ec00000    # 0.375f
    .end array-data

    :array_1
    .array-data 4
        0x3f400000    # 0.75f
        0x3ec00000    # 0.375f
    .end array-data

    :array_2
    .array-data 4
        0x3f400000    # 0.75f
        0x3f200000    # 0.625f
    .end array-data

    :array_3
    .array-data 4
        0x3e800000    # 0.25f
        0x3f200000    # 0.625f
    .end array-data
.end method

.method static a(I)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 436
    if-gtz p0, :cond_1

    .line 447
    :cond_0
    :goto_0
    return v0

    .line 440
    :cond_1
    const/4 v1, 0x3

    :goto_1
    if-lez v1, :cond_0

    .line 441
    const/4 v2, 0x1

    shl-int/2addr v2, v1

    .line 442
    add-int/lit8 v3, v2, -0x1

    sub-int v3, p0, v3

    rem-int v2, v3, v2

    if-nez v2, :cond_2

    move v0, v1

    .line 443
    goto :goto_0

    .line 440
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method static a(Lcom/google/android/apps/gmm/map/internal/c/az;F)I
    .locals 9

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v3, 0x1

    const/4 v7, 0x3

    const/4 v0, 0x0

    .line 633
    cmpg-float v1, p1, v4

    if-ltz v1, :cond_0

    if-nez p0, :cond_1

    :cond_0
    move v1, v3

    .line 635
    :goto_0
    if-eqz v1, :cond_2

    .line 651
    :goto_1
    return v0

    :cond_1
    move v1, v0

    .line 633
    goto :goto_0

    .line 643
    :cond_2
    const/high16 v1, 0x42c00000    # 96.0f

    invoke-static {p1}, Lcom/google/android/apps/gmm/map/b/a/x;->a(F)F

    move-result v2

    mul-float/2addr v1, v2

    .line 645
    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    div-float v2, v4, v1

    .line 647
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/az;->a:Lcom/google/android/apps/gmm/map/b/a/ab;

    .line 649
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/ab;->e()F

    move-result v4

    .line 651
    invoke-static {v2}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v2}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v4}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v4}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v1

    if-nez v1, :cond_3

    cmpg-float v1, v4, v5

    if-lez v1, :cond_3

    cmpg-float v1, v2, v5

    if-gtz v1, :cond_5

    :cond_3
    move v1, v3

    :goto_2
    if-eqz v1, :cond_6

    :cond_4
    mul-int/lit8 v0, v0, 0x4

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_2

    :cond_6
    mul-float/2addr v4, v2

    move v1, v0

    move v2, v0

    :goto_3
    if-ge v1, v7, :cond_8

    shl-int v5, v3, v1

    int-to-float v5, v5

    mul-float/2addr v5, v4

    const v6, 0x3eaaaaab

    cmpg-float v6, v5, v6

    if-gez v6, :cond_7

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    float-to-int v0, v5

    add-int/lit8 v1, v0, 0x0

    move v8, v0

    move v0, v1

    move v1, v8

    :goto_4
    add-int/lit8 v3, v2, 0x1

    if-ge v3, v7, :cond_4

    add-int/lit8 v2, v1, 0x1

    add-int/2addr v0, v2

    add-int/lit8 v1, v3, 0x1

    if-ge v1, v7, :cond_4

    const/4 v1, 0x2

    :goto_5
    if-ge v1, v7, :cond_4

    shl-int/lit8 v2, v2, 0x1

    add-int v3, v0, v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v3

    goto :goto_5

    :cond_8
    move v1, v0

    goto :goto_4
.end method

.method static a(ILcom/google/android/apps/gmm/map/internal/c/az;)Z
    .locals 1

    .prologue
    .line 791
    const/16 v0, 0xe

    if-le p0, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/c/az;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 822
    const/4 v0, 0x0

    .line 823
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    if-eqz v1, :cond_0

    .line 824
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->f:Lcom/google/android/apps/gmm/map/internal/vector/gl/j;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/vector/gl/j;->a:I

    add-int/lit8 v0, v0, 0x0

    .line 826
    :cond_0
    return v0
.end method

.method a(FIFLcom/google/android/apps/gmm/map/b/a/ay;)V
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/legacy/a/c/b/af;->i:Lcom/google/android/apps/gmm/map/l/b;

    int-to-float v1, p2

    add-float/2addr v1, p3

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1, p4}, Lcom/google/android/apps/gmm/map/l/b;->a(FLcom/google/android/apps/gmm/map/b/a/ay;)I

    .line 452
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 831
    const/16 v0, 0x50

    return v0
.end method
