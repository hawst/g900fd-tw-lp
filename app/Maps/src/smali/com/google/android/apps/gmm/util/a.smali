.class public Lcom/google/android/apps/gmm/util/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/s;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    invoke-static {}, Lcom/google/b/c/cv;->h()Lcom/google/b/c/cx;

    move-result-object v0

    .line 42
    sget-object v1, Lcom/google/maps/g/s;->a:Lcom/google/maps/g/s;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cx;->b(Ljava/lang/Object;)Lcom/google/b/c/cx;

    .line 45
    invoke-virtual {v0}, Lcom/google/b/c/cx;->a()Lcom/google/b/c/cv;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/util/a;->a:Ljava/util/List;

    .line 46
    return-void
.end method

.method public static a(Landroid/content/Context;ZI)Landroid/graphics/drawable/Drawable;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 115
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 116
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/apps/gmm/util/a;->a(Landroid/text/SpannableStringBuilder;Landroid/content/res/Resources;Z)V

    .line 122
    const-string v1, "\u200b"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 124
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 125
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 127
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/apps/gmm/e;->c:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 126
    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 128
    invoke-virtual {v1, v4, v4, p2, v4}, Landroid/widget/TextView;->setPaddingRelative(IIII)V

    .line 129
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    invoke-static {v1}, Lcom/google/android/apps/gmm/util/r;->e(Landroid/view/View;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {v0, v4, v4, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 133
    return-object v0
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Landroid/content/res/Resources;Z)V
    .locals 11

    .prologue
    const/16 v10, 0x21

    .line 59
    sget v4, Lcom/google/android/apps/gmm/e;->c:I

    if-eqz p2, :cond_0

    sget v0, Lcom/google/android/apps/gmm/l;->aQ:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {p0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    sget v0, Lcom/google/android/apps/gmm/d;->D:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    sget v0, Lcom/google/android/apps/gmm/d;->a:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sget v4, Lcom/google/android/apps/gmm/e;->b:I

    invoke-virtual {p1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    sget v5, Lcom/google/android/apps/gmm/e;->a:I

    invoke-virtual {p1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    new-instance v8, Landroid/text/style/TypefaceSpan;

    const-string v9, "sans-serif"

    invoke-direct {v8, v9}, Landroid/text/style/TypefaceSpan;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v8, v6, v7, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v8, Landroid/text/style/StyleSpan;

    const/4 v9, 0x1

    invoke-direct {v8, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p0, v8, v6, v7, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v8, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v8, v0}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    invoke-virtual {p0, v8, v6, v7, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    new-instance v0, Lcom/google/android/apps/gmm/util/i;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/util/i;-><init>(Ljava/lang/String;IIII)V

    invoke-virtual {p0, v0, v6, v7, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 60
    return-void

    .line 59
    :cond_0
    sget v0, Lcom/google/android/apps/gmm/l;->aG:I

    goto :goto_0
.end method

.method public static a(Lcom/google/e/a/a/a/b;)V
    .locals 5

    .prologue
    const/16 v4, 0x17

    .line 140
    invoke-static {p0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->h(Lcom/google/e/a/a/a/b;I)V

    .line 141
    sget-object v0, Lcom/google/android/apps/gmm/util/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/s;

    .line 143
    iget v0, v0, Lcom/google/maps/g/s;->b:I

    .line 142
    int-to-long v2, v0

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 145
    :cond_0
    return-void
.end method

.method public static b(Lcom/google/e/a/a/a/b;)V
    .locals 5

    .prologue
    const/16 v4, 0xd

    .line 152
    invoke-static {p0, v4}, Lcom/google/android/apps/gmm/shared/c/b/a;->h(Lcom/google/e/a/a/a/b;I)V

    .line 154
    sget-object v0, Lcom/google/android/apps/gmm/util/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/s;

    .line 156
    iget v0, v0, Lcom/google/maps/g/s;->b:I

    .line 155
    int-to-long v2, v0

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 158
    :cond_0
    return-void
.end method
