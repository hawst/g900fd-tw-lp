.class public Lcom/google/android/apps/gmm/p/d/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/p/d/a;


# instance fields
.field private final a:D

.field private final b:D


# direct methods
.method public constructor <init>(DD)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-wide p1, p0, Lcom/google/android/apps/gmm/p/d/b;->a:D

    .line 41
    iput-wide p3, p0, Lcom/google/android/apps/gmm/p/d/b;->b:D

    .line 42
    return-void
.end method


# virtual methods
.method public final a()D
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/d/b;->a:D

    return-wide v0
.end method

.method public final a(D)D
    .locals 7

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/google/android/apps/gmm/p/d/b;->a:D

    iget-wide v4, p0, Lcom/google/android/apps/gmm/p/d/b;->b:D

    move-wide v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/gmm/p/d/d;->a(DDD)D

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 56
    const-class v0, Lcom/google/android/apps/gmm/p/d/b;

    new-instance v1, Lcom/google/b/a/ah;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v0, "observed"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/b;->a:D

    .line 57
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "sigma"

    iget-wide v2, p0, Lcom/google/android/apps/gmm/p/d/b;->b:D

    .line 58
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
