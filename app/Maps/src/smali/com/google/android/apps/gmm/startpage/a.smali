.class public Lcom/google/android/apps/gmm/startpage/a;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/apps/gmm/startpage/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/google/android/apps/gmm/startpage/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/startpage/c;)V
    .locals 0

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    .line 87
    return-void
.end method

.method private static a(Lcom/google/o/h/a/bi;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)D
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/o/h/a/bi;",
            "Ljava/util/List",
            "<",
            "Lcom/google/d/a/a/ju;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/r;",
            ")D"
        }
    .end annotation

    .prologue
    .line 316
    sget-object v0, Lcom/google/android/apps/gmm/startpage/b;->a:[I

    invoke-virtual {p0}, Lcom/google/o/h/a/bi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 327
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    .line 318
    :pswitch_0
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/b/a/r;->c()D

    move-result-wide v0

    goto :goto_0

    .line 320
    :pswitch_1
    invoke-static {p1}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;)D

    move-result-wide v0

    goto :goto_0

    .line 322
    :pswitch_2
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/map/b/a/r;->c()D

    move-result-wide v0

    invoke-static {p1}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;)D

    move-result-wide v2

    add-double/2addr v0, v2

    .line 323
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/startpage/a;->b(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v2

    sub-double/2addr v0, v2

    goto :goto_0

    .line 325
    :pswitch_3
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/startpage/a;->b(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v0

    goto :goto_0

    .line 316
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Ljava/util/List;)D
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/d/a/a/ju;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 336
    const-wide/16 v0, 0x0

    .line 337
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    .line 338
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/r;->a(Lcom/google/d/a/a/ju;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/r;->c()D

    move-result-wide v0

    add-double/2addr v0, v2

    move-wide v2, v0

    .line 339
    goto :goto_0

    .line 340
    :cond_0
    return-wide v2
.end method

.method public static a(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/startpage/a;
    .locals 8
    .param p1    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 91
    if-nez p1, :cond_0

    move-object v4, v0

    .line 93
    :goto_0
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a()Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-result-object v1

    .line 94
    if-nez v1, :cond_1

    move-object v5, v0

    .line 95
    :goto_1
    if-nez p1, :cond_2

    const/high16 v6, 0x41700000    # 15.0f

    .line 97
    :goto_2
    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v1

    .line 98
    if-eqz v1, :cond_3

    .line 99
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v7

    .line 100
    :goto_3
    new-instance v0, Lcom/google/android/apps/gmm/startpage/a;

    new-instance v1, Lcom/google/android/apps/gmm/startpage/c;

    invoke-interface {p0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/startpage/c;-><init>(JLcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/l;FLcom/google/o/b/a/v;)V

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/startpage/a;-><init>(Lcom/google/android/apps/gmm/startpage/c;)V

    return-object v0

    .line 92
    :cond_0
    invoke-static {p1}, Lcom/google/android/apps/gmm/map/w;->b(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v4

    goto :goto_0

    .line 94
    :cond_1
    iget-object v5, v1, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    goto :goto_1

    .line 96
    :cond_2
    iget-object v1, p1, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v6, v1, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    goto :goto_2

    :cond_3
    move-object v7, v0

    .line 99
    goto :goto_3
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)Z
    .locals 6

    .prologue
    const-wide v4, 0x40f86a0000000000L    # 100000.0

    .line 163
    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v0, v4

    double-to-int v0, v0

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v0, v4

    double-to-int v0, v0

    iget-wide v2, p1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    mul-double/2addr v2, v4

    double-to-int v1, v2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;)Z
    .locals 4
    .param p0    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p1    # Lcom/google/android/apps/gmm/map/b/a/r;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 149
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v0

    .line 152
    :cond_1
    if-eqz p0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    move v0, v1

    .line 153
    goto :goto_0

    .line 155
    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/r;->a:Lcom/google/android/apps/gmm/map/b/a/q;

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/r;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    .line 156
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/map/b/a/q;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/gmm/startpage/c;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 136
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 137
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;)Z

    .line 138
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    .line 139
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/c;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/c;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    .line 140
    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget v2, v2, Lcom/google/android/apps/gmm/startpage/c;->d:F

    iget v3, p1, Lcom/google/android/apps/gmm/startpage/c;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    .line 142
    :cond_2
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    .line 143
    return v0

    :cond_3
    move v2, v0

    .line 140
    goto :goto_0
.end method

.method private static a(Ljava/util/List;F)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/bm;",
            ">;F)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 379
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 387
    :goto_0
    return v0

    .line 382
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bm;

    .line 383
    iget v3, v0, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_4

    move v3, v1

    :goto_1
    if-eqz v3, :cond_2

    iget v3, v0, Lcom/google/o/h/a/bm;->b:F

    cmpg-float v3, v3, p1

    if-gtz v3, :cond_6

    :cond_2
    iget v3, v0, Lcom/google/o/h/a/bm;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v5, 0x2

    if-ne v3, v5, :cond_5

    move v3, v1

    :goto_2
    if-eqz v3, :cond_3

    iget v0, v0, Lcom/google/o/h/a/bm;->c:F

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_6

    :cond_3
    move v0, v1

    :goto_3
    if-eqz v0, :cond_1

    move v0, v1

    .line 384
    goto :goto_0

    :cond_4
    move v3, v2

    .line 383
    goto :goto_1

    :cond_5
    move v3, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    .line 387
    goto :goto_0
.end method

.method private static a(Ljava/util/List;I)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/at;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 253
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 261
    :goto_0
    return v0

    .line 256
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/at;

    .line 257
    iget v3, v0, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v3, v3, 0x1

    if-ne v3, v1, :cond_4

    move v3, v1

    :goto_1
    if-eqz v3, :cond_2

    iget v3, v0, Lcom/google/o/h/a/at;->b:I

    if-gt v3, p1, :cond_6

    :cond_2
    iget v3, v0, Lcom/google/o/h/a/at;->a:I

    and-int/lit8 v3, v3, 0x2

    const/4 v5, 0x2

    if-ne v3, v5, :cond_5

    move v3, v1

    :goto_2
    if-eqz v3, :cond_3

    iget v0, v0, Lcom/google/o/h/a/at;->c:I

    if-gt p1, v0, :cond_6

    :cond_3
    move v0, v1

    :goto_3
    if-eqz v0, :cond_1

    move v0, v1

    .line 258
    goto :goto_0

    :cond_4
    move v3, v2

    .line 257
    goto :goto_1

    :cond_5
    move v3, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v0, v2

    .line 261
    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)Z
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/bg;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/r;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 272
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 280
    :goto_0
    return v0

    .line 275
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/bg;

    .line 276
    if-nez p1, :cond_2

    move v0, v3

    :goto_1
    if-eqz v0, :cond_1

    move v0, v2

    .line 277
    goto :goto_0

    .line 276
    :cond_2
    iget v1, v0, Lcom/google/o/h/a/bg;->c:I

    invoke-static {v1}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/o/h/a/bi;->a:Lcom/google/o/h/a/bi;

    :cond_3
    iget-object v5, v0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-static {v1, v5, p1}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/o/h/a/bi;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v6

    iget v1, v0, Lcom/google/o/h/a/bg;->d:I

    invoke-static {v1}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/google/o/h/a/bi;->a:Lcom/google/o/h/a/bi;

    :cond_4
    iget-object v5, v0, Lcom/google/o/h/a/bg;->b:Ljava/util/List;

    invoke-static {v1, v5, p1}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/o/h/a/bi;Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v8

    div-double v10, v6, v8

    const-wide/high16 v12, 0x4059000000000000L    # 100.0

    mul-double/2addr v10, v12

    invoke-static {v10, v11}, Ljava/lang/Double;->isNaN(D)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v5, Lcom/google/android/apps/gmm/startpage/a;->a:Ljava/lang/String;

    const-string v1, "Cache cannot be used because value is NaN, numeratorArea="

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iget v1, v0, Lcom/google/o/h/a/bg;->c:I

    invoke-static {v1}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v1

    if-nez v1, :cond_5

    sget-object v1, Lcom/google/o/h/a/bi;->a:Lcom/google/o/h/a/bi;

    :cond_5
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget v0, v0, Lcom/google/o/h/a/bg;->d:I

    invoke-static {v0}, Lcom/google/o/h/a/bi;->a(I)Lcom/google/o/h/a/bi;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/google/o/h/a/bi;->a:Lcom/google/o/h/a/bi;

    :cond_6
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x5c

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v10, ", denominatorArea="

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", numerator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", denominator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v5, v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v3

    goto/16 :goto_1

    :cond_7
    iget v1, v0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v1, v1, 0x4

    const/4 v5, 0x4

    if-ne v1, v5, :cond_a

    move v1, v2

    :goto_2
    if-eqz v1, :cond_8

    iget v1, v0, Lcom/google/o/h/a/bg;->e:I

    int-to-double v6, v1

    cmpg-double v1, v10, v6

    if-ltz v1, :cond_9

    :cond_8
    iget v1, v0, Lcom/google/o/h/a/bg;->a:I

    and-int/lit8 v1, v1, 0x8

    const/16 v5, 0x8

    if-ne v1, v5, :cond_b

    move v1, v2

    :goto_3
    if-eqz v1, :cond_c

    iget v0, v0, Lcom/google/o/h/a/bg;->f:I

    int-to-double v0, v0

    cmpl-double v0, v10, v0

    if-lez v0, :cond_c

    :cond_9
    move v0, v2

    :goto_4
    if-nez v0, :cond_d

    move v0, v2

    goto/16 :goto_1

    :cond_a
    move v1, v3

    goto :goto_2

    :cond_b
    move v1, v3

    goto :goto_3

    :cond_c
    move v0, v3

    goto :goto_4

    :cond_d
    move v0, v3

    goto/16 :goto_1

    :cond_e
    move v0, v3

    .line 280
    goto/16 :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/google/o/b/a/v;)Z
    .locals 14
    .param p1    # Lcom/google/o/b/a/v;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/ba;",
            ">;",
            "Lcom/google/o/b/a/v;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 407
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    const/4 v0, 0x1

    .line 440
    :goto_0
    return v0

    .line 412
    :cond_0
    if-eqz p1, :cond_1

    iget v0, p1, Lcom/google/o/b/a/v;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    .line 413
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 412
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 416
    :cond_3
    iget-object v0, p1, Lcom/google/o/b/a/v;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/b/a/l;->d()Lcom/google/o/b/a/l;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/b/a/l;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/o/b/a/l;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v10

    .line 417
    iget v0, p1, Lcom/google/o/b/a/v;->i:F

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    float-to-double v12, v0

    .line 419
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_4
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Lcom/google/o/h/a/ba;

    .line 420
    iget v0, v9, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    .line 422
    iget-object v0, v9, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    if-nez v0, :cond_a

    invoke-static {}, Lcom/google/d/a/a/hp;->d()Lcom/google/d/a/a/hp;

    move-result-object v0

    :goto_3
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/q;->a(Lcom/google/d/a/a/hp;)Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v6

    .line 426
    const/4 v0, 0x1

    new-array v8, v0, [F

    iget-wide v0, v10, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v2, v10, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget-wide v4, v6, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v1, v8, v0

    .line 428
    iget v0, v9, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_b

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_5

    .line 429
    iget v0, v9, Lcom/google/o/h/a/ba;->c:I

    int-to-float v0, v0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_4

    .line 430
    :cond_5
    iget v0, v9, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x4

    const/4 v2, 0x4

    if-ne v0, v2, :cond_c

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_6

    .line 431
    iget v0, v9, Lcom/google/o/h/a/ba;->d:I

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gtz v0, :cond_4

    .line 432
    :cond_6
    iget v0, v9, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x8

    const/16 v1, 0x8

    if-ne v0, v1, :cond_d

    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_7

    .line 433
    iget v0, v9, Lcom/google/o/h/a/ba;->e:I

    int-to-double v0, v0

    cmpg-double v0, v0, v12

    if-gtz v0, :cond_4

    .line 434
    :cond_7
    iget v0, v9, Lcom/google/o/h/a/ba;->a:I

    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_e

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_8

    .line 435
    iget v0, v9, Lcom/google/o/h/a/ba;->f:I

    int-to-double v0, v0

    cmpg-double v0, v12, v0

    if-gtz v0, :cond_4

    .line 436
    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 420
    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    .line 422
    :cond_a
    iget-object v0, v9, Lcom/google/o/h/a/ba;->b:Lcom/google/d/a/a/hp;

    goto :goto_3

    .line 428
    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    .line 430
    :cond_c
    const/4 v0, 0x0

    goto :goto_5

    .line 432
    :cond_d
    const/4 v0, 0x0

    goto :goto_6

    .line 434
    :cond_e
    const/4 v0, 0x0

    goto :goto_7

    .line 440
    :cond_f
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private static b(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)D
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/d/a/a/ju;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/r;",
            ")D"
        }
    .end annotation

    .prologue
    .line 351
    const-wide/16 v0, 0x0

    .line 352
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/d/a/a/ju;

    .line 353
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/b/a/r;->a(Lcom/google/d/a/a/ju;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/apps/gmm/map/b/a/r;->a(Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/r;)D

    move-result-wide v0

    add-double/2addr v0, v2

    move-wide v2, v0

    .line 354
    goto :goto_0

    .line 355
    :cond_0
    return-wide v2
.end method


# virtual methods
.method final a(Ljava/util/List;J)Lcom/google/o/h/a/be;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/aq;",
            ">;J)",
            "Lcom/google/o/h/a/be;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 178
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/aq;

    .line 179
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-wide v4, v1, Lcom/google/android/apps/gmm/startpage/c;->a:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    sub-long/2addr v4, p2

    long-to-int v4, v4

    new-instance v5, Ljava/util/ArrayList;

    iget-object v1, v0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, v0, Lcom/google/o/h/a/aq;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/at;->d()Lcom/google/o/h/a/at;

    move-result-object v7

    invoke-virtual {v1, v7}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/at;

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v5, v4}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;I)Z

    move-result v4

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->d()Ljava/util/List;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v5, v5, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    invoke-static {v1, v5}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;Lcom/google/android/apps/gmm/map/b/a/r;)Z

    move-result v5

    iget-object v1, v0, Lcom/google/o/h/a/aq;->g:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v6, v6, Lcom/google/android/apps/gmm/startpage/c;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->h()Ljava/util/List;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget v7, v7, Lcom/google/android/apps/gmm/startpage/c;->d:F

    invoke-static {v6, v7}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;F)Z

    move-result v6

    invoke-virtual {v0}, Lcom/google/o/h/a/aq;->g()Ljava/util/List;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v8, v8, Lcom/google/android/apps/gmm/startpage/c;->e:Lcom/google/o/b/a/v;

    invoke-static {v7, v8}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;Lcom/google/o/b/a/v;)Z

    move-result v7

    if-eqz v4, :cond_5

    if-eqz v5, :cond_5

    if-eqz v1, :cond_5

    if-eqz v6, :cond_5

    if-eqz v7, :cond_5

    move v1, v2

    :goto_2
    if-eqz v1, :cond_0

    .line 180
    iget v0, v0, Lcom/google/o/h/a/aq;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/be;->a(I)Lcom/google/o/h/a/be;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/o/h/a/be;->a:Lcom/google/o/h/a/be;

    .line 183
    :cond_2
    :goto_3
    return-object v0

    .line 179
    :cond_3
    if-eqz v6, :cond_4

    iget-wide v6, v6, Lcom/google/android/apps/gmm/map/b/a/j;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    :cond_4
    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2

    .line 183
    :cond_6
    sget-object v0, Lcom/google/o/h/a/be;->c:Lcom/google/o/h/a/be;

    goto :goto_3
.end method

.method public final b(Ljava/util/List;J)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Lcom/google/o/h/a/lh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 206
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    .line 208
    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->j()Ljava/util/List;

    move-result-object v3

    invoke-virtual {p0, v3, p2, p3}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;J)Lcom/google/o/h/a/be;

    move-result-object v3

    .line 209
    sget-object v4, Lcom/google/o/h/a/be;->c:Lcom/google/o/h/a/be;

    if-eq v3, v4, :cond_0

    .line 210
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    :cond_1
    return-object v1
.end method

.method public final b(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)Z
    .locals 8
    .param p2    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x0

    .line 114
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    .line 115
    if-eqz v0, :cond_1

    .line 116
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v7

    .line 117
    :goto_0
    if-eqz p2, :cond_0

    iget-object v0, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/startpage/c;

    .line 121
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/c;->b:Lcom/google/android/apps/gmm/map/b/a/r;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget-object v5, v0, Lcom/google/android/apps/gmm/startpage/c;->c:Lcom/google/android/apps/gmm/map/b/a/l;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    iget v6, v0, Lcom/google/android/apps/gmm/startpage/c;->d:F

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/startpage/c;-><init>(JLcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/l;FLcom/google/o/b/a/v;)V

    .line 120
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/android/apps/gmm/startpage/c;)Z

    move-result v0

    .line 131
    :goto_1
    return v0

    :cond_1
    move-object v7, v5

    .line 116
    goto :goto_0

    .line 124
    :cond_2
    invoke-static {p2}, Lcom/google/android/apps/gmm/map/w;->b(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v4

    .line 125
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->l()Lcom/google/android/apps/gmm/map/indoor/a/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/indoor/a/a;->a()Lcom/google/android/apps/gmm/map/indoor/d/a;

    move-result-object v0

    .line 126
    if-nez v0, :cond_3

    .line 127
    :goto_2
    iget-object v0, p2, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->d()Lcom/google/android/apps/gmm/map/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/f/o;->d:Lcom/google/android/apps/gmm/map/f/a/a;

    iget v6, v0, Lcom/google/android/apps/gmm/map/f/a/a;->i:F

    .line 128
    new-instance v1, Lcom/google/android/apps/gmm/startpage/c;

    .line 129
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/startpage/c;-><init>(JLcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/map/b/a/l;FLcom/google/o/b/a/v;)V

    .line 131
    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/startpage/a;->a(Lcom/google/android/apps/gmm/startpage/c;)Z

    move-result v0

    goto :goto_1

    .line 126
    :cond_3
    iget-object v5, v0, Lcom/google/android/apps/gmm/map/indoor/d/a;->a:Lcom/google/android/apps/gmm/map/b/a/l;

    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/a;->b:Lcom/google/android/apps/gmm/startpage/c;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xd

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "cacheContext="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
