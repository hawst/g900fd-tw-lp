.class public Lcom/google/android/apps/gmm/search/SearchLoadingFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/am;


# static fields
.field private static final e:Ljava/util/regex/Pattern;


# instance fields
.field private a:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/android/apps/gmm/search/aq;

.field private c:Lcom/google/android/apps/gmm/base/l/aj;

.field private d:Lcom/google/android/apps/gmm/base/activities/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-string v0, "^\\s*>>>\\s*(.*)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/x/o;)Lcom/google/android/apps/gmm/search/SearchLoadingFragment;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/x/a;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/search/al;",
            ">;)",
            "Lcom/google/android/apps/gmm/search/SearchLoadingFragment;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;-><init>()V

    .line 58
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 59
    const-string v2, "searchRequest"

    invoke-virtual {p0, v1, v2, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 60
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->setArguments(Landroid/os/Bundle;)V

    .line 61
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/search/al;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 117
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v1, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->a()I

    move-result v1

    if-ne v1, v4, :cond_6

    invoke-static {}, Lcom/google/android/apps/gmm/z/b/l;->a()Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v1

    new-array v0, v4, [Lcom/google/b/f/cq;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/b/f/t;->cU:Lcom/google/b/f/t;

    aput-object v3, v0, v2

    iput-object v0, v1, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    invoke-static {}, Lcom/google/b/f/b/a/ck;->newBuilder()Lcom/google/b/f/b/a/cm;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/b/a/cp;->a:Lcom/google/b/f/b/a/cp;

    invoke-virtual {v0, v2}, Lcom/google/b/f/b/a/cm;->a(Lcom/google/b/f/b/a/cp;)Lcom/google/b/f/b/a/cm;

    move-result-object v0

    sget-object v2, Lcom/google/b/f/b/a/cp;->b:Lcom/google/b/f/b/a/cp;

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget v3, v0, Lcom/google/b/f/b/a/cm;->a:I

    or-int/lit8 v3, v3, 0x4

    iput v3, v0, Lcom/google/b/f/b/a/cm;->a:I

    iget v2, v2, Lcom/google/b/f/b/a/cp;->e:I

    iput v2, v0, Lcom/google/b/f/b/a/cm;->d:I

    invoke-virtual {v0}, Lcom/google/b/f/b/a/cm;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/b/f/b/a/ck;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Lcom/google/b/f/b/a/ck;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->m_()Lcom/google/android/apps/gmm/z/a/b;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/z/b/n;

    sget-object v3, Lcom/google/r/b/a/a;->h:Lcom/google/r/b/a/a;

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/b/n;-><init>(Lcom/google/r/b/a/a;)V

    invoke-interface {v0, v2, v1}, Lcom/google/android/apps/gmm/z/a/b;->a(Lcom/google/android/apps/gmm/z/b/n;Lcom/google/android/apps/gmm/z/b/l;)Ljava/lang/String;

    .line 123
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ap;->a:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 125
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Lcom/google/android/apps/gmm/l;->gJ:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 125
    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 130
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/x/o;->b(Ljava/io/Serializable;)V

    .line 131
    iget-object v1, p1, Lcom/google/android/apps/gmm/search/al;->c:Lcom/google/android/apps/gmm/search/ap;

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->b:Lcom/google/android/apps/gmm/search/aq;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/search/aq;->b(Lcom/google/android/apps/gmm/x/o;)V

    .line 136
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/z/a/a;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->o()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/z/a/a;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 141
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v2, Lcom/google/android/apps/gmm/search/ak;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/apps/gmm/search/ak;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 144
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/b;

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->s()Lcom/google/e/a/a/a/b;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p1, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v0, v0, Lcom/google/android/apps/gmm/search/ai;->b:Lcom/google/android/apps/gmm/map/internal/d/b;

    .line 147
    invoke-virtual {v1}, Lcom/google/android/apps/gmm/search/ap;->s()Lcom/google/e/a/a/a/b;

    move-result-object v1

    .line 146
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/internal/d/a;->k:Landroid/os/Handler;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/b;->g:Lcom/google/android/apps/gmm/map/internal/d/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/map/internal/d/a;->k:Landroid/os/Handler;

    const/4 v4, 0x7

    new-instance v5, Lcom/google/b/a/an;

    invoke-direct {v5, v0, v1}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/apps/gmm/search/al;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 5

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/search/al;->l()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p2}, Lcom/google/android/apps/gmm/f/b;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    invoke-static {v1, v0, p1}, Lcom/google/android/apps/gmm/f/b;->a(Landroid/app/Activity;Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/net/i;)V

    goto :goto_0

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v0

    sget v2, Lcom/google/android/apps/gmm/l;->mK:I

    sget-object v3, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    new-instance v3, Lcom/google/android/apps/gmm/f/a;

    invoke-direct {v3, v0, p1}, Lcom/google/android/apps/gmm/f/a;-><init>(Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/shared/net/i;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->od:I

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->bg:I

    invoke-virtual {v0, v1, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/k;)Z
    .locals 1

    .prologue
    .line 181
    invoke-static {p1}, Lcom/google/android/apps/gmm/f/b;->a(Lcom/google/android/apps/gmm/shared/net/k;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/apps/gmm/search/al;)V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStackImmediate()Z

    goto :goto_0
.end method

.method public final f_()Lcom/google/b/f/t;
    .locals 1

    .prologue
    .line 186
    sget-object v0, Lcom/google/b/f/t;->ea:Lcom/google/b/f/t;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onCreate(Landroid/os/Bundle;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->i_()Lcom/google/android/apps/gmm/x/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "searchRequest"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/x/o;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->a:Lcom/google/android/apps/gmm/x/o;

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    .line 69
    iput-object p0, v0, Lcom/google/android/apps/gmm/search/al;->d:Lcom/google/android/apps/gmm/search/am;

    .line 70
    return-void
.end method

.method public onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 74
    invoke-super {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->onResume()V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->d:Lcom/google/android/apps/gmm/base/activities/c;

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v1, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->b:Lcom/google/android/apps/gmm/search/aq;

    .line 79
    new-instance v0, Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/base/l/aj;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->c:Lcom/google/android/apps/gmm/base/l/aj;

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->c:Lcom/google/android/apps/gmm/base/l/aj;

    sget-object v1, Lcom/google/android/apps/gmm/base/l/c;->c:Lcom/google/android/apps/gmm/base/l/c;

    iput-object v1, v0, Lcom/google/android/apps/gmm/base/l/b;->g:Lcom/google/android/apps/gmm/base/l/c;

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/l/b;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 81
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->c:Lcom/google/android/apps/gmm/base/l/aj;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/search/al;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/l/aj;->a(Ljava/lang/String;)V

    .line 84
    new-instance v2, Lcom/google/android/apps/gmm/base/activities/w;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/base/activities/w;-><init>()V

    .line 85
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->q:Landroid/view/View;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->r:Z

    .line 86
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v5, v0, Lcom/google/android/apps/gmm/base/activities/p;->l:Landroid/view/View;

    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->p:Z

    .line 87
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v3, v0, Lcom/google/android/apps/gmm/base/activities/p;->I:I

    const-class v0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;

    .line 88
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->R:Ljava/lang/String;

    sget-object v0, Lcom/google/android/apps/gmm/z/b/o;->z:Lcom/google/android/apps/gmm/z/b/o;

    .line 89
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->N:Lcom/google/android/apps/gmm/z/b/o;

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->c:Lcom/google/android/apps/gmm/base/l/aj;

    .line 90
    iget-object v1, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput-object v0, v1, Lcom/google/android/apps/gmm/base/activities/p;->f:Lcom/google/android/apps/gmm/base/l/ai;

    .line 91
    iget-object v0, v2, Lcom/google/android/apps/gmm/base/activities/w;->a:Lcom/google/android/apps/gmm/base/activities/p;

    iput v4, v0, Lcom/google/android/apps/gmm/base/activities/p;->d:I

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/SearchLoadingFragment;->a:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/search/al;

    .line 94
    iget-object v1, v0, Lcom/google/android/apps/gmm/search/al;->a:Lcom/google/android/apps/gmm/search/ai;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/v/a;->u:Lcom/google/e/a/a/a/b;

    const/16 v3, 0x1c

    invoke-virtual {v1, v4, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 95
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/base/activities/w;->a()Lcom/google/android/apps/gmm/base/activities/p;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->q:Lcom/google/android/apps/gmm/base/activities/ae;

    invoke-virtual {v1, v2}, Lcom/google/android/apps/gmm/base/activities/ae;->a(Lcom/google/android/apps/gmm/base/activities/p;)V

    .line 99
    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v1, v1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    const-class v2, Lcom/google/android/apps/gmm/search/aq;

    invoke-interface {v1, v2}, Lcom/google/android/apps/gmm/base/j/b;->a(Ljava/lang/Class;)Lcom/google/android/apps/gmm/base/j/c;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/search/aq;

    .line 100
    iget-object v1, v1, Lcom/google/android/apps/gmm/search/aq;->f:Lcom/google/android/apps/gmm/search/ah;

    iget-object v1, v1, Lcom/google/android/apps/gmm/search/ah;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->b()Lcom/google/android/apps/gmm/shared/net/r;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/android/apps/gmm/shared/net/i;)Z

    .line 102
    return-void
.end method
