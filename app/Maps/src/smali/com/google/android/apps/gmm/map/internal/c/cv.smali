.class public final Lcom/google/android/apps/gmm/map/internal/c/cv;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final f:Lcom/google/android/apps/gmm/map/internal/c/cv;

.field public static final g:Lcom/google/android/apps/gmm/map/internal/c/cv;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:[I

.field public final d:[I

.field public e:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:I

.field private final i:[I

.field private final j:I

.field private final k:[F

.field private final l:[F


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v3, 0x16

    const/16 v4, 0x15

    const/4 v1, 0x0

    .line 74
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cv;

    invoke-direct {v0, v4}, Lcom/google/android/apps/gmm/map/internal/c/cv;-><init>(I)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->f:Lcom/google/android/apps/gmm/map/internal/c/cv;

    .line 82
    new-array v2, v3, [I

    move v0, v1

    .line 84
    :goto_0
    if-ge v0, v3, :cond_0

    .line 85
    aput v0, v2, v0

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/cv;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v1, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/cv;-><init>([IIII)V

    sput-object v0, Lcom/google/android/apps/gmm/map/internal/c/cv;->g:Lcom/google/android/apps/gmm/map/internal/c/cv;

    .line 89
    return-void
.end method

.method private constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    .line 149
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->h:I

    .line 150
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->b:I

    .line 151
    iput p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    .line 153
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->j:I

    .line 154
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->k:[F

    .line 155
    new-array v0, v1, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->l:[F

    .line 156
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->e:Ljava/util/TreeSet;

    .line 157
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    .line 158
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    .line 159
    return-void
.end method

.method public constructor <init>([IIII)V
    .locals 9

    .prologue
    const/high16 v2, -0x40800000    # -1.0f

    const/4 v3, -0x1

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    .line 99
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->h:I

    .line 100
    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, p3

    div-float v5, v0, v1

    .line 101
    iput p4, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->b:I

    .line 102
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    array-length v6, v0

    .line 103
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    add-int/lit8 v1, v6, -0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    .line 105
    iput p2, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->j:I

    .line 106
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->k:[F

    .line 107
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->l:[F

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->k:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->l:[F

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([FF)V

    .line 113
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->e:Ljava/util/TreeSet;

    .line 114
    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->j:I

    .line 115
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->j:I

    int-to-float v2, v0

    .line 116
    const/4 v0, 0x0

    move v4, v0

    move v0, v1

    :goto_0
    if-ge v4, v6, :cond_1

    .line 117
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->e:Ljava/util/TreeSet;

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    aget v7, v7, v4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->k:[F

    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    aget v7, v7, v4

    aget v1, v1, v7

    const/4 v7, 0x0

    cmpg-float v1, v1, v7

    if-gez v1, :cond_4

    .line 119
    int-to-float v1, v4

    mul-float/2addr v1, v5

    iget v7, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->j:I

    int-to-float v7, v7

    add-float/2addr v1, v7

    .line 120
    :goto_1
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    aget v7, v7, v4

    if-ge v0, v7, :cond_0

    .line 121
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->k:[F

    aput v2, v7, v0

    .line 122
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->l:[F

    aput v1, v7, v0

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 124
    :cond_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->k:[F

    aput v1, v2, v0

    move v8, v1

    move v1, v0

    move v0, v8

    .line 116
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    move v0, v1

    goto :goto_0

    .line 128
    :cond_1
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    .line 129
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    invoke-static {v0, v3}, Ljava/util/Arrays;->fill([II)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->e:Ljava/util/TreeSet;

    invoke-virtual {v0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v3

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 134
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->c:[I

    aput v1, v3, v0

    .line 135
    if-ltz v1, :cond_2

    .line 136
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->d:[I

    aput v0, v3, v1

    :cond_2
    move v1, v0

    .line 139
    goto :goto_3

    .line 140
    :cond_3
    return-void

    :cond_4
    move v1, v0

    move v0, v2

    goto :goto_2
.end method


# virtual methods
.method public final a(F)I
    .locals 2

    .prologue
    .line 169
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->j:I

    int-to-float v0, v0

    sub-float v0, p1, v0

    iget v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->h:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 170
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 171
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->a:I

    .line 175
    :goto_0
    return v0

    .line 172
    :cond_0
    if-gez v0, :cond_1

    .line 173
    const/4 v0, -0x1

    goto :goto_0

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/c/cv;->i:[I

    aget v0, v1, v0

    goto :goto_0
.end method
