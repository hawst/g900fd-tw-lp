.class public Lcom/google/android/apps/gmm/suggest/e/b;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;"
        }
    .end annotation
.end field

.field public final b:I

.field public final c:Lcom/google/r/b/a/afz;


# direct methods
.method public constructor <init>(Ljava/util/List;ILcom/google/maps/g/a/hm;Lcom/google/r/b/a/afz;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ap;",
            ">;I",
            "Lcom/google/maps/g/a/hm;",
            "Lcom/google/r/b/a/afz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/b;->a:Ljava/util/List;

    .line 38
    iput p2, p0, Lcom/google/android/apps/gmm/suggest/e/b;->b:I

    .line 39
    iput-object p4, p0, Lcom/google/android/apps/gmm/suggest/e/b;->c:Lcom/google/r/b/a/afz;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 44
    new-instance v1, Lcom/google/android/apps/gmm/map/r/a/aq;

    iget-object v0, p0, Lcom/google/android/apps/gmm/suggest/e/b;->a:Ljava/util/List;

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/e/b;->b:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/ap;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/a/aq;-><init>(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 45
    iput-object p1, v1, Lcom/google/android/apps/gmm/map/r/a/aq;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/r/a/aq;->a()Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v0

    .line 46
    iget-object v1, p0, Lcom/google/android/apps/gmm/suggest/e/b;->a:Ljava/util/List;

    iget v2, p0, Lcom/google/android/apps/gmm/suggest/e/b;->b:I

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 47
    return-void
.end method
