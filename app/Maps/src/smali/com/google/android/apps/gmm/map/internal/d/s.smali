.class Lcom/google/android/apps/gmm/map/internal/d/s;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final b:Lcom/google/android/apps/gmm/map/util/a/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/e",
            "<",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field final c:Lcom/google/android/apps/gmm/shared/net/a/b;

.field final d:Lcom/google/android/apps/gmm/shared/c/f;

.field final e:Lcom/google/android/apps/gmm/map/b/a/ai;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/util/a/b;Lcom/google/android/apps/gmm/map/b/a/ai;Lcom/google/android/apps/gmm/shared/net/a/b;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 3

    .prologue
    const/16 v2, 0x3e8

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v1, "staleTileCache"

    invoke-direct {v0, v2, v1, p1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 61
    new-instance v0, Lcom/google/android/apps/gmm/map/util/a/e;

    const-string v1, "recentlyUpdatedTileCache"

    invoke-direct {v0, v2, v1, p1}, Lcom/google/android/apps/gmm/map/util/a/e;-><init>(ILjava/lang/String;Lcom/google/android/apps/gmm/map/util/a/b;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    .line 63
    iput-object p3, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 64
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 65
    iput-object p4, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 66
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/apps/gmm/map/internal/c/bp;)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v2

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v0

    sub-long v4, v2, v0

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 77
    new-instance v6, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v6, v7, v7, v7}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    .line 78
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-lez v1, :cond_2

    .line 79
    invoke-virtual {p1, v1, v6}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(ILcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 81
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v0, v8, v4

    if-gez v0, :cond_1

    .line 82
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 83
    iget v7, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    .line 84
    iget v8, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    .line 85
    iget v9, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    .line 86
    iget-object v10, v6, Lcom/google/android/apps/gmm/map/internal/c/bp;->d:Lcom/google/android/apps/gmm/map/internal/c/cd;

    invoke-direct {v0, v7, v8, v9, v10}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(IIILcom/google/android/apps/gmm/map/internal/c/cd;)V

    .line 87
    iget-object v7, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v0, v8}, Lcom/google/android/apps/gmm/map/util/a/e;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 78
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 90
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 186
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v4

    sub-long/2addr v0, v4

    cmp-long v0, v0, p2

    if-lez v0, :cond_0

    move v0, v2

    .line 206
    :goto_0
    return v0

    .line 190
    :cond_0
    new-instance v4, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v4, v2, v2, v2}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    .line 191
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 192
    iget v1, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    :goto_1
    if-lez v3, :cond_3

    .line 193
    invoke-virtual {p1, v3, v4}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(ILcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 194
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v1, v4}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 195
    if-eqz v1, :cond_2

    .line 199
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->e:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    invoke-virtual {v5, v8}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v8

    sub-long/2addr v6, v8

    .line 200
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v1, p2, v8

    if-gez v1, :cond_2

    if-eqz v0, :cond_1

    .line 201
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v1, v8, v6

    if-gez v1, :cond_2

    :cond_1
    move v0, v2

    .line 202
    goto :goto_0

    .line 192
    :cond_2
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    goto :goto_1

    .line 206
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Lcom/google/android/apps/gmm/map/internal/c/bt;)Z
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 159
    iget v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->e:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->f:I

    if-eq v2, v3, :cond_0

    .line 160
    const/4 v0, 0x1

    .line 163
    :goto_0
    return v0

    :cond_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->d:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->c:Lcom/google/android/apps/gmm/shared/net/a/b;

    .line 164
    iget-wide v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    cmp-long v4, v4, v0

    if-nez v4, :cond_1

    .line 163
    :goto_1
    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/s;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Z

    move-result v0

    goto :goto_0

    .line 164
    :cond_1
    iget-wide v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->a:J

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/internal/c/bt;->c:Lcom/google/android/apps/gmm/map/b/a/ai;

    invoke-virtual {v4, v3}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/net/a/b;)J

    move-result-wide v4

    sub-long/2addr v0, v4

    goto :goto_1
.end method

.method final b(Lcom/google/android/apps/gmm/map/internal/c/bp;J)Lcom/google/android/apps/gmm/map/internal/c/bp;
    .locals 6
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 219
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    invoke-direct {v1, v0, v0, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    .line 220
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-lez v2, :cond_1

    .line 221
    invoke-virtual {p1, v2, v1}, Lcom/google/android/apps/gmm/map/internal/c/bp;->a(ILcom/google/android/apps/gmm/map/internal/c/bp;)V

    .line 222
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->a:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/s;->b:Lcom/google/android/apps/gmm/map/util/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/util/a/e;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 226
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, p2

    if-lez v0, :cond_0

    move-object v0, v1

    .line 230
    :goto_1
    return-object v0

    .line 220
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 230
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
