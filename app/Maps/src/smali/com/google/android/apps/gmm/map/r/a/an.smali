.class public Lcom/google/android/apps/gmm/map/r/a/an;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/map/r/a/am;I)Lcom/google/b/a/an;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/r/a/am;",
            "I)",
            "Lcom/google/b/a/an",
            "<",
            "Lcom/google/android/apps/gmm/map/r/a/ag;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 186
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/am;->i:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v2, Lcom/google/maps/g/a/bx;->c:Lcom/google/maps/g/a/bx;

    if-ne v1, v2, :cond_4

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v1, :cond_4

    .line 190
    iget-object v0, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    move-object v1, v0

    .line 192
    :goto_0
    if-eqz v1, :cond_3

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    if-eqz v0, :cond_3

    .line 193
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->c:Lcom/google/maps/g/a/ez;

    if-eq v0, v2, :cond_1

    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->c:Lcom/google/maps/g/a/ez;

    sget-object v2, Lcom/google/maps/g/a/ez;->d:Lcom/google/maps/g/a/ez;

    if-ne v0, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 194
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/r/a/ag;->B:Lcom/google/android/apps/gmm/map/r/a/ag;

    .line 195
    iget v1, v0, Lcom/google/android/apps/gmm/map/r/a/ag;->j:I

    add-int/2addr p1, v1

    move-object v1, v0

    goto :goto_0

    .line 193
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 197
    :cond_3
    new-instance v0, Lcom/google/b/a/an;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/b/a/an;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/r/a/ag;Z)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 153
    if-nez p1, :cond_0

    move v0, v1

    .line 163
    :goto_0
    return v0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/r/a/am;

    .line 158
    iget-object v3, v0, Lcom/google/android/apps/gmm/map/r/a/am;->a:Lcom/google/maps/g/a/bx;

    sget-object v4, Lcom/google/maps/g/a/bx;->a:Lcom/google/maps/g/a/bx;

    if-ne v3, v4, :cond_1

    .line 159
    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/r/a/am;->k:Z

    goto :goto_0

    :cond_2
    move v0, v1

    .line 163
    goto :goto_0
.end method

.method static a([Lcom/google/android/apps/gmm/map/r/a/ag;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 68
    array-length v0, p0

    if-nez v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v0, v1

    .line 71
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 73
    aget-object v2, p0, v0

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/r/a/ag;->v:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 77
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method
