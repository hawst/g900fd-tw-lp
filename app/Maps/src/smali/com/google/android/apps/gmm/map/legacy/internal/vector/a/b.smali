.class public Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/b;
.super Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 428
    const-string v0, "uniform mat4 uMVPMatrix;\nattribute vec4 aPosition;\nattribute vec2 aTextureCoord;\nattribute vec4 userData;\nvarying float styleIndex;\nuniform float uRoadAlpha;\nvarying float vRoadAlpha;\nvarying float strokeIndex;\nvarying vec2 texCoord;\nvoid main() {\n  gl_Position = uMVPMatrix * vec4(aPosition.xy, 0.0, 1.0);\n  styleIndex = (256.0 * userData.x) + userData.y;\n  strokeIndex = userData.z;\n  if (strokeIndex < 0.5 && uRoadAlpha < 1.0)\n    vRoadAlpha = 0.0;\n  else\n    vRoadAlpha = uRoadAlpha;\n  texCoord = aTextureCoord;\n  texCoord -= vec2(0.5);\n}\n"

    const-string v1, "precision lowp float;\nuniform vec3 uTextureInfo;\nvarying float styleIndex;\nuniform vec3 uStrokeInfo;\nvarying float strokeIndex;\nuniform float reciprocal2xBitmapWidth;\nuniform float minimumTextureMinificationScale;\nvarying vec2 texCoord;\nuniform sampler2D sTexture0;\nuniform sampler2D sStyleTexture;\nvarying float vRoadAlpha;\nuniform float brightnessScale;\nvoid main() {\n  float texHeight = uTextureInfo[0];\n  vec2 textureScale = vec2(uTextureInfo[1], uTextureInfo[2]);\n  float widthVecIndex = strokeIndex;\n  if (strokeIndex > 2.5)\n    widthVecIndex = strokeIndex - 3.0;\n  float widthPixel = 4.0;\n  if (strokeIndex > 2.5)\n    widthPixel = 5.0;\n  float colorTexX = (1.0 + 2.0 * strokeIndex) * reciprocal2xBitmapWidth;\n  float widthTexX = (1.0 + 2.0 * widthPixel) * reciprocal2xBitmapWidth;\n  float texY = (styleIndex + 0.5) / texHeight;\n  vec4 roadWidthVec = texture2D(sStyleTexture, vec2(widthTexX, texY));\n  float roadScale;\n  if (widthVecIndex < 0.5)\n    roadScale = max(minimumTextureMinificationScale, roadWidthVec[0]);\n  else if (widthVecIndex < 1.5)\n    roadScale = max(minimumTextureMinificationScale, roadWidthVec[1]);\n  else if (widthVecIndex < 2.5)\n    roadScale = max(minimumTextureMinificationScale, roadWidthVec[2]);\n  else\n    roadScale = 0.001;\n  textureScale.x /= roadScale;\n  vec2 texCoordTmp = (texCoord * 0.5 * textureScale) + vec2(0.5);\n  vec4 vColor = texture2D(sStyleTexture, vec2(colorTexX, texY));\n  float t = texture2D(sTexture0, texCoordTmp, -0.25).a;\n  t *= vRoadAlpha * vColor.a;\n  gl_FragColor = vec4(brightnessScale * vColor.rgb, t);\n}\n"

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/gmm/map/legacy/internal/vector/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    return-void
.end method
