.class public Lcom/google/android/apps/gmm/o/a/b;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/o/a/a;

.field public b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/o/a/a;Z)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/apps/gmm/o/a/b;->a:Lcom/google/android/apps/gmm/o/a/a;

    .line 16
    iput-boolean p2, p0, Lcom/google/android/apps/gmm/o/a/b;->b:Z

    .line 17
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 21
    if-ne p0, p1, :cond_1

    .line 29
    :cond_0
    :goto_0
    return v0

    .line 23
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 24
    goto :goto_0

    .line 25
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 26
    goto :goto_0

    .line 28
    :cond_3
    check-cast p1, Lcom/google/android/apps/gmm/o/a/b;

    .line 29
    iget-object v2, p0, Lcom/google/android/apps/gmm/o/a/b;->a:Lcom/google/android/apps/gmm/o/a/a;

    iget-object v3, p1, Lcom/google/android/apps/gmm/o/a/b;->a:Lcom/google/android/apps/gmm/o/a/a;

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/o/a/b;->b:Z

    iget-boolean v3, p1, Lcom/google/android/apps/gmm/o/a/b;->b:Z

    if-eq v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 35
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/o/a/b;->a:Lcom/google/android/apps/gmm/o/a/a;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/o/a/b;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
