.class public Lcom/google/android/apps/gmm/addaplace/a/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/maps/g/gy;

.field public d:Lcom/google/maps/g/cc;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/google/maps/g/hs;


# direct methods
.method public constructor <init>(Lcom/google/maps/g/hs;Ljava/lang/String;Ljava/lang/String;Lcom/google/maps/g/cc;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p4    # Lcom/google/maps/g/cc;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p5    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 34
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/addaplace/a/a;->g:Lcom/google/maps/g/hs;

    .line 35
    iput-object p2, p0, Lcom/google/android/apps/gmm/addaplace/a/a;->a:Ljava/lang/String;

    .line 36
    iput-object p3, p0, Lcom/google/android/apps/gmm/addaplace/a/a;->b:Ljava/lang/String;

    .line 37
    iput-object p4, p0, Lcom/google/android/apps/gmm/addaplace/a/a;->d:Lcom/google/maps/g/cc;

    .line 38
    if-eqz p5, :cond_1

    .line 39
    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v0

    iget-wide v2, p5, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    .line 40
    iget v1, v0, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v0, Lcom/google/maps/g/ha;->b:D

    iget-wide v2, p5, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    .line 41
    iget v1, v0, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/g/ha;->a:I

    iput-wide v2, v0, Lcom/google/maps/g/ha;->c:D

    .line 42
    invoke-virtual {v0}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/gy;

    iput-object v0, p0, Lcom/google/android/apps/gmm/addaplace/a/a;->c:Lcom/google/maps/g/gy;

    .line 44
    :cond_1
    iput-object p6, p0, Lcom/google/android/apps/gmm/addaplace/a/a;->e:Ljava/lang/String;

    .line 45
    iput-object p7, p0, Lcom/google/android/apps/gmm/addaplace/a/a;->f:Ljava/lang/String;

    .line 46
    return-void
.end method

.method public static a(Lcom/google/maps/g/hs;)Lcom/google/android/apps/gmm/addaplace/a/a;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/addaplace/a/a;

    move-object v1, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/addaplace/a/a;-><init>(Lcom/google/maps/g/hs;Ljava/lang/String;Ljava/lang/String;Lcom/google/maps/g/cc;Lcom/google/android/apps/gmm/map/b/a/q;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
