.class Lcom/google/android/apps/gmm/base/k/ak;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Lcom/google/android/apps/gmm/base/k/a;

.field public final d:Landroid/graphics/drawable/AnimationDrawable;

.field public final e:Z

.field public final f:Z


# direct methods
.method public constructor <init>(IIZLcom/google/android/apps/gmm/base/k/a;Z)V
    .locals 1

    .prologue
    .line 291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292
    iput p1, p0, Lcom/google/android/apps/gmm/base/k/ak;->a:I

    .line 293
    iput p2, p0, Lcom/google/android/apps/gmm/base/k/ak;->b:I

    .line 294
    iput-boolean p3, p0, Lcom/google/android/apps/gmm/base/k/ak;->e:Z

    .line 295
    iput-object p4, p0, Lcom/google/android/apps/gmm/base/k/ak;->c:Lcom/google/android/apps/gmm/base/k/a;

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/k/ak;->d:Landroid/graphics/drawable/AnimationDrawable;

    .line 297
    iput-boolean p5, p0, Lcom/google/android/apps/gmm/base/k/ak;->f:Z

    .line 298
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 302
    instance-of v1, p1, Lcom/google/android/apps/gmm/base/k/ak;

    if-nez v1, :cond_1

    .line 313
    :cond_0
    :goto_0
    return v0

    .line 306
    :cond_1
    check-cast p1, Lcom/google/android/apps/gmm/base/k/ak;

    .line 307
    iget v1, p0, Lcom/google/android/apps/gmm/base/k/ak;->a:I

    iget v2, p1, Lcom/google/android/apps/gmm/base/k/ak;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/apps/gmm/base/k/ak;->b:I

    iget v2, p1, Lcom/google/android/apps/gmm/base/k/ak;->b:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/base/k/ak;->e:Z

    iget-boolean v2, p1, Lcom/google/android/apps/gmm/base/k/ak;->e:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/k/ak;->c:Lcom/google/android/apps/gmm/base/k/a;

    iget-object v2, p1, Lcom/google/android/apps/gmm/base/k/ak;->c:Lcom/google/android/apps/gmm/base/k/a;

    if-ne v1, v2, :cond_0

    .line 313
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 319
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/ak;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/apps/gmm/base/k/ak;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/apps/gmm/base/k/ak;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/k/ak;->c:Lcom/google/android/apps/gmm/base/k/a;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
