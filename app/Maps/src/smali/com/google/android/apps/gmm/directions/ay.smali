.class Lcom/google/android/apps/gmm/directions/ay;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/av;

.field final synthetic b:Lcom/google/android/apps/gmm/directions/a/c;

.field final synthetic c:Lcom/google/android/apps/gmm/directions/ax;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/ax;Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/directions/a/c;)V
    .locals 0

    .prologue
    .line 234
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/ay;->c:Lcom/google/android/apps/gmm/directions/ax;

    iput-object p2, p0, Lcom/google/android/apps/gmm/directions/ay;->a:Lcom/google/android/apps/gmm/directions/av;

    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/ay;->b:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 237
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ay;->c:Lcom/google/android/apps/gmm/directions/ax;

    invoke-static {v1}, Lcom/google/android/apps/gmm/directions/ax;->a(Lcom/google/android/apps/gmm/directions/ax;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v1

    .line 241
    iget-boolean v2, v1, Lcom/google/android/apps/gmm/base/activities/c;->g:Z

    if-nez v2, :cond_1

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ay;->c:Lcom/google/android/apps/gmm/directions/ax;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    if-eqz v2, :cond_2

    .line 247
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ay;->c:Lcom/google/android/apps/gmm/directions/ax;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/ax;->a:Lcom/google/android/apps/gmm/directions/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/e;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->e()Lcom/google/android/apps/gmm/p/b/a;

    move-result-object v2

    if-nez v2, :cond_4

    .line 249
    :cond_2
    :goto_1
    if-eqz v0, :cond_3

    .line 250
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ay;->a:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/r/b/a;->a()Lcom/google/o/b/a/v;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/o/b/a/v;)V

    .line 252
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ay;->a:Lcom/google/android/apps/gmm/directions/av;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ay;->c:Lcom/google/android/apps/gmm/directions/ax;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/directions/ax;->e()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Ljava/util/List;)V

    .line 255
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    .line 262
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ay;->a:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/a/a;)V

    .line 265
    iget-object v0, v1, Lcom/google/android/apps/gmm/base/activities/c;->b:Lcom/google/android/apps/gmm/map/MapFragment;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/MapFragment;->a:Lcom/google/android/apps/gmm/map/t;

    .line 264
    invoke-static {v0}, Lcom/google/android/apps/gmm/map/w;->a(Lcom/google/android/apps/gmm/map/t;)Lcom/google/android/apps/gmm/map/b/a/r;

    move-result-object v0

    .line 267
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ay;->c:Lcom/google/android/apps/gmm/directions/ax;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/ay;->a:Lcom/google/android/apps/gmm/directions/av;

    iget-object v4, p0, Lcom/google/android/apps/gmm/directions/ay;->b:Lcom/google/android/apps/gmm/directions/a/c;

    invoke-virtual {v2, v3, v0, v4}, Lcom/google/android/apps/gmm/directions/ax;->a(Lcom/google/android/apps/gmm/directions/av;Lcom/google/android/apps/gmm/map/b/a/r;Lcom/google/android/apps/gmm/directions/a/c;)Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    move-result-object v0

    .line 269
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e()Landroid/app/Fragment;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/fragments/a/c;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    goto :goto_0

    .line 247
    :cond_4
    invoke-interface {v2}, Lcom/google/android/apps/gmm/p/b/a;->a()Lcom/google/android/apps/gmm/map/r/b/a;

    move-result-object v0

    goto :goto_1
.end method
