.class public Lcom/google/android/apps/gmm/v/x;
.super Lcom/google/android/apps/gmm/v/bp;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/v/r;


# instance fields
.field private volatile a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/x;-><init>(I)V

    .line 39
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/google/android/apps/gmm/v/y;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 21
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    .line 49
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/v/x;->a(I)V

    .line 50
    return-void
.end method

.method private constructor <init>(ILjava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/v/bp;-><init>(Ljava/lang/Class;)V

    .line 21
    const/4 v0, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    .line 79
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/v/x;->a(I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/google/android/apps/gmm/v/bo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/gmm/v/x;-><init>(ILjava/lang/Class;)V

    .line 63
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 2

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/x;->p:Z

    if-eqz v0, :cond_0

    .line 119
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x3

    aput p1, v0, v1

    .line 122
    return-void
.end method

.method public final a(FFFF)V
    .locals 2

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/x;->p:Z

    if-eqz v0, :cond_0

    .line 108
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 112
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    .line 114
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 91
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/v/x;->p:Z

    if-eqz v0, :cond_0

    .line 92
    invoke-static {}, Lcom/google/android/apps/gmm/v/i;->b()V

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x1

    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x2

    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v1, 0x3

    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    aput v2, v0, v1

    .line 98
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V
    .locals 3

    .prologue
    .line 127
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/apps/gmm/v/bp;->a(Lcom/google/android/apps/gmm/v/aa;Lcom/google/android/apps/gmm/v/n;Lcom/google/android/apps/gmm/v/cj;I)V

    .line 129
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/gmm/v/x;->a:[F

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glVertexAttrib4fv(I[FI)V

    .line 130
    return-void
.end method
