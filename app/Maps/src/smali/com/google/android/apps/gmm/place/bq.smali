.class public Lcom/google/android/apps/gmm/place/bq;
.super Lcom/google/android/apps/gmm/place/af;
.source "PG"


# instance fields
.field private A:Lcom/google/android/apps/gmm/place/e/b/b;

.field private B:Lcom/google/android/apps/gmm/place/bv;

.field private C:Lcom/google/android/apps/gmm/place/h/b/a;

.field private D:Lcom/google/android/apps/gmm/place/bx;

.field private E:Lcom/google/android/apps/gmm/place/by;

.field private F:Lcom/google/android/apps/gmm/place/station/b;

.field private G:Lcom/google/android/apps/gmm/place/j/aa;

.field private H:Lcom/google/android/apps/gmm/place/j/g;

.field private I:Lcom/google/android/apps/gmm/place/reservation/g;

.field private J:Lcom/google/android/apps/gmm/place/bi;

.field private K:Lcom/google/android/apps/gmm/place/bb;

.field private L:Lcom/google/android/apps/gmm/place/j/q;

.field private M:Lcom/google/android/apps/gmm/place/j/p;

.field private N:Lcom/google/android/apps/gmm/place/bc;

.field public c:Z

.field public d:Lcom/google/android/apps/gmm/place/aq;

.field public e:Lcom/google/android/apps/gmm/place/riddler/d/d;

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/place/br;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/google/android/apps/gmm/base/g/f;

.field private h:Z

.field private i:Lcom/google/android/apps/gmm/place/j/i;

.field private j:Lcom/google/android/apps/gmm/place/j/c;

.field private k:Lcom/google/android/apps/gmm/place/j/h;

.field private l:Lcom/google/android/apps/gmm/place/ay;

.field private m:Lcom/google/android/apps/gmm/place/a/b/c;

.field private n:Lcom/google/android/apps/gmm/place/a/b/e;

.field private o:Lcom/google/android/apps/gmm/place/ba;

.field private p:Lcom/google/android/apps/gmm/place/facepile/k;

.field private q:Lcom/google/android/apps/gmm/place/j/s;

.field private r:Lcom/google/android/apps/gmm/place/bo;

.field private s:Lcom/google/android/apps/gmm/place/bp;

.field private t:Lcom/google/android/apps/gmm/place/d/q;

.field private u:Lcom/google/android/apps/gmm/base/l/v;

.field private v:Lcom/google/android/apps/gmm/place/g/b;

.field private w:Lcom/google/android/apps/gmm/place/bt;

.field private x:Lcom/google/android/apps/gmm/place/bu;

.field private y:Lcom/google/android/apps/gmm/place/f/b/a;

.field private z:Lcom/google/android/apps/gmm/place/e/b/c;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/activities/c;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 142
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/place/af;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    .line 96
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    .line 98
    sget-object v0, Lcom/google/android/apps/gmm/base/g/f;->f:Lcom/google/android/apps/gmm/base/g/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->g:Lcom/google/android/apps/gmm/base/g/f;

    .line 143
    new-instance v0, Lcom/google/android/apps/gmm/place/j/h;

    invoke-direct {v0, p1, v2}, Lcom/google/android/apps/gmm/place/j/h;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Z)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/h;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->k:Lcom/google/android/apps/gmm/place/j/h;

    .line 145
    new-instance v0, Lcom/google/android/apps/gmm/place/j/q;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/j/q;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->L:Lcom/google/android/apps/gmm/place/j/q;

    .line 148
    new-instance v0, Lcom/google/android/apps/gmm/place/j/p;

    invoke-direct {v0, p1, v2}, Lcom/google/android/apps/gmm/place/j/p;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Z)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/p;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->M:Lcom/google/android/apps/gmm/place/j/p;

    .line 151
    new-instance v0, Lcom/google/android/apps/gmm/place/facepile/k;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/facepile/k;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/facepile/k;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->p:Lcom/google/android/apps/gmm/place/facepile/k;

    .line 159
    new-instance v0, Lcom/google/android/apps/gmm/place/j/u;

    .line 160
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/place/j/u;-><init>(Landroid/content/Context;Lcom/google/android/apps/gmm/base/j/b;)V

    .line 159
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/base/l/v;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->u:Lcom/google/android/apps/gmm/base/l/v;

    .line 162
    new-instance v0, Lcom/google/android/apps/gmm/place/bv;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bv;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/bv;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->B:Lcom/google/android/apps/gmm/place/bv;

    .line 164
    new-instance v0, Lcom/google/android/apps/gmm/place/aq;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->B:Lcom/google/android/apps/gmm/place/bv;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/place/aq;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/cj;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/aq;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->d:Lcom/google/android/apps/gmm/place/aq;

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->k:Lcom/google/android/apps/gmm/place/j/h;

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->k:Lcom/google/android/apps/gmm/place/j/h;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/j/h;->b:Lcom/google/android/apps/gmm/place/i/e;

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->M:Lcom/google/android/apps/gmm/place/j/p;

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->M:Lcom/google/android/apps/gmm/place/j/p;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/af;->a:Lcom/google/android/apps/gmm/place/j/a;

    iput-object v1, v0, Lcom/google/android/apps/gmm/place/j/p;->a:Lcom/google/android/apps/gmm/place/i/e;

    .line 174
    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/place/a/b/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/a/b/c;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/a/b/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->m:Lcom/google/android/apps/gmm/place/a/b/c;

    .line 176
    new-instance v0, Lcom/google/android/apps/gmm/place/ay;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/ay;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/ay;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->l:Lcom/google/android/apps/gmm/place/ay;

    .line 178
    new-instance v0, Lcom/google/android/apps/gmm/place/a/b/e;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/a/b/e;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/a/b/e;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->n:Lcom/google/android/apps/gmm/place/a/b/e;

    .line 180
    new-instance v0, Lcom/google/android/apps/gmm/place/ba;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/ba;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/ba;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->o:Lcom/google/android/apps/gmm/place/ba;

    .line 182
    new-instance v0, Lcom/google/android/apps/gmm/place/j/s;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/j/s;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/s;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->q:Lcom/google/android/apps/gmm/place/j/s;

    .line 184
    new-instance v0, Lcom/google/android/apps/gmm/place/j/g;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/j/g;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->H:Lcom/google/android/apps/gmm/place/j/g;

    .line 186
    new-instance v0, Lcom/google/android/apps/gmm/place/reservation/g;

    invoke-direct {v0, p1, v2}, Lcom/google/android/apps/gmm/place/reservation/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Z)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/reservation/g;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->I:Lcom/google/android/apps/gmm/place/reservation/g;

    .line 191
    new-instance v0, Lcom/google/android/apps/gmm/place/bp;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/bp;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/bp;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->s:Lcom/google/android/apps/gmm/place/bp;

    .line 193
    new-instance v0, Lcom/google/android/apps/gmm/place/d/q;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/d/q;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/d/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->t:Lcom/google/android/apps/gmm/place/d/q;

    .line 195
    new-instance v0, Lcom/google/android/apps/gmm/place/g/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/g/b;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/g/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->v:Lcom/google/android/apps/gmm/place/g/b;

    .line 197
    new-instance v0, Lcom/google/android/apps/gmm/place/bs;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bs;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    new-instance v0, Lcom/google/android/apps/gmm/place/bt;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bt;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/bt;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->w:Lcom/google/android/apps/gmm/place/bt;

    .line 201
    new-instance v0, Lcom/google/android/apps/gmm/place/bb;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bb;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/bb;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->K:Lcom/google/android/apps/gmm/place/bb;

    .line 210
    new-instance v0, Lcom/google/android/apps/gmm/place/bx;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bx;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/bx;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->D:Lcom/google/android/apps/gmm/place/bx;

    .line 212
    new-instance v0, Lcom/google/android/apps/gmm/place/bu;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/bu;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/bu;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->x:Lcom/google/android/apps/gmm/place/bu;

    .line 214
    new-instance v0, Lcom/google/android/apps/gmm/place/f/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/f/b/a;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/f/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->y:Lcom/google/android/apps/gmm/place/f/b/a;

    .line 218
    new-instance v0, Lcom/google/android/apps/gmm/place/e/b/c;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/e/b/c;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/e/b/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->z:Lcom/google/android/apps/gmm/place/e/b/c;

    .line 222
    new-instance v1, Lcom/google/android/apps/gmm/place/e/b/b;

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/place/e/b/b;-><init>(Lcom/google/android/apps/gmm/shared/c/f;)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/place/e/b/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->A:Lcom/google/android/apps/gmm/place/e/b/b;

    .line 226
    new-instance v0, Lcom/google/android/apps/gmm/place/station/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/station/b;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/station/b;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->F:Lcom/google/android/apps/gmm/place/station/b;

    .line 228
    new-instance v0, Lcom/google/android/apps/gmm/place/h/b/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/h/b/a;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/h/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->C:Lcom/google/android/apps/gmm/place/h/b/a;

    .line 230
    new-instance v0, Lcom/google/android/apps/gmm/place/by;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/place/by;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/by;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->E:Lcom/google/android/apps/gmm/place/by;

    .line 232
    new-instance v0, Lcom/google/android/apps/gmm/place/bo;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/bo;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/bo;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->r:Lcom/google/android/apps/gmm/place/bo;

    .line 234
    new-instance v0, Lcom/google/android/apps/gmm/place/j/aa;

    .line 240
    iget-object v1, p1, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/place/j/aa;-><init>(Lcom/google/android/apps/gmm/base/j/b;)V

    .line 239
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/aa;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->G:Lcom/google/android/apps/gmm/place/j/aa;

    .line 242
    new-instance v1, Lcom/google/android/apps/gmm/place/bi;

    .line 244
    invoke-static {}, Landroid/support/v4/f/a;->a()Landroid/support/v4/f/a;

    move-result-object v2

    .line 245
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->u()Lcom/google/android/apps/gmm/map/h/d;

    move-result-object v0

    invoke-direct {v1, p1, v2, v0}, Lcom/google/android/apps/gmm/place/bi;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Landroid/support/v4/f/a;Lcom/google/android/apps/gmm/map/h/d;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    check-cast v0, Lcom/google/android/apps/gmm/place/bi;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->J:Lcom/google/android/apps/gmm/place/bi;

    .line 247
    new-instance v0, Lcom/google/android/apps/gmm/place/j/i;

    iget-object v2, p0, Lcom/google/android/apps/gmm/place/bq;->d:Lcom/google/android/apps/gmm/place/aq;

    iget-object v3, p0, Lcom/google/android/apps/gmm/place/bq;->G:Lcom/google/android/apps/gmm/place/j/aa;

    iget-object v4, p0, Lcom/google/android/apps/gmm/place/bq;->x:Lcom/google/android/apps/gmm/place/bu;

    iget-object v5, p0, Lcom/google/android/apps/gmm/place/bq;->K:Lcom/google/android/apps/gmm/place/bb;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/place/j/i;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/i/a;Lcom/google/android/apps/gmm/base/l/a/y;Lcom/google/android/apps/gmm/place/ch;Lcom/google/android/apps/gmm/place/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/i;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->i:Lcom/google/android/apps/gmm/place/j/i;

    .line 254
    new-instance v0, Lcom/google/android/apps/gmm/place/j/c;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->x:Lcom/google/android/apps/gmm/place/bu;

    invoke-direct {v0, p1, v1}, Lcom/google/android/apps/gmm/place/j/c;-><init>(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/place/ch;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/c;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->j:Lcom/google/android/apps/gmm/place/j/c;

    .line 259
    new-instance v0, Lcom/google/android/apps/gmm/place/j/q;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/j/q;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/j/q;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->L:Lcom/google/android/apps/gmm/place/j/q;

    .line 261
    new-instance v0, Lcom/google/android/apps/gmm/place/riddler/d/g;

    invoke-direct {v0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/g;-><init>(Lcom/google/android/apps/gmm/base/activities/c;)V

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    check-cast v0, Lcom/google/android/apps/gmm/place/riddler/d/d;

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->e:Lcom/google/android/apps/gmm/place/riddler/d/d;

    .line 271
    return-void
.end method


# virtual methods
.method public final A()Lcom/google/android/apps/gmm/base/l/a/ab;
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->g:Lcom/google/android/apps/gmm/base/g/f;

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->a:Lcom/google/android/apps/gmm/base/g/f;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bq;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->j:Lcom/google/android/apps/gmm/place/j/c;

    .line 511
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->i:Lcom/google/android/apps/gmm/place/j/i;

    goto :goto_0
.end method

.method public final B()Lcom/google/android/apps/gmm/place/i/h;
    .locals 1

    .prologue
    .line 503
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->J:Lcom/google/android/apps/gmm/place/bi;

    return-object v0
.end method

.method public final bridge synthetic C()Lcom/google/android/apps/gmm/base/l/a/o;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->L:Lcom/google/android/apps/gmm/place/j/q;

    return-object v0
.end method

.method public final D()Lcom/google/android/apps/gmm/place/f/b/c;
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->y:Lcom/google/android/apps/gmm/place/f/b/a;

    return-object v0
.end method

.method public final E()Lcom/google/android/apps/gmm/place/e/b/a;
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->z:Lcom/google/android/apps/gmm/place/e/b/c;

    return-object v0
.end method

.method public final F()Lcom/google/android/apps/gmm/place/e/b/a;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->A:Lcom/google/android/apps/gmm/place/e/b/b;

    return-object v0
.end method

.method public final G()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 557
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bq;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final H()Lcom/google/android/apps/gmm/place/riddler/d/a;
    .locals 1

    .prologue
    .line 562
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->e:Lcom/google/android/apps/gmm/place/riddler/d/d;

    return-object v0
.end method

.method public final I()Lcom/google/android/apps/gmm/place/j;
    .locals 1

    .prologue
    .line 567
    const/4 v0, 0x0

    return-object v0
.end method

.method public final K()V
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->A:Lcom/google/android/apps/gmm/place/e/b/b;

    if-eqz v0, :cond_0

    .line 581
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->A:Lcom/google/android/apps/gmm/place/e/b/b;

    invoke-static {v0}, Lcom/google/android/libraries/curvular/cq;->a(Lcom/google/android/libraries/curvular/ce;)I

    .line 583
    :cond_0
    return-void
.end method

.method public final L()Lcom/google/android/apps/gmm/base/l/a/q;
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->L:Lcom/google/android/apps/gmm/place/j/q;

    return-object v0
.end method

.method public final a()Lcom/google/android/apps/gmm/base/g/f;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->g:Lcom/google/android/apps/gmm/base/g/f;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 281
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 282
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->M()Lcom/google/android/apps/gmm/base/g/f;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->g:Lcom/google/android/apps/gmm/base/g/f;

    .line 283
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->f:Z

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/bq;->h:Z

    .line 284
    iget-boolean v1, v0, Lcom/google/android/apps/gmm/base/g/c;->e:Z

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/bq;->c:Z

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/gmm/place/bq;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/place/br;

    .line 287
    const-string v3, "Trying to set a placemark on a null view model"

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-interface {v1, p1, p2, p3}, Lcom/google/android/apps/gmm/place/br;->a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V

    goto :goto_0

    .line 290
    :cond_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/place/af;->b:Lcom/google/android/apps/gmm/base/l/j;

    .line 291
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/c;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/android/apps/gmm/z/b/l;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v3

    const/4 v1, 0x1

    new-array v4, v1, [Lcom/google/b/f/cq;

    const/4 v5, 0x0

    .line 293
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->F:Z

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v1, v1, Lcom/google/r/b/a/ads;->G:Z

    if-eqz v1, :cond_3

    :cond_2
    sget-object v1, Lcom/google/b/f/t;->dZ:Lcom/google/b/f/t;

    :goto_1
    aput-object v1, v4, v5

    .line 292
    iput-object v4, v3, Lcom/google/android/apps/gmm/z/b/m;->c:[Lcom/google/b/f/cq;

    .line 296
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->b:Lcom/google/android/apps/gmm/base/g/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/a;->i:Ljava/lang/String;

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/apps/gmm/z/b/m;->a(Ljava/lang/String;)Lcom/google/android/apps/gmm/z/b/m;

    move-result-object v0

    .line 297
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/z/b/m;->a()Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    .line 290
    invoke-interface {v2, v0}, Lcom/google/android/apps/gmm/base/l/j;->a(Lcom/google/android/apps/gmm/z/b/l;)V

    .line 298
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/place/bq;->J()V

    .line 299
    return-void

    .line 293
    :cond_3
    sget-object v1, Lcom/google/b/f/t;->dd:Lcom/google/b/f/t;

    goto :goto_1

    .line 296
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->d:Lcom/google/android/apps/gmm/place/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/aq;->h:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->d(Ljava/lang/Object;)V

    .line 527
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->e:Lcom/google/android/apps/gmm/place/riddler/d/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/d;->a(Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 530
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->M:Lcom/google/android/apps/gmm/place/j/p;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/place/j/p;->b:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/j/p;->j()V

    .line 347
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->k:Lcom/google/android/apps/gmm/place/j/h;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/place/j/h;->a:Z

    .line 348
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->I:Lcom/google/android/apps/gmm/place/reservation/g;

    iput-boolean p1, v0, Lcom/google/android/apps/gmm/place/reservation/g;->d:Z

    iget-object v1, v0, Lcom/google/android/apps/gmm/place/reservation/g;->b:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/place/reservation/g;->d:Z

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lcom/google/android/apps/gmm/place/reservation/g;->c:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/place/reservation/g;->j()V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/apps/gmm/place/reservation/g;->c:Z

    .line 349
    :cond_0
    return-void
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 330
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bq;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/util/b/g;)V
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->d:Lcom/google/android/apps/gmm/place/aq;

    iget-object v0, v0, Lcom/google/android/apps/gmm/place/aq;->h:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/apps/gmm/map/util/b/a/a;->e(Ljava/lang/Object;)V

    .line 535
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->e:Lcom/google/android/apps/gmm/place/riddler/d/d;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/place/riddler/d/d;->b(Lcom/google/android/apps/gmm/map/util/b/g;)V

    .line 538
    return-void
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 335
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->g:Lcom/google/android/apps/gmm/base/g/f;

    sget-object v1, Lcom/google/android/apps/gmm/base/g/f;->a:Lcom/google/android/apps/gmm/base/g/f;

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/bq;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 336
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->M:Lcom/google/android/apps/gmm/place/j/p;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/j/p;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 341
    :goto_0
    return-object v0

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->k:Lcom/google/android/apps/gmm/place/j/h;

    if-eqz v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->k:Lcom/google/android/apps/gmm/place/j/h;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/place/j/h;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 341
    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Lcom/google/android/apps/gmm/place/i/a;
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->d:Lcom/google/android/apps/gmm/place/aq;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/gmm/place/a/b/a;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->m:Lcom/google/android/apps/gmm/place/a/b/c;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/gmm/place/x;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->l:Lcom/google/android/apps/gmm/place/ay;

    return-object v0
.end method

.method public final bridge synthetic g()Lcom/google/android/apps/gmm/place/a/b/b;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->n:Lcom/google/android/apps/gmm/place/a/b/e;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/gmm/place/b;
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->o:Lcom/google/android/apps/gmm/place/ba;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/gmm/place/i/c;
    .locals 1

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->N:Lcom/google/android/apps/gmm/place/bc;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/gmm/place/facepile/a/c;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->p:Lcom/google/android/apps/gmm/place/facepile/k;

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/gmm/place/j/g;
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->H:Lcom/google/android/apps/gmm/place/j/g;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/gmm/place/reservation/b/c;
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->I:Lcom/google/android/apps/gmm/place/reservation/g;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/gmm/place/i/g;
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->q:Lcom/google/android/apps/gmm/place/j/s;

    return-object v0
.end method

.method public final n()Lcom/google/android/apps/gmm/place/k;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->r:Lcom/google/android/apps/gmm/place/bo;

    return-object v0
.end method

.method public final o()Lcom/google/android/apps/gmm/place/d/j;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->t:Lcom/google/android/apps/gmm/place/d/q;

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/gmm/base/l/v;
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->u:Lcom/google/android/apps/gmm/base/l/v;

    return-object v0
.end method

.method public final q()Lcom/google/android/apps/gmm/place/g/a/b;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->v:Lcom/google/android/apps/gmm/place/g/b;

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/gmm/place/x;
    .locals 1

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->w:Lcom/google/android/apps/gmm/place/bt;

    return-object v0
.end method

.method public final s()Lcom/google/android/apps/gmm/place/station/b/b;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->F:Lcom/google/android/apps/gmm/place/station/b;

    return-object v0
.end method

.method public final bridge synthetic t()Lcom/google/android/apps/gmm/place/i/b;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->k:Lcom/google/android/apps/gmm/place/j/h;

    return-object v0
.end method

.method public final bridge synthetic u()Lcom/google/android/apps/gmm/place/i/f;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->M:Lcom/google/android/apps/gmm/place/j/p;

    return-object v0
.end method

.method public final v()Lcom/google/android/apps/gmm/place/cj;
    .locals 1

    .prologue
    .line 478
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->B:Lcom/google/android/apps/gmm/place/bv;

    return-object v0
.end method

.method public final w()Lcom/google/android/apps/gmm/place/h/b/d;
    .locals 1

    .prologue
    .line 483
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->C:Lcom/google/android/apps/gmm/place/h/b/a;

    return-object v0
.end method

.method public final x()Lcom/google/android/apps/gmm/base/l/a/y;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->G:Lcom/google/android/apps/gmm/place/j/aa;

    return-object v0
.end method

.method public final y()Lcom/google/android/apps/gmm/place/g/a/e;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->D:Lcom/google/android/apps/gmm/place/bx;

    return-object v0
.end method

.method public final z()Lcom/google/android/apps/gmm/place/i/l;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/bq;->E:Lcom/google/android/apps/gmm/place/by;

    return-object v0
.end method
