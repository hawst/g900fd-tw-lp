.class public Lcom/google/android/apps/gmm/p/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/location/rawlocationevents/e;
.implements Lcom/google/android/apps/gmm/p/b/a;
.implements Lcom/google/android/apps/gmm/p/e/l;


# annotations
.annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
    a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
.end annotation


# instance fields
.field public final a:Lcom/google/android/apps/gmm/base/a;

.field public b:Lcom/google/android/apps/gmm/p/e/i;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/android/apps/gmm/p/d;

.field public e:Lcom/google/android/apps/gmm/p/a/a;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field private f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Lcom/google/android/apps/gmm/map/r/b/a;

.field private l:Lcom/google/android/apps/gmm/p/b/b;

.field private final m:Lcom/google/android/gms/location/e;

.field private final n:Lcom/google/android/apps/gmm/p/b;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->g:Z

    .line 63
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->h:Z

    .line 64
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->i:Z

    .line 65
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->j:Z

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/p/b/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/p/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    .line 85
    new-instance v0, Lcom/google/android/apps/gmm/p/b;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/p/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a;->n:Lcom/google/android/apps/gmm/p/b;

    .line 88
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/a;->a:Lcom/google/android/apps/gmm/base/a;

    .line 90
    new-instance v0, Lcom/google/android/gms/location/e;

    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/gmm/p/a;->n:Lcom/google/android/apps/gmm/p/b;

    iget-object v3, p0, Lcom/google/android/apps/gmm/p/a;->n:Lcom/google/android/apps/gmm/p/b;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/location/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a;->m:Lcom/google/android/gms/location/e;

    .line 92
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/map/r/b/a;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v2, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v0, v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 182
    iput-object p1, p0, Lcom/google/android/apps/gmm/p/a;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/map/location/a;

    invoke-direct {v1, p1}, Lcom/google/android/apps/gmm/map/location/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 185
    :cond_1
    return-void

    .line 181
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 7

    .prologue
    .line 249
    const/4 v1, 0x0

    .line 251
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    .line 252
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;->d()Z

    move-result v3

    .line 253
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x1f

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "RLEP.isMaybeAvailable(): "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    if-eqz v3, :cond_0

    .line 260
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    if-eq v0, v1, :cond_2

    .line 261
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    if-eqz v1, :cond_1

    .line 262
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;->b()V

    .line 264
    :cond_1
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;->a()V

    .line 265
    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    .line 267
    :cond_2
    return-void

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/map/r/b/a;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->k:Lcom/google/android/apps/gmm/map/r/b/a;

    return-object v0
.end method

.method public a(Lcom/google/android/apps/gmm/car/a/d;)V
    .locals 1
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 212
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/car/a/d;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->i:Z

    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a;->g()V

    .line 214
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;)V
    .locals 2
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 284
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/AndroidLocationEvent;->getLocation()Landroid/location/Location;

    move-result-object v0

    .line 285
    iget-boolean v1, p0, Lcom/google/android/apps/gmm/p/a;->g:Z

    if-nez v1, :cond_1

    .line 286
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/p/c;->a(Landroid/location/Location;Lcom/google/android/apps/gmm/map/c/a;)Landroid/location/Location;

    move-result-object v0

    .line 287
    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/c;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/r/b/c;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/c;->a(Landroid/location/Location;)Lcom/google/android/apps/gmm/map/r/b/c;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/r/b/c;->l:Lcom/google/android/apps/gmm/map/b/a/u;

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "latitude and longitude must be set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/r/b/a;

    invoke-direct {v1, v0}, Lcom/google/android/apps/gmm/map/r/b/a;-><init>(Lcom/google/android/apps/gmm/map/r/b/c;)V

    invoke-direct {p0, v1}, Lcom/google/android/apps/gmm/p/a;->b(Lcom/google/android/apps/gmm/map/r/b/a;)V

    .line 289
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/r/b/a;)V
    .locals 1

    .prologue
    .line 122
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->g:Z

    if-eqz v0, :cond_0

    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/p/a;->b(Lcom/google/android/apps/gmm/map/r/b/a;)V

    .line 125
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/j/a/b;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 206
    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->b:Lcom/google/android/apps/gmm/navigation/g/b/d;

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    iget-object v2, p1, Lcom/google/android/apps/gmm/navigation/j/a/b;->a:Lcom/google/android/apps/gmm/navigation/g/b/f;

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->g:Z

    .line 207
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a;->g()V

    .line 208
    return-void

    :cond_2
    move v2, v0

    .line 206
    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1
.end method

.method public a(Lcom/google/android/apps/gmm/p/b/f;)V
    .locals 3
    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 192
    iget-object v0, p1, Lcom/google/android/apps/gmm/p/b/f;->a:Lcom/google/android/apps/gmm/p/b/b;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onLocationProviderStatusEvent() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    .line 195
    new-instance v1, Lcom/google/android/apps/gmm/p/b/b;

    iget-object v2, p1, Lcom/google/android/apps/gmm/p/b/f;->a:Lcom/google/android/apps/gmm/p/b/b;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/p/b/b;-><init>(Lcom/google/android/apps/gmm/p/b/b;)V

    iput-object v1, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/p/b/b;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a;->g()V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->j()Lcom/google/android/apps/gmm/map/h/c;

    move-result-object v0

    sget-object v1, Lcom/google/r/b/a/av;->m:Lcom/google/r/b/a/av;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/h/c;->b(Lcom/google/r/b/a/av;)V

    .line 202
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    iget-object v1, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v2, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v1, v2, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v1, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->h:Z

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->m:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->d()V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a;->g()V

    .line 144
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->h:Z

    .line 149
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/p/a;->g()V

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->m:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->c()V

    .line 151
    return-void
.end method

.method public final e()Lcom/google/android/apps/gmm/p/b/b;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/location/LocationStatus;
    .locals 1
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->a()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/util/c/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->m:Lcom/google/android/gms/location/e;

    .line 177
    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->m:Lcom/google/android/gms/location/e;

    invoke-virtual {v0}, Lcom/google/android/gms/location/e;->b()Lcom/google/android/gms/location/LocationStatus;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 222
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 223
    const-string v3, "isActivityResumed: %b, isCarConnected: %b, isNavigating: %b, isEnabled(): %b"

    const/4 v0, 0x4

    new-array v4, v0, [Ljava/lang/Object;

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->h:Z

    .line 225
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v2

    const/4 v0, 0x2

    iget-boolean v5, p0, Lcom/google/android/apps/gmm/p/a;->g:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v5, 0x3

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    iget-object v6, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v7, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v6, v7, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v6, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v0, v6, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v5

    .line 223
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 227
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->h:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->i:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->g:Z

    if-eqz v0, :cond_6

    :cond_1
    move v0, v2

    .line 229
    :goto_1
    if-eqz v0, :cond_7

    .line 230
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/a;->d:Lcom/google/android/apps/gmm/p/d;

    iget-boolean v4, v3, Lcom/google/android/apps/gmm/p/d;->d:Z

    if-nez v4, :cond_2

    iget-object v4, v3, Lcom/google/android/apps/gmm/p/d;->e:Landroid/location/LocationManager;

    if-eqz v4, :cond_2

    iput-boolean v2, v3, Lcom/google/android/apps/gmm/p/d;->d:Z

    iget-object v4, v3, Lcom/google/android/apps/gmm/p/d;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->E_()Lcom/google/android/apps/gmm/shared/c/a/j;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/p/d;->a:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {v4, v3, v5}, Lcom/google/android/apps/gmm/shared/c/a/j;->a(Ljava/lang/Runnable;Lcom/google/android/apps/gmm/shared/c/a/p;)V

    .line 235
    :cond_2
    :goto_2
    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->l:Lcom/google/android/apps/gmm/p/b/b;

    iget-object v3, v0, Lcom/google/android/apps/gmm/p/b/b;->a:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v4, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-eq v3, v4, :cond_3

    iget-object v0, v0, Lcom/google/android/apps/gmm/p/b/b;->b:Lcom/google/android/apps/gmm/p/b/d;

    sget-object v3, Lcom/google/android/apps/gmm/p/b/d;->c:Lcom/google/android/apps/gmm/p/b/d;

    if-ne v0, v3, :cond_8

    :cond_3
    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    move v0, v2

    .line 237
    :goto_4
    if-eqz v0, :cond_a

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/p/a;->j:Z

    if-nez v3, :cond_a

    .line 238
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/a;->j()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->b:Lcom/google/android/apps/gmm/p/e/i;

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/p/e/i;->a:Z

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/e/i;->a()V

    iput-boolean v2, p0, Lcom/google/android/apps/gmm/p/a;->j:Z

    .line 242
    :cond_4
    :goto_5
    return-void

    :cond_5
    move v0, v1

    .line 225
    goto :goto_0

    :cond_6
    move v0, v1

    .line 227
    goto :goto_1

    .line 232
    :cond_7
    iget-object v3, p0, Lcom/google/android/apps/gmm/p/a;->d:Lcom/google/android/apps/gmm/p/d;

    iput-boolean v1, v3, Lcom/google/android/apps/gmm/p/d;->d:Z

    goto :goto_2

    :cond_8
    move v0, v1

    .line 235
    goto :goto_3

    :cond_9
    move v0, v1

    goto :goto_4

    .line 239
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/apps/gmm/p/a;->j:Z

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->b:Lcom/google/android/apps/gmm/p/e/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/e/i;->b()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/p/a;->f:Lcom/google/android/apps/gmm/map/location/rawlocationevents/d;

    iput-boolean v1, p0, Lcom/google/android/apps/gmm/p/a;->j:Z

    goto :goto_5
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->j:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x44

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "onLocationSensorAvailabilityChange() areLocationSensorsStarted:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 276
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/p/a;->j:Z

    if-eqz v0, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/google/android/apps/gmm/p/a;->j()V

    .line 279
    :cond_0
    return-void
.end method

.method public final i()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/location/ActivityRecognitionResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->e:Lcom/google/android/apps/gmm/p/a/a;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/gmm/p/a;->e:Lcom/google/android/apps/gmm/p/a/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/p/a/a;->a()Ljava/util/List;

    move-result-object v0

    .line 296
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method
