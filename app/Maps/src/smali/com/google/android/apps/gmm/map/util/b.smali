.class public Lcom/google/android/apps/gmm/map/util/b;
.super Lcom/google/android/apps/gmm/map/util/a/i;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/map/util/a/i",
        "<",
        "Lcom/google/android/apps/gmm/map/util/f;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I


# direct methods
.method public constructor <init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/util/a/b;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/gmm/map/util/a/i;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/gmm/map/util/b;->a:I

    .line 35
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(I)Lcom/google/android/apps/gmm/map/util/f;
    .locals 3

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/map/util/b;->a:I

    .line 43
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/util/b;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/util/f;

    .line 45
    iget v1, p0, Lcom/google/android/apps/gmm/map/util/b;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 46
    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/map/util/f;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_0
    monitor-exit p0

    return-object v0

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final synthetic a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/apps/gmm/map/util/f;

    iget v1, p0, Lcom/google/android/apps/gmm/map/util/b;->a:I

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/map/util/f;-><init>(I)V

    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/apps/gmm/map/util/b;->a:I

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/util/f;)Z
    .locals 1

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/util/f;->c()V

    .line 54
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 53
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/apps/gmm/map/util/f;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/util/b;->a(Lcom/google/android/apps/gmm/map/util/f;)Z

    move-result v0

    return v0
.end method
