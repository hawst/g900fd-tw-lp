.class public Lcom/google/android/apps/gmm/place/j/s;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/br;
.implements Lcom/google/android/apps/gmm/place/i/g;


# instance fields
.field a:Z

.field private b:Lcom/google/android/apps/gmm/base/activities/c;

.field private c:Lcom/google/android/apps/gmm/x/o;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/google/android/apps/gmm/place/an;

.field private e:Lcom/google/android/apps/gmm/util/webimageview/g;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/j/s;->a:Z

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->e:Lcom/google/android/apps/gmm/util/webimageview/g;

    .line 31
    iput-boolean v1, p0, Lcom/google/android/apps/gmm/place/j/s;->f:Z

    return-void
.end method

.method private e()Ljava/lang/String;
    .locals 5
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/x/o;->a()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/g/c;

    .line 110
    if-eqz v0, :cond_1

    .line 111
    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->T:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v1, v1, Lcom/google/r/b/a/ads;->T:Ljava/util/List;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/tf;->g()Lcom/google/maps/g/tf;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/maps/g/tf;

    invoke-virtual {v1}, Lcom/google/maps/g/tf;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-object v0, v0, Lcom/google/r/b/a/ads;->T:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/n/ao;

    invoke-static {}, Lcom/google/maps/g/tf;->g()Lcom/google/maps/g/tf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/tf;

    invoke-virtual {v0}, Lcom/google/maps/g/tf;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/base/g/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v2

    .line 111
    goto :goto_0

    :cond_1
    move-object v0, v2

    .line 113
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 36
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/activities/c;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 38
    :goto_0
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/j/s;->e()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    move v3, v1

    .line 39
    :goto_1
    if-nez v0, :cond_2

    if-eqz v3, :cond_2

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/j/s;->f:Z

    if-eqz v0, :cond_2

    :goto_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 36
    goto :goto_0

    :cond_1
    move v3, v2

    .line 38
    goto :goto_1

    :cond_2
    move v1, v2

    .line 39
    goto :goto_2
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/apps/gmm/x/o;Lcom/google/android/apps/gmm/place/an;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/apps/gmm/x/o",
            "<",
            "Lcom/google/android/apps/gmm/base/g/c;",
            ">;",
            "Lcom/google/android/apps/gmm/place/an;",
            ")V"
        }
    .end annotation

    .prologue
    .line 58
    iput-object p2, p0, Lcom/google/android/apps/gmm/place/j/s;->c:Lcom/google/android/apps/gmm/x/o;

    .line 59
    invoke-static {p1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/base/activities/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    .line 60
    iput-object p3, p0, Lcom/google/android/apps/gmm/place/j/s;->d:Lcom/google/android/apps/gmm/place/an;

    .line 61
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 81
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/place/j/s;->f:Z

    .line 82
    return-void
.end method

.method public final b()Lcom/google/android/libraries/curvular/cf;
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->d:Lcom/google/android/apps/gmm/place/an;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->d:Lcom/google/android/apps/gmm/place/an;

    iget-object v1, p0, Lcom/google/android/apps/gmm/place/j/s;->c:Lcom/google/android/apps/gmm/x/o;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/place/an;->g(Lcom/google/android/apps/gmm/x/o;)V

    .line 52
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    if-eqz v0, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/gmm/place/j/s;->b:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/g;->ax:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/views/expandingscrollview/ExpandingScrollView;->a()V

    goto :goto_0

    .line 50
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1}, Ljava/lang/IllegalStateException;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/apps/gmm/shared/c/m;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/base/views/c/k;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-direct {p0}, Lcom/google/android/apps/gmm/place/j/s;->e()Ljava/lang/String;

    move-result-object v1

    .line 66
    if-eqz v1, :cond_1

    .line 67
    new-instance v0, Lcom/google/android/apps/gmm/base/views/c/k;

    sget-object v2, Lcom/google/android/apps/gmm/util/webimageview/b;->c:Lcom/google/android/apps/gmm/util/webimageview/b;

    .line 68
    iget-object v4, p0, Lcom/google/android/apps/gmm/place/j/s;->e:Lcom/google/android/apps/gmm/util/webimageview/g;

    if-nez v4, :cond_0

    new-instance v4, Lcom/google/android/apps/gmm/place/j/t;

    invoke-direct {v4, p0}, Lcom/google/android/apps/gmm/place/j/t;-><init>(Lcom/google/android/apps/gmm/place/j/s;)V

    iput-object v4, p0, Lcom/google/android/apps/gmm/place/j/s;->e:Lcom/google/android/apps/gmm/util/webimageview/g;

    :cond_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/place/j/s;->e:Lcom/google/android/apps/gmm/util/webimageview/g;

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/base/views/c/k;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/util/webimageview/b;IILcom/google/android/apps/gmm/util/webimageview/g;)V

    .line 70
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/place/j/s;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
