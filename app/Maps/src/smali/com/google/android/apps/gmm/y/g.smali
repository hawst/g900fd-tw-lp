.class public final enum Lcom/google/android/apps/gmm/y/g;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/apps/gmm/y/g;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcom/google/android/apps/gmm/y/g;

.field public static final enum b:Lcom/google/android/apps/gmm/y/g;

.field public static final enum c:Lcom/google/android/apps/gmm/y/g;

.field public static final enum d:Lcom/google/android/apps/gmm/y/g;

.field public static final enum e:Lcom/google/android/apps/gmm/y/g;

.field public static final enum f:Lcom/google/android/apps/gmm/y/g;

.field public static final enum g:Lcom/google/android/apps/gmm/y/g;

.field private static final synthetic k:[Lcom/google/android/apps/gmm/y/g;


# instance fields
.field public final h:I

.field private final i:Lcom/google/d/a/a/pa;

.field private final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/apps/gmm/y/g;

    const-string v1, "MONDAY"

    const/4 v2, 0x0

    sget-object v3, Lcom/google/d/a/a/pa;->b:Lcom/google/d/a/a/pa;

    const/4 v4, 0x2

    sget v5, Lcom/google/android/apps/gmm/l;->iC:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/y/g;-><init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->a:Lcom/google/android/apps/gmm/y/g;

    .line 43
    new-instance v0, Lcom/google/android/apps/gmm/y/g;

    const-string v1, "TUESDAY"

    const/4 v2, 0x1

    sget-object v3, Lcom/google/d/a/a/pa;->c:Lcom/google/d/a/a/pa;

    const/4 v4, 0x3

    sget v5, Lcom/google/android/apps/gmm/l;->oe:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/y/g;-><init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->b:Lcom/google/android/apps/gmm/y/g;

    .line 44
    new-instance v0, Lcom/google/android/apps/gmm/y/g;

    const-string v1, "WEDNESDAY"

    const/4 v2, 0x2

    sget-object v3, Lcom/google/d/a/a/pa;->d:Lcom/google/d/a/a/pa;

    const/4 v4, 0x4

    sget v5, Lcom/google/android/apps/gmm/l;->pa:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/y/g;-><init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->c:Lcom/google/android/apps/gmm/y/g;

    .line 45
    new-instance v0, Lcom/google/android/apps/gmm/y/g;

    const-string v1, "THURSDAY"

    const/4 v2, 0x3

    sget-object v3, Lcom/google/d/a/a/pa;->e:Lcom/google/d/a/a/pa;

    const/4 v4, 0x5

    sget v5, Lcom/google/android/apps/gmm/l;->nx:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/y/g;-><init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->d:Lcom/google/android/apps/gmm/y/g;

    .line 46
    new-instance v0, Lcom/google/android/apps/gmm/y/g;

    const-string v1, "FRIDAY"

    const/4 v2, 0x4

    sget-object v3, Lcom/google/d/a/a/pa;->f:Lcom/google/d/a/a/pa;

    const/4 v4, 0x6

    sget v5, Lcom/google/android/apps/gmm/l;->gK:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/y/g;-><init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->e:Lcom/google/android/apps/gmm/y/g;

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/y/g;

    const-string v1, "SATURDAY"

    const/4 v2, 0x5

    sget-object v3, Lcom/google/d/a/a/pa;->g:Lcom/google/d/a/a/pa;

    const/4 v4, 0x7

    sget v5, Lcom/google/android/apps/gmm/l;->mB:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/y/g;-><init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->f:Lcom/google/android/apps/gmm/y/g;

    .line 48
    new-instance v0, Lcom/google/android/apps/gmm/y/g;

    const-string v1, "SUNDAY"

    const/4 v2, 0x6

    sget-object v3, Lcom/google/d/a/a/pa;->a:Lcom/google/d/a/a/pa;

    const/4 v4, 0x1

    sget v5, Lcom/google/android/apps/gmm/l;->nr:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/y/g;-><init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->g:Lcom/google/android/apps/gmm/y/g;

    .line 41
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/google/android/apps/gmm/y/g;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/apps/gmm/y/g;->a:Lcom/google/android/apps/gmm/y/g;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/apps/gmm/y/g;->b:Lcom/google/android/apps/gmm/y/g;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/apps/gmm/y/g;->c:Lcom/google/android/apps/gmm/y/g;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/apps/gmm/y/g;->d:Lcom/google/android/apps/gmm/y/g;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/apps/gmm/y/g;->e:Lcom/google/android/apps/gmm/y/g;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/apps/gmm/y/g;->f:Lcom/google/android/apps/gmm/y/g;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/apps/gmm/y/g;->g:Lcom/google/android/apps/gmm/y/g;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/apps/gmm/y/g;->k:[Lcom/google/android/apps/gmm/y/g;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/d/a/a/pa;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/d/a/a/pa;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 55
    iput-object p3, p0, Lcom/google/android/apps/gmm/y/g;->i:Lcom/google/d/a/a/pa;

    .line 56
    iput p4, p0, Lcom/google/android/apps/gmm/y/g;->j:I

    .line 57
    iput p5, p0, Lcom/google/android/apps/gmm/y/g;->h:I

    .line 58
    return-void
.end method

.method protected static a(I)Lcom/google/android/apps/gmm/y/g;
    .locals 5

    .prologue
    .line 61
    invoke-static {}, Lcom/google/android/apps/gmm/y/g;->values()[Lcom/google/android/apps/gmm/y/g;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 62
    iget-object v4, v0, Lcom/google/android/apps/gmm/y/g;->i:Lcom/google/d/a/a/pa;

    iget v4, v4, Lcom/google/d/a/a/pa;->i:I

    if-ne v4, p0, :cond_0

    .line 66
    :goto_1
    return-object v0

    .line 61
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 66
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected static b(I)Lcom/google/android/apps/gmm/y/g;
    .locals 5

    .prologue
    .line 70
    invoke-static {}, Lcom/google/android/apps/gmm/y/g;->values()[Lcom/google/android/apps/gmm/y/g;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 71
    iget v4, v0, Lcom/google/android/apps/gmm/y/g;->j:I

    if-ne v4, p0, :cond_0

    .line 75
    :goto_1
    return-object v0

    .line 70
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 75
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/apps/gmm/y/g;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/apps/gmm/y/g;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/y/g;

    return-object v0
.end method

.method public static values()[Lcom/google/android/apps/gmm/y/g;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/apps/gmm/y/g;->k:[Lcom/google/android/apps/gmm/y/g;

    invoke-virtual {v0}, [Lcom/google/android/apps/gmm/y/g;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/apps/gmm/y/g;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/y/g;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/gmm/y/g;->i:Lcom/google/d/a/a/pa;

    iget v0, v0, Lcom/google/d/a/a/pa;->i:I

    add-int/lit8 v0, v0, 0x1

    rem-int/lit8 v0, v0, 0x7

    invoke-static {v0}, Lcom/google/android/apps/gmm/y/g;->a(I)Lcom/google/android/apps/gmm/y/g;

    move-result-object v0

    return-object v0
.end method
