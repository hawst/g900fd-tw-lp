.class public Lcom/google/android/apps/gmm/search/restriction/c/a;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/search/restriction/d/a;
.implements Lcom/google/android/apps/gmm/search/restriction/d/d;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/qt;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/google/maps/g/qt;

.field private final c:Landroid/content/res/Resources;

.field private d:Ljava/lang/Boolean;

.field private e:Ljava/lang/Runnable;

.field private final f:Lcom/google/maps/g/qt;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/maps/g/qm;Lcom/google/r/b/a/alh;)V
    .locals 3

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->c:Landroid/content/res/Resources;

    .line 49
    const/4 v0, 0x0

    .line 48
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->d:Ljava/lang/Boolean;

    .line 50
    invoke-static {}, Lcom/google/maps/g/qt;->newBuilder()Lcom/google/maps/g/qv;

    move-result-object v0

    const/4 v1, -0x1

    .line 51
    iget v2, v0, Lcom/google/maps/g/qv;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/qv;->a:I

    iput v1, v0, Lcom/google/maps/g/qv;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->c:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/apps/gmm/l;->lE:I

    .line 52
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v2, v0, Lcom/google/maps/g/qv;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/qv;->a:I

    iput-object v1, v0, Lcom/google/maps/g/qv;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/maps/g/qv;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qt;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->f:Lcom/google/maps/g/qt;

    .line 53
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->f:Lcom/google/maps/g/qt;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    .line 54
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->f:Lcom/google/maps/g/qt;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 55
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-virtual {p2}, Lcom/google/maps/g/qm;->g()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 57
    invoke-virtual {p0, p3}, Lcom/google/android/apps/gmm/search/restriction/c/a;->a(Lcom/google/r/b/a/alh;)V

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/maps/g/qo;)Lcom/google/maps/g/qo;
    .locals 0

    .prologue
    .line 70
    return-object p1
.end method

.method public final a(Lcom/google/r/b/a/alm;)Lcom/google/r/b/a/alm;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->f:Lcom/google/maps/g/qt;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    iget v0, v0, Lcom/google/maps/g/qt;->b:I

    iget v1, p1, Lcom/google/r/b/a/alm;->a:I

    or-int/lit16 v1, v1, 0x100

    iput v1, p1, Lcom/google/r/b/a/alm;->a:I

    iput v0, p1, Lcom/google/r/b/a/alm;->d:I

    .line 65
    :cond_0
    return-object p1
.end method

.method public final a()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final a(I)Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 141
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qt;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/r/b/a/alh;)V
    .locals 4

    .prologue
    .line 83
    iget v0, p1, Lcom/google/r/b/a/alh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v1, 0x20

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 84
    iget v0, p1, Lcom/google/r/b/a/alh;->m:I

    move v1, v0

    .line 85
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qt;

    .line 86
    iget v3, v0, Lcom/google/maps/g/qt;->b:I

    if-ne v3, v1, :cond_0

    .line 87
    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    .line 91
    :cond_1
    return-void

    .line 83
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->f:Lcom/google/maps/g/qt;

    iget v0, v0, Lcom/google/maps/g/qt;->b:I

    move v1, v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->e:Ljava/lang/Runnable;

    .line 96
    return-void
.end method

.method public final ag_()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 110
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    iget-object v2, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->f:Lcom/google/maps/g/qt;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    invoke-virtual {v0}, Lcom/google/maps/g/qt;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final ai_()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    iget-object v1, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->f:Lcom/google/maps/g/qt;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 150
    const-string v0, ""

    .line 153
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qt;

    invoke-virtual {v0}, Lcom/google/maps/g/qt;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final c(I)Lcom/google/android/libraries/curvular/cf;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qt;

    iput-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->b:Lcom/google/maps/g/qt;

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->e:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 173
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->c:Landroid/content/res/Resources;

    const v1, -0x21524111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(I)Lcom/google/android/apps/gmm/z/b/l;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 184
    const/4 v0, 0x0

    .line 192
    :goto_0
    return-object v0

    .line 187
    :cond_0
    if-nez p1, :cond_1

    .line 188
    sget-object v0, Lcom/google/b/f/t;->aU:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0

    .line 192
    :cond_1
    sget-object v0, Lcom/google/b/f/t;->aV:Lcom/google/b/f/t;

    invoke-static {v0}, Lcom/google/android/apps/gmm/z/b/l;->a(Lcom/google/b/f/cq;)Lcom/google/android/apps/gmm/z/b/l;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->c:Landroid/content/res/Resources;

    const v1, -0x21524111

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 159
    const-string v0, ""

    .line 162
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/search/restriction/c/a;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/qt;

    invoke-virtual {v0}, Lcom/google/maps/g/qt;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g(I)Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 198
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
