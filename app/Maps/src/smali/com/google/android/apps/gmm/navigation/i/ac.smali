.class public Lcom/google/android/apps/gmm/navigation/i/ac;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/i/aa;

.field final b:I

.field final c:I

.field final d:Lcom/google/android/apps/gmm/map/b/a/y;

.field final e:Lcom/google/android/apps/gmm/map/b/a/y;

.field final f:[Lcom/google/android/apps/gmm/map/u/a/f;

.field g:Lcom/google/maps/g/a/ez;

.field h:Lcom/google/maps/g/a/fb;

.field i:Lcom/google/maps/g/a/fd;

.field private final j:Lcom/google/android/apps/gmm/navigation/i/ad;

.field private final k:Lcom/google/android/apps/gmm/map/u/a/b;

.field private final l:Lcom/google/android/apps/gmm/map/b/a/ae;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/navigation/i/aa;I)V
    .locals 9
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/ae;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    sget-object v0, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    .line 119
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->k:Lcom/google/android/apps/gmm/map/u/a/b;

    .line 120
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->l:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 121
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    .line 122
    iput p4, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    .line 123
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v5, v0, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    .line 125
    aget-object v0, v5, p4

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/navigation/i/ad;

    move-result-object v0

    .line 126
    array-length v1, v5

    move v2, p4

    move-object v3, v0

    .line 128
    :goto_0
    if-ge v2, v1, :cond_11

    .line 129
    aget-object v0, v5, v2

    const/4 v6, 0x0

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/u/a/e;->d:[Lcom/google/android/apps/gmm/map/u/a/f;

    aget-object v0, v0, v6

    .line 130
    sget-object v6, Lcom/google/android/apps/gmm/map/u/a/e;->a:Lcom/google/android/apps/gmm/map/u/a/f;

    if-eq v0, v6, :cond_0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 131
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    array-length v6, v0

    add-int/lit8 v6, v6, -0x1

    if-ne v2, v6, :cond_1

    sget-object v0, Lcom/google/maps/g/a/ez;->D:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    sget-object v0, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_e

    .line 134
    add-int/lit8 v0, v2, 0x1

    .line 140
    :goto_2
    iput-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->j:Lcom/google/android/apps/gmm/navigation/i/ad;

    .line 141
    iput v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    .line 142
    if-nez p4, :cond_f

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/aa;->a:Lcom/google/android/apps/gmm/navigation/i/e;

    iget-object v2, v2, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 143
    :goto_3
    iput-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 144
    if-ne v0, v1, :cond_10

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/aa;->b:Lcom/google/android/apps/gmm/navigation/i/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/i/e;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 145
    :goto_4
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 146
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/apps/gmm/map/u/a/f;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->f:[Lcom/google/android/apps/gmm/map/u/a/f;

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->f:[Lcom/google/android/apps/gmm/map/u/a/f;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 148
    const-string v0, "Describer"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x11

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Created subPath: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/m;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 149
    return-void

    .line 133
    :cond_1
    aget-object v6, v0, v2

    add-int/lit8 v7, v2, 0x1

    aget-object v7, v0, v7

    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/ad;->b:Lcom/google/android/apps/gmm/navigation/i/ad;

    if-ne v3, v0, :cond_6

    invoke-static {v7}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/navigation/i/ad;

    move-result-object v0

    sget-object v8, Lcom/google/android/apps/gmm/navigation/i/ad;->b:Lcom/google/android/apps/gmm/navigation/i/ad;

    if-eq v0, v8, :cond_3

    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/16 v0, 0x2d

    invoke-direct {p0, v6, v7, v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;I)Ljava/util/List;

    move-result-object v0

    invoke-static {v6, v7, v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;Ljava/util/List;)Lcom/google/maps/g/a/fb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    sget-object v6, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    if-eq v0, v6, :cond_4

    sget-object v0, Lcom/google/maps/g/a/ez;->k:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    const/4 v0, 0x1

    :goto_5
    if-nez v0, :cond_5

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_6
    invoke-static {v7}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/navigation/i/ad;

    move-result-object v0

    sget-object v8, Lcom/google/android/apps/gmm/navigation/i/ad;->b:Lcom/google/android/apps/gmm/navigation/i/ad;

    if-ne v0, v8, :cond_c

    const/16 v0, 0x2d

    invoke-direct {p0, v6, v7, v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;I)Ljava/util/List;

    move-result-object v0

    invoke-static {v6, v7, v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;Ljava/util/List;)Lcom/google/maps/g/a/fb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    iget v0, v6, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    const/16 v8, 0x8

    if-eq v0, v8, :cond_7

    const/16 v8, 0x9

    if-ne v0, v8, :cond_8

    :cond_7
    const/4 v0, 0x1

    :goto_6
    if-eqz v0, :cond_9

    const/4 v0, 0x1

    :goto_7
    if-eqz v0, :cond_b

    sget-object v0, Lcom/google/maps/g/a/ez;->j:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    :goto_8
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v0, 0x0

    goto :goto_6

    :cond_9
    iget v0, v6, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    iget v6, v7, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    if-le v0, v6, :cond_a

    const/4 v0, 0x1

    goto :goto_7

    :cond_a
    const/4 v0, 0x0

    goto :goto_7

    :cond_b
    sget-object v0, Lcom/google/maps/g/a/ez;->i:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    goto :goto_8

    :cond_c
    invoke-direct {p0, v6, v7}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)Z

    move-result v0

    if-nez v0, :cond_d

    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_d
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 137
    :cond_e
    aget-object v0, v5, v2

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/navigation/i/ad;

    move-result-object v3

    .line 128
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 142
    :cond_f
    aget-object v3, v5, p4

    const/4 v6, 0x0

    .line 143
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-virtual {v3, v6, v2}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    goto/16 :goto_3

    .line 144
    :cond_10
    add-int/lit8 v0, v0, -0x1

    aget-object v0, v5, v0

    .line 145
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/u/a/e;->a()Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v0

    goto/16 :goto_4

    :cond_11
    move v0, v1

    goto/16 :goto_2
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;Lcom/google/android/apps/gmm/navigation/i/aa;IILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;[Lcom/google/android/apps/gmm/map/u/a/f;Lcom/google/maps/g/a/ez;Lcom/google/maps/g/a/fb;Lcom/google/maps/g/a/fd;)V
    .locals 1
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/ae;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    sget-object v0, Lcom/google/maps/g/a/fd;->a:Lcom/google/maps/g/a/fd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    .line 177
    iput-object p1, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->k:Lcom/google/android/apps/gmm/map/u/a/b;

    .line 178
    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->l:Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 179
    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    .line 180
    iput p4, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    .line 181
    iput p5, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    .line 182
    iput-object p6, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 183
    iput-object p7, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 184
    iput-object p8, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->f:[Lcom/google/android/apps/gmm/map/u/a/f;

    .line 185
    iput-object p9, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 186
    iput-object p10, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    .line 187
    iput-object p11, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    .line 188
    iget-object v0, p3, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    aget-object v0, v0, p4

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/navigation/i/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->j:Lcom/google/android/apps/gmm/navigation/i/ad;

    .line 189
    return-void
.end method

.method private static a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/navigation/i/ad;
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lcom/google/android/apps/gmm/map/u/a/e;->g:I

    packed-switch v0, :pswitch_data_0

    .line 202
    :pswitch_0
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/ad;->a:Lcom/google/android/apps/gmm/navigation/i/ad;

    :goto_0
    return-object v0

    .line 198
    :pswitch_1
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/ad;->b:Lcom/google/android/apps/gmm/navigation/i/ad;

    goto :goto_0

    .line 200
    :pswitch_2
    sget-object v0, Lcom/google/android/apps/gmm/navigation/i/ad;->c:Lcom/google/android/apps/gmm/navigation/i/ad;

    goto :goto_0

    .line 192
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;Ljava/util/List;)Lcom/google/maps/g/a/fb;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            ">;)",
            "Lcom/google/maps/g/a/fb;"
        }
    .end annotation

    .prologue
    .line 286
    invoke-static {p0, p1}, Lcom/google/android/apps/gmm/navigation/i/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)I

    move-result v4

    .line 287
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    :cond_0
    sget-object v0, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    .line 299
    :goto_0
    return-object v0

    .line 290
    :cond_1
    const/4 v3, 0x0

    .line 291
    const/4 v1, 0x0

    .line 292
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/e;

    .line 293
    invoke-static {p0, v0}, Lcom/google/android/apps/gmm/navigation/i/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)I

    move-result v2

    .line 294
    if-eqz v3, :cond_2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v7

    if-le v6, v7, :cond_5

    :cond_2
    move-object v1, v0

    move v0, v2

    :goto_2
    move-object v3, v1

    move v1, v0

    .line 298
    goto :goto_1

    .line 299
    :cond_3
    if-le v4, v1, :cond_4

    sget-object v0, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    goto :goto_0

    :cond_5
    move v0, v1

    move-object v1, v3

    goto :goto_2
.end method

.method private a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/u/a/e;",
            ">;"
        }
    .end annotation

    .prologue
    .line 353
    const/4 v1, 0x0

    .line 354
    const/4 v0, 0x0

    move v5, v0

    move-object v0, v1

    move v1, v5

    :goto_0
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 355
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    aget-object v2, v2, v1

    .line 356
    iget-object v3, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->k:Lcom/google/android/apps/gmm/map/u/a/b;

    iget-object v4, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->l:Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/gmm/map/u/a/a;->a(Lcom/google/android/apps/gmm/map/u/a/b;Lcom/google/android/apps/gmm/map/b/a/ae;)Lcom/google/android/apps/gmm/map/u/a/e;

    move-result-object v2

    .line 357
    if-eqz v2, :cond_1

    if-eq v2, p2, :cond_1

    .line 358
    invoke-static {p1, v2}, Lcom/google/android/apps/gmm/navigation/i/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)I

    move-result v3

    .line 359
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-gt v3, p3, :cond_1

    .line 360
    if-nez v0, :cond_0

    .line 361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 363
    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 367
    :cond_2
    return-object v0
.end method

.method private a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)Z
    .locals 10

    .prologue
    const/16 v9, 0x14

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 405
    invoke-static {p1, p2}, Lcom/google/android/apps/gmm/navigation/i/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)I

    move-result v4

    .line 406
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v5

    .line 417
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/u/a/e;->d:[Lcom/google/android/apps/gmm/map/u/a/f;

    aget-object v0, v0, v2

    iget-object v3, p2, Lcom/google/android/apps/gmm/map/u/a/e;->d:[Lcom/google/android/apps/gmm/map/u/a/f;

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/map/u/a/f;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v3, v1

    .line 419
    :goto_0
    if-lez v4, :cond_2

    sget-object v0, Lcom/google/maps/g/a/fb;->b:Lcom/google/maps/g/a/fb;

    :goto_1
    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    .line 421
    const/16 v0, 0x2d

    if-gt v5, v0, :cond_9

    .line 422
    add-int/lit8 v0, v5, 0x1e

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;I)Ljava/util/List;

    move-result-object v6

    .line 425
    if-eqz v6, :cond_3

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/e;

    invoke-static {v0}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;)Lcom/google/android/apps/gmm/navigation/i/ad;

    move-result-object v0

    sget-object v8, Lcom/google/android/apps/gmm/navigation/i/ad;->b:Lcom/google/android/apps/gmm/navigation/i/ad;

    if-ne v0, v8, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_1
    move v3, v2

    .line 417
    goto :goto_0

    .line 419
    :cond_2
    sget-object v0, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    goto :goto_1

    .line 426
    :cond_3
    iget v0, p1, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    iget v7, p2, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    if-ne v0, v7, :cond_5

    .line 427
    iget v7, p1, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    if-eqz v6, :cond_5

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_4
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    if-ge v0, v7, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 431
    :cond_5
    if-eqz v6, :cond_6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_6

    if-ge v5, v9, :cond_6

    .line 434
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/e;

    .line 433
    invoke-static {p1, v0}, Lcom/google/android/apps/gmm/navigation/i/c;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;)I

    move-result v5

    .line 435
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/u/a/e;

    iget v0, v0, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    iget v7, p2, Lcom/google/android/apps/gmm/map/u/a/e;->h:I

    if-ne v0, v7, :cond_6

    if-ge v5, v9, :cond_6

    .line 437
    sget-object v0, Lcom/google/maps/g/a/ez;->k:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 438
    invoke-static {p1, p2, v6}, Lcom/google/android/apps/gmm/navigation/i/ac;->a(Lcom/google/android/apps/gmm/map/u/a/e;Lcom/google/android/apps/gmm/map/u/a/e;Ljava/util/List;)Lcom/google/maps/g/a/fb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    move v0, v1

    .line 472
    :goto_4
    return v0

    .line 445
    :cond_6
    const/16 v0, 0x1e

    if-lt v4, v0, :cond_7

    if-nez v3, :cond_8

    :cond_7
    if-eqz v6, :cond_c

    .line 446
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_c

    .line 447
    :cond_8
    sget-object v0, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 448
    sget-object v0, Lcom/google/maps/g/a/fd;->b:Lcom/google/maps/g/a/fd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    move v0, v1

    .line 449
    goto :goto_4

    .line 451
    :cond_9
    const/16 v0, 0x87

    if-gt v5, v0, :cond_a

    .line 452
    sget-object v0, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 453
    sget-object v0, Lcom/google/maps/g/a/fd;->c:Lcom/google/maps/g/a/fd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    move v0, v1

    .line 454
    goto :goto_4

    .line 455
    :cond_a
    const/16 v0, 0xb3

    if-gt v5, v0, :cond_b

    .line 456
    sget-object v0, Lcom/google/maps/g/a/ez;->f:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 457
    sget-object v0, Lcom/google/maps/g/a/fd;->d:Lcom/google/maps/g/a/fd;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    move v0, v1

    .line 458
    goto :goto_4

    .line 460
    :cond_b
    sget-object v0, Lcom/google/maps/g/a/ez;->h:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 463
    sget-object v0, Lcom/google/maps/g/a/fb;->a:Lcom/google/maps/g/a/fb;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    move v0, v1

    .line 464
    goto :goto_4

    .line 467
    :cond_c
    if-eqz v3, :cond_d

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/u/a/e;->f:[Lcom/google/android/apps/gmm/map/u/a/a;

    array-length v0, v0

    if-le v0, v1, :cond_d

    .line 468
    sget-object v0, Lcom/google/maps/g/a/ez;->c:Lcom/google/maps/g/a/ez;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    .line 469
    sget-object v0, Lcom/google/maps/g/a/fb;->c:Lcom/google/maps/g/a/fb;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    move v0, v1

    .line 470
    goto :goto_4

    :cond_d
    move v0, v2

    .line 472
    goto :goto_4
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/ad;)D
    .locals 19

    .prologue
    .line 517
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v9, v2, Lcom/google/android/apps/gmm/navigation/i/aa;->c:[Lcom/google/android/apps/gmm/map/u/a/e;

    .line 518
    const/4 v3, 0x0

    .line 519
    new-instance v10, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v10}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    .line 520
    const-wide/16 v4, 0x0

    .line 521
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    move/from16 v16, v2

    move-wide/from16 v17, v4

    move/from16 v5, v16

    move-object v4, v3

    move-wide/from16 v2, v17

    :goto_0
    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    if-ge v5, v6, :cond_6

    .line 522
    aget-object v11, v9, v5

    .line 523
    iget-object v6, v11, Lcom/google/android/apps/gmm/map/u/a/e;->e:Lcom/google/android/apps/gmm/map/b/a/ab;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v6, v6

    div-int/lit8 v7, v6, 0x3

    .line 526
    if-nez v5, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v6, v6, Lcom/google/android/apps/gmm/navigation/i/aa;->a:Lcom/google/android/apps/gmm/navigation/i/e;

    iget v6, v6, Lcom/google/android/apps/gmm/navigation/i/e;->d:I

    .line 527
    :goto_1
    add-int/lit8 v7, v7, -0x1

    .line 528
    array-length v8, v9

    add-int/lit8 v8, v8, -0x1

    if-ne v5, v8, :cond_0

    .line 532
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v7, v7, Lcom/google/android/apps/gmm/navigation/i/aa;->b:Lcom/google/android/apps/gmm/navigation/i/e;

    iget v7, v7, Lcom/google/android/apps/gmm/navigation/i/e;->d:I

    add-int/lit8 v7, v7, 0x1

    :cond_0
    move v8, v6

    .line 534
    :goto_2
    if-gt v8, v7, :cond_5

    .line 535
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    if-ne v5, v12, :cond_2

    if-ne v8, v6, :cond_2

    .line 536
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v12, v12, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v12, v10, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 542
    :goto_3
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    .line 543
    if-eqz v4, :cond_4

    .line 547
    invoke-virtual {v4, v10}, Lcom/google/android/apps/gmm/map/b/a/y;->c(Lcom/google/android/apps/gmm/map/b/a/y;)F

    move-result v12

    float-to-double v12, v12

    .line 548
    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/b/a/y;->f()D

    move-result-wide v14

    div-double/2addr v12, v14

    add-double/2addr v2, v12

    .line 552
    :goto_4
    iget v12, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v12, v4, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v12, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v12, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v12, v10, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v12, v4, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 534
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 526
    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    .line 537
    :cond_2
    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    add-int/lit8 v12, v12, -0x1

    if-ne v5, v12, :cond_3

    if-ne v8, v7, :cond_3

    .line 538
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v13, v12, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v13, v10, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v12, v12, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    iput v12, v10, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    goto :goto_3

    .line 540
    :cond_3
    invoke-virtual {v11, v8, v10}, Lcom/google/android/apps/gmm/map/u/a/e;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_3

    .line 550
    :cond_4
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    goto :goto_4

    .line 521
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 555
    :cond_6
    return-wide v2
.end method

.method public final a()I
    .locals 4

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->a:Lcom/google/android/apps/gmm/navigation/i/aa;

    iget-object v2, v0, Lcom/google/android/apps/gmm/navigation/i/aa;->d:[I

    .line 480
    const/4 v1, 0x0

    .line 481
    iget v0, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->b:I

    :goto_0
    iget v3, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->c:I

    if-ge v0, v3, :cond_0

    .line 482
    aget v3, v2, v0

    add-int/2addr v1, v3

    .line 481
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 484
    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 612
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 613
    const-string v0, "type: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->g:Lcom/google/maps/g/a/ez;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 614
    const-string v0, " side: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->h:Lcom/google/maps/g/a/fb;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 615
    const-string v0, " turnType: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->i:Lcom/google/maps/g/a/fd;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " names: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->f:[Lcom/google/android/apps/gmm/map/u/a/f;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 617
    iget-object v4, v4, Lcom/google/android/apps/gmm/map/u/a/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 618
    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 616
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 620
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    .line 621
    const-string v0, " start: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 622
    const-string v0, " end: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->e:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/b/a/y;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    const-string v0, " type: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/navigation/i/ac;->j:Lcom/google/android/apps/gmm/navigation/i/ad;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 624
    const-string v0, "]"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 625
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
