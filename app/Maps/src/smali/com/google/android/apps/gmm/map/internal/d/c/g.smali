.class abstract Lcom/google/android/apps/gmm/map/internal/d/c/g;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/map/internal/d/c/b/f;


# instance fields
.field private final a:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

.field private final b:[Landroid/graphics/Bitmap;

.field private final c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

.field private final d:Lcom/google/android/apps/gmm/shared/c/f;

.field private final e:I

.field final i:[Lcom/google/android/apps/gmm/map/internal/d/c/b/a;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;Lcom/google/android/apps/gmm/map/internal/d/c/b/f;IILcom/google/android/apps/gmm/shared/c/f;)V
    .locals 1

    .prologue
    .line 565
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 566
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->a:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    .line 567
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    .line 568
    iput-object p5, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->d:Lcom/google/android/apps/gmm/shared/c/f;

    .line 569
    iput p3, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->e:I

    .line 570
    new-array v0, p4, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->b:[Landroid/graphics/Bitmap;

    .line 571
    new-array v0, p4, [Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->i:[Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    .line 572
    return-void
.end method


# virtual methods
.method public abstract a([Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 597
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 644
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 603
    :cond_1
    const/4 v0, -0x1

    move v1, v3

    .line 608
    :goto_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->i:[Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    array-length v2, v2

    if-ge v3, v2, :cond_4

    .line 609
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->i:[Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    aget-object v2, v2, v3

    if-ne v2, p1, :cond_8

    .line 611
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->b:[Landroid/graphics/Bitmap;

    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->b:I

    if-eq v0, v4, :cond_3

    const/4 v0, 0x0

    :cond_2
    :goto_2
    aput-object v0, v2, v3

    move v2, v3

    .line 613
    :goto_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->b:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v3

    if-eqz v0, :cond_7

    .line 614
    add-int/lit8 v0, v1, 0x1

    .line 608
    :goto_4
    add-int/lit8 v3, v3, 0x1

    move v1, v0

    move v0, v2

    goto :goto_1

    .line 611
    :cond_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->e:Lcom/google/android/apps/gmm/map/internal/d/c/b/d;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->d()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 597
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 618
    :cond_4
    :try_start_2
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->b:[Landroid/graphics/Bitmap;

    aget-object v0, v2, v0

    if-nez v0, :cond_5

    .line 620
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(I)V

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(J)V

    .line 622
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Z)V

    .line 623
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->a:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    if-eqz v0, :cond_5

    .line 624
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->a:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-interface {v0, v2}, Lcom/google/android/apps/gmm/map/internal/d/c/b/f;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    .line 628
    :cond_5
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->e:I

    if-ne v1, v0, :cond_0

    .line 629
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->b:[Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/g;->a([Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 630
    if-eqz v0, :cond_6

    .line 631
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Landroid/graphics/Bitmap;)V

    .line 632
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(I)V

    .line 638
    :goto_5
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->d:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(J)V

    .line 639
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(Z)V

    .line 640
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->a:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    if-eqz v0, :cond_0

    .line 641
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->a:Lcom/google/android/apps/gmm/map/internal/d/c/b/f;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/f;->a(Lcom/google/android/apps/gmm/map/internal/d/c/b/a;)V

    goto/16 :goto_0

    .line 634
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/c/g;->c:Lcom/google/android/apps/gmm/map/internal/d/c/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/c/b/a;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_4

    :cond_8
    move v2, v0

    goto/16 :goto_3
.end method
