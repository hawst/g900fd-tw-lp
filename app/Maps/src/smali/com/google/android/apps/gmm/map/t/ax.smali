.class public Lcom/google/android/apps/gmm/map/t/ax;
.super Lcom/google/android/apps/gmm/map/t/aw;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    const-string v0, "precision mediump float;\nvarying vec2 vTextureCoord;\nuniform sampler2D sTexture0;\nuniform sampler2D sTexture1;\nuniform lowp vec3 uFillColor;\nuniform lowp vec3 uBlurColor;\nuniform lowp float uBlurScale;\nuniform float brightnessScale;\nvoid main() {\n  lowp float blur1 = texture2D(sTexture0, vTextureCoord).a;\n  lowp float blur2 = texture2D(sTexture1, vTextureCoord).a;\n  lowp float blur = mix(blur1, blur2, uBlurScale);\n  lowp vec3 c = mix(uBlurColor, uFillColor, blur);\n  gl_FragColor = vec4(c * brightnessScale, 1.0);\n}\n"

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/map/t/aw;-><init>(Ljava/lang/String;)V

    .line 118
    return-void
.end method
