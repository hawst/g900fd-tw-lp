.class public Lcom/google/android/apps/gmm/startpage/ag;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/startpage/ae;
.implements Lcom/google/android/gms/common/c;
.implements Lcom/google/android/gms/common/d;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field final b:Lcom/google/android/apps/gmm/base/a;

.field final c:Lcom/google/android/apps/gmm/map/t;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final d:Lcom/google/android/apps/gmm/startpage/j;

.field final e:Lcom/google/android/apps/gmm/startpage/y;

.field f:Lcom/google/android/gms/location/reporting/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field final g:Lcom/google/android/apps/gmm/startpage/ak;

.field h:Lcom/google/android/apps/gmm/startpage/a;

.field i:Z

.field j:Z

.field k:Lcom/google/android/apps/gmm/startpage/d/a;

.field l:Lcom/google/android/apps/gmm/startpage/ai;

.field private final m:Landroid/content/Context;

.field private n:Lcom/google/r/b/a/pk;

.field private final o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/google/android/apps/gmm/startpage/a/a;",
            ">;"
        }
    .end annotation
.end field

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/google/android/apps/gmm/startpage/ag;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;Lcom/google/android/apps/gmm/startpage/j;)V
    .locals 8
    .param p2    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 109
    .line 110
    invoke-interface {p1}, Lcom/google/android/apps/gmm/base/a;->t()Lcom/google/android/apps/gmm/shared/net/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/a/b;->t()Lcom/google/r/b/a/pk;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/gmm/startpage/y;

    invoke-direct {v6, p1, p2, p3}, Lcom/google/android/apps/gmm/startpage/y;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;)V

    .line 112
    invoke-static {p3}, Lcom/google/android/apps/gmm/map/h/f;->b(Landroid/content/Context;)Z

    move-result v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    .line 109
    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/startpage/ag;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;Lcom/google/android/apps/gmm/startpage/j;Lcom/google/r/b/a/pk;Lcom/google/android/apps/gmm/startpage/y;Z)V

    .line 113
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Landroid/content/Context;Lcom/google/android/apps/gmm/startpage/j;Lcom/google/r/b/a/pk;Lcom/google/android/apps/gmm/startpage/y;Z)V
    .locals 6
    .param p2    # Lcom/google/android/apps/gmm/map/t;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->o:Ljava/util/Set;

    .line 99
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/a;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-direct {v0, v2, v2, v2, v1}, Lcom/google/android/apps/gmm/startpage/d/a;-><init>(Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/o/h/a/gx;Lcom/google/android/apps/gmm/startpage/d/e;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->p:Z

    .line 121
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    .line 122
    iput-object p2, p0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    .line 123
    iput-object p3, p0, Lcom/google/android/apps/gmm/startpage/ag;->m:Landroid/content/Context;

    .line 124
    iput-object p4, p0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    .line 125
    iput-object p5, p0, Lcom/google/android/apps/gmm/startpage/ag;->n:Lcom/google/r/b/a/pk;

    .line 126
    iput-object p6, p0, Lcom/google/android/apps/gmm/startpage/ag;->e:Lcom/google/android/apps/gmm/startpage/y;

    .line 127
    new-instance v0, Lcom/google/android/apps/gmm/startpage/ak;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p6

    move-object v4, p0

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/startpage/ak;-><init>(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/startpage/y;Lcom/google/android/apps/gmm/startpage/ag;Lcom/google/android/apps/gmm/startpage/j;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->g:Lcom/google/android/apps/gmm/startpage/ak;

    .line 129
    return-void
.end method

.method static a(Lcom/google/android/apps/gmm/startpage/a;Lcom/google/android/apps/gmm/startpage/aj;Lcom/google/o/h/a/gx;)Lcom/google/android/apps/gmm/startpage/d/a;
    .locals 5
    .param p1    # Lcom/google/android/apps/gmm/startpage/aj;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param
    .param p2    # Lcom/google/o/h/a/gx;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 257
    if-nez p1, :cond_0

    .line 258
    new-instance v0, Lcom/google/android/apps/gmm/startpage/d/a;

    sget-object v2, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-direct {v0, v1, v1, p2, v2}, Lcom/google/android/apps/gmm/startpage/d/a;-><init>(Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/o/h/a/gx;Lcom/google/android/apps/gmm/startpage/d/e;)V

    .line 265
    :goto_0
    return-object v0

    .line 262
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aj;->a:Ljava/util/List;

    iget-wide v2, p1, Lcom/google/android/apps/gmm/startpage/aj;->d:J

    .line 261
    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/gmm/startpage/a;->b(Ljava/util/List;J)Ljava/util/List;

    move-result-object v0

    .line 260
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    iget v2, v0, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v2, v2, 0x40

    const/16 v4, 0x40

    if-ne v2, v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/google/o/h/a/lh;->j:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/hb;->g()Lcom/google/o/h/a/hb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/hb;

    .line 264
    :goto_2
    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/a;

    iget-object v2, p1, Lcom/google/android/apps/gmm/startpage/aj;->b:Ljava/lang/String;

    .line 265
    iget-object v3, p1, Lcom/google/android/apps/gmm/startpage/aj;->c:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-direct {v1, v0, v2, p2, v3}, Lcom/google/android/apps/gmm/startpage/d/a;-><init>(Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/o/h/a/gx;Lcom/google/android/apps/gmm/startpage/d/e;)V

    move-object v0, v1

    goto :goto_0

    .line 260
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2
.end method

.method private a(Lcom/google/o/h/a/gx;)Lcom/google/o/h/a/gx;
    .locals 3
    .param p1    # Lcom/google/o/h/a/gx;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 405
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->j:Z

    if-nez v0, :cond_0

    .line 416
    :goto_0
    return-object p1

    .line 409
    :cond_0
    if-nez p1, :cond_1

    .line 410
    const/4 p1, 0x0

    goto :goto_0

    .line 412
    :cond_1
    invoke-static {}, Lcom/google/o/h/a/gx;->newBuilder()Lcom/google/o/h/a/gz;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/o/h/a/gz;->a(Lcom/google/o/h/a/gx;)Lcom/google/o/h/a/gz;

    move-result-object v2

    .line 413
    iget-object v0, p1, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/lang/String;

    :goto_1
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v1, p1, Lcom/google/o/h/a/gx;->j:Ljava/lang/Object;

    :cond_3
    move-object v0, v1

    goto :goto_1

    :cond_4
    iget v1, v2, Lcom/google/o/h/a/gz;->a:I

    or-int/lit16 v1, v1, 0x80

    iput v1, v2, Lcom/google/o/h/a/gz;->a:I

    iput-object v0, v2, Lcom/google/o/h/a/gz;->d:Ljava/lang/Object;

    .line 414
    iget-object v0, p1, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/String;

    :goto_2
    if-nez v0, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_5
    check-cast v0, Lcom/google/n/f;

    invoke-virtual {v0}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/n/f;->e()Z

    move-result v0

    if-eqz v0, :cond_6

    iput-object v1, p1, Lcom/google/o/h/a/gx;->h:Ljava/lang/Object;

    :cond_6
    move-object v0, v1

    goto :goto_2

    :cond_7
    iget v1, v2, Lcom/google/o/h/a/gz;->a:I

    or-int/lit8 v1, v1, 0x10

    iput v1, v2, Lcom/google/o/h/a/gz;->a:I

    iput-object v0, v2, Lcom/google/o/h/a/gz;->b:Ljava/lang/Object;

    .line 415
    iget v0, v2, Lcom/google/o/h/a/gz;->a:I

    and-int/lit8 v0, v0, -0x21

    iput v0, v2, Lcom/google/o/h/a/gz;->a:I

    invoke-static {}, Lcom/google/o/h/a/gx;->h()Lcom/google/o/h/a/gx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/o/h/a/gx;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/o/h/a/gz;->c:Ljava/lang/Object;

    .line 416
    invoke-virtual {v2}, Lcom/google/o/h/a/gz;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gx;

    move-object p1, v0

    goto :goto_0
.end method

.method private static a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/aa;)Lcom/google/o/h/a/sa;
    .locals 3

    .prologue
    .line 361
    invoke-static {}, Lcom/google/o/h/a/sa;->newBuilder()Lcom/google/o/h/a/sc;

    move-result-object v0

    .line 362
    if-nez p0, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_0
    iget v1, v0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/o/h/a/sc;->a:I

    iget v1, p0, Lcom/google/o/h/a/dq;->s:I

    iput v1, v0, Lcom/google/o/h/a/sc;->b:I

    .line 363
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->d:Z

    iget v2, v0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v0, Lcom/google/o/h/a/sc;->a:I

    iput-boolean v1, v0, Lcom/google/o/h/a/sc;->d:Z

    .line 364
    iget-boolean v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->e:Z

    iget v2, v0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v2, v2, 0x8

    iput v2, v0, Lcom/google/o/h/a/sc;->a:I

    iput-boolean v1, v0, Lcom/google/o/h/a/sc;->e:Z

    .line 365
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->f:Lcom/google/o/h/a/eq;

    if-eqz v1, :cond_2

    .line 366
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->f:Lcom/google/o/h/a/eq;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v2, v0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/o/h/a/sc;->a:I

    iget v1, v1, Lcom/google/o/h/a/eq;->d:I

    iput v1, v0, Lcom/google/o/h/a/sc;->c:I

    .line 368
    :cond_2
    invoke-virtual {v0}, Lcom/google/o/h/a/sc;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sa;

    return-object v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/startpage/a/a;

    .line 232
    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    .line 233
    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    iget-boolean v3, p0, Lcom/google/android/apps/gmm/startpage/ag;->p:Z

    .line 232
    invoke-interface {v0, v2, v3}, Lcom/google/android/apps/gmm/startpage/a/a;->a(Lcom/google/o/h/a/gx;Z)V

    goto :goto_0

    .line 235
    :cond_0
    return-void
.end method

.method private b(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 5
    .param p2    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 455
    if-eqz p2, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_7

    .line 456
    iput-boolean v2, p0, Lcom/google/android/apps/gmm/startpage/ag;->j:Z

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/o/h/a/gx;)Lcom/google/o/h/a/gx;

    move-result-object v0

    new-instance v1, Lcom/google/android/apps/gmm/startpage/d/a;

    iget-object v2, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    iget-object v2, v2, Lcom/google/android/apps/gmm/startpage/d/a;->a:Lcom/google/o/h/a/hb;

    iget-object v3, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    iget-object v3, v3, Lcom/google/android/apps/gmm/startpage/d/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    iget-object v4, v4, Lcom/google/android/apps/gmm/startpage/d/a;->d:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/android/apps/gmm/startpage/d/a;-><init>(Lcom/google/o/h/a/hb;Ljava/lang/String;Lcom/google/o/h/a/gx;Lcom/google/android/apps/gmm/startpage/d/e;)V

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/android/apps/gmm/startpage/d/a;)V

    .line 460
    :goto_1
    return-void

    .line 455
    :cond_0
    iget-object v1, p1, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    if-eqz v1, :cond_1

    iget v0, v1, Lcom/google/r/b/a/xt;->a:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_2
    if-nez v0, :cond_3

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_2

    :cond_3
    iget-object v0, v1, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/mj;

    iget v1, v0, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v1}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v1

    if-nez v1, :cond_4

    sget-object v1, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_4
    sget-object v4, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    if-eq v1, v4, :cond_6

    iget v0, v0, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v0}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_5
    sget-object v1, Lcom/google/o/h/a/mo;->d:Lcom/google/o/h/a/mo;

    if-eq v0, v1, :cond_6

    move v0, v2

    goto :goto_0

    :cond_6
    move v0, v3

    goto :goto_0

    .line 458
    :cond_7
    iput-boolean v3, p0, Lcom/google/android/apps/gmm/startpage/ag;->j:Z

    goto :goto_1
.end method

.method private c(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 2
    .param p2    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->e:Lcom/google/android/apps/gmm/startpage/y;

    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/y;->e:Lcom/google/android/apps/gmm/startpage/z;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/startpage/z;->c(Lcom/google/android/apps/gmm/startpage/aa;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    .line 465
    :cond_0
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    sget-object v1, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 466
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->d:Lcom/google/android/apps/gmm/startpage/af;

    if-ne v0, v1, :cond_2

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->g:Lcom/google/android/apps/gmm/startpage/ak;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/startpage/ak;->a(Lcom/google/android/apps/gmm/startpage/aa;)V

    .line 472
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/startpage/ag;->b(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 478
    :cond_1
    :goto_0
    return-void

    .line 473
    :cond_2
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    sget-object v1, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 474
    iget-object v0, p1, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v1, Lcom/google/android/apps/gmm/startpage/af;->b:Lcom/google/android/apps/gmm/startpage/af;

    if-ne v0, v1, :cond_1

    .line 476
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/startpage/ag;->b(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V

    goto :goto_0
.end method


# virtual methods
.method a()Lcom/google/android/apps/gmm/startpage/aj;
    .locals 8
    .annotation runtime Lb/a/a;
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    sget-object v1, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    sget-object v2, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    invoke-virtual {v0, v1, v2, v5}, Lcom/google/android/apps/gmm/startpage/j;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/d/e;Ljava/lang/String;)Lcom/google/android/apps/gmm/startpage/k;

    move-result-object v6

    if-nez v6, :cond_0

    :goto_0
    return-object v5

    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/startpage/aj;

    iget-object v2, v6, Lcom/google/android/apps/gmm/startpage/k;->a:Lcom/google/o/h/a/mj;

    invoke-static {}, Lcom/google/o/h/a/sa;->newBuilder()Lcom/google/o/h/a/sc;

    move-result-object v0

    sget-object v3, Lcom/google/o/h/a/dq;->n:Lcom/google/o/h/a/dq;

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget v4, v0, Lcom/google/o/h/a/sc;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/o/h/a/sc;->a:I

    iget v3, v3, Lcom/google/o/h/a/dq;->s:I

    iput v3, v0, Lcom/google/o/h/a/sc;->b:I

    invoke-virtual {v0}, Lcom/google/o/h/a/sc;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/sa;

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/o/h/a/mj;Lcom/google/o/h/a/sa;)Ljava/util/List;

    move-result-object v2

    iget-object v0, v6, Lcom/google/android/apps/gmm/startpage/k;->a:Lcom/google/o/h/a/mj;

    invoke-virtual {v0}, Lcom/google/o/h/a/mj;->d()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/gmm/startpage/d/e;->a:Lcom/google/android/apps/gmm/startpage/d/e;

    iget-boolean v0, v6, Lcom/google/android/apps/gmm/startpage/k;->c:Z

    if-eqz v0, :cond_2

    sget-object v5, Lcom/google/r/b/a/tf;->d:Lcom/google/r/b/a/tf;

    :cond_2
    iget-wide v6, v6, Lcom/google/android/apps/gmm/startpage/k;->b:J

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/gmm/startpage/aj;-><init>(Ljava/util/List;Ljava/lang/String;Lcom/google/android/apps/gmm/startpage/d/e;Lcom/google/r/b/a/tf;J)V

    move-object v5, v1

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 576
    sget-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 577
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/mylocation/e/a;)V
    .locals 2
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->g:Lcom/google/android/apps/gmm/startpage/ak;

    iget-object v1, v0, Lcom/google/android/apps/gmm/startpage/ak;->a:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/startpage/ak;->a(Lcom/google/android/apps/gmm/shared/c/f;)V

    .line 196
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/apps/gmm/startpage/a/a;)V
    .locals 2

    .prologue
    .line 306
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    .line 307
    iget-object v0, v0, Lcom/google/android/apps/gmm/startpage/d/a;->c:Lcom/google/o/h/a/gx;

    iget-boolean v1, p0, Lcom/google/android/apps/gmm/startpage/ag;->p:Z

    .line 306
    invoke-interface {p1, v0, v1}, Lcom/google/android/apps/gmm/startpage/a/a;->a(Lcom/google/o/h/a/gx;Z)V

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    monitor-exit p0

    return-void

    .line 306
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/apps/gmm/startpage/aa;)V
    .locals 1

    .prologue
    .line 547
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/startpage/ag;->c(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 548
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V
    .locals 18
    .param p2    # Lcom/google/android/apps/gmm/shared/net/k;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 482
    invoke-direct/range {p0 .. p2}, Lcom/google/android/apps/gmm/startpage/ag;->c(Lcom/google/android/apps/gmm/startpage/aa;Lcom/google/android/apps/gmm/shared/net/k;)V

    .line 483
    if-eqz p2, :cond_1

    .line 484
    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x24

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "A StartPage request has failed with "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/startpage/aa;->l()Z

    move-result v2

    if-nez v2, :cond_0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v3, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    invoke-virtual {v2}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/o/h/a/dq;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v16

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v11, v0, Lcom/google/android/apps/gmm/startpage/aa;->d:Z

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/apps/gmm/startpage/aa;->f:Lcom/google/o/h/a/eq;

    new-instance v3, Lcom/google/android/apps/gmm/startpage/b/c;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    move-object/from16 v14, p2

    invoke-direct/range {v3 .. v15}, Lcom/google/android/apps/gmm/startpage/b/c;-><init>(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;ZLcom/google/o/h/a/eq;ZLcom/google/android/apps/gmm/shared/net/k;Z)V

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 488
    :cond_1
    move-object/from16 v0, p1

    iget-object v15, v0, Lcom/google/android/apps/gmm/startpage/aa;->F:Lcom/google/r/b/a/xt;

    .line 489
    if-nez v15, :cond_3

    .line 490
    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 543
    :cond_2
    :goto_2
    return-void

    .line 494
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/base/a;->D_()Lcom/google/android/apps/gmm/map/i/a/a;

    move-result-object v2

    .line 495
    invoke-virtual {v15}, Lcom/google/r/b/a/xt;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/apps/gmm/map/i/a/a;->a(Ljava/util/Collection;)V

    .line 497
    iget-object v2, v15, Lcom/google/r/b/a/xt;->b:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/mj;->h()Lcom/google/o/h/a/mj;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v2

    check-cast v2, Lcom/google/o/h/a/mj;

    .line 500
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/af;->d:Lcom/google/android/apps/gmm/startpage/af;

    if-ne v3, v4, :cond_6

    .line 501
    iget v3, v2, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v3}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v3

    if-nez v3, :cond_4

    sget-object v3, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_4
    sget-object v4, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    if-eq v3, v4, :cond_6

    .line 502
    iget v3, v2, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v3}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v3

    if-nez v3, :cond_5

    sget-object v3, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_5
    sget-object v4, Lcom/google/o/h/a/mo;->d:Lcom/google/o/h/a/mo;

    if-eq v3, v4, :cond_6

    .line 503
    sget-object v3, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 504
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->i:Z

    .line 506
    :cond_6
    iget v3, v2, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v3}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v3

    if-nez v3, :cond_7

    sget-object v3, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_7
    sget-object v4, Lcom/google/o/h/a/mo;->e:Lcom/google/o/h/a/mo;

    if-ne v3, v4, :cond_8

    .line 507
    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    goto :goto_2

    .line 511
    :cond_8
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->h_()Lcom/google/android/apps/gmm/login/a/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/login/a/a;->g()Landroid/accounts/Account;

    move-result-object v3

    .line 512
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/aa;->b:Landroid/accounts/Account;

    if-eq v4, v3, :cond_9

    if-eqz v4, :cond_a

    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    :cond_9
    const/4 v3, 0x1

    :goto_3
    if-nez v3, :cond_b

    .line 513
    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    goto :goto_2

    .line 512
    :cond_a
    const/4 v3, 0x0

    goto :goto_3

    .line 516
    :cond_b
    iget v3, v2, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v3}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v3

    if-nez v3, :cond_c

    sget-object v3, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_c
    sget-object v4, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    if-eq v3, v4, :cond_11

    .line 517
    iget v3, v2, Lcom/google/o/h/a/mj;->b:I

    invoke-static {v3}, Lcom/google/o/h/a/mo;->a(I)Lcom/google/o/h/a/mo;

    move-result-object v3

    if-nez v3, :cond_d

    sget-object v3, Lcom/google/o/h/a/mo;->a:Lcom/google/o/h/a/mo;

    :cond_d
    sget-object v4, Lcom/google/o/h/a/mo;->d:Lcom/google/o/h/a/mo;

    if-eq v3, v4, :cond_11

    .line 518
    sget-object v3, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    const-string v5, "StartPageRequest failed: "

    iget-object v3, v2, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    instance-of v4, v3, Ljava/lang/String;

    if-eqz v4, :cond_e

    move-object v2, v3

    check-cast v2, Ljava/lang/String;

    :goto_4
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_10

    invoke-virtual {v5, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_2

    :cond_e
    check-cast v3, Lcom/google/n/f;

    invoke-virtual {v3}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/n/f;->e()Z

    move-result v3

    if-eqz v3, :cond_f

    iput-object v4, v2, Lcom/google/o/h/a/mj;->c:Ljava/lang/Object;

    :cond_f
    move-object v2, v4

    goto :goto_4

    :cond_10
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 521
    :cond_11
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long v6, v4, v6

    .line 522
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0, v6, v7}, Lcom/google/android/apps/gmm/startpage/j;->a(Lcom/google/android/apps/gmm/startpage/aa;J)V

    .line 523
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/gmm/startpage/ag;->e:Lcom/google/android/apps/gmm/startpage/y;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/apps/gmm/startpage/aa;->b:Landroid/accounts/Account;

    iget-object v3, v2, Lcom/google/o/h/a/mj;->f:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/da;->d()Lcom/google/o/h/a/da;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v3

    check-cast v3, Lcom/google/o/h/a/da;

    if-eqz v3, :cond_12

    iget v4, v3, Lcom/google/o/h/a/da;->a:I

    and-int/lit8 v4, v4, 0x1

    const/4 v5, 0x1

    if-ne v4, v5, :cond_13

    const/4 v4, 0x1

    :goto_5
    if-eqz v4, :cond_12

    iget-object v4, v3, Lcom/google/o/h/a/da;->b:Ljava/lang/Object;

    instance-of v5, v4, Ljava/lang/String;

    if-eqz v5, :cond_14

    move-object v3, v4

    check-cast v3, Ljava/lang/String;

    :goto_6
    sget-object v4, Lcom/google/android/apps/gmm/startpage/y;->a:Ljava/lang/String;

    const-string v4, "configToken is saved:"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v10

    if-eqz v10, :cond_16

    invoke-virtual {v4, v5}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :goto_7
    iget-object v4, v8, Lcom/google/android/apps/gmm/startpage/y;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v4}, Lcom/google/android/apps/gmm/base/a;->c()Lcom/google/android/apps/gmm/shared/b/a;

    move-result-object v4

    sget-object v5, Lcom/google/android/apps/gmm/shared/b/c;->av:Lcom/google/android/apps/gmm/shared/b/c;

    invoke-virtual {v4, v5, v9, v3}, Lcom/google/android/apps/gmm/shared/b/a;->a(Lcom/google/android/apps/gmm/shared/b/c;Landroid/accounts/Account;Ljava/lang/String;)V

    .line 524
    :cond_12
    sget-object v3, Lcom/google/android/apps/gmm/startpage/ao;->b:Lcom/google/android/apps/gmm/startpage/ao;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/android/apps/gmm/startpage/ao;)V

    .line 526
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/gmm/z/a/a;

    invoke-virtual {v2}, Lcom/google/o/h/a/mj;->d()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/apps/gmm/z/a/a;-><init>(Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    .line 528
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/apps/gmm/startpage/aa;->l()Z

    move-result v3

    if-nez v3, :cond_17

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/aa;->g:Lcom/google/android/apps/gmm/startpage/af;

    sget-object v4, Lcom/google/android/apps/gmm/startpage/af;->a:Lcom/google/android/apps/gmm/startpage/af;

    if-ne v3, v4, :cond_17

    const/4 v3, 0x1

    :goto_8
    if-eqz v3, :cond_2

    .line 529
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    invoke-virtual {v3}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v16

    :goto_9
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/o/h/a/dq;

    .line 532
    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/aa;)Lcom/google/o/h/a/sa;

    move-result-object v3

    .line 530
    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/o/h/a/mj;Lcom/google/o/h/a/sa;)Ljava/util/List;

    move-result-object v4

    .line 533
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v17

    new-instance v3, Lcom/google/android/apps/gmm/startpage/b/c;

    .line 534
    invoke-virtual {v2}, Lcom/google/o/h/a/mj;->d()Ljava/lang/String;

    move-result-object v5

    .line 535
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 537
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    .line 538
    move-object/from16 v0, p1

    iget-boolean v11, v0, Lcom/google/android/apps/gmm/startpage/aa;->d:Z

    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/apps/gmm/startpage/aa;->f:Lcom/google/o/h/a/eq;

    const/4 v13, 0x0

    .line 540
    iget-boolean v14, v15, Lcom/google/r/b/a/xt;->c:Z

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/gmm/startpage/b/c;-><init>(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;ZLcom/google/o/h/a/eq;ZZ)V

    .line 533
    move-object/from16 v0, v17

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_9

    .line 523
    :cond_13
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_14
    check-cast v4, Lcom/google/n/f;

    invoke-virtual {v4}, Lcom/google/n/f;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/n/f;->e()Z

    move-result v4

    if-eqz v4, :cond_15

    iput-object v5, v3, Lcom/google/o/h/a/da;->b:Ljava/lang/Object;

    :cond_15
    move-object v3, v5

    goto/16 :goto_6

    :cond_16
    new-instance v5, Ljava/lang/String;

    invoke-direct {v5, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_7

    .line 528
    :cond_17
    const/4 v3, 0x0

    goto :goto_8
.end method

.method a(Lcom/google/android/apps/gmm/startpage/ao;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 620
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 621
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->c:Lcom/google/android/apps/gmm/map/o;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/o;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 630
    :cond_0
    :goto_0
    return-void

    .line 627
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/startpage/ag;->a()Lcom/google/android/apps/gmm/startpage/aj;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/apps/gmm/startpage/ag;->g:Lcom/google/android/apps/gmm/startpage/ak;

    iget-object v6, p0, Lcom/google/android/apps/gmm/startpage/ag;->h:Lcom/google/android/apps/gmm/startpage/a;

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    .line 629
    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v7

    .line 626
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v6, v0, v1}, Lcom/google/android/apps/gmm/startpage/a;->b(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ao;->a:Lcom/google/android/apps/gmm/startpage/ao;

    if-eq p1, v0, :cond_0

    :cond_2
    if-eqz v4, :cond_0

    iget-object v0, v4, Lcom/google/android/apps/gmm/startpage/aj;->a:Ljava/util/List;

    iget-wide v8, v4, Lcom/google/android/apps/gmm/startpage/aj;->d:J

    invoke-virtual {v6, v0, v8, v9}, Lcom/google/android/apps/gmm/startpage/a;->b(Ljava/util/List;J)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/o/h/a/lh;

    iget v0, v1, Lcom/google/o/h/a/lh;->a:I

    and-int/lit8 v0, v0, 0x20

    const/16 v9, 0x20

    if-ne v0, v9, :cond_6

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, v1, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gx;->h()Lcom/google/o/h/a/gx;

    move-result-object v9

    invoke-virtual {v0, v9}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gx;

    iget-boolean v0, v0, Lcom/google/o/h/a/gx;->b:Z

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/apps/gmm/startpage/a;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/o/h/a/lh;->i:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/gx;->h()Lcom/google/o/h/a/gx;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/gx;

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/o/h/a/gx;)Lcom/google/o/h/a/gx;

    move-result-object v0

    invoke-static {v6, v4, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/android/apps/gmm/startpage/a;Lcom/google/android/apps/gmm/startpage/aj;Lcom/google/o/h/a/gx;)Lcom/google/android/apps/gmm/startpage/d/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/android/apps/gmm/startpage/d/a;)V

    sget-object v0, Lcom/google/android/apps/gmm/startpage/ao;->a:Lcom/google/android/apps/gmm/startpage/ao;

    if-ne p1, v0, :cond_4

    invoke-interface {v7}, Lcom/google/android/apps/gmm/shared/c/f;->b()J

    move-result-wide v0

    iput-wide v0, v5, Lcom/google/android/apps/gmm/startpage/ak;->f:J

    :cond_4
    iget-object v0, v4, Lcom/google/android/apps/gmm/startpage/aj;->e:Ljava/util/List;

    iget-wide v8, v4, Lcom/google/android/apps/gmm/startpage/aj;->d:J

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/lh;

    invoke-virtual {v0}, Lcom/google/o/h/a/lh;->j()Ljava/util/List;

    move-result-object v0

    invoke-virtual {v6, v0, v8, v9}, Lcom/google/android/apps/gmm/startpage/a;->a(Ljava/util/List;J)Lcom/google/o/h/a/be;

    move-result-object v0

    sget-object v4, Lcom/google/o/h/a/be;->a:Lcom/google/o/h/a/be;

    if-eq v0, v4, :cond_5

    move v0, v2

    :goto_3
    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->n:Lcom/google/r/b/a/pk;

    invoke-virtual {v5, v7, v0}, Lcom/google/android/apps/gmm/startpage/ak;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/pk;)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    move v0, v3

    goto :goto_3
.end method

.method declared-synchronized a(Lcom/google/android/apps/gmm/startpage/d/a;)V
    .locals 1

    .prologue
    .line 217
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/gmm/startpage/d/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 218
    iput-object p1, p0, Lcom/google/android/apps/gmm/startpage/ag;->k:Lcom/google/android/apps/gmm/startpage/d/a;

    .line 219
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/ag;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 221
    :cond_0
    monitor-exit p0

    return-void

    .line 217
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/a;)V
    .locals 1

    .prologue
    .line 586
    sget-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 587
    return-void
.end method

.method declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 224
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->p:Z

    if-eq p1, v0, :cond_0

    .line 225
    iput-boolean p1, p0, Lcom/google/android/apps/gmm/startpage/ag;->p:Z

    .line 226
    invoke-direct {p0}, Lcom/google/android/apps/gmm/startpage/ag;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    :cond_0
    monitor-exit p0

    return-void

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method a(Landroid/accounts/Account;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 590
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ag;->f:Lcom/google/android/gms/location/reporting/c;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ag;->f:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/c;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 591
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 609
    :goto_0
    return v0

    .line 594
    :cond_1
    if-nez p1, :cond_2

    .line 595
    sget-object v1, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    goto :goto_0

    .line 599
    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/startpage/ag;->f:Lcom/google/android/gms/location/reporting/c;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/location/reporting/c;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v1

    .line 600
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->b()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/location/reporting/d;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 601
    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/ReportingState;->a()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/location/reporting/d;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 602
    sget-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 603
    const/4 v0, 0x1

    goto :goto_0

    .line 605
    :cond_3
    sget-object v1, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 607
    :catch_0
    move-exception v1

    .line 608
    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2a

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "IOException happens in accessing GMS core:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public final am_()V
    .locals 1

    .prologue
    .line 581
    sget-object v0, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 582
    return-void
.end method

.method public final declared-synchronized b(Lcom/google/android/apps/gmm/startpage/a/a;)V
    .locals 1

    .prologue
    .line 313
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/startpage/ag;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    monitor-exit p0

    return-void

    .line 313
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b(Lcom/google/android/apps/gmm/startpage/aa;)V
    .locals 17

    .prologue
    .line 374
    monitor-enter p1

    .line 375
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/gmm/startpage/ag;->d:Lcom/google/android/apps/gmm/startpage/j;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/startpage/j;->a(Lcom/google/android/apps/gmm/startpage/aa;)Lcom/google/android/apps/gmm/startpage/k;

    move-result-object v2

    .line 376
    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    if-nez v2, :cond_1

    .line 378
    sget-object v2, Lcom/google/android/apps/gmm/startpage/ag;->a:Ljava/lang/String;

    .line 402
    :cond_0
    return-void

    .line 376
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 384
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->h:Lcom/google/android/apps/gmm/startpage/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/gmm/startpage/ag;->c:Lcom/google/android/apps/gmm/map/t;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/apps/gmm/startpage/a;->b(Lcom/google/android/apps/gmm/base/a;Lcom/google/android/apps/gmm/map/t;)Z

    .line 386
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/aa;->c:Lcom/google/b/c/cv;

    invoke-virtual {v3}, Lcom/google/b/c/cv;->b()Lcom/google/b/c/lg;

    move-result-object v15

    :goto_0
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/o/h/a/dq;

    .line 388
    iget-object v3, v2, Lcom/google/android/apps/gmm/startpage/k;->a:Lcom/google/o/h/a/mj;

    .line 389
    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/google/android/apps/gmm/startpage/ag;->a(Lcom/google/o/h/a/dq;Lcom/google/android/apps/gmm/startpage/aa;)Lcom/google/o/h/a/sa;

    move-result-object v4

    .line 387
    invoke-static {v3, v4}, Lcom/google/android/apps/gmm/startpage/u;->a(Lcom/google/o/h/a/mj;Lcom/google/o/h/a/sa;)Ljava/util/List;

    move-result-object v3

    .line 390
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/gmm/startpage/ag;->h:Lcom/google/android/apps/gmm/startpage/a;

    .line 391
    iget-wide v6, v2, Lcom/google/android/apps/gmm/startpage/k;->b:J

    .line 390
    invoke-virtual {v4, v3, v6, v7}, Lcom/google/android/apps/gmm/startpage/a;->b(Ljava/util/List;J)Ljava/util/List;

    move-result-object v4

    .line 392
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/gmm/startpage/ag;->b:Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v3}, Lcom/google/android/apps/gmm/base/a;->d()Lcom/google/android/apps/gmm/map/util/b/g;

    move-result-object v16

    new-instance v3, Lcom/google/android/apps/gmm/startpage/b/c;

    .line 393
    iget-object v5, v2, Lcom/google/android/apps/gmm/startpage/k;->a:Lcom/google/o/h/a/mj;

    invoke-virtual {v5}, Lcom/google/o/h/a/mj;->d()Ljava/lang/String;

    move-result-object v5

    .line 394
    iget-wide v6, v2, Lcom/google/android/apps/gmm/startpage/k;->b:J

    .line 395
    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/apps/gmm/startpage/aa;->A:Lcom/google/android/apps/gmm/startpage/d/e;

    .line 396
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/apps/gmm/startpage/aa;->h:Ljava/lang/String;

    .line 397
    move-object/from16 v0, p1

    iget-boolean v11, v0, Lcom/google/android/apps/gmm/startpage/aa;->d:Z

    .line 398
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/apps/gmm/startpage/aa;->f:Lcom/google/o/h/a/eq;

    const/4 v13, 0x1

    .line 400
    iget-boolean v14, v2, Lcom/google/android/apps/gmm/startpage/k;->c:Z

    invoke-direct/range {v3 .. v14}, Lcom/google/android/apps/gmm/startpage/b/c;-><init>(Ljava/util/List;Ljava/lang/String;JLcom/google/android/apps/gmm/startpage/d/e;Lcom/google/o/h/a/dq;Ljava/lang/String;ZLcom/google/o/h/a/eq;ZZ)V

    .line 392
    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Lcom/google/android/apps/gmm/map/util/b/g;->c(Ljava/lang/Object;)V

    goto :goto_0
.end method
