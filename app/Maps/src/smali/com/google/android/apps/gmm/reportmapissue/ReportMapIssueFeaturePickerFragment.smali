.class public Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;
.super Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/place/b/c;
.implements Lcom/google/android/apps/gmm/reportmapissue/a/h;


# instance fields
.field a:Lcom/google/android/apps/gmm/reportmapissue/n;

.field b:Lcom/google/android/apps/gmm/shared/net/i;

.field c:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;-><init>()V

    .line 47
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/k;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/reportmapissue/k;-><init>(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->c:Ljava/lang/Object;

    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/x/a;Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;)Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;
    .locals 3
    .param p1    # Lcom/google/android/apps/gmm/base/g/c;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 126
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 127
    if-eqz p1, :cond_0

    .line 128
    const-string v1, "placemark"

    invoke-virtual {p0, v0, v1, p1}, Lcom/google/android/apps/gmm/x/a;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/io/Serializable;)V

    .line 130
    :cond_0
    const-string v1, "rdp_entry point_type"

    iget v2, p2, Lcom/google/maps/g/hs;->n:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 131
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;-><init>()V

    .line 132
    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 133
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/q;->o()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/views/q;->o()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->a(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;Z)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v0, p1}, Lcom/google/android/apps/gmm/base/views/q;->a(Z)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 153
    sget v0, Lcom/google/android/apps/gmm/l;->gH:I

    return v0
.end method

.method protected final a(Z)I
    .locals 1

    .prologue
    .line 263
    const/4 v0, 0x6

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;)V
    .locals 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    :goto_0
    return-void

    .line 251
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/shared/net/i;

    .line 252
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/l;

    invoke-direct {v1, p0}, Lcom/google/android/apps/gmm/reportmapissue/l;-><init>(Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/shared/net/i;Lcom/google/android/apps/gmm/base/g/c;)V
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/shared/net/i;

    if-ne p1, v0, :cond_0

    .line 237
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/shared/net/i;

    .line 240
    invoke-virtual {p2}, Lcom/google/android/apps/gmm/base/g/c;->f()Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 241
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->i()V

    goto :goto_0
.end method

.method public final a(Lcom/google/e/a/a/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    sget-object v0, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a/p;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/shared/c/a/p;->a(Z)V

    .line 188
    iput-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    .line 189
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-void

    .line 192
    :cond_1
    const/4 v0, 0x1

    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 195
    if-eqz v0, :cond_3

    .line 196
    new-instance v1, Lcom/google/android/apps/gmm/base/g/g;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/base/g/g;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/base/g/g;->a(Lcom/google/e/a/a/a/b;)Lcom/google/android/apps/gmm/base/g/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/g/g;->a()Lcom/google/android/apps/gmm/base/g/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    .line 203
    invoke-super {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->i()V

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->w:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->w:Z

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/reportmapissue/ReportAProblemNotAvailableFragment;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityDialogFragment;->a(Lcom/google/android/apps/gmm/base/activities/c;Lcom/google/android/apps/gmm/base/fragments/a/b;)V

    goto :goto_0

    .line 199
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    sget v1, Lcom/google/android/apps/gmm/l;->hQ:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected final a_()V
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/activities/c;->a:Lcom/google/android/apps/gmm/base/j/b;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/base/j/b;->E()Lcom/google/android/apps/gmm/reportmapissue/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->e:Lcom/google/maps/g/hs;

    sget-object v3, Lcom/google/maps/g/hq;->c:Lcom/google/maps/g/hq;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/apps/gmm/reportmapissue/a/f;->a(Lcom/google/android/apps/gmm/base/g/c;Lcom/google/maps/g/hs;Lcom/google/maps/g/hq;)V

    .line 178
    return-void
.end method

.method public final af_()V
    .locals 3

    .prologue
    .line 221
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    .line 222
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/l;->hQ:I

    invoke-virtual {p0, v1}, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 223
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 224
    return-void
.end method

.method protected final b()I
    .locals 1

    .prologue
    .line 158
    sget v0, Lcom/google/android/apps/gmm/l;->gE:I

    return v0
.end method

.method protected final c()I
    .locals 1

    .prologue
    .line 163
    sget v0, Lcom/google/android/apps/gmm/d;->aH:I

    return v0
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 168
    sget v0, Lcom/google/android/apps/gmm/l;->iV:I

    return v0
.end method

.method protected final f()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->c:Ljava/lang/Object;

    return-object v0
.end method

.method protected final i()V
    .locals 2

    .prologue
    .line 213
    invoke-super {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->i()V

    .line 214
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->d:Lcom/google/android/apps/gmm/base/g/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/base/g/c;->a:Lcom/google/r/b/a/ads;

    iget-boolean v0, v0, Lcom/google/r/b/a/ads;->w:Z

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/fragments/FeaturePickerFragment;->g:Lcom/google/android/apps/gmm/base/views/q;

    invoke-interface {v1, v0}, Lcom/google/android/apps/gmm/base/views/q;->b(Z)V

    .line 217
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 138
    invoke-super {p0}, Lcom/google/android/apps/gmm/reportmapissue/RapEnabledFeaturePickerFragment;->onDestroy()V

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/reportmapissue/n;->f()V

    .line 142
    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->a:Lcom/google/android/apps/gmm/reportmapissue/n;

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/shared/net/i;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/shared/net/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/net/i;->f()V

    .line 147
    iput-object v1, p0, Lcom/google/android/apps/gmm/reportmapissue/ReportMapIssueFeaturePickerFragment;->b:Lcom/google/android/apps/gmm/shared/net/i;

    .line 149
    :cond_1
    return-void
.end method
