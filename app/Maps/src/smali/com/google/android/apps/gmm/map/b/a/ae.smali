.class public final Lcom/google/android/apps/gmm/map/b/a/ae;
.super Lcom/google/android/apps/gmm/map/b/a/af;
.source "PG"


# instance fields
.field public a:Lcom/google/android/apps/gmm/map/b/a/y;

.field public b:Lcom/google/android/apps/gmm/map/b/a/y;

.field volatile c:Lcom/google/android/apps/gmm/map/b/a/y;

.field volatile d:Lcom/google/android/apps/gmm/map/b/a/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/gmm/map/b/a/af;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 22
    iput-object p2, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 23
    return-void
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ab;)Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 30
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x3

    if-gtz v0, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 58
    :goto_0
    return-object v0

    .line 34
    :cond_0
    invoke-virtual {p0, v7}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(I)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v5

    .line 35
    iget v2, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 37
    iget v1, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 40
    const/4 v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_1
    iget-object v6, p0, Lcom/google/android/apps/gmm/map/b/a/ab;->a:[I

    array-length v6, v6

    div-int/lit8 v6, v6, 0x3

    if-ge v0, v6, :cond_5

    .line 41
    invoke-virtual {p0, v0, v5}, Lcom/google/android/apps/gmm/map/b/a/ab;->a(ILcom/google/android/apps/gmm/map/b/a/y;)V

    .line 42
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v6, v4, :cond_1

    .line 43
    iget v4, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 45
    :cond_1
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v6, v3, :cond_2

    .line 46
    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 48
    :cond_2
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ge v6, v2, :cond_3

    .line 49
    iget v2, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 51
    :cond_3
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-le v6, v1, :cond_4

    .line 52
    iget v1, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 40
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 56
    :cond_5
    iput v4, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v2, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iput v7, v5, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 57
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 58
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v0, v5, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;I)Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 4

    .prologue
    .line 130
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v1, p1

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v2, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 131
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v2, p1

    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v3, p1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 132
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v2, v0, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v2
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 6

    .prologue
    .line 106
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v0, v1, :cond_0

    .line 107
    iget v1, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 108
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 113
    :goto_0
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ge v2, v3, :cond_1

    .line 114
    iget v3, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 115
    iget v2, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 120
    :goto_1
    new-instance v4, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v5, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v5, v1, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-direct {v4, v5, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v4

    .line 110
    :cond_0
    iget v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 111
    iget v0, p0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    goto :goto_0

    .line 117
    :cond_1
    iget v3, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 118
    iget v2, p0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    goto :goto_1
.end method

.method public static b([Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 3

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v1}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-direct {v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>()V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 95
    invoke-virtual {v0, p0}, Lcom/google/android/apps/gmm/map/b/a/ae;->a([Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 96
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/map/b/a/ae;)Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 5

    .prologue
    .line 256
    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 257
    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 258
    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v3, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v4, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 259
    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    .line 260
    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v0, v3, :cond_0

    iget v0, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget v3, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-le v0, v3, :cond_1

    .line 261
    :cond_0
    const/4 v0, 0x0

    .line 263
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    goto :goto_0
.end method

.method public final a()Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 3

    .prologue
    .line 201
    packed-switch p1, :pswitch_data_0

    .line 214
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 203
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_0

    .line 204
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 213
    :goto_0
    return-object v0

    .line 207
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 209
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-nez v0, :cond_1

    .line 210
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    .line 212
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 213
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    goto :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(IIII)V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 308
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 309
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 310
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 311
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p3, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->c:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 317
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->d:Lcom/google/android/apps/gmm/map/b/a/y;

    iput p4, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 319
    :cond_1
    return-void
.end method

.method public final a([Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 7

    .prologue
    .line 65
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 66
    iget v2, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 68
    iget v1, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 71
    const/4 v0, 0x1

    move v3, v2

    move v4, v2

    move v2, v1

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_4

    .line 72
    aget-object v5, p1, v0

    .line 73
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-ge v6, v4, :cond_0

    .line 74
    iget v4, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 76
    :cond_0
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-le v6, v3, :cond_1

    .line 77
    iget v3, v5, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    .line 79
    :cond_1
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-ge v6, v2, :cond_2

    .line 80
    iget v2, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 82
    :cond_2
    iget v6, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-le v6, v1, :cond_3

    .line 83
    iget v1, v5, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    .line 71
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_4
    invoke-virtual {p0, v4, v2, v3, v1}, Lcom/google/android/apps/gmm/map/b/a/ae;->a(IIII)V

    .line 88
    return-void
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 2

    .prologue
    .line 182
    instance-of v0, p1, Lcom/google/android/apps/gmm/map/b/a/ae;

    if-eqz v0, :cond_1

    .line 183
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v1, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    .line 190
    :goto_0
    return v0

    .line 184
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 190
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/apps/gmm/map/b/a/af;->a(Lcom/google/android/apps/gmm/map/b/a/af;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/b/a/y;)Z
    .locals 2

    .prologue
    .line 162
    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v0, v1, :cond_0

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x4

    return v0
.end method

.method public final b(I)Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 5

    .prologue
    .line 270
    if-gez p1, :cond_0

    .line 271
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "distance cannot be negative: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/map/b/a/ae;

    new-instance v1, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    sub-int/2addr v2, p1

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    sub-int/2addr v3, p1

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    new-instance v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v3, v3, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v3, p1

    iget-object v4, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v4, v4, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v4, p1

    invoke-direct {v2, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/y;-><init>(II)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/apps/gmm/map/b/a/ae;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    return-object v0
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iput v1, p1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    const/4 v0, 0x0

    iput v0, p1, Lcom/google/android/apps/gmm/map/b/a/y;->c:I

    .line 149
    return-object p1
.end method

.method public final b(Lcom/google/android/apps/gmm/map/b/a/af;)Z
    .locals 3

    .prologue
    .line 173
    invoke-virtual {p1}, Lcom/google/android/apps/gmm/map/b/a/af;->c()Lcom/google/android/apps/gmm/map/b/a/ae;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-gt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v2, v2, Lcom/google/android/apps/gmm/map/b/a/y;->a:I

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v1, v1, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget v0, v0, Lcom/google/android/apps/gmm/map/b/a/y;->b:I

    if-lt v1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/apps/gmm/map/b/a/ae;
    .locals 0

    .prologue
    .line 168
    return-object p0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 289
    if-ne p0, p1, :cond_1

    .line 295
    :cond_0
    :goto_0
    return v0

    .line 291
    :cond_1
    instance-of v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;

    if-eqz v2, :cond_3

    .line 292
    check-cast p1, Lcom/google/android/apps/gmm/map/b/a/ae;

    .line 293
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 295
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/b/a/y;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 283
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/y;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->a:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/b/a/ae;->b:Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x4

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
