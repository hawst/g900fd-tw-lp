.class public Lcom/google/android/apps/gmm/prefetch/r;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lcom/google/android/apps/gmm/prefetch/r;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:I


# instance fields
.field final b:Lcom/google/android/apps/gmm/map/internal/c/bp;

.field c:Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Lcom/google/android/apps/gmm/shared/c/f;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 34
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1f

    .line 35
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/apps/gmm/prefetch/r;->a:I

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/shared/c/f;)V
    .locals 6

    .prologue
    .line 68
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v2

    const/4 v3, 0x0

    sget v4, Lcom/google/android/apps/gmm/prefetch/r;->a:I

    move-object v0, p0

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/prefetch/r;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/Map;Ljava/lang/String;ILcom/google/android/apps/gmm/shared/c/f;)V

    .line 70
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/Map;Ljava/lang/String;ILcom/google/android/apps/gmm/shared/c/f;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/internal/c/bp;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;>;",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/apps/gmm/shared/c/f;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lcom/google/android/apps/gmm/prefetch/r;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 75
    iput-object p2, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    .line 76
    iput-object p3, p0, Lcom/google/android/apps/gmm/prefetch/r;->c:Ljava/lang/String;

    .line 77
    iput-object p5, p0, Lcom/google/android/apps/gmm/prefetch/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    .line 78
    iput p4, p0, Lcom/google/android/apps/gmm/prefetch/r;->d:I

    .line 79
    return-void
.end method

.method public static a(Lcom/google/e/a/a/a/b;Lcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/prefetch/r;
    .locals 12

    .prologue
    .line 274
    const/4 v0, 0x1

    const/16 v1, 0x1a

    invoke-virtual {p0, v0, v1}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 275
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/bp;

    const/4 v2, 0x3

    const/16 v3, 0x15

    invoke-virtual {v0, v2, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-int v3, v2

    const/4 v2, 0x1

    const/16 v4, 0x15

    invoke-virtual {v0, v2, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v2, v4

    const/4 v4, 0x2

    const/16 v5, 0x15

    invoke-virtual {v0, v4, v5}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v0, v4

    invoke-direct {v1, v3, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/bp;-><init>(III)V

    .line 278
    invoke-static {}, Lcom/google/b/c/hj;->a()Ljava/util/HashMap;

    move-result-object v2

    .line 279
    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v3, v0}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v6

    .line 280
    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-ge v5, v6, :cond_1

    .line 281
    const/4 v0, 0x2

    .line 282
    const/16 v3, 0x1a

    invoke-virtual {p0, v0, v5, v3}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/e/a/a/a/b;

    .line 283
    const/4 v3, 0x1

    const/16 v4, 0x15

    invoke-virtual {v0, v3, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    long-to-int v7, v8

    .line 284
    const/4 v3, 0x2

    .line 285
    iget-object v4, v0, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3}, Lcom/google/e/a/b/b;->a(I)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/e/a/a/a/b;->a(Ljava/lang/Object;)I

    move-result v8

    .line 286
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 287
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v8, :cond_0

    .line 288
    const/4 v3, 0x2

    const/16 v10, 0x13

    invoke-virtual {v0, v3, v4, v10}, Lcom/google/e/a/a/a/b;->a(III)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v9, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 287
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 290
    :cond_0
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 292
    :cond_1
    const/4 v0, 0x3

    const/16 v3, 0x1c

    invoke-virtual {p0, v0, v3}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 293
    const/4 v0, 0x4

    .line 294
    const/16 v4, 0x15

    invoke-virtual {p0, v0, v4}, Lcom/google/e/a/a/a/b;->b(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v4, v4

    .line 295
    new-instance v0, Lcom/google/android/apps/gmm/prefetch/r;

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/gmm/prefetch/r;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Ljava/util/Map;Ljava/lang/String;ILcom/google/android/apps/gmm/shared/c/f;)V

    return-object v0
.end method

.method private declared-synchronized c(I)Ljava/util/LinkedList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/LinkedList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 95
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/apps/gmm/prefetch/r;->c(I)Ljava/util/LinkedList;

    move-result-object v0

    .line 96
    if-nez v0, :cond_1

    .line 97
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 98
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 103
    monitor-exit p0

    return-void

    .line 99
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_0

    .line 100
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()V
    .locals 8

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 128
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 129
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    .line 132
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 133
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 134
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 135
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 136
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v6, p0, Lcom/google/android/apps/gmm/prefetch/r;->d:I

    int-to-long v6, v6

    invoke-virtual {v1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_1

    .line 137
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 136
    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 142
    :cond_3
    :try_start_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 146
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized b(I)V
    .locals 1

    .prologue
    .line 114
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/apps/gmm/prefetch/r;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    monitor-exit p0

    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final c()I
    .locals 12

    .prologue
    .line 195
    const/4 v0, 0x0

    .line 197
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 198
    iget-object v1, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 199
    invoke-direct {p0, v5}, Lcom/google/android/apps/gmm/prefetch/r;->c(I)Ljava/util/LinkedList;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 201
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v8, v2, v8

    invoke-virtual {v0, v8, v9}, Ljava/util/concurrent/TimeUnit;->toDays(J)J

    move-result-wide v8

    long-to-int v0, v8

    .line 204
    rsub-int/lit8 v0, v0, 0x1f

    int-to-double v8, v0

    const-wide/high16 v10, 0x403f000000000000L    # 31.0

    div-double/2addr v8, v10

    const-wide/high16 v10, 0x4024000000000000L    # 10.0

    mul-double/2addr v8, v10

    double-to-int v0, v8

    .line 205
    const/4 v7, 0x4

    if-ne v5, v7, :cond_2

    .line 218
    const/16 v7, 0x9

    if-lt v0, v7, :cond_1

    mul-int/lit16 v0, v0, 0x3e8

    :goto_1
    add-int/2addr v1, v0

    goto :goto_0

    :cond_1
    mul-int/2addr v0, v0

    goto :goto_1

    .line 220
    :cond_2
    mul-int/2addr v0, v0

    add-int/2addr v1, v0

    .line 222
    goto :goto_0

    .line 225
    :cond_3
    return v1
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/apps/gmm/prefetch/r;

    invoke-virtual {p0}, Lcom/google/android/apps/gmm/prefetch/r;->c()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/apps/gmm/prefetch/r;->c()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    if-le v0, v1, :cond_1

    const/4 v0, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized d()Lcom/google/e/a/a/a/b;
    .locals 8

    .prologue
    .line 242
    monitor-enter p0

    :try_start_0
    new-instance v1, Lcom/google/e/a/a/a/b;

    sget-object v0, Lcom/google/r/b/a/b/j;->J:Lcom/google/e/a/a/a/d;

    invoke-direct {v1, v0}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->b:Lcom/google/android/apps/gmm/map/internal/c/bp;

    new-instance v2, Lcom/google/e/a/a/a/b;

    sget-object v3, Lcom/google/r/b/a/b/j;->H:Lcom/google/e/a/a/a/d;

    invoke-direct {v2, v3}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    const/4 v3, 0x1

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->b:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v3, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v3, 0x2

    iget v4, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->c:I

    int-to-long v4, v4

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v5, v3, v4}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    const/4 v3, 0x3

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/c/bp;->a:I

    int-to-long v4, v0

    invoke-static {v4, v5}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v4, v2, Lcom/google/e/a/a/a/b;->e:Lcom/google/e/a/b/b;

    invoke-virtual {v4, v3, v0}, Lcom/google/e/a/b/b;->a(ILjava/lang/Object;)V

    .line 246
    const/4 v0, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 250
    invoke-direct {p0, v0}, Lcom/google/android/apps/gmm/prefetch/r;->c(I)Ljava/util/LinkedList;

    move-result-object v3

    .line 251
    new-instance v4, Lcom/google/e/a/a/a/b;

    sget-object v5, Lcom/google/r/b/a/b/j;->K:Lcom/google/e/a/a/a/d;

    invoke-direct {v4, v5}, Lcom/google/e/a/a/a/b;-><init>(Lcom/google/e/a/a/a/d;)V

    .line 253
    const/4 v5, 0x1

    int-to-long v6, v0

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 254
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 255
    :goto_1
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    const/4 v5, 0x2

    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 242
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 258
    :cond_0
    const/4 v0, 0x2

    :try_start_1
    invoke-virtual {v1, v0, v4}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    goto :goto_0

    .line 261
    :cond_1
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/prefetch/r;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V

    .line 262
    const/4 v0, 0x4

    iget v2, p0, Lcom/google/android/apps/gmm/prefetch/r;->d:I

    int-to-long v2, v2

    invoke-static {v2, v3}, Lcom/google/e/a/b/d;->a(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/e/a/a/a/b;->a(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 264
    monitor-exit p0

    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .prologue
    const/16 v9, 0xa

    .line 301
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->f:Lcom/google/android/apps/gmm/shared/c/f;

    invoke-interface {v0}, Lcom/google/android/apps/gmm/shared/c/f;->a()J

    move-result-wide v2

    .line 302
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 304
    const-string v6, "  usage: "

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v7, 0xb

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    invoke-direct {p0, v5}, Lcom/google/android/apps/gmm/prefetch/r;->c(I)Ljava/util/LinkedList;

    move-result-object v0

    .line 306
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 307
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 308
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sub-long v6, v2, v6

    invoke-virtual {v0, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v6

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v8, 0x25

    invoke-direct {v0, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "    minutes ago: "

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 304
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 310
    :cond_1
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 312
    :cond_2
    const-string v0, "description: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    const-string v0, ", expireTimeInterval (sec): "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 315
    iget v0, p0, Lcom/google/android/apps/gmm/prefetch/r;->d:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 316
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 317
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
