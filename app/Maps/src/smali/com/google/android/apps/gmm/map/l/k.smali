.class public Lcom/google/android/apps/gmm/map/l/k;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;>;"
        }
    .end annotation
.end field

.field private static c:Lcom/google/android/apps/gmm/map/util/a/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/apps/gmm/map/util/a/i",
            "<",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Lcom/google/android/apps/gmm/map/l/o;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/google/android/apps/gmm/map/l/l;

    const-string v1, "Points"

    invoke-direct {v0, v2, v1}, Lcom/google/android/apps/gmm/map/l/l;-><init>(Lcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/l/k;->a:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 39
    new-instance v0, Lcom/google/android/apps/gmm/map/l/m;

    const-string v1, "PointsUsedMap"

    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/apps/gmm/map/l/m;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/l/k;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    .line 49
    new-instance v0, Lcom/google/android/apps/gmm/map/l/n;

    const-string v1, "LineSegmentMap"

    invoke-direct {v0, v3, v2, v1}, Lcom/google/android/apps/gmm/map/l/n;-><init>(ILcom/google/android/apps/gmm/map/util/a/b;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/gmm/map/l/k;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    return-void
.end method

.method private static a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ")",
            "Lcom/google/android/apps/gmm/map/b/a/y;"
        }
    .end annotation

    .prologue
    .line 225
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 226
    if-nez v0, :cond_0

    .line 227
    invoke-interface {p0, p1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    :goto_0
    return-object p1

    .line 230
    :cond_0
    sget-object v1, Lcom/google/android/apps/gmm/map/l/k;->a:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v1, p1}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    move-object p1, v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/apps/gmm/map/b/a/ax;[B)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/map/b/a/ax;",
            "[B)",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/ab;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 243
    sget-object v0, Lcom/google/android/apps/gmm/map/l/k;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 244
    sget-object v1, Lcom/google/android/apps/gmm/map/l/k;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    move v5, v6

    .line 246
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->b:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x3

    :goto_1
    if-ge v5, v2, :cond_4

    .line 247
    aget-byte v2, p1, v5

    if-eqz v2, :cond_2

    .line 250
    sget-object v2, Lcom/google/android/apps/gmm/map/l/k;->a:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v2}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 251
    sget-object v3, Lcom/google/android/apps/gmm/map/l/k;->a:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 252
    sget-object v4, Lcom/google/android/apps/gmm/map/l/k;->a:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v4}, Lcom/google/android/apps/gmm/map/util/a/i;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 254
    invoke-virtual {p0, v5, v2, v3, v4}, Lcom/google/android/apps/gmm/map/b/a/ax;->a(ILcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 256
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/map/l/k;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v2

    .line 257
    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/map/l/k;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v3

    .line 258
    invoke-static {v1, v4}, Lcom/google/android/apps/gmm/map/l/k;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;)Lcom/google/android/apps/gmm/map/b/a/y;

    move-result-object v4

    .line 260
    aget-byte v7, p1, v5

    and-int/lit8 v7, v7, 0x1

    if-eqz v7, :cond_0

    .line 261
    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/map/l/k;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 263
    :cond_0
    aget-byte v7, p1, v5

    and-int/lit8 v7, v7, 0x2

    if-eqz v7, :cond_1

    .line 264
    invoke-static {v0, v3, v4}, Lcom/google/android/apps/gmm/map/l/k;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 266
    :cond_1
    aget-byte v3, p1, v5

    and-int/lit8 v3, v3, 0x4

    if-eqz v3, :cond_2

    .line 267
    invoke-static {v0, v4, v2}, Lcom/google/android/apps/gmm/map/l/k;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 246
    :cond_2
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/b/a/ax;->a:[I

    array-length v2, v2

    div-int/lit8 v2, v2, 0x9

    goto :goto_1

    .line 274
    :cond_4
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    .line 275
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 276
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    if-ltz v4, :cond_5

    const/4 v6, 0x1

    :cond_5
    if-nez v6, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_6
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 277
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/l/o;

    .line 278
    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 279
    new-instance v6, Lcom/google/android/apps/gmm/map/b/a/ad;

    iget-object v7, v2, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v7}, Ljava/util/Deque;->size()I

    move-result v7

    invoke-direct {v6, v7}, Lcom/google/android/apps/gmm/map/b/a/ad;-><init>(I)V

    iget-object v2, v2, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v6, v2}, Lcom/google/android/apps/gmm/map/b/a/ad;->a(Lcom/google/android/apps/gmm/map/b/a/y;)Z

    goto :goto_3

    :cond_8
    invoke-virtual {v6}, Lcom/google/android/apps/gmm/map/b/a/ad;->a()Lcom/google/android/apps/gmm/map/b/a/ab;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 284
    :cond_9
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/l/o;

    .line 285
    iget-object v2, v2, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->clear()V

    goto :goto_4

    .line 289
    :cond_a
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    .line 290
    sget-object v4, Lcom/google/android/apps/gmm/map/l/k;->a:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    goto :goto_5

    .line 292
    :cond_b
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 293
    sget-object v2, Lcom/google/android/apps/gmm/map/l/k;->b:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 294
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 295
    sget-object v1, Lcom/google/android/apps/gmm/map/l/k;->c:Lcom/google/android/apps/gmm/map/util/a/i;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/gmm/map/util/a/i;->a(Ljava/lang/Object;)Z

    .line 297
    return-object v5
.end method

.method private static a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Lcom/google/android/apps/gmm/map/l/o;",
            ">;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            "Lcom/google/android/apps/gmm/map/b/a/y;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 186
    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/l/o;

    .line 187
    invoke-interface {p0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/map/l/o;

    .line 189
    if-eqz v0, :cond_6

    .line 192
    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/apps/gmm/map/l/o;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v6

    .line 195
    :goto_0
    if-eqz v1, :cond_5

    if-eq v0, v1, :cond_5

    .line 197
    if-eqz v0, :cond_1

    if-eqz v6, :cond_1

    .line 201
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {p0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    iget-object v2, v1, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/Deque;->addFirst(Ljava/lang/Object;)V

    goto :goto_1

    :cond_0
    iget-object v2, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v4

    :goto_2
    move v5, v0

    .line 203
    :cond_1
    if-nez v5, :cond_5

    .line 206
    invoke-virtual {v1, p0, p1, p2}, Lcom/google/android/apps/gmm/map/l/o;->a(Ljava/util/Map;Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)Z

    move-result v0

    or-int/2addr v0, v6

    .line 210
    :goto_3
    if-nez v0, :cond_2

    .line 213
    new-instance v1, Lcom/google/android/apps/gmm/map/l/o;

    invoke-direct {v1, p1, p2}, Lcom/google/android/apps/gmm/map/l/o;-><init>(Lcom/google/android/apps/gmm/map/b/a/y;Lcom/google/android/apps/gmm/map/b/a/y;)V

    .line 214
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    iget-object v0, v1, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {p0, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 218
    :cond_2
    return-void

    .line 201
    :cond_3
    iget-object v2, v1, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v3}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/map/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {p0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    iget-object v3, v1, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2, v3}, Ljava/util/Deque;->addAll(Ljava/util/Collection;)Z

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/l/o;->a:Ljava/util/Deque;

    invoke-interface {v2}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/gmm/map/b/a/y;

    invoke-interface {p0, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v4

    goto :goto_2

    :cond_4
    move v0, v5

    goto :goto_2

    :cond_5
    move v0, v6

    goto :goto_3

    :cond_6
    move v6, v5

    goto/16 :goto_0
.end method
