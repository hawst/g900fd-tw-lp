.class public Lcom/google/android/apps/gmm/directions/u;
.super Lcom/google/android/apps/gmm/base/b/a;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/gmm/base/b/a",
        "<",
        "Lcom/google/android/apps/gmm/map/r/a/w;",
        ">;"
    }
.end annotation


# instance fields
.field final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/v;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/google/android/apps/gmm/map/r/a/w;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "[",
            "Lcom/google/android/apps/gmm/map/r/a/w;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/directions/v;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0, p2}, Lcom/google/android/apps/gmm/base/b/a;-><init>([Ljava/lang/Object;)V

    .line 28
    iput-object p3, p0, Lcom/google/android/apps/gmm/directions/u;->d:Ljava/util/List;

    .line 31
    return-void
.end method


# virtual methods
.method public final synthetic c(Landroid/view/ViewGroup;ILjava/lang/Object;)Landroid/view/View;
    .locals 2

    .prologue
    .line 16
    check-cast p3, Lcom/google/android/apps/gmm/map/r/a/w;

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/u;->d:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/directions/v;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p3, v1}, Lcom/google/android/apps/gmm/directions/v;->a(Landroid/view/ViewGroup;Lcom/google/android/apps/gmm/map/r/a/w;Landroid/widget/AdapterView$OnItemClickListener;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 63
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0}, Lcom/google/b/c/cv;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/a;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/u;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/b/a;->b:Lcom/google/b/c/cv;

    invoke-virtual {v0, v1}, Lcom/google/b/c/cv;->get(I)Ljava/lang/Object;

    .line 63
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 66
    :cond_1
    return-void
.end method
