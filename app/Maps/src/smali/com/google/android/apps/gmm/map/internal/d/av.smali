.class Lcom/google/android/apps/gmm/map/internal/d/av;
.super Lcom/google/android/apps/gmm/shared/c/a/d;
.source "PG"


# instance fields
.field volatile a:Z

.field volatile b:Z

.field final synthetic c:Lcom/google/android/apps/gmm/map/internal/d/au;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/au;)V
    .locals 3

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    .line 326
    const-string v1, "CacheCommitter:"

    iget-object v0, p1, Lcom/google/android/apps/gmm/map/internal/d/au;->g:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x0

    .line 327
    iget-object v2, p1, Lcom/google/android/apps/gmm/map/internal/d/au;->k:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-interface {v2}, Lcom/google/android/apps/gmm/map/internal/d/ac;->r()Lcom/google/android/apps/gmm/map/c/a/a;

    move-result-object v2

    .line 326
    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/gmm/shared/c/a/d;-><init>(Ljava/lang/String;Lcom/google/android/apps/gmm/shared/c/a/p;Lcom/google/android/apps/gmm/map/c/a/a;)V

    .line 328
    iget v0, p1, Lcom/google/android/apps/gmm/map/internal/d/au;->i:I

    if-gez v0, :cond_1

    .line 329
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->b:Z

    .line 333
    :goto_1
    return-void

    .line 326
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 332
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/av;->start()V

    goto :goto_1
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 340
    const/16 v0, 0x13

    :try_start_0
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/internal/d/au;->b()Lcom/google/android/apps/gmm/map/internal/d/b/s;

    move-result-object v1

    .line 345
    if-nez v1, :cond_0

    .line 375
    :goto_1
    return-void

    .line 341
    :catch_0
    move-exception v0

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/internal/d/av;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Could not set thread priority: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/apps/gmm/shared/c/m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 349
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-boolean v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->j:Z

    if-eqz v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    iput-boolean v4, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->j:Z

    .line 354
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->T_()V

    .line 373
    :goto_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->b:Z

    .line 374
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-boolean v1, v1, Lcom/google/android/apps/gmm/map/internal/d/au;->j:Z

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/map/internal/d/au;->b(Z)V

    goto :goto_1

    .line 356
    :cond_1
    iput-boolean v4, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->a:Z

    .line 358
    :try_start_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget v0, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->i:I

    .line 359
    :goto_3
    if-lez v0, :cond_2

    .line 360
    const-wide/16 v2, 0xfa

    invoke-static {v2, v3}, Lcom/google/android/apps/gmm/map/internal/d/av;->sleep(J)V

    .line 361
    iget-object v2, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    iget-boolean v2, v2, Lcom/google/android/apps/gmm/map/internal/d/au;->j:Z

    if-eqz v2, :cond_3

    .line 362
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/av;->c:Lcom/google/android/apps/gmm/map/internal/d/au;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/apps/gmm/map/internal/d/au;->j:Z

    .line 368
    :cond_2
    invoke-interface {v1}, Lcom/google/android/apps/gmm/map/internal/d/b/s;->T_()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 370
    :catch_1
    move-exception v0

    goto :goto_1

    .line 365
    :cond_3
    add-int/lit16 v0, v0, -0xfa

    goto :goto_3
.end method
