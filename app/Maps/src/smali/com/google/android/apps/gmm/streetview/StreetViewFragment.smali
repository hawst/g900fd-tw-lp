.class public abstract Lcom/google/android/apps/gmm/streetview/StreetViewFragment;
.super Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;-><init>()V

    return-void
.end method

.method static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/gmm/map/b/a/q;Lcom/google/android/apps/gmm/streetview/b/a;Lcom/google/android/apps/gmm/base/activities/c;Z)V
    .locals 3
    .param p2    # Lcom/google/android/apps/gmm/map/b/a/q;
        .annotation runtime Lb/a/a;
        .end annotation
    .end param

    .prologue
    .line 54
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 55
    const-string v0, "address"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "panoId"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    if-eqz p2, :cond_0

    .line 58
    const-string v0, "latLng"

    invoke-virtual {v1, v0, p2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 60
    :cond_0
    iget-object v0, p4, Lcom/google/android/apps/gmm/base/activities/c;->d:Lcom/google/android/apps/gmm/map/t;

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/t;->d:Lcom/google/android/apps/gmm/map/ah;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/ah;->a()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v0

    .line 61
    :goto_0
    if-eqz v0, :cond_1

    .line 62
    const-string v2, "placemarkLatLng"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 64
    :cond_1
    const-string v0, "userOrientation"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 65
    const-string v0, "isInnerSpace"

    invoke-virtual {v1, v0, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 68
    new-instance v0, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/streetview/JavaStreetViewFragment;-><init>()V

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/streetview/StreetViewFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    sget-object v1, Lcom/google/android/apps/gmm/base/fragments/a/a;->a:Lcom/google/android/apps/gmm/base/fragments/a/a;

    invoke-virtual {p4, v0, v1}, Lcom/google/android/apps/gmm/base/activities/c;->a(Landroid/app/Fragment;Lcom/google/android/apps/gmm/base/fragments/a/a;)V

    .line 76
    return-void

    .line 60
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method b(Z)V
    .locals 0

    .prologue
    .line 37
    return-void
.end method
