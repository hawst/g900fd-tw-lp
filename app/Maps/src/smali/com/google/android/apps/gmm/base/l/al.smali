.class public final Lcom/google/android/apps/gmm/base/l/al;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/google/android/libraries/curvular/ce;

.field public e:Lcom/google/android/libraries/curvular/b/i;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/b/i",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public f:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/apps/gmm/z/b/l;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/google/android/libraries/curvular/ah;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/libraries/curvular/ah",
            "<",
            "Lcom/google/android/libraries/curvular/ce;",
            "Lcom/google/android/libraries/curvular/aw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/gmm/base/l/ak;
    .locals 8

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->d:Lcom/google/android/libraries/curvular/ce;

    if-nez v0, :cond_4

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->b:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->b:Lcom/google/android/libraries/curvular/ah;

    .line 89
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->c:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->c:Lcom/google/android/libraries/curvular/ah;

    .line 90
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->e:Lcom/google/android/libraries/curvular/b/i;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->f:Lcom/google/android/libraries/curvular/ah;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->f:Lcom/google/android/libraries/curvular/ah;

    .line 92
    invoke-interface {v0}, Lcom/google/android/libraries/curvular/ah;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 93
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "When build() was called, Builder had non-constant propert[y|ies], and a non-null ViewModel was not set. Set a ViewModel or make all property values constant."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :cond_4
    new-instance v0, Lcom/google/android/apps/gmm/base/l/ak;

    iget-object v1, p0, Lcom/google/android/apps/gmm/base/l/al;->a:Lcom/google/android/libraries/curvular/ah;

    iget-object v2, p0, Lcom/google/android/apps/gmm/base/l/al;->b:Lcom/google/android/libraries/curvular/ah;

    iget-object v3, p0, Lcom/google/android/apps/gmm/base/l/al;->g:Lcom/google/android/libraries/curvular/ah;

    iget-object v4, p0, Lcom/google/android/apps/gmm/base/l/al;->c:Lcom/google/android/libraries/curvular/ah;

    iget-object v5, p0, Lcom/google/android/apps/gmm/base/l/al;->d:Lcom/google/android/libraries/curvular/ce;

    iget-object v6, p0, Lcom/google/android/apps/gmm/base/l/al;->e:Lcom/google/android/libraries/curvular/b/i;

    iget-object v7, p0, Lcom/google/android/apps/gmm/base/l/al;->f:Lcom/google/android/libraries/curvular/ah;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/gmm/base/l/ak;-><init>(Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ah;Lcom/google/android/libraries/curvular/ce;Lcom/google/android/libraries/curvular/b/i;Lcom/google/android/libraries/curvular/ah;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/libraries/curvular/ce;)Lcom/google/android/apps/gmm/base/l/al;
    .locals 2

    .prologue
    .line 55
    invoke-static {p1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "An actual ViewModel instance is required, not a proxy."

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 57
    :cond_1
    iput-object p1, p0, Lcom/google/android/apps/gmm/base/l/al;->d:Lcom/google/android/libraries/curvular/ce;

    .line 58
    return-object p0
.end method

.method public final a(Ljava/lang/Runnable;)Lcom/google/android/apps/gmm/base/l/al;
    .locals 1

    .prologue
    .line 67
    invoke-static {p1}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Class;

    invoke-static {p1, v0}, Lcom/google/android/libraries/curvular/b/e;->a(Ljava/lang/Object;[Ljava/lang/Class;)Lcom/google/android/libraries/curvular/b/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->e:Lcom/google/android/libraries/curvular/b/i;

    .line 78
    :goto_0
    return-object p0

    .line 70
    :cond_0
    new-instance v0, Lcom/google/android/apps/gmm/base/l/am;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/gmm/base/l/am;-><init>(Lcom/google/android/apps/gmm/base/l/al;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/base/l/al;->e:Lcom/google/android/libraries/curvular/b/i;

    goto :goto_0
.end method
