.class public Lcom/google/android/apps/gmm/iamhere/c/h;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/apps/gmm/iamhere/c/h;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Lcom/google/android/apps/gmm/iamhere/c/h;

.field public static final b:J

.field public static final c:J


# instance fields
.field public final d:Lcom/google/android/apps/gmm/iamhere/c/r;

.field public final e:J

.field public final f:J

.field public final g:Ljava/lang/Object;

.field public final h:J

.field public final i:Lcom/google/b/c/dn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final j:J


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 378
    new-instance v1, Lcom/google/android/apps/gmm/iamhere/c/h;

    const-wide/16 v2, 0x0

    sget-object v4, Lcom/google/android/apps/gmm/iamhere/c/r;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    const-string v5, ""

    const-string v6, ""

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/gmm/iamhere/c/h;-><init>(JLcom/google/android/apps/gmm/iamhere/c/r;Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/apps/gmm/iamhere/c/h;->a:Lcom/google/android/apps/gmm/iamhere/c/h;

    .line 387
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/iamhere/c/h;->b:J

    .line 394
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/apps/gmm/iamhere/c/h;->c:J

    .line 662
    new-instance v0, Lcom/google/android/apps/gmm/iamhere/c/i;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/iamhere/c/i;-><init>()V

    sput-object v0, Lcom/google/android/apps/gmm/iamhere/c/h;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/google/android/apps/gmm/iamhere/c/r;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 11

    .prologue
    const-wide/16 v5, 0x0

    .line 470
    .line 474
    invoke-static/range {p5 .. p5}, Lcom/google/b/c/dn;->b(Ljava/lang/Object;)Lcom/google/b/c/dn;

    move-result-object v7

    move-object v0, p0

    move-object v1, p3

    move-wide v2, p1

    move-object v4, p4

    move-wide v8, v5

    .line 470
    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/gmm/iamhere/c/h;-><init>(Lcom/google/android/apps/gmm/iamhere/c/r;JLjava/lang/Object;JLcom/google/b/c/dn;J)V

    .line 476
    return-void
.end method

.method public constructor <init>(Lcom/google/android/apps/gmm/iamhere/c/r;JJLjava/lang/Object;JLcom/google/b/c/dn;J)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/iamhere/c/r;",
            "JJ",
            "Ljava/lang/Object;",
            "J",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 442
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 443
    iput-object p1, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    .line 444
    iput-wide p2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    .line 445
    iput-wide p4, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    .line 446
    iput-object p6, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    .line 447
    iput-wide p7, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    .line 448
    iput-object p9, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    .line 449
    iput-wide p10, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    .line 450
    return-void
.end method

.method private constructor <init>(Lcom/google/android/apps/gmm/iamhere/c/r;JLjava/lang/Object;JLcom/google/b/c/dn;J)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/apps/gmm/iamhere/c/r;",
            "J",
            "Ljava/lang/Object;",
            "J",
            "Lcom/google/b/c/dn",
            "<",
            "Ljava/lang/String;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 460
    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p2

    move-object/from16 v6, p4

    move-wide/from16 v7, p5

    move-object/from16 v9, p7

    move-wide/from16 v10, p8

    invoke-direct/range {v0 .. v11}, Lcom/google/android/apps/gmm/iamhere/c/h;-><init>(Lcom/google/android/apps/gmm/iamhere/c/r;JJLjava/lang/Object;JLcom/google/b/c/dn;J)V

    .line 467
    return-void
.end method

.method private c(J)Z
    .locals 5

    .prologue
    .line 618
    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    cmp-long v0, v0, p1

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    sget-wide v2, Lcom/google/android/apps/gmm/iamhere/c/h;->b:J

    add-long/2addr v0, v2

    cmp-long v0, v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Lcom/google/android/apps/gmm/iamhere/c/j;
    .locals 7

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    invoke-virtual {v0}, Lcom/google/b/c/dn;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    sget-wide v2, Lcom/google/android/apps/gmm/iamhere/c/h;->c:J

    add-long/2addr v0, v2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/c/h;->c(J)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    sget-wide v4, Lcom/google/android/apps/gmm/iamhere/c/h;->b:J

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_0
    cmp-long v0, v0, p1

    if-gtz v0, :cond_1

    .line 561
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->d:Lcom/google/android/apps/gmm/iamhere/c/j;

    .line 569
    :goto_0
    return-object v0

    .line 563
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    invoke-virtual {v0}, Lcom/google/b/c/dn;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 564
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->c:Lcom/google/android/apps/gmm/iamhere/c/j;

    goto :goto_0

    .line 566
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/c/h;->c(J)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 567
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->b:Lcom/google/android/apps/gmm/iamhere/c/j;

    goto :goto_0

    .line 569
    :cond_3
    sget-object v0, Lcom/google/android/apps/gmm/iamhere/c/j;->a:Lcom/google/android/apps/gmm/iamhere/c/j;

    goto :goto_0
.end method

.method public final b(J)J
    .locals 7

    .prologue
    .line 578
    const-wide v0, 0x7fffffffffffffffL

    .line 579
    sget-object v2, Lcom/google/android/apps/gmm/iamhere/c/c;->a:[I

    invoke-virtual {p0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/c/h;->a(J)Lcom/google/android/apps/gmm/iamhere/c/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/iamhere/c/j;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 585
    :cond_0
    :goto_0
    return-wide v0

    .line 581
    :pswitch_0
    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    sget-wide v2, Lcom/google/android/apps/gmm/iamhere/c/h;->c:J

    add-long/2addr v0, v2

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/iamhere/c/h;->c(J)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    sget-wide v4, Lcom/google/android/apps/gmm/iamhere/c/h;->b:J

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 584
    :pswitch_1
    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    sget-wide v2, Lcom/google/android/apps/gmm/iamhere/c/h;->b:J

    add-long/2addr v0, v2

    goto :goto_0

    .line 579
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 648
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 480
    instance-of v2, p1, Lcom/google/android/apps/gmm/iamhere/c/h;

    if-eqz v2, :cond_3

    .line 481
    check-cast p1, Lcom/google/android/apps/gmm/iamhere/c/h;

    .line 482
    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    iget-object v3, p1, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    .line 484
    invoke-virtual {v2, v3}, Lcom/google/android/apps/gmm/iamhere/c/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    iget-object v3, p1, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    .line 485
    invoke-virtual {v2, v3}, Lcom/google/b/c/dn;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    iget-wide v4, p1, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    .line 488
    if-eq v2, v3, :cond_0

    if-eqz v2, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    .line 490
    :goto_1
    return v0

    :cond_1
    move v2, v1

    .line 488
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 490
    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 495
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    .line 496
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    .line 495
    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 10

    .prologue
    const-wide v8, 0x7fffffffffffffffL

    const-wide/16 v6, 0x0

    .line 627
    new-instance v0, Lcom/google/android/apps/gmm/shared/c/a;

    invoke-direct {v0}, Lcom/google/android/apps/gmm/shared/c/a;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/shared/c/a;->a()J

    move-result-wide v0

    .line 628
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/iamhere/c/h;->b(J)J

    move-result-wide v2

    .line 629
    new-instance v4, Lcom/google/b/a/ah;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/b/a/ah;-><init>(Ljava/lang/String;)V

    const-string v5, "state"

    .line 630
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/gmm/iamhere/c/h;->a(J)Lcom/google/android/apps/gmm/iamhere/c/j;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v4, "update-time"

    .line 631
    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    cmp-long v0, v2, v8

    if-eqz v0, :cond_0

    const-string v0, "HH:mm:ss"

    invoke-static {v0, v2, v3}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v4, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "start"

    iget-wide v4, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    .line 632
    cmp-long v0, v4, v6

    if-eqz v0, :cond_1

    cmp-long v0, v4, v8

    if-eqz v0, :cond_1

    const-string v0, "HH:mm:ss"

    invoke-static {v0, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "last"

    iget-wide v4, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    .line 633
    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    cmp-long v0, v4, v8

    if-eqz v0, :cond_2

    const-string v0, "HH:mm:ss"

    invoke-static {v0, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_2
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "dismissal"

    iget-wide v4, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    .line 634
    cmp-long v0, v4, v6

    if-eqz v0, :cond_3

    cmp-long v0, v4, v8

    if-eqz v0, :cond_3

    const-string v0, "HH:mm:ss"

    invoke-static {v0, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_3
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    const-string v1, "sources"

    iget-object v2, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    .line 635
    invoke-virtual {v0, v1, v2}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v1

    const-string v2, "removal"

    iget-wide v4, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    .line 636
    cmp-long v0, v4, v6

    if-eqz v0, :cond_4

    cmp-long v0, v4, v8

    if-eqz v0, :cond_4

    const-string v0, "HH:mm:ss"

    invoke-static {v0, v4, v5}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_4
    invoke-virtual {v1, v2, v0}, Lcom/google/b/a/ah;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/b/a/ah;

    move-result-object v0

    .line 637
    invoke-virtual {v0}, Lcom/google/b/a/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 631
    :cond_0
    const-string v0, "-"

    goto :goto_0

    .line 632
    :cond_1
    const-string v0, "-"

    goto :goto_1

    .line 633
    :cond_2
    const-string v0, "-"

    goto :goto_2

    .line 634
    :cond_3
    const-string v0, "-"

    goto :goto_3

    .line 636
    :cond_4
    const-string v0, "-"

    goto :goto_4
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->d:Lcom/google/android/apps/gmm/iamhere/c/r;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/iamhere/c/r;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 654
    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 655
    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->f:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 656
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->g:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 657
    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->h:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 658
    iget-object v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->i:Lcom/google/b/c/dn;

    invoke-static {v0}, Lcom/google/b/c/cv;->a(Ljava/util/Collection;)Lcom/google/b/c/cv;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 659
    iget-wide v0, p0, Lcom/google/android/apps/gmm/iamhere/c/h;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 660
    return-void
.end method
