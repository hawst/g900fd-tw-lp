.class public Lcom/google/android/apps/gmm/navigation/f/i;
.super Lcom/google/android/apps/gmm/navigation/commonui/a;
.source "PG"


# instance fields
.field final a:Lcom/google/android/apps/gmm/navigation/f/r;

.field public final b:Lcom/google/android/apps/gmm/navigation/c/f;

.field public c:Lcom/google/android/apps/gmm/navigation/c/a/a;

.field private final d:Lcom/google/android/apps/gmm/map/util/b/g;

.field private final e:Lcom/google/android/apps/gmm/map/t;

.field private final f:Lcom/google/android/apps/gmm/navigation/commonui/g;

.field private final g:Lcom/google/android/apps/gmm/navigation/commonui/h;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/navigation/f/q;Lcom/google/android/apps/gmm/navigation/f/r;Lcom/google/android/apps/gmm/map/util/b/g;Lcom/google/android/apps/gmm/navigation/c/f;Lcom/google/android/apps/gmm/map/t;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/apps/gmm/navigation/commonui/a;-><init>()V

    .line 92
    new-instance v0, Lcom/google/android/apps/gmm/navigation/f/j;

    invoke-direct {v0, p0}, Lcom/google/android/apps/gmm/navigation/f/j;-><init>(Lcom/google/android/apps/gmm/navigation/f/i;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->g:Lcom/google/android/apps/gmm/navigation/commonui/h;

    .line 52
    const-string v0, "stateController"

    if-nez p2, :cond_0

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    check-cast p2, Lcom/google/android/apps/gmm/navigation/f/r;

    iput-object p2, p0, Lcom/google/android/apps/gmm/navigation/f/i;->a:Lcom/google/android/apps/gmm/navigation/f/r;

    .line 53
    const-string v0, "eventBus"

    if-nez p3, :cond_1

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast p3, Lcom/google/android/apps/gmm/map/util/b/g;

    iput-object p3, p0, Lcom/google/android/apps/gmm/navigation/f/i;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    .line 54
    const-string v0, "cameraController"

    if-nez p4, :cond_2

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    check-cast p4, Lcom/google/android/apps/gmm/navigation/c/f;

    iput-object p4, p0, Lcom/google/android/apps/gmm/navigation/f/i;->b:Lcom/google/android/apps/gmm/navigation/c/f;

    .line 55
    const-string v0, "mapContainer"

    if-nez p5, :cond_3

    new-instance v1, Ljava/lang/NullPointerException;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    move-object v0, p5

    check-cast v0, Lcom/google/android/apps/gmm/map/t;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->e:Lcom/google/android/apps/gmm/map/t;

    .line 56
    new-instance v0, Lcom/google/android/apps/gmm/navigation/commonui/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/f/i;->g:Lcom/google/android/apps/gmm/navigation/commonui/h;

    invoke-direct {v0, p1, p5, v1}, Lcom/google/android/apps/gmm/navigation/commonui/g;-><init>(Lcom/google/android/apps/gmm/navigation/commonui/d;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/navigation/commonui/h;)V

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->f:Lcom/google/android/apps/gmm/navigation/commonui/g;

    .line 57
    sget-object v0, Lcom/google/android/apps/gmm/navigation/c/a/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 58
    return-void
.end method


# virtual methods
.method public G_()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/f/i;->f:Lcom/google/android/apps/gmm/navigation/commonui/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->e(Ljava/lang/Object;)V

    .line 68
    return-void
.end method

.method public a(Lcom/google/android/apps/gmm/navigation/f/t;)V
    .locals 1
    .annotation runtime Lcom/google/android/apps/gmm/shared/c/a/q;
        a = .enum Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;
    .end annotation

    .annotation runtime Lcom/google/b/d/c;
    .end annotation

    .prologue
    .line 84
    iget-object v0, p1, Lcom/google/android/apps/gmm/navigation/f/t;->a:Lcom/google/android/apps/gmm/navigation/f/b/a;

    iget-object v0, v0, Lcom/google/android/apps/gmm/navigation/commonui/b/a;->a:Lcom/google/android/apps/gmm/navigation/c/a/a;

    iput-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->c:Lcom/google/android/apps/gmm/navigation/c/a/a;

    .line 85
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    invoke-interface {v0, p0}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/gmm/navigation/f/i;->d:Lcom/google/android/apps/gmm/map/util/b/g;

    iget-object v1, p0, Lcom/google/android/apps/gmm/navigation/f/i;->f:Lcom/google/android/apps/gmm/navigation/commonui/g;

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/map/util/b/g;->d(Ljava/lang/Object;)V

    .line 75
    return-void
.end method
