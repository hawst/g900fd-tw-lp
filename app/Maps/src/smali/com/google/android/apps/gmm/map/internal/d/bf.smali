.class Lcom/google/android/apps/gmm/map/internal/d/bf;
.super Lcom/google/android/apps/gmm/map/internal/d/b;
.source "PG"


# instance fields
.field final synthetic o:Lcom/google/android/apps/gmm/map/internal/d/be;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/map/internal/d/be;Z)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->o:Lcom/google/android/apps/gmm/map/internal/d/be;

    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/gmm/map/internal/d/b;-><init>(Lcom/google/android/apps/gmm/map/internal/d/a;Z)V

    return-void
.end method


# virtual methods
.method protected final a(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bo;
    .locals 13

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_1

    .line 132
    const/4 v0, 0x0

    .line 152
    :cond_0
    :goto_0
    return-object v0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v1, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->b:[[B

    aget-object v2, v0, p1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->b:[[B

    aget-object v0, v0, p1

    array-length v4, v0

    .line 140
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v5, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    .line 141
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->o:Lcom/google/android/apps/gmm/map/internal/d/be;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/be;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v6}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v6

    .line 142
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->o:Lcom/google/android/apps/gmm/map/internal/d/be;

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/d/be;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v8}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v8

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->o:Lcom/google/android/apps/gmm/map/internal/d/be;

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/be;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    .line 143
    invoke-interface {v0}, Lcom/google/android/apps/gmm/map/internal/d/ac;->B()Lcom/google/android/apps/gmm/map/internal/c/o;

    move-result-object v10

    new-instance v11, Lcom/google/android/apps/gmm/map/internal/c/cr;

    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->a:I

    iget-object v12, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->d:[I

    aget v12, v12, p1

    invoke-direct {v11, v0, v12}, Lcom/google/android/apps/gmm/map/internal/c/cr;-><init>(II)V

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->e:[I

    aget v12, v0, p1

    .line 135
    invoke-static/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/cm;->a(Lcom/google/android/apps/gmm/map/internal/c/bp;[BIILcom/google/android/apps/gmm/map/b/a/ai;JJLcom/google/android/apps/gmm/map/internal/c/o;Lcom/google/android/apps/gmm/map/internal/c/cr;I)Lcom/google/android/apps/gmm/map/internal/c/cm;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->o:Lcom/google/android/apps/gmm/map/internal/d/be;

    iget-object v1, v1, Lcom/google/android/apps/gmm/map/internal/d/be;->o:Lcom/google/android/apps/gmm/map/b/a/ai;

    sget-object v2, Lcom/google/android/apps/gmm/map/b/a/ai;->x:Lcom/google/android/apps/gmm/map/b/a/ai;

    if-ne v1, v2, :cond_0

    .line 150
    check-cast v0, Lcom/google/android/apps/gmm/map/internal/c/cm;

    invoke-static {v0}, Lcom/google/android/apps/gmm/map/internal/c/aj;->a(Lcom/google/android/apps/gmm/map/internal/c/cm;)Lcom/google/android/apps/gmm/map/internal/c/bo;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(I)[B
    .locals 4

    .prologue
    .line 176
    iget v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->a:I

    iget-object v1, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->d:[I

    aget v1, v1, p1

    const/16 v2, 0x8

    new-array v2, v2, [B

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/apps/gmm/map/internal/c/ai;->a(I[BI)V

    const/4 v0, 0x4

    invoke-static {v1, v2, v0}, Lcom/google/android/apps/gmm/map/internal/c/ai;->a(I[BI)V

    return-object v2
.end method

.method protected final b(ILcom/google/android/apps/gmm/shared/c/f;)Lcom/google/android/apps/gmm/map/internal/c/bt;
    .locals 13

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->b:[[B

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 158
    const/4 v1, 0x0

    .line 171
    :goto_0
    return-object v1

    .line 161
    :cond_0
    new-instance v1, Lcom/google/android/apps/gmm/map/internal/c/ct;

    .line 162
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v2, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->a:Lcom/google/android/apps/gmm/map/internal/c/bp;

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v3, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->b:[[B

    aget-object v4, v0, p1

    const/4 v5, 0x0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v6, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->o:Lcom/google/android/apps/gmm/map/internal/d/be;

    iget-object v6, v6, Lcom/google/android/apps/gmm/map/internal/d/be;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v6}, Lcom/google/android/apps/gmm/map/b/a/ai;->a(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v6

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/i;->h:[Lcom/google/android/apps/gmm/map/internal/d/p;

    aget-object v0, v0, p1

    iget-object v0, v0, Lcom/google/android/apps/gmm/map/internal/d/p;->g:Lcom/google/android/apps/gmm/map/b/a/ai;

    iget-object v8, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->o:Lcom/google/android/apps/gmm/map/internal/d/be;

    iget-object v8, v8, Lcom/google/android/apps/gmm/map/internal/d/be;->j:Lcom/google/android/apps/gmm/map/internal/d/ac;

    invoke-virtual {v0, p2, v8}, Lcom/google/android/apps/gmm/map/b/a/ai;->b(Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/android/apps/gmm/shared/net/ad;)J

    move-result-wide v8

    iget v10, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->a:I

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->d:[I

    aget v11, v0, p1

    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/d/bf;->e:[I

    aget v12, v0, p1

    invoke-direct/range {v1 .. v12}, Lcom/google/android/apps/gmm/map/internal/c/ct;-><init>(Lcom/google/android/apps/gmm/map/internal/c/bp;Lcom/google/android/apps/gmm/map/b/a/ai;[BIJJIII)V

    goto :goto_0
.end method
