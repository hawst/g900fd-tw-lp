.class Lcom/google/android/apps/gmm/directions/ar;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcom/google/android/apps/gmm/cardui/a/e;


# instance fields
.field final synthetic a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;


# direct methods
.method constructor <init>(Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;)V
    .locals 0

    .prologue
    .line 880
    iput-object p1, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/cardui/a/f;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 891
    invoke-interface {p1}, Lcom/google/android/apps/gmm/cardui/a/f;->a()Lcom/google/o/h/a/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/o/h/a/a;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/ez;->g()Lcom/google/o/h/a/ez;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/ez;

    .line 892
    invoke-virtual {v0}, Lcom/google/o/h/a/ez;->d()Ljava/util/List;

    move-result-object v2

    .line 893
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x2

    if-ge v1, v3, :cond_0

    .line 894
    sget-object v0, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->c:Ljava/lang/String;

    .line 895
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x41

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "DirectionAction is triggered, but waypointList.size()="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 914
    :goto_0
    return-void

    .line 898
    :cond_0
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ph;

    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-static {v1, v3}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v3

    .line 900
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/ph;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    .line 899
    invoke-static {v1, v2}, Lcom/google/android/apps/gmm/cardui/f/e;->a(Lcom/google/o/h/a/ph;Landroid/content/Context;)Lcom/google/android/apps/gmm/map/r/a/ap;

    move-result-object v4

    .line 901
    iget-object v1, v0, Lcom/google/o/h/a/ez;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/et;->d()Lcom/google/o/h/a/et;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v1

    check-cast v1, Lcom/google/o/h/a/et;

    iget v1, v1, Lcom/google/o/h/a/et;->b:I

    invoke-static {v1}, Lcom/google/maps/g/a/hm;->a(I)Lcom/google/maps/g/a/hm;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/google/maps/g/a/hm;->a:Lcom/google/maps/g/a/hm;

    move-object v2, v1

    .line 902
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    .line 903
    iget-object v1, v1, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->j:Lcom/google/android/apps/gmm/base/activities/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/activities/c;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/gmm/map/c/b;->a(Landroid/content/Context;)Lcom/google/android/apps/gmm/map/c/a;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/gmm/base/a;

    invoke-interface {v1}, Lcom/google/android/apps/gmm/base/a;->f()Lcom/google/android/apps/gmm/shared/c/f;

    move-result-object v1

    .line 904
    iget-object v0, v0, Lcom/google/o/h/a/ez;->c:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/o/h/a/et;->d()Lcom/google/o/h/a/et;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/o/h/a/et;

    iget-object v0, v0, Lcom/google/o/h/a/et;->d:Lcom/google/n/ao;

    invoke-static {}, Lcom/google/r/b/a/afz;->d()Lcom/google/r/b/a/afz;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/n/ao;->b(Lcom/google/n/at;)Lcom/google/n/at;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/afz;

    .line 902
    invoke-static {v2, v1, v0}, Lcom/google/android/apps/gmm/directions/f/d/c;->a(Lcom/google/maps/g/a/hm;Lcom/google/android/apps/gmm/shared/c/f;Lcom/google/r/b/a/afz;)Lcom/google/r/b/a/afz;

    move-result-object v0

    .line 905
    iget-object v1, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v1, v1, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    monitor-enter v1

    .line 906
    :try_start_0
    iget-object v5, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v5, v5, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v5, v3}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 907
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v3, v4}, Lcom/google/android/apps/gmm/directions/av;->b(Lcom/google/android/apps/gmm/map/r/a/ap;)V

    .line 908
    iget-object v3, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v3, v3, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v3, v2}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/maps/g/a/hm;)V

    .line 909
    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    iget-object v2, v2, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->d:Lcom/google/android/apps/gmm/directions/av;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/gmm/directions/av;->a(Lcom/google/r/b/a/afz;)V

    .line 910
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 911
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->a(Lcom/google/maps/g/hy;)Z

    .line 912
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;->i()V

    .line 913
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/ar;->a:Lcom/google/android/apps/gmm/directions/DirectionsStartPageFragment;

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/apps/gmm/base/fragments/GmmActivityFragment;->e_()Lcom/google/android/apps/gmm/base/fragments/a/a;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/apps/gmm/base/activities/c;->a(Ljava/lang/Class;Lcom/google/android/apps/gmm/base/fragments/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v6}, Landroid/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    goto/16 :goto_0

    :cond_1
    move-object v2, v1

    .line 901
    goto :goto_1

    .line 910
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/google/o/h/a/g;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 887
    sget-object v0, Lcom/google/o/h/a/g;->d:Lcom/google/o/h/a/g;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 888
    return-void
.end method

.method public final a(Lcom/google/o/h/a/a;)Z
    .locals 2

    .prologue
    .line 883
    iget v0, p1, Lcom/google/o/h/a/a;->a:I

    and-int/lit8 v0, v0, 0x2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
