.class public Lcom/google/android/apps/gmm/map/internal/c/ai;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lcom/google/android/apps/gmm/map/internal/c/bi;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 65
    .line 66
    if-eqz p0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v4, v0

    move v2, v1

    move v0, v1

    .line 68
    :goto_0
    if-ge v2, v4, :cond_1

    .line 69
    iget-object v3, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    iget-object v5, p0, Lcom/google/android/apps/gmm/map/internal/c/bi;->b:[Lcom/google/android/apps/gmm/map/internal/c/be;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    invoke-static {v2, v1, v5}, Lcom/google/android/apps/gmm/shared/c/s;->a(III)I

    move-result v5

    aget-object v3, v3, v5

    invoke-virtual {v3}, Lcom/google/android/apps/gmm/map/internal/c/be;->f()I

    move-result v3

    add-int/2addr v3, v0

    .line 68
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_0

    :cond_0
    move v0, v1

    .line 72
    :cond_1
    return v0
.end method

.method public static a(I[BI)V
    .locals 3

    .prologue
    .line 160
    add-int/lit8 v0, p2, 0x1

    shr-int/lit8 v1, p0, 0x18

    aput-byte v1, p1, p2

    .line 161
    add-int/lit8 v1, v0, 0x1

    shr-int/lit8 v2, p0, 0x10

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    .line 162
    add-int/lit8 v0, v1, 0x1

    shr-int/lit8 v2, p0, 0x8

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    .line 163
    int-to-byte v1, p0

    aput-byte v1, p1, v0

    .line 164
    return-void
.end method

.method public static a(II)Z
    .locals 1

    .prologue
    .line 150
    and-int v0, p0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
