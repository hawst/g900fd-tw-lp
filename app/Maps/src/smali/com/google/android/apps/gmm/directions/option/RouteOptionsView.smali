.class public Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:Landroid/widget/ListView;

.field public c:Lcom/google/android/apps/gmm/directions/f/c;
    .annotation runtime Lb/a/a;
    .end annotation
.end field

.field public d:Lcom/google/android/apps/gmm/directions/option/l;
    .annotation runtime Lb/a/a;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/apps/gmm/h;->p:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->b:Landroid/widget/ListView;

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->b:Landroid/widget/ListView;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->addView(Landroid/view/View;)V

    .line 92
    return-void
.end method

.method private a(Ljava/lang/Integer;I)V
    .locals 3

    .prologue
    .line 132
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/gmm/directions/f/b/m;->a(I)Lcom/google/android/apps/gmm/directions/f/b/m;

    move-result-object v1

    .line 133
    if-nez v1, :cond_1

    .line 134
    sget-object v0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->a:Ljava/lang/String;

    .line 147
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-virtual {v0, v1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v0, v0, Lcom/google/android/apps/gmm/directions/f/c;->b:Ljava/util/EnumMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->d:Lcom/google/android/apps/gmm/directions/option/l;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->d:Lcom/google/android/apps/gmm/directions/option/l;

    new-instance v1, Lcom/google/android/apps/gmm/directions/f/c;

    iget-object v2, p0, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->c:Lcom/google/android/apps/gmm/directions/f/c;

    invoke-direct {v1, v2}, Lcom/google/android/apps/gmm/directions/f/c;-><init>(Lcom/google/android/apps/gmm/directions/f/c;)V

    invoke-interface {v0, v1}, Lcom/google/android/apps/gmm/directions/option/l;->a(Lcom/google/android/apps/gmm/directions/f/c;)V

    goto :goto_0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->a(Ljava/lang/Integer;I)V

    .line 124
    return-void

    .line 123
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 1

    .prologue
    .line 128
    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/apps/gmm/directions/option/RouteOptionsView;->a(Ljava/lang/Integer;I)V

    .line 129
    return-void
.end method
