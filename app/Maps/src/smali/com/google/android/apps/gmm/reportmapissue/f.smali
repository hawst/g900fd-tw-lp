.class public Lcom/google/android/apps/gmm/reportmapissue/f;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    return-void
.end method

.method private static a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/bq;",
            ">;I",
            "Lcom/google/android/apps/gmm/reportmapissue/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 199
    iget-object v0, p2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v2, p2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    .line 207
    :goto_1
    return-void

    .line 199
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 202
    :cond_1
    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v0

    .line 203
    iget v2, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/maps/g/bs;->a:I

    iput p1, v0, Lcom/google/maps/g/bs;->b:I

    .line 204
    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget v4, v2, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v2, Lcom/google/maps/g/bw;->a:I

    iput-boolean v3, v2, Lcom/google/maps/g/bw;->c:Z

    iget-object v3, v0, Lcom/google/maps/g/bs;->c:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v2, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v0, Lcom/google/maps/g/bs;->a:I

    .line 205
    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    iget v4, v2, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v2, Lcom/google/maps/g/bw;->a:I

    iput-boolean v3, v2, Lcom/google/maps/g/bw;->c:Z

    iget-object v3, v0, Lcom/google/maps/g/bs;->d:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v4, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v3, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v5, v3, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v3, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/g/bs;->a:I

    .line 206
    invoke-virtual {v0}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private static a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/g;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/bq;",
            ">;I",
            "Lcom/google/android/apps/gmm/reportmapissue/a/g;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 158
    iget-object v2, p2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_0

    .line 170
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v3

    .line 162
    iget v2, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v3, Lcom/google/maps/g/bs;->a:I

    iput p1, v3, Lcom/google/maps/g/bs;->b:I

    .line 163
    iget-object v2, p2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->b:Ljava/lang/String;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    move v2, v1

    :goto_1
    if-nez v2, :cond_4

    .line 164
    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v2

    iget-object v4, p2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->b:Ljava/lang/String;

    if-nez v4, :cond_3

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    move v2, v0

    .line 163
    goto :goto_1

    .line 164
    :cond_3
    iget v5, v2, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v2, Lcom/google/maps/g/bw;->a:I

    iput-object v4, v2, Lcom/google/maps/g/bw;->b:Ljava/lang/Object;

    iget-object v4, v3, Lcom/google/maps/g/bs;->c:Lcom/google/n/ao;

    invoke-virtual {v2}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v4, Lcom/google/n/ao;->d:Z

    iget v2, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v3, Lcom/google/maps/g/bs;->a:I

    .line 166
    :cond_4
    iget-object v2, p2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_6

    :cond_5
    move v0, v1

    :cond_6
    if-nez v0, :cond_8

    .line 167
    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v0

    iget-object v2, p2, Lcom/google/android/apps/gmm/reportmapissue/a/g;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    iget v4, v0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/maps/g/bw;->a:I

    iput-object v2, v0, Lcom/google/maps/g/bw;->b:Ljava/lang/Object;

    iget-object v2, v3, Lcom/google/maps/g/bs;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v4, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v6, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    iput-boolean v1, v2, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v3, Lcom/google/maps/g/bs;->a:I

    .line 169
    :cond_8
    invoke-virtual {v3}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private static b(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/maps/g/bq;",
            ">;I",
            "Lcom/google/android/apps/gmm/reportmapissue/a/a;",
            ")V"
        }
    .end annotation

    .prologue
    .line 216
    iget-object v0, p2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v1, p2, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    .line 222
    :goto_1
    return-void

    .line 216
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 219
    :cond_1
    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v0

    .line 220
    iget v1, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/maps/g/bs;->a:I

    iput p1, v0, Lcom/google/maps/g/bs;->b:I

    .line 221
    invoke-virtual {v0}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/gmm/reportmapissue/a/c;Lcom/google/android/apps/gmm/shared/net/r;Lcom/google/android/apps/gmm/map/t;Lcom/google/android/apps/gmm/o/a/c;Lcom/google/android/apps/gmm/reportmapissue/h;Z)V
    .locals 10

    .prologue
    .line 53
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p6, :cond_8

    iget-object v0, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->k:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    iget-object v1, v0, Lcom/google/android/apps/gmm/reportmapissue/a/a;->b:Ljava/lang/Boolean;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->l:Lcom/google/android/apps/gmm/reportmapissue/a/e;

    iget-object v0, v0, Lcom/google/android/apps/gmm/reportmapissue/a/e;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_7

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->l:Lcom/google/android/apps/gmm/reportmapissue/a/e;

    iget-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/a/e;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/a/e;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/a/e;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v4, Lcom/google/android/apps/gmm/reportmapissue/a/e;->b:[Ljava/lang/Integer;

    array-length v4, v4

    if-lt v0, v4, :cond_4

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/google/android/apps/gmm/reportmapissue/a/e;->a:Ljava/util/Map;

    sget-object v4, Lcom/google/android/apps/gmm/reportmapissue/a/e;->b:[Ljava/lang/Integer;

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/e;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v1, v4, v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/maps/g/ot;

    iget v0, v0, Lcom/google/maps/g/ot;->F:I

    iget v1, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v3, Lcom/google/maps/g/bs;->a:I

    iput v0, v3, Lcom/google/maps/g/bs;->b:I

    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v0

    const/4 v1, 0x0

    iget v4, v0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/maps/g/bw;->a:I

    iput-boolean v1, v0, Lcom/google/maps/g/bw;->c:Z

    iget-object v1, v3, Lcom/google/maps/g/bs;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v4, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Lcom/google/maps/g/bs;->a:I

    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v0

    const/4 v1, 0x1

    iget v4, v0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v4, v4, 0x2

    iput v4, v0, Lcom/google/maps/g/bw;->a:I

    iput-boolean v1, v0, Lcom/google/maps/g/bw;->c:Z

    iget-object v1, v3, Lcom/google/maps/g/bs;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v4, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v3, Lcom/google/maps/g/bs;->a:I

    invoke-virtual {v3}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    :goto_3
    iget-object v0, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->n:Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_e

    :cond_6
    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_10

    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v0

    const/16 v1, 0xd

    iget v3, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/maps/g/bs;->a:I

    iput v1, v0, Lcom/google/maps/g/bs;->b:I

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->n:Ljava/lang/String;

    if-nez v1, :cond_f

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_7
    const/16 v0, 0x9

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->k:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    goto :goto_3

    :cond_8
    const/4 v0, 0x1

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->b:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/g;)V

    const/4 v0, 0x2

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->c:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/g;)V

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->m:Lcom/google/android/apps/gmm/reportmapissue/a/b;

    iget-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/google/maps/g/bq;->newBuilder()Lcom/google/maps/g/bs;

    move-result-object v3

    const/4 v0, 0x5

    iget v4, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/maps/g/bs;->a:I

    iput v0, v3, Lcom/google/maps/g/bs;->b:I

    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v5, v4, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/maps/g/ha;->a:I

    iput-wide v6, v4, Lcom/google/maps/g/ha;->b:D

    iget-object v5, v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;->b:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v5, v4, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v4, Lcom/google/maps/g/ha;->a:I

    iput-wide v6, v4, Lcom/google/maps/g/ha;->c:D

    iget-object v5, v0, Lcom/google/maps/g/bw;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v4

    iget-object v6, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v5, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v4, 0x0

    iput-object v4, v5, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v4, 0x1

    iput-boolean v4, v5, Lcom/google/n/ao;->d:Z

    iget v4, v0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v4, v4, 0x4

    iput v4, v0, Lcom/google/maps/g/bw;->a:I

    iget-object v4, v3, Lcom/google/maps/g/bs;->c:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v5, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v4, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v3, Lcom/google/maps/g/bs;->a:I

    iget-object v0, v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_5
    if-eqz v0, :cond_9

    invoke-static {}, Lcom/google/maps/g/bu;->newBuilder()Lcom/google/maps/g/bw;

    move-result-object v0

    invoke-static {}, Lcom/google/maps/g/gy;->newBuilder()Lcom/google/maps/g/ha;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v5, Lcom/google/android/apps/gmm/map/b/a/q;->a:D

    iget v5, v4, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v4, Lcom/google/maps/g/ha;->a:I

    iput-wide v6, v4, Lcom/google/maps/g/ha;->b:D

    iget-object v1, v1, Lcom/google/android/apps/gmm/reportmapissue/a/b;->c:Lcom/google/android/apps/gmm/map/b/a/q;

    iget-wide v6, v1, Lcom/google/android/apps/gmm/map/b/a/q;->b:D

    iget v1, v4, Lcom/google/maps/g/ha;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v4, Lcom/google/maps/g/ha;->a:I

    iput-wide v6, v4, Lcom/google/maps/g/ha;->c:D

    iget-object v1, v0, Lcom/google/maps/g/bw;->d:Lcom/google/n/ao;

    invoke-virtual {v4}, Lcom/google/maps/g/ha;->g()Lcom/google/n/t;

    move-result-object v4

    iget-object v5, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v4, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v4, 0x0

    iput-object v4, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/bw;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/g/bw;->a:I

    iget-object v1, v3, Lcom/google/maps/g/bs;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/bw;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v4, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v3, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v3, Lcom/google/maps/g/bs;->a:I

    :cond_9
    invoke-virtual {v3}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_a
    const/4 v0, 0x3

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->d:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/g;)V

    const/4 v0, 0x4

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->e:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/g;)V

    iget-boolean v0, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->p:Z

    if-eqz v0, :cond_d

    const/4 v0, 0x6

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->f:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->b(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    :cond_b
    :goto_6
    const/16 v0, 0xb

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->j:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->b(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    const/16 v0, 0x9

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->i:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->b(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    const/16 v0, 0x12

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->h:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->b(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    const/16 v0, 0xc

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->g:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->b(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    if-nez p6, :cond_5

    const/16 v0, 0x9

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->k:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->a(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    goto/16 :goto_3

    :cond_c
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_d
    iget-boolean v0, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->q:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x7

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->f:Lcom/google/android/apps/gmm/reportmapissue/a/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/apps/gmm/reportmapissue/f;->b(Ljava/util/List;ILcom/google/android/apps/gmm/reportmapissue/a/a;)V

    goto :goto_6

    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_f
    iget v3, v0, Lcom/google/maps/g/bs;->a:I

    or-int/lit8 v3, v3, 0x8

    iput v3, v0, Lcom/google/maps/g/bs;->a:I

    iput-object v1, v0, Lcom/google/maps/g/bs;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/maps/g/bs;->g()Lcom/google/n/t;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    invoke-virtual {p3}, Lcom/google/android/apps/gmm/map/t;->a()Lcom/google/maps/a/a;

    move-result-object v3

    sget-object v0, Lcom/google/android/apps/gmm/o/a/a;->d:Lcom/google/android/apps/gmm/o/a/a;

    invoke-interface {p4, v0}, Lcom/google/android/apps/gmm/o/a/c;->a(Lcom/google/android/apps/gmm/o/a/a;)Z

    move-result v0

    if-eqz v0, :cond_12

    sget-object v0, Lcom/google/maps/g/hu;->c:Lcom/google/maps/g/hu;

    :goto_7
    iget-object v4, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->b:Lcom/google/android/apps/gmm/reportmapissue/a/g;

    iget-object v1, v4, Lcom/google/android/apps/gmm/reportmapissue/a/g;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_14

    iget-object v1, v4, Lcom/google/android/apps/gmm/reportmapissue/a/g;->b:Ljava/lang/String;

    if-eqz v1, :cond_11

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_13

    :cond_11
    const/4 v1, 0x1

    :goto_8
    if-nez v1, :cond_14

    iget-object v1, v4, Lcom/google/android/apps/gmm/reportmapissue/a/g;->b:Ljava/lang/String;

    :goto_9
    invoke-static {}, Lcom/google/r/b/a/ajp;->newBuilder()Lcom/google/r/b/a/ajr;

    move-result-object v4

    invoke-static {}, Lcom/google/maps/g/og;->newBuilder()Lcom/google/maps/g/oi;

    move-result-object v5

    invoke-static {}, Lcom/google/maps/g/eq;->newBuilder()Lcom/google/maps/g/es;

    move-result-object v6

    invoke-static {}, Lcom/google/maps/g/ei;->newBuilder()Lcom/google/maps/g/ek;

    move-result-object v7

    iget-object v8, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->r:Ljava/lang/String;

    if-nez v8, :cond_15

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_12
    sget-object v0, Lcom/google/maps/g/hu;->b:Lcom/google/maps/g/hu;

    goto :goto_7

    :cond_13
    const/4 v1, 0x0

    goto :goto_8

    :cond_14
    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->u:Ljava/lang/String;

    goto :goto_9

    :cond_15
    iget v9, v7, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v9, v9, 0x1

    iput v9, v7, Lcom/google/maps/g/ek;->a:I

    iput-object v8, v7, Lcom/google/maps/g/ek;->b:Ljava/lang/Object;

    if-nez v1, :cond_16

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_16
    iget v8, v7, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v8, v8, 0x4

    iput v8, v7, Lcom/google/maps/g/ek;->a:I

    iput-object v1, v7, Lcom/google/maps/g/ek;->c:Ljava/lang/Object;

    if-nez v3, :cond_17

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_17
    iget-object v1, v7, Lcom/google/maps/g/ek;->d:Lcom/google/n/ao;

    iget-object v8, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v8, 0x0

    iput-object v8, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v8, 0x1

    iput-boolean v8, v1, Lcom/google/n/ao;->d:Z

    iget v1, v7, Lcom/google/maps/g/ek;->a:I

    or-int/lit8 v1, v1, 0x8

    iput v1, v7, Lcom/google/maps/g/ek;->a:I

    iget-object v1, v6, Lcom/google/maps/g/es;->b:Lcom/google/n/ao;

    invoke-virtual {v7}, Lcom/google/maps/g/ek;->g()Lcom/google/n/t;

    move-result-object v7

    iget-object v8, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v7, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v7, 0x0

    iput-object v7, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v7, 0x1

    iput-boolean v7, v1, Lcom/google/n/ao;->d:Z

    iget v1, v6, Lcom/google/maps/g/es;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v6, Lcom/google/maps/g/es;->a:I

    invoke-virtual {v6, v2}, Lcom/google/maps/g/es;->a(Ljava/lang/Iterable;)Lcom/google/maps/g/es;

    move-result-object v1

    iget-object v2, v5, Lcom/google/maps/g/oi;->b:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/g/es;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v6, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/n/ao;->d:Z

    iget v1, v5, Lcom/google/maps/g/oi;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v5, Lcom/google/maps/g/oi;->a:I

    iget-object v1, v4, Lcom/google/r/b/a/ajr;->b:Lcom/google/n/ao;

    invoke-virtual {v5}, Lcom/google/maps/g/oi;->g()Lcom/google/n/t;

    move-result-object v2

    iget-object v5, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v1, v4, Lcom/google/r/b/a/ajr;->a:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v4, Lcom/google/r/b/a/ajr;->a:I

    invoke-static {}, Lcom/google/maps/g/hn;->newBuilder()Lcom/google/maps/g/hp;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->s:Lcom/google/maps/g/hs;

    if-nez v2, :cond_18

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_18
    iget v5, v1, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v5, v5, 0x1

    iput v5, v1, Lcom/google/maps/g/hp;->a:I

    iget v2, v2, Lcom/google/maps/g/hs;->n:I

    iput v2, v1, Lcom/google/maps/g/hp;->b:I

    iget-object v2, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->t:Lcom/google/maps/g/hq;

    if-nez v2, :cond_19

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_19
    iget v5, v1, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v5, v5, 0x2

    iput v5, v1, Lcom/google/maps/g/hp;->a:I

    iget v2, v2, Lcom/google/maps/g/hq;->d:I

    iput v2, v1, Lcom/google/maps/g/hp;->c:I

    if-nez v0, :cond_1a

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1a
    iget v2, v1, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v1, Lcom/google/maps/g/hp;->a:I

    iget v0, v0, Lcom/google/maps/g/hu;->d:I

    iput v0, v1, Lcom/google/maps/g/hp;->d:I

    if-nez v3, :cond_1b

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1b
    iput-object v3, v1, Lcom/google/maps/g/hp;->e:Lcom/google/maps/a/a;

    iget v0, v1, Lcom/google/maps/g/hp;->a:I

    or-int/lit8 v0, v0, 0x8

    iput v0, v1, Lcom/google/maps/g/hp;->a:I

    iget-object v0, v4, Lcom/google/r/b/a/ajr;->c:Lcom/google/n/ao;

    invoke-virtual {v1}, Lcom/google/maps/g/hp;->g()Lcom/google/n/t;

    move-result-object v1

    iget-object v2, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v0, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/r/b/a/ajr;->a:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v4, Lcom/google/r/b/a/ajr;->a:I

    invoke-static {}, Lcom/google/maps/g/cg;->newBuilder()Lcom/google/maps/g/ci;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/apps/gmm/reportmapissue/a/c;->a:Lcom/google/android/apps/gmm/base/g/c;

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/base/g/c;->z()Lcom/google/android/apps/gmm/map/b/a/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/gmm/map/b/a/q;->b()Lcom/google/maps/g/gy;

    move-result-object v1

    if-nez v1, :cond_1c

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1c
    iget-object v2, v0, Lcom/google/maps/g/ci;->b:Lcom/google/n/ao;

    iget-object v5, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v1, v2, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v1, 0x1

    iput-boolean v1, v2, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/ci;->a:I

    or-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/maps/g/ci;->a:I

    if-nez v3, :cond_1d

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1d
    iget-object v1, v0, Lcom/google/maps/g/ci;->c:Lcom/google/n/ao;

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v3, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/n/ao;->d:Z

    iget v1, v0, Lcom/google/maps/g/ci;->a:I

    or-int/lit8 v1, v1, 0x4

    iput v1, v0, Lcom/google/maps/g/ci;->a:I

    iget-object v1, v4, Lcom/google/r/b/a/ajr;->d:Lcom/google/n/ao;

    invoke-virtual {v0}, Lcom/google/maps/g/ci;->g()Lcom/google/n/t;

    move-result-object v0

    iget-object v2, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    iput-object v0, v1, Lcom/google/n/ao;->e:Lcom/google/n/at;

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/n/ao;->b:Lcom/google/n/f;

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/n/ao;->d:Z

    iget v0, v4, Lcom/google/r/b/a/ajr;->a:I

    or-int/lit8 v0, v0, 0x4

    iput v0, v4, Lcom/google/r/b/a/ajr;->a:I

    invoke-virtual {v4}, Lcom/google/r/b/a/ajr;->g()Lcom/google/n/t;

    move-result-object v0

    check-cast v0, Lcom/google/r/b/a/ajp;

    .line 55
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 70
    :goto_a
    return-void

    .line 60
    :cond_1e
    new-instance v1, Lcom/google/android/apps/gmm/reportmapissue/g;

    invoke-direct {v1, p0, p5}, Lcom/google/android/apps/gmm/reportmapissue/g;-><init>(Lcom/google/android/apps/gmm/reportmapissue/f;Lcom/google/android/apps/gmm/reportmapissue/h;)V

    sget-object v2, Lcom/google/android/apps/gmm/shared/c/a/p;->UI_THREAD:Lcom/google/android/apps/gmm/shared/c/a/p;

    invoke-interface {p2, v0, v1, v2}, Lcom/google/android/apps/gmm/shared/net/r;->a(Lcom/google/n/at;Lcom/google/android/apps/gmm/shared/net/c;Lcom/google/android/apps/gmm/shared/c/a/p;)Lcom/google/android/apps/gmm/shared/net/b;

    goto :goto_a
.end method
