.class public abstract Lcom/google/android/apps/gmm/map/m/d;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcom/google/android/apps/gmm/map/m/r;

.field b:Z

.field public final c:F

.field public final d:F


# direct methods
.method public constructor <init>(Lcom/google/android/apps/gmm/map/m/r;FF)V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 58
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/gmm/map/m/d;->a:Lcom/google/android/apps/gmm/map/m/r;

    .line 60
    iput p2, p0, Lcom/google/android/apps/gmm/map/m/d;->c:F

    .line 61
    iput p3, p0, Lcom/google/android/apps/gmm/map/m/d;->d:F

    .line 62
    return-void
.end method

.method protected static a(FF)F
    .locals 3

    .prologue
    .line 175
    cmpl-float v0, p1, p0

    if-ltz v0, :cond_1

    .line 185
    sub-float v0, p1, p0

    .line 186
    const v1, 0x40c90fdb

    add-float/2addr v1, p0

    sub-float/2addr v1, p1

    .line 187
    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    .line 191
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 187
    goto :goto_0

    .line 191
    :cond_1
    invoke-static {p1, p0}, Lcom/google/android/apps/gmm/map/m/d;->a(FF)F

    move-result v0

    neg-float v0, v0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/map/m/k;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")",
            "Lcom/google/android/apps/gmm/map/m/e;"
        }
    .end annotation
.end method

.method public final a(JLjava/util/LinkedList;ZLjava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/google/android/apps/gmm/map/m/k;",
            ">;Z",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/gmm/map/m/d;",
            ">;",
            "Ljava/lang/StringBuilder;",
            ")",
            "Lcom/google/android/apps/gmm/map/m/e;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/m/d;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 109
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    .line 133
    :goto_0
    return-object v0

    .line 113
    :cond_0
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_4

    .line 114
    invoke-interface {p5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/gmm/map/m/d;

    .line 115
    invoke-virtual {v0}, Lcom/google/android/apps/gmm/map/m/d;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_2
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    move v0, v2

    goto :goto_2

    .line 121
    :cond_2
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0

    .line 113
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 126
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/gmm/map/m/d;->c()Z

    move-result v0

    if-eq p4, v0, :cond_5

    .line 130
    sget-object v0, Lcom/google/android/apps/gmm/map/m/e;->a:Lcom/google/android/apps/gmm/map/m/e;

    goto :goto_0

    :cond_5
    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    .line 133
    invoke-virtual/range {v1 .. v6}, Lcom/google/android/apps/gmm/map/m/d;->a(JLjava/util/LinkedList;Ljava/util/List;Ljava/lang/StringBuilder;)Lcom/google/android/apps/gmm/map/m/e;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 4

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-eqz v0, :cond_1

    .line 142
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Gesture already active: "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 144
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/m/d;->b(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    .line 145
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract b(Lcom/google/android/apps/gmm/map/m/n;)Z
.end method

.method public final c(Lcom/google/android/apps/gmm/map/m/n;)V
    .locals 4

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-nez v0, :cond_1

    .line 154
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Gesture already inactive: "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 156
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    .line 157
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/m/d;->d(Lcom/google/android/apps/gmm/map/m/n;)V

    .line 158
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract d(Lcom/google/android/apps/gmm/map/m/n;)V
.end method

.method public final e(Lcom/google/android/apps/gmm/map/m/n;)Z
    .locals 4

    .prologue
    .line 165
    iget-boolean v0, p0, Lcom/google/android/apps/gmm/map/m/d;->b:Z

    if-nez v0, :cond_1

    .line 166
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Gesture is not active: "

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 168
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/apps/gmm/map/m/d;->f(Lcom/google/android/apps/gmm/map/m/n;)Z

    move-result v0

    return v0
.end method

.method protected abstract f(Lcom/google/android/apps/gmm/map/m/n;)Z
.end method
