.class public Lcom/google/android/apps/gmm/v/aq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field static final a:Lcom/google/android/apps/gmm/v/aq;

.field private static h:I


# instance fields
.field final b:I

.field c:F

.field d:F

.field e:F

.field f:F

.field g:[I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private final n:Lcom/google/android/apps/gmm/v/be;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/apps/gmm/v/aq;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/apps/gmm/v/aq;-><init>(Lcom/google/android/apps/gmm/v/be;)V

    sput-object v0, Lcom/google/android/apps/gmm/v/aq;->a:Lcom/google/android/apps/gmm/v/aq;

    .line 22
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/gmm/v/aq;->h:I

    return-void
.end method

.method constructor <init>(Lcom/google/android/apps/gmm/v/be;)V
    .locals 3

    .prologue
    const/high16 v0, -0x40800000    # -1.0f

    const/4 v2, -0x1

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput v2, p0, Lcom/google/android/apps/gmm/v/aq;->i:I

    .line 27
    iput v2, p0, Lcom/google/android/apps/gmm/v/aq;->j:I

    .line 28
    iput v2, p0, Lcom/google/android/apps/gmm/v/aq;->k:I

    .line 29
    iput v2, p0, Lcom/google/android/apps/gmm/v/aq;->l:I

    .line 31
    iput v0, p0, Lcom/google/android/apps/gmm/v/aq;->c:F

    .line 32
    iput v0, p0, Lcom/google/android/apps/gmm/v/aq;->d:F

    .line 33
    iput v0, p0, Lcom/google/android/apps/gmm/v/aq;->e:F

    .line 34
    iput v0, p0, Lcom/google/android/apps/gmm/v/aq;->f:F

    .line 36
    iput v2, p0, Lcom/google/android/apps/gmm/v/aq;->m:I

    .line 43
    const/16 v0, 0x8

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/gmm/v/aq;->g:[I

    .line 46
    iput-object p1, p0, Lcom/google/android/apps/gmm/v/aq;->n:Lcom/google/android/apps/gmm/v/be;

    .line 47
    sget v0, Lcom/google/android/apps/gmm/v/aq;->h:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/google/android/apps/gmm/v/aq;->h:I

    iput v0, p0, Lcom/google/android/apps/gmm/v/aq;->b:I

    .line 48
    iget-object v0, p0, Lcom/google/android/apps/gmm/v/aq;->g:[I

    invoke-static {v0, v2}, Ljava/util/Arrays;->fill([II)V

    .line 49
    return-void
.end method


# virtual methods
.method final a(II)V
    .locals 2

    .prologue
    .line 56
    const v0, 0x84c0

    sub-int v0, p2, v0

    .line 60
    iget v1, p0, Lcom/google/android/apps/gmm/v/aq;->m:I

    if-eq v1, p2, :cond_0

    invoke-static {p2}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    iput p2, p0, Lcom/google/android/apps/gmm/v/aq;->m:I

    .line 61
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/aq;->g:[I

    aget v1, v1, v0

    if-ne v1, p1, :cond_1

    .line 77
    :goto_0
    return-void

    .line 68
    :cond_1
    sget-boolean v1, Lcom/google/android/apps/gmm/v/ad;->a:Z

    if-eqz v1, :cond_2

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/aq;->n:Lcom/google/android/apps/gmm/v/be;

    invoke-interface {v1, p1}, Lcom/google/android/apps/gmm/v/be;->a(I)V

    .line 75
    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/gmm/v/aq;->g:[I

    aput p1, v1, v0

    .line 76
    const/16 v0, 0xde1

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_0
.end method

.method final a(IIII)V
    .locals 4

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/apps/gmm/v/aq;->i:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/aq;->j:I

    if-ne p2, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/aq;->k:I

    if-ne p3, v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/gmm/v/aq;->l:I

    if-eq p4, v0, :cond_1

    .line 101
    :cond_0
    iput p1, p0, Lcom/google/android/apps/gmm/v/aq;->i:I

    .line 102
    iput p2, p0, Lcom/google/android/apps/gmm/v/aq;->j:I

    .line 103
    iput p3, p0, Lcom/google/android/apps/gmm/v/aq;->k:I

    .line 104
    iput p4, p0, Lcom/google/android/apps/gmm/v/aq;->l:I

    .line 105
    iget v0, p0, Lcom/google/android/apps/gmm/v/aq;->i:I

    iget v1, p0, Lcom/google/android/apps/gmm/v/aq;->j:I

    iget v2, p0, Lcom/google/android/apps/gmm/v/aq;->k:I

    iget v3, p0, Lcom/google/android/apps/gmm/v/aq;->l:I

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 107
    :cond_1
    return-void
.end method
